<?php
/*
 * members.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('TABLE_HEADING_LASTNAME', 'Lastname');
define('TABLE_HEADING_FIRSTNAME', 'Firstname');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Account Created');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_COMPANY', 'Company');
define('BOX_CUSTOMERS_APPROVAL', 'Waiting Approvation');
define('HEADING_TITLE_SEARCH', 'Search');
define('HEADING_TITLE', 'Customer approval');
define('MEMBER_DEACTIVATED', '<style color:#FF0000;>Customer approval system is inactive !</style><trong> See your Configuration</strong>');

define('EMAIL_CONTACT', 'For help with any of our online services, please email us at: ' . STORE_OWNER_EMAIL_ADDRESS);
define('EMAIL_TEXT_SUBJECT', 'Account Approved');
define('EMAIL_TEXT_SUBJECT', 'Important Information attention: Count approval from '.STORE_NAME.'');
define('EMAIL_TEXT_CONFIRM', '<div align="justify">Hello ! '. "\n\n" . 'You will receive an email notifying  the creation of your account with the information allowing you to carry out your purchases on '.STORE_NAME .''. "\n\n" . 'Regards,'. "\n" . '<strong>'.STORE_NAME.' Team</strong>'. "\n" . '<p>'.STORE_NAME_ADDRESS.' '. "\n\n" . '<U><font size="2">Confidentiality Note :</font></U><font size="2">'. "\n\n" . 'This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '<p><font size="2">In accordance with the <strong>Law</strong> n 78-17 of January 6, 1978 known as Data-processing Law and Freedom, you are entitled to correction of your personal data or on request by email</font></div>' . "\n\n");
define('EMAIL_WARNING', '' . "\n\n" . '<strong>Note:</strong> This email address was used to request access to our e-commerce website. If you did not signup to be a customer, please send an email to ' . STORE_OWNER_EMAIL_ADDRESS . "\n\n");
define('EMAIL_SEPARATOR', '----------------------------------------------');

define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Recall of access on your account -');
define('EMAIL_PASSWORD_REMINDER_BODY', '<div align="justify">Hello ! '. "\n\n" . 'You asked us to open an office account on '.STORE_NAME.' and we thank you for your confidence. '. "\n" . 'Your request is validated and  your access on '.STORE_NAME.' are below. '. "\n\n" . 'Your username is :  %s  '. "\n\n" . 'Your password is : %s '. "\n\n" . '<strong>Your useful links :</strong>'. "\n\n" . '
1) To reach on your clients\' account, to modify your informations, your addresses of delivery and/or invoicing, <a href="'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account.php">'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account.php</a>'. "\n" . '2) To reach directly the follow-up of your orders, <a href="'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account_history.php">'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account_history.php</a>'. "\n" . '3) To find its password forgotten : <a href="'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'password_forgotten.php">'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'password_forgotten.php</a><br />'. "\n\n" . 'Regards,'. "\n" . '<strong>'.STORE_NAME.' Team</strong>'. "\n" . '<p>'.STORE_NAME_ADDRESS.''. "\n\n" . '<U><font size="2">Confidentiality Note :</font></U><font size="2">'. "\n" . 'This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '<p><font size="2">In accordance with the <strong>Law</strong> on Data-processing Law and Freedom, you are entitled to correction of your personal data or on request by email</font></div>');
define('EMAIL_TEXT_COUPON', STORE_NAME . ' are pleased to offer you a discount coupon that you can use at any time on the shop.' . "\n\n" . ' The coupon number is : <strong>');

define('IMAGE_ACTIVATE','Activate account');
define('ICON_EDIT_CUSTOMER', 'Edit customer');
?>