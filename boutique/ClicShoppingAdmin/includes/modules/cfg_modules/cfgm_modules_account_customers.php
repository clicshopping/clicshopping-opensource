<?php
/**
 * cfm_account_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class cfgm_modules_account_customers {
    var $code = 'modules_account_customers';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_ACCOUNT_CUSTOMERS_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_account_customers() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_account_customers/';
      $this->title = MODULE_CFG_MODULE_ACCOUNT_CUSTOMERS_TITLE;	  
    }
  }
?>