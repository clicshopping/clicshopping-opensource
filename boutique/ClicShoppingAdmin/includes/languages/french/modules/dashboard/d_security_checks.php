<?php
/**
 * d_security_checkr.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_TITLE', 'Souhaitez-vous activer la vérification de la sécurité de l\'installation de la boutique ?');
define('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_DESCRIPTION', 'Vérification de la sécurité de la boutique');
define('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SUCCESS', 'L installation de clicshopping est correctement configurée');
?>
