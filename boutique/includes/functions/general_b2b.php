<?php
/**
 * general_b2b.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
/**
 * Select the customer group 
 * @param string $OSCOM_Customer->getCustomersGroupID() the id of the customer group
 * @return the customer_group id
 * @access public
 */

  function osc_get_customers_group($default = '') {
    global $OSCOM_Customer, $OSCOM_PDO;

    $customers_group_array = array();

    if ($default) {
      $customers_group_array[] = array('id' => '',
                                       'text' => $default);
    }

    $QcustomersGroup = $OSCOM_PDO->prepare('select g.customers_group_id, 
                                                   g.customers_group_name 
                                            from :table_customers_groups as g, 
                                                 :table_customers as c 
                                            where g.customers_group_id = c.customers_group_id 
                                            order by customers_group_name
                                          ');
    $QcustomersGroup->execute();

    $customers_group = $QcustomersGroup->fetch();

    $customers_groups_id = $OSCOM_Customer->getCustomersGroupID();

    $customers_group_array[] = array('id' => $customers_groups_id,
                                     'text' => $customers_group['customers_group_name']);
    return $customers_group_array;
  }



//***********************************************************************
// Shopping Cart
//***********************************************************************

/**
 * Display the qty min order
 *
 * @param string $min_order_qty_produts, the id of the products
 * @access public
 */
////
function osc_get_min_order_qty_products($min_order_qty_produts) {
  global $products_id;

  if (osc_get_products_min_order_qty_shopping_cart($products_id) > 0) {
    $min_order_qty_produts = osc_get_products_min_order_qty_shopping_cart($products_id);
  } else {
    $min_order_qty_produts = (int)MAX_MIN_IN_CART;
  }

  return $min_order_qty_produts;
}

/**
 * Return a product's min order quantity if available and min order quantity > 1
 * in different formular ormis shopping cart -function included in application_top
 *
 * @param string $min_order_qty_values['products_min_qty_order'], the id of the products
 * @access public
 */
////

function osc_get_products_min_order_quantity($product_id) {
  global $OSCOM_PDO, $OSCOM_Customer;

  $products_id = osc_get_prid($products_id);
  $customers_group_id =  $OSCOM_Customer->getCustomersGroupID();

  if ($customers_group_id  == 0) {

    $QproductMinOrder = $OSCOM_PDO->prepare('select products_min_qty_order
                                            from :table_products
                                            where products_id = :products_id
                                          ');
    $QproductMinOrder->bindInt(':products_id', (int)$_GET['products_id'] );

    $QproductMinOrder->execute();
    $product_min_order_values = $QproductMinOrder->fetch();

    if ($product_min_order_values['products_min_qty_order'] > 0.1) {
      $min_quantity_order = (int)$product_min_order_values['products_min_qty_order'];
    } else {
      $min_quantity_order = (int)MAX_MIN_IN_CART;
      if (MAX_MIN_IN_CART > MAX_QTY_IN_CART)  {
        $min_quantity_order = (int)MAX_QTY_IN_CART;
      }
    }

  } else {

    $QcustomersGroupMinOrder = $OSCOM_PDO->prepare('select customers_group_quantity_default
                                                    from :table_customers_groups
                                                    where customers_group_id = :customers_group_id
                                                   ');
    $QcustomersGroupMinOrder->bindInt(':customers_group_id', (int)$customers_group_id );

    $QcustomersGroupMinOrder->execute();
    $customers_group_min_order = $QcustomersGroupMinOrder->fetch();

    $QcustomersProductsGroupMinOrder = $OSCOM_PDO->prepare('select products_quantity_fixed_group
                                                            from :table_products_groups
                                                            where customers_group_id = :customers_group_id
                                                          ');
    $QcustomersProductsGroupMinOrder->bindInt(':customers_group_id', (int)$customers_group_id );
    $QcustomersProductsGroupMinOrder->execute();
    $customers_products_group_min_order = $QcustomersProductsGroupMinOrder->fetch();

    if ($customers_products_group_min_order['products_quantity_fixed_group'] > 1) {
      $min_quantity_order = $customers_products_group_min_order['products_quantity_fixed_group'];
    } else if ($customers_group_min_order['customers_group_quantity_default'] > 1) {
      $min_quantity_order = (int)$customers_group_min_order['customers_group_quantity_default'];
    } else {
      $min_quantity_order = 1;
    }
  }

  return $min_quantity_order;
}

/**
 * display min. order. qty. mod 
 *
 * @param string $min_order_qty_values['products_min_qty_order'], the id of the products
 * @access public
 */
////
   function osc_get_products_min_order_qty_shopping_cart($products_id) {

      global $OSCOM_PDO, $OSCOM_Customer;

      $products_id = osc_get_prid($products_id);
      $customers_group_id =  $OSCOM_Customer->getCustomersGroupID();

      if ($customers_group_id  == 0) {
        $QminOrderQty = $OSCOM_PDO->prepare('select products_min_qty_order
                                            from :table_products
                                            where products_id = :products_id
                                          ');
        $QminOrderQty->bindInt(':products_id', (int)$products_id );

        $QminOrderQty->execute();
        $min_order_qty_values = $QminOrderQty->fetch();

      } else {

        $QcustomersGroupMinOrder = $OSCOM_PDO->prepare('select customers_group_quantity_default
                                                        from :table_customers_groups
                                                        where customers_group_id = :customers_group_id
                                                      ');
        $QcustomersGroupMinOrder->bindInt(':customers_group_id', (int)$customers_group_id );

        $QcustomersGroupMinOrder->execute();
        $customers_group_min_order = $QcustomersGroupMinOrder->fetch();

        $min_order_qty_values['products_min_qty_order'] = (int)$customers_group_min_order['customers_group_quantity_default'];
      }

      return $min_order_qty_values['products_min_qty_order'];
    }
//***********************************************************************
// Shopping Cart
//***********************************************************************



/**
 * Check the vat european
 *
 * @param string $iso, $piva, number of the company vat
 * @access public
 */
  function osc_iso_check($iso,$piva) {

       $fp1 = fsockopen ("europa.eu.int", 80, $errno1, $errstr1, 30);
       if (!$fp1) {
       //echo "$errstr1 ($errno1)<br />\n";
       $iso='2';
       } else {
       $lang="EN";
       $find="No, invalid VAT number";
       fputs ($fp1, "GET /comm/taxation_customs/vies/cgi-bin/viesquer?Lang=".$lang."&MS=".$iso."&ISO=".$iso."&VAT=".$piva." HTTP/1.1\r\nHost: europa.eu.int\r\n\r\n");
    $iso='0';
       while (!feof($fp1)) {
                     if (substr_count(fgets ($fp1,128),$find)==1){
                     $iso="1";
                     }
       }
       fclose ($fp1);
       }
   return $iso;
  } 



/**
 * Display the payment mode in different mode B2B or not
 *
 * @param string $product_price_d, the price of the product or not
 * @access public
 */
 
  function osc_get_payment_unallowed ($pay_check) {
    global $OSCOM_Customer, $OSCOM_PDO;

    $customer_group_id = $OSCOM_Customer->getCustomersGroupID();

    if (($OSCOM_Customer->isLoggedOn()) && ($customer_group_id != '0')) {

      $QpaymentsNotAllowed = $OSCOM_PDO->prepare('select group_payment_unallowed 
                                                  from :table_customers_groups
                                                  where customers_group_id = :customers_group_id
                                                ');
      $QpaymentsNotAllowed->bindInt(':customers_group_id', (int)$customer_group_id);
      $QpaymentsNotAllowed->execute();

      $payments_not_allowed = $QpaymentsNotAllowed->fetch();

      $payments_unallowed = explode(",",$payments_not_allowed['group_payment_unallowed']);
      $clearance = (in_array($pay_check, $payments_unallowed)) ? true : false;

    } else if ($OSCOM_Customer->isLoggedOn()) {
      $clearance = true;
    } else {
        $clearance = false;
    }

    return $clearance;
  }

/**
* Not Display  the payment module if customer_group = 0
 * @param string $customer_group_id, the group of the customer
 * @access public
**/
  function osc_get_payment_not_display_payment($customer_group_id) {
    global $OSCOM_Customer;

    $customer_group_id = $OSCOM_Customer->getID();

    return $customer_group_id;
  }


/**
 * Display the shipping mode in different mode B2B or not
 *
 * @param string $product_price_d, the price of the product or not
 * @access public
 */
  function osc_get_shipping_unallowed ($shipping_check) {
    global $OSCOM_Customer, $OSCOM_PDO;

    $customer_group_id = $OSCOM_Customer->getCustomersGroupID();

    if (($OSCOM_Customer->isLoggedOn()) && ($customer_group_id != '0')) {

      $QshippingNotAllowed = $OSCOM_PDO->prepare('select group_shipping_unallowed 
                                                  from :table_customers_groups
                                                  where customers_group_id = :customers_group_id
                                                ');
      $QshippingNotAllowed->bindInt(':customers_group_id', (int)$customer_group_id);
      $QshippingNotAllowed->execute();

      $shipping_not_allowed = $QshippingNotAllowed->fetch();

      $shipping_unallowed = explode(",",$shipping_not_allowed['group_shipping_unallowed']);
      $shipping_clearance = (in_array($shipping_check, $shipping_unallowed)) ? true : false;
    } elseif ($OSCOM_Customer->isLoggedOn()) {
      $shipping_clearance = true;
  } else {
      $shipping_clearance = false;
  }
    return $shipping_clearance;
  }


/**
 * Display the taxe or not mode in different mode B2B or not
 *
 * @param string $product_price_d, the price of the product or not
 * @access public
 */
  function osc_get_tax_unallowed ($tax_check) {
    global $OSCOM_Customer, $OSCOM_PDO;

    $customer_group_id = $OSCOM_Customer->getCustomersGroupID();

    if (($OSCOM_Customer->isLoggedOn()) && ($customer_group_id != '0')) {

      $Qtaxb2b = $OSCOM_PDO->prepare('select group_order_taxe 
                                     from :table_customers_groups
                                      where customers_group_id = :customers_group_id
                                    ');
      $Qtaxb2b->bindInt(':customers_group_id', (int)$customer_group_id);
      $Qtaxb2b->execute();

      $tax_b2b = $Qtaxb2b->fetch();

      if ($tax_b2b['group_order_taxe'] == '1') {
        $tax_clearance = false;
      } else {
        $tax_clearance = true;
      }
    } else {
      $tax_clearance = true;
    }
    return $tax_clearance;
  }
