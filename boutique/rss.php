<?php
/*
 * rss.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
  

*/
require('includes/application_top.php');

if (MODE_VENTE_PRIVEE == 'false') {

  require($OSCOM_Template->GeTemplatetLanguageFiles('rss'));

  $_SESSION['navigation']->remove_current_page();

// If the language is not specified
  $Qlang = $OSCOM_PDO->prepare('select languages_id,
                                       code,
                                       status
                              from :table_languages
                              where languages_id = :languages_id
                              and status = :status
                              ');
  $Qlang->bindInt(':languages_id', (int)$_SESSION['languages_id'] );
  $Qlang->bindInt(':status', 1 );
  $Qlang->execute();

// Recover the code (fr, en, etc) and the id (1, 2, etc) of the current language
    if ($Qlang->rowCount() > 0) {
      $lang_a = $Qlang->fetch();
      $lang_code = $lang_a['code'];
    }

  define(RSS_STRIP_HTML_TAGS, true);
// If the default of your catalog is not what you want in your RSS feed, then
// please change this three constants:
// Enter an appropriate title for your website
  define(RSS_TITLE, STORE_NAME);
// Enter your main shopping cart link
  define(WEBLINK, HTTP_SERVER);
// Enter a description of your shopping cart
  define(DESCRIPTION, TITLE);
  define(MAX_RSS_ARTICLES,'100');
/////////////////////////////////////////////////////////////
//That's it.  No More Editing (Unless you renamed DB tables or need to switch
//to SEO links (Apache Rewrite URL)
/////////////////////////////////////////////////////////////

  $store_name = STORE_NAME;
  $rss_title = RSS_TITLE;
  $weblink = WEBLINK;
  $description = DESCRIPTION;
  $email_address = STORE_OWNER_EMAIL_ADDRESS;

  $subtitle = '';


  if(!function_exists('eval_rss_urls')) {
    function eval_rss_urls($string) {
// rewrite all local urls to absolute urls
    return preg_replace_callback("/(href|src|action)=[\"']{0,1}(\.{0,2}[\\|\/]{1})(.*?)[\"']{0,1}( .*?){0,1}>/is","rewrite_rss_local_urls",$string);
 //"/(href|src|action)=[\"']{0,1}(\.{2}[\\|\/]{1})(.*?)[\"']{0,1}( .*?){0,1}>/is"
    }
  } 

    function rewrite_rss_local_urls($string) {

      $repl_text = $string[0];
      $repl_by = HTTP_SERVER.'\\';
      $repl_len = 0;

      $index_pos = strpos  ( $repl_text , '..');

      if ($index_pos > -1) {
        $repl_len = 2;	
      } else {
        $index_pos = strpos  ( $repl_text , '\"\\');	
        if ($index_pos > -1) {
          $repl_len = 0;	
          $index_pos+=1;
        }
      }

      if ($index_pos > -1) {
       substr_replace($repl_text, $repl_by, $index_pos,2 );	
      }

      ob_start();
        echo $repl_text;
        $return = ob_get_contents();
      ob_end_clean();
      return $return;
    }

// to strip html or not to strip html tags
    if (isset($_GET['html'])) {
      if ($_GET['html']=='false') {
         $strip_html_tags = true;	
      } else {
         $strip_html_tags = false;	
      }
    } else {
    $strip_html_tags = RSS_STRIP_HTML_TAGS;
  }

// show the products
   if (STOCK_CHECK == 'true') {
     $Qlisting =  $OSCOM_PDO->prepare('select DISTINCTROW  p.products_id,
                                                           p.manufacturers_id,
                                                           p.products_model,
                                                           p.products_price,
                                                           p.products_image,
                                                           p.products_date_added,
                                                           p.products_last_modified,
                                                           pd.products_name,
                                                           pd.products_description,
                                                           m.manufacturers_name,
                                                           p.products_tax_class_id,
                                                           p.products_ordered,
                                                           p.products_view,
                                                           p.products_quantity,
                                                           p.orders_view,
                                                           IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price,
                                                           IF(s.status, s.specials_new_products_price, p.products_price) as final_price
                        from :table_products_description pd,
                             :table_products p left join :table_manufacturers  m on p.manufacturers_id = m.manufacturers_id
                                                left join :table_specials s on p.products_id = s.products_id,
                             :table_products_to_categories p2c
                        where p.products_status = 1
                        and p.products_view = 1
                        and p.products_id = p2c.products_id 
                        and pd.products_id = p2c.products_id 
                        and pd.language_id = :language_id
                        and p.products_archive = 0
                        and p.products_quantity > 0
                        ORDER BY  p.products_date_added DESC
                        LIMIT :limit
                        ');
    } else {
     $Qlisting = $OSCOM_PDO->prepare('select DISTINCTROW p.products_id,
                                                         p.manufacturers_id,
                                                         p.products_model,
                                                         p.products_price,
                                                         p.products_image,
                                                         p.products_date_added,
                                                         p.products_last_modified,
                                                         pd.products_name,
                                                         pd.products_description,
                                                         m.manufacturers_name,
                                                         p.products_tax_class_id,
                                                         p.products_ordered,
                                                         p.products_view,
                                                         p.products_quantity,
                                                         p.orders_view,
                                                         IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price,
                                                         IF(s.status, s.specials_new_products_price, p.products_price) as final_price
                        from :table_products_description pd,
                             :table_products p left join :table_manufacturers  m on p.manufacturers_id = m.manufacturers_id
                                               left join :table_specials s on p.products_id = s.products_id,
                             :table_products_to_categories p2c
                        where p.products_status = 1
                        and p.products_view = 1
                        and p.products_id = p2c.products_id 
                        and pd.products_id = p2c.products_id 
                        and pd.language_id = :language_id
                        and p.products_archive = 0
                        ORDER BY  p.products_date_added DESC
                        LIMIT :limit
                       ');
     }

  $Qlisting->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $Qlisting->bindInt(':limit', (int)MAX_RSS_ARTICLES);

  $Qlisting->execute();



  $rss_title = BOX_INFORMATION_RSS;

  if ( $Qlisting->rowCount() > 0 ){

       if ($subtitle != '') $rss_title .= ' - '.$subtitle;
// Encoding to UTF-8
       $store_name =  osc_replace_problem_characters($store_name);
       $rss_title =  osc_replace_problem_characters($rss_title);

       $description =  osc_replace_problem_characters($description);

       $headers = getallheaders(); 
       $refresh = 1; // refresca por defecto 
       $etag = md5($last_modified);

       if(isset($headers["If-Modified-Since"])) { // Verificamos fecha enviada por el lector RSS
         $since = strtotime($headers["If-Modified-Since"]); 
         if($since >= strtotime($last_modified)) { $refresh = 0; }
       } 

       if (isset($headers["If-None-Match"])) { // Verificamos el ETag
         if (strcmp($headers["If-None-Match"], $etag) == 0) {
           $refresh = 0;
         } else {
           $refresh = 1;
         }
       }

       if ($refresh == 0) {
          header("HTTP/1.1 304 Not changed");
         ob_end_clean(); 
       }

// Begin sending of the data
       header('Content-Type: application/rss+xml; charset=UTF-8');
//header("Last-Modified: " . osc_date_raw($last_modified));
        header('Last-Modified: ' .gmdate("D, d M Y G:i:s", strtotime($last_modified)). ' GMT');
//header('Last-Modified: ' .gmdate("D, d M Y G:i:s T",strtotime($last_modified)) . ' GMT');
        header("ETag: " . md5($etag));
        echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
        echo '<?xml-stylesheet href="http://www.w3.org/2000/08/w3c-synd/style.css" type="text/css"?>' . "\n";
        echo '<!-- RSS for ' . $store_name . ', generated on ' . gmdate("D, d M Y G:i:s", strtotime($last_modified)) . ' GMT'. ' -->' . "\n";
?>
        <rss version="2.0"
          xmlns:atom="http://www.w3.org/2005/Atom"
          xmlns:ecommerce="http://shopping.discovery.com/erss/"
          xmlns:media="http://search.yahoo.com/mrss/">
          <channel>
            <title><?php echo $rss_title; ?></title>
            <link><?php echo $weblink;?></link>
            <description><?php echo $description; ?></description>
            <atom:link href="<?php echo osc_href_link('rss.php', $_SERVER['QUERY_STRING'] = str_replace('&','&amp;',$_SERVER['QUERY_STRING']), 'NONSSL', false ); ?>" rel="self" type="application/rss+xml" />
            <language><?php echo $lang_code; ?></language>
            <lastBuildDate><?php echo gmdate("D, d M Y H:i:s", strtotime($last_modified)). ' GMT'; ?></lastBuildDate>
            <image>
              <url><?php echo $weblink . DIR_WS_HTTP_CATALOG . DIR_WS_ICONS.'icon_feed.gif';?></url>
              <title><?php echo $rss_title; ?></title>
              <link><?php echo $weblink;?></link>
              <description><?php echo $description; ?></description>
            </image>
           <docs>http://blogs.law.harvard.edu/tech/rss</docs>
<?php
// Format results by row
          while( $row = osc_db_fetch_array($query) ){
             $id = $row['products_id'];
             $quantity = $row['products_quantity'];
// RSS Links for Ultimate SEO (Gareth Houston 10 May 2005)
             $link = osc_href_link('product_info.php', 'products_id=' . (int)$id, 'NONSSL', false);
             $model = $row['products_model'];
             $image = $row['products_image'];
             $added = date(r,strtotime($row['products_date_added']));
// Setting and cleaning the data
             $name = $row['products_name'];
             $desc = eval_rss_urls($row['products_description']);

// Encoding to UTF-8  
             if ($strip_html_tags ==true) {
               $name = osc_strip_html_tags($name);
               $desc = osc_strip_html_tags($desc);
             }
  
             $name = osc_replace_problem_characters($name);
             $desc = osc_replace_problem_characters($desc);


    if (osc_not_null($row['specials_new_products_price'])) {
      $price = osc_add_tax($row['specials_new_products_price'], osc_get_tax_rate($row['products_tax_class_id']));
    } else {
       $price = osc_add_tax($row['products_price'], osc_get_tax_rate($row['products_tax_class_id']));
    }

//add price limit decimals to 2  
$price = sprintf("%01.2f", $price);

// Setting the URLs to the images and buttons
             $relative_image_url = osc_image(DIR_WS_IMAGES . $image, $name, 'style="float: left; margin: 0px 8px 8px 0px;"');
             $relative_image_url = str_replace('" />', '', $relative_image_url);
             $relative_image_url = str_replace('<img src="', '', $relative_image_url);
             $image_url = HTTP_SERVER . DIR_WS_CATALOG . $relative_image_url;
// setup the price
if (PRICES_LOGGED_IN == 'false') {
  if ($row['orders_view']  == '1') {
             $relative_buy_url = osc_image_button('button_shopping_cart.gif', IMAGE_BUTTON_IN_CART, 'style="margin: 0px;"');
             $relative_buy_url = str_replace('" />', '', $relative_buy_url);
             $relative_buy_url = str_replace('<img src="', '', $relative_buy_url);
             $buy_url = HTTP_SERVER . DIR_WS_CATALOG . $relative_buy_url;
  }
}
             $relative_button_url = osc_image_button('button_more_info.gif', IMAGE_BUTTON_MORE_INFO, 'style="margin: 0px;"');
             $relative_button_url = str_replace('" />', '', $relative_button_url);
             $relative_button_url = str_replace('<img src="', '', $relative_button_url);
             $button_url = HTTP_SERVER . DIR_WS_CATALOG . $relative_button_url;

  // http://www.w3.org/TR/REC-xml/#dt-chardata
  // The ampersand character (&) and the left angle bracket (<) MUST NOT appear in their literal form
             $name = str_replace('&','&amp;',$name);
             $desc = str_replace('&','&amp;',$desc);
             $link = str_replace('&','&amp;',$link);
             $cat_name = str_replace('&','&amp;',$cat_name);

             $name = str_replace('<','&lt;',$name);
             $desc = str_replace('<','&lt;',$desc);
             $link = str_replace('<','&lt;',$link);
             $cat_name = str_replace('<','&lt;',$cat_name);

             $name = str_replace('>','&gt;',$name);
             $desc = str_replace('>','&gt;',$desc);
             $link = str_replace('>','&gt;',$link);
             $cat_name = str_replace('>','&gt;',$cat_name);
  
// shopping cart display
             $buy_link = osc_href_link('shopping_cart.php', osc_get_all_get_params(array('action')) . 'action=buy_now&products_id=' . (int)$id);

// Encoding to UTF-8

//             $buy_link = str_replace('&','&amp;',$buy_link);
             $buy_link = str_replace('<','&lt;',$buy_link);
             $buy_link = str_replace('>','&gt;',$buy_link);

// Writing the output
             echo '<item>' . "\n";
             echo '<title>' . $name . ' - ' . $model . '</title>' . "\n";
             echo '<link>' . $link . '</link>';
             echo '<description>';

             if ($ecommerce == '') {
               echo '<![CDATA[<table width="100%"  border="0" cellspacing="0" cellpadding="0"><td align="left">' .round($price,2). ' - ' . DEFAULT_CURRENCY .'&nbsp;&nbsp;]]>';
//               echo '<![CDATA[<a href="' .$buy_link. '"><img src="' . $buy_url . '" border="0"></a>&nbsp;]]>';
               echo '<![CDATA[<a href="' . $link . '"><img src="' . $button_url . '" border="0"></a></td></tr>]]>' . "\n";
             } 
             if ($ecommerce=='' && $image != '') {
               echo '<![CDATA[<td align="right"><img src="' . $image_url . '"></td></tr></table>]]>';
             }
             echo $desc ;

             echo '</description>' . "\n";
             echo '  <guid>' . $link . '</guid>' . "\n";
             echo '  <pubDate>' . gmdate("D, d M Y H:i:s", strtotime($added)). ' GMT' . '</pubDate>' . "\n";
            
            if($ecommerce!='') {
              echo '  <media:thumbnail url="' . $image_url . '">' . $image_url . '</media:thumbnail>' . "\n";
              echo '  <ecommerce:SKU>' . $id . '</ecommerce:SKU>' . "\n";
              echo '  <ecommerce:listPrice currency="' . DEFAULT_CURRENCY . '">' . $price . '</ecommerce:listPrice>' . "\n";
            }
          echo '</item>' . "\n";
          }
        }

        echo '</channel>' . "\n";
        echo '</rss>' . "\n";
}
  include_once('includes/application_bottom.php');
