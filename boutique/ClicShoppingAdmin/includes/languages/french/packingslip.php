<?php
/**
 * packingslip.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('TABLE_HEADING_QTE', 'Qté');
define('TABLE_HEADING_COMMENTS', 'Commentaires');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Référence');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_TAX', 'Taxes');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Prix (HT)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Prix (TTC)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (HT)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (TTC)');
define('DELIVERY_NOTE', 'Bon de Livraison');

// Cadre adresses facturation et livraison
define('ENTRY_SOLD_TO', 'VENDU A :');
define('ENTRY_SHIP_TO', 'LIVRE A :');

// Méthode de paiement
define('ENTRY_PAYMENT_METHOD', 'Mode de paiement :');

// Affichage selon la méthode d'affichage et le statut une Facture ou une Commande
define('ENTRY_NUMERO_INVOICE', 'Numéro de facture :');
define('ENTRY_NUMERO_ORDER', 'Numéro de commande :');
define('ENTRY_PAYMENT_CANCEL','Commande annulée : ');
define('ENTRY_DATE_ORDER', 'Date :');
define('ENTRY_PAYMENT_DATE', 'Date : ');
define('PRINT_ORDER_DATE', 'Date ');
define('ENTRY_INVOICE_DATE_PRINT', 'Date : ');

// Email et URL de la société
define('ENTRY_HTTP_SITE','Site Internet : ');
define('PRINT_INVOICE_URL', HTTP_CATALOG_SERVER );

// Customer information
define('ENTRY_PHONE','Téléphone : ');
define('ENTRY_EMAIL','Adresse e-mail : ');
define('ENTRY_CUSTOMER_INFORMATION','Informations Client');
define('ENTRY_CUSTOMER_NUMBER','Référence Client : ');

// Remerciement
define('THANK_YOU_CUSTOMER', 'Nous vous remercions d\'avoir passé une commande chez nous. Nous espérons vous revoir bientôt');

// Reserve de propriété
define('RESERVE_PROPRIETE', 'Réserve de propriété : Selon la loi en vigueur ' . STORE_NAME . ' se réserve la propriété des marchandises livrées jusqu\'au paiement intégral du prix.');
define('RESERVE_PROPRIETE_NEXT', 'Il pourra en demander la restitution par lettre recommandée. Pour tout autre information, veuillez consulter nos conditions générales de vente');
define('RESERVE_PROPRIETE_NEXT1', 'disponible sur le site é l\'adresse suivante : '. HTTP_SERVER . DIR_WS_CATALOG . SHOP_CODE_URL_CONDITIONS_VENTE);

// gestion de la double taxe ou non 
if (DISPLAY_DOUBLE_TAXE == 'false') {
  define('ENTRY_INFO_SOCIETE', '' . SHOP_CODE_CAPITAL . ' - ' . SHOP_CODE_RCS . ' - ' . SHOP_CODE_APE . ' ');
  define('ENTRY_INFO_SOCIETE_NEXT', ' '. TVA_SHOP_INTRACOM . ' - ' .  SHOP_DIVERS . ' ');
} else {
  define('ENTRY_INFO_SOCIETE', '' . SHOP_CODE_CAPITAL . ' - ' . SHOP_CODE_RCS . ' - ' . SHOP_CODE_APE . ' ');
  efine('ENTRY_INFO_SOCIETE_NEXT', ' '. TVA_SHOP_PROVINCIAL . ' - ' .TVA_SHOP_FEDERAL . ' - ' . SHOP_DIVERS . ' ');
}

?>