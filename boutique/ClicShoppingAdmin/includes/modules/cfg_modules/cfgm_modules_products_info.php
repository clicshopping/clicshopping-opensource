<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class cfgm_modules_products_info {
    var $code = 'modules_products_info';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_PRODUCTS_INFO_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_products_info() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_products_info/';
      $this->title = MODULE_CFG_MODULE_PRODUCTS_INFO_MODULES_TITLE;
    }
  }
?>
