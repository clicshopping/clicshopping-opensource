<?php
/**
 * ot_discount_coupon.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class ot_discount_coupon {
    var $title, $output, $coupon;

    function ot_discount_coupon() {
      $this->code = 'ot_discount_coupon';
      $this->title = MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TITLE;
      $this->description = MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DESCRIPTION;
      $this->enabled = ((MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true') ? true : false);
      $this->sort_order = MODULE_ORDER_TOTAL_DISCOUNT_COUPON_SORT_ORDER;
      $this->output = array();
    }

    function process() {
      global $order, $currencies;

      if( is_object( $order->coupon ) ) {
//if the order total lines for multiple tax groups should be displayed as one, add them all together
        if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_LINES == 'false' ) {
          $discount_lines = array( array_sum( $order->coupon->applied_discount ) );
        } else {
          $discount_lines = $order->coupon->applied_discount;
        }

        if( is_array( $discount_lines ) ) foreach( $discount_lines as $tax_group => $discount ) {
          if( $discount > 0 ) {
//add in the tax if needed:
           if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TAX == 'Display discount with discounted tax applied' && is_array( $order->coupon->discount_tax ) ) $discount += array_sum( $order->coupon->discount_tax );
//determine the display type (with or without the minus sign):
              $display_type = ( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TYPE == 'true' ? '-' : '' );
              $text = $display_type . $currencies->format( $discount, true, $order->info['currency'], $order->info['currency_value'] );
//add debug text if debug is on:

              if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DEBUG == 'true' )  {
                $text .= print_r( '<br /><br /><!-- Discount Coupons DEBUG<br /><br />');
                $text .= print_r($order,true);
                $text .= print_r('<br /><br />End Discount Coupons DEBUG--><br /><br />');
              }

              $this->output[] = array( 'title' => $order->coupon->format_display( $tax_group ) . ':',
                                       'text' => $text,
                                       'value' => $display_type . $discount
                                      );
            }
         }
//determine if we need to display a second line to show tax no longer applied

        if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TAX == 'Display discounted tax in separate line' && is_array( $order->coupon->discount_tax ) ) {
          $discounted_tax = array_sum( $order->coupon->discount_tax );
          $text = $display_type . $currencies->format( $discounted_tax, true, $order->info['currency'], $order->info['currency_value'] );
          $this->output[] = array( 'title' => MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TAX_NOT_APPLIED . ':',
                                   'text' => $text,
                                   'value' => $display_type . $discounted_tax
                                  );
        }
      } else {
        $this->enabled = false;
      }
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = osc_db_query("select configuration_value 
                                    from configuration
                                    where configuration_key = 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS'
                                    ");
        $this->_check = osc_db_num_rows($check_query);
      }

      return $this->_check;
    }

    function keys() {
      return array( 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TYPE',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_SUBTOTAL',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TOTAL',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TAX',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_EXCLUDE_SPECIALS',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_RANDOM_CODE_LENGTH',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_LINES',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_ALLOW_NEGATIVE',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_USE_LANGUAGE_FILE',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_CONFIG',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DEBUG',
                    'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_SORT_ORDER'
                  );
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Affichage des coupons r&eacute;duction ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS', 'true', 'Souhaitez utiliser les coupons r&eacute;duction ?', '6', '1', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier).<br />', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_SORT_ORDER','9','Ordre de tri d\'affichage','6','1','',now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous afficher la remise avec le signe(-) ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TYPE', 'true','<br /><strong>true</strong> - La remise du coupon sera affich&eacute;e avec le signe (-)<br />', '6','3','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous afficher le sous-total avec la remise appliqu&eacute;e ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_SUBTOTAL', 'false', '<br /><strong>true</strong> - Le sous total de la commande sera affich&eacute; avec la remise du coupon appliqu&eacute;e','6', '4', 'osc_cfg_select_option(array(\'true\', \'false\'), ',  now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous calculer la remise apr&grave;s le calcul de la taxe sur le produit ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TOTAL', 'false', '<br /><strong>true</strong> - Le calcul de la remise sera fait apr&egrave;s le calcul de la taxe sur le produit<br /><br /><strong>Note :</strong><br /><br />la remise apr&grave;s le calcul de la taxe sur le produit doit etre mis sur false ','6', '4', 'osc_cfg_select_option(array(\'true\', \'false\'), ',  now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values  ('Souhaitez vous afficher taxe appliqu&eacute;e sur le coupon ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_TAX', 'None', 'Veuillez s&eacute;lectionner une m&eacute;thode d\'affichage de la taxe.<br />- None = Aucun<br />- Display discounted tax in separate line = Afficher la remise sur une ligne s&eacute;par&eacute;e<br />- Display discount with discounted tax applied = Afficher la remise avec l\'application de la taxe', '6', '5', 'osc_cfg_select_option(array(\'None\', \'Display discounted tax in separate line\', \'Display discount with discounted tax applied\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous exclure les promotions ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_EXCLUDE_SPECIALS', 'true', '<br /><strong>true</strong> - Les produits ayant une promotion active seront exclues du calcul de la remise', '6', '6', 'osc_cfg_select_option(array(\'true\', \'false\'), ',  now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Veuillez indiquer la taille du coupon g&eacute;n&eacute;r&eacute;e al&eacute;eatoirement','MODULE_ORDER_TOTAL_DISCOUNT_COUPON_RANDOM_CODE_LENGTH','6','Taille du coupon &agrave; g&eacute;en&eacute;er&eacute;e al&eacute;eatoirement.', '6', '7', '', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous afficher la ligne des taxes sur la remise du coupon ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_LINES', 'false', '<br /><strong>true</strong> - la remise du coupon sera affich&eacute;e avec chaque groupe de taxes pour la commande<br /><strong>false</strong> - La remise du coupon affichera la somme de toutes les taxes sur une seule ligne', '6', '8', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous permettre les commandes quand l\'affichage est n&eacute;gatif ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_ALLOW_NEGATIVE',  'false', '<br /><strong>false</strong> - Les commandes dont le total est n&eacute;gatif ne seront pas accept&eacute;es, la commande totale aura un tarif &eacute;gal &agrave; 0.', '6', '9', 'osc_cfg_select_option(array(\'true\', \'false\'), ',  now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Utilisez la langage du navigateur du visiteur ?', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_USE_LANGUAGE_FILE', 'true', '<br /><strong>true</strong> - Clishopping analyse le langage du navigateur visitant le site. Si le langage n\'est pas identifi&eacute;, il affichera un texte par d&eacute;faut sinon le texte dans la langue utilis&eacute;e', '6', '10', 'osc_cfg_select_option(array(\'true\', \'false\'), ',  now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Type d\'affichage du coupon', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_CONFIG','Coupon remise [code] appliqu&eacute;', 'Vous pouvez param&eacute;trer le type d\affichage du coupon .<br /><br />Variables:<br />[code] : code du coupon<br />[coupon_desc] : description <br />[discount_amount] : montant<br />[min_order] : minimum de la commande <br />[number_available] :  nombre disponible<br />[tax_desc] :  description de la taxe','6', '11', '',  now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Mode d&eacute;bugage', 'MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DEBUG', 'false', 'Mode qui permet d\'&eacute;valuer le module et de voir les param&egrave;tres dans la page checkout confirmation page.', '6', '12', 'osc_cfg_select_option(array(\'true\', \'false\'), ',  now())");
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>
