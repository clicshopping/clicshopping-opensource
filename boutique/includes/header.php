<?php
/*
   * header.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/

  if ( $OSCOM_MessageStack->exists('header') ) {
    echo $OSCOM_MessageStack->get('header');
  }

  if (MODE_VENTE_PRIVEE == 'true') {
    if ( (!$OSCOM_Customer->isLoggedOn())  && (!strstr($_SERVER['PHP_SELF'], 'login.php')) ) {
      if (
        (!strstr($_SERVER['PHP_SELF'],'create_account.php')) &&
        (!strstr($_SERVER['PHP_SELF'],'password_forgotten.php')) &&
        (!strstr($_SERVER['PHP_SELF'],'create_account_pro.php')) &&
        (!strstr($_SERVER['PHP_SELF'],'contact_us.php'))
      ) {
        $_SESSION['navigation']->set_snapshot();
        osc_redirect(osc_href_link('login.php', '', 'SSL'));
      }
    }
  }

   if ($template_bloc != 'false') {
     $OSCOM_Template->buildBlocks();
   }  
  
  if (!$OSCOM_Template->hasBlocks('boxes_column_left')) {
    $OSCOM_Template->setGridContentWidth($OSCOM_Template->getGridContentWidth() + $OSCOM_Template->getGridColumnWidth());
  }

  if (!$OSCOM_Template->hasBlocks('boxes_column_right')) {
    $OSCOM_Template->setGridContentWidth($OSCOM_Template->getGridContentWidth() + $OSCOM_Template->getGridColumnWidth());
  }
