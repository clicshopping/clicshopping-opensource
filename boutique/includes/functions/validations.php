<?php
/**
 * validation.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Function    : osc_validate_email
  //
  // Arguments   : email   email address to be checked
  //
  // Return      : true  - valid email address
  //               false - invalid email address
  //
  // Description : function for validating email address that conforms to RFC 822 specs
  //
  //              This function will first attempt to validate the Email address using the filter
  //              extension for performance. If this extension is not available it will
  //              fall back to a regex based validator which doesn't validate all RFC822
  //              addresses but catches 99.9% of them. The regex is based on the code found at
  //              http://www.regular-expressions.info/email.html
  //
  //              Optional validation for validating the domain name is also valid is supplied
  //              and can be enabled using the administration tool.
  //
  // Sample Valid Addresses:
  //
  //    first.last@host.com
  //    firstlast@host.to
  //    first-last@host.com
  //    first_last@host.com
  //
  // Invalid Addresses:
  //
  //    first last@host.com
  //    first@last@host.com
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  function osc_validate_email($email) {
    $email = trim($email);

    if ( strlen($email) > 255 ) {
      $valid_address = false;
    } else {
      $valid_address = (bool)filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    if ($valid_address && ENTRY_EMAIL_ADDRESS_CHECK == 'true') {
      $domain = explode('@', $email);

      if ( !checkdnsrr($domain[1], "MX") && !checkdnsrr($domain[1], "A") ) {
        $valid_address = false;
      }
    }
    return $valid_address;
  }
  

  function osc_validate_number_email($number_email_confirmation) {
      if ($number_email_confirmation != 5) {
        $valid_number_email_confirmation = false;
      } else {
        $valid_number_email_confirmation = true;
      }
   return $valid_number_email_confirmation;
  }


/*
 * Analyse the customer email and validate or not the email
 * @param string : $mail : mail of the customer
 * @param string : $monEmail : an email of the server
 * @param string return : value 0,1,2,-1, the value of the test result
 * return : 1  valid email
 * return : -1 email no valid
 * return : 2 time out risk;
 * return : 0 try later;
*/ 
  

  function osc_validate_mail($mail,$monMail) {
    $resultat=-1;
    $erreur=false;
    $addr=explode("@",$mail);
    if(getmxrr($addr[1],$hosts,$weight)) {
        foreach($hosts as $host) {   // on va tester tous les MX
            $Connect = @fsockopen($host,25,$errno,$errstr);
            if($Connect) {
                $reponse1=substr(fgets($Connect, 1024),0,1); //seul le premier caractere de la reponse nous interesse
                fputs ($Connect, "HELO {$_SERVER['HTTP_HOST']}\r\n");
                $reponse2 = substr(fgets ( $Connect, 1024 ),0,1); //seul le premier caractere de la reponse nous interesse
                fputs ($Connect, "MAIL FROM: <".$monMail.">\r\n");
                $reponse3 = substr(fgets ( $Connect, 1024 ),0,1); //seul le premier caractere de la reponse nous interesse
                fputs ($Connect, "RCPT TO: <".$mail.">\r\n");
                $reponse4 = substr(fgets ($Connect, 1024),0,1); //seul le premier caractere de la reponse nous interesse
                if($reponse1==2 && $reponse2==2 && $reponse3==2 && $reponse4==2)  return(1); // c'est bon, l'adresse mail est valide
                else if ($reponse1==5 || $reponse2==5 || $reponse3==5 || $reponse4==5) $erreur=true; // l'adresse est invalide
                else if ($reponse1==3 || $reponse2==3 || $reponse3==3 || $reponse4==3) $resultat=3; // demande en cours de traitement, vraisemblablement timeout un peu long
                else if ($reponse1==4 || $reponse2==4 || $reponse3==4 || $reponse4==4) $resultat=0; // erreur temporaire, faudra re-essayer
                else $resultat=0; // bon, on essayera plus tard               
            }
        }
        //si on arrive ici, c'est qu'il y a une erreur ou une indisponibilite/erreur temporaire
        if($resultat==0) return(0); // au moins un MX a signale une erreur temporaire, ca vaut le coup de re-essayer plus tard
        else if($resultat==3) return(0); // vraisemblablement timeout un peu long, ca vaut le coup de re-essayer plus tard
        else if($erreur) return(-1); // l'adresse mail a de forte chance d'etre incorrecte
        else return(2);
    } else {
        return(-1);
    }
  }  
?>