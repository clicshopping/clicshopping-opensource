<?php
/**
 * index.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

  require('includes/application_top.php');

  echo osc_hide_session_id();

  require('includes/functions/statistics.php');
  require_once('calcul_statistics.php');

// Langues
  $module_directory = DIR_WS_MODULES . 'index';
  $languages = osc_get_languages();
  $languages_array = array();
  $languages_selected = DEFAULT_LANGUAGE;
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    $languages_array[] = array('id' => $languages[$i]['code'],
                               'text' => $languages[$i]['name']);
    if ($languages[$i]['directory'] == $_SESSION['language']) {
      $languages_selected = $languages[$i]['code'];
    }
  }
  require('includes/header.php');
?>
  <!--[if IE]><script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/excanvas.min.js"></script><![endif]-->
 <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.time.min.js"></script>

<!-- header_eof //-->
<!-- body //-->
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>


        <div class="adminTitle">
            <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/home.gif', HEADING_TITLE, '40', '40'); ?></span>
            <span class="col-md-7 pageHeading"><?php echo HEADING_TITLE; ?></span>

<?php
  echo osc_draw_form('languages', 'index.php', '', 'get');
  if (sizeof($languages_array) > 1) {
?>

            <span class="col-md-1"><?php echo osc_draw_form('adminlanguage', 'index.php', '', 'get') .  osc_draw_pull_down_menu('language', $languages_array, $languages_selected, 'onchange="this.form.submit();"')  . osc_hide_session_id(); ?></form></span><?php
<?php
  }
?>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('orders.php') . '">' .osc_image(DIR_WS_IMAGES . 'categories/orders.gif', HEADING_SHORT_ORDERS, '40', '40') .'</a>'; ?></span>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('categories.php') . '">' . osc_image(DIR_WS_IMAGES . 'categories/categorie_produit.gif', HEADING_SHORT_CATEGORIES, '40', '40').'</a>'; ?></span>
        </div>
        <div class="clearfix"></div>

<div><?php echo osc_draw_separator('pixel_trans.gif', '', '10'); ?></div>
<table border="0" width="1190" cellspacing="2" cellpadding="2" align="center">
  <tr valign="top">
    <td><table border="0" width="830" cellspacing="2" cellpadding="2" class="boxeCentral">
      <td width="100%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr valign="top">
<?php
  if ( defined('MODULE_ADMIN_DASHBOARD_INSTALLED') && osc_not_null(MODULE_ADMIN_DASHBOARD_INSTALLED) ) {
    $adm_array = explode(';', MODULE_ADMIN_DASHBOARD_INSTALLED);

    for ( $i=0, $n=sizeof($adm_array); $i<$n; $i++ ) {
      $adm = $adm_array[$i];

      if (strpos($adm, '\\') !== false) {
          $class = $adm;
        } else {
          $class = substr($adm, 0, strrpos($adm, '.'));

          if ( !class_exists($class) ) {
              include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/dashboard/' . $adm);
              include(DIR_WS_MODULES . 'dashboard/' . $class . '.php');
            }
      }
      $ad = new $class();

      if ( $ad->isEnabled() ) {
        echo $ad->getOutput();
      }
    }
  }
?>
          </tr>
        </table></td>
        <td align="right" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="400">

              <div>
                <ul class="nav nav-tabs" role="tablist"  id="myTab">
                  <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_STATISITICS; ?></a></li>
                  <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_CUSTOMER; ?></a></li>
                  <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_IMAGINIS; ?></a></li>
                </ul>

                <div class="tabsClicShopping">
                  <div class="tab-content">
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Statistics                                   //-->
<!-- ------------------------------------------------------------ //-->
                    <div class="tab-pane active" id="tab1">
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td align="left" class="mainTitle"><?php echo HEADING_TITLE_ORDERS; ?></td>
                       </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
<?php
  $QordersStatus = $OSCOM_PDO->prepare('select orders_status_name,
                                                orders_status_id
                                        from :table_orders_status
                                        where language_id = :language_id
                                      ');
  $QordersStatus->bindint(':language_id', (int)$_SESSION['languages_id']);
  $QordersStatus->execute();

  while ($orders_status = $QordersStatus->fetch() ) {

    $QordersPending = $OSCOM_PDO->prepare('select count(*) as count
                                           from :table_orders
                                           where orders_status = :orders_status
                                         ');
    $QordersPending->bindInt(':orders_status',  (int)$orders_status['orders_status_id']);
    $QordersPending->execute();

    $orders_pending = $QordersPending->fetch();

    if ($orders_pending['count'] != 0) {
?>

                       <tr>
                         <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('orders.php', 'selected_box=customers&status=' . $orders_status['orders_status_id']) . '"><font class="main">' . $orders_status['orders_status_name'] . '</font></a>';?></td>
                         <td width="100%" class="mainTable"><?php echo $orders_pending['count'];?></td>
                       </tr>
<?php
    }
  }
?>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                          <td class="mainTitle"><?php echo HEADING_TITLE_DIVERS; ?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
<?php
  if ($panier_client_total != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><font class="main"><?php echo BOX_ENTRY_BASKET; ?></font></td>
                          <td width="100%" class="mainTable" align="right"><?php echo $panier_client_total; ?></td>
                        </tr>
<?php
  }
  if ($purchase_average_order_total_delivery != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><font class="main"><?php echo BOX_ENTRY_BUY_AVERAGE_ORDER_TOTAL_DELIVERY; ?></font></a></td>
                          <td width="100%" class="mainTable" align="right"><?php echo $purchase_average_order_total_delivery; ?></td>
                        </tr>
<?php
  }
  if ($customers_total != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('customers.php', 'selected_box=customers') .'"><font class="main">' . BOX_ENTRY_CUSTOMERS . '</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><?php echo $customers_total; ?></td>
                        </tr>
<?php
  }
  if (stat_analyse_customers_man($stat_analyse_customers_man) != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><font class="main"><?php echo TEXT_STATS_MAN; ?></font></td>
                          <td width="100%" class="mainTable" align="right"><?php echo stat_analyse_customers_man(); ?></td>
                        </tr>
<?php
  }

  if (stat_analyse_customers_woman($stat_analyse_customers_woman) != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><font class="main"><?php echo TEXT_STATS_WOMAN; ?></font></td>
                          <td width="100%" class="mainTable" align="right"><?php echo stat_analyse_customers_woman(); ?></td>
                        </tr>
<?php
  }
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('categories.php') . '"><font class="main">' . BOX_ENTRY_PRODUCTS . '</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><?php echo $products_total; ?></td>
                        </tr>
<?php
  if (stat_gen5() != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('categories.php') . '"><font class="main">' . BOX_ENTRY_PRODUCTS_OFF_LINE . '</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><?php echo  stat_gen5(); ?></td>
                        </tr>
<?php
}
  if (stat_gen4() != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('products_archive.php') . '"><font class="main">' . BOX_ENTRY_PRODUCTS_ARCHIVES . '</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><?php echo stat_gen4(); ?></td>
                        </tr>
<?php
  }
  if ($reviews_total != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('reviews.php') . '"><font class="main">' . BOX_ENTRY_REVIEWS . '</font></a>'; ?></td>
                          <td class="mainTable" align="right"><font class="main"><?php echo $reviews_total; ?></font></td>
                        </tr>
<?php
  }
  if ( $customers_total_newsletter != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<font class="main">' . BOX_ENTRY_NEWSLETTER . '</font>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><font class="main"><?php echo $customers_total_newsletter; ?></font></td>
                        </tr>
<?php
  }
  if ($customers_total_newsletter_no_account  != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('stats_newsletter_no_account.php') . '"><font class="main">' . BOX_ENTRY_NEWSLETTER_NO_ACCOUNT . '</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><font class="main"><?php echo $customers_total_newsletter_no_account ; ?></font></td>
                        </tr>
<?php
  }
  if ($customers_total_notification != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('stats_products_notifications.php') . '"><font class="main">' .BOX_ENTRY_NOTIFICATION .'</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><font class="main"><?php echo $customers_total_notification; ?></font></td>
                        </tr>
<?php
  }
  if ($stock_expected != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' .'products_expected.php' . '"><font class="main">' . BOX_ENTRY_STOCK_EXPECTING . '</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><?php echo $stock_expected; ?></td>
                        </tr>
<?php
  }
  if ($contact_customers_total != 0) {
?>
                        <tr>
                          <td width="100%" class="mainTable"><?php echo '<a href="' . osc_href_link('contact_customers.php') . '"><font class="main">'. BOX_ENTRY_CONTACT_DEMAND_NO_ARCHIVE .'</font></a>'; ?></td>
                          <td width="100%" class="mainTable" align="right"><?php echo $contact_customers_total; ?></td>
                        </tr>
<?php
  }
?>
                      </table>
                    </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Contract                                    //-->
<!-- ------------------------------------------------------------ //-->
                    <div class="tab-pane" id="tab2">
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td align="left" class="mainTitle"><?php echo TITLE_CUSTOMER_INFORMATION; ?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                        <tr>
                          <td valign="top" class="mainTable"><?php echo PACK; ?></td>
                          <td valign="top" class="mainTable" width="70%"><?php echo 'B2B / B2C'; ?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?php echo CONTRACT_DATE; ?></td>
                          <td valign="top" class="mainTable" width="70%"><?php echo $contract_date; ?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?php echo EMAIL_SIZE; ?></td>
                          <td valign="top" class="mainTable" width="70%"><?php echo EMAIL_SIZE_INFO; ?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?php echo CONTRACT_RENEWAL; ?></td>
                          <td valign="top" class="mainTable"><?php echo CONTRATC_RENEWAL_INFO; ?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?php echo NOTICE_TERMINATION; ?></td>
                          <td valign="top" class="mainTable"><?php echo NOTICE_TERMINATION_INFO; ?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?php echo SPACE; ?></td>
                          <td valign="top" class="mainTable"><?php echo $space_disk;?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?php echo BANDWIDTH; ?></td>
                          <td valign="top" class="mainTable"><?php echo $bandwidth;?></td>
                        </tr>
                        <tr>
                          <td valign="top" class="mainTable"><?PHP echo TITLE_WEB_SITE_SIZE; ?> </td>
                          <td valign="top" class="mainTable"><?php echo osc_size_readable(osc_dirsize('.')); ?></td>
                        </tr>
                        <tr>
                          <td valign="top"  class="mainTable"><?php echo TITLE_DB_INDEX; ?> </td>
                          <td valign="top"  class="mainTable"><?php echo osc_size_bd($size_bd) .' MB'; ?></td>
                        </tr>
                        <tr>
                          <td width="40%" class="mainTable"><?php echo BACKUP_SITE; ?></td>
                          <td width="60%" class="mainTable"><?php echo $backup; ?></td>
                        </tr>
                        <tr>
                          <td width="40%" class="mainTable"><?php echo ASK_INFORMATIONS; ?></td>
                          <td width="60%" class="mainTable"><a href="mailto:support@e-imaginis.com"><font class="main"><?php echo 'Support'; ?></font></a></td>
                        </tr>
                        <tr>
                          <td>System Memory</td>
                          <td>
<?php
  $memory = round(memory_get_usage() / 1048576,2);
  echo "use : ".$memory." Megabytes";
?>
                          </td>
                        </tr>
                      </table>
                    </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information                           //-->
<!-- ------------------------------------------------------------ //-->
                    <div class="tab-pane" id="tab3">
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td align="left" class="mainTitle"><?php echo TITLE_IMAGINIS_INFORMATION; ?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
<?php
   $twitter_authentificate_clicshopping = osc_twitter_authentificate_clicshopping();
// enables caching (path must exists and must be writable!)
   Twitter::$cacheDir = DIR_FS_ADMIN . 'cache/twitter';
//  $channel = $twitter->load($withFriends);

  $statuses = $twitter_authentificate_clicshopping->load(Twitter::ME);

  foreach ($statuses as $status) {
?>
                        <tr>
                          <td><a href="http://twitter.com/<?php echo $status->user->screen_name ?>"><img border ="0"  src="<?php echo htmlspecialchars($status->user->profile_image_url) ?>"></a></td>
                          <td class="mainTable" valign="top">
                            <p class="main"><?php echo date("j/n/Y H:i", strtotime($status->created_at)); ?></p>
                            <span style="font-size=3"><a href="http://twitter.com/<?php echo $status->user->screen_name; ?>" target="_blank"><?php echo htmlspecialchars($status->user->name);?></a></span> :
                            <?php echo  Twitter::clickable($status); ?>
                          </td>
                        </tr>
<?php
  }
?>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div style="text-align:center"><?php echo LINKEDIN_CERTIFICATE_CLICSHOPPING; ?><a href="https://www.linkedin.com/profile/add?_ed=0_7nTFLiuDkkQkdELSpruCwGpUMNDIZlXEx27EQU-qViHbBgqjFXJtPnC2wRYrmwrBaSgvthvZk7wTBMS3S-m0L6A6mLjErM6PJiwMkk6nYZylU7__75hCVwJdOTZCAkdv&pfCertificationName=ClicShopping%20professionnal&pfCertificationUrl=http%3A%2F%2Fwww.clicshopping.org&trk=onsite_html" rel="nofollow" target="_blank"><img src="https://download.linkedin.com/desktop/add2profile/buttons/en_US.png" alt="LinkedIn Add to Profile button"></a></div>
            </td>
          </tr>
        </table></td>
      </tr>
  </table>
<!-- footer //-->
<?php
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
