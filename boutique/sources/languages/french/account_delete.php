<?php
/**
 *
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */
define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Suppression du compte');
define('HEADING_TITLE', 'Suppression du compte');
define('TEXT_MODULE_ACCOUNT_CUSTOMERS_DELETE_ACCOUNT_INTRO','En confirmant cette action, votre compte client sera définitivement supprimé');
define('TEXT_MODULE_ACCOUNT_CUSTOMERS_DELETE_ACCOUNT_CHECKBOX', 'Confirmation de votre suppression de compte : ');
define('EMAIL_TEXT_SUBJECT','Suppression de compte sur '.STORE_NAME);
define('EMAIL_TEXT_MESSAGE','Nous vous confirmons que votre demande a bien &eactue;té effectuée sur '.STORE_NAME. '<br /><br />Nous espérons vous revoir bientot.<br />Cordialement, l\Eacute;quipe '. STORE_NAME);
?>