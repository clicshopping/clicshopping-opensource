<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Reviews');
define('HEADING_TITLE', 'Customers reviews concerning this product');
define('TEXT_OF_5_STARS', '%s of 5 Stars!');
define('REVIEWS_TEXT_READ_MORE','See the comment');
?>