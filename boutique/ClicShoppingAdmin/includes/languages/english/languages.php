<?php
/*
 * languages.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Languages');

define('TABLE_HEADING_LANGUAGE_NAME', 'Language');
define('TABLE_HEADING_LANGUAGE_CODE', 'Code');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_LANGUAGE_STATUS','On / Off');
define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_COMMON_CURRENCIES', '-- Common Currencies --');
define('TEXT_INFO_LANGUAGE_NAME', 'Name:');
define('TEXT_INFO_LANGUAGE_CODE', 'Code:');
define('TEXT_INFO_LANGUAGE_IMAGE', 'Image:');
define('TEXT_INFO_LANGUAGE_DIRECTORY', 'Directory:');
define('TEXT_INFO_LANGUAGE_SORT_ORDER', 'Sort Order:');
define('TEXT_INFO_INSERT_INTRO', 'Please enter the new language with its related data');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this language?<br /><br /><strong><font color="#FF0000">Note :</font></strong>If you remove this language, it will remove all associated records');
define('TEXT_INFO_HEADING_NEW_LANGUAGE', 'New Language');
define('TEXT_INFO_HEADING_EDIT_LANGUAGE', 'Edit Language');
define('TEXT_INFO_HEADING_DELETE_LANGUAGE', 'Delete Language');
define('TEXT_INFO_CREATE_LANGUAGE','Do you wish create the directories  ?');
define('TEXT_CREATE_LANGUAGE','Directories creation confirmation');
define('TEXT_NOTE_CREATE_LANGUAGE','<br /><strong>Notes :</strong><br /> The directories creation will be the default language directory. <br /><strong>Depends of the servers caracteristics, this operation can not work fine.</strong> Please, realise this operation manually.<br /><br />The creation directories and files are : <br />- In administration<br />- In catalog languages default directory<br /><br />- In catalog template default directory.<br />- In graphism template default directory.<br /><br />Please connect you on the market place to upload  your new language  and to install il with the module installaton.');

define('ERROR_REMOVE_DEFAULT_LANGUAGE', 'Error: The default language can not be removed. Please set another language as default, and try again.');
?>
