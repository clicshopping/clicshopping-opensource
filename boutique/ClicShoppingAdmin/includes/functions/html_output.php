<?php
/*
 * html_output.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

/**
 * Generate an internal URL address for the administration side
 *
 * @param string $page The page to link to
 * @param string $parameters The parameters to pass to the page (in the GET scope)
 * @access public
 */
////
// The HTML href link wrapper function
  function osc_href_link($page = '', $parameters = '', $connection = 'SSL', $add_session_id = true) {
    global $request_type, $SID;

    $page = osc_output_string($page);

    if ($page == '') {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><strong>Error!</strong></font><br /><br /><strong>Unable to determine the page link!<br /><br />Function used:<br /><br />osc_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</strong>');
    }

    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_ADMIN;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == 'true') {
        $link = HTTPS_SERVER . DIR_WS_HTTPS_ADMIN;
      } else {
        $link = HTTP_SERVER . DIR_WS_ADMIN;
      }
    } else {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><strong>Error!</strong></font><br /><br /><strong>Unable to determine connection method on a link!<br /><br />Known methods: NONSSL SSL<br /><br />Function used:<br /><br />osc_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</strong>');
    }

    if (osc_not_null($parameters)) {
      $link .= $page . '?' . osc_output_string($parameters);
      $separator = '&';
    } else {
      $link .= $page;
      $separator = '?';
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

 // Add the session ID when moving from different HTTP and HTTPS servers, or when SID is defined
     if ( ($add_session_id == true) && (SESSION_FORCE_COOKIE_USE == 'False') ) {
       if (isset($SID) && osc_not_null($SID)) {
         $_sid = $SID;
       } elseif ( ( ($request_type == 'NONSSL') && ($connection == 'SSL') && (ENABLE_SSL == true) ) || ( ($request_type == 'SSL') && ($connection == 'NONSSL') ) ) {
         if (HTTP_COOKIE_DOMAIN != HTTPS_COOKIE_DOMAIN) {
           $_sid = session_name() . '=' . session_id();
         }
       }
     }

    if (isset($_sid)) {
       $link .= $separator . osc_output_string($_sid);
     }

    while (strstr($link, '&&')) $link = str_replace('&&', '&', $link);

    return $link;
  }

  function osc_catalog_href_link($page = '', $parameters = '', $connection = 'NONSSL') {
    if ($connection == 'NONSSL') {
      $link = HTTP_CATALOG_SERVER . DIR_WS_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL_CATALOG == 'true') {
        $link = HTTPS_CATALOG_SERVER . (defined('DIR_WS_HTTPS_CATALOG') ? DIR_WS_HTTPS_CATALOG : DIR_WS_CATALOG);
      } else {
        $link = HTTP_CATALOG_SERVER . DIR_WS_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><strong>Error!</strong></font><br /><br /><strong>Unable to determine connection method on a link!<br /><br />Known methods: NONSSL SSL<br /><br />Function used:<br /><br />osc_href_link(\'' . $page . '\', \'' . $parameters . '\', \'' . $connection . '\')</strong>');
    }
    if ($parameters == '') {
      $link .= $page;
    } else {
      $link .= $page . '?' . $parameters;
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

    return $link;
  }

////
// The HTML image wrapper function
/**
 * Outputs an image
 *
 * @param string $image The image filename to display
 * @param string $src The path of the image
 * @param string $title The title of the image button
 * @param int $width The width of the image
 * @param int $height The height of the image
 * @param string $parameters Additional parameters for the image
 * @access public
 */
  function osc_image($src, $alt = '', $width = '', $height = '', $parameters = '') {

// the image filename as default
    $image = '<img src="' . osc_output_string($src) . '" border="0" alt="' . osc_output_string($alt) . '"';

    if (osc_not_null($alt)) {
      $image .= ' title="' . osc_output_string($alt) . '"';
    }


    if (osc_not_null($width) || osc_not_null($height)) {
      $image .= ' width="' . osc_output_string($width) . '" height="' . osc_output_string($height) . '"';
    }

    if (osc_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= ' />';

    return $image;
  }

/**
 * Outputs an image submit button
 *
 * @param string $image The image filename to display
 * @param string $title The title of the image button
 * @param string $parameters Additional parameters for the image submit button
 * @access public
 */

////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
  function osc_image_submit($image, $alt = '', $parameters = '') {
    global $language;

    $image_submit = '<input type="image" src="' . osc_output_string(DIR_WS_LANGUAGES . $_SESSION['language'] . '/images/buttons/' . $image) . '" border="0" alt="' . osc_output_string($alt) . '"';

    if (osc_not_null($alt)) $image_submit .= ' title=" ' . osc_output_string($alt) . ' "';

    if (osc_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= ' />';

    return $image_submit;
  }

////
// Draw a 1 pixel black line
  function osc_black_line() {
    return osc_image(DIR_WS_IMAGES . 'pixel_black.gif', '', '100%', '1');
  }

////
// Output a separator either through whitespace, or with an image
  function osc_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return osc_image(DIR_WS_IMAGES . $image, '', $width, $height);
  }

/**
 * Outputs an image button
 *
 * @param string $image The image filename to display
 * @param string $title The title of the image button
 * @param string $parameters Additional parameters for the image button
 * @access public
 */
////
// Output a function button in the selected language
  function osc_image_button($image, $alt = '', $params = '') {

    return osc_image(DIR_WS_LANGUAGES . $_SESSION['language'] . '/images/buttons/' . $image, $alt, '', '', $params);
  }


// Fonction permettant d'avoir une image survole d'un bouton
  function osc_button_over($image, $imageover, $width = '', $height = '', $alt = '', $params = '', $page = '', $parameters = '', $connection = 'NONSSL') {
    global $language;

    $button_over = '<a href="' . osc_href_link($page, $parameters, $connection) . '" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage(\'' . $params . '\',\'\',\'includes/languages/' . $language . '/images/buttons/' . $imageover . '\',1)">' . osc_image_button($image, $alt, 'name="' . $params . '" width="' . $width . '" height="' . $height . '"') . '</a>';

    return $button_over;
  }

// Fonction permettant d'avoir une image survole d'un bouton
  function osc_button_over_css($image, $imageover, $width = '', $height = '', $alt = '', $params = '', $page = '', $parameters = '', $connection = 'NONSSL') {
    global $language;

// L'effet de survol sans permutation d'image (sans besoin de prechargement d'image)
    $button_over_css = "<style>";
    $button_over_css .= "a." . $params . " { display: block; width: " . $width . "px; height: " . $height . "px; background-image: url(includes/languages/" . $language . "/images/buttons/" . $imageover . ") }";
    $button_over_css .= "a." . $params . ":hover { visibility: visible }";
    $button_over_css .= "a." . $params . ":hover img { visibility: hidden }";
    $button_over_css .= "</style>";
    $button_over_css .= "<a class='" . $params . "' href='" . osc_href_link($page, $parameters,$connection) . "' title='" . $alt . "' alt='" . $alt . "'><img border='0' src='includes/languages/" . $language . "/images/buttons/" . $image . "' width='" . $width . "px' height='" . $height . "px'></a>";

    return $button_over_css;
  }

////
// javascript to dynamically update the states/provinces list when the country is changed
// TABLES: zones
  function osc_js_zone_list($country, $form, $field) {
    global $OSCOM_PDO;
    
    $Qcountries = $OSCOM_PDO->prepare('select distinct zone_country_id
                                       from :table_zones
                                       order by zone_country_id
                                      ');
    $Qcountries->execute();


    $num_country = 1;
    $output_string = '';

    while ($countries = $Qcountries-fetch() ) {
      if ($num_country == 1) {
        $output_string .= '  if (' . $country . ' == "' . $countries['zone_country_id'] . '") {' . "\n";
      } else {
        $output_string .= '  } else if (' . $country . ' == "' . $countries['zone_country_id'] . '") {' . "\n";
      }

      $Qzone = $OSCOM_PDO->prepare('select zone_name,
                                          zone_id
                                    from :table_zones
                                    where  zone_country_id = :zone_country_id
                                    order by zone_name
                                  ');
      $Qzone->bindInt(':zone_country_id', (int)$countries['zone_country_id']);

      $Qzone->execute();

      $num_state = 1;
      while ($states = $Qzone->fetch() ) {
        if ($num_state == '1') $output_string .= '    ' . $form . '.' . $field . '.options[0] = new Option("' . PLEASE_SELECT . '", "");' . "\n";
        $output_string .= '    ' . $form . '.' . $field . '.options[' . $num_state . '] = new Option("' . $states['zone_name'] . '", "' . $states['zone_id'] . '");' . "\n";
        $num_state++;
      }
      $num_country++;
    }
    $output_string .= '  } else {' . "\n" .
                      '    ' . $form . '.' . $field . '.options[0] = new Option("' . TYPE_BELOW . '", "");' . "\n" .
                      '  }' . "\n";

    return $output_string;
  }



/**
 * Creates a pull-down list of countries
 *
 * @param string $name The name of the hidden field
 * @param string $selected The value for the hidden field
 * @param string $parameters Additional parameters for the hidden field
 * @access public
 */

  function osc_get_country_list($name, $selected = '', $parameters = '') {
    $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
    $countries = osc_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_iso_code_2'], 'text' => $countries[$i]['countries_name']);
    }

    return osc_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }


////
// Output a form
  function osc_draw_form($name, $action, $parameters = '', $method = 'post', $params = '') {
    $form = '<form name="' . osc_output_string($name) . '" action="';
    if (osc_not_null($parameters)) {
      $form .= osc_href_link($action, $parameters);
    } else {
      $form .= osc_href_link($action);
    }
    $form .= '" method="' . osc_output_string($method) . '"';
    if (osc_not_null($params)) {
      $form .= ' ' . $params;
    }
    $form .= '>';

    return $form;
  }

/**
 * Outputs a form password field
 *
 * @param string $name The name and ID of the password field
 * @param string $parameters Additional parameters for the password field
 * @access public
 */
////
// Output a form input field
  function osc_draw_input_field($name, $value = '', $parameters = '', $required = false, $type = 'text', $reinsert_value = true, $class = 'form-control') {

    $field = '<input type="' . osc_output_string($type) . '" name="' . osc_output_string($name) . '"';

    if ( ($reinsert_value == true) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $value = $_GET[$name];
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $value = $_POST[$name];
      }
    }

    if (osc_not_null($value)) {
      $field .= ' value="' . osc_output_string($value) . '"';
    }

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    if (osc_not_null($class)) $field .= ' class="' . $class . '"';


    $field .= ' />';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

////
// Output a form password field
  function osc_draw_password_field($name, $value = '', $parameters='', $required = false) {

    if (osc_not_null($parameters)) {
      $parameters = $parameters . ' maxlength="40"';
    } else {
      $parameters = 'maxlength="40"';
    }

    $field = osc_draw_input_field($name, $value,  $parameters, $required, 'password', false);

    return $field;
  }

////
// Output a form file field
  function osc_draw_file_field($name, $required = false) {
    $field = osc_draw_input_field($name, '', '', $required, 'file');

    return $field;
  }

/**
 * Outputs a form selection field (checkbox/radio)
 *
 * @param string $name The name and indexed ID of the selection field
 * @param string $type The type of the selection field (checkbox/radio)
 * @param mixed $values The value of, or an array of values for, the selection field
 * @param string $default The default value for the selection field
 * @param string $parameters Additional parameters for the selection field
 * @param string $separator The separator to use between multiple options for the selection field
 * @access public
 */
////
// Output a selection field - alias function for osc_draw_checkbox_field() and osc_draw_radio_field()
  function osc_draw_selection_field($name, $type, $value = '', $checked = false, $compare = '') {

    $selection = '<input type="' . osc_output_string($type) . '" name="' . osc_output_string($name) . '"';

    if (osc_not_null($value)) $selection .= ' value="' . osc_output_string($value) . '"';

    if ( ($checked == true) || (isset($_GET[$name]) && is_string($_GET[$name]) && (($_GET[$name] == 'on') || ($_GET[$name] == $value))) || (isset($_POST[$name]) && is_string($_POST[$name]) && (($_POST[$name] == 'on') || ($_POST[$name] == $value))) || (osc_not_null($compare) && ($value == $compare)) ) {
      $selection .= ' checked="checked"';
    }

    $selection .= ' />';

    return $selection;
  }

/**
 * Outputs a form checkbox field
 *
 * @param string $name The name and indexed ID of the checkbox field
 * @param mixed $values The value of, or an array of values for, the checkbox field
 * @param string $default The default value for the checkbox field
 * @param string $parameters Additional parameters for the checkbox field
 * @param string $separator The separator to use between multiple options for the checkbox field
 * @access public
 */
////
// Output a form checkbox field
  function osc_draw_checkbox_field($name, $value = '', $checked = false, $compare = '') {
    return osc_draw_selection_field($name, 'checkbox', $value, $checked, $compare);
  }

////
// Bouton radio
// Output a form radio field
  function osc_draw_radio_field($name, $value = '', $checked = false, $compare = '') {
    return osc_draw_selection_field($name, 'radio', $value, $checked, $compare);
  }

/**
 * Outputs a form textarea field
 * The $wrap parameter is no longer used in the core xhtml template
 * @param string $name The name and ID of the textarea field
 * @param string $value The default value for the textarea field
 * @param int $width The width of the textarea field
 * @param int $height The height of the textarea field
 * @param string $parameters Additional parameters for the textarea field
 * @param boolean $override Override the default value with the value found in the GET or POST scope
 * @access public
 */
////
// Output a form textarea field
  function osc_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true, $class = 'form-control') {

    $field = '<textarea class="form-control" name="' . osc_output_string($name) . '" cols="' . osc_output_string($width) . '" rows="' . osc_output_string($height) . '"';

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    if (osc_not_null($class)) $field .= ' class="' . $class . '"';

    $field .= '>';

    if ( ($reinsert_value == true) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $field .= osc_output_string_protected($_GET[$name]);
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $field .= osc_output_string_protected($_POST[$name]);
      }
    } elseif (osc_not_null($text)) {
      $field .= osc_output_string_protected($text);
    }

    $field .= '</textarea>';

    return $field;
  }


/**
 * Outputs a form hidden field
 *
 * @param string $name The name of the hidden field
 * @param string $value The value for the hidden field
 * @param string $parameters Additional parameters for the hidden field
 * @access public
 */
  function osc_draw_hidden_field($name, $value = '', $parameters = '') {

    $field = '<input type="hidden" name="' . osc_output_string($name) . '"';

    if (osc_not_null($value)) {
      $field .= ' value="' . osc_output_string($value) . '"';
    } elseif ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) {
      if ( (isset($_GET[$name]) && is_string($_GET[$name])) ) {
        $field .= ' value="' . osc_output_string($_GET[$name]) . '"';
      } elseif ( (isset($_POST[$name]) && is_string($_POST[$name])) ) {
        $field .= ' value="' . osc_output_string($_POST[$name]) . '"';
      }
    }

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= ' />';

    return $field;
  }

////
// Hide form elements
  function osc_hide_session_id() {
    $string = '';

    if (defined('SID') && osc_not_null(SID)) {
      $string = osc_draw_hidden_field(session_name(), session_id());
    }

    return $string;
  }

/**
 * Outputs a form pull down menu for a date selection
 *
 * @param string $name The base name of the date pull down menu fields
 * @param array $value An array containing the year, month, and date values for the default date (year, month, date)
 * @param boolean $default_today Default to todays date if no default value is used
 * @param boolean $show_days Show the days in a pull down menu
 * @param boolean $use_month_names Show the month names in the month pull down menu
 * @param int $year_range_start The start of the years range to use for the year pull down menu
 * @param int $year_range_end The end of the years range to use for the year pull down menu
 * @access public
 */
////
// Output a form pull down menu
// Menu deroulant

  function osc_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {

    $field = '<select name="' . osc_output_string($name) . '"';

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

   if (empty($default) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $default = $_GET[$name];
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $default = $_POST[$name];
      }
    }

    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . osc_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
       $field .= ' selected="selected"';
    }

      $field .= '>' . osc_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }


/**
 * Pulldown products
 * 
 * @param string $name, $parameters, $exclude
 * @return string $select_string, the pulldown value of products
 * @access public
 */
  function osc_draw_products_pull_down($name, $parameters = '', $exclude = '', $class = 'form-control') {
    global $currencies, $OSCOM_PDO;

    if ($exclude == '') {
      $exclude = array();
    }

    $select_string = '<select name="' . $name . '"';

    if ($parameters) {
      $select_string .= ' ' . $parameters;
    }

    if (osc_not_null($class)) $field .= ' class="' . $class . '"';

    $select_string .= ' />';

    $all_groups = array();

    $QcustomersGroups = $OSCOM_PDO->prepare("select customers_group_name,
                                                   customers_group_id
                                            from :table_customers_groups
                                            order by customers_group_id
                                          ");
    $QcustomersGroups->execute();

    while ($existing_groups =  $QcustomersGroups->fetch()) {
      $all_groups[$existing_groups['customers_group_id']]=$existing_groups['customers_group_name'];
    }

    $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                             pd.products_name,
                                             p.products_price
                                      from :table_products p,
                                           :table_products_description pd
                                      where p.products_id = pd.products_id
                                      and pd.language_id = :language_id
                                      and p.products_archive = 0
                                      order by products_name
                                     ');
    $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
    $Qproducts->execute();

    while ($products = $Qproducts->fetch() ) {

// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {
    //B2BSuite modification
    if (!in_array($products['products_id'], $exclude)) {

      $Qprice = $OSCOM_PDO->prepare('select customers_group_price,
                                            customers_group_id
                                    from :table_products_groups
                                    where products_id = :products_id
                                   ');
      $Qprice->bindInt(':products_id', (int)$products['products_id']);
      $Qprice->execute();

        $product_prices=array();

        while($prices_array = $Qprice->fetch() ) {
          $product_prices[$prices_array['customers_group_id']] = $prices_array['customers_group_price'];
        }
        reset($all_groups);
        $price_string="";
        $sde=0;
        while(list($sdek,$sdev)=each($all_groups)){
          if (!in_array((int)$products['products_id'].":".(int)$sdek, $exclude)) {
            if($sde)
              $price_string.=" - ";
              $price_string.=$sdev.": ".$currencies->format(isset($product_prices[$sdek]) ? $product_prices[$sdek]:$products['products_price']);
              $sde=1;
          }
        }
        // Ajouter VISITOR_NAME . ': ' . $currencies->format($products['products_price']) pour permettre d'afficher le prix des clients qui ne font pas partie d'un groupe B2B
        $select_string .= '<option value="' . $products['products_id'] . '">' . $products['products_name'] . ' (' . VISITOR_NAME . ': ' . $currencies->format($products['products_price']) . ' - ' . $price_string . ')</option>\n';
      }
    } else {
       if (!in_array($products['products_id'], $exclude)) {
        $select_string .= '<option value="' . $products['products_id'] . '">' . $products['products_name'] . ' (' . $currencies->format($products['products_price']) . ')</option>';
      }
    }

// ####### END  #######
    }

    $select_string .= '</select>';

    return $select_string;
  }


////
// Output a jQuery UI Button
  function osc_draw_button($title = null, $icon = null, $link = null, $priority = null, $params = null) {
    static $button_counter = 1;

    $types = array('submit', 'button', 'reset');

    if ( !isset($params['type']) ) {
      $params['type'] = 'submit';
    }

    if ( !in_array($params['type'], $types) ) {
      $params['type'] = 'submit';
    }

    if ( ($params['type'] == 'submit') && isset($link) ) {
      $params['type'] = 'button';
    }

    if (!isset($priority)) {
      $priority = 'secondary';
    }

    $button = '<span class="tdbLink">';

    if ( ($params['type'] == 'button') && isset($link) ) {
      $button .= '<a id="tdb' . $button_counter . '" href="' . $link . '"';

      if ( isset($params['newwindow']) ) {
        $button .= ' target="_blank"';
      }
    } else {
      $button .= '<button id="tdb' . $button_counter . '" type="' . osc_output_string($params['type']) . '"';
    }

    if ( isset($params['params']) ) {
      $button .= ' ' . $params['params'];
    }

    $button .= '>' . $title;

    if ( ($params['type'] == 'button') && isset($link) ) {
      $button .= '</a>';
    } else {
      $button .= '</button>';
    }

    $button .= '</span><script type="text/javascript">$("#tdb' . $button_counter . '").button(';

    $args = array();

    if ( isset($icon) ) {
      if ( !isset($params['iconpos']) ) {
        $params['iconpos'] = 'left';
      }

      if ( $params['iconpos'] == 'left' ) {
        $args[] = 'icons:{primary:"ui-icon-' . $icon . '"}';
      } else {
        $args[] = 'icons:{secondary:"ui-icon-' . $icon . '"}';
      }
    }

    if (empty($title)) {
      $args[] = 'text:false';
    }

    if (!empty($args)) {
      $button .= '{' . implode(',', $args) . '}';
    }

    $button .= ').addClass("ui-priority-' . $priority . '").parent().removeClass("tdbLink");</script>';

    $button_counter++;

    return $button;
  }

// review stars
  function osc_draw_stars($rating = 0, $empty = true) {
    $stars = str_repeat('<span class="glyphicon glyphicon-star"></span>', (int)$rating);
    if ($empty === true) $stars .= str_repeat('<span class="glyphicon glyphicon-star-empty"></span>', 5-(int)$rating);

    return $stars;
  }
?>