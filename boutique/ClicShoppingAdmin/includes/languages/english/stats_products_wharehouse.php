<?php
/**
 * stats_products_wharehouse.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Order whare management');

define('TABLE_HEADING_MODEL', 'product model');
define('TABLE_HEADING_QTY_LEFT', 'Stock quantity');
define('TABLE_HEADING_CUSTOMERS_ORDERS_QUANTITY','Order quantity');
define('TABLE_HEADING_ACTION','Action');
define('TABLE_HEADING_WAHREHOUSE_TIME_REPLENISHMENT','Resupplying');
define('TABLE_HEADING_WHAREHOUSE','Wharehouse name');
define('TABLE_HEADING_WHAREHOUSE_ROW','Wharehouse row');
define('TABLE_HEADING_WHAREHOUSE_LEVEL','Location level');
define('TABLE_HEADING_CUSTOMERS_ID','No customers');
define('TABLE_HEADING_CUSTOMERS_NAME','Customers name');
define('TABLE_HEADING_CUSTOMERS_PHONE','phones');
define('TABLE_HEADING_ORDER_ID','Order id');
define('TABLE_HEADING_PRODUCTS_NAME','Product name');
define('TABLE_HEADING_PACKAGING','Packaging');
define('TITLE_STATUS','Order status');
define('TEXT_ALL_ORDERS','All orders');
define('TEXT_ACTION','Action');
define('TEXT_PRODUCTS_PACKAGING_NEW','New product');
define('TEXT_PRODUCTS_PACKAGING_REPACKAGED','Product repackaged');
define('TEXT_PRODUCTS_PACKAGING_USED','Product Used');
?>