<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Mes garanties');

define('TEXT_SECURE_PAYMENT', 'Paiement sécurisé SSL Certifié');
define('TEXT_FIRST_VISIT', 'Première visite !');
define('TEXT_SHIPPING', 'Livraison rapide');

define('TEXT_RETURN_PRODUCT', 'Retour Marchandise 7j');
define('TEXT_SATISFY_CUSTOMER', 'Satisfaction ou Remboursé');

?>
