<?php
/**
 * email.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require_once (DIR_WS_CLASSES . '/phpmailer/class.phpmailer.php');

  $phpMail = new PHPMailer();

  class email {
    var $html;
    var $text;
    var $html_text;
    var $lf;
    var $debug = 0;
    var $debug_output = 'error_log';

    function email($headers = '') {
      global $phpMail;

      $phpMail->XMailer = 'ClicShopping';
      $phpMail->SMTPDebug = $this->debug;
      $phpMail->Debugoutput = $this->debug_output;
      $phpMail->CharSet = CHARSET;
      $phpMail->WordWrap = 998;

      if (EMAIL_TRANSPORT == 'smtp' || EMAIL_TRANSPORT == 'gmail') {
        $phpMail->IsSMTP();

        $phpMail->Port = EMAIL_SMTP_PORT;
        if (EMAIL_SMTP_SECURE !== 'no') {
          $phpMail->SMTPSecure = EMAIL_SMTP_SECURE;
        }

        $phpMail->Host = EMAIL_SMTP_HOSTS;
        $phpMail->SMTPAuth = EMAIL_SMTP_AUTHENTICATION;

        $phpMail->Username = EMAIL_SMTP_USER;
        $phpMail->Password = EMAIL_SMTP_PASSWORD;

      } else {
        $phpMail->isSendmail();
      }

      if (EMAIL_LINEFEED == 'CRLF') {
        $this->lf = "\r\n";
      } else {
        $this->lf = "\n";
      }
    }

    function add_text($text = '') {
      global $phpMail;

      $phpMail->IsHTML(false);
      $this->text = osc_convert_linefeeds(array("\r\n", "\n", "\r"), $this->lf, $text);
    }

/**
 * Adds a html part to the mail.
 * Also replaces image names with
 * content-id's.
 */

    function add_html($html, $text = NULL, $images_dir = NULL) {
      global $phpMail;

      $phpMail->IsHTML(true);
      $this->html = osc_convert_linefeeds(array("\r\n", "\n", "\r"), '<br />', $html);
      $this->html_text = osc_convert_linefeeds(array("\r\n", "\n", "\r"), $this->lf, $text);

      if (isset($images_dir)) $this->html = $phpMail->msgHTML($this->html, $images_dir);
    }

/**
 * Adds a html part to the mail.
 * Also replaces image names with
 * content-id's.
 */

// FCKeditor 
    function add_html_fckeditor($html, $text = NULL, $images_dir = NULL) {
      global $phpMail;

      $phpMail->IsHTML(true);

      $this->html = osc_convert_linefeeds(array("\r\n", "\n", "\r"), '', $html);
      $this->html_text = osc_convert_linefeeds(array("\r\n", "\n", "\r"), $this->lf, $text);

      if (isset($images_dir)) $this->html = $phpMail->msgHTML($this->html, $images_dir);
    }


    function add_attachment($path, $name = '', $encoding = 'base64', $type = '', $disposition = 'attachment') {
      global $phpMail;

      $phpMail->AddAttachment($path, $name, $encoding, $type, $disposition);
    }

    function build_message() {
      //out of work function
    }

/**
 * Sends the mail.
 */

    function send($to_name, $to_addr, $from_name, $from_addr, $subject = '', $reply_to = false) {
      global $phpMail;

      if ((strstr($to_name, "\n") != false) || (strstr($to_name, "\r") != false)) {
        return false;
      }

      if ((strstr($to_addr, "\n") != false) || (strstr($to_addr, "\r") != false)) {
        return false;
      }

      if ((strstr($subject, "\n") != false) || (strstr($subject, "\r") != false)) {
        return false;
      }

      if ((strstr($from_name, "\n") != false) || (strstr($from_name, "\r") != false)) {
        return false;
      }

      if ((strstr($from_addr, "\n") != false) || (strstr($from_addr, "\r") != false)) {
        return false;
      }

      $phpMail->From = $from_addr;
      $phpMail->FromName = $from_name;
      $phpMail->AddAddress($to_addr, $to_name);

      if ($reply_to) {
        $phpMail->AddReplyTo(EMAIL_SMTP_REPLYTO, STORE_NAME);
      } else {
        $phpMail->AddReplyTo($from_addr, $from_name);
      }

      $phpMail->Subject = $subject;

      if (!empty($this->html)) {
        $phpMail->Body = $this->html;
        $phpMail->AltBody = $this->html_text;
      } else {
        $phpMail->Body = $this->text;
      }

      if (EMAIL_TRANSPORT == 'smtp' || EMAIL_TRANSPORT == 'gmail') {
        $phpMail->IsSMTP();

        $phpMail->Port = EMAIL_SMTP_PORT;
        if (EMAIL_SMTP_SECURE !== 'no') {
          $phpMail->SMTPSecure = EMAIL_SMTP_SECURE;
        }

        $phpMail->Host = EMAIL_SMTP_HOSTS;
        $phpMail->SMTPAuth = EMAIL_SMTP_AUTHENTICATION;

        $phpMail->Username = EMAIL_SMTP_USER;
        $phpMail->Password = EMAIL_SMTP_PASSWORD;

      } else {
        $phpMail->isSendmail();
      }

      $error = false;
      if (!$phpMail->Send()) {
        $error = true;
      }

      $phpMail->clearAddresses();
      $phpMail->clearAttachments();

      if ($error == true) {
        return false;
      }

      return true;
    }
  }
