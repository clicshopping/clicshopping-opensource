<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Create an Account');

define('HEADING_TITLE', 'My Account Information');

define('EMAIL_SUBJECT', 'Ask for opening of clients\' account on ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");

define('ADMIN_EMAIL_WELCOME', 'Hi'. "\n\n" . '');
define('ADMIN_EMAIL_TEXT', 'New client (B2C mode) is registered, below user details:<br />
Name :' . $_POST['lastname'] . '<br />
Surname :' . $_POST['firstname'] . '<br />
Company :' . $_POST['company'] . '<br />
E-Mail :' . $_POST['email_address'] . '<br />
');

define('EMAIL_NETWORK','Follow us in real time on  :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');
?>
