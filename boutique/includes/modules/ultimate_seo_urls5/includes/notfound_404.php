<?php
  /**
  *
  * ULTIMATE Seo Urls 5 PRO ( version 1.1 )
  *
  * 
  * @package USU5_PRO
  * @license http://www.opensource.org/licenses/gpl-2.0.php GNU Public License
  * @link http://www.fwrmedia.co.uk
  * @copyright Copyright 2008-2009 FWR Media
  * @copyright Portions Copyright 2005 ( rewrite uri concept ) Bobby Easland
  * @author Robert Fisher, FWR Media, http://www.fwrmedia.co.uk 
  * @lastdev $Author:: Rob                                              $:  Author of last commit
  * @lastmod $Date:: 2010-12-21 22:45:02 +0000 (Tue, 21 Dec 2010)       $:  Date of last commit
  * @version $Rev:: 196                                                 $:  Revision of last commit
  * @Id $Id:: notfound_404.php 196 2010-12-21 22:45:02Z Rob             $:  Full Details   
  */
  
  /**
  * Page not found html with 404 header
  * @package USU5_PRO
  * 
  * @var array $text - array of text strings to be used in the html
  */
  $text = array( 'title' => 'Page not found',
                 'text' => 'The page you were looking for could not be found. Please click the below link to return to ' . STORE_NAME . '
                            <p><a href="' . osc_href_link( 'index.php' ) . '" title="' . STORE_NAME . '">' . STORE_NAME . '</a></p><br />' );
  header( "HTTP/1.0 404 Not Found" );
?>
<title>Page Not Found</title>  
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
    <td>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="70" align="left" valign="top"></td>
        <td valign="top" width="100%"><p align="center"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'] .'/boutique'; ?>"><img src="../boutique/sources/image/logos/invoice/invoice_logo.jpg"  border="0"></a></td>
        <td height="70" align="right" valign="top"></td>
      </tr>
    </table>
  </td>
  </tr>
  <tr>
    <td width="100%" height="290" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><p>&nbsp;</p><p>&nbsp;</p>
          <p><img src="<?php echo 'http://'.$_SERVER['HTTP_HOST'] ; ?>/erreur/images/404.gif" width="644" height="100"></p>
          <br />
        </td>
      </tr>
      <tr>
        <td>
<script type="text/javascript">
  var GOOG_FIXURL_LANG = 'fr';
  var GOOG_FIXURL_SITE = 'http://'.$_SERVER['HTTP_HOST'] .'/boutique/';
</script>
<script type="text/javascript" src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js"></script>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="100%" height="33" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" valign="top"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'] ; ?>"><img src="<?php echo 'http://'.$_SERVER['HTTP_HOST'] ; ?>/erreur/images/button_continue.png" width="93" height="20" border="0"></a></td>
      </tr>
    </table></td>
  </tr>
</table>

</table>