<?php
/**
 * reviews.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Comments');

define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_RATING', 'Rating');
define('TABLE_HEADING_DATE_ADDED', 'Date Added');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_REVIEW_AUTHOR','Author');						
define('TABLE_HEADING_PRODUCTS_AVERAGE_RATING','Average');
define('TABLE_HEADING_REVIEW_READ','Read');					
define('TABLE_HEADING_LAST_MODIFIED','Modified date');
define('TABLE_HEADING_APPROVED', 'Approved ?');								
define('ICON_PREVIEW_COMMENT','Comment preview');								
define('ENTRY_PRODUCT', 'Product name:');
define('ENTRY_FROM', 'Name of the customer:');
define('ENTRY_DATE', 'Date from the opinion:');
define('ENTRY_REVIEW', 'Opinion of the customer:');
define('ENTRY_REVIEW_TEXT', '<small><font color="#ff0000"><strong>NOTE:</strong></font></small>&nbsp;HTML is not translated!&nbsp;');
define('ENTRY_RATING', 'Rating:');
define('ENTRY_STATUS', 'Status :');
define('ENTRY_STATUS_YES', 'Accept the comment');
define('ENTRY_STATUS_NO', 'Refuse the comment');
define('ENTRY_ACTUAL_STATUS', '<br />Actual Status : ');
define('ENTRY_STATUS_ACTUAL_YES', 'Comment accepted');
define('ENTRY_STATUS_ACTUAL_NO', 'Comment refused');

define('TEXT_INFO_DELETE_REVIEW_INTRO', 'Are you sure you want to delete this review?');

define('TEXT_INFO_DATE_ADDED', 'Date Added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');
define('TEXT_INFO_REVIEW_AUTHOR', 'Author:');
define('TEXT_INFO_REVIEW_RATING', 'Rating:');
define('TEXT_INFO_REVIEW_READ', 'Read:');
define('TEXT_INFO_REVIEW_SIZE', 'Size:');
define('TEXT_INFO_PRODUCTS_AVERAGE_RATING', 'Average Rating:');

define('TEXT_OF_5_STARS', '%s of 5 Stars!');
define('TEXT_GOOD', '<small><font color="#ff0000"><strong>GOOD</strong></font></small>');
define('TEXT_BAD', '<small><font color="#ff0000"><strong>BAD</strong></font></small>');
define('TEXT_INFO_HEADING_DELETE_REVIEW', 'Delete Review');

define('TITLE_REVIEWS_GENERAL', 'Information general');
define('TITLE_REVIEWS_ENTRY', 'Opinion of the customer');
define('TITLE_REVIEWS_RATING', 'Rating');
?>