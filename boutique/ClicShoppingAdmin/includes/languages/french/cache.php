<?php
/**
 * cache.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Contr&ocirc;le du cache');

define('TABLE_HEADING_CACHE', 'Cache des blocs');
define('TABLE_HEADING_DATE_CREATED', 'Date de création');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_FILE_DOES_NOT_EXIST', 'Le fichier n\'existe pas');
define('TEXT_CACHE_DIRECTORY', 'Répertoire du cache :');

define('ERROR_CACHE_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire de cache n\'existe pas. Merci de le préciser dans Configuration -> Cache.');
define('ERROR_CACHE_DIRECTORY_NOT_WRITEABLE', 'Erreur : Le répertoire de cache n\'est pas autorisé en écriture.');
?>
