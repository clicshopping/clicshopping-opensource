<?php
/**
 * statistics.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */


//
// Statistiques Generales
//

// Nbr de clients total
function stat_gen1() {
  global $OSCOM_PDO;

  $Qcustomer = $OSCOM_PDO->prepare("select count(customers_id) as count
                                     from :table_customers
                                  ");
  $Qcustomer->execute();
  $customers = $Qcustomer->fetch();

  $customers_total = $customers['count'];
  return $customers_total;
}

// Nbr de clients total
function stat_order_total_delivery() {
  global $OSCOM_PDO;

  $QstatOrderTotalDelivery = $OSCOM_PDO->prepare("select  count(orders_id) as count
                                                   from :table_orders
                                                   where orders_status ='3'
                                                  ");
  $QstatOrderTotalDelivery->execute();
  $stat_order_total_delivery = $QstatOrderTotalDelivery->fetch();

  $stat_order_total_delivery = $stat_order_total_delivery['count'];
  return($stat_order_total_delivery);
}

// Average age et % of men
function stat_analyse_customers_man () {
  global $OSCOM_PDO;

  $QstatAnalyseCustomersMan = $OSCOM_PDO->prepare("select ROUND(((COUNT(*)/(SELECT COUNT(*) FROM customers))*100),2) AS numberByGenderPerCent,
                                                         ROUND(AVG(TIMESTAMPDIFF(YEAR,(customers_dob), now())),0) AS avgage
                                                   from :table_customers
                                                   where customers_gender = 'm'
                                                  ");
  $QstatAnalyseCustomersMan->execute();
  $stat_analyse_customers_man = $QstatAnalyseCustomersMan->fetch();

  if ($stat_analyse_customers_man['numberByGenderPerCent'] != 'null') {
   $numberByGenderPerCent = $stat_analyse_customers_man['numberByGenderPerCent'];
  }

  if ($stat_analyse_customers_man['avgage'] != 'null') {
   $avgage =  $stat_analyse_customers_man['avgage'];
  }

  $stat_analyse_customers_man = $numberByGenderPerCent .'% <br />' . $avgage.' ' .  TEXT_YEAR;

  return $stat_analyse_customers_man; 
}


// Average age et % of women
function stat_analyse_customers_woman() {
  global $OSCOM_PDO;

  $QstatAnalyseCustomersWoman = $OSCOM_PDO->prepare("SELECT ROUND(((COUNT(*)/(SELECT COUNT(*) FROM customers))*100),2) AS numberByGenderPerCent,
                                                        ROUND(AVG(TIMESTAMPDIFF(YEAR,(customers_dob), now())),0) AS avgage
                                                   from :table_customers
                                                   where customers_gender = 'f'
                                                  ");
  $QstatAnalyseCustomersWoman->execute();
  $stat_analyse_customers_woman = $QstatAnalyseCustomersWoman->fetch();

  if ($stat_analyse_customers_woman['numberByGenderPerCent'] != 'null') {
   $numberByGenderPerCent = $stat_analyse_customers_woman['numberByGenderPerCent'];
  }

  if ($stat_analyse_customers_woman['avgage'] != 'null') {
   $avgage =  $stat_analyse_customers_woman['avgage'];
  }
  
  $stat_analyse_customers_woman = $numberByGenderPerCent .'% <br />' . $avgage .' ' .  TEXT_YEAR;

  return $stat_analyse_customers_woman; 
}


// Nbr de produits total hors archive
function stat_gen2() {
  global $OSCOM_PDO;

  $Qproducts = $OSCOM_PDO->prepare("select count(products_id) as count
                                   from :table_products
                                   where products_status = :products_status
                                   and products_archive = :products_archive
                                  ");
  $Qproducts->bindInt(':products_status', '1');
  $Qproducts->bindInt(':products_archive', '0' );
  $Qproducts->execute();
  $products = $Qproducts->fetch();

  $products_total = $products['count'];

  return $products_total;
}

//nbr de commentaires total
function stat_gen3() {
  global $OSCOM_PDO;

  $Qreviews = $OSCOM_PDO->prepare("select count(reviews_id) as count
                                   from :table_reviews
                                   where status = :status
                                  ");
  $Qreviews->bindInt(':status', '0');
  $Qreviews->execute();
  $reviews = $Qreviews->fetch();

  $review_total = $reviews['count'];

  return $review_total;
}

// Nbr de produits total archive
function stat_gen4() {
  global $OSCOM_PDO;

  $QproductsArchives = $OSCOM_PDO->prepare("select count(products_id) as count
                                            from :table_products
                                            where products_archive = :products_archive
                                           ");
  $QproductsArchives->bindInt(':products_archive', '1');
  $QproductsArchives->execute();
  $products_archives = $QproductsArchives->fetch();

  $products_archives_total = $products_archives['count'];

  return $products_archives_total;
}


// Nbr de produits total hors ligne
  function stat_gen5() {
    global $OSCOM_PDO;

    $Qproducts = $OSCOM_PDO->prepare('select count(products_id) as count
                                       from :table_products
                                       where products_status = :products_status
                                       and products_archive = :products_archive
                                      ');
    $Qproducts->bindInt(':products_status', '0');
    $Qproducts->bindInt(':products_archive', '0' );
    $Qproducts->execute();
    $products = $Qproducts->fetch();

    $products_total_off_line = $products['count'];

    return $products_total_off_line;
  }


//nbr clients abonnes e la newsletter
function stat_customers_newsletter() {
  global $OSCOM_PDO;

  $QcustomersNewsletter = $OSCOM_PDO->prepare("select count(customers_id) as count
                                               from :table_customers
                                               where customers_newsletter = :customers_newsletter
                                              ");
  $QcustomersNewsletter->bindInt(':customers_newsletter', '1');
  $QcustomersNewsletter->execute();
  $customers_newsletter = $QcustomersNewsletter->fetch();

  $customers_total_newsletter = $customers_newsletter['count'];

  return $customers_total_newsletter;
}


//nbr clients abonnes e la newsletter sans compte
function stat_customers_newsletter_no_account() {
  global $OSCOM_PDO;

  $QcustomersNewsletterNoAccount = $OSCOM_PDO->prepare("select count(customers_newsletter) as count
                                                        from :table_newsletter_no_account
                                                        where customers_newsletter = :customers_newsletter
                                                        ");
  $QcustomersNewsletterNoAccount->bindInt(':customers_newsletter', '1');
  $QcustomersNewsletterNoAccount->execute();
  $customers_newsletter_no_account = $QcustomersNewsletterNoAccount->fetch();

  $customers_total_newsletter_no_account = $customers_newsletter_no_account['count'];

  return $customers_total_newsletter_no_account;
}



//nbr de clients abonne e la surveillance produit
function stat_customers_notification() {
  global $OSCOM_PDO;

  $QcustomersTotalNotification = $OSCOM_PDO->prepare("select count(products_id) as count
                                                        from :table_products_notifications
                                                        ");
  $QcustomersTotalNotification->execute();
  $customers_total_notification = $QcustomersTotalNotification->fetch();

  $customers_total_notification = $customers_total_notification['count'];

  return $customers_total_notification;
}

//nbr de clients abonne e la surveillance produit
function stat_recover_shopping_cart() {
  global $OSCOM_PDO;
  $QcustomersRecoverShoppingCart = $OSCOM_PDO->prepare("select count(customers_basket_id) as count
                                                        from :table_customers_basket
                                                        ");
  $QcustomersRecoverShoppingCart->execute();
  $customers_recover_shopping_cart = $QcustomersRecoverShoppingCart->fetch();

  $customers_recover_shopping_cart = $customers_recover_shopping_cart['count'];

  return $customers_recover_shopping_cart;
}



//
// Statistiques sur le CA
//

// CA total sur toutes les annees
  function stat_total() {
    global $OSCOM_PDO;

    $QcaTotal = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                                from :table_orders o,
                                                     :table_orders_products op
                                                where o.orders_id = op.orders_id
                                                and o.orders_status = :orders_status
                                                and  (YEAR(o.date_purchased))
                                              ");
    $QcaTotal->bindInt(':orders_status', '3');
    $QcaTotal->execute();
    $ca_total = $QcaTotal->fetch();

    $ca_total =  $ca_total['psum'];

    if (($ca_total == '') or ($ca_total == 0)) $ca_total = 0;
    return $ca_total;
  }


// CA annee en cours
  function stat0() {
    global $OSCOM_PDO;

    $QcaYear = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and  ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaYear->bindInt(':orders_status', '3');
    $QcaYear->execute();
    $ca_year = $QcaYear->fetch();

    $ca_year =  $ca_year['psum'];
    if (($ca_year == '') or ($ca_year == 0)) $ca_year = 0;
    return $ca_year;
  }


// CA annee n-1
  function stat1() {
    global $OSCOM_PDO;

    $QcaYear1 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and ((YEAR(o.date_purchased)) >= (YEAR(CURRENT_DATE))-1)
                                    and ((YEAR(o.date_purchased)) < (YEAR(CURRENT_DATE)))
                                  ");
    $QcaYear1->bindInt(':orders_status', '3');
    $QcaYear1->execute();
    $ca_year1 = $QcaYear1->fetch();

    $ca_year1 =  $ca_year1['psum'];
    if (($ca_year1 == '') or ($ca_year1 == 0)) $ca_year1 = 0;
    return $ca_year1;
  }

// Ca annee n-2
  function stat2() {
    global $OSCOM_PDO;

    $QcaYear2 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and ((YEAR(o.date_purchased)) >= (YEAR(CURRENT_DATE))-2)
                                    and ((YEAR(o.date_purchased)) < (YEAR(CURRENT_DATE))-1)
                                  ");
    $QcaYear2->bindInt(':orders_status', '3');
    $QcaYear2->execute();
    $ca_year2 = $QcaYear2->fetch();

    $ca_year2 =  $ca_year2['psum'];
    if (($ca_year2 == '') or ($ca_year2 == 0)) $ca_year2 = 0;
    return $ca_year2;
  }

// CA annee n-3
  function stat3() {
    global $OSCOM_PDO;

    $QcaYear3 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and ((YEAR(o.date_purchased)) >= (YEAR(CURRENT_DATE))-3)
                                    and  ((YEAR(o.date_purchased)) < (YEAR(CURRENT_DATE))-2)
                                  ");
    $QcaYear3->bindInt(':orders_status', '3');
    $QcaYear3->execute();
    $ca_year3 = $QcaYear3->fetch();

    $ca_year3 =  $ca_year3['psum'];
    if (($ca_year3 == '') or ($ca_year3 == 0)) $ca_year3 = 0;
    return $ca_year3;
  }

//
// Statististique sur nbr de client
//

//annee

function stat_customers_annee() {
  global $OSCOM_PDO;

  $QCustomersTotalYear = $OSCOM_PDO->prepare("select count(customers_id) as customers
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and  ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
  $QCustomersTotalYear->bindInt(':orders_status', '3');
  $QCustomersTotalYear->execute();
  $customers_total_year = $QCustomersTotalYear->fetch();

  $customers_total_year = $customers_total_year['customers'];

  return $customers_total_year ;
}

//annee n-1
function stat_customers_annee1() {
  global $OSCOM_PDO;

  $QCustomersTotalYear1 = $OSCOM_PDO->prepare("select count(customers_id) as customers
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and  ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)-1))
                                  ");
  $QCustomersTotalYear1->bindInt(':orders_status', '3');
  $QCustomersTotalYear1->execute();
  $customers_total_year1 = $QCustomersTotalYear1->fetch();

  $customers_total_year1 = $customers_total_year1['customers'];

  return $customers_total_year1;
}

// anneee n-2
function stat_customers_annee2() {
  global $OSCOM_PDO;

  $QCustomersTotalYear2 = $OSCOM_PDO->prepare("select count(customers_id) as customers
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and  ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)-2))
                                  ");
  $QCustomersTotalYear2->bindInt(':orders_status', '3');
  $QCustomersTotalYear2->execute();
  $customers_total_year2 = $QCustomersTotalYear2->fetch();

  $customers_total_year2 = $customers_total_year2['customers'];

  return $customers_total_year2;
}

//annee n-3
function stat_customers_annee3() {
  global $OSCOM_PDO;

  $QCustomersTotalYear3 = $OSCOM_PDO->prepare("select count(customers_id) as customers
                                    from :table_orders o,
                                         :table_orders_products op
                                    where o.orders_id = op.orders_id
                                    and o.orders_status = :orders_status
                                    and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)-3))
                                  ");
  $QCustomersTotalYear3->bindInt(':orders_status', '3');
  $QCustomersTotalYear3->execute();
  $customers_total_year3 = $QCustomersTotalYear3->fetch();

  $customers_total_year3 = $customers_total_year3['customers'];

  return $customers_total_year3;
}


//
// Statistiques par mois
//

// Ca mois janvier
  function stat1_month() {
    global $OSCOM_PDO;

    $QcaMonth1 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 1
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth1->bindInt(':orders_status', '3');
    $QcaMonth1->execute();
    $month_janvier = $QcaMonth1->fetch();

    $month_janvier =  $month_janvier['psum'];

    if (($month_janvier == '') or ($month_janvier == 0)) $month_janvier = 0;
    return $month_janvier;
  }

// Ca mois fevrier

  function stat2_month() {
    global $OSCOM_PDO;

    $QcaMonth2 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 2
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth2->bindInt(':orders_status', '3');
    $QcaMonth2->execute();
    $month_fevrier = $QcaMonth2->fetch();

    $month_fevrier =  $month_fevrier['psum'];
    if (($month_fevrier == '') or ($month_fevrier == 0)) $month_fevrier = 0;
    return $month_fevrier;
  }

// Ca mois mars
  function stat3_month() {
    global $OSCOM_PDO;

    $QcaMonth3 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 3
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth3->bindInt(':orders_status', '3');
    $QcaMonth3->execute();
    $month_mars = $QcaMonth3->fetch();

    $month_mars =  $month_mars['psum'];
    if (($month_mars == '') or ($month_mars == 0)) $month_mars = 0;
    return $month_mars;
  }

// Ca mois avril
  function stat4_month() {
    global $OSCOM_PDO;

    $QcaMonth4 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 4
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth4->bindInt(':orders_status', '3');
    $QcaMonth4->execute();
    $month_avril = $QcaMonth4->fetch();

    $month_avril =  $month_avril['psum'];
    if (($month_avril == '') or ($month_avril == 0)) $month_avril = 0;
    return $month_avril;
  }
// Ca mois mai
  function stat5_month() {
    global $OSCOM_PDO;

    $QcaMonth5 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 5
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth5->bindInt(':orders_status', '3');
    $QcaMonth5->execute();
    $month_mai = $QcaMonth5->fetch();

    $month_mai =  $month_mai['psum'];
    if (($month_mai == '') or ($month_mai == 0)) $month_mai = 0;
    return $month_mai;
  }

// Ca mois juin
  function stat6_month() {
    global $OSCOM_PDO;

    $QcaMonth6 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 6
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth6->bindInt(':orders_status', '3');
    $QcaMonth6->execute();
    $month_juin = $QcaMonth6->fetch();

    $month_juin =  $month_juin['psum'];
    if (($month_juin == '') or ($month_juin == 0)) $month_juin = 0;
    return $month_juin;
  }

// Ca mois juillet
  function stat7_month() {
    global $OSCOM_PDO;

    $QcaMonth7 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 7
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth7->bindInt(':orders_status', '3');
    $QcaMonth7->execute();
    $month_juillet = $QcaMonth7->fetch();

    $month_juillet =  $month_juillet['psum'];
    if (($month_juillet == '') or ($month_juillet == 0)) $month_juillet = 0;
    return $month_juillet;
  }

// Ca mois Aout
  function stat8_month() {
    global $OSCOM_PDO;

    $QcaMonth8 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 8
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth8->bindInt(':orders_status', '3');
    $QcaMonth8->execute();
    $month_aout = $QcaMonth8->fetch();

    $month_aout =  $month_aout['psum'];
    if (($month_aout == '') or ($month_aout == 0)) $month_aout = 0;
    return $month_aout;
  }

// Ca mois Septembre
  function stat9_month() {
    global $OSCOM_PDO;

    $QcaMonth9 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 9
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth9->bindInt(':orders_status', '3');
    $QcaMonth9->execute();
    $month_septembre = $QcaMonth9->fetch();

    $month_septembre =  $month_septembre['psum'];
    if (($month_septembre == '') or ($month_septembre == 0)) $month_septembre = 0;
    return $month_septembre;
  }
// Ca mois Octobre
  function stat10_month() {
    global $OSCOM_PDO;

    $QcaMonth10 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 10
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth10->bindInt(':orders_status', '3');
    $QcaMonth10->execute();
    $month_octobre = $QcaMonth10->fetch();

    $month_octobre =  $month_octobre['psum'];
    if (($month_octobre == '') or ($month_octobre == 0)) $month_octobre = 0;
    return $month_octobre;
  }

// Ca mois Novembre
  function stat11_month() {
    global $OSCOM_PDO;

    $QcaMonth11 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 11
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth11->bindInt(':orders_status', '3');
    $QcaMonth11->execute();
    $month_novembre = $QcaMonth11->fetch();

    $month_novembre =  $month_novembre['psum'];
    if (($month_novembre == '') or ($month_novembre == 0)) $month_novembre = 0;
    return $month_novembre;
  }

// Ca mois Decembre
  function stat12_month() {
    global $OSCOM_PDO;

    $QcaMonth12 = $OSCOM_PDO->prepare("select sum(op.final_price * op.products_quantity) as psum
                                      from :table_orders o,
                                           :table_orders_products op
                                      where o.orders_id = op.orders_id
                                      and o.orders_status = :orders_status
                                      and  ((MONTH(o.date_purchased))) = 12
                                      and ((YEAR(o.date_purchased)) = (YEAR(CURRENT_DATE)))
                                  ");
    $QcaMonth12->bindInt(':orders_status', '3');
    $QcaMonth12->execute();
    $month_decembre = $QcaMonth12->fetch();

    $month_decembre =  $month_decembre['psum'];
    if (($month_decembre == '') or ($month_decembre == 0)) $month_decembre = 0;
    return($month_decembre);
  }


// Nbr demande de contact a traiter
function stat_contact_customers() {
  global $OSCOM_PDO;

  $QcontactCustomers = $OSCOM_PDO->prepare("select count(contact_customers_id) as count
                                      from :table_contact_customers
                                      where contact_customers_archive = :contact_customers_archive
                                  ");
  $QcontactCustomers->bindInt(':contact_customers_archive', '0');
  $QcontactCustomers->execute();
  $contact_customers = $QcontactCustomers->fetch();

  $contact_customers_total = $contact_customers['count'];
  return $contact_customers_total;
}
