<?php
/*
 * quick_quantity_update.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_MESSAGE','Update your modifications before all changing display (sorting, changing page # ...');
define('TOP_BAR_TITLE', 'Quick Update');
define('HEADING_TITLE', 'Quick Update');
define('TEXT_MARGE_INFO','Modify by commercial margin, if you check this box, your products will be stroked at the rates given. To stroke to 25%, means that the price is marked up of 33%.');
define('TEXT_PRODUCTS_UPDATED', 'Item(s) Updated!');
define('TEXT_IMAGE_PREVIEW','Preview item');
define('TEXT_IMAGE_SWITCH_EDIT','Switch to complete edit');
define('TEXT_QTY_UPDATED', 'Value(s) changed');
define('TEXT_INPUT_SPEC_PRICE','<strong>Increase / reduction and click on preview</strong><br /><i>(Example : 10, 15%, -20, -25%..)</i>');
define('TEXT_SPEC_PRICE_INFO_UPDATE','<strong>Want strip the prices that you do not wish to modify<br />or all to cancel while clicking on the button "Cancel"');
define('TEXT_MAXI_ROW_BY_PAGE', 'Max. of lines per page :');
define('TEXT_SPECIALS_PRODUCTS', 'Special Product Price !');
define('TEXT_ALL_MANUFACTURERS', 'All brands');
define('TEXT_ALL_SUPPLIERS', 'All suppliers');
define('TEXT_INSERT', 'here your comment');
define('TEXT_SEARCH','Search (product or model)');
define('NO_PRICE','-- N/A --');
define('NO_TAX_TEXT','No Tax');
define('NO_MANUFACTURER','None');
define('NO_SUPPLIER','None');
define('TABLE_HEADING_CATEGORIES', 'Category');
define('TABLE_HEADING_PRICE_COMPARISON','Comparison (Yes/No)');
define('TABLE_HEADING_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Name');
define('TABLE_HEADING_PRICE', 'Public price');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_WEIGHT', 'Weight');
define('TABLE_HEADING_QUANTITY', 'Stock');
define('TABLE_HEADING_MIN_ORDER_QUANTITY', 'Min. order qty');
define('TABLE_HEADING_STATUS', 'Status Off/On');
define('TABLE_HEADING_PRODUCTS_ONLY_ONLINE','Exclusivity web (No/yes)');
define('TABLE_HEADING_MANUFACTURERS', 'Brands');
define('TABLE_HEADING_SUPPLIERS','Suppliers');
define('TABLE_HEADING_IMAGE', 'Image');
define('DISPLAY_CATEGORIES', 'Select category :');
define('DISPLAY_MANUFACTURERS', 'Select brands :');
define('DISPLAY_SUPPLIERS', 'Select suppliers :');
define('PRINT_TEXT', 'Print this page');
define('TOTAL_COST', 'Public price TAX inc.');
define('TEXT_SORT_ALL', 'Sort ');
define('TEXT_DESCENDINGLY', '- Descendingly');
define('TEXT_ASCENDINGLY', '- Ascendingly');
define('TEXT_BY', ' by ');
define('EDIT','Print');
define('TEXT_QUICK_UPDATES_SPECIALS','You cannot modify the public price because the product is currently in promotion.');
define('TEXT_QUICK_UPDATES_LOCKED','You cannot modify the public price because the prices in mode B2B are in manual mode. Please publish the card directly produces to carry out your modifications.');
define('TEXT_NO_TAXE','&nbsp;TAX exc.');
?>