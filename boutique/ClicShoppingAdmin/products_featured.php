<?php
/**
 * products_featured.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'setflag':

        osc_set_products_featured_status($_GET['id'], $_GET['flag']);

        osc_redirect(osc_href_link('products_featured.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'sID=' . $_GET['id']));
     
      break;

      case 'insert':
        $products_id = osc_db_prepare_input($_POST['products_id']);
        $products_price = osc_db_prepare_input($_POST['products_price']);
        $expdate = osc_db_prepare_input($_POST['expdate']);
        $schdate = osc_db_prepare_input($_POST['schdate']);
        $customers_group = osc_db_prepare_input($_POST['customers_group']);
        $twitter_status = osc_db_prepare_input($_POST['products_featured_twitter']);

        $Qprice = $OSCOM_PDO->prepare('select customers_group_price
                                       from :table_products_groups
                                       where products_id = :products_id
                                       and customers_group_id  =:customers_group_id
                                      ');
        $Qprice->bindInt(':products_id', (int)$products_id );
        $Qprice->bindInt(':customers_group_id', (int)$customers_group );
        $Qprice->execute();

// B2B
        while ($gprice =  $Qprice->fetch() ) {
          $products_price = $gprice['customers_group_price'];
        }

        $expires_date = '';
        if (osc_not_null($expdate)) {
          $expires_date = substr($expdate, 0, 4) . substr($expdate, 5, 2) . substr($expdate, 8, 2);
        }

        $scheduled_date = '';
        if (osc_not_null($schdate)) {
          $schedule_date = substr($schdate, 0, 4) . substr($schdate, 5, 2) . substr($schdate, 8, 2);
        }

        $OSCOM_PDO->save('products_featured', [
                                                'products_id' => (int)$products_id,
                                                'products_featured_date_added' => 'now()',
                                                'scheduled_date' => (osc_not_null($schedule_date) ? "'" . $schedule_date . "'" : 'null'),
                                                'expires_date' => (osc_not_null($expires_date) ? "'" . $expires_date . "'" : 'null'),
                                                'status' => 1,
                                                'customers_group_id' => (int)$customers_group
                                              ]
                        );

        if (($customers_group == 0 )) {
          if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
            if ($twitter_status == '1') {

              $Qproducts = $OSCOM_PDO->prepare('select distinct p.products_image,
                                                                pd.products_name,
                                                                p.products_status
                                                 from :table_products p,
                                                      :table_products_description pd,
                                                      :table_products_featured pf
                                                  where p.products_id =  :products_id
                                                  and p.products_id = pd.products_id
                                                  and p.products_status = 1
                                                  and pf.status = 1
                                                ');
              $Qproducts->bindInt(':products_id', (int)$products_id );
              $Qproducts->execute();

              $product = $Qproducts->fetch();

              $twitter_image =  DIR_FS_CATALOG_IMAGES . $product['products_image'];

              $text_products_featured =   TEXT_NEW_PRODUCTS_FEATURED_TWITTER . $product['products_name'] .' : ' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php?products_id='. $products_id;

              $_POST['twitter_msg'] =  $text_products_featured;
              $_POST['twitter_media'] =  $twitter_image;

              if ( $product['products_status'] == 1 ) {
                echo  osc_send_twitter($twitter_authentificate_administrator);
              }
            }
          }
        }

        osc_redirect(osc_href_link('products_featured.php', 'page=' . $_GET['page']));
        break;

      case 'update':
        $products_featured_id = osc_db_prepare_input($_POST['products_featured_id']);
        $products_price = osc_db_prepare_input($_POST['products_price']);
        $expdate = osc_db_prepare_input($_POST['expdate']);
        $schdate = osc_db_prepare_input($_POST['schdate']);
        $customers_group = osc_db_prepare_input($_POST['customers_group']);
        $twitter_status = osc_db_prepare_input($_POST['products_featured_twitter']);
        $status = osc_db_prepare_input($_POST['status']);


        $Qproducts = $OSCOM_PDO->prepare('select customers_group_price
                                          from :table_products_groups
                                          where products_id = :products_id
                                          and customers_group_id  = :customers_group_id
                                         ');
        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindInt(':customers_group_id',(int)$customers_group);
        $Qproducts->execute();


        while ($gprices =  $Qproducts->fetch() ) {
          $products_price = $gprices['customers_group_price'];
        };

        $expires_date = '';
        $scheduled_date = '';

        if (osc_not_null($expdate)) {
          $expires_date = substr($expdate, 0, 4) . substr($expdate, 5, 2) . substr($expdate, 8, 2);
        }

        if (osc_not_null($schdate)) {
          $scheduled_date = substr($schdate, 0, 4) . substr($schdate, 5, 2) . substr($schdate, 8, 2);
        }

        osc_db_query("update products_featured set customers_group_id = '" . (int)$customers_group . "',
                                                  products_featured_last_modified = now(),
                                                  expires_date = " . (osc_not_null($expires_date) ? "'" . $expires_date . "'" : 'null') . ",
                                                  scheduled_date  = " . (osc_not_null($scheduled_date) ? "'" . $scheduled_date. "'" : 'null') ."
                      where products_featured_id = '" . (int)$products_featured_id . "'");

        osc_redirect(osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&sID=' . $products_featured_id));
        break;

      case 'delete_all':

        if ($_POST['selected'] != '') {
          foreach ($_POST['selected'] as $products_featured['products_featured_id'] ) {

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_products_featured
                                            where products_featured_id = :products_featured_id 
                                          ');
            $Qdelete->bindInt(':products_featured_id',  (int)$products_featured['products_featured_id']);
            $Qdelete->execute();
          }
        }
        osc_redirect(osc_href_link('products_featured.php', 'page=' . $_GET['page']));
        break;
    }
  }

  require('includes/header.php');

  if (empty($action)) {

    $all_groups=array();

    $QcustomersGroups = $OSCOM_PDO->prepare('select customers_group_name,
                                                   customers_group_id
                                            from :table_customers_groups
                                            order by customers_group_id
                                ');

    $QcustomersGroups->execute();

    while ($existing_groups =  $QcustomersGroups->fetch() ) {
      $all_groups[$existing_groups['customers_group_id']]=$existing_groups['customers_group_name'];
    }

    $products_featured_query_raw = "select p.products_id, 
                                          p.products_model, 
                                          p.products_image,
                                          pd.products_name, 
                                          p.products_price, 
                                          s.products_featured_id, 
                                          s.customers_group_id, 
                                          s.products_featured_date_added, 
                                          s.products_featured_last_modified, 
                                          s.scheduled_date,
                                          s.expires_date, 
                                          s.date_status_change, 
                                          s.status,
                                          p.products_archive 
                                 from products p,
                                      products_featured s,
                                      products_description pd
                                where p.products_id = pd.products_id 
                                and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                                and p.products_id = s.products_id 
                                order by pd.products_name
                          ";

    $products_featured_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_featured_query_raw, $products_featured_query_numrows);
    $products_featured_query = osc_db_query($products_featured_query_raw);
  } // end empty action
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <div>
     <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <div class="adminTitle">
        <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_featured.png', HEADING_TITLE, '40', '40'); ?></span>
        <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
    <span class="col-md-4 smallText" style="text-align: center;">
      <?php echo $products_featured_split->display_count($products_featured_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?><br/>
      <?php echo $products_featured_split->display_links($products_featured_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
        </span>
    <span class="pull-right">
     <?php echo '<a href="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_products_featured.png', IMAGE_NEW_PRODUCT) . '</a>'; ?>
      <form name="delete_all" <?php echo 'action="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
      <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>
    </span>
<?php
 } // end empty

 if ( ($action == 'new') || ($action == 'edit') ) {
  $form_action = 'insert';
  if ( ($action == 'edit') && isset($_GET['sID']) ) {
   $form_action = 'update';
  }
?>
    <form name="new_special"<?php echo 'action="' . osc_href_link('products_featured.php', osc_get_all_get_params(array('action', 'info', 'sID')) . 'action=' . $form_action) . '"'; ?> method="post"><?php if ($form_action == 'update') echo osc_draw_hidden_field('products_featured_id', $_GET['sID']); ?>
    <span class="pull-right"><?php echo (($form_action == 'insert') ? osc_image_submit('button_insert_products_featured.png', IMAGE_INSERT) : osc_image_submit('button_update.gif', IMAGE_UPDATE)); ?>&nbsp;</span>
    <span class="pull-right"><?php echo '<a href="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . (isset($_GET['sID']) ? '&sID=' . $_GET['sID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
<?php
  }
?>
       </div>
       <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
     </div>
     <div class="clearfix"></div>
  </tr>
<!-- //################################################################################################################ -->
<!-- //                                      EDITION ET INSERTION D'UN 	COUP DE COEUR                          -->
<!-- //################################################################################################################ -->
<?php
  if ( ($action == 'new') || ($action == 'edit') ) {
    $form_action = 'insert';
    if ( ($action == 'edit') && isset($_GET['sID']) ) {
      $form_action = 'update';

      $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                              pd.products_name,
                                              s.customers_group_id,
                                              p.products_price,
                                              s.scheduled_date,
                                              s.expires_date
                                        from :table_products p,
                                             :table_products_description pd,
                                             :table_products_featured s
                                        where p.products_id = pd.products_id
                                        and pd.language_id = :language_id
                                        and p.products_id = s.products_id
                                        and s.products_featured_id = :products_featured_id
                                ');

      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->bindInt(':products_featured_id',(int)$_GET['sID']  );
      $Qproducts->execute();

      $product = $Qproducts->fetch();
      $product = osc_db_fetch_array($product_query);
      $sInfo = new objectInfo($product);
    } else {

     $sInfo = new objectInfo(array());

// create an array of products on special, which will be excluded from the pull down menu of products
// (when creating a new product on special)

      $products_featured_array = array();

      $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                               pf.customers_group_id
                                        from :table_products p,
                                             :table_products_featured pf
                                        where pf.products_id = p.products_id
                                        and p.products_status = 1
                                       ');

      $Qproducts->execute();

      while ($products_featured = $Qproducts->fetch() ) {
        $products_featured_array[] = (int)$products_featured['products_id'].":".(int)$products_featured['customers_group_id'];
      }


      $input_groups=array();
      $all_groups=array();

      if(isset($_GET['sID']) && $sInfo->customers_group_id != 0 ){

        $QcustomerGroupPrice = $OSCOM_PDO->prepare('select customers_group_price
                                                    from :table_products_groups
                                                    where products_id = :products_id
                                                    and customers_group_id =  :customers_group_id
                                                  ');
        $QcustomerGroupPrice->bindInt(':products_id', (int)$sInfo->products_id  );
        $QcustomerGroupPrice->bindInt(':customers_group_id', (int)$sInfo->customers_group_id  );

        $QcustomerGroupPrice->execute();

        if ($customer_group_price = $QcustomerGroupPrice->fetch() ) {
          $sInfo->products_price = $customer_group_price['customers_group_price'];
        }
      }
    }
?>
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
<!-- //#################################################################### //-->
<!--          ONGLET Information General de la Promotion                    //-->
<!-- //#################################################################### //-->
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                    <td class="mainTitle"><?php echo TITLE_PRODUCTS_FEATURED_GENERAL; ?></td>
                   </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                   <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                       <td class="main"><?php echo TEXT_PRODUCTS_FEATURED_PRODUCT; ?>&nbsp;</td>
                       <td class="main"><?php echo (isset($sInfo->products_name)) ? $sInfo->products_name . ' <small>(' . $currencies->format($sInfo->products_price) . ')</small>' : osc_draw_products_pull_down('products_id', 'style="font-size:10px"', $products_featured_array); echo osc_draw_hidden_field('products_price', (isset($sInfo->products_price) ? $sInfo->products_price : '')); ?></td>
                      </tr>
<?php
  if ($action ==  'new')  {
     if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
?>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_FEATURED_TWITTER; ?></td>
                        <td class="main"><?php echo  osc_draw_radio_field('products_featured_twitter', '1', $sInfo->in_accept_twitter) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('products_featured_twitter', '0', $sInfo->out_accept_twitter) . '&nbsp;' . TEXT_NO; ?></td>
                      </tr>
<?php
    }
  }
?>
                     </table></td>
                   </tr>
                  </table>
<?php
// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {
?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                     <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                   </tr>
                   <tr>
                      <td class="mainTitle"><?php echo TITLE_PRODUCTS_FEATURED_GROUPE; ?></td>
                   </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                   <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_FEATURED_GROUPS; ?>&nbsp;</td>
                       <td class="main"><?php echo osc_draw_pull_down_menu('customers_group', osc_get_customers_group(VISITOR_NAME), (isset($sInfo->customers_group_id)? $sInfo->customers_group_id:''));?> </td>
                      </tr>
                     </table></td>
                   </tr>
                  </table>
<?php
  }
?>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                   </tr>
                   <tr>
                    <td class="mainTitle"><?php echo TITLE_PRODUCTS_FEATURED_DATE; ?></td>
                   </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                   <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                       <td class="main"><?php echo TEXT_PRODUCTS_FEATURED_SCHEDULED_DATE; ?></td>
                       <td class="main"><?php  echo osc_draw_input_field('schdate', (osc_not_null($sInfo->scheduled_date) ? substr($sInfo->scheduled_date, 0, 4) . '-' . substr($sInfo->scheduled_date, 5, 2) . '-' . substr($sInfo->scheduled_date, 8, 2) : ''), 'id="schdate"'); ?></td>
                      </tr>
                      <tr>
                       <td class="main"><?php echo TEXT_PRODUCTS_FEATURED_EXPIRES_DATE; ?></td>
                       <td class="main"><?php echo osc_draw_input_field('expdate', (osc_not_null($sInfo->expires_date) ? substr($sInfo->expires_date, 0, 4) . '-' . substr($sInfo->expires_date, 5, 2) . '-' . substr($sInfo->expires_date, 8, 2) : ''), 'id="expdate"'); ?></td>
                      </tr>
                     </table></td>
                   </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                   </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
                   <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                       <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_BANNERS_IMAGE); ?></td>
                       <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_PRODUCTS_FEATURED_PRICE; ?></strong></td>
                      </tr>
                      <tr>
                       <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                       <td class="main"><?php echo TEXT_AIDE_PRODUCTS_FEATURED_PRICE; ?></td>
                      </tr>
                     </table></td>
                   </tr>
                  </table>
            </div>
          </div>
        </div>
      </td>
    </tr>
   </form>
<?php
  } else {
?>
<!-- //################################################################################################################ -->
<!-- //                                             LISTING DES COUPS DE COEUR                                             -->
<!-- //################################################################################################################ -->
    <tr>
     <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr class="dataTableHeadingRow">
           <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
           <td class="dataTableHeadingContent">&nbsp;</td>
           <td class="dataTableHeadingContent">&nbsp;</td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MODEL; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
<?php
// Permettre le changement de groupe en mode B2B
   if (MODE_B2B_B2C == 'true') {
?>
           <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_PRODUCTS_GROUP; ?></td>
<?php
   }
?>
               <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_PRODUCTS_PRICE; ?></td>
               <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SCHEDULED_DATE; ?></td>
               <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_EXPIRES_DATE; ?></td>
               <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ARCHIVE; ?></td>
               <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
               <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    while ($products_featured = osc_db_fetch_array($products_featured_query)) {

      if ((!isset($_GET['sID']) || (isset($_GET['sID']) && ($_GET['sID'] == $products_featured['products_featured_id']))) && !isset($sInfo)) {

        $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                                 products_image
                                          from :table_products
                                          where products_id = :products_id
                                         ');
                                       
        $Qproducts->bindInt(':products_id', (int)$products_featured['products_id']);

        $Qproducts->execute();

        $products = $Qproducts->fetch();

        $sInfo_array = array_merge($products_featured, $products);
        $sInfo = new objectInfo($sInfo_array);
      }

      if (isset($sInfo) && is_object($sInfo) && ($products_featured['products_featured_id'] == $sInfo->products_featured_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }

      $QcustomerGroupPrice = $OSCOM_PDO->prepare('select customers_group_price
                                                    from :table_products_groups
                                                    where products_id = :products_id
                                                    and customers_group_id =  :customers_group_id
                                                 ');
      $QcustomerGroupPrice->bindInt(':products_id', (int)$products_featured['products_id']);
      $QcustomerGroupPrice->bindInt(':customers_group_id', (int)$sInfo->customers_group_id);

      $QcustomerGroupPrice->execute();

      if ($scustomer_group_price = $QcustomerGroupPrice->fetch()) {
        $sInfo->products_price = $products_featured['products_price'] = $scustomer_group_price['customers_group_price'];
      }
?>
              <td>
<?php
      if ($products_featured['selected']) {
?>
              <input type="checkbox" name="selected[]" value="<?php echo $products_featured['products_featured_id']; ?>" checked="checked" />
<?php
      } else {
?>
              <input type="checkbox" name="selected[]" value="<?php echo $products_featured['products_featured_id']; ?>" />
<?php
      }
?>
                </td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products_featured['products_image'], $products_featured['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 	
                <td  class="dataTableContent"><?php echo $products_featured['products_model']; ?></td>
                <td  class="dataTableContent"><?php echo $products_featured['products_name']; ?></td>
<?php
// Permettre le changement de groupe en mode B2B
      if (MODE_B2B_B2C == 'true') {
// Ajouter afin de permettre d'afficher dans la liste des promotions deja cree le nom du groupe des clients non B2B
              if ($all_groups[$products_featured['customers_group_id']] != '') {
                $all_groups_name_products_featured = $all_groups[$products_featured['customers_group_id']];
        } else {
                $all_groups_name_products_featured = VISITOR_NAME;
        }
?>
              <td  class="dataTableContent"><?php echo $all_groups_name_products_featured; ?></td>
<?php
      } // end mode b2B_B2C
?>
                <td  class="dataTableContent" align="left"><?php echo $currencies->format($products_featured['products_price']); ?></td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($products_featured['scheduled_date']); ?></td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($products_featured['expires_date']); ?></td>
<?php
            if ( $products_featured['products_archive'] =='1') {
?>
              <td  class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', '', 16, 16) ; ?></td>
<?php
       } else {
?>
                <td  class="dataTableContent"></td>
<?php
       }
?>
                <td  class="dataTableContent" align="center">
<?php
            if ($products_featured['status'] == '1') {
              echo '<a href="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&action=setflag&flag=0&id=' . $products_featured['products_featured_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
              echo '<a href="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&action=setflag&flag=1&id=' . $products_featured['products_featured_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php
            echo '<a href="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&sID=' . (int)$products_featured['products_featured_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
            echo osc_draw_separator('pixel_trans.gif', '6', '16');
            if (isset($sInfo) && is_object($sInfo) && ($products_featured['products_featured_id'] == $sInfo->products_featured_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('products_featured.php', 'page=' . $_GET['page'] . '&sID=' . (int)$products_featured['products_featured_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    } // end while
?>
              </form><!-- end form delete all -->
              <tr>
                  <td colspan="12" class="smallText" valign="top"><?php echo $products_featured_split->display_count($products_featured_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
              </tr>
            </table></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

