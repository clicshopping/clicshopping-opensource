<?php
/**
 * products_attributes.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Options produits');

define('HEADING_TITLE_OPT', 'Etape 1 : Nom de l\'option du produit');
define('HEADING_TITLE_VAL', 'Etape 2 : Caractéristique des options pour le produit');
define('HEADING_TITLE_ATRIB', 'Etape 3 : Définition des attributs des produits');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_REF_ATTRIBUTES','Ref. Attribut produits');
define('TABLE_HEADING_PRODUCT', 'Nom du produit');
define('TABLE_HEADING_OPT_NAME', 'Nom de l\'option');
define('TABLE_HEADING_OPT_VALUE', 'Caratéristique de l\'option');
define('TABLE_HEADING_OPT_PRICE', 'Prix');
define('TABLE_HEADING_OPT_PRICE_PREFIX', 'Préfixe');
define('TABLE_HEADING_OPT_ORDER','Ordre affichage catalogue');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_DOWNLOAD', 'Produits téléchargeable :');
define('TABLE_TEXT_FILENAME', 'Nom du fichier :');
define('TABLE_TEXT_MAX_DAYS', 'Expire le :');
define('TABLE_TEXT_MAX_COUNT', 'Compte de téléchargement maximal :');
define('HEADING_TITLE_CLONE_PRODUCTS_ATTRIBUTES','Etape 4 :Cloner / Copier des attributs');
define('CLONE_PRODUCTS_FROM','de ');
define('CLONE_PRODUCTS_TO','vers ');

define('TITLE_AIDE_ATTRIBUTS', 'Note sur les attributs');

$upload_max_filesize =  @ini_get('upload_max_filesize');
define('TEXT_AIDE_ATTRIBUTS', '<ul><strong><u>les préfixes d\'attribut</u></strong> : <br /><br /><li> - La synthaxe "+" permet de multiplier l\'attribut par la quantité et de l\'additionner au prix</li><li>- La synthaxe "-" permet de mullitplier l\'attribut par la quantité et de le soustraire au prix</li><br /><li> - N\'oubliez pas aussi de paramétrer le statut de commande concernant l\'autorisation de téléchargement.</li><li> - Le téléchargement de fichiers ne peut excéder : ' . $upload_max_filesize .'</li>');

define('TITLE_AIDE_CLONE', 'Note sur les attributs');
define('TEXT_AIDE_CLONE', '<ul><strong><u>La Copie / Clonage d\'attribut</u></strong> : <br /><br /><li>Vous pouvez copier un ou plusieurs attributs d\'un produit sur un ou plusieurs autres produits</li><li>Pour supprimer en masse un attribut vous devez choisir un produit, puis un ou plusieurs autres produits.<br />Cliquez ensuite sur supprimer et l\'ensemble des attributs sera effacé</li></ul>');


define('TEXT_WARNING_OF_DELETE', 'Certains produits utilisent cette option - il n\'est pas recommandé de la supprimer.');
define('TEXT_OK_TO_DELETE', 'Cette option n\'est utilisée par aucun produit - Il est possible de la supprimer.');
define('TEXT_OPTION_ID', 'Option ID');
define('TEXT_OPTION_NAME', 'Nom de l\'option');


define('ONGLET_STEP1','Etape 1');
define('ONGLET_STEP2','Etape 2');
define('ONGLET_STEP3','Etape 3');
define('ONGLET_STEP4','Etape 4');
?>