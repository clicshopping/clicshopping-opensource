<?php
/**
 * ot_tax.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class ot_tax {
    var $title, $output;

    function ot_tax() {
      $this->code = 'ot_tax';
      $this->title = MODULE_ORDER_TOTAL_TAX_TITLE;
      $this->description = MODULE_ORDER_TOTAL_TAX_DESCRIPTION;
// Controle en B2B l'assujetti a la TVA (valeur true par defaut en mode B2C)
      if ( osc_get_tax_unallowed($this->code) ) {
        $this->enabled = ((MODULE_ORDER_TOTAL_TAX_STATUS == 'true') ? true : false);
    }
      $this->sort_order = MODULE_ORDER_TOTAL_TAX_SORT_ORDER;

      $this->output = array();
    }

    function process() {
      global $order, $currencies;


// Txe Canada - Quebec
      if (DISPLAY_DOUBLE_TAXE =='true') {
  //WARNING: This module does not consider tax_class!!! We assume everything is taxable.
  //We run SQL to get total number of taxes configured for our shipping zone
  //If we have many taxes rates configured, we will compare tax priorities.
  //If taxes have different priorities, ot_tax will apply 2nd priority tax_rate over 1rst (ex: for Quebec we have PST over GST), we assume GST has the lowest priority. 
  //If taxes have the same priorities, ot_tax still show taxes on two line but dosen't apply compounded taxes (ie: Ontario)
  //If we get only one tax result, we assume we are handling only GST or HST (same scenario)
  // take also the shipping taxe
        $tax_priority_query = osc_db_query("select tax_priority 
                                           from tax_rates tr left join zones_to_geo_zones za on (tr.tax_zone_id = za.geo_zone_id)
                                                            left join geo_zones tz on (tz.geo_zone_id = tr.tax_zone_id)
                                           where za.zone_country_id = '" . $order->delivery['country']['id'] . "'
                                           and za.zone_id = '" . $order->delivery['zone_id'] . "' 
                                           order by tr.tax_priority
                                          ");
                                          
        $tax_query_raw="select tax_rates_id, 
                              tax_priority, 
                              tax_rate, 
                              tax_description 
                       from tax_rates tr left join zones_to_geo_zones za on (tr.tax_zone_id = za.geo_zone_id)
                                         left join geo_zones tz on (tz.geo_zone_id = tr.tax_zone_id)
                       where za.zone_country_id = '" . $order->delivery['country']['id'] . "'
                       and za.zone_id = '" . $order->delivery['zone_id'] . "'
                       order by tr.tax_priority
                      ";
        if (osc_db_num_rows($tax_priority_query)) {
         if (osc_db_num_rows($tax_priority_query) == 2) { //Show taxes on two lines
          $i=0;
            while ($tax = osc_db_fetch_array($tax_priority_query)) { //compare tax_priotiries
              if ($i == 0) {
                $tax_priority = $tax['tax_priority']; 
              } else {
                if ($tax_priority != $tax['tax_priority']) {
                  $compound_tax=true; 
                } else {
                  $compound_tax=false; 
                }
             }
           $i++;
         }
//END Compare tax priorities
        $tax_query = osc_db_query($tax_query_raw);
          if ($compound_tax) { //ie Quebec different de false et true
             $j=0;
             while ($tax = osc_db_fetch_array($tax_query)) {
               if ($j == 0) {
                 $gst_description = $tax['tax_description']; 
                 $gst_rate = $tax['tax_rate'] / 100;
               } elseif ($j >= 1) {
                 $pst_description = $tax['tax_description'];
                 $pst_rate = $tax['tax_rate'] / 100;
               }
               $j++;
             }
             $subtotal = $order->info['subtotal'] + $order->info['shipping_cost'];
// Si l'ordre d'affichage du shipping < sort order on additionne les frais d'envoi au sous total
             if (MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER < MODULE_ORDER_TOTAL_TAX_SORT_ORDER) $subtotal += $order->info['shipping_cost'];

             $gst_total = osc_round($subtotal * $gst_rate, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
             $pst_total = ($subtotal+$gst_total) * $pst_rate;
             foreach ( $order->info['tax_groups'] as $key => $value ) {
               if ($value > 0) {
                 $this->output[] = array('title' => $gst_description.':',
                                         'text' => $currencies->format( $gst_total, true, $order->info['currency'], $order->info['currency_value']), 'value' => $gst_total);
                 $this->output[] = array('title' => $pst_description.':',
                                         'text' => $currencies->format( $pst_total, true, $order->info['currency'], $order->info['currency_value']), 
					  'value' => $pst_total);										
               } 
             } // end while
          } else { //ie: Ontario
            $j=0;
            while ($tax = osc_db_fetch_array($tax_query)) {
              if ($j == 0) {
                $gst_description = $tax['tax_description'];
                $gst_rate = $tax['tax_rate'] / 100;
              } elseif ($j >= 1) {
                $pst_description = $tax['tax_description'];
                $pst_rate = $tax['tax_rate'] / 100;
              }
              $j++;
          }
            $subtotal = $order->info['subtotal'];

// Si l'ordre d'affichage du shipping < sort order on additionne les frais d'envoi au sous total
          if (MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER < MODULE_ORDER_TOTAL_TAX_SORT_ORDER) $subtotal += $order->info['shipping_cost'];

          $gst_total = osc_round($subtotal * $gst_rate, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
          $pst_total = $subtotal * $pst_rate;

          foreach ( $order->info['tax_groups'] as $key => $value ) {
            if ($value > 0) {
              $this->output[] = array('title' => $gst_description.':',
                                      'text' => $currencies->format($gst_total, true, $order->info['currency'], $order->info['currency_value']),
                                      'value' => $gst_total);
              $this->output[] = array('title' => $pst_description.':',
                                      'text' => $currencies->format($pst_total, true, $order->info['currency'], $order->info['currency_value']),
                                       'value' => $pst_total);
            } 
          } 
        }
// -----------------------------        
// Only one taxe
// -----------------------------        
      } elseif (osc_db_num_rows($tax_priority_query) == 1) { //Only GST or HST applies
        $tax_query = osc_db_query($tax_query_raw);
        while ($tax = osc_db_fetch_array($tax_query)) {

          $subtotal = $order->info['subtotal'];

// Si l'ordre d'affichage du shipping < sort order on additionne les frais d'envoi au sous total
          if (MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER < MODULE_ORDER_TOTAL_TAX_SORT_ORDER) $subtotal += $order->info['shipping_cost'];

          $hst_total = $subtotal * ($tax['tax_rate'] / 100);

          reset($order->info['tax_groups']);
          while (list($key, $value) = each($order->info['tax_groups'])) {
            if ($value > 0) {
              $this->output[] = array('title' => $tax['tax_description'].':',
                                      'text' => $currencies->format( $hst_total, true, $order->info['currency'], $order->info['currency_value']),'value' => $hst_total);
            } 
          } 
        }
      } // end elseif
    }

//We calculate $order->info with updated tax values. For this to work ot_tax has to be last ot module called, just before ot_total
    $order->info['tax'] = osc_round($gst_total,$currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + osc_round($pst_total,$currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + osc_round($hst_total,$currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
    $order->info['total'] = $order->info['subtotal'] + $order->info['tax'] + $order->info['shipping_cost'];

    } else {
// **********************************
// normal tax
// ************************************
      reset($order->info['tax_groups']);
      while (list($key, $value) = each($order->info['tax_groups'])) {
        if ($value > 0) {
          $this->output[] = array('title' => $key . ':',
                                  'text' => $currencies->format($value, true, $order->info['currency'], $order->info['currency_value']),
                                  'value' => $value);
        }
      }
    }
  }

    function check() {
      if (!isset($this->_check)) {
        $check_query = osc_db_query("select configuration_value from configuration where configuration_key = 'MODULE_ORDER_TOTAL_TAX_STATUS'");
        $this->_check = osc_db_num_rows($check_query);
      }

      return $this->_check;
    }

    function keys() {
      return array('MODULE_ORDER_TOTAL_TAX_STATUS',
                   'MODULE_ORDER_TOTAL_TAX_SORT_ORDER');
    }

    function install() {
      if ($_SESSION['languages_id'] =='1') {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Affichage de la taxe', 'MODULE_ORDER_TOTAL_TAX_STATUS', 'true', 'Voulez-vous montrer la taxe de la commande ?', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ordre de tri', 'MODULE_ORDER_TOTAL_TAX_SORT_ORDER', '3', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier).', '6', '2', now())");
      } else {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Display Tax', 'MODULE_ORDER_TOTAL_TAX_STATUS', 'true', 'Do you want to display the order tax value?', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_ORDER_TOTAL_TAX_SORT_ORDER', '3', 'Sort order of display.', '6', '2', now())");

      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>
