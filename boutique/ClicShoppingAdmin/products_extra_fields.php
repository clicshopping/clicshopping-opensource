<?php
/**
 * products_extra_fields.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

// Has "Remove" button been pressed?
if (isset($_POST['remove_x']) || isset($_POST['remove_y'])) $action='remove';

if (osc_not_null($action)) {
  switch ($action) {
    case 'setflag':
      $sql_data_array = array('products_extra_fields_status' => osc_db_prepare_input($_GET['flag']));
      osc_db_perform('products_extra_fields', $sql_data_array, 'update', 'products_extra_fields_id=' . (int)$_GET['id']);
      osc_redirect(osc_href_link('products_extra_fields.php'));
  break;

    case 'add':

      $sql_data_array = array('products_extra_fields_name' => osc_db_prepare_input($_POST['field']['name']),
                              'languages_id' => ($_POST['field']['language']),
                              'products_extra_fields_order' => osc_db_prepare_input((int)$order),
                              'customers_group_id' => osc_db_prepare_input($_POST['field']['customers_group']),
                              'products_extra_fields_type' => osc_db_prepare_input($_POST['field']['products_extra_fields_type'])
                             );

      $OSCOM_PDO->save('products_extra_fields', $sql_data_array);

      osc_redirect(osc_href_link('products_extra_fields.php'));
    break;
    case 'update':

      foreach ($_POST['field'] as $key=>$val) {
        $sql_data_array = array('products_extra_fields_name' => osc_db_prepare_input($val['name']),
                                'languages_id' =>  osc_db_prepare_input($val['language']),
                                'products_extra_fields_order' => osc_db_prepare_input($val['order']),
                                'customers_group_id' => osc_db_prepare_input($val['customers_group']),
                                'products_extra_fields_type' => osc_db_prepare_input($val['products_extra_fields_type'])
                               );
        $OSCOM_PDO->save('products_extra_fields', $sql_data_array, ['products_extra_fields_id' =>(int)$key ] );
      }
      osc_redirect(osc_href_link('products_extra_fields.php'));

    break;

    case 'remove':
      if ($_POST['mark']) {
        foreach ($_POST['mark'] as $key=>$val) {

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_products_extra_fields
                                        where products_extra_fields_id = :products_extra_fields_id 
                                      ');
        $Qdelete->bindInt(':products_extra_fields_id',  (int)$key);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_products_to_products_extra_fields
                                        where products_extra_fields_id = :products_extra_fields_id 
                                      ');
        $Qdelete->bindInt(':products_extra_fields_id',  (int)$key );
        $Qdelete->execute();
        }
        osc_redirect(osc_href_link('products_extra_fields.php'));
      }
    break;
  }
}

// Put languages information into an array for drop-down boxes
  $customers_group = osc_get_customers_group();
  $values_customers_group[0]=array ('id' =>'0', 'text' => VISITOR_NAME);
  for ($i=0, $n=sizeof($customers_group); $i<$n; $i++) {
    $values_customers_group[$i+1]=array ('id' =>$customers_group[$i]['id'],
                                         'text' =>$customers_group[$i]['text']
                                        );
  }


// Put languages information into an array for drop-down boxes
  $languages=osc_get_languages();
  $values[0]=array ('id' =>'0', 'text' => TEXT_ALL_LANGUAGES);
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
    $values[$i+1]=array ('id' =>$languages[$i]['id'],
                         'text' =>$languages[$i]['name']
                        );
  }

// Select the  fields type
    if (MODE_B2B_B2C == 'true') {
      $field_type_array = array(array('id' => '0', 'text' => ENTRY_TEXT),
                                array('id' => '1', 'text' => ENTRY_TEXT_AREA),
                                array('id' => '2', 'text' => ENTRY_TEXT_CHECKBOX),
                              );
    } else {
      $field_type_array = array(array('id' => '0', 'text' => ENTRY_TEXT));
    }

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>


   <div  class="adminTitle">
     <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_options.gif', HEADING_TITLE, '40', '40'); ?></span>
     <span class="col-md-5 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
   </div>
   <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
   <div>
     <?php echo osc_draw_form('add_field', 'products_extra_fields.php', 'action=add', 'post'); ?>
     <span class="col-md-5"><?php echo HEADING_TITLE_INSERT; ?></span>
     <span class="pull-right"><?php echo osc_image_submit('button_insert.gif',IMAGE_INSERT)?></span>
  </div>


<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="5">
      <tr class="dataTableHeadingRow">
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIELDS; ?></td>
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIELDS_TYPE; ?></td>
<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS_GROUP; ?></td>
<?php
  }
?>
        <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
        <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LANGUAGE; ?></td>
      </tr>
      <tr>
        <td class="dataTableContent" align="left"><?php echo osc_draw_input_field('field[name]', $field['name'], 'size=30 required aria-required="true" id="field[name]"', false, 'text', true);?></td>
        <td class="smallText" align="left"><?php echo osc_draw_pull_down_menu('field[products_extra_fields_type]', $field_type_array, '0', ''); ?></td>
<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
          <td class="smallText" align="left"><?php echo  osc_draw_pull_down_menu('field[customers_group]', $values_customers_group, '0', ''); ?></td>
<?php
  }
?>
          <td class="dataTableContent" align="center"><?php echo osc_draw_input_field('field[order]', $order, 'size=5', false, 'text', true);?></td>
          <td class="dataTableContent" align="center"><?php echo osc_draw_pull_down_menu('field[language]', $values, '0', '');?></td>
        </tr>
    </table></td>
  </tr>
</form>
</table>


<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <div class="adminformAide">
        <div class="row">
          <span class="col-md-12">
            <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_OPTIONS); ?>
            <strong><?php echo '&nbsp;' . TITLE_AIDE_OPTIONS; ?></strong>
          </span>
        </div>
        <div class="spaceRow"></div>
        <div class="row">
          <span class="col-md-12">
            <?php echo '&nbsp;&nbsp;' . TEXT_AIDE_OPTIONS . '<strong>*</strong>'; ?>
          </span>
        </div>
      </div>
    </div>
</table>
<?php
  }
?>
     <div><br /><hr></div>



<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <?php echo osc_draw_form('extra_fields', 'products_extra_fields.php','action=update','post'); ?>
  <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="4">
      <tr>
      <td class="main"><?php echo HEADING_TITLE_MODIFIY; ?></td>
        <td align=right><?php echo osc_image_submit('button_mini_update.gif',IMAGE_UPDATE)?> &nbsp;&nbsp;<?php echo osc_image_submit('button_delete.gif',IMAGE_DELETE,'name="remove"')?> </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="5">
      <tr class="dataTableHeadingRow">
        <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DELETE_FIELDS; ?></td>
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ID_FIELDS; ?></td>
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIELDS; ?></td>
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIELDS_TYPE; ?></td>
<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
        <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS_GROUP; ?></td>
<?php
  }
?>
        <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS_DIPSLAY; ?></td>		
        <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
        <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LANGUAGE; ?></td>
      </tr>
<?php
  $QproductsExtraFields = $OSCOM_PDO->prepare('select *
                                               from :table_products_extra_fields
                                               order by products_extra_fields_id,
                                                        products_extra_fields_order
                                              ');
  $QproductsExtraFields->execute();

  while ($extra_fields = $QproductsExtraFields->fetch() ) {
?>
      <tr>
        <td align="center"><?php echo osc_draw_checkbox_field('mark['.$extra_fields['products_extra_fields_id'].']', 1) ?></td>
        <td class="dataTableContent" width="80"><?php echo $extra_fields['products_extra_fields_id']; ?>
        <td class="dataTableContent"><?php echo osc_draw_input_field('field['.$extra_fields['products_extra_fields_id'].'][name]', $extra_fields['products_extra_fields_name'], 'size=30 required aria-required="true" id="name"', false, 'text', true);?></td>

<?php	
    if ($extra_fields['products_extra_fields_type'] == '0'){
      $extra_fields_type = ENTRY_TEXT;
    } elseif ($extra_fields['products_extra_fields_type'] =='1') {
      $extra_fields_type	= ENTRY_TEXT_AREA;
    } else {
      $extra_fields_type	= ENTRY_TEXT_CHECKBOX;
    }
?>
        <td class="dataTableContent" align="left"><?php echo $extra_fields_type . osc_draw_hidden_field('field['.$extra_fields['products_extra_fields_id'].'][products_extra_fields_type]', $extra_fields['products_extra_fields_type']); ?> </td>
<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
        <td class="dataTableContent" align="center"><?php echo osc_draw_pull_down_menu('field['.$extra_fields['products_extra_fields_id'].'][customers_group]', $values_customers_group, $extra_fields['customers_group_id'], ''); ?></td>	
<?php
  }
?>
        <td  class="dataTableContent" align="center">
<?php
      if ($extra_fields['products_extra_fields_status'] == '1') {
        echo '<a href="' . osc_href_link('products_extra_fields.php', 'action=setflag&flag=0&id=' . $extra_fields['products_extra_fields_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
        } else {
        echo '<a href="' . osc_href_link('products_extra_fields.php', 'action=setflag&flag=1&id=' . $extra_fields['products_extra_fields_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
        </td>
        <td class="dataTableContent" align="center"><?php echo osc_draw_input_field('field['.$extra_fields['products_extra_fields_id'].'][order]', $extra_fields['products_extra_fields_order'], 'size=5', false, 'text', true);?></td>
        <td class="dataTableContent" align="center"><?php echo osc_draw_pull_down_menu('field['.$extra_fields['products_extra_fields_id'].'][language]', $values, $extra_fields['languages_id'], ''); ?></td>	
      </tr>
<?php 
  } 
?>
    </table></td>
 <!-- body_text_eof //-->
  </tr>
</form>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
