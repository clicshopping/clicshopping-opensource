<?php
/**
 * page_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version 
*/
  set_time_limit(0);

  require('includes/application_top.php');
  require('includes/functions/page_manager.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  
  $ClassMessageStackError = 0;

  $languages = osc_get_languages();

  if (osc_not_null($action)) {
    switch ($action) {
      case 'setflag':
        osc_set_page_status($_GET['id'], $_GET['flag']);


        $OSCOM_MessageStack->add_session(SUCCESS_PAGE_MANAGER_STATUS_UPDATED, 'success');
        osc_redirect(osc_href_link('page_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . '&bID=' . $_GET['id']));
      break;
      
      case 'insert':
      case 'update':

        if (isset($_POST['pages_id'])) $pages_id = osc_db_prepare_input($_POST['pages_id']);
        $sort_order = osc_db_prepare_input($_POST['sort_order']);
        $page_type = osc_db_prepare_input($_POST['page_type']);
        $links_target = osc_db_prepare_input($_POST['links_target']);
        $page_box = osc_db_prepare_input($_POST['page_box']);
        $page_time = osc_db_prepare_input($_POST['page_time']);
        $page_date_start = osc_db_prepare_input($_POST['page_date_start']);
        $page_date_closed = osc_db_prepare_input($_POST['page_date_closed']);
        $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);
        $page_general_condition = osc_db_prepare_input($_POST['page_general_condition']);

        $page_error = false;

        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $title_field_name = $_POST['pages_title_'.$languages[$i]['id']];
          if (empty($title_field_name)) {
            $OSCOM_MessageStack->add(ERROR_PAGE_TITLE_REQUIRED, 'error');
            $page_error = true;
            $languages_title_error = $languages[$i]['id'];
          }
        }

        if ($page_error == false) {

// Preparation pour inserer les donnees dans la table "pages"
           $sql_data_array_pages = array('sort_order' => $sort_order,
                                         'links_target' => $links_target,
                                         'page_time' => $page_time,
                                         'status' => '1',
                                         'page_type' => $page_type,
                                         'customers_group_id' => $customers_group_id,
                                         'page_box' => $page_box,
                                         'page_general_condition' => $page_general_condition
                                         );

// Insertion ou mise a jour de la table "pages"
           if ($action == 'insert') {

             $insert_sql_data_pages = array('date_added' => 'now()');
             $sql_data_array_pages = array_merge($sql_data_array_pages, $insert_sql_data_pages);

             $OSCOM_PDO->save('pages_manager', $sql_data_array);

             $pages_id = $OSCOM_PDO->lastInsertId();

           } else if ($action == 'update') {

             $insert_sql_data_pages = array('last_modified' => 'now()',
                                            'customers_group_id' => (int)$customers_group_id
                                           );
             $sql_data_array_pages = array_merge($sql_data_array_pages, $insert_sql_data_pages);

             $OSCOM_PDO->save('pages_manager', $sql_data_array_pages, ['pages_id' => (int)$pages_id] );

           }

// Enregistrement de la date de debut d'impression dans la table "pages"
          if (osc_not_null($page_date_start)) {
            list($day, $month, $year) = explode('/', $page_date_start);

            $page_date_start = $year .
                              ((strlen($month) == 1) ? '0' . $month : $month) .
                              ((strlen($day) == 1) ? '0' . $day : $day);

            $Qupdate = $OSCOM_PDO->prepare('update :table_pages_manager
                                            set status = :status, 
                                            page_date_start = :page_date_start
                                            where pages_id = :pages_id
                                          ');
            $Qupdate->bindInt(':status','0' );
            $Qupdate->bindValue(':page_date_start', $page_date_start);
            $Qupdate->bindInt(':pages_id', (int)$pages_id );
            $Qupdate->execute();

          } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_pages_manager
                                            set page_date_start = null
                                            where pages_id = :pages_id
                                          ');

            $Qupdate->bindInt(':pages_id', (int)$pages_id );
            $Qupdate->execute();
          }

// Enregistrement de la date de fin d'impression dans la table "pages"
          if (osc_not_null($page_date_closed)) {
            list($day, $month, $year) = explode('/', $page_date_closed);

            $page_date_closed = $year . ((strlen($month) == 1) ? '0' . $month : $month) . ((strlen($day) == 1) ? '0' . $day : $day);

            $Qupdate = $OSCOM_PDO->prepare('update :table_pages_manager
                                            set page_date_closed = :page_date_closed
                                            where pages_id = :pages_id
                                          ');
            $Qupdate->bindValue(':page_date_closed', $page_date_closed);
            $Qupdate->bindInt(':pages_id', (int)$pages_id );
            $Qupdate->execute();

          } else {
       
            $Qupdate = $OSCOM_PDO->prepare('update :table_pages_manager
                                            set page_date_closed = null
                                            where pages_id = :pages_id
                                          ');

            $Qupdate->bindInt(':pages_id', (int)$pages_id );
            $Qupdate->execute();
         }

// Insertion ou mise a jour de la table "pages_description"
         for ($i=0, $n=sizeof($languages); $i<$n; $i++) {

            $pages_title = osc_db_prepare_input($_POST['pages_title_' . $languages[$i]['id']]);
            $pages_html_text = osc_db_prepare_input($_POST['pages_html_text_' . $languages[$i]['id']]);
            $externallink = osc_db_prepare_input($_POST['externallink_' . $languages[$i]['id']]);
            $page_manager_head_title_tag = osc_db_prepare_input($_POST['page_manager_head_title_tag_' . $languages[$i]['id']]);
            $page_manager_head_keywords_tag = osc_db_prepare_input($_POST['page_manager_head_keywords_tag_' . $languages[$i]['id']]);
            $page_manager_head_desc_tag = osc_db_prepare_input($_POST['page_manager_head_desc_tag_' . $languages[$i]['id']]);


            $sql_data_array_pages_description = array('pages_title' => $pages_title,
                                                      'pages_html_text' => $pages_html_text,
                                                      'externallink' => $externallink,
                                                      'page_manager_head_title_tag'  => $page_manager_head_title_tag,
                                                      'page_manager_head_keywords_tag'  => $page_manager_head_keywords_tag,
                                                      'page_manager_head_desc_tag'  => $page_manager_head_desc_tag
                                                      );
           if ($action == 'insert') {
            $bID = "";
             $pageid_merge = array('pages_id' => $pages_id,
                                  'language_id' => $languages[$i]['id']);

             $sql_data_array_pages_desc = array_merge($sql_data_array_pages_description, $pageid_merge);
              osc_db_perform('pages_manager_description', $sql_data_array_pages_desc);

            } elseif ($action == 'update') {

              $QRecordExists = $OSCOM_PDO->prepare('select  count( * ) as countrecords 
                                                    from :table_pages_manager_description
                                                    where pages_id= :pages_id
                                                    and language_id= :language_id
                                                 ');
              $QRecordExists->bindInt(':pages_id',  (int)$pages_id);
              $QRecordExists->bindInt(':language_id',  (int)$languages[$i]['id']);
              $QRecordExists->execute();

              $recordexists = $QRecordExists->fetch();

              if($recordexists['countrecords'] >= 1 )  {

                $OSCOM_PDO->save('pages_manager_description', $sql_data_array_pages_description, ['pages_id' => (int)$pages_id,
                                                                                                  'language_id' => (int)$languages[$i]['id']
                                                                                                  ]
                                );

              } else {
                $pageid_merge= array('pages_id' => $pages_id,
                                     'language_id' => $languages[$i]['id']);

                $sql_data_array_pages_desc = array_merge($sql_data_array_pages_description, $pageid_merge);

                $OSCOM_PDO->save('pages_manager_description', $sql_data_array_pages_desc);
              }
            }
          } //for


          $OSCOM_MessageStack->add_session(SUCCESS_PAGE_UPDATED, 'success');
          osc_redirect(osc_href_link('page_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'bID=' . (int)$pages_id));

        } else {
          $action = 'edit';

        }
      break;

      case 'delete_all':

        $pages_id = osc_db_prepare_input($_GET['bID']);

        if ($_POST['selected'] != '') {
         foreach ($_POST['selected'] as $pages_id['pages_id'] ) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_pages_manager
                                          where pages_id = :pages_id 
                                        ');
          $Qdelete->bindInt(':pages_id',  (int)$pages_id['pages_id']);
          $Qdelete->execute();


          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_pages_manager_description
                                          where pages_id = :pages_id 
                                        ');
          $Qdelete->bindInt(':pages_id',  (int)$pages_id['pages_id']);
          $Qdelete->execute();
         }
       }


      osc_redirect(osc_href_link('page_manager.php', 'page=' . $_GET['page']));
      break;
    }
  }

  require('includes/header.php');
?>
<script type="text/javascript"><!--
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<!-- ########## Script pour autoriser l'ecriture des liens ########## //-->
<script type="text/javascript">
  function disableIt(a){
    document.getElementById(a).disabled=true;
  }

  function enableIt(a){
    document.getElementById(a).disabled=false;
  }
</script>
<?php
  if ( ($action == 'new') || ($action == 'edit') ) {
/*
      $language_query = osc_db_query("select code 
                                      from languages
                                      where languages_id = '" . $_SESSION['languages_id'] ."' 
                                    ");
      $languages = osc_db_fetch_array($language_query);
      $languages_code = $languages['code'];
      $uidatepicker = 'ui.datepicker-'.$languages['code'].'.js';
*/
?>
  <script type="text/javascript">
  $(function() {
    $('#datepicker').datepicker({
      changeMonth: true,
      changeYear: true
    });
  });

  $(function() {
    $('#datepicker_expire').datepicker({
      changeMonth: true,
      changeYear: true
    });
  });
  </script>
  <script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>
<!-- header_eof //-->
<?php
  }
?>
<!-- header_eof //-->
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top">
<!-- //######################################################################################################## //-->
<!-- //                                                NOUVELLE PAGE                                            //-->
<!-- //######################################################################################################## //-->
<?php
  if ($action == 'new') {
    $form_action = 'edit';
     
// permet de faire aparaitre le menu pour la page d'information si id=4
// Controle si une page contact existe deje afin de la proposer dans le menu deroulant des nouvelles pages
    $page_type_contact = 'true';

    $Qpages = $OSCOM_PDO->prepare('select page_type
                                   from :table_pages_manager
                                   where page_type = 3
                                  ');

    $Qpages->execute();

    if ($Qpages->fetch() !== false) {
      while ($pages = $Qpages->fetch() ) {
        if ($pages['page_type'] == '3') {
          $page_type_contact = 'false';
        }
      }
    }

// Type de la page demande dans le menu deroulant a la creation d'une nouvelle page
    $page_type_statut = array();
    $page_type_statut[] = array('id' => '1',
                                'text' => PAGE_MANAGER_INTRODUCTION_PAGE);
    $page_type_statut[] = array('id' => '2',
                                'text' => PAGE_MANAGER_MAIN_PAGE);
    if ($page_type_contact == 'true') {
      $page_type_statut[] = array('id' => '3',
                                  'text' => PAGE_MANAGER_CONTACT_US);
    }
    $page_type_statut[] = array('id' => '4',
                                'text' => PAGE_MANAGER_INFORMATIONS);

// Type de la page demande dans le menu deroulant pour le choix d'affichage du text dans une des 2 boxes
    $page_box_statut = array();
    $page_box_statut[] = array('id' => '0',
                                'text' => PAGE_MANAGER_MAIN_BOX);
    $page_box_statut[] = array('id' => '1',
                                'text' => PAGE_MANAGER_SECONDARY_BOX);

    $page_box_statut[] = array('id' => '2',
                                'text' => PAGE_MANAGER_NO_APPEAR);

// Type de lien demande dans le menu deroulant
    $links_target_statut = array();
    $links_target_statut[] = array('id' => '_self',
                                   'text' => TEXT_LINKS_SAME_WINDOWS);
    $links_target_statut[] = array('id' => '_blank',
                                   'text' => TEXT_LINKS_NEW_WINDOWS);

// Select if the page is general condition page or or not
    $page_general_condition_statut = array();
    $page_general_condition_statut[] = array('id' => '0',
                                             'text' => PAGE_MANAGER_TEXT_NO);
    $page_general_condition_statut[] = array('id' => '1',
                                             'text' => PAGE_MANAGER_TEXT_YES);

?>
   <table border="0" width="100%" cellspacing="0" cellpadding="2">
     <tr>
       <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
       <div class="adminTitle">
         <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/page_manager.gif', HEADING_TITLE, '40', '40'); ?></span>
         <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_NEW; ?></span>
         <span class="pull-right">
           <?php echo osc_draw_form('page_manager', 'page_manager.php', osc_get_all_get_params(array('action', 'info', 'sID')) . 'action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?>
         </span>
         <span class="pull-right"><?php echo (($form_action == 'insert') ? osc_image_submit('button_update.gif', IMAGE_INSERT) : osc_image_submit('button_update.gif', IMAGE_UPDATE)); ?>&nbsp;</span>
         <span class="pull-right">
           <?php echo '<a href="' . osc_href_link('page_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . (!empty($bID) and $bID != "" ? 'bID=' . $bID : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>&nbsp;'; ?>
         </span>
       </div>
       <div class="clearfix"></div>
       <div style="padding-top:40px;></div>
      </tr>

      <tr>
       <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
         <tr>
          <td class="mainTitle" colspan="6"><?php echo TITLE_PAGES_CHOOSE; ?></td>
         </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
         <tr>
          <td><table border="0" cellpadding="2" cellspacing="2">
            <tr>
             <td class="main"><?php echo TEXT_PAGES_CHOOSE; ?></td>
             <td class="main"><?php echo osc_draw_pull_down_menu('page_type', $page_type_statut); ?></td>
            </tr>
           </table></td>
         </tr>
        </table></form></td>
      </tr>
     </table>
<!-- //######################################################################################################## //-->
<!-- //                                             EDITION D'UNE PAGE                                          //-->
<!-- //######################################################################################################## //-->
<?php
  } else if ($action == 'edit') {
    $form_action = 'insert';

    $parameters = array('pages_title'       => '',
                        'page_time'         => '',
                        'page_date_start'   => '',
                        'page_date_closed'  => '',
                        'pages_html_text'   => '',
                        'sort_order'        => '',
                        'customers_group_id' => '',
                        'status'            => '',
                        'page_manager_head_title_tag' => '',
                        'page_manager_head_keywords_tag' => '',
                        'page_manager_head_desc_tag' => ''
                       );
 
    $bInfo = new objectInfo($parameters);

    if (isset($_GET['bID'])) {
      $form_action = 'update';

      $bID = osc_db_prepare_input($_GET['bID']);


      $Qpage = $OSCOM_PDO->prepare('select s.status,
                                           s.links_target,
                                           s.customers_group_id,
                                           s.sort_order,
                                           s.page_type,
                                           s.page_box,
                                           p.pages_title,
                                           p.pages_html_text,
                                           p.externallink,
                                           s.page_time,
                                           p.language_id,
                                           date_format(s.page_date_start, "%d/%m/%Y") as page_date_start,
                                           date_format(s.page_date_closed, "%d/%m/%Y") as page_date_closed,
                                           s.page_general_condition,
                                           p.page_manager_head_title_tag,
                                           p.page_manager_head_keywords_tag,
                                           p.page_manager_head_desc_tag
                                    from :table_pages_manager s left join :table_pages_manager_description p on s.pages_id = p.pages_id
                                    where s.pages_id = :pages_id
                                   ');
      $Qpage->bindint(':pages_id', (int)$bID );
      $Qpage->execute();

        while($page = $Qpage->fetch() )  {
          $languageid = $page['language_id'];
          $customers_group_id = $page['customers_group_id'];
          $page_type = $page['page_type'];
          $links_target = $page['links_target'];
          $page_box = $page['page_box'];
          $pagetitle[$languageid] = $page['pages_title'];
          $sortorder = $page['sort_order'];
          $page_time = $page['page_time'];
          $page_date_start = $page['page_date_start'];
          $page_date_closed = $page['page_date_closed'];
          $pages_html_text[$languageid] = $page['pages_html_text'];
          $externallink[$languageid] = $page['externallink'];
          $page_general_condition = $page['page_general_condition'];
          $page_manager_head_title_tag[$languageid] = $page['page_manager_head_title_tag'];
          $page_manager_head_keywords_tag[$languageid] = $page['page_manager_head_title_tag'];
          $page_manager_head_desc_tag[$languageid] = $page['page_manager_head_title_tag'];
        }
      } elseif (osc_not_null($_POST)) {
         $bInfo->objectInfo($_POST);
      }

      $bIDif = "";

      if(!empty($bID) && $bID != "")  {
        $bIDif='&bID='.$bID;
      }

      $form_action = 'insert';

//transmission de la valeur $page_type
// si >0 transmission de la valeur : cas insertion d'une nouvelle information
  // Si <0 rin cas de l'edition d'une page information
      if (osc_db_prepare_input($_POST['page_type']) != 0) {
        $page_type = osc_db_prepare_input($_POST['page_type']);
      }

      if ( ($action == 'edit') && isset($_GET['bID']) ) {
        $form_action = 'update';
      }

  // Type de la page demandee dans le menu deroulant
      if ($page_type == '1') { 
        $page_manager_introduction_page = 'false';
      } else { 
        $page_manager_introduction_page = 'true'; 
      }

      if ($page_type == '2') { 
        $page_manager_main_page = 'false'; 
      } else { 
        $page_manager_main_page = 'true';
      }

      if ($page_type == '3') { 
        $page_manager_contact_us = 'false'; 
      } else { 
        $page_manager_contact_us = 'true'; 
      }
      
      if ($page_type == '4') { 
        $page_manager_informations = 'false'; 
      } else { 
        $page_manager_informations = 'true'; 
      }


      $page_type_statut = array();
      $page_type_statut[] = array('id' => '1',
                                  'text' => PAGE_MANAGER_INTRODUCTION_PAGE,
                                  'disabled' => $page_manager_introduction_page);
      $page_type_statut[] = array('id' => '2',
                                  'text' => PAGE_MANAGER_MAIN_PAGE,
                                  'disabled' => $page_manager_main_page);
      $page_type_statut[] = array('id' => '3',
                                  'text' => PAGE_MANAGER_CONTACT_US,
                                  'disabled' => $page_manager_contact_us);
      $page_type_statut[] = array('id' => '4',
                                  'text' => PAGE_MANAGER_INFORMATIONS,
                                  'disabled' => $page_manager_informations);

      $page_box_statut = array();
      $page_box_statut[] = array('id' => '0',
                                  'text' => PAGE_MANAGER_MAIN_BOX);
      $page_box_statut[] = array('id' => '1',
                                  'text' => PAGE_MANAGER_SECONDARY_BOX);
      $page_box_statut[] = array('id' => '2',
                                 'text' => PAGE_MANAGER_NO_APPEAR);


  // Type de lien demande dans le menu deroulant
      $links_target_statut = array();
      $links_target_statut[] = array('id' => '_self',
                                     'text' => TEXT_LINKS_SAME_WINDOWS);
      $links_target_statut[] = array('id' => '_blank',
                                     'text' => TEXT_LINKS_NEW_WINDOWS);

// seclect if the page is general condition or not
      $page_general_condition_statut = array();
      $page_general_condition_statut[] = array('id' => '0',
                                               'text' => PAGE_MANAGER_TEXT_NO);
      $page_general_condition_statut[] = array('id' => '1',
                                               'text' => PAGE_MANAGER_TEXT_YES);

// Put languages information into an array for drop-down boxes
    $customers_group = osc_get_customers_group();

    $customers_group[] = array('id'=>'99', 'text'=> TEXT_ALL_GROUPS);
    $values_customers_group_id[0] = array('id'=>0, 'text'=> TEXT_ALL_CUSTOMERS);

    for ($i=0, $n=sizeof($customers_group); $i<$n; $i++) {
      $values_customers_group_id[$i+1] = array ('id' =>$customers_group[$i]['id'],
                                                'text' =>$customers_group[$i]['text']);
    }

?>


    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <div class="adminTitle">
      <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/page_manager.gif', HEADING_TITLE, '40', '40'); ?></span>
      <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_EDITION; ?></span>
<?php
  echo osc_draw_form('page_manager', 'page_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'action=' . $form_action, 'post', 'enctype="multipart/form-data"');
  if ($form_action == 'update') echo osc_draw_hidden_field('pages_id', $bID);
?>
      </span>
      <span class="col-md-3 pull-right">
        <span class="pull-right">
          <?php echo (($form_action == 'insert') ? osc_image_submit('button_update.gif', IMAGE_INSERT) : osc_image_submit('button_update.gif', IMAGE_UPDATE)); ?>&nbsp;
        </span>
        <span class="pull-right">
         <?php echo '<a href="' . osc_href_link('page_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . (!empty($bID) and $bID != "" ? 'bID=' . $bID : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;
        </span>
      </span>
    </div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

   <table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
    if ($page_error == true) {
?>
    <tr>
     <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
    </tr>
    <tr>
     <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="messageStackError">
       <tr>
        <td class="messageStackError" height="20" colspan="2"><table width="100%">
          <tr>
           <td width="27" align="center"><?php echo osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING); ?></td>
           <td><?php echo WARNING_EDIT_CUSTOMERS; ?></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
<?php
    }
?>
    <tr>
      <td>
        <div>
<?php
  if ($page_type == '4') {
?>
          <ul class="nav nav-tabs" role="tablist"  id="myTab">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
            <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_PAGE_LINK; ?></a></li>
            <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_PAGE_DESCRIPTION; ?></a></li>
            <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_PAGE_META_TAG; ?></a></li>
          </ul>
<?php
  } elseif ($page_type == '3') {
?>
          <ul class="nav nav-tabs" role="tablist"  id="myTab">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
            <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_PAGE_DESCRIPTION; ?></a></li>
            <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_PAGE_META_TAG; ?></a></li>
          </ul>
<?php
  } else {
?>
          <ul class="nav nav-tabs" role="tablist"  id="myTab">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
            <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_PAGE_DESCRIPTION; ?></a></li>
          </ul>
<?php
  }
?>
         <div class="tabsClicShopping">
           <div class="tab-content">
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General                          //-->
<!-- ------------------------------------------------------------ //-->
             <div class="tab-pane active" id="tab1">
               <table width="100%" border="0" cellspacing="0" cellpadding="5">
                 <tr>
                   <td class="mainTitle"><?php echo TITLE_NAME_PAGE; ?></td>
                 </tr>
               </table>
               <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      if ($page_error == 'true') {
        if ($languages_title_error == $languages[$i]['id']) {
?>
                    <tr>
                      <td class="main"><?php echo osc_draw_input_field('pages_title_'.$languages[$i]['id'], $pagetitle[$languages[$i]['id']], 'required aria-required="true" id="title" maxlength="64" style="border: 2px solid #FF0000"', false); ?></td>
                      <td class="main" valign="middle"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                      <td class="main"><?php echo ERROR_PAGE_TITLE_REQUIRED_CURSEUR; ?></td>
                    </tr>
<?php
        } else {
?>
                    <tr>
                      <td class="main"><?php echo osc_draw_input_field('pages_title_'.$languages[$i]['id'], $pagetitle[$languages[$i]['id']], 'required aria-required="true"', false); ?></td>
                      <td class="main" valign="middle"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '1'); ?></td>
                    </tr>
<?php
           }
      } else {
?>
                    <tr>
                      <td class="main"><?php echo osc_draw_input_field('pages_title_'.$languages[$i]['id'], $pagetitle[$languages[$i]['id']], 'required aria-required="true"', false); ?></td>
                      <td class="main" valign="middle"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                      <td class="main"><?php echo TEXT_FIELD_REQUIRED; ?></td>
                    </tr>
<?php
      }
    }
?>
                   </table></td>
                 </tr>
               </table>
<!-- Type de la page //-->
               <table width="100%" border="0" cellspacing="0" cellpadding="5">
                 <tr>
                   <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                 </tr>
                 <tr>
                  <td class="mainTitle"><?php echo TITLE_PAGES_TYPE; ?></td>
                </tr>
               </table>
               <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                 <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo TEXT_PAGES_TYPE; ?></td>
                      <td class="main"><?php echo osc_draw_pull_down_menu('page_type', $page_type_statut, $page_type); ?></td>
                    </tr>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
                    <tr>
                      <td class="main" valign="top"><?php echo TEXT_PAGE_MANAGER_CUSTOMERS_GROUP; ?></td>
                      <td class="main" align="left"><?php echo  osc_draw_pull_down_menu('customers_group_id', $values_customers_group_id,  $customers_group_id); ?></td>
                    </tr>
<?php
  }
    if ($page_type == '4' || $page_type == '3') { 
?>
                    <tr>
                      <td class="main"><?php echo TEXT_PAGES_BOX; ?></td>
                      <td class="main"><?php echo osc_draw_pull_down_menu('page_box', $page_box_statut, $page_box); ?></td>
                    </tr>
<?php
   }
   if ($page_type == '4') {
?>
                    <tr>
                      <td class="main"><?php echo TEXT_PAGES_GENERAL_CONDITIONS; ?></td>
                      <td class="main"><?php echo osc_draw_pull_down_menu('page_general_condition', $page_general_condition_statut, $page_general_condition); ?></td>
                    </tr>
<?php
   }
?>   
                   </table></td>
                 </tr>
                </table>
 <!-- Date de demarrage et arret automatique de la page //-->
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_PAGES_DATE; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                       <td class="main"><?php echo TEXT_PAGES_DATE_START; ?><br /><small>(dd/mm/yyyy)</small></td>
                       <td valign="top" class="main"><?php echo osc_draw_input_field('page_date_start', $page_date_start, 'id="datepicker"'); ?></td>
                      </tr>
                      <tr>
                       <td valign="top" class="main"><?php echo TEXT_PAGES_DATE_CLOSED; ?><br /><small>(dd/mm/yyyy)</small></td>
                       <td class="main"><?php echo osc_draw_input_field('page_date_closed', $page_date_closed, 'id="datepicker_expire"'); ?></td>
                      </tr>
                    </table></td>
                   </tr>
                </table>
 <!-- Ordre de trie et temps d'affichage de la page d'introduction //-->
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                  <tr>
                    <td class="mainTitle" colspan="6"><?php echo TITLE_DIVERS; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                   <td><table border="0" cellpadding="2" cellspacing="2">
<?php 
    if ($page_type == '1') {
?>
                     <tr>
                       <td class="main"><?php echo TEXT_PAGES_TIME; ?></td>
                       <td class="main"><?php echo osc_draw_input_field('page_time', $page_time, '', false) . '<i>&nbsp;' . TEXT_PAGES_TIME_SECONDE; ?></td>
                     </tr>
<?php 
    }
?>
                     <tr>
                       <td class="main"><?php echo TEXT_PAGES_SORT_ORDER; ?></td>
                       <td class="main"><?php echo osc_draw_input_field('sort_order', $sortorder, '', false); ?></td>
                     </tr>
                   </table></td>
                  </tr>
                </table>
<!-- Aide //-->
<?php 
    if ($page_type == '1') {
?>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_IMAGE); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_PAGE_MANAGER; ?></strong></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo TEXT_PAGES_TYPE_INFORMATION; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
<?php 
  }
?>
              </div>
 <!-- ------------------------------------------------------------ //-->
 <!--               ONGLET Type de lien sur la page		          //-->
 <!-- ------------------------------------------------------------ //-->
<?php
    if ($page_type == '4') {
?>
              <div class="tab-pane" id="tab2">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_LINK; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <tr>
                         <td class="main" valign="top"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                         <td><table border="0" cellpadding="2" cellspacing="2" class="adminform">
                           <tr>
                             <td class="main"><?php echo TEXT_PAGES_EXTERNAL_LINK; ?></td>
                             <td class="main"><?php echo osc_draw_input_field('externallink_'.$languages[$i]['id'], $externallink[$languages[$i]['id']], '', false); ?></td>
                           </tr>
                         </table></td>
                      </tr>
<?php
    }
?>
                      <tr>
                        <td class="main"><?php echo TEXT_PAGES_INTEXT; ?></td>
                        <td class="main"><?php echo osc_draw_pull_down_menu('links_target', $links_target_statut, $links_target); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<?php
    }
?>
<!-- //################################################################################################################ -->
<!--               ONGLET Information description		          //-->
<!-- //################################################################################################################ -->
              <div class="tab-pane" id="tab3">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TEXT_PAGES_INFORMATION_DESCRIPTION; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <tr>
                        <td class="main" valign="top"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                        <td align="left" class="main"><div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('pages_html_text_'.$languages[$i]['id'], 'soft', '750', '300', str_replace('& ', '&amp; ', trim($pages_html_text[$languages[$i]['id']]))); ?></div></td>
                      </tr>
<?php
   }
?>
                   </table></td>
                 </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_DESCRIPTION); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_DESCRIPTION; ?></strong></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo HELP_DESCRIPTION; ?>
                          <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg1"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                          <div class="modal fade" id="myModalWysiwyg1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                                </div>
                                <div class="modal-body" style="text-align:center;">
                                  <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                     </table></td>
                  </tr>
                </table>
              </div>

<!-- //################################################################################################################ -->
<!--               ONGLET Information seo		          //-->
<!-- //################################################################################################################ -->
<?php
   if ($page_type == '1' || $page_type == '3' || $page_type == '4') {
?>
              <div class="tab-pane" id="tab4">
                 <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                     <td class="mainTitle"><?php echo TEXT_PRODUCTS_PAGE_REFEFRENCEMENT; ?></td>
                   </tr>
                 </table>
                 <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                   <tr>
                     <td colspan="3"><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                   </tr>
                   <tr>
                     <td class="text" align="right" ><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></td>
                     <td class="text" align="center"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></td>
                     <td></td>
                   </tr>
                   <tr>
                     <td colspan="2">
<!-- decompte caracteres -->
<script type="text/javascript">
 $(document).ready(function(){
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
   //default title
   $("#default_title_<?php echo $i?>").charCount({
     allowed: 70,
     warning: 20,
     counterText: ' Max : '
   });

   //default_description
   $("#default_description_<?php echo $i?>").charCount({
     allowed: 150,
     warning: 20,
     counterText: 'Max : '
   });

   //default tag product
   $("#default_tag_product_<?php echo $i?>").charCount({
     allowed: 100,
     warning: 70,
     counterText: ' Max : '
   });

   //default tag
   $("#default_tag_blog_<?php echo $i?>").charCount({
     allowed: 100,
     warning: 70,
     counterText: ' Max : '
   });
<?php
    }
?>
 });
</script>

<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                       <tr>
                         <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                       </tr>
                       <tr>
                         <td class="main" valign="top"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']). '&nbsp '. TEXT_PRODUCTS_PAGE_TITLE; ?></td>
                         <td class="main"><?php echo osc_draw_input_field('page_manager_head_title_tag_'.$languages[$i]['id'], (($page_manager_head_title_tag[$languages[$i]['id']]) ? $page_manager_head_title_tag[$languages[$i]['id']] : osc_get_page_manager_head_title_tag($bInfo->page_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?>&nbsp;
                       </tr>
                       <tr>
                         <td class="main" valign="top"><?php echo TEXT_PRODUCTS_KEYWORDS; ?></td>
                         <td class="main"><?php echo osc_draw_textarea_field('page_manager_head_keywords_tag_'.$languages[$i]['id'], 'soft', '75', '5', (isset($page_manager_head_keywords_tag[$languages[$i]['id']]) ? $page_manager_head_keywords_tag[$languages[$i]['id']] : osc_get_page_manager_head_keywords_tag($bInfo->page_id, $languages[$i]['id']))); ?>&nbsp;
                       </td>
                       </tr>
                       <tr>
                         <td class="main" valign="top"><?php echo TEXT_PRODUCTS_HEADER_DESCRIPTION; ?></td>
                         <td class="main"><?php echo osc_draw_textarea_field('page_manager_head_desc_tag_'.$languages[$i]['id'], 'soft', '75', '2', (isset($page_manager_head_desc_tag[$languages[$i]['id']]) ? $page_manager_head_desc_tag[$languages[$i]['id']] : osc_get_page_manager_head_desc_tag($bInfo->page_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></td>
                       </tr>
<?php
   }
?>
                      </td>
                    </tr>
                 </table>
                 <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                     <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                   </tr>
                 </table>
                 <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                   <tr>
                     <td><table border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?></td>
                         <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong></td>
                       </tr>
                     </table></td>
                   </tr>
                   <tr>
                     <td><table border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                         <td class="main"><?php echo HELP_SUBMIT; ?></td>
                       </tr>
                     </table></td>
                   </tr>
                 </table>
               </div>
             </div>
           </div>
         </div>
<?php
  }
?>
   </form>

<?php

  } else {

//######################################################################################################## //-->
//                                             LISTE DES PAGES                                             //-->
//########################################################################################################
    $pages_query_raw = "select p.pages_id,
                               p.links_target,
                               p.page_type,
                               p.page_box,
                               p.status,
                               p.sort_order,
                               p.date_added,
                               p.page_date_start,
                               p.page_date_closed,
                               p.last_modified,
                               p.date_status_change,
                               s.pages_title,
                               s.externallink,
                               p.customers_group_id,
                               p.page_general_condition
                        from pages_manager p,
                             pages_manager_description s
                        where s.language_id='" . (int)$_SESSION['languages_id'] . "'
                        and p.pages_id = s.pages_id
                        order by  p.pages_id
                       ";
  
    $pages_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $pages_query_raw, $pages_query_numrows);    
    $pages_query = osc_db_query($pages_query_raw);
?>
   <table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
      <div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        <div class="adminTitle">
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/page_manager.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
          <span class="col-md-4 smallText" style="text-align: center;">
<?php  echo $pages_split->display_count($pages_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PAGES); ?>
<?php echo $pages_split->display_links($pages_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
          </span>

          <span class="pull-right">
            <?php echo '<a href="' . osc_href_link('page_manager.php', 'action=new') . '">' . osc_image_button('button_new_page_manager.gif', IMAGE_NEW_PAGE) . '</a>'; ?>
          </span>
          <span class="pull-right">
            <form name="delete_all" <?php echo 'action="' . osc_href_link('page_manager.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
            <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
          </span>
        </div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      </div>
      <div class="clearfix"></div>
    </tr>
    <tr>
     <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr class="dataTableHeadingRow">
           <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
           <td class="dataTableHeadingContent"><?php echo 'Id'; ?></td>
           <td class="dataTableHeadingContent"></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PAGES; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TYPE_PAGE; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_GROUP; ?></td>
<?php
  }
?>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LINKS_TARGET; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PAGE_TYPE; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SORT_ORDER; ?></td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?></td>
          </tr>
          <tr>
<?php 	
    while ($pages = osc_db_fetch_array($pages_query)) {

// Permettre l'affichage des groupes en mode B2B
     if (MODE_B2B_B2C == 'true') {

        $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name
                                                from :table_customers_groups
                                                where customers_group_id = :customers_group_id
                                              ');
        $QcustomersGroup->bindInt(':customers_group_id',  (int)$pages['customers_group_id']);
        $QcustomersGroup->execute();

        $customers_group = $QcustomersGroup->fetch();

        if ($pages['customers_group_id'] == '99') {
          $customers_group['customers_group_name'] =  TEXT_ALL_GROUPS;
        } elseif ($pages['customers_group_id'] == '0') {
            $customers_group['customers_group_name'] =  NORMAL_CUSTOMER;
        }
      }

      if ((!isset($_GET['bID']) || (isset($_GET['bID']) && ($_GET['bID'] == $pages['pages_id']))) && !isset($bInfo) && (substr($action, 0, 3) != 'new')) {
        $bInfo_array = array_merge((array)$pages,(array)$info);
        $bInfo = new objectInfo($bInfo_array);
      }

      if (isset($bInfo) && is_object($bInfo) && ($pages['pages_id'] == $bInfo->pages_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                <td>
<?php 
      if ($pages['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $pages['pages_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $pages['pages_id']; ?>" />
<?php 
      } 
?>
           <td class="dataTableContent"><?php echo $pages['pages_id']; ?></td>

<?php
  if ( osc_not_null($pages['pages_id']) && $pages['page_type'] == 4 && empty($pages['externallink']))  {
?>
           <td class="dataTableContent" width="20px;"><?php echo '<a href="' . DIR_WS_CATALOG . 'page_manager.php' .'?pages_id=' . $pages['pages_id'], '" target="_blank">' . osc_image(DIR_WS_ICONS . 'preview_catalog.png', ICON_PREVIEW_CATALOG) . '</a>'; ?></td>
<?php
  } else {
 ?>
    <td class="dataTableContent"></td>
<?php
  }
?>
           <td class="dataTableContent"><?php echo $pages['pages_title']; ?></td>
<?php
      if ($pages['page_type'] =='1'){
        $page_function = PAGE_MANAGER_INTRODUCTION_PAGE;
      } elseif ($pages['page_type'] =='2') {
        $page_function = PAGE_MANAGER_MAIN_PAGE;
      } elseif ($pages['page_type'] == '3') {
        $page_function = PAGE_MANAGER_CONTACT_US;
      } else {
        $page_function = PAGE_MANAGER_INFORMATIONS;
      }
?>
           <td class="dataTableContent" align="left"><?php echo $page_function; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
           <td class="dataTableContent" align="center"><?php echo $customers_group['customers_group_name']; ?></td>
<?php
  }
?>
           <td class="dataTableContent" align="center">
<?php
      if ($pages['status'] == '1') {
        echo '<a href="' . osc_href_link('page_manager.php', 'action=setflag&flag=0&id=' . $pages['pages_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('page_manager.php', 'action=setflag&flag=1&id=' . $pages['pages_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
           </td>
<?php
  if ($pages['links_target'] == '_self') {
    $links_target =  TEXT_LINKS_SAME_WINDOWS;
  } elseif ($pages['links_target'] == '_blank') {
    $links_target =  TEXT_LINKS_NEW_WINDOWS;
  } else {
    $links_target =  '';
  }
?>
         <td class="dataTableContent" align="center"><?php echo $links_target; ?></td>
<?php
  if ($pages['page_box'] == '0') {
    $page_box =  PAGE_MANAGER_MAIN_BOX;
  } elseif ($pages['page_box'] == '1') {
    $page_box =  PAGE_MANAGER_SECONDARY_BOX;
  } else {
    $page_box =  PAGE_MANAGER_NO_APPEAR;
  }
?>
           <td class="dataTableContent" align="center"><?php echo $page_box; ?></td>
           <td class="dataTableContent" align="center"><?php echo $pages['sort_order']; ?></td>
           <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('page_manager.php', 'bID=' . $pages['pages_id']) . '&page=' . $_GET['page'] . '&action=edit">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
// suppression du bouton delete
  if (($pages['pages_id'] == '4') || ($pages['pages_id'] == '5')) { 
     echo osc_draw_separator('pixel_trans.gif', '16', '16');
  } else {
      echo '<a href="' . osc_href_link('page_manager.php', 'bID=' . $pages['pages_id']) . '&page=' . $_GET['page'] . '&action=delete">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
  }
?>
           </td>
          </tr>
<?php
    }
?>
              </form><!-- end form delete all -->
              <tr>
                <td colspan="12" class="smallText" valign="top"><?php  echo $pages_split->display_count($pages_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PAGES); ?></td>
              </tr>
            </table></td>
<?php
  }
?>
       </tr>
      </table></td>
    </tr>
   </table></td>
 </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
