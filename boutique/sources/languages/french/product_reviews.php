<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Commentaires');
define('HEADING_TITLE', 'Commentaires clients ');
define('TEXT_CLICK_TO_ENLARGE', 'Cliquer pour agrandir');

define('TEXT_OF_5_STARS', '%s sur 5 étoiles !');
define('TABLE_HEADING_NEW_PRODUCTS','&nbsp;');
?>