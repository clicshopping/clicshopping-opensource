<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'My Account');
define('HEADING_TITLE', 'My Account Information');

define('OVERVIEW_TITLE', 'Overview');
define('OVERVIEW_SHOW_ALL_ORDERS', '(show all orders)');
define('OVERVIEW_PREVIOUS_ORDERS', 'Previous Orders');


define('MY_SUPPORT','Contat the customers support services');
define('MY_ORDERS_TITLE', 'My Orders');
define('MY_ORDERS_VIEW', 'View the orders I have made.');

//ONLY HERE

  if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_INFORMATION', 'Change my company information.');
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_ADDRESS_BOOK', 'Change the addresses of deliveries and contact commercial. ');
  } else {
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_INFORMATION', 'View or change my account information.');
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_ADDRESS_BOOK', 'View or change entries in my address book.');
  }


define('MIN_QTY_ORDER_PRODUCT','Qty; min : ');
?>

