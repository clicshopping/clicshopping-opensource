<?php
/*
 * stats_low_stock.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Products Stock Level');
define('TABLE_HEADING_PRODUCTS', 'Product Name');
define('TABLE_HEADING_MODEL', 'Product Model');
define('TABLE_HEADING_QTY_LEFT', 'Quantity available');
define('TABLE_HEADING_ACTION','Action');
define('TABLE_HEADING_WAHREHOUSE_TIME_REPLENISHMENT','Time replenishment');
define('TABLE_HEADING_WHAREHOUSE','Storage');
define('TABLE_HEADING_WHAREHOUSE_ROW','Wharehouse row:');
define('TABLE_HEADING_WHAREHOUSE_LEVEL','Location level:');


?>