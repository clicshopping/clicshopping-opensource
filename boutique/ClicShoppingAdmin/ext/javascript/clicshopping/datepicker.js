  $(function(){

/* categories.php */
    $('#products_date_available').datepicker({
      format: 'yyyy-mm-dd'
    });


    /* categories.php */
    $('#schdate').datepicker({
      format: 'yyyy-mm-dd'
    });

    /* categories.php */
    $('#expdate').datepicker({
      format: 'yyyy-mm-dd'
    });


// disabling dates
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

/* categories.php */
    var checkin = $('#products_date_available').datepicker({
      onRender: function(date) {
      return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
    }).on('changeDate', function(ev) {
      if (ev.date.valueOf() > checkout.date.valueOf()) {
      var newDate = new Date(ev.date)
      newDate.setDate(newDate.getDate() + 1);
      checkout.setValue(newDate);
      }
    checkin.hide();

    }).on('changeDate', function(ev) {
      checkout.hide();
    }).data('datepicker');


/* products heart start date  && specials*/
    var checkin = $('#schdate').datepicker({
      onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
    }).on('changeDate', function(ev) {
      if (ev.date.valueOf() > checkout.date.valueOf()) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
      }
      checkin.hide();

    }).on('changeDate', function(ev) {
      checkout.hide();
    }).data('datepicker');


/* products heart end date && specials*/
    var checkin = $('#expdate').datepicker({
      onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
    }).on('changeDate', function(ev) {
      if (ev.date.valueOf() > checkout.date.valueOf()) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.setValue(newDate);
      }
      checkin.hide();

    }).on('changeDate', function(ev) {
      checkout.hide();
    }).data('datepicker');
  })

