<?php
/*
 * stats_products_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

  require('includes/application_top.php');
  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">

<?php
// show customers for a product
if ($_GET['action'] == 'show_customers' && (int)$_GET['pID']) {
  $products_id = (int)$_GET['pID'];
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
           <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/client.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
            <td align="right"><?php echo '<a href="' . osc_href_link('stats_products_notifications.php', osc_get_all_get_params(array('action', 'pID', 'page'))) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NUMBER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_EMAIL; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DATE; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php
  if ($_GET['page'] > 1) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN - MAX_DISPLAY_SEARCH_RESULTS_ADMIN;

    $customers_query_raw = "select c.customers_firstname, 
                                   c.customers_lastname, 
                                   c.customers_email_address, 
                                   pn.date_added
                            from customers c,
                                 products_notifications pn
                            where c.customers_id = pn.customers_id 
                            and pn.products_id = '" . $products_id . "' 
                            order by c.customers_firstname, 
                                     c.customers_lastname";

    $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $customers_query_raw, $customers_query_numrows);
    $customers_query = osc_db_query($customers_query_raw);
	
    while ($customers = osc_db_fetch_array($customers_query)) {
      $rows++;

      if (strlen($rows) < 2) {
        $rows = '0' . $rows;
      }
?>
              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                <td width="30" nowrap class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('customers.php', 'search=' . $customers['customers_lastname']) . '">' . $customers['customers_firstname'] . ' ' . $customers['customers_lastname'] . '</a>'; ?></td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('mail.php', 'selected_box=tools&customer=' . $customers['customers_email_address']) . '">' . $customers['customers_email_address'] . '</a>'; ?>&nbsp;</td>
                <td class="dataTableContent"><?php echo $OSCOM_Date->getLong($customers['date_added']); ?>&nbsp;</td>
                <td class="dataTableContent" align="right">
<?php
                    echo '<a href="' . osc_href_link('customers.php', 'search=' . $customers['customers_lastname']) . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_CUSTOMER) . '</a>';
                    echo osc_draw_separator('pixel_trans.gif', '6', '16');
                    echo '<a href="' . osc_href_link('mail.php', 'customer=' . $customers['customers_email_address']) . '">' . osc_image(DIR_WS_ICONS . 'email.gif', IMAGE_EMAIL) . '</a>';
?>
                </td>
              </tr>
<?php
    }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
                <td class="smallText" align="right"><?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], osc_get_all_get_params(array('page')), 'page'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
<?php
// default
} else {

  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN - MAX_DISPLAY_SEARCH_RESULTS_ADMIN;

  $rows = 0;
    $products_query_raw = "select count(pn.products_id) as count_notifications,
                                                pn.products_id, 
                                                pd.products_name, 
                                                p.products_image,
                                                p.products_model 
                                         from products_notifications pn,
                                              products_description pd,
                                              products p,
                                              customers c
                                         where pn.products_id = pd.products_id 
                                         and pd.language_id = '" . $_SESSION['languages_id'] . "' 
                                         and pn.customers_id = c.customers_id 
                                         and pn.products_id = p.products_id
                                         group by pn.products_id order by count_notifications desc, 
                                                  pn.products_id";
    // fix numrows
//    $products_count_query = osc_db_query($products_notifications_query_raw);

    $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_query_raw, $products_query_numrows);
?>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
           <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/categorie_produit.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading" width="250"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td class="smallText" valign="middle" align="center">
<?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?><br />
<?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="20"></td> 
                <td width="50"></td>  			  
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NUMBER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MODEL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_COUNT; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php
    $products_query = osc_db_query($products_query_raw);
//    $products_notifications_query_numrows = osc_db_num_rows($products_count_query);
	
  while ($products = osc_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                <td class="dataTableContent" width="50"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id'] . '&origin=' . 'stats_products_viewed.php' . '?page=' . $_GET['page']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('stats_products_notifications.php', 'action=show_customers&pID=' . $products['products_id'] . '&page=' . $page) . '">' . $products['products_name'] . '</a>'; ?></td>
                <td class="dataTableContent" align="center"><?php echo '<a href="' . osc_href_link('categories.php', 'pID=' . $products['products_id'] . '&action=new_product') . '">' . $products['products_model'] . '</a>'; ?>&nbsp;</td>
                <td class="dataTableContent" align="center"><?php echo $products['count_notifications']; ?>&nbsp;</td>
                <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('stats_products_notifications.php', 'action=show_customers&pID=' . $products['products_id'] . '&page=' . $page) . '">' . osc_image(DIR_WS_ICONS . 'client_b2b.gif', ICON_EDIT_CUSTOMER) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'pID=' . $products['products_id'] . '&action=new_product') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>';
?>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
<?php
} // end else
?>
		</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

