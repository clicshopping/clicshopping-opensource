<?php
/**
 * export_data_bd.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
// temps d'execution infini
// ne fonctionne pas sur tous les serveurs, dans ce cas il y aura des timeouts
// et les fichiers seront incomplets. Mieux vaut alors opter pour un hebergement plus performant.
  set_time_limit(0);
  require('includes/application_top.php');
  $output = '';

// securisation des variables et verification
//La ligne ci-apres permet de passer des frais de port fixe dans l'url
//$port = (isset($_GET['port']) && osc_not_null($_GET['port'])) ? osc_db_prepare_input($_GET['port']) : "-1";
  $verif = true;
  $pass = EXPORT_CODE;
  $language_code = (isset($_GET['language']) && osc_not_null($_GET['language'])) ? osc_db_prepare_input($_GET['language']) : DEFAULT_LANGUAGE;
  $p = osc_db_prepare_input($_GET['p']);
  $format = basename(osc_db_prepare_input($_GET['format']));
  $cache = osc_db_prepare_input($_GET['cache']);
  $fichier = osc_db_prepare_input($_GET['fichier']);
  $libre = osc_db_prepare_input($_GET['libre']);
  $rep = 'ext/export/';

// fonction de nettoyage des donnees si presence d'un editeur html
function netoyage_html($CatList, $length) {
  $CatList = html_entity_decode($CatList);
  $CatList = strip_tags ($CatList);
  $CatList = trim ($CatList);
  $CatList = strtolower ($CatList);
  $CatList = str_replace(chr(9),"",$CatList); 
  $CatList = str_replace(chr(10),"",$CatList);
  $CatList = str_replace(chr(13),"",$CatList);
  $CatList = str_replace('&nbsp;',' ',$CatList);
  $CatList = preg_replace("[<(.*?)>]","",$CatList);
  if (strlen($CatList) > $length) {
    $CatList = substr($CatList, 0, $length-3) . "...";
  }
  return $CatList;  
}


//On verifie le code avant de lancer les requetes
  if (($verif == true and $p==$pass) OR $verif == false ) {


//  ------------------------------------------ //
//  -- Categories with languague             -- //
//  ------------------------------------------ //
    $inc_cat = array();

    $QincludedCategories = $OSCOM_PDO->prepare('select c.categories_id,
                                                      c.parent_id,
                                                      cd.categories_name
                                               from :table_categories c,
                                                    :table_categories_description cd
                                               where c.categories_id = cd.categories_id
                                               and cd.language_id = :language_id
                                              ');
    $QincludedCategories->bindInt(':language_id', $_SESSION['languages_id']);
    $QincludedCategories->execute();

// Identification du nom de la categorie, et l'id de la categorie parent
    while ($included_categories = $QincludedCategories->fetch() ) {

      $inc_cat[] = array ('id' => $included_categories['categories_id'],
                          'parent' => $included_categories['parent_id'],
                          'name' => $included_categories['categories_name']
                          );
    }

  $cat_info = array();
  for ($i=0; $i<sizeof($inc_cat); $i++)

    $cat_info[$inc_cat[$i]['id']] = array ( 'parent'=> $inc_cat[$i]['parent'],
                                            'name'  => $inc_cat[$i]['name'],
                                            'path'  => $inc_cat[$i]['id'],
                                            'link'  => '' );

    for ($i=0; $i<sizeof($inc_cat); $i++) {
      $cat_id = $inc_cat[$i]['id'];

      while ($cat_info[$cat_id]['parent'] != 0){
        $cat_info[$inc_cat[$i]['id']]['path'] = $cat_info[$cat_id]['parent'] . '_' . $cat_info[$inc_cat[$i]['id']]['path'];
        $cat_id = $cat_info[$cat_id]['parent'];
      }

      $link_array = preg_split('#_#', $cat_info[$inc_cat[$i]['id']] ['path']);

      for ($j=0; $j<sizeof($link_array); $j++) {
        $cat_info[$inc_cat[$i]['id']]['link'] .= '&nbsp;<a href="' . osc_href_link('index.php', 'cPath=' . $cat_info[$link_array[$j]]['path']) . '"><nobr>' . $cat_info[$link_array[$j]]['name'] . '</nobr></a>&nbsp;&raquo;&nbsp;';
      }
    }


//  -------------------------------------------- //
//  -- products with language                 -- //
//  -------------------------------------------- //
    $product_num = 0;

if ($format =='product_all.php') {
  // Requete identifiant les produits disponibles dans le catalogue

    $Qproducts = $OSCOM_PDO->prepare('select p.*,
                                             pd.*,
                                             pc.categories_id
                                      from  :table_products p,
                                            :table_products_description pd,
                                            :table_products_to_categories pc
                                      where p.products_id = pd.products_id
                                      and p.products_id = pc.products_id
                                      and pd.language_id = :language_id
                                      order by p.products_id
                                     ');
    $Qproducts->bindInt(':language_id', $_SESSION['languages_id']);
    $Qproducts->execute();

    while($products = $Qproducts->fetch()) {

      if (intval($products['manufacturers_id']) > 0) {

        $Qmanufacturers = $OSCOM_PDO->prepare('select m.manufacturers_name,
                                                      m.manufacturers_image,
                                                      mi.manufacturers_url
                                               from :table_manufacturers  m,
                                                    :table_manufacturers_info mi
                                               where m.manufacturers_id = :manufacturers_id
                                               and mi.manufacturers_id = m.manufacturers_id
                                              ');
        $Qmanufacturers->bindInt(':manufacturers_id',(int)$products['manufacturers_id']);
        $Qmanufacturers->execute();

        $manufacturers_result = $Qmanufacturers->fetch();

        $products['manufacturers_name'] = $manufacturers_result['manufacturers_name'];
        $products['manufacturers_image'] = $manufacturers_result['manufacturers_image'];
        $products['manufacturers_url'] = $manufacturers_result['manufacturers_url'];
      }

      if (intval($products['suppliers_id']) > 0) {

        $Qsuppliers = $OSCOM_PDO->prepare('select s.*,
                                                  si.suppliers_url
                                           from :table_suppliers s,
                                                :table_suppliers_info si
                                           where s.suppliers_id = :suppliers_id
                                           and si.suppliers_id = s.suppliers_id
                                          ');
        $Qsuppliers->bindInt(':suppliers_id', (int)$products['suppliers_id']);
        $Qsuppliers->execute();

        $suppliers_result = $Qsuppliers->fetch();

        $products['suppliers_name'] = $suppliers_result['suppliers_name'];
        $products['suppliers_image'] = $manufacturers_result['suppliers_image'];
        $products['suppliers_url'] = $manufacturers_result['suppliers_url'];
        $products['suppliers_manager'] = $suppliers_result['suppliers_manager'];
        $products['suppliers_phone'] = $suppliers_result['suppliers_phone'];
        $products['suppliers_email_address'] = $suppliers_result['suppliers_email_address'];
        $products['suppliers_fax'] = $suppliers_result['suppliers_fax'];
        $products['suppliers_address'] = $suppliers_result['suppliers_address'];
        $products['suppliers_suburb'] = $suppliers_result['suppliers_suburb'];
        $products['suppliers_email_address'] = $suppliers_result['suppliers_email_address'];
        $products['suppliers_postcode'] = $suppliers_result['suppliers_postcode'];
        $products['suppliers_city'] = $suppliers_result['suppliers_city'];
        $products['suppliers_states'] = $suppliers_result['suppliers_states'];
        $products['suppliers_country_id'] = $suppliers_result['suppliers_country_id'];
        $products['suppliers_notes'] = $suppliers_result['suppliers_notes'];
      }

      $product_num++;
// On appelle le "plugin" definissant le format du fichier
      include(DIR_WS_MODULES . 'export/' . $format);
    }
  } elseif ($format =='product.php') {

    $Qproducts = $OSCOM_PDO->prepare('select p.*,
                                             pd.*,
                                             pc.categories_id
                                      from  :table_products p,
                                            :table_products_description pd,
                                            :table_products_to_categories pc
                                     where p.products_id = pd.products_id
                                     and p.products_id = pc.products_id
                                     and pd.language_id = :language_id
                                     order by p.products_id
                                     ');
    $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qproducts->execute();

    while($products = $Qproducts->fetch() ) {
      $product_num++;
      include(DIR_WS_MODULES . 'export/' . $format);
    }

  } elseif ($format =='options.php') {

  } elseif ($format =='products_extra_fields.php') {



  } elseif ($format =='manufacturers.php') {

    $Qmanufacturers = $OSCOM_PDO->prepare('select m.*,
                                                 mi.*
                                           from  :table_manufacturers m,
                                                :table_manufacturers_info mi
                                           where m.manufacturers_id = mi.manufacturers_id
                                           and mi.languages_id = :language_id
                                           order by m.manufacturers_id
                                          ');
    $Qmanufacturers->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qmanufacturers->execute();

    while($manufacturers =  $Qmanufacturers->fetch() ) {
      $product_num++;
      include(DIR_WS_MODULES . 'export/' . $format);
    }

  } elseif ($format =='suppliers.php') {

    $Qsuppliers = $OSCOM_PDO->prepare('select s.*,
                                               si.*
                                        from  :table_suppliers s,
                                              :table_suppliers_info si
                                       where s.suppliers_id = si.suppliers_id
                                       and si.languages_id = :language_id
                                       order by s.suppliers_id
                                       ');
    $Qsuppliers->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qsuppliers->execute();

    while($suppliers = $Qsuppliers->fetch() ) {
      $product_num++;
      include(DIR_WS_MODULES . 'export/' . $format);
    }


} elseif ($format == 'customers.php') {

  $Qcustomers = $OSCOM_PDO->prepare('select c.*,
                                            a.entry_company,
                                            a.entry_street_address,
                                            a.entry_suburb,
                                            a.entry_postcode,
                                            a.entry_city,
                                            a.entry_state,
                                            a.entry_zone_id,
                                            a.entry_country_id
                                      from :table_customers c left join :table_address_book a on c.customers_default_address_id = a.address_book_id
                                      where a.customers_id = c.customers_id
                                      ');
  $Qcustomers->execute();

    while($customers = $Qcustomers->fetch() ) {
      $product_num++;
      include(DIR_WS_MODULES . 'export/' . $format);
    }

} elseif ($format =='newsletter.php') {

    $Qnewsletter = $OSCOM_PDO->prepare('select customers_firstname,
                                              customers_lastname,
                                              customers_email_address
                                        from  :table_customers
                                        where customers_newsletter = :customers_newsletter
                                        and customers_group_id = :customers_group_id
                                        order by customers_lastname
                                       ');
    $Qnewsletter->bindValue(':customers_newsletter', 1);
    $Qnewsletter->bindValue(':customers_group_id', 0);
    $Qnewsletter->execute();

    while($newsletter = $Qnewsletter->fetch() ) {
      $product_num++;
      include(DIR_WS_MODULES . 'export/' . $format);
    }

} elseif ($format =='newsletter_no_account.php') {

    $QnewsletterNoAccount = $OSCOM_PDO->prepare('select customers_firstname,
                                                        customers_lastname,
                                                        customers_email_address
                                                 from  :table_newsletter_no_account
                                                 where customers_newsletter = :customers_newsletter
                                                 order by customers_lastname
                                               ');
    $QnewsletterNoAccount->bindValue(':customers_newsletter', 1);

    $QnewsletterNoAccount->execute();

    while($newsletter_no_account = $QnewsletterNoAccount->fetch() ) {
      $product_num++;
      include(DIR_WS_MODULES . 'export/' . $format);
    }
}  

    $content =   $head . $output . $foot;
//Soit on met en cache, soit on affiche le resulat
    if ($cache != "true") {
      Header( $header );
      if ($header2) Header( $header2 );
        echo $content;
      } else {
        $fp = fopen(DIR_FS_ADMIN . $rep . $fichier,"w");
        fputs($fp,"$content");
        fclose($fp);
?>
      <div class="contentext">
        <div style="text-align: center; padding-top:200px;">
          <p>Op&eacute;ration r&eacute;alis&eacute;e avec succ&egrave;s - Veuillez fermer cette page <br /></p>
        </div>
        <div style="text-align: center; padding-top:10px;"> Success Operation - Please close this page</div>
        <div style="text-align: center;  padding-top:10px">
          <br /><p>Votre fichier / See your file : <a href="<?php echo HTTP_SERVER . DIR_WS_ADMIN .'ext/export/'. $fichier; ?>"><?php echo $fichier; ?></a></p>
        </div>
        <div style="text-align: center;  padding-top:10px">
          <img src="<?php echo '../images/logo_clicshopping_1.png'; ?>" ></td>
        </div>
      </div>
<?php
      }  
    }
  require('includes/application_bottom.php');
