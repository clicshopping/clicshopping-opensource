<?php
/**
 * specials.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Specials products');

define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Products price');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_PRODUCTS_GROUP','Customer groups');
define('TABLE_HEADING_START_DATE','Start date');
define('TABLE_HEADING_EXPIRES_DATE','Expiry date');
define('TABLE_HEADING_ARCHIVE','Archives products');
define('TABLE_HEADING_MODEL','Models');
define('TABLE_HEADING_SCHEDULED_DATE','Start date');
define('TABLE_HEADING_PERCENTAGE','Pourcentage');
define('TABLE_HEADING_FLASH_DISCOUNT','Flash');

define('TEXT_SPECIALS_PRODUCT', 'Product:');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Special Price TAX exc.:');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Expiry date:');
define('TEXT_SPECIALS_GROUPS', 'Customer Group:');
define('TEXT_SPECIALS_FLASH_DISCOUNT', 'Is it a flash discount ?');

define('TEXT_INFO_DATE_ADDED', 'Date added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last modified:');
define('TEXT_INFO_NEW_PRICE', 'New price:');
define('TEXT_INFO_ORIGINAL_PRICE', 'Original price:');
define('TEXT_INFO_PERCENTAGE', 'Percentage:');
define('TEXT_INFO_START_DATE', 'Start at :');
define('TEXT_INFO_EXPIRES_DATE', 'Expires at:');
define('TEXT_INFO_STATUS_CHANGE', 'Status change:');

define('TEXT_NEW_SPECIALS_TWITTER','Specials new on  '. STORE_NAME  .' ! ' );
define('TEXT_FLASH_DISCOUNT_TWITTER','BECAREFULL : Flash Discount Now and not after ! ');

define('TEXT_SPECIALS_TWITTER','Do you want publish this special or the flash discount on Twitter ? ');
define('TEXT_YES','yes');
define('TEXT_NO','No');
define('TEXT_STATUS','Product status');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Delete special');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete the special products price?');
define('TEXT_SPECIALS_START_DATE', 'Start date');

define('TITLE_SPECIALS_GENERAL', 'Information on the product');
define('TITLE_SPECIALS_GROUPE', 'Price and customer group');
define('TITLE_SPECIALS_DATE', 'Display date');
define('TITLE_AIDE_SPECIALS_PRICE', 'Specials notes');

define('TEXT_AIDE_SPECIALS_PRICE', '<ul><li>You can insert a percentage in the Specials Price field, for example: <strong>20%</strong></li>
<li>If you enter a new price, the decimal separator must be a \'.\' (decimal-point), example: <strong>49.99</strong></li>
<li>Leave the expiry date empty for no expiration.</li><li>The start date has more rights that the expire date.</li>
<li>If you want initialize the dates, please change the status on the main page.</li>
<li>The twitter publication information is only allow on the product creation (with a status on it and a normal customer group). If your store name + URL of your product is more than 140 characters, the promotion will not be published (strong constraint)</li>
<li>The specials are automatically updated (the store catalog manages this element), it is unnecessary to change the status of the specials after registration.</li>
<li>The flash discount has a function expiry date and start date.</li>
</ul>');?>