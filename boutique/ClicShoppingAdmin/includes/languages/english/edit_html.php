<?php
  /*
  * edit_html.php
  * @copyright Copyright 2008 - http://www.e-imaginis.com
  * @copyright Portions Copyright 2003 osCommerce
  * @license GNU Public License V2.0
  * @version $Id:
  */

  define('HEADING_TITLE','HTML Editor');
  define('TITLE_HELP_EDIT_HTML_IMAGE','Help on HTML Editor');
  define('TITLE_HELP_EDIT_HTML','Help on HTML Editor');
  define('TEXT_HELP_EDIT_HTML','For security reasons, permissions are not allowed in the original html editor. It is important to understand to change file permissions, can cause security problem in your application. <br />
It is better to download yours files via FTP and to change them on your computer. <br />
In case you want to make modification via HTML Editor, you must follow this procedure. <br />
- Rights directories: chmod 755 <br />
- Rights Files: chmod 666 <br />
After the modifications, change your rights files in : chmod 444 <br />
- Please note,  the configuration of your server will impact on yours authorisations to modify the files or not. We encourage you to visit <a href="http://clicshopping.org/marketplace/blog.php" target="_blank"> our guide </a>
for further explanation.');

