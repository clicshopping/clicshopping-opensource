<?php
/*
 * general_b2b.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

/**
 * Name of the customer group in B2B
 * 
 * @param string $customers_group_id
 * @return string $customers_group['customers_group_name'], the name of the group
 * @access public
 */
  function osc_get_customers_group_name($customers_group_id) {
    global $OSCOM_PDO;
    
    $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name
                                           from :table_customers_groups
                                           where customers_group_id = :customers_group_id
                                          ');
    $QcustomersGroup->bindInt(':customers_group_id', (int)$customers_group_id);

    $QcustomersGroup->execute();

    if ($QcustomersGroup->fetch() === false) {
      return $customers_group_id;
    } else {
      return $QcustomersGroup->value('customers_group_name');
    }
  }
  
// Returns an array with customers_groups
// TABLES: customers_groups
  function osc_get_customers_group($default = '') {
    global $OSCOM_PDO;
    
    $customers_group_array = array();
    if ($default) {
      $customers_group_array[] = array('id' => '',
                                       'text' => $default);
    }

    $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_id,
                                                   customers_group_name
                                             from :table_customers_groups
                                             order by customers_group_name
                                          ');

    $QcustomersGroup->execute();

    while ($customers_group = $QcustomersGroup->fetch() ) {
      $customers_group_array[] = array('id' => $customers_group['customers_group_id'],
                                       'text' => $customers_group['customers_group_name']);
    }

    return $customers_group_array;
  }
  
// Pull drop down customer_group_id
  function osc_cfg_pull_down_customers_group_list($customers_group_id) {
    return osc_draw_pull_down_menu('configuration_value', osc_get_customers_group(), $customers_group_id);
  }
  


/**
 * Checbox concerning the payment module
 * 
 * @param string customers_group_id
 * @return string payment_list, the list of the payment module
 * @access public
 */
  function osc_draw_checkbox_field_payment_list($customers_group_id) {
    global $OSCOM_PDO;
    
// Recherche des modules de paiement en production
    $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
    $module_key = 'MODULE_PAYMENT_INSTALLED';
    $module_key = 'cc.php;cod.php';

    $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_array = array();
    if ($dir = @dir($module_directory)) {
      while ($file = $dir->read()) {
        if (!is_dir($module_directory . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $directory_array[] = $file;
          }
        }
      }
      sort($directory_array);
      $dir->close();
    }

    $module_active = explode (";",MODULE_PAYMENT_INSTALLED);
    $module_active = explode (";",'cc.php;cod.php');
    $installed_modules = array();
    for ($i = 0, $n = sizeof($directory_array); $i < $n; $i++) {
      $file = $directory_array[$i];
      if (in_array ($directory_array[$i], $module_active)) {
        include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/payment/' . $file);
        include($module_directory . $file);
        $class = substr($file, 0, strrpos($file, '.'));
        if (osc_class_exists($class)) {
          $module = new $class;
          if ($module->check() > 0) {
            $installed_modules[] = $file;
          }
	    }
        $payment_list .= osc_draw_checkbox_field('payment_unallowed[' . $i . ']', $module->code , 1);
      } 
    }
    return $payment_list;
  }



// Mode B2B : Verifie les modes de paiements autorises
  function osc_get_payment_unallowed ($pay_check) {
    global $OSCOM_PDO;
    
    $Qpayments = $OSCOM_PDO->prepare('select group_payment_unallowed
                                      from :table_customers_groups
                                     ');

    $Qpayments->execute();

    $payments_not_allowed = $Qpayments->fetch();
    $payments_unallowed = explode (",",$payments_not_allowed['group_payment_unallowed']);
    $clearance = (!in_array ($pay_check, $payments_unallowed)) ?  true : false;

    return $clearance;
  }

/**
* Not Display  the payment module if customer_group = 0
 * @param string $customer_group_id, the group of the customer
 * @access public
**/
  function osc_get_payment_not_display_payment($customer_group_id) {
  return $customer_group_id;
}

// Mode B2B : Verifie les modes de livraison autorises
  function osc_get_shipping_unallowed ($shipping_check) {
    global $OSCOM_PDO;
    
    $Qshipping = $OSCOM_PDO->prepare('select group_shipping_unallowed
                                      from :table_customers_groups
                                     ');

    $Qshipping->execute();

    $shipping_not_allowed = $Qshipping->fetch();
    $shipping_unallowed = explode (",",$shipping_not_allowed['group_payment_unallowed']);
    $shipping_clearance = (!in_array ($shipping_check, $shipping_unallowed)) ?  true : false;
    return $shipping_clearance;
  }

// Mode B2B : Verifie si l'on doit calculer la taxe selon la configuration du groupe (assujetti a la TVA ou non)
  function osc_get_tax_unallowed ($tax_check) {
    return $tax_clearance;
  }
