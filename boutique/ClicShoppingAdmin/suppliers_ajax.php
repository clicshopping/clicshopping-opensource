<?php
/**
 * suppliers_ajax.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (isset($_REQUEST['q'])) {

    $terms = strtolower($_GET["q"]);

    $Qcheck = $OSCOM_PDO->prepare('select distinct suppliers_id as id,
                                                    suppliers_name as name
                                   from :table_suppliers
                                   where suppliers_name LIKE :terms
                                   limit 10;
                                  ');
    $Qcheck->bindValue(':terms', '%'.$terms.'%');
    $Qcheck->execute();


    $list = $Qcheck->rowCount() ;

    if ($list > 0) {
      $array = array();

      while ($value =  $Qcheck->fetch() ) {
        $array[] = $value;
      }

    # JSON-encode the response
      $json_response = json_encode($array); //Return the JSON Array

    # Return the response
      echo $json_response;
    }
  }
