<?php
/**
 * suppliers_popup_ajax.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!empty($_POST['suppliers_name'])) {
    $suppliers_name = osc_db_prepare_input($_POST['suppliers_name']);
    $suppliers_manager = osc_db_prepare_input($_POST['suppliers_manager']);
    $suppliers_phone = osc_db_prepare_input($_POST['suppliers_phone']);
    $suppliers_email_address = osc_db_prepare_input($_POST['suppliers_email_address']);
    $suppliers_fax = osc_db_prepare_input($_POST['suppliers_fax']);
    $suppliers_address = osc_db_prepare_input($_POST['suppliers_address']);
    $suppliers_suburb = osc_db_prepare_input($_POST['suppliers_suburb']);
    $suppliers_postcode = osc_db_prepare_input($_POST['suppliers_postcode']);
    $suppliers_city = osc_db_prepare_input($_POST['suppliers_city']);
    $suppliers_states = osc_db_prepare_input($_POST['suppliers_states']);
    $suppliers_country_id = osc_db_prepare_input($_POST['suppliers_country_id']);
    $suppliers_notes = osc_db_prepare_input($_POST['suppliers_notes']);
    $suppliers_image = osc_db_prepare_input($_POST['suppliers_image']);

    // Insertion images des fabricants via l'editeur FCKeditor (fonctionne sur les nouvelles et editions des fabricants)
    if (isset($_POST['suppliers_image']) && osc_not_null($_POST['suppliers_image']) && ($_POST['suppliers_image'] != 'none') && ($_POST['delete_image'] != 'yes')) {
      $suppliers_image = htmlspecialchars($suppliers_image);
      $suppliers_image = strstr($suppliers_image, DIR_WS_CATALOG_IMAGES);
      $suppliers_image = str_replace(DIR_WS_CATALOG_IMAGES, '', $suppliers_image);
      $suppliers_image_end = strstr($suppliers_image, '&quot;');
      $suppliers_image = str_replace($suppliers_image_end, '', $suppliers_image);
      $suppliers_image = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $suppliers_image);
    } else {
      $suppliers_image = 'null';
    }

    $sql_data_array = array('suppliers_name' => $suppliers_name,
                            'suppliers_manager' => $suppliers_manager,
                            'suppliers_phone' => $suppliers_phone,
                            'suppliers_email_address' => $suppliers_email_address,
                            'suppliers_fax' => $suppliers_fax,
                            'suppliers_address' => $suppliers_address,
                            'suppliers_suburb' => $suppliers_suburb,
                            'suppliers_postcode' => $suppliers_postcode,
                            'suppliers_city' => $suppliers_city,
                            'suppliers_states' => $suppliers_states,
                            'suppliers_country_id' => $suppliers_country_id,
                            'suppliers_notes' => $suppliers_notes
    );

    if ($suppliers_image != 'null') {
      $insert_image_sql_data = array('suppliers_image' => $suppliers_image);
      $sql_data_array = array_merge($sql_data_array, $insert_image_sql_data);
    }

    $insert_sql_data = array('date_added' => 'now()');

    $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

    $OSCOM_PDO->save('suppliers', $sql_data_array);

    $suppliers_id = $OSCOM_PDO->lastInsertId();
    
    $languages = osc_get_languages();

    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      $suppliers_url_array = $_POST['suppliers_url'];
      $language_id = $languages[$i]['id'];

      $sql_data_array = array('suppliers_url' => osc_db_prepare_input($suppliers_url_array[$language_id]));

      if ($action == 'insert') {
        $insert_sql_data = array('suppliers_id' => $suppliers_id,
                                 'languages_id' => $language_id);

        $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

        $OSCOM_PDO->save('suppliers_info', $sql_data_array);

      }
    }

//***************************************
// odoo web service
//***************************************
    if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_SUPPLIERS_ADMIN == 'true') {
      require ('ext/odoo_xmlrpc/xml_rpc_admin_suppliers.php');
    }
//***************************************
// End odoo web service
//***************************************

  /*
          if (USE_CACHE == 'true') {
            osc_reset_cache_block('suppliers');
          }
  */
    echo 'Success';
//    echo "From Server : ".json_encode($_POST)."<br>";
  } else {
    echo 'Error <br />';
  }

?>