<?php
  /**
   * cfm_modules_index_categories.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_index_categories {
    var $code = 'modules_index_categories';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_INDEX_CATEGORIES_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_index_categories() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_index_categories/';
      $this->title = MODULE_CFG_MODULE_INDEX_CATEGORIES_TITLE;
    }
  }
?>
