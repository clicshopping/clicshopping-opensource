<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Commande');
define('NAVBAR_TITLE_2', 'Confirmation');

define('HEADING_TITLE', 'Confirmation commande');

define('HEADING_DELIVERY_ADDRESS', 'Adresse de livraison');
define('HEADING_SHIPPING_INFORMATION','Informations sur le destinataire');
define('HEADING_SHIPPING_METHOD', 'Méthode d\'expédition');
define('HEADING_PRODUCTS', 'Produits');
define('HEADING_TAX', 'TVA');
define('HEADING_TOTAL', 'Total');
define('HEADING_BILLING_INFORMATION', 'Information sur la facturation');
define('HEADING_BILLING_ADDRESS', 'Adresse de facturation');
define('HEADING_PAYMENT_METHOD', 'Mode de Paiement');
define('HEADING_PAYMENT_INFORMATION', 'Paiement destiné à :');
define('HEADING_ORDER_COMMENTS', 'Commentaires au sujet de votre commande ');

define('TEXT_EDIT', 'Editer');
define('TEXT_CONDITIONS_CONFIRMATION','En validant cette case à cocher et en appuyant sur le bouton confirmé, je consens à confirmer la commande des produits commandés');
define('TEXT_CONDITION_CONFIRMATION_ERROR_AGREEMENT','Erreur : Veuillez cocher la case à cocher');
?>