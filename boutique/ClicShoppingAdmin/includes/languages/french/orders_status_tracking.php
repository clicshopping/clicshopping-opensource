<?php
/**
 * orders_status_tracking.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Statut du tracking pour les expéditions pour les commandes / factures');

define('TABLE_HEADING_ORDERS_STATUS', 'Statut du tracking pour les éditions pour les commandes / factures');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Nom de la société :');
define('TEXT_INFO_ORDERS_STATUS_LINK', 'Url du tracking <br /><br /><strong>Note:</strong><br />-Ne pas insérer d\'url si le numéro de tracking ne se situe pas à la fin de l\'url');

define('TEXT_INFO_INSERT_INTRO', 'Merci de compléter cette nouvelle demande avec les données liées');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer ce statut ?');
define('TEXT_INFO_HEADING_NEW_ORDERS_STATUS', 'Nouveau statut ');
define('TEXT_INFO_HEADING_EDIT_ORDERS_STATUS', 'Editer statut ');
define('TEXT_INFO_HEADING_DELETE_ORDERS_STATUS', 'Supprimer ce statut');

define('ERROR_REMOVE_DEFAULT_ORDER_STATUS', 'Erreur : Le statut par défaut ne peut pas être supprimé. Merci de choisir un autre statut par défaut et de réessayer');
define('ERROR_STATUS_USED_IN_ORDERS', 'Erreur : Ce statut est actuellement utilisé.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Erreur : Ce statut est déjà utilisé dans l\'historique de commande.');
?>