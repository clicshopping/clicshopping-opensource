<?php
/**
 * manufacturers_popup.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  $languages = osc_get_languages();
?>
<form name="ajaxform" id="ajaxform" <?php echo 'action="' . osc_href_link('manufacturers_popup_ajax.php') . '"'; ?> method="post">

  <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
      <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
            <tr>
               <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/manufacturers.gif', HEADING_TITLE, '40', '40'); ?></td>
               <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
               <td align="right" class="pageHeading"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                <div id="simple-msg" style="float:left;"></div>
                <div style="float:right;">&nbsp;<?php echo osc_image_submit('button_insert_specials.gif', IMAGE_INSERT, 'id="simple-post"'); ?></div>
              </td>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
    </tr>
    <tr>
      <td>
        <div>
          <ul class="nav nav-tabs" role="tablist"id="myTab">
            <li class="active"><a href="#tab20" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
            <li><a href="#tab21" role="tab" data-toggle="tab"><?php echo TAB_DESCRIPTION; ?></a></li>
            <li><a href="#tab22" role="tab" data-toggle="tab"><?php echo TAB_VISUEL; ?></a></li>
            <li><a href="#tab23" role="tab" data-toggle="tab"><?php echo TAB_SEO; ?></a></li>
          </ul>
          <div class="tabsClicShopping">
            <div class="tab-content">
<?php
// -- ------------------------------------------------------------ //
// --          ONGLET Information General du fabricant           //
// -- ------------------------------------------------------------ //
?>
              <div class="tab-pane active" id="tab20">
                <div class="col-md-12 mainTitle">
                  <div class="pull-left"><?php echo TITLE_MANUFACTURER_GENERAL; ?></div>
                </div>
                <div class="adminformTitle">
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-3 centerInputFields"><?php echo TEXT_MANUFACTURERS_NAME; ?></span>
                      <span class="col-md-3"><?php echo osc_draw_input_field('manufacturers_name', $mInfo->manufacturers_name, 'required aria-required="true" id="manufacturers_name"'); ?></span>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-3"><?php echo TEXT_MANUFACTURERS_URL; ?></span>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <div class="col-md-12">
                        <div  class="col-md-12">
                          <span class="col-md-1 centerInputFields"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                          <span class="col-md-3 pull-left" style="height:50px;"><?php echo osc_draw_input_field('manufacturers_url[' . $languages[$i]['id'] . ']', osc_get_manufacturer_url($mInfo->manufacturers_id, $languages[$i]['id'])); ?></span>
                        </div>
                      </div>

<?php
    }
?>
                  </div>
                </div>
              </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information description       //-->
<!-- ------------------------------------------------------------ //-->
              <div class="tab-pane" id="tab21">
                <div class="mainTitle">
                  <span><?php echo TEXT_MANUFACTURERS_DESCRIPTION; ?></span>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main" valign="top"><?php echo TEXT_MANUFACTURERS_DESCRIPTION; ?>&nbsp;</td>
                      </tr>
<?php
    for ($i=0; $i<sizeof($languages); $i++) {
?>
                       <tr>
                         <td class="main" valign="top" align="right"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                         <td class="main">
                           <div style="visibility:visible; display:block;">
<?php  echo osc_draw_textarea_ckeditor('manufacturer_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300',''); ?>
                           </div>
                         </td>
                       </tr>
<?php
   }
?>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
              </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information visuelle          //-->
<!-- ------------------------------------------------------------ //-->
              <div class="tab-pane" id="tab22">
                <div class="col-md-12 mainTitle">
                  <div class="pull-left"><?php echo TITLE_MANUFACTURER_IMAGE; ?></div>
                </div>
                <div class="adminformTitle">
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-3"><?php echo  TEXT_MANUFACTURERS_NEW_IMAGE; ?></span>
                          <span  class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_PRODUCTS_IMAGE_VIGNETTE, '40', '40'); ?></span>
                          <span  class="col-md-4"><?php echo TEXT_PRODUCTS_IMAGE_VIGNETTE . '<br /><br />' . osc_draw_file_field_image_ckeditor('manufacturers_image', '212', '212','') ; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information seo                              //-->
<!-- ------------------------------------------------------------ //-->
              <div class="tab-pane" id="tab23">
                <div class="col-md-12 mainTitle">
                  <div class="pull-left"><?php echo TITLE_MANUFACTURER_SEO; ?></div>
                </div>
                <div class="adminformTitle">
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2"></span>
                      <span class="col-md-4"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                      <span class="col-md-4"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                    </div>
                  </div>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                  <div class="row">
                    <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2 centerInputFields"><?php echo  TEXT_MANUFACTURER_SEO_TITLE; ?></span>
                      <span  class="col-md-8"><?php echo osc_draw_input_field('manufacturer_seo_title[' . $languages[$i]['id'] . ']', (($manufacturer_seo_title[$languages[$i]['id']]) ? $manufacturer_seo_title[$languages[$i]['id']] : osc_get_manufacturer_seo_title($mInfo->manufacturers_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?></span>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2 centerInputFields"><?php echo  TEXT_MANUFACTURER_SEO_DESCRIPTION; ?></span>
                      <span  class="col-md-8"><?php echo osc_draw_textarea_field('manufacturer_seo_description[' . $languages[$i]['id'] . ']', 'soft', '75', '2', (isset($manufacturer_seo_description[$languages[$i]['id']]) ? $manufacturer_seo_description[$languages[$i]['id']] : osc_get_manufacturer_seo_description($mInfo->manufacturers_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></span>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2 centerInputFields"><?php echo TEXT_MANUFACTURER_SEO_KEYWORDS; ?></span>
                      <span  class="col-md-8"><?php echo osc_draw_textarea_field('manufacturer_seo_keyword[' . $languages[$i]['id'] . ']', 'soft', '75', '5', (isset($manufacturer_seo_keyword[$languages[$i]['id']]) ? $manufacturer_seo_keyword[$languages[$i]['id']] : osc_get_manufacturer_seo_keyword($mInfo->manufacturers_id, $languages[$i]['id']))); ?></span>
                    </div>
                  </div>
<?php
    }
?>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </td>
    </tr>
  </table>

</form>

  <script type="text/javascript" src="ext/javascript/bootstrap/js/bootstrap_ajax_form.js" /></script>
<?php
 require('includes/application_bottom.php');
?>