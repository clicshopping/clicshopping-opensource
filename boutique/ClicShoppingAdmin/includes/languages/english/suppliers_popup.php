<?php
/**
 * suppliers_ajax.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Suppliers management');

define('TITLE_SUPPLIERS_GENERAL','General information');
define('TITLE_SUPPLIERS_IMAGE','Supplier Image');
define('TAB_VISUEL','Image');
define('TAB_SUPPLIERS_NOTE','Notes');

define('TEXT_SUPPLIERS', 'Suppliers:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');

define('TEXT_SUPPLIERS_NAME', 'Suppliers Name:');
define('TEXT_SUPPLIERS_NEW_IMAGE', 'New suppliers Image:');
define('TEXT_SUPPLIERS_IMAGE', 'Suppliers Image:');
define('TEXT_SUPPLIERS_DELETE_IMAGE', 'Not display image suppliers');
define('TEXT_SUPPLIERS_URL', 'Suppliers URL:');
define('TEXT_SUPPLIERS_MANAGER', 'Manager name :');
define('TEXT_SUPPLIERS_PHONE', 'Telephone :');
define('TEXT_SUPPLIERS_FAX', 'Fax :');
define('TEXT_SUPPLIERS_EMAIL_ADDRESS','Email address :');
define('TEXT_SUPPLIERS_ADDRESS', 'Address :');
define('TEXT_SUPPLIERS_SUBURB', 'Suburb :');
define('TEXT_SUPPLIERS_POSTCODE', 'Postcode : ');
define('TEXT_SUPPLIERS_CITY', 'City :');
define('TEXT_SUPPLIERS_STATES', 'States :');
define('TEXT_SUPPLIERS_COUNTRY', 'Country :');
define('TEXT_SUPPLIERS_NOTES','Others informations');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');
define('TEXT_DELETE_IMAGE', 'Delete suppliers image?');
?>