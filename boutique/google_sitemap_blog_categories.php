<?php
/*
 * google_sitemap_specials.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n" . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $blog_category_array = array();

    $QblogCategories = $OSCOM_PDO->prepare('select blog_categories_id,
                                            coalesce(NULLIF(last_modified, :last_modified),
                                                           date_added) as last_modified
                                            from :table_blog_categories
                                            where customers_group_id = :customers_group_id
                                            group by blog_categories_id
                                            order by last_modified DESC
                                          ');

    $QblogCategories->bindValue(':last_modified', '');
    $QblogCategories->bindValue(':customers_group_id', '0');
    $QblogCategories->execute();

    while($blog_categories = $QblogCategories->fetch() ) {
      $blog_category_array[$blog_categories['blog_categories_id']]['loc'] = osc_href_link('blog.php', 'current=' . (int)$blog_categories['blog_categories_id']);
      $blog_category_array[$blog_categories['blog_categories_id']]['lastmod'] = $blog_categories['last_modified'];
      $blog_category_array[$blog_categories['blog_categories_id']]['changefreq'] = 'weekly';
      $blog_category_array[$blog_categories['blog_categories_id']]['priority'] = '0.5';
    }

    foreach ($blog_category_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
