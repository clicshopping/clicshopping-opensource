<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class d_security_checks {
    var $code = 'd_security_checks';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_security_checks() {
      $this->title = MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS == 'True');
      }
    }

    function getOutput() {
      global $PHP_SELF;

      $output = '';

      $secCheck_types = array('info', 'warning', 'error');
      $secCheck_messages = array();

      $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
      $secmodules_array = array();
      if ($secdir = @dir(DIR_FS_ADMIN . 'includes/modules/security_check/')) {
        while ($file = $secdir->read()) {
          if (!is_dir(DIR_FS_ADMIN . 'includes/modules/security_check/' . $file)) {
            if (substr($file, strrpos($file, '.')) == $file_extension) {
              $secmodules_array[] = $file;
            }
          }
        }
        sort($secmodules_array);
        $secdir->close();
      }

      foreach ($secmodules_array as $secmodule) {
        include(DIR_FS_ADMIN . 'includes/modules/security_check/' . $secmodule);

        $secclass = 'securityCheck_' . substr($secmodule, 0, strrpos($secmodule, '.'));
        if (class_exists($secclass)) {
          $secCheck = new $secclass;

          if ( !$secCheck->pass() ) {
            if (!in_array($secCheck->type, $secCheck_types)) {
              $secCheck->type = 'info';
            }

            $secCheck_messages[$secCheck->type][] = $secCheck->getMessage();
          }
        }
      }
?>

<?php
      $output = '<div class="clearfix"></div>';

      if (isset($secCheck_messages['error'])) {
        $output .= '<div class="alert alert-danger" style="word-wrap: break-word;"><h4>Error!</h4>';

        foreach ($secCheck_messages['error'] as $error) {
          $output .= '<p>' . $error . '</p>';
        }

        $output .= '</div>';
      }

      if (isset($secCheck_messages['warning'])) {
        $output .= '<div class="alert alert-warning" style="word-wrap: break-word;"><h4>Warning!</h4>';

        foreach ($secCheck_messages['warning'] as $warning) {
          $output .= '<p>' . $warning . '</p>';
        }

        $output .= '</div>';
      }

      if (isset($secCheck_messages['info'])) {
        $output .= '<div class="alert alert-info" style="word-wrap: break-word;"><h4>Info!</h4>';

        foreach ($secCheck_messages['info'] as $info) {
          $output .= '<p>' . $info . '</p>';
        }

        $output .= '</div>';
      }

      if (empty($secCheck_messages)) {
        $output .= '<div class="alert alert-success" style="word-wrap: break-word;"><h4>Success!</h4><p>' . MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SUCCESS . '</p></div>';
      }

      $output .= '<div class="clearfix"></div>';

      return $output;
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS');
    }

    function install() {
      global $OSCOM_PDO;
      
      if ($_SESSION['languages_id'] =='1') {
        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SORT_ORDER',
            'configuration_value' => '200',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr� en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '99',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      } else {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Do you want enable this Module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Do you want enable this Module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Sort Order',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SORT_ORDER',
            'configuration_value' => '200',
            'configuration_description' => 'Sort order of display. Lowest is displayed first.',
            'configuration_group_id' => '6',
            'sort_order' => '99',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );
      }

    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS', 
                   'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SORT_ORDER'
                  );
    }
  }
