<?php
/**
 * modules_upload.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Installateur de modules');
define('HEADING_TITLE_STEP1', 'Choix du type d\'installation');
define('HEADING_TITLE_STEP2', 'Choix du Template');
define('HEADING_TITLE_STEP3', 'Téléchargement du module');
define('HEADING_TITLE_STEP4', 'Instructions complémentaires');
define('HEADING_TITLE', 'Informations complémentaires');

// step1

define('WELCOME_HEADING_TITLE','
<br /><br />Bienvenue dans le système d\'installation des modules issues de la market place de ClicShopping.<br /><br />
        <ul>Procédure d\'installation</ul>
        <blockquote>
         <li>Etape 1 - Télécharger le module de la market place dont le lien se trouve dans page récapitulative de la commande / facture de votre compte dans la market place</li>
         <li>Etape 2 - Choisir le type de module à installer dans la boite de dialogue (toutes les informations concernant ce choix se trouve sur la market place concernant le module)</li>
         <li>Etape 3 - Installer le module</li>
         <li>Etape 4 - Configurer le module dans la section appropriée de l\'administration</li>
        </blockquote>
           <br /><br />
          <p align="center">Cliquer sur le bouton pour commencer la procédure</p>
');

// step1

define('WELCOME_MESSAGE_STEP1','
<ul>Procédure d\'installation</ul>
        <blockquote>
         <li>Etape 2 - Choisir le type de module à installer dans la boite de dialogue (toutes les informations concernant ce choix se trouve sur la market place contenant le module)</li>
         <li>Etape 3 - Installer le module</li>
         <li>Etape 4 - Configurer le module dans la section appropriée de l\'administration</li>
        </blockquote>
           <br /><br />
          <p align="center">Veuillez choisir parmi les options suivantes</p>
');

define('TEXT_MODULE_TEMPLATE','Module pour le template');
define('TEXT_MODULE_FIXE','Module fixe');
define('TEXT_MODULE_TEMPLATE','Template');



//step2
define('WELCOME_MESSAGE_STEP2','
        <ul>Procédure d\'installation</ul>
        <blockquote>
         <li>Etape 3 - Choisir le template ou le module doit etre installé</li>
         <li>Etape 4 - Configurer le module dans la section appropriée de l\'administration</li>
        </blockquote>
           <br /><br />
          <p align="center">Veuillez Sélectionner le template ou le module doit etre installé </p>
');

//STEP3
define('WELCOME_MESSAGE_STEP3','
          <ul>Procédure d\'installation</ul>
        <blockquote>
         <li>Etape 4 - Installer le module</li>
         <li>Etape 5 - Configurer le module dans la section appropriée de l\'administration</li>
        </blockquote>
           <br /><br />
          <p align="center">Veuillez télécharger le module qui sera installé dans le répertoire : '. $template_install .'</p>
');

//step4
define('WELCOME_MESSAGE_INTRO_STEP4','
        <blockquote>
         <li>Etape 5 - Configurer le module dans la section approprié dans l\'administration</li>
        </blockquote>
           <br /><br />
          <p align="center">la procédure d\'installation est terminée</p>
');



define('WELCOME_MESSAGE_STEP4','
        <ul>Ou trouver votre module installé ? Voici quelques exemples</ul>
        <blockquote><blockquote>
         <li>Module Boxe : Menu Design / Boxes gauche et groite</li>
         <li>Module page accueil : Menu Design / page accueil</li>
         <li>Module listing : Menu Design / Listing</li>
         <li>Module catégories : Menu Design /catégories</li>
         <li>Module Description des produits : Menu Design / description des produits</li>

         <li>Module Meta tag : menu Configuration / référencement / Activation méta données</li>
         <li>Module Réseaux sociaux : Menu Configuration / référencement / Activation des réseaux sociaux</li>

        </blockquote></blockquote>
');
?>