<?php
/*
 * tax_rates.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':
        $tax_zone_id = osc_db_prepare_input($_POST['tax_zone_id']);
        $tax_class_id = osc_db_prepare_input($_POST['tax_class_id']);
        $tax_rate = osc_db_prepare_input($_POST['tax_rate']);
        $tax_description = osc_db_prepare_input($_POST['tax_description']);
        $tax_priority = osc_db_prepare_input($_POST['tax_priority']);
        $code_tax_odoo = osc_db_prepare_input($_POST['code_tax_odoo']);


        $OSCOM_PDO->save('tax_rates', [
                                        'tax_zone_id' =>  (int)$tax_zone_id ,
                                        'tax_class_id' => (int)$tax_class_id,
                                        'tax_rate' =>  $tax_rate,
                                        'tax_description' => $tax_description,
                                        'tax_priority' => $tax_priority,
                                        'date_added' => 'now()',
                                        'code_tax_odo' => $code_tax_odoo
                                      ]
                        );

        osc_redirect(osc_href_link('tax_rates.php'));
        break;
      case 'save':
        $tax_rates_id = osc_db_prepare_input($_GET['tID']);
        $tax_zone_id = osc_db_prepare_input($_POST['tax_zone_id']);
        $tax_class_id = osc_db_prepare_input($_POST['tax_class_id']);
        $tax_rate = osc_db_prepare_input($_POST['tax_rate']);
        $tax_description = osc_db_prepare_input($_POST['tax_description']);
        $tax_priority = osc_db_prepare_input($_POST['tax_priority']);
        $code_tax_odoo = osc_db_prepare_input($_POST['code_tax_odoo']);

        $Qupdate = $OSCOM_PDO->prepare('update :table_tax_rates
                                        set tax_rates_id = :tax_rates_id, 
                                            tax_zone_id = :tax_zone_id, 
                                            tax_class_id = :tax_class_id, 
                                            tax_rate = :tax_rate, 
                                            tax_description = :tax_description, 
                                            tax_priority = :tax_priority,
                                            last_modified = now(),
                                            code_tax_odoo = :code_tax_odoo
                                        where tax_rates_id = :tax_rates_id
                                      ');
        $Qupdate->bindInt(':tax_rates_id', (int)$tax_rates_id);
        $Qupdate->bindInt(':tax_zone_id', (int)$tax_zone_id);
        $Qupdate->bindInt(':tax_class_id', (int)$tax_class_id);
        $Qupdate->bindValue(':tax_rate', osc_db_input($tax_rate));
        $Qupdate->bindValue(':tax_description',osc_db_input($tax_description) );  
        $Qupdate->bindValue(':tax_priority', osc_db_input($tax_priority) );
        $Qupdate->bindValue(':code_tax_odoo', osc_db_input($code_tax_odoo) );

        $Qupdate->execute();

        osc_redirect(osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $tax_rates_id));
        break;
      case 'deleteconfirm':
        $tax_rates_id = osc_db_prepare_input($_GET['tID']);

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_tax_rates
                                        where tax_rates_id = :tax_rates_id 
                                      ');
        $Qdelete->bindInt(':tax_rates_id', (int)$tax_rates_id  );
        $Qdelete->execute();
        
        osc_redirect(osc_href_link('tax_rates.php', 'page=' . $_GET['page']));
        break;
    }
  }

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/tax_rates.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
    <span class="pull-right"><?php echo '<a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_tax_rate.gif', IMAGE_NEW_TAX_RATE) . '</a>'; ?></span>
<?php
  }
?>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TAX_RATE_PRIORITY; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TAX_CLASS_TITLE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ZONE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TAX_RATE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TAX_DESCRIPTION; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CODE_TAX_ODOO; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $rates_query_raw = "select r.tax_rates_id, 
                             z.geo_zone_id, 
                             z.geo_zone_name, 
                             tc.tax_class_title, 
                             tc.tax_class_id, 
                             r.tax_priority, 
                             r.tax_rate, 
                             r.tax_description, 
                             r.date_added, 
                             r.last_modified,
                             r.code_tax_odoo
                      from tax_class tc,
                           tax_rates r left join geo_zones z on r.tax_zone_id = z.geo_zone_id
                      where r.tax_class_id = tc.tax_class_id
                    ";
  $rates_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $rates_query_raw, $rates_query_numrows);
  $rates_query = osc_db_query($rates_query_raw);

  while ($rates = osc_db_fetch_array($rates_query)) {
    if ((!isset($_GET['tID']) || (isset($_GET['tID']) && ($_GET['tID'] == $rates['tax_rates_id']))) && !isset($trInfo) && (substr($action, 0, 3) != 'new')) {
      $trInfo = new objectInfo($rates);
    }

    if (isset($trInfo) && is_object($trInfo) && ($rates['tax_rates_id'] == $trInfo->tax_rates_id)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $rates['tax_priority']; ?></td>
                <td class="dataTableContent"><?php echo $rates['tax_class_title']; ?></td>
                <td class="dataTableContent"><?php echo $rates['geo_zone_name']; ?></td>
                <td class="dataTableContent"><?php echo osc_display_tax_value($rates['tax_rate']); ?>%</td>
                <td class="dataTableContent"><?php echo $rates['tax_description']; ?></td>
                <td class="dataTableContent"><?php echo $rates['code_tax_odoo']; ?></td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $rates['tax_rates_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $rates['tax_rates_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($trInfo) && is_object($trInfo) && ($rates['tax_rates_id'] == $trInfo->tax_rates_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $rates['tax_rates_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $rates_split->display_count($rates_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_TAX_RATES); ?></td>
                    <td class="smallText" align="right"><?php echo $rates_split->display_links($rates_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_TAX_RATE . '</strong>');

      $contents = array('form' => osc_draw_form('rates', 'tax_rates.php', 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_CLASS_TITLE . '<br />' . osc_tax_classes_pull_down('name="tax_class_id" style="font-size:10px"'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_ZONE_NAME . '<br />' . osc_geo_zones_pull_down('name="tax_zone_id" style="font-size:10px"'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_TAX_RATE . '<br />' . osc_draw_input_field('tax_rate'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_RATE_DESCRIPTION . '<br />' . osc_draw_input_field('tax_description'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_TAX_ODOO . '<br />' . osc_draw_input_field('code_tax_odoo'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_TAX_RATE_PRIORITY . '<br />' . osc_draw_input_field('tax_priority'));
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_TAX_RATE . '</strong>');

      $contents = array('form' => osc_draw_form('rates', 'tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $trInfo->tax_rates_id  . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_CLASS_TITLE . '<br />' . osc_tax_classes_pull_down('name="tax_class_id" style="font-size:10px"', $trInfo->tax_class_id));
      $contents[] = array('text' => '<br />' . TEXT_INFO_ZONE_NAME . '<br />' . osc_geo_zones_pull_down('name="tax_zone_id" style="font-size:10px"', $trInfo->geo_zone_id));
      $contents[] = array('text' => '<br />' . TEXT_INFO_TAX_RATE . '<br />' . osc_draw_input_field('tax_rate', $trInfo->tax_rate));
      $contents[] = array('text' => '<br />' . TEXT_INFO_RATE_DESCRIPTION . '<br />' . osc_draw_input_field('tax_description', $trInfo->tax_description));
      $contents[] = array('text' => '<br />' . TEXT_INFO_TAX_ODOO . '<br />' . osc_draw_input_field('code_tax_odoo', $trInfo->code_tax_odoo));
      $contents[] = array('text' => '<br />' . TEXT_INFO_TAX_RATE_PRIORITY . '<br />' . osc_draw_input_field('tax_priority', $trInfo->tax_priority));
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $trInfo->tax_rates_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_TAX_RATE . '</strong>');

      $contents = array('form' => osc_draw_form('rates', 'tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $trInfo->tax_rates_id  . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $trInfo->tax_class_title . ' ' . number_format($trInfo->tax_rate, TAX_DECIMAL_PLACES) . '%</strong>');
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('tax_rates.php', 'page=' . $_GET['page'] . '&tID=' . $trInfo->tax_rates_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    default:
    break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

