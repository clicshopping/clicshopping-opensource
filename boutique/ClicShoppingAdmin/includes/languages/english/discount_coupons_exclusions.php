<?php
/**
 * discount_coupons_exclusions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Discount Coupons Exclusions for Coupon %s');
define('HEADING_TITLE_VIEW_MANUAL', 'Click here to read the Discount Coupon Codes manual for help editing coupons.');
if( isset( $_GET['type'] ) && $_GET['type'] != '' ) {
	switch( $_GET['type'] ) {
		//category exclusions
		case 'categories':
			$heading_available = 'This coupon may be applied to products in these categories.';
			$heading_selected = 'This coupon may <strong>not</strong> be applied to products in these categories.';
			break;
		//end category exclusions
		//manufacturer exclusions
		case 'manufacturers':
			$heading_available = 'This coupon may be applied to products assigned to these manufacturers.';
			$heading_selected = 'This coupon may <strong>not</strong> be applied to products assigned to these manufacturers.';
			break;
		//end manufacturer exclusions
    //customer exclusions
		case 'customers':
			$heading_available = 'This coupon may be used by these customers.';
			$heading_selected = 'This coupon may <strong>not</strong> be used by these customers.';
			break;
		//end customer exclusions
		//product exclusions
		case 'products':
      $heading_available = 'This coupon may be applied to these products.';
			$heading_selected = 'This coupon may <strong>not</strong> be applied to these products.';
			break;
		//end product exclusions
    //shipping zone exclusions
    case 'zones' :
      $heading_available = 'This coupon may be used in these shipping zones.';
      $heading_selected = 'This coupon may <strong>not</strong> be used in these shipping zones.';
      break;
    //end zone exclusions
	}
}
define('HEADING_AVAILABLE', $heading_available);
define('HEADING_SELECTED', $heading_selected);

define('TEXT_SAVE','Sauvegarder');
define('TEXT_REMOVE','Tout surpprimer');
define('TEXT_CHOOSE_ALL','tout choisir ');

define('MESSAGE_DISCOUNT_COUPONS_EXCLUSIONS_SAVED', 'New exclusion rules saved.');

define('ERROR_DISCOUNT_COUPONS_NO_COUPON_CODE', 'No coupon selected.' );
define('ERROR_DISCOUNT_COUPONS_INVALID_TYPE', 'Cannot create exclusions of that type.');
define('ERROR_DISCOUNT_COUPONS_SELECTED_LIST', 'There has been an error determining the already excluded '.$_GET['type'].'.');
define('ERROR_DISCOUNT_COUPONS_ALL_LIST', 'There has been an error determining the available '.$_GET['type'].'.');
define('ERROR_DISCOUNT_COUPONS_SAVE', 'Error saving new exclusion rules.');

?>
