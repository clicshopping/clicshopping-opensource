<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ACTION_RECORDER_NEWSLETTER_NO_ACCOUNT_TITLE', 'Anonymous newsletter');
  define('MODULE_ACTION_RECORDER_NEWSLETTER_NO_ACCOUNT_DESCRIPTION', 'Record usage of the Anonymous newsletter.');
?>