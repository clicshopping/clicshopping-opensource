<?php
/*
 * products_preview.php 
 * @copyright Copyright 2010 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Prévisualisation de la fiche du produit');


define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Métadonnées pour le produit');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Titre : ');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description : ');
define('TEXT_PRODUCTS_KEYWORDS', 'Mots clef : ');
define('TEXT_PRODUCTS_TAG','Tags produit : ');

define('TEXT_NO_PRODUCTS','Le produit à prévisualiser n\'existe plus');

define('TEXT_PRODUCTS', 'Produits :');
define('TEXT_PRODUCTS_PRICE_INFO', 'Prix :');
define('TEXT_PRODUCTS_TAX_CLASS', 'Classe Fiscale :');
define('TEXT_PRODUCTS_COST','Cout d\'achat fournisseur :');
define('TEXT_PRODUCTS_HANDLING','Autres couts :');

define('TEXT_PRODUCT_DATE_ADDED', 'Produit ajouté au catalogue le :');
define('TEXT_PRODUCTS_SHIPPING_DELAY','Délais de livraison spécifique (autre que par défaut) : ');
define('TEXT_PRODUCT_OPTION','Champs optionnels :');
define('TEXT_PRODUCTS_VIEW','Options d\'affichage :');

define('TEXT_PRODUCTS_STATUS', 'Statut :');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date de disponibilité :');
define('TEXT_PRODUCT_AVAILABLE', 'En stock');
define('TEXT_PRODUCTS_DIMENSION','Dimensions (LxHxP) :');
define('TEXT_PRODUCTS_DIMENSION_TYPE','Type de dimension:');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Hors stock');
define('TEXT_PRODUCTS_MANUFACTURER', 'Marque du produit : ');
define('TEXT_PRODUCTS_NAME', 'Nom du produit : ');
define('TEXT_PRODUCTS_DESCRIPTION', 'Description du produit');
define('TEXT_PRODUCTS_QUANTITY', 'Quantité en stock : ');
define('TEXT_PRODUCTS_MIN_ORDER_QUANTITY', 'Quantité minimale d\'achat pour une commande : ');
define('TEXT_PRODUCTS_VOLUME','Volume du produit : ');
define('TEXT_PRODUCTS_QUANTITY_UNIT','Type quantité de commande : ');
define('TEXT_PRODUCTS_ONLY_ONLINE','Exclusivité web (non vendu en magasin)');
define('TEXT_PRODUCTS_SUPPLIERS', 'Fournisseur du produit : ');
define('TEXT_PRODUCTS_MANUFACTURER', 'Marque du produit:');
define('TEXT_PRODUCTS_WEIGHT_POUNDS','Poids (Livres)');


define('TEXT_PRODUCTS_TIME_REPLENISHMENT','Délais de réapprovisionnement :');
define('TEXT_PRODUCTS_WHAREHOUSE','Nom de l\'entrepot');
define('TEXT_PRODUCTS_WHAREHOUSE_ROW','Rangée de l\'entreprot :');
define('TEXT_PRODUCTS_WHAREHOUSE_LEVEL_LOCATION','Niveau de localisation :');
define('TEXT_PRODUCTS_WHAREHOUSE_PACKAGING','Type de packaging :');
define('TEXT_WHAREHOUSE','Entreprosage');

define('TEXT_PRODUCTS_PACKAGING_NEW','Produit neuf');
define('TEXT_PRODUCTS_PACKAGING_REPACKAGED','Produit reconditionné');
define('TEXT_PRODUCTS_PACKAGING_USED','Produit d\'occasion');


define('TEXT_PRODUCTS_MODEL', 'Référence : ');
define('TEXT_PRODUCTS_EAN', 'Code barre (EAN13) : ');
define('TEXT_PRODUCTS_SKU','SKU : ');
define('TEXT_PRODUCTS_PRICE_COMPARISON','Exclure des comparateurs de prix ? ');
define('TEXT_PRODUCTS_SUPPLIERS', 'Fournisseur du produit : ');

define('TAB_PRICE_GROUP_VIEW', 'Actif : Affiche le prix du groupe - Non Actif : Affiche le prix public');
define('TAB_PRODUCTS_GROUP_VIEW', 'Autoriser l\'affichage du produit pour les clients du groupe ');
define('TAB_PRODUCTS_VIEW', 'Autoriser l\'affichage du produit pour les clients Grand Public');
define('TAB_ORDERS_GROUP_VIEW', 'Autoriser la commande du produit pour les clients du groupe ');
define('TAB_ORDERS_VIEW', 'Autoriser la commande du produit pour les clients Grand Public');

define('TEXT_PRODUCTS_URL', 'URL Externe du produit : ');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(sans http://)</small>');
define('TEXT_PRODUCTS_PRESENTATION', 'Présentation');
define('TEXT_PRODUCTS_STOCK', 'Stock');
define('TEXT_PRODUCTS_PRICE_PUBLIC', 'Prix Public / taxes');
define('TEXT_PRODUCTS_PRICE', 'Prix de vente: ');
define('TEXT_PRODUCTS_PRICE_NET', 'Hors Taxes');

define('TEXT_PRODUCTS_WEIGHT', 'Poids <i>(Kg)</i> : ');
define('TEXT_PRODUCTS_PRICE_KILO','Afficher le prix au kilo : ');


define('TEXT_USER_NAME','Créé / Modifié par : ');

?>
