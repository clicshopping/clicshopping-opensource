<?php
/**
 * manufacturers_popup_ajax.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */

  require('includes/application_top.php');

  if (!empty($_POST['manufacturers_name'])) {

    $manufacturers_name = osc_db_prepare_input($_POST['manufacturers_name']);
    $language_id = osc_db_prepare_input($_POST['languages_id']);
    $manufacturers_image = osc_db_prepare_input($_POST['manufacturers_image']);

    if (isset($_POST['manufacturers_image']) && osc_not_null($_POST['manufacturers_image']) && ($_POST['manufacturers_image'] != 'none') && ($_POST['delete_image'] != 'yes')) {
      $manufacturers_image = htmlspecialchars($manufacturers_image);
      $manufacturers_image = strstr($manufacturers_image, DIR_WS_CATALOG_IMAGES);
      $manufacturers_image = str_replace(DIR_WS_CATALOG_IMAGES, '', $manufacturers_image);
      $manufacturers_image_end = strstr($manufacturers_image, '&quot;');
      $manufacturers_image = str_replace($manufacturers_image_end, '', $manufacturers_image);
      $manufacturers_image = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $manufacturers_image);
    } else {
      $manufacturers_image = 'null';
    }

    $sql_data_array = array('manufacturers_name' => $manufacturers_name);

    if ($manufacturers_image != 'null') {
      $insert_image_sql_data = array('manufacturers_image' => $manufacturers_image);
      $sql_data_array = array_merge($sql_data_array, $insert_image_sql_data);
    }

    $insert_sql_data = array('date_added' => 'now()');

    $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
    osc_db_perform('manufacturers', $sql_data_array);

    $manufacturers_id = osc_db_insert_id();
    $languages = osc_get_languages();

    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      $manufacturers_url_array = $_POST['manufacturers_url'];
      $manufacturer_description_array =  $_POST['manufacturer_description'];
      $manufacturer_seo_title_array = $_POST['manufacturer_seo_title'];
      $manufacturer_seo_description_array = $_POST['manufacturer_seo_description'];
      $manufacturer_seo_keyword_array = $_POST['manufacturer_seo_keyword'];
      $language_id = $languages[$i]['id'];

      $sql_data_array = array('manufacturers_id' => $manufacturers_id);


      $insert_sql_data = array('manufacturers_url' => osc_db_prepare_input($manufacturers_url_array[$language_id]),
                              'languages_id' => $language_id,
                              'manufacturer_description' => osc_db_prepare_input($manufacturer_description_array[$language_id]),
                              'manufacturer_seo_title' => osc_db_prepare_input($manufacturer_seo_title_array[$language_id]),
                              'manufacturer_seo_description' => osc_db_prepare_input($manufacturer_seo_description_array[$language_id]),
                              'manufacturer_seo_keyword' => osc_db_prepare_input($manufacturer_seo_keyword_array[$language_id])
                            );

      $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

      $OSCOM_PDO->save('manufacturers_info', $sql_data_array );

//***************************************
// odoo web service
//***************************************
      if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_MANUFACTURER_ADMIN == 'true') {
        require('ext/odoo_xmlrpc/xml_rpc_admin_manufacturer.php');
      }
//***************************************
// End odoo web service
//***************************************
    }

    if (USE_CACHE == 'true') {
      osc_reset_cache_block('manufacturers');
    }

    echo 'Success';
//    echo "From Server : ".json_encode($_POST)."<br>";
  }else {
    echo 'Error <br />';
  }
?>