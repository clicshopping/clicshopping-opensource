<?php
/**
 * template_email.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE','Template emails sent');
define('TABLE_HEADING_TEMPLATE_EMAIL_NAME','Template name');
define('TABLE_HEADING_TEMPLATE_EMAIL_DESCRIPTION','Description');
define('TABLE_HEADING_TEMPLATE_EMAIL_TYPE','Email sending of');
define('TABLE_HEADING_TEMPLATE_CUSTOMER_GROUPS','Customer type');
define('TABLE_HEADING_ACTION','Actions');

define('TEXT_INFO_TEMPLATE_CATALOG','Admin / Catalog template');

define('TEMPLATE_EMAIL_TEXT_NAME','Template name : ');
define('TEMPLATE_EMAIL_TEXT_SHORT_DESCRIPTION','Short description : ');
define('TEMPLATE_EMAIL_TEXT_DESCRIPTION','Message description : ');
define('TEMPLATE_TEXT_CATALOG_ADMIN','Message sent from :');
define('TEMPLATE_EMAIL_TEXT_CUSTOMER_GROUP','Customer group : ');


define('TEMPLATE_EMAIL_TEXT_VARIABLE','Variable');
define('TEXT_TEMPLATE_EMAIL_CATALOG','Catalog');
define('TEXT_TEMPLATE_EMAIL_ADMIN','Administration');
define('TEXT_TEMPLATE_EMAIL_ADMIN_CATALOG','Administration and catalog');
define('TEXT_TEMPLATE_EMAIL_B2C','Mode B2C');
define('TEXT_TEMPLATE_EAIL_B2C_B2B','Mode B2B B2C');



define('TAB_DESCRIPTION','Description');
define('TAB_GENERAL','General');
define('TITLE_INFORMATION_NAME', 'Genaral informations');
define('TITLE_MESSAGE','Message to sent');

define('AIDE_TITLE_ONGLET_GENERAL', 'Informations on template management');

define('TEXT_AIDE_TEMPLATE', '<ul>
<li>%%HTTP_CATALOG%% will allow to insert a catalog url of you site in the email : ex : %%HTTP_CATALOG%%login.php = http://www.maboutique/boutique/login.php</li>
<li>%%STORE_NAME%% will allow to insert the name the shop in your email</li>
<li>%%STORE_OWNER_EMAIL_ADDRESS%%  will allow to insert an email address of the the shop in your email</li>
</ul>');
?>