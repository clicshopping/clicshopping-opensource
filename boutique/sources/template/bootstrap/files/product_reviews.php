<?php
/*
 * product_reviews.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
// ---------------------- 
// --- Title   -----  
// ---------------------- 

  if ( $OSCOM_MessageStack->exists('product_reviews') ) {
    echo $OSCOM_MessageStack->get('product_reviews');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
<div class="contentContainer">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <h3><strong><?php echo $products['products_name']; ?></strong></h3>
<?php
  if ($reviews_split->number_of_rows > 0) {
    if ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3')) {
?>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-sm-6 pagenumber hidden-xs">
          <?php echo $products_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?>
        </div>
        <div class="col-sm-6">';
        <div class="pull-right pagenav"><ul class="pagination"><?php echo $products_split->display_links(MAX_DISPLAY_PAGE_LINKS, osc_get_all_get_params(array('page', 'info'))); ?></ul></div>
          <span class="pull-right"><?php echo TEXT_RESULT_PAGE; ?></span>
        </div>
      </div>
     <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
     <div class="clearfix"></div>
<?php
    }
// ---------------------- 
// --- Image   -----  
// ---------------------- 
?>
  <div style="float: right; width: <?php echo SMALL_IMAGE_WIDTH+10; ?>px; text-align: center;">
<?php
  echo $review_image;
// Affichage du bouton acheter selon les options cochees dans la fiche produit
  echo  TEXT_PRICE . ' ' . $product_price .'<br />';
  echo $button_small_view_details;
?>
  </div>

  <br />
  <br />
<?php
// ---------------------- 
// --- Review   -----  
// ---------------------- 

    while ($reviews = osc_db_fetch_array($reviews_query)) {
?>
  <div>
    <span style="float: left;"><?php echo sprintf(TEXT_REVIEW_DATE_ADDED,  $OSCOM_Date->getLong($reviews['date_added'])); ?></span>
  </div>
  <div>
    <br />
    <h3><span style="float: left;"<?php echo '<a href="' . osc_href_link('product_reviews_info.php', 'products_id=' . $product_info['products_id'] . '&reviews_id=' . $reviews['reviews_id']) . '">' . sprintf(TEXT_REVIEW_BY, osc_output_string_protected($reviews['customers_name'])) . '</a>'; ?></h3>
  </div>
   <div><br /><br /></div>
   <div class="contentText">
    <?php echo osc_break_string(osc_output_string_protected($reviews['reviews_text']), 60, '-<br />') . ((strlen($reviews['reviews_text']) >= 100) ? '..' : '') . '<br /><br /><i>' .  osc_draw_stars($products['reviews_rating']), sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating']) . '</i>'; ?>
  </div>

<?php
    }
  } else {
// ---------------------- 
// --- No review   -----  
// ---------------------- 
?>

  <div class="contentText">
    <div class="alert alert-info">
    <?php echo TEXT_NO_REVIEWS; ?>
    </div>
  </div>
<?php
  }
  if (($reviews_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
// ---------------------- 
// --- Count Review   -----  
// ---------------------- 
?>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-sm-6 pagenumber hidden-xs">
        <?php echo $products_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?>
      </div>
      <div class="col-sm-6">';
        <div class="pull-right pagenav"><ul class="pagination"><?php echo $products_split->display_links(MAX_DISPLAY_PAGE_LINKS, osc_get_all_get_params(array('page', 'info'))); ?></ul></div>
        <span class="pull-right"><?php echo TEXT_RESULT_PAGE; ?></span>
      </div>
    </div>
<?php
  }
?>
<?php
// ---------------------- 
// --- button   -----  
// ---------------------- 
?>

    <div class="clearfix"></div>
    <div class="buttonSet row">
      <div class="col-xs-6"<?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('product_info.php', osc_get_all_get_params()), 'info'); ?></div>
      <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_WRITE_REVIEW, null, osc_href_link('product_reviews_write.php', osc_get_all_get_params()), 'success'); ?></div>
    </div>
  </div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>