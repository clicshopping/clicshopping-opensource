<?php
/*
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
?>

<script type="text/javascript" src="<?php echo DIR_WS_SOURCES; ?>javascript/clicshopping/general.js"></script>

<?php
  if ( $OSCOM_MessageStack->exists('search') ) {
    echo $OSCOM_MessageStack->get('search');
  }
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
  echo osc_draw_form('advanced_search', osc_href_link('advanced_search_result.php', '', 'NONSSL', false), 'get', 'onsubmit="return check_form(this);"') . osc_hide_session_id();
?>
<div class="contentContainer">
  <legend><?php echo HEADING_SEARCH_CRITERIA; ?></legend>
  <div class="contentText">
    <?php echo $OSCOM_Template->getBlocks('modules_advanced_search'); ?>
  </div>
  <div class="clearfix"></div>
  <div style="padding-top:10px;"></div>
  <span style="float: right;"><?php echo osc_draw_button(IMAGE_BUTTON_SEARCH, null, null, 'success'); ?></span>
</div>
</form>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer'));?>