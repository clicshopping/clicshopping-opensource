<?php
/*
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('advanced_search'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('advanced_search.php'));

  require($OSCOM_Template->getTemplateFiles('advanced_search'));
