<?php
/*
 * stats_discount_coupons.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Rapport Statistiques concernant les coupons');
define('HEADING_COUPONS_LIST', 'Tous les coupons');
define('HEADING_CUSTOMERS_LIST', 'Clients qui ont utilisé un ou plusieurs coupons %s');
define('HEADING_ORDERS_LIST', 'Commande client %s utilisant le coupon %s');

define('TABLE_HEADING_CODE', 'Code');
define('TABLE_HEADING_PERCENTAGE', 'Montant de la remise');
define('TABLE_HEADING_CUSTOMER', 'Nom du client');
define('TABLE_HEADING_ORDER', 'Numéro de commande');
define('TABLE_HEADING_MAX_USE', 'Nombre d\'utilisation par client' );
define('TABLE_HEADING_USE_STILL_AVAIL', 'Nombre restant utilisable' );
define('TABLE_HEADING_NUM_AVAIL', 'Nombre disponible');
define('TABLE_HEADING_NUM_STILL_AVAIL', 'Nombre total restant disponible');
define('TABLE_HEADING_USE_COUNT', 'Nombre utilisé');
define('TABLE_HEADING_CUSTOMERS', 'Client');
define('TABLE_HEADING_ORDER_TOTAL', 'Total des commandes');
define('TABLE_HEADING_ORDER_DISCOUNT', 'Remise commande');
define('TABLE_HEADING_DATE_PURCHASED', 'Date d\'achat');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_CONTENT_UNLIMITED','illimité');
define('TEXT_DISPLAY_SHIPPING_DISCOUNT', 'off shipping');
?>