<?php
/*
 * account_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  echo osc_draw_form('account_notifications', osc_href_link('account_notifications.php', '', 'SSL'), 'post', '', true) . osc_draw_hidden_field('action', 'process');
// ---------------------- ------------
// --- Notification description   -----  
// ---------------------- -------------
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<div class="contentContainer">
  <div class="contentText">
    <?php echo MY_NOTIFICATIONS_DESCRIPTION; ?>
  </div>

  <h3><?php echo GLOBAL_NOTIFICATIONS_TITLE; ?></h3>

  <div class="contentText">
    <div class="checkbox">
      <label>
        <?php echo osc_draw_checkbox_field('product_global', '1', (($global['global_product_notifications'] == '1') ? true : false)); ?>
         <span style="padding-top: 20px;"><strong><?php echo GLOBAL_NOTIFICATIONS_TITLE; ?></strong></span>
      </label>
    </div>
  </div>
<?php
// ---------------------- 
// --- Notification   -----  
// ---------------------- 
  if ($global['global_product_notifications'] != '1') {
?>
  <h3><?php echo NOTIFICATIONS_TITLE; ?></h3>
  <div class="contentText">
<?php
    $QCheck= $OSCOM_PDO->prepare('select count(*)
                                  from :table_products_notifications
                                  where customers_id = :customers_id
                                ');
    $QCheck->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    $QCheck->execute();

    if ($QCheck->rowCount() > 0) {
?>
      <div><?php echo NOTIFICATIONS_DESCRIPTION; ?></div>
      <div class="clearfix"></div>
<?php
// Select the products notification
      $counter = 0;

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_id,
                                             pd.products_name
                                      from :table_products_description pd,
                                           :table_products_notifications pn
                                      where pn.customers_id = :customers_id
                                      and pn.products_id = pd.products_id
                                      and pd.language_id = :language_id
                                      order by pd.products_name
                                      ');
      $Qproducts->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qproducts->execute();

      while ($products = $Qproducts->fetch()) {
?>
        <div class="contentText">
          <div class="checkbox">
            <label>
              <?php echo osc_draw_checkbox_field('products[' . $counter . ']', $products['products_id'], true); ?>
              <span style="padding-top: 20px;"><strong><?php echo $products['products_name'] ?></strong></span>
            </label>
          </div>
        </div>
<?php
        $counter++;
      }
    } else {
?>
      
      <div class="alert alert-warning">
        <?php echo NOTIFICATIONS_NON_EXISTING; ?>
      </div>
<?php
    }
?>
  </div>
<?php
  }
// ---------------------- 
// --- button   -----  
// ---------------------- 
?>

   <div class="buttonSet row">
     <div class="col-xs-6"><?php echo  osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('account.php', '', 'SSL'), 'primary', null, null);  ?></div>
     <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null);  ?></div>
  </div>
</div>
</form>
<?php  require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>