<?php
/*
 * quick_quantity_update.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_MESSAGE','Enregistrez vos modifications AVANT tout changement d\'affichage (tri, page, catégorie, marque...)');
define('TOP_BAR_TITLE', 'Mise à jour rapide');
define('HEADING_TITLE', 'Mise à jour rapide');
define('TEXT_PRODUCTS_UPDATED', 'Article(s) Mis à Jour!');
define('TEXT_IMAGE_PREVIEW','Visualiser');
define('TEXT_IMAGE_SWITCH_EDIT','Bascule vers l\'édition de la fiche produit');
define('TEXT_QTY_UPDATED', 'modification(s)');
define('TEXT_INPUT_SPEC_PRICE','<strong>Augmenter / diminuer les tarifs (cliquez sur visualiser) :</strong><br /><i>(Exemple: 10, 15%, -20, -25%..)</i>');
define('TEXT_SPEC_PRICE_INFO_UPDATE','<strong>Veuillez décochez les prix que vous ne souhaitez pas modifier<br />ou tout annuler en cliquant sur le bouton "Annuler"');
define('TEXT_MAXI_ROW_BY_PAGE', 'Nombre de lignes à afficher :');
define('TEXT_SPECIALS_PRODUCTS', 'Produit en Promotion !');
define('TEXT_ALL_MANUFACTURERS', 'Toutes les marques');
define('TEXT_ALL_SUPPLIERS', 'Tous les fournisseurs');
define('TEXT_INSERT', '');
define('TEXT_SEARCH','Rechercher (produit ou réf.)');
define('NO_PRICE','-- NC --');
define('NO_TAX_TEXT','Aucune');
define('NO_MANUFACTURER','Aucun');
define('NO_SUPPLIER','Aucun');
define('TABLE_HEADING_CATEGORIES', 'Catégorie');
define('TABLE_HEADING_PRICE_COMPARISON','Comparateurs (Oui/Non)');
define('TABLE_HEADING_MODEL', 'Réf');
define('TABLE_HEADING_PRODUCTS', 'Nom');
define('TABLE_HEADING_PRICE', 'Prix public');
define('TABLE_HEADING_TAX', 'Type taxe');
define('TABLE_HEADING_WEIGHT', 'Poids');
define('TABLE_HEADING_QUANTITY', 'Stock');
define('TABLE_HEADING_MIN_ORDER_QUANTITY', 'Qté min cmd');
define('TABLE_HEADING_MANUFACTURERS', 'Marques');
define('TABLE_HEADING_SUPPLIERS','Fournisseurs');
define('TABLE_HEADING_IMAGE', 'Image');
define('TABLE_HEADING_STATUS', 'Statut Off/On');
define('TABLE_HEADING_PRODUCTS_ONLY_ONLINE','Exclusivif web (Non/Oui)');
define('DISPLAY_CATEGORIES', 'Catégories à afficher :');
define('DISPLAY_MANUFACTURERS', 'Marques à afficher :');
define('DISPLAY_SUPPLIERS', 'Fournisseurs à afficher :');
define('PRINT_TEXT', 'Imprimer cette page');
define('TOTAL_COST', 'Prix public Toutes taxes');
define('TEXT_SORT_ALL', 'Trié par ');
define('TEXT_DESCENDINGLY', '- Descendant');
define('TEXT_ASCENDINGLY', '- Ascendant');
define('TEXT_BY', ' par ');
define('EDIT','Imprimer');
define('TEXT_QUICK_UPDATES_SPECIALS','Vous ne pouvez pas modifier le prix public car le produit est actuellement en promotion.');
define('TEXT_QUICK_UPDATES_LOCKED','Vous ne pouvez pas modifier le prix public car les prix en mode B2B sont en mode manuel. Veuillez éditer directement la fiche produit pour effectuer vos modifications.');
define('TEXT_NO_TAXE','&nbsp;HT');
?>
