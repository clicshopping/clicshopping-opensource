<?php
/*
 * ar_contact_us.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  class ar_contact_us {
    var $code = 'ar_contact_us';
    var $title;
    var $description;
    var $sort_order = 0;
    var $minutes = 15;
    var $identifier;

    function ar_contact_us() {
      $this->title = MODULE_ACTION_RECORDER_CONTACT_US_TITLE;
      $this->description = MODULE_ACTION_RECORDER_CONTACT_US_DESCRIPTION;

      if ($this->check()) {
        $this->minutes = (int)MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES;
        $this->attempts = 3; // nbr de possiblite d'envoi d'email
      }
    }

    function setIdentifier() {
      $this->identifier = osc_get_ip_address();
    }

    function canPerform($user_id, $user_name) {
     global $OSCOM_PDO;

      $sql_query = 'select id 
                    from :table_action_recorder 
                    where module = :module
                   ';

      if (!empty($user_id)) {
        $sql_query .= ' and (user_id = :user_id or identifier = :identifier)';
      } else {
        $sql_query .= ' and identifier = :identifier';
      }

      $sql_query .= ' and date_added >= date_sub(now(), 
                      interval :limit_minutes minute) 
                      and success = 1 
                      limit 1
                      ';

      $Qcheck = $OSCOM_PDO->prepare($sql_query);
      $Qcheck->bindValue(':module', $this->code);

      if (!empty($user_id)) {
        $Qcheck->bindInt(':user_id', $user_id);
      }

      $Qcheck->bindValue(':identifier', $this->identifier);
      $Qcheck->bindInt(':limit_minutes', $this->minutes);
      $Qcheck->execute();

      if ($Qcheck->fetch() !== false) {
        return false;
      }

      return true;
    }

    function expireEntries() {
      global $OSCOM_PDO, $db_link;
      
      $Qdel = $OSCOM_PDO->prepare('delete
                                  from :table_action_recorder 
                                  where module = :module 
                                  and date_added < date_sub(now(), 
                                  interval :limit_minutes minute)
                                ');
      $Qdel->bindValue(':module', $this->code);
      $Qdel->bindInt(':limit_minutes', $this->minutes);
      $Qdel->execute();

      return $Qdel->rowCount();
    }

    function check() {
      return defined('MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES');
    }

    function install() {
     global $OSCOM_PDO; 
     
      if ($_SESSION['languages_id'] =='1') {
        $OSCOM_PDO->save('configuration', [
                                          'configuration_title' => 'Quel d&eacute;lais minimum souhaitez-vous concernant la gestion des emails par le module contactez-nous',
                                          'configuration_key' => 'MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES',
                                          'configuration_value' => '15',
                                          'configuration_description' => 'Veuillez indiquer un nombre minimum pour permettre la gestion des envois des emails<br /><font color=red><br /><strong>Note :</strong></font>15 pour la gestion des envois des emails tous les 15 minutes (cas de spam)',
                                          'configuration_group_id' => '6',
                                          'sort_order' => '0',
                                          'date_added' => 'now()'
                                        ]
                        );

      } else {

        $OSCOM_PDO->save('configuration', [
                                          'configuration_title' => 'Minimum Minutes Per E-Mail',
                                          'configuration_key' => 'MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES',
                                          'configuration_value' => '15',
                                          'configuration_description' => 'Minimum number of minutes to allow 1 e-mail to be sent (eg, 15 for 1 e-mail every 15 minutes)',
                                          'configuration_group_id' => '6',
                                          'sort_order' => '0',
                                          'date_added' => 'now()'
                                        ]
                        );
      }
    }

    function remove() {
     global $OSCOM_PDO;
    
     $Qdel = $OSCOM_PDO -> prepare('delete
                                    from :table_configuration
                                    where configuration_key in  :configuration_key
                                  ');
      $Qdel->bindValue(':configuration_key', implode("', '", $this->keys() ) );

      $Qdel->execute();
    }

    function keys() {
      return array('MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES');
    }
  }
