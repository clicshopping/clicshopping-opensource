<?php
/*
 * banner_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $banner_extension = osc_banner_image_extension();

  if (osc_not_null($action)) {
// Register global off
 if (isset($_GET['bID'])) {
   $bID = (int)$_GET['bID'];
 }
    switch ($action) {
      case 'setflag':
        if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
          osc_set_banner_status($_GET['bID'], $_GET['flag']);

          $OSCOM_MessageStack->add_session(SUCCESS_BANNER_STATUS_UPDATED, 'success');
        } else {
          $OSCOM_MessageStack->add_session(ERROR_UNKNOWN_STATUS_FLAG, 'error');
        }

        osc_redirect(osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $_GET['bID']));
        break;
      case 'insert':
      case 'update':

        if (isset($_POST['banners_id'])) $banners_id = osc_db_prepare_input($_POST['banners_id']);

        $banners_title = osc_db_prepare_input($_POST['banners_title']);
        $banners_title_admin = osc_db_prepare_input($_POST['banners_title_admin']);

        $banners_url = osc_db_prepare_input($_POST['banners_url']);

        $new_banners_group = osc_db_prepare_input($_POST['new_banners_group']);
        $banners_group = (empty($new_banners_group)) ? osc_db_prepare_input($_POST['banners_group']) : $new_banners_group;

        $banners_target = osc_db_prepare_input($_POST['banners_target']);
        $banners_html_text = osc_db_prepare_input($_POST['banners_html_text']);

        $banners_image_local = osc_db_prepare_input($_POST['banners_image_local']);
        $banners_image_target = osc_db_prepare_input($_POST['banners_image_target']);
        $db_image_location = '';

        $expires_date = osc_db_prepare_input($_POST['expires_date']);
        $expires_impressions = osc_db_prepare_input($_POST['expires_impressions']);
        $date_scheduled = osc_db_prepare_input($_POST['date_scheduled']);

        $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);
        $language_id = osc_db_prepare_input($_POST['languages_id']);

        $banner_error = false;

        if (empty($banners_title)) {
          $OSCOM_MessageStack->add(ERROR_BANNER_TITLE_REQUIRED, 'error');
          $banner_error = true;
        }

// Insertion de l'image de la banniere

        if (isset($_POST['banners_image_local']) && osc_not_null($_POST['banners_image_local']) && ($_POST['banners_image_local'] != 'none')) {

          $banners_image_local = htmlspecialchars($banners_image_local);
          $banners_image_local = strstr($banners_image_local, DIR_WS_CATALOG_IMAGES);
          $banners_image_local = str_replace(DIR_WS_CATALOG_IMAGES, '', $banners_image_local);
          $banners_image_local_end = strstr($banners_image_local, '&quot;');
          $banners_image_local = str_replace($banners_image_local_end, '', $banners_image_local);          
          $banners_image_local = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $banners_image_local);

        } else {
          $banners_image_local = 'null';
        }

        if ($_POST['delete_image'] == 'yes') {
          $banners_image_local = '';
        }

        if (empty($banners_group)) {
          $OSCOM_MessageStack->add(ERROR_BANNER_GROUP_REQUIRED, 'error');
          $banner_error = true;
        }

        if (empty($banners_html_text)) {
          if ((empty($banners_image_local)) && (osc_not_null($banners_image_local))) {
            $banners_image = new upload('banners_image');
            $banners_image->set_destination(DIR_FS_CATALOG_IMAGES .'banners/'. $banners_image_target);
            if ( ($banners_image->parse() == false) || ($banners_image->save() == false) ) {
              $banner_error = true;
            }
          }
        }

        if ($banner_error == false) {

          $db_image_location = (osc_not_null($banners_image_local)) ? $banners_image_local : $banners_image_target . $banners_image->filename;

          $sql_data_array = array('banners_title' => $banners_title,
                                  'banners_url' => $banners_url,
                                  'banners_group' => $banners_group,
                                  'banners_target' => $banners_target,
                                  'customers_group_id' => $customers_group_id,
                                  'languages_id' => $language_id,
                                  'banners_html_text' => $banners_html_text,
                                  'expires_date' => 'null',
                                  'expires_impressions' => 0,
                                  'date_scheduled' => 'null',
                                  'banners_title_admin' => $banners_title_admin
                                  );

          if ($banners_image_local != 'null') {
            $insert_image_sql_data = array('banners_image' => $db_image_location);
            $sql_data_array = array_merge($sql_data_array, $insert_image_sql_data);
          }

          if ($action == 'insert') {
            $insert_sql_data = array('date_added' => 'now()',
                                     'status' => '1'
                                    );

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            $OSCOM_PDO->save('banners', $sql_data_array);

            $banners_id = $OSCOM_PDO->lastInsertId();

            $OSCOM_MessageStack->add_session(SUCCESS_BANNER_INSERTED, 'success');
// update

          } elseif ($action == 'update') {

            $OSCOM_PDO->save('banners', $sql_data_array, array('banners_id' => (int)$banners_id ) );

            $OSCOM_MessageStack->add_session(SUCCESS_BANNER_UPDATED, 'success');
          }

          if (osc_not_null($expires_date)) {
            $expires_date = substr($expires_date, 0, 4) . substr($expires_date, 5, 2) . substr($expires_date, 8, 2);

            $Qupdate = $OSCOM_PDO->prepare('update :table_banners
                                            set expires_date = :expires_date, 
                                                expires_impressions = :expires_impressions
                                            where banners_id = :banners_id
                                          ');
            $Qupdate->bindValue(':expires_date', $expires_date);
            $Qupdate->bindValue(':expires_impressions', null);           
            $Qupdate->bindInt(':banners_id', (int)$banners_id);
            $Qupdate->execute();

          } elseif (osc_not_null($expires_impressions)) {

            $Qupdate = $OSCOM_PDO->prepare('update :table_banners
                                            set expires_impressions = :expires_impressions, 
                                                expires_date = :expires_date
                                            where banners_id = :banners_id
                                          ');
            $Qupdate->bindValue(':expires_impressions', $expires_impressions);
            $Qupdate->bindValue(':expires_date', null);
            $Qupdate->bindInt(':banners_id', (int)$banners_id);
            $Qupdate->execute();
          }

// date debut
          if (osc_not_null($date_scheduled)) {
            $date_scheduled = substr($date_scheduled, 0, 4) . substr($date_scheduled, 5, 2) . substr($date_scheduled, 8, 2);

            $Qupdate = $OSCOM_PDO->prepare('update :table_banners
                                            set status = :status, 
                                                date_scheduled = :date_scheduled
                                            where banners_id = :banners_id
                                          ');
            $Qupdate->bindInt(':status', '0');
            $Qupdate->bindValue(':date_scheduled',$date_scheduled);
            $Qupdate->bindInt(':banners_id', (int)$banners_id);
            $Qupdate->execute();
         }

          osc_redirect(osc_href_link('banner_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'bID=' . $banners_id));

        } else {
          $action = 'new';
        }
        break;

        case 'copy_to':
        if (isset($_GET['bID'])) {

          $banners_page = $_GET['page'];

          $QbannersCopy = $OSCOM_PDO->prepare('select banners_title,
                                                     banners_url,
                                                     banners_image,
                                                     banners_group,
                                                     banners_target,
                                                     banners_html_text,
                                                     expires_impressions,
                                                     date_format(date_scheduled, "%Y/%m/%d") as date_scheduled,
                                                     date_format(expires_date, "%Y/%m/%d") as expires_date,
                                                     date_added,
                                                     date_status_change,
                                                     status,
                                                     customers_group_id,
                                                     languages_id,
                                                     banners_title_admin
                                              from :table_banners
                                              where banners_id = :banners_id
                                              ');
          $QbannersCopy->bindInt(':banners_id', $_GET['bID']);
          $QbannersCopy->execute();

          $banners_copy = $QbannersCopy->fetch();

          $OSCOM_PDO->save('banners', [
                                      'banners_title' => $banners_copy['banners_title'],
                                      'banners_url' => $banners_copy['banners_url'],
                                      'banners_image' => $banners_copy['banners_image'],
                                      'banners_group' => $banners_copy['banners_group'],
                                      'banners_target' => $banners_copy['banners_target'],
                                      'banners_html_text' => $banners_copy['banners_html_text'],
                                      'expires_impressions' => $banners_copy['expires_impressions'],
                                      'date_scheduled' => (empty($banners_copy['date_scheduled']) ? "null" : "'" . $banners_copy['date_scheduled'] . "'"),
                                      'expires_date' => (empty($banners_copy['expires_date']) ? "null" : "'" . ($banners_copy['expires_date']) . "'"),
                                      'date_added' => 'now()',
                                      'date_status_change' => (empty($banners_copy['date_status_change']) ? "null" : "'" . $banners_copy['date_status_change'] . "'"),
                                      'status' => 0,
                                      'customers_group_id' => $banners_copy['customers_group_id'],
                                      'languages_id' => $banners_copy['languages_id'],
                                      'banners_title_admin' => $banners_copy['banners_title_admin']
                                    ]
                           );
        }

          osc_redirect(osc_href_link('banner_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'bID=' . $banners_copy_id1));
      break;

      case 'deleteconfirm':
        $banners_id = $_GET['bID'];
        $banner_id1 = (int)$bID;

        if (isset($_POST['delete_image']) && ($_POST['delete_image'] == 'on')) {

          $Qbanner = $OSCOM_PDO->prepare('select banners_image 
                                          from :table_banners
                                          where banners_id = :banners_id
                                      ');
          $Qbanner->bindInt(':banners_id',  (int)$banner_id1);
          $Qbanner->execute();

          $banner = $Qbanner->fetch();

// delete image
          if (is_file(DIR_FS_CATALOG_IMAGES . $banner['banners_image'])) {
            if (osc_is_writable(DIR_FS_CATALOG_IMAGES . $banner['banners_image'])) {
              unlink(DIR_FS_CATALOG_IMAGES . $banner['banners_image']);
            } else {
              $OSCOM_MessageStack->add_session(ERROR_IMAGE_IS_NOT_WRITEABLE, 'error');
            }
          } else {
            $OSCOM_MessageStack->add_session(ERROR_IMAGE_DOES_NOT_EXIST, 'error');
          }
        }

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_banners
                                        where  banners_id = :banners_id
                                      ');
        $Qdelete->bindInt(':banners_id', (int)$banners_id   );
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_banners_history
                                        where  banners_id = :banners_id
                                      ');
        $Qdelete->bindInt(':banners_id', (int)$banners_id   );
        $Qdelete->execute();

        if (function_exists('imagecreate') && osc_not_null($banner_extension)) {
          if (is_file(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banners_id . '.' . $banner_extension)) {
            if (osc_is_writable(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_infobox-' . $banners_id . '.' . $banner_extension);
            }
          }

          if (is_file(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banners_id . '.' . $banner_extension)) {
            if (osc_is_writable(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banners_id . '.' . $banner_extension);
            }
          }

          if (is_file(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banners_id . '.' . $banner_extension)) {
            if (osc_is_writable(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banners_id . '.' . $banner_extension);
            }
          }

          if (is_file(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banners_id . '.' . $banner_extension)) {
            if (osc_is_writable(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banners_id . '.' . $banner_extension)) {
              unlink(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banners_id . '.' . $banner_extension);
            }
          }
        }

        $OSCOM_MessageStack->add_session(SUCCESS_BANNER_REMOVED, 'success');

        osc_redirect(osc_href_link('banner_manager.php', 'page=' . $_GET['page']));
        break;
    }
  }

// check if the graphs directory exists
  $dir_ok = false;
  if (function_exists('imagecreate') && osc_not_null($banner_extension)) {
    if (is_dir(DIR_WS_IMAGES . 'graphs')) {
      if (osc_is_writable(DIR_WS_IMAGES . 'graphs')) {
        $dir_ok = true;
      } else {
        $OSCOM_MessageStack->add(ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE, 'error');
      }
    } else {
      $OSCOM_MessageStack->add(ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST, 'error');
    }
  }

  require('includes/header.php');
?>
<script type="text/javascript"><!--
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>

<!-- header_eof //-->

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>


          <div>
            <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
            <div class="adminTitle">
              <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/banner_manager.gif', HEADING_TITLE, '40', '40'); ?></span>
              <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if ($action != 'new') {
?>
               <span class="col-md-2"></span>
                <span class="pull-left">
                  <div class="form-group">
                    <div class="controls">
                      <?php echo osc_draw_form('search', 'banner_manager.php', '', 'get'); ?>
                      <?php echo osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"'); ?>
                      </form>
                    </div>
                  </div>
                </span>
<?php
  if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
?>
                <span><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></span>
                <span class="pull-left" style="padding-left:5px;"><?php echo '<a href="' . osc_href_link('banner_manager.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></span>
<?php
  }
  echo osc_hide_session_id();
?>
                <span class="pull-right"><?php echo '<a href="' . osc_href_link('banner_manager.php', 'action=new') . '">' . osc_image_button('button_new_banner.gif', IMAGE_NEW_BANNER) . '</a>'; ?></span>
<?php
  } else {
    $form_action = 'insert';

    if (isset($_GET['bID'])) {
      $form_action = 'update';
    }

    echo osc_draw_form('new_banner', 'banner_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'action=' . $form_action, 'post', 'enctype="multipart/form-data"');

    if ($form_action == 'update') echo osc_draw_hidden_field('banners_id', $bID);

    if ($form_action == 'insert') {
?>
      <span class="pull-right" style="padding-left:5px;"><?php echo osc_image_submit('button_insert_banner.gif', IMAGE_INSERT); ?></span>
<?php
    } else {
?>
      <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></span>
<?php
    }
?>
        <span><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></span>
        <span class="pull-right" style="padding-left:5px;"><?php echo '<a href="' . osc_href_link('banner_manager.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . (isset($_GET['bID']) ? 'bID=' . $_GET['bID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
<?php
  }
?>
            </div>
            <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
          </div>
          <div class="clearfix"></div>
        </tr>
        <!-- //################################################################################################################ -->
        <!-- //                                      EDITION ET INSERTION D'UNE BANNIERE -->
        <!-- //################################################################################################################ -->
<?php
  if ($action == 'new') {
// Insert a new banner
    $form_action = 'insert';

    $parameters = array('expires_date' => '',
                        'date_scheduled' => '',
                        'banners_title' => '',
                        'banners_url' => '',
                        'banners_group' => '',
                        'banners_target' => '',
                        'banners_image' => '',
                        'banners_html_text' => '',
                        'expires_impressions' => '',
                        'banners_title_admin' => ''
                        );

    $bInfo = new objectInfo($parameters);
// Update a banner
    
    if (isset($_GET['bID'])) {
      $form_action = 'update';

      $bID = osc_db_prepare_input($_GET['bID']);

      $Qbanner = $OSCOM_PDO->prepare('select banners_title,
                                           banners_url,
                                           banners_image,
                                           banners_group,
                                           banners_target,
                                           banners_html_text,
                                           status,
                                           date_format(date_scheduled, "%Y-%m-%d") as date_scheduled,
                                           date_format(expires_date, "%d/%m/%Y") as expires_date,
                                           expires_impressions,
                                           date_status_change ,
                                           customers_group_id,
                                           languages_id,
                                           banners_title_admin
                                    from :table_banners
                                    where banners_id = :banners_id
                                    ');
      $Qbanner->bindInt(':banners_id', (int)$bID);
      $Qbanner->execute();

      $banner = $Qbanner->fetch();

      $bInfo->objectInfo($banner);
    } elseif (osc_not_null($_POST)) {
      $bInfo->objectInfo($_POST);
    }

// Put languages information into an array for drop-down boxes
    $customers_group = osc_get_customers_group();

    $customers_group[] = array('id'=>'99',
                               'text'=> TEXT_ALL_GROUPS
                              );
    $values_customers_group_id[0] = array('id'=>0,
                                         'text'=> TEXT_ALL_CUSTOMERS
                                         );

    for ($i=0, $n=sizeof($customers_group); $i<$n; $i++) {
      $values_customers_group_id[$i+1] = array ('id' =>$customers_group[$i]['id'],
                                                'text' =>$customers_group[$i]['text']);
    }

// Put languages information into an array for drop-down boxes
  $languages = osc_get_languages();
  $values_languages_id[0]=array ('id' =>'0',
                                  'text' => TEXT_ALL_LANGUAGES);
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
    $values_languages_id[$i+1]=array ('id' =>$languages[$i]['id'],
                                      'text' =>$languages[$i]['name']);
  }

    $groups_array = array();

    $Qgroups = $OSCOM_PDO->prepare('select distinct banners_group
                                    from :table_banners
                                    order by banners_group
                                 ');
    $Qgroups->execute();

    while ($groups = $Qgroups->fetch() ) {
      $groups_array[] = array('id' => $groups['banners_group'],
                               'text' => $groups['banners_group']);
    }

// reactions au niveau du clique
    $banners_target_array = array(array('id' => '_self', 'text' => TEXT_BANNERS_SAME_WINDOWS),
                                  array('id' => '_blank', 'text' => TEXT_BANNERS_NEW_WINDOWS));

?>
        <tr>
          <td>
            <div>
              <ul class="nav nav-tabs" role="tablist"  id="myTab">
                <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
                <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_IMG; ?></a></li>
                <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_CODE_HTML; ?></a></li>
              </ul>
              <div class="tabsClicShopping">
                <div class="tab-content">
<!-- ----------------------------------------------------------- //-->
<!--          ONGLET Information General de la Banniere          //-->
<!-- ----------------------------------------------------------- //-->
                  <div class="tab-pane active" id="tab1">
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td class="mainTitle"><?php echo TITLE_BANNERS_GENERAL; ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table border="0" cellspacing="0" cellpadding="2">
                            <tr>
                              <td class="main"><?php echo TEXT_BANNERS_TITLE; ?></td>
                              <td class="main"><?php echo osc_draw_input_field('banners_title', $bInfo->banners_title, 'required aria-required="true" id="banner_title"', true); ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo TEXT_BANNERS_TITLE_ADMIN; ?></td>
                              <td class="main"><?php echo osc_draw_input_field('banners_title_admin', $bInfo->banners_title_admin, 'required aria-required="true" id="banner_title"', true); ?></td>
                             </tr>
                            <tr>
                              <td class="main"><?php echo TEXT_BANNERS_URL; ?></td>
                              <td class="main"><?php echo osc_draw_input_field('banners_url', $bInfo->banners_url); ?></td>
                            </tr>
                            <tr>
                              <td class="main" valign="top"><?php echo TEXT_BANNERS_TARGET; ?></td>
                              <td class="main"><?php echo osc_draw_pull_down_menu('banners_target', $banners_target_array, $bInfo->banners_target); ?></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                      <tr>
                        <td class="mainTitle"><?php echo TITLE_BANNERS_GROUPE; ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
                          <tr>
                            <td class="main" valign="top"><?php echo TEXT_BANNERS_CUSTOMERS_GROUP; ?></td>
                            <td class="main" align="left"><?php echo  osc_draw_pull_down_menu('customers_group_id', $values_customers_group_id,  $bInfo->customers_group_id); ?></td>
                          </tr>
<?php
	}
?>
                          <tr>
                            <td class="main" valign="top"><?php echo TEXT_BANNERS_LANGUAGE; ?></td>
                            <td class="main" align="left"><?php echo osc_draw_pull_down_menu('languages_id', $values_languages_id,  $bInfo->languages_id);?></td>
                          </tr>
                          <tr>
                            <td class="main" valign="top"><?php echo TEXT_BANNERS_GROUP; ?></td>
                            <td class="main"><?php echo osc_draw_pull_down_menu('banners_group', $groups_array, $bInfo->banners_group); ?></td>
                          </tr>
                          <tr>
                            <td class="main" valign="top"><?php echo TEXT_BANNERS_NEW_GROUP; ?></td>
                            <td class="main"><?php echo osc_draw_input_field('new_banners_group', '', '', ((sizeof($groups_array) > 0) ? false : true)); ?></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                      <tr>
                        <td class="mainTitle"><?php echo TITLE_BANNERS_DATE; ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td class="main"><?php echo TEXT_BANNERS_SCHEDULED_AT; ?></td>
                              <td valign="top" class="main"><?php echo osc_draw_input_field('date_scheduled', $bInfo->date_scheduled, 'id="schdate"'); ?></td>
                            </tr>
                            <tr>
                              <td valign="top" class="main"><?php echo TEXT_BANNERS_EXPIRES_ON; ?><br /></td>
                             <td class="main"><?php echo osc_draw_input_field('expires_date', $bInfo->expires_date, 'id="expdate"') .'&nbsp;'. TEXT_BANNERS_OR_AT . '&nbsp;' . TEXT_BANNERS_IMPRESSIONS . '&nbsp;' . osc_draw_input_field('expires_impressions', $bInfo->expires_impressions, 'maxlength="7" size="7"') . ' '; ?></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_BANNERS_IMAGE); ?></td>
                              <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_BANNERS_DATE; ?></strong></td>
                            </tr>
                            <tr>
                              <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                              <td class="main"><?php echo TEXT_BANNERS_EXPIRCY_NOTE . '<br />' . TEXT_BANNERS_SCHEDULE_NOTE; ?></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                  </div>
<!-- --------------------------------------- //-->
<!--          ONGLET Image banniere          //-->
<!-- --------------------------------------- //-->
                  <div class="tab-pane" id="tab2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td class="mainTitle"><?php echo TITLE_BANNERS_IMAGE; ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td valign="top"><table border="0">
                                  <tr>
                                    <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/banner_manager.gif', TEXT_BANNERS_IMAGE, '40', '40'); ?></td>
                                    <td class="main"><?php echo TEXT_BANNERS_IMAGE; ?></td>
                                  </tr>
                                  <tr>
                                    <td class="main" colspan="2"><?php echo osc_draw_file_field_image_ckeditor('banners_image_local', '400', '200', ''); ?></td>
                                  </tr>
                                </table></td>
                            </tr>
                          </table></td>
                        <td valign="top" width="80%"><table width="100%" border="0">
                            <tr>
                              <td><table>
                                <tr>
                                  <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/banner_manager.gif', TEXT_BANNERS_IMAGE, '40', '40'); ?></td>
                                  <td class="main" align="left"><?php echo TEXT_BANNERS_IMAGE; ?></td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr align="center">
                              <td width="100%" align="center" valign="top"><table width="100%" border="0" class="adminformAide">
                                <tr>
                                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                </tr>
                                <tr>
                                  <td align="center"><?php echo osc_info_image($bInfo->banners_image, TEXT_BANNERS_IMAGE); ?></td>
                                </tr>
                                <tr>
                                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                </tr>
                                <tr>
                                  <td class="main" align="right" colspan="2"><?php echo TEXT_BANNERS_IMAGE_DELETE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                </tr>
                              </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                          <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_BANNERS_IMAGE); ?></td>
                          <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_BANNERS_IMAGE; ?></strong></td>
                        </tr>
                        <tr>
                          <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                          <td class="main"><?php echo TEXT_BANNERS_INSERT_NOTE; ?>
                            <blockquote><strong><i><?php echo '<a href="javascript:popupWysiwyg(\'' . DIR_WS_IMAGES . 'wysiwyg.png' . '\')">' . TEXT_HELP_WYSIWYG . '</a>'; ?></i></strong></blockquote></td>
                        </tr>
                      </table></td>
                  </tr>
                </table>
                  </div>
<!-- -------------------------------------------- //-->
<!--          ONGLET Texte HTML Banniere          //-->
<!-- -------------------------------------------- //-->
                  <div class="tab-pane" id="tab3">
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td class="mainTitle"><?php echo TITLE_BANNERS_HTML; ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td valign="top" class="main"><?php echo TEXT_BANNERS_HTML_TEXT; ?></td>
                              <td class="main"><?php echo osc_draw_textarea_field('banners_html_text', 'soft', '60', '5', $bInfo->banners_html_text); ?></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_BANNERS_IMAGE); ?></td>
                              <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_BANNERS_HTML_TEXT; ?></strong></td>
                            </tr>
                            <tr>
                              <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                              <td class="main"><?php echo TEXT_BANNERS_BANNER_NOTE; ?></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </form>
<?php
  } else {
?>
        <tr>
          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
                    <tr class="dataTableHeadingRow">
                      <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_BANNERS_ADMIN; ?></td>
                      <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_BANNERS; ?></td>
                      <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_GROUPS; ?></td>
                      <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATISTICS; ?></td>
                      <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
                      <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_GROUP; ?></td>
<?php
  }
?>
                      <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LANGUAGE; ?></td>
                      <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                    </tr>
<?php
// Recherche
    $search = '';
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "where  (banners_title like '" . $keywords . "'
                         or banners_title_admin like '" . $keywords . "'        
                         or banners_group like '%" . $keywords . "%' 
                        ) 
               ";
    }

    $banners_query_raw = "select banners_id, 
                                 banners_title, 
                                 banners_image, 
                                 banners_group, 
                                 banners_target,
                                 status, 
                                 expires_date, 
                                 expires_impressions, 
                                 date_status_change, 
                                 date_scheduled, 
                                 date_added,
                                 customers_group_id,
                                 languages_id,
                                 banners_title_admin
                         from banners
                              " . $search . "
                         order by banners_title_admin desc,
                                  banners_title, 
                                  banners_group";

    $banners_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $banners_query_raw, $banners_query_numrows);
    $banners_query = osc_db_query($banners_query_raw);

    while ($banners = osc_db_fetch_array($banners_query)) {

      $Qinfo = $OSCOM_PDO->prepare('select sum(banners_shown) as banners_shown,
                                           sum(banners_clicked) as banners_clicked
                                     from :table_banners_history
                                     where banners_id = :banners_id
                                   ');
      $Qinfo->bindInt(':banners_id', (int)$banners['banners_id']);
      $Qinfo->execute();

      $info = $Qinfo->fetch();

// Permettre l'affichage des groupes en mode B2B
     if (MODE_B2B_B2C == 'true') {

      $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name
                                              from :table_customers_groups
                                              where customers_group_id = :customers_group_id
                                            ');
      $QcustomersGroup->bindInt(':customers_group_id',  (int)$banners['customers_group_id']);
      $QcustomersGroup->execute();

      $customers_group = $QcustomersGroup->fetch();

       if ($banners['customers_group_id'] == '99') {
         $customers_group['customers_group_name'] =  TEXT_ALL_GROUPS;
       } elseif ($banners['customers_group_id'] == '0') {
          $customers_group['customers_group_name'] =  NORMAL_CUSTOMER;
       }
     }

     if ($banners['languages_id'] != '0') {
        $QbannerLanguages = $OSCOM_PDO->prepare('select name 
                                                 from :table_languages
                                                 where languages_id = :languages_id
                                              ');
        $QbannerLanguages->bindInt(':languages_id',  (int)$banners['languages_id']);
        $QbannerLanguages->execute();

        $banner_language = $QbannerLanguages->fetch();

      } else {
        $banner_language['name'] =  TEXT_ALL_LANGUAGES;
      }


      if ((!isset($_GET['bID']) || (isset($_GET['bID']) && ($_GET['bID'] == $banners['banners_id']))) && !isset($bInfo) && (substr($action, 0, 3) != 'new')) {
        $bInfo_array = array_merge($banners, $info);
        $bInfo = new objectInfo($bInfo_array);
      }

      $banners_shown = ($info['banners_shown'] != '') ? $info['banners_shown'] : '0';
      $banners_clicked = ($info['banners_clicked'] != '') ? $info['banners_clicked'] : '0';

      if (isset($bInfo) && is_object($bInfo) && ($banners['banners_id'] == $bInfo->banners_id)) {
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('banner_statistics.php', 'page=' . $_GET['page'] . '&bID=' . $bInfo->banners_id) . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']) . '\'">' . "\n";
      }
?>
                      <td class="dataTableContent"><?php echo $banners['banners_title_admin']; ?></td>
                      <td class="dataTableContent"><?php echo $banners['banners_title']; ?></td>
                      <td class="dataTableContent" align="right"><?php echo $banners['banners_group']; ?></td>
                      <td class="dataTableContent" align="right"><?php echo $banners_shown . ' / ' . $banners_clicked; ?></td>
                      <td class="dataTableContent" align="center">
<?php
      if ($banners['status'] == '1') {
        echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=setflag&flag=0') . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', SET_ACTIVE, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=setflag&flag=1') . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', SET_INACTIVE, 16, 16) . '</a>';
      }
?>
                      </td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
                      <td class="dataTableContent" align="center"><?php echo $customers_group['customers_group_name']; ?></td>
<?php
  }
?>
                      <td class="dataTableContent" align="center"><?php echo $banner_language['name']; ?></td>
                      <td class="dataTableContent" align="right">
<?php
    echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=new') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=copy_to') . '">' . osc_image(DIR_WS_ICONS . 'copy.gif', IMAGE_COPY_TO) . '</a>' ;
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="javascript:popupImageWindow(\'' . 'popup_image.php' . '?banner=' . $banners['banners_id'] . '\')">' . osc_image(DIR_WS_IMAGES . 'icon_popup.gif', ICON_VIEW_BANNER) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('banner_statistics.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']) . '">' . osc_image(DIR_WS_ICONS . 'statistics.gif', ICON_STATISTICS) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    if (isset($bInfo) && is_object($bInfo) && ($banners['banners_id'] == $bInfo->banners_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $banners['banners_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
  ?>
                     </td>
                    </tr>
<?php
    }
?>
                    <tr>
                      <td colspan="8"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr>
                            <td class="smallText" valign="top"><?php echo $banners_split->display_count($banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_BANNERS); ?></td>
                            <td class="smallText" align="right"><?php echo $banners_split->display_links($banners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
<?php
  $heading = array();
  $contents = array();
  switch ($action) {
    case 'delete':
      $heading[] = array('text' => '<strong>' . $bInfo->banners_title . '</strong>');

      $contents = array('form' => osc_draw_form('banners', 'banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $bInfo->banners_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $bInfo->banners_title . '</strong>');
      if ($bInfo->banners_image) $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('delete_image', 'on', true) . ' ' . TEXT_INFO_DELETE_IMAGE);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $_GET['bID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'copy_to':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');
      $contents = array('form' => osc_draw_form('copy_to', 'banners.php',  'page=' . $_GET['page'] . '&bID=' . $bInfo->banners_id . 'action=copy_to_confirm'));
      $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_copy.gif', IMAGE_COPY) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $_GET['bID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    default:
    break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
              </tr>
            </table></td>
        </tr>
<?php
  }
?>
      </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

