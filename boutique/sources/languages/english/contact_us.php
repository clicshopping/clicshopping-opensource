<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Contact Us');
define('NAVBAR_TITLE', 'Contact Us');
define('TEXT_SUCCESS', 'Your enquiry has been successfully sent to the Store Owner.');
define('EMAIL_SUBJECT', 'Enquiry from a client of ' . STORE_NAME);
define('TEXT_ADMINISTRATOR_DEPARTMENT','No department defined');

define('ENTRY_CUSTOMERS_ID','Your Customer number :');
define('ENTRY_ORDER','Your order number : ');
define('ENTRY_DATE','Date : ');
define('ENTRY_CUSTOMERS_PHONE','Phone : ');

define('ENTRY_NEED_CUSTOMERS','Please indicate the service to contact ?');
define('ENTRY_CUSTOMER','Question Customer : ');

define('ENTRY_NOTE_REGISTERED','Please tell us the product model or any relevant information about the problem on the order you have made or your need');
define('ENTRY_NOTE_NO_REGISTERED','If you apply for an order already made, please click: <a href="' . osc_href_link('account.php', '', 'SSL') . '"><u>Login to my account </u></a> then choose your order number');

define('ENTRY_INFORMATION_ADMIN',' A question about an order of a client '. STORE_NAME. ' you requested. '."\n\n".' Please log in to your admin panel (control section) to see this demand. '."\n\n".' Meanwhile the treatment of this issue, the status of the article. were amended and returned by default . '."\n\n".' Please find enclosed information concerning order number  on the customer : ');
define('ENTRY_ADDITIONAL_INFORMATION','For all correspondance with our services, please use the website contact form.');

define('EMAIL_TEXT_LAW', '<p style="font-size:7px; text-decoration: underline;">Confidentiality Note :</p>'. "\n" . '<p style="font-size:7px;">This message as well as the documents which would be appended are addressed exclusively to their recipient and could contain confidential information subjected to the professional secrecy or whose disclosure is prohibited under the terms of the legislation in force. So we inform the person who would receive it without being the recipient or an authorized person, that this information is confidential and that any use, copies, files or disclosure is prohibited.  If you received this message, we ask you to agree to communicate it to us by email : ' . STORE_OWNER_EMAIL_ADDRESS . '. and to proceed directly to its destruction.'. "\n" . '
In accordance with the <strong>Law</strong> no 78-17 of January 6, 1978 known as Data-processing Law and Freedom, you are entitled to correction of your personal data or on request by email</p>');

define('ENTRY_NAME', 'Full Name:');
define('ENTRY_EMAIL', 'E-Mail Address:');
define('ENTRY_ENQUIRY', 'Enquiry:');
define('ENTRY_ENQUIRY_CUSTOMER', 'Your message : ');
define('ENTRY_ENQUIRY_CUSTOMER_INFORMATION', 'Customer message : ');
define('ENTRY_EMAIL_OBJECT_CUSTOMER','Request copy ');
define('ENTRY_CUSTOMERS_SUBJECT','Subject of your request : ');

define('ENTRY_NUMBER_EMAIL_CONFIRMATION', 'How many : ');
define('SEND_DEPARTMENT_COMPANY','Contact to Department :  ');

define('ERROR_ACTION_RECORDER', 'Error: An enquiry has already been sent. Please try again in %s minutes.');
?>