<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('MODULE_SHIPPING_ZONES_TEXT_TITLE', 'Gestion des co&ucirc;ts de livraison par zone et régions');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION', 'Co&ucirc;ts d\'envois par zone et région<br /><br /><font color="#FF0000"><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut');
define('MODULE_SHIPPING_ZONES_TEXT_WAY', 'Livraison vers');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS', 'Kg(s)');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE', 'Cette zone n\'est pas couverte.');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE', 'Le cout de livraison ne peut etre déterminé pour le moment');
?>