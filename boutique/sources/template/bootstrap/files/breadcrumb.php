<?php
/**
 * breadcrumb.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
?>
<div class="contentContainer">
  <div class="contentText">
    <div class="col-xs-12 breadcrumb"><?php echo $OSCOM_Breadcrumb->get(' &raquo; '); ?></div>
<?php
  if ($PHP_SELF != 'product_info.php') {
?>
      <div class="pageHeading"><span class="pageHeading"><h1><?php echo HEADING_TITLE; ?></h1></span></div>
<?php
  }
?>
  </div>
</div>
