<?php
/**
 * @category ip2Country
 * @package ip2Country Class
 * @license http://opensource.org/licenses/GPL-3.0
 */

 class ip2country {
/**
 * Construct Function
 * To setting up the constants and making available for the scope.
 */
  function __construct() {

//ip2Country API URL in format.
    $this->apiURL = 'http://ip2country.sourceforge.net/ip2c.php?format=XML&ip=';

//Europe Country List

    $this->europeCountries = array('AL', 'AD', 'AM', 'AT',
                                   'BY','BE', 'BA', 'BG',
                                   'CH', 'CY','CZ',
                                   'DE', 'DK', 'EE', 'ES',
                                   'FO', 'FI', 'FR',
                                   'GB', 'GE','GI', 'GR',
                                   'HU', 'HR',
                                   'IE','IS', 'IT',
                                   'LT', 'LU', 'LV',
                                   'MC', 'MK', 'MT',
                                   'NO', 'NL',
                                   'PO', 'PT',
                                   'RO', 'RU',
                                   'SE','SI', 'SK', 'SM',
                                   'TR', 'UA', 'VA', 'EU');

//Asia Pacefic Country List

    $this->asianCountries = array('AF', 'CN', 'VN', 'TH', 'CN','AZ', 'BH', 'BD', 'BT', 'BN','KH', 'CY', 'GE', 'IN', 'ID','IR',
                                  'IQ', 'IL', 'JP', 'JO','KZ', 'KP', 'KR', 'KW', 'KG','LA', 'LB', 'MY', 'MV', 'MN','MM', 'NP',
                                  'OM', 'PK', 'PH','QA', 'RU', 'SA', 'SG', 'LK','SY', 'TJ', 'TH', 'TR', 'TM','AE', 'UZ', 'YE',
                                  'HK', 'TW');
    /* Add additional Region here */

    $this->NorthAmericaCountries = array('CA', 'US','MX'); // North America


  }
/**
 * Funtion getRegion
 * scope Public
 * @param none.
 * @return specified region (string).
 */
  public function getRegion() {
    $region = 'No Region Assigned';//setting up the default region No Region, if this happens please add new region/country to the class.
    $myIp = $this->getIpAddress();
    $URL = $this->apiURL . $myIp;
    $data = file_get_contents($URL); //receiving response from the API URL.

    try {
      $xml = new SimpleXMLElement($data);
    } catch(Exception $e){
      $xml->country_code = '';
    }


    if ($xml->country_code) {
      $countryCode = $xml->country_code;

/**
 * Add Region or country checks in below switch-case block.
 * If it is a country add a case block
 * If its a region add the region array in constructor and add the condition in default block.
 */
      switch ($countryCode) {
/*
        case 'US':
          $region = 'United States';
          break;
        case 'CA':
          $region = 'Canada';
          break;
*/


        default:
          if (in_array($countryCode, $this->europeCountries))
            $region = 'EU';
          if (in_array($countryCode, $this->asianCountries))
            $region = 'AP';
          if (in_array($countryCode, $this->NorthAmericaCountries))
            $region = 'NA';
          break;
      }
    }
    return $region;
  }


   public function getCountrycode() {
     $region = 'No Region Assigned';//setting up the default region No Region, if this happens please add new region/country to the class.
     $myIp = $this->getIpAddress();
     $URL = $this->apiURL . $myIp;
     $data = file_get_contents($URL); //receiving response from the API URL.

     try {
       $xml = new SimpleXMLElement($data);
     } catch(Exception $e){
       $xml->country_code = '';
     }


     if ($xml->country_code) {
       $countryCode = $xml->country_code;


       /**
        * Add Region or country checks in below switch-case block.
        * If it is a country add a case block
        * If its a region add the region array in constructor and add the condition in default block.
        */
       /*
       switch ($countryCode) {
         case 'US':
           $region = 'United States';
           break;
         case 'CA':
           $region = 'Canada';
           break;
         default:
           if (in_array($countryCode, $this->europeCountries))
             $region = 'Europe';
           if (in_array($countryCode, $this->asianCountries))
             $region = 'Asia Pacefic';
           break;

       }
     */
     }
     return $countryCode;
   }

  /**
   * Function getIpAddress()
   * scope Protected
   * @param none
   * @return public Ip Address(string)
   * This function will check several possibilities and return the client's public IP.
   */ 
  protected function getIpAddress() {
    if($this->validateIp(getenv('HTTP_CLIENT_IP'))) return getenv('HTTP_CLIENT_IP');
    if ($this->validateIp(getenv('HTTP_X_FORWARDED'))) return getenv('HTTP_X_FORWARDED');
    foreach (explode(',',getenv('HTTP_X_FORWARDED_FOR')) as $ip) {
      if ($this->validateIp($ip)) return $ip;
    }
    if ($this->validateIp(getenv('HTTP_X_CLUSTER_CLIENT_IP'))) return getenv('HTTP_X_CLUSTER_CLIENT_IP');
    if ($this->validateIp(getenv('HTTP_FORWARDED_FOR'))) return getenv('HTTP_FORWARDED_FOR');
    if ($this->validateIp(getenv('HTTP_FORWARDED'))) return getenv('HTTP_FORWARDED');
    if ($this->validateIp(getenv('REMOTE_ADDR'))) return getenv('REMOTE_ADDR');
    $fetchedIp = file_get_contents('http://www.ipmango.com/api/myip'); //get real IP through ipMango API.
    if ($this->validateIp($fetchedIp)) return $fetchedIp;
    return false;
  }
  /**
   * scope Protected
   * @param ipAddress (string)
   * @return true of false (boolean)
   * recieves the ip and put through various checks and if recieved parameter goes through all, returns true indicating its the valid ip.
   */ 
  protected function validateIp($ipAddress) {
    if (empty($ipAddress) || ip2long($ipAddress)==-1 || ip2long($ipAddress)==false) return false;
    
    $privateIpArray = array(
      array('min'=>'0.0.0.0','max'=>'2.255.255.255'),
      array('min'=>'10.0.0.0','max'=>'10.255.255.255'),
      array('min'=>'127.0.0.0','max'=>'127.255.255.255'),
      array('min'=>'169.254.0.0','max'=>'169.254.255.255'),
      array('min'=>'172.16.0.0','max'=>'172.31.255.255'),
      array('min'=>'192.0.2.0','max'=>'192.0.2.255'),
      array('min'=>'192.168.0.0','max'=>'192.168.255.255'),
      array('min'=>'255.255.255.0','max'=>'255.255.255.255')
    );
    foreach($privateIpArray as $pvtIp) {
      if((ip2long($ipAddress)>=ip2long($pvtIp['min'])) && (ip2long($ipAddress) <=ip2long($pvtIp['max']))) return false;
    }
    return true;
  }
  #code
 }
