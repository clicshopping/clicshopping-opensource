<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('MODULE_SHIPPING_TABLE_TEXT_TITLE', 'Frais sur le montant total');
define('MODULE_SHIPPING_TABLE_TEXT_DESCRIPTION', 'Frais sur le montant total de la commande<br /><br /><font color="#FF0000"><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut');
define('MODULE_SHIPPING_TABLE_TEXT_WAY', 'Frais sur le montant total de la commande');
define('MODULE_SHIPPING_TABLE_TEXT_WEIGHT', 'Poids');
define('MODULE_SHIPPING_TABLE_TEXT_AMOUNT', 'Montant');
?>