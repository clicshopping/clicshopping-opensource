<?php
  /**
   * banner.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

/**
 * Sets the status of a banner
 *
 * @param int $id The $banners_id of the banner to set the status to
 * @param boolean $active_flag A flag that enables or disables the banner
 * @access private
 * @return boolean
 */
  function osc_set_banner_status($banners_id, $status) {
    global $OSCOM_PDO;

        if ($status == '1') {

          return $OSCOM_PDO->save('banners', ['status' => 1,
                                             'date_status_change' => 'now()',
                                             'date_scheduled' => 'null'],
                                             ['banners_id' => (int)$banners_id]
                                 );


      } elseif ($status == '0') {
          return $OSCOM_PDO->save('banners', ['status' => 0,
                                              'date_status_change' => 'now()'],
                                             ['banners_id' => (int)$banners_id]
                                  );
      } else {
          return -1;
      }
    }

/**
 * Activate a banner that has been on schedule
 *
 * @param int $id The ID of the banner to activate
 * @access public
 * @return boolean
 */
  function osc_activate_banners() {
    global $OSCOM_PDO, $OSCOM_Date;

      $Qbanners = $OSCOM_PDO->query('select banners_id
                                    from :table_banners
                                    where date_scheduled is not null
                                    and date_scheduled <= now()
                                    and status != 1
                                   ');

      $Qbanners->execute;

      if ($Qbanners->fetch() !== false) {
          do {
            static::SetBannerStatus($Qbanners->valueInt('banners_id'), 1);
          } while ($Qbanners->fetch());
      }
    }

/**
 * Deactivate a banner
 *
 * @param int $id The ID of the banner to deactivate
 * @access public
 * @return boolean
 */
  function osc_expire_banners() {
    global $OSCOM_PDO, $OSCOM_Date;

      $Qbanners = $OSCOM_PDO->query('select b.banners_id,
                                          sum(bh.banners_shown) as banners_shown
                                    from :table_banners b,
                                         :table_banners_history bh
                                    where b.status = 1
                                    and b.banners_id = bh.banners_id
                                    and ((b.expires_date is not null
                                         and now() >= b.expires_date)
                                         or (b.expires_impressions >= banners_shown)
                                        )
                                    group by b.banners_id
                                  ');

      $Qbanners->execute;

      if ($Qbanners->fetch() !== false) {
        do {
          static::SetBannerStatus($Qbanners->valueInt('banners_id'), 0);
        } while ($Qbanners->fetch());
      }
    }

/**
 * Display a banner. If no ID is passed, the value defined in $_exists_id is
 * used.
 *
 * @param int $action of the banner (dynamic or static)
 * @param The $identifier of the banner to show
 * @access public
 * @return string
 */
  function osc_display_banner($action, $identifier) {
    global $OSCOM_Customer, $OSCOM_PDO;

      $banner = null;

      if ($action == 'dynamic') {

        $Qcheck = $OSCOM_PDO->prepare('select banners_id
                                     from :table_banners
                                     where banners_group = :banners_group
                                     and status = :status
                                     limit 1
                                     ');
        $Qcheck->bindValue(':banners_group', $identifier);
        $Qcheck->bindInt(':status', 1);
        $Qcheck->execute();

        if ( $Qcheck !== false ) {

          if ($OSCOM_Customer->getCustomersGroupID() != '0') { // Clients en mode B2B

            $Qbanner = $OSCOM_PDO->prepare('select  banners_id,
                                                    banners_title,
                                                    banners_image,
                                                    banners_target,
                                                    banners_html_text,
                                                    customers_group_id,
                                                    banners_group,
                                                    languages_id
                                           from :table_banners
                                           where banners_group = :banners_group
                                           and status = :status
                                           and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1)
                                           and (languages_id  = :languages_id or languages_id  = :languages_id1)
                                           order by rand()
                                           limit 1
                                        ');


            $Qbanner->bindValue(':banners_group', $identifier);
            $Qbanner->bindInt(':status', 1);
            $Qbanner->bindInt(':customers_group_id',  (int)$OSCOM_Customer->getCustomersGroupID() );
            $Qbanner->bindInt(':customers_group_id1', 99);
            $Qbanner->bindInt(':languages_id', (int)$_SESSION['languages_id']);
            $Qbanner->bindInt(':languages_id1', 0);
            $Qbanner->execute();

            $banner = $Qbanner->fetch();

          } else {

            $Qbanner = $OSCOM_PDO->prepare('select  banners_id,
                                                    banners_title,
                                                    banners_image,
                                                    banners_target,
                                                    banners_html_text,
                                                    customers_group_id,
                                                    banners_group,
                                                    languages_id
                                           from :table_banners
                                           where banners_group = :banners_group
                                           and status = :status
                                           and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1)
                                           and (languages_id  = :languages_id or languages_id  = :languages_id1)
                                           order by rand()
                                           limit 1
                                        ');


            $Qbanner->bindValue(':banners_group', $identifier);
            $Qbanner->bindInt(':status', 1);
            $Qbanner->bindInt(':customers_group_id', 0);
            $Qbanner->bindInt(':customers_group_id1', 99);
            $Qbanner->bindInt(':languages_id', (int)$_SESSION['languages_id']);
            $Qbanner->bindInt(':languages_id1', 0);
            $Qbanner->execute();

            $banner = $Qbanner->fetch();
          }
        }
      } elseif ($action == 'static') {
        if (is_array($identifier)) {
          $banner = $identifier;
        } else {
          if ($OSCOM_Customer->getCustomersGroupID() != '0') { // Clients en mode B2B

            $Qbanner = $OSCOM_PDO->prepare('select  banners_id,
                                                   banners_title,
                                                   banners_image,
                                                   banners_target,
                                                   banners_html_text,
                                                   customers_group_id,
                                                   languages_id
                                           from :table_banners
                                           where status = :status
                                           and banners_group = :banners_group
                                           and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1)
                                           and (languages_id  = :languages_id or languages_id  = :languages_id1)
                                           limit 1
                                         ');
            $Qbanner->bindValue(':banners_group', $identifier);
            $Qbanner->bindInt(':status', 1);
            $Qbanner->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
            $Qbanner->bindInt(':customers_group_id1', 99);
            $Qbanner->bindInt(':languages_id', (int)$_SESSION['languages_id']);
            $Qbanner->bindInt(':languages_id1', 0);
            $Qbanner->execute();

          } else {

            $Qbanner = $OSCOM_PDO->prepare('select  banners_id,
                                                   banners_title,
                                                   banners_image,
                                                   banners_target,
                                                   banners_html_text,
                                                   customers_group_id,
                                                   languages_id
                                           from :table_banners
                                           where status = :status
                                           and banners_group = :banners_group
                                           and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1)
                                           and (languages_id  = :languages_id or languages_id  = :languages_id1)
                                           limit 1
                                         ');
            $Qbanner->bindValue(':banners_group', $identifier);
            $Qbanner->bindInt(':status', 1);
            $Qbanner->bindInt(':customers_group_id', 0 );
            $Qbanner->bindInt(':customers_group_id1', 99);
            $Qbanner->bindInt(':languages_id', (int)$_SESSION['languages_id']);
            $Qbanner->bindInt(':languages_id1', 0);
            $Qbanner->execute();
          }

            $banner = $Qbanner->toArray();
        }
      }

      $output = '';

      if (isset($banner)) {
        if (!empty($banner['banners_html_text'])) {
          $output = $banner['banners_html_text'];
        } else {
          $output = '<a href="' . osc_href_link('redirect.php', 'action=banner&goto=' . $banner['banners_id']) . '" target="' . $banner['banners_target'] . '">' . osc_image(DIR_WS_IMAGES . $banner['banners_image'], $banner['banners_title'], NULL, NULL, NULL, true) . '</a>';
        }

        osc_update_banner_display_count($banner['banners_id']);
      }

      return $output;

    }


/**
 * Check if an existing banner is active
 *
 * @param int $id The ID of the banner to check
 * @access public
 * @return boolean
 */
  function osc_banner_exists($action, $identifier) {
    global $OSCOM_Customer, $OSCOM_PDO;

      if ($action == 'dynamic') {

        if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B


          $Qbanners = $OSCOM_PDO->prepare('select banners_id,
                                                banners_title,
                                                banners_image,
                                                banners_target,
                                                banners_html_text,
                                                languages_id,
                                               customers_group_id
                                       from :table_banners
                                       where banners_group = :banners_group
                                       and status = :status
                                       and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1 )
                                       and (languages_id  = :languages_id or languages_id  = :languages_id1)
                                       order by rand()
                                       limit 1
                                      ');

          $Qbanners->bindInt(':status', 1);
          $Qbanners->bindValue(':banners_group', $identifier);
          $Qbanners->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
          $Qbanners->bindInt(':customers_group_id1', 99);
          $Qbanners->bindInt(':languages_id', (int)$_SESSION['languages_id']);
          $Qbanners->bindInt(':languages_id1', 0);

          $Qbanners->execute();


            $result = $Qbanners->fetch();
           return $result;

        } else {

          $Qbanners = $OSCOM_PDO->prepare('select banners_id,
                                                banners_title,
                                                banners_image,
                                                banners_target,
                                                banners_html_text,
                                                languages_id,
                                               customers_group_id
                                       from :table_banners
                                       where banners_group = :banners_group
                                       and status = :status
                                       and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1 )
                                       and (languages_id  = :languages_id or languages_id  = :languages_id1)
                                       order by rand()
                                       limit 1
                                      ');

          $Qbanners->bindInt(':status', 1);
          $Qbanners->bindValue(':banners_group', $identifier);
          $Qbanners->bindInt(':customers_group_id', 0 );
          $Qbanners->bindInt(':customers_group_id1', 99);
          $Qbanners->bindInt(':languages_id', (int)$_SESSION['languages_id']);
          $Qbanners->bindInt(':languages_id1', 0);

          $Qbanners->execute();

            $result = $Qbanners->fetch();
            return $result;
        }


      } elseif ($action == 'static') {

        if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B

          $Qbanners= $OSCOM_PDO->prepare('select banners_id,
                                                 banners_title,
                                                 banners_image,
                                                 banners_target,
                                                 banners_html_text,
                                                 customers_group_id,
                                                 languages_id
                                          from :table_banners
                                          where status = :status
                                          and banners_group = :banners_group
                                          and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1)
                                          and (languages_id  = :languages_id or languages_id  = languages_id1)
                                        ');

          $Qbanners->bindValue(':status', 1);
          $Qbanners->bindValue(':banners_group', $identifier );
          $Qbanners->bindInt(':customers_group_id',(int)$OSCOM_Customer->getCustomersGroupID() );
          $Qbanners->bindInt(':customers_group_id1', 99 );
          $Qbanners->bindInt(':languages_id',(int)$_SESSION['languages_id'] );
          $Qbanners->bindInt(':languages_id1', 0 );

          $Qbanners->execute();

            $result = $Qbanners->fetch();
            return $result;
        } else {

          $Qbanners= $OSCOM_PDO->prepare('select banners_id,
                                               banners_title,
                                               banners_image,
                                               banners_target,
                                               banners_html_text,
                                               customers_group_id,
                                               languages_id
                                          from :table_banners
                                          where status = :status
                                          and banners_group = :banners_group
                                          and (customers_group_id = :customers_group_id or customers_group_id = :customers_group_id1 )
                                          and (languages_id = :languages_id or languages_id = :languages_id1 )
                                        ');

          $Qbanners->bindInt(':status', 1);
          $Qbanners->bindValue(':banners_group', $identifier );
          $Qbanners->bindInt(':customers_group_id', 0 );
          $Qbanners->bindInt(':customers_group_id1', 99);
          $Qbanners->bindInt(':languages_id',(int)$_SESSION['languages_id'] );
          $Qbanners->bindInt(':languages_id1', 0);

          $Qbanners->execute();

            $result = $Qbanners->fetch();
            return $result;
        }
      }
  }


/**
 * Increment the display count of the banner
 *
 * @param int $id The ID of the banner
 * @access private
 */
  function osc_update_banner_display_count($banner_id) {
    global $OSCOM_PDO;

      $Qcheck = $OSCOM_PDO->prepare('select banners_history_id
                                      from :table_banners_history
                                      where banners_id = :banners_id
                                      and date_format(banners_history_date, "%Y%m%d") = date_format(now(), "%Y%m%d")
                                      limit 1
                                     ');

      $Qcheck->bindInt(':banners_id', $banner_id);
      $Qcheck->execute();

      $result = $Qcheck->fetch();

      if ( ($result !== false) && ($result['count'] > 0) ) {

        $Qview = $OSCOM_PDO->prepare('update :table_banners_history
                                      set banners_shown = banners_shown + 1
                                      where banners_id = :banners_id
                                      and date_format(banners_history_date, "%Y%m%d") = date_format(now(), "%Y%m%d")
                                      ');
        $Qview->bindInt(':banners_id', $banner_id);
        $Qview->execute();

      } else {
        $Qbanner = $OSCOM_PDO->prepare('insert into :table_banners_history (banners_id,
                                                                            banners_shown,
                                                                            banners_history_date)
                                        values (:banners_id,
                                                1, now()
                                                )
                                      ');

      $Qbanner->bindInt(':banners_id', $banner_id);
      $Qbanner->execute();
    }


/**
 * Increment the click count of the banner
 *
 * @param int $banner_id The ID of the banner
 * @access private
 */

  function osc_update_banner_click_count($banner_id) {
    global $OSCOM_PDO;



      $Qcheck = $OSCOM_PDO->prepare('select count(*) as count
                                    from :table_banners_history where banners_id = :banners_id
                                    and date_format(banners_history_date, "%Y%m%d") = date_format(now(), "%Y%m%d")
                                   ');
      $Qcheck->bindInt(':banners_id', $banner_id);
      $Qcheck->execute();

      $result = $Qcheck->fetch();

      if ( ($result !== false) && ($result['count'] > 0) ) {
        $Qbanner = $OSCOM_PDO->prepare('update :table_banners_history
                                        set banners_clicked = banners_clicked + 1
                                        where banners_id = :banners_id
                                        and date_format(banners_history_date, "%Y%m%d") = date_format(now(), "%Y%m%d")
                                       ');
      } else {
        $Qbanner = $OSCOM_PDO->prepare('insert into :table_banners_history (banners_id,
                                                                            banners_clicked,
                                                                            banners_history_date)
                                        values (:banners_id,
                                                1, now()
                                               )
                                      ');
      }

      $Qbanner->bindInt(':banners_id', $banner_id);
      $Qbanner->execute();



    }
  }