<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


  define('MODULE_ORDER_TOTAL_TAX_TITLE', 'Taxes');
  define('MODULE_ORDER_TOTAL_TAX_DESCRIPTION', 'Inclure les Taxes <br /><br /><font color="#FF0000"><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut</font>');
?>