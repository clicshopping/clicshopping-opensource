<?php
  /*
   * credit.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

define('HEADING_TITLE','Crédits');
define('TEXT_IDEA','Idea : ');
define('TEXT_GRAPHISM','Graphism Administration : ');
define('TEXT_ARCHITECTURE','Architecture / Development / Concept: ');
define('TEXT_COMPANY','Company : ');
define('TEXT_COPYRIGHT','Brand copyright : ');
define('TEXT_LICENCE','Licenses uses : ');
define('TEXT_THANK_YOU','Thank you');
define('TEXT_THANK_YOU_1','The team wishes to thank all the communities which particularly the community  OpenSource Oscommerce which throughout the years has brought us significant support and assistance in solving problems.
Depending on the evolution of the project we will commit ourselves to provide financial support so that this community can continue over time as it will be free.<br /><br />
The amounts obtained from the marketplace used to offset the cost of the infrastructure community.<br /><br />
We also wish to thank all those who trust us in the development of this project.');
define('TEXT_CLICSHOPPING_OPENSOURCE','This project is important to you, please subscribe to one of our services, so we can continue and develop this project in time. The establishment of infrastructure at significant cost in management and maintenance.');
?>