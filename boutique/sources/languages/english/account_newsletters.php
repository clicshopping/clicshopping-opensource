<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Newsletter Subscriptions');

define('HEADING_TITLE', 'Newsletter Subscriptions');

define('MY_NEWSLETTERS_TITLE', 'My Newsletter Subscriptions');
define('MY_NEWSLETTERS_GENERAL_NEWSLETTER', 'General Newsletter');
define('MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION', 'Including store news, new products, special offers, and other promotional announcements.');

define('MY_NEWSLETTERS_RSS','Flux RSS Subscription');
define('MY_NEWSLETTERS_RSS_DESCRIPTION','In more your subscription, we propose you to follow our news with RSS technology. Just click on the link below :<br /><p align="center">
<img src ="' . HTTP_SERVER . DIR_WS_HTTP_CATALOG . DIR_WS_ICONS . 'icon_feed.gif"> <a href=' . HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'rss.php' .'>' . HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'rss.php' .'</a></p>');
define('MY_NEWSLETTERS_RSS_EXPLANATION','<strong>Note : </strong><br />This system is used to disseminate the updates to sites whose content changes frequently.
With this format, you do not need to go to the site to be informed of new products. <br /> Simply insert in a single click, the thread of information in RSS compatible software for all your sources of information on one page: your emails, weather and traffic in your city, blogs that you follow the latest articles sites sports or news and sales of' .STORE_NAME.'');

define('SUCCESS_NEWSLETTER_UPDATED', 'Your newsletter subscriptions have been successfully updated.');
define('TABLE_HEADING_NEW_PRODUCTS','News and Collections');
define('MIN_QTY_ORDER_PRODUCT','Qty; min : ');
?>
