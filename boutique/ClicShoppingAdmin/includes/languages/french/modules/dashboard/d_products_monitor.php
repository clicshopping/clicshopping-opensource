<?php
/**
 * d_products_monitor.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_TITLE', 'Surveillande des produits');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_DESCRIPTION', 'Affiche des informations sur des erreurs identifées concernant les produits');

define('MODULE_ADMIN_DASHBOARD_PRODUCTS_INFO_PRODUCTS_ID', 'Id');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_INFO_PRODUCTS_NAME', 'Produits');
define('MODULE_ADMIN_DASHBOARD_MODEL_INFO_PRODUCTS_NAME', 'Ref');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_PRODUCTS_PRODUCTS_LAST_MODIFIED', 'Derniere modification');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_PRODUCTS_ERRORS', 'Description des erreurs');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_MODEL', 'Aucune référence');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_PICTURE', 'Aucune petite image');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_TAX', 'Aucune classe de taxes');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_PRICE', 'Produit dont le tarif est égal à zéro');

define('MODULE_ADMIN_DASHBOARD_PERFECT_PRODUCTS', 'Aucune erreur n\'a &eacuteté identifi&eacutee.');
?>