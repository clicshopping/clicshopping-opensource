<?php
/*
 * google_sitemap_products_heart.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $products_array = array();

    $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                     coalesce(NULLIF(products_heart_last_modified, :products_heart_last_modified),
                                                     products_heart_date_added) as last_modified
                                      from :table_products_heart
                                      where status = :status
                                      and customers_group_id = :customers_group_id
                                      order by last_modified DESC
                                      ');

    $Qproducts->bindValue(':products_heart_last_modified', '');
    $Qproducts->bindValue(':status', '1');
    $Qproducts->bindValue(':customers_group_id', '0');
    $Qproducts->execute();


    while ($products = $Qproducts->fetch() ) {
      $products_array[$products_heart['products_id']]['loc'] = osc_href_link('product_info.php', 'products_id=' . (int)$products_heart['products_id'], 'NONSSL', false);
      $products_array[$products_heart['products_id']]['lastmod'] = $products_heart['last_modified'];
      $products_array[$products_heart['products_id']]['changefreq'] = 'weekly';
      $products_array[$products_heart['products_id']]['priority'] = '0.5';
    }

    foreach ($special_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
