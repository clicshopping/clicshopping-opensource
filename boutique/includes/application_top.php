<?php
/*
   * applicationtop.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/
// start the timer for the page parse time log
  define('PAGE_PARSE_START_TIME', microtime());

// Set the level of error reporting
 error_reporting(E_ALL | E_STRICT);

// load server configuration parameters
  if (file_exists('includes/local/configure.php')) { // for developers
    include('includes/local/configure.php');
  } else {
    include('includes/configure.php');
  }


// redirect to the installation module if DB_SERVER is empty
  if (DB_SERVER == '') {
    if (is_dir('install')) {
      header('Location: boutique/install/index.php');
      exit;
    }
  }

// set default timezone if none exists (PHP 5.3 throws an E_WARNING)
  date_default_timezone_set(defined('CFG_TIME_ZONE') ? CFG_TIME_ZONE : date_default_timezone_get());

// set the type of request (secure or not)
  if ( (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')) || (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == 443)) ) {
    $request_type =  'SSL';
    define('DIR_WS_CATALOG', DIR_WS_HTTPS_CATALOG);
// set the cookie domain
    $cookie_domain = HTTPS_COOKIE_DOMAIN;
    $cookie_path = HTTPS_COOKIE_PATH;
  } else {
    $request_type =  'NONSSL';
    define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG);
    $cookie_domain = HTTP_COOKIE_DOMAIN;
    $cookie_path = HTTP_COOKIE_PATH;
  }

// ULTIMATE Seo Urls 5 
// set php_self in the local scope
  $req = parse_url($_SERVER['SCRIPT_NAME']);

  /** 
  * USU5 function to return the base filename  
  */ 
  function usu5_base_filename() {
    // Probably won't get past SCRIPT_NAME unless this is reporting cgi location
    $base = new ArrayIterator( array( 'SCRIPT_NAME', 'PHP_SELF', 'REQUEST_URI', 'ORIG_PATH_INFO', 'HTTP_X_ORIGINAL_URL', 'HTTP_X_REWRITE_URL' ) );
    while ( $base->valid() ) {
      if ( array_key_exists(  $base->current(), $_SERVER ) && !empty(  $_SERVER[$base->current()] ) ) {
        if ( false !== strpos( $_SERVER[$base->current()], '.php' ) ) {
          preg_match( '@[a-z0-9_]+\.php@i', $_SERVER[$base->current()], $matches );
          if ( is_array( $matches ) && ( array_key_exists( 0, $matches ) )
                                    && ( substr( $matches[0], -4, 4 ) == '.php' )
                                    && ( is_readable( $matches[0] ) ) ) {
            return $matches[0];
          } 
        } 
      }
      $base->next();
    }
    // Some odd server set ups return / for SCRIPT_NAME and PHP_SELF when accessed as mysite.com (no index.php) where they usually return /index.php
    if ( ( $_SERVER['SCRIPT_NAME'] == '/' ) || ( $_SERVER['PHP_SELF'] == '/' ) ) {
      return 'index.php';
    }
// Return the standard RC3 code
    return substr($req['path'], ($request_type == 'NONSSL') ? strlen(DIR_WS_HTTP_CATALOG) : strlen(DIR_WS_HTTPS_CATALOG));
  } // End function

// set php_self in the local scope  rewriting by FWR Media
  $PHP_SELF = usu5_base_filename();

// Security Pro by FWR Media
  include_once ('includes/modules/security_pro/fwr_media_security_pro.php');
  $security_pro = new Fwr_Media_Security_Pro( true );
  // If you need to exclude a file from cleansing then you can add it like below
 // $security_pro->addExclusion( 'google_sitemap_products.php' );
  $security_pro->cleanse( $PHP_SELF );
// End - Security Pro 

// configuration generale du systeme
  require_once('includes/config_opc.php');

// define general functions used application-wide
  require('includes/functions/general.php');

// define general add on functions used application-wide
  require('includes/functions/general_addon.php');

// define general functions used  General B2B
  require('includes/functions/general_b2b.php');

// define general functions used  for the fields and button
  require('includes/functions/html_output.php');

//gestion des metas donnees - Referencement
  require('includes/functions/header_tags.php');

// include the database functions
  require('includes/functions/database_' . DB_DATABASE_TYPE . '.php');

// make a connection to the database... now
  osc_db_connect() or die('Unable to connect to database server!');

  require('includes/classes/DateTime.php');
  $OSCOM_Date = new date_time();

  require('includes/classes/cache.php');
  $OSCOM_Cache = new cache();

  require('includes/classes/db.php');
  $OSCOM_PDO = db::initialize();

// set the application parameters
// (fonctionnel) mais pb avec les modules qui ne sont pas actualises
  $Qcfg = $OSCOM_PDO->prepare('select configuration_key as k,
                                     configuration_value as v
                               from :table_configuration');
  $Qcfg->setCache('configuration');
  $Qcfg->execute();

  foreach ($Qcfg->fetchAll() as $param) {
    define($param['k'], $param['v']);
  }

// if gzip_compression is enabled, start to buffer the output
  if ( (GZIP_COMPRESSION == 'true') && ($ext_zlib_loaded = extension_loaded('zlib')) && !headers_sent() ) {
    if (($ini_zlib_output_compression = (int)ini_get('zlib.output_compression')) < 1) {
      if (PHP_VERSION < '5.4' || PHP_VERSION > '5.4.5') { // see PHP bug 55544
        ob_start('ob_gzhandler');
      }
    } elseif (function_exists('ini_set')) {
      ini_set('zlib.output_compression_level', GZIP_LEVEL);
    }
  }

// include cache functions if enabled
  if (USE_CACHE == 'true') include('includes/functions/cache.php');

// include shopping cart class
  require('includes/classes/shopping_cart.php');

// include navigation history class
  require('includes/classes/navigation_history.php');

// include mobile class
  if (PRICE_MOBILE_ACCEPT == 'true') {
    require('ext/Mobile-Detect-master/Mobile_Detect.php');
    $OSCOM_DetectMobile = new Mobile_Detect();
  }

// define how the session functions will be used
  require('includes/functions/sessions.php');

// set the session name and save path
  session_name('LORsid');
  session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
  session_set_cookie_params(0, $cookie_path, $cookie_domain);

  if ( function_exists('ini_set') ) {
    ini_set('session.use_only_cookies', (SESSION_FORCE_COOKIE_USE == 'True') ? 1 : 0);
  }

// set the session ID if it exists
  if ( SESSION_FORCE_COOKIE_USE == 'False' ) {
    if ( isset($_GET[session_name()]) && (!isset($_COOKIE[session_name()]) || ($_COOKIE[session_name()] != $_GET[session_name()])) ) {
       session_id($_GET[session_name()]);
    } elseif ( isset($_POST[session_name()]) && (!isset($_COOKIE[session_name()]) || ($_COOKIE[session_name()] != $_POST[session_name()])) ) {
        session_id($_POST[session_name()]);
    }
  }

// start the session
  $session_started = false;

  if (SESSION_FORCE_COOKIE_USE == 'True') {
    osc_setcookie('cookie_test', 'please_accept_for_session', time()+60*60*24*30);

    if (isset($_COOKIE['cookie_test'])) {
      osc_session_start();
      $session_started = true;
    }
  } elseif (SESSION_BLOCK_SPIDERS == 'True') {
    $user_agent = '';

    if (isset($_SERVER['HTTP_USER_AGENT'])) {
      $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    }

    $spider_flag = false;

    if ( !empty($user_agent) ) {
      foreach ( file('includes/spiders.txt') as $spider ) {
        if ( !empty($spider) ) {
          if ( strpos($user_agent, $spider) !== false ) {
            $spider_flag = true;
            break;
          }
        }
      }
    }
    if ( $spider_flag === false ) {
      osc_session_start();
      $session_started = true;
    }
  } else {
    osc_session_start();
    $session_started = true;
  }

// initialize a session token
  if (!isset($_SESSION['sessiontoken'])) {
    $_SESSION['sessiontoken'] = md5(osc_rand() . osc_rand() . osc_rand() . osc_rand());
  }

// set SID once, even if empty
  $SID = (defined('SID') ? SID : '');

// verify the ssl_session_id if the feature is enabled
  if ( ($request_type == 'SSL') && (SESSION_CHECK_SSL_SESSION_ID == 'True') && (ENABLE_SSL == true) && ($session_started === true) ) {
    if (!isset($_SESSION['SSL_SESSION_ID'])) {
      $_SESSION['SESSION_SSL_ID'] = $_SERVER['SSL_SESSION_ID'];
    }

    if ( $_SESSION['SESSION_SSL_ID'] != $_SERVER['SSL_SESSION_ID'] ) {
      session_destroy();
      osc_redirect(osc_href_link('ssl_check.php'));
    }
  }

// verify the browser user agent if the feature is enabled
  if (SESSION_CHECK_USER_AGENT == 'True') {
    if (!isset($_SESSION['SESSION_USER_AGENT'])) {
      $_SESSION['SESSION_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
    }

    if ($_SESSION['SESSION_USER_AGENT'] != $_SERVER['HTTP_USER_AGENT']) {
      session_destroy();
      osc_redirect(osc_href_link('login.php'));
    }
  }

// verify the IP address if the feature is enabled
  if (SESSION_CHECK_IP_ADDRESS == 'True') {
    if (!isset($_SESSION['SESSION_IP_ADDRESS'])) {
      $_SESSION['SESSION_IP_ADDRESS'] = osc_get_ip_address();
    }

    if ($_SESSION['SESSION_IP_ADDRESS'] !=  osc_get_ip_address()) {
      session_destroy();
      osc_redirect(osc_href_link('login.php'));
    }
  }

  require('includes/classes/customer.php');
  $OSCOM_Customer = new customer();

// create the shopping cart
  if ( !isset($_SESSION['cart']) || !is_object($_SESSION['cart']) || (get_class($_SESSION['cart']) != 'shoppingCart') ) {
    $_SESSION['cart'] = new shoppingCart;
  }

// include currencies class and create an instance
  require('includes/classes/currencies.php');
  $currencies = new currencies();

// include the mail classes
  require('includes/classes/mime.php');
  require('includes/classes/email.php');

// set the language
  if (!isset($_SESSION['language']) || isset($_GET['language'])) {
    require('includes/classes/language.php');
    $lng = new language();

    if ( isset($_GET['language']) && !empty($_GET['language']) ) {
      $lng->set_language($_GET['language']);
    } else {
      $lng->get_browser_language();
    }

    $_SESSION['language'] = $lng->language['directory'];
    $_SESSION['languages_id'] = $lng->language['id'];
  }
  
  $_system_locale_numeric = setlocale(LC_NUMERIC, 0);
  
// ULTIMATE Seo Urls 5 PRO 
   Usu_Main::i()->setVar( 'languages_id', $_SESSION['languages_id'] )
               ->setVar( 'request_type', $request_type ) 
               ->setVar( 'session_started', $session_started ) 
               ->setVar( 'sid', $SID ) 
               ->setVar( 'language', $_SESSION['language'] )
               ->setVar( 'filename', $PHP_SELF )
               ->initiate( ( isset( $lng ) && ( $lng instanceof language ) ) ? $lng : array(), $_SESSION['languages_id'], $_SESSION['language'] );

// include the language translations

  setlocale(LC_NUMERIC, $_system_locale_numeric); // Prevent LC_ALL from setting LC_NUMERIC to a locale with 1,0 float/decimal values instead of 1.0 (see bug #634)

// currency
  if (!isset($_SESSION['currency']) || isset($_GET['currency']) || ( (USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && (LANGUAGE_CURRENCY != $_SESSION['currency']) ) ) {
    if (isset($_GET['currency']) && $currencies->is_set($_GET['currency'])) {
      $_SESSION['currency'] = $_GET['currency'];
    } else {
      $_SESSION['currency'] = ((USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && $currencies->is_set(LANGUAGE_CURRENCY)) ? LANGUAGE_CURRENCY : DEFAULT_CURRENCY;
    }
  }

  if ( $spider_flag === false ) {
// currencies geolocalisation
    if (CONFIGURATION_CURRENCIES_GEOLOCALISATION == 'true') {
      require('includes/functions/geolocalisation.php');
      $_SESSION['currency'] = osc_get_currencies_location();
    }
  }

// navigation history
  if ( !isset($_SESSION['navigation']) || !is_object($_SESSION['navigation']) || (get_class($_SESSION['navigation']) != 'navigationHistory') ) {
    $_SESSION['navigation'] = new navigationHistory;
  }
  $_SESSION['navigation']->add_current_page();

// blog
  if ( isset($_GET['blog_content_id']) ) {
    $blog_content_id = $_GET['blog_content_id'];
  }

  require('includes/classes/blog.php');
  $OSCOM_Blog = new Blog($_GET['blog_content_id']);

// products
  if ( isset($_GET['products_id']) ) {
    $products_id = $_GET['products_id'];
  }

// for products_info with $_GET['products_id']
  require('includes/classes/products.php');
  $OSCOM_Products = new Products($_GET['products_id']);
// for other listing with $products_id
  require('includes/classes/products_listing.php');
  $OSCOM_ProductsListing = new ProductsListing($products_id);
// reviews
  require('includes/classes/products_reviews.php');
  $OSCOM_ProductsReviews = new ProductsReviews($products_id);

// Define the quantity by products see general and shopping cart module
  $min_quantity = $OSCOM_ProductsListing->getProductsMinimumQuantity($product_id);

// action recorder
  require('includes/classes/action_recorder.php');

// initialize the message stack for output messages
  require('includes/classes/message_stack.php');
  $OSCOM_MessageStack = new messageStack();


// Shopping cart actions
  if (isset($_GET['action'])) {
// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled
    if ($session_started == false) {
      osc_redirect(osc_href_link('cookie_usage.php'));
    }

    if (DISPLAY_CART == 'true') {
      $goto =  'shopping_cart.php';
      $parameters = array('action', 'cPath', 'products_id', 'pid');
    } else {
      $goto = $PHP_SELF;
      if ($_GET['action'] == 'buy_now') {
        $parameters = array('action', 'pid', 'products_id');
      } else {
        $parameters = array('action', 'pid');
      }
    }

    switch ($_GET['action']) {
      // customer wants to update the product quantity in their shopping cart
      case 'update_product' : for ($i=0, $n=sizeof($_POST['products_id']); $i<$n; $i++) {
                                if (in_array($_POST['products_id'][$i], (is_array($_POST['cart_delete']) ? $_POST['cart_delete'] : array()))) {
                                  $_SESSION['cart']->remove($_POST['products_id'][$i]);
                                } else {
                                  $attributes = ($_POST['id'][$_POST['products_id'][$i]]) ? $_POST['id'][$_POST['products_id'][$i]] : '';
                                  $_SESSION['cart']->add_cart($_POST['products_id'][$i],  $_POST['cart_quantity'][$i],$attributes, false);
                                }
                              }

                              osc_redirect(osc_href_link($goto, osc_get_all_get_params($parameters)));
     break;
// customer adds a product from the products page
      case 'add_product' :    if (isset($_POST['products_id']) && is_numeric($_POST['products_id'])) {
// include cart quantity number
                                $attributes = isset($_POST['id']) ? $_POST['id'] : '';
                                $_SESSION['cart']->add_cart($_POST['products_id'], $_SESSION['cart']->get_quantity(osc_get_uprid($_POST['products_id'], $attributes))+($_POST['cart_quantity']), $attributes);
                              }

                              osc_redirect(osc_href_link($goto, osc_get_all_get_params($parameters)));
      break;

// Remove the product of shopping cart
      case 'remove_product' :    if (isset($_GET['products_id'])) {
                                 $_SESSION['cart']->remove($_GET['products_id']);
                              }

                              osc_redirect(osc_href_link($goto, osc_get_all_get_params($parameters)));
      break;

// performed by the 'buy now' button in product listings and review page
      case 'buy_now' :        if (isset($_GET['products_id'])) {
                                if (osc_has_product_attributes($_GET['products_id'])) {
                                  osc_redirect(osc_href_link('product_info.php', 'products_id=' . $_GET['products_id']));
                                } else {
// minimum to take an order : add quantity defined in administration panel
                                  $_SESSION['cart']->add_cart($_GET['products_id'], $_SESSION['cart']->get_quantity($_GET['products_id'])+(int)$min_quantity);
                                }
                              }

                              osc_redirect(osc_href_link($goto, osc_get_all_get_params($parameters)));
      break;
      case 'notify' :         if ($OSCOM_Customer->isLoggedOn()) {
                                if (isset($_GET['products_id'])) {
                                  $notify = $_GET['products_id'];
                                } elseif (isset($_GET['notify'])) {
                                  $notify = $_GET['notify'];
                                } elseif (isset($_POST['notify'])) {
                                  $notify = $_POST['notify'];
                                } else {
                                  osc_redirect(osc_href_link($PHP_SELF, osc_get_all_get_params(array('action', 'notify'))));
                                }
                                if (!is_array($notify)) $notify = array($notify);
                                for ($i=0, $n=sizeof($notify); $i<$n; $i++) {

                                  $Qcheck = $OSCOM_PDO->prepare('select count(*) as count 
                                                                from :table_products_notifications
                                                                where products_id = :products_id
                                                                and customers_id = :customers_id
                                                              ');
                                  $Qcheck->bindInt(':products_id', (int)$notify[$i] );
                                  $Qcheck->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );

                                  $Qcheck->execute();
                                  $check = $Qcheck->fetch();

                                  if ($check['count'] < 1) {
                                    osc_db_query("insert into products_notifications (products_id,
                                                                                      customers_id,
                                                                                      date_added)
                                                  values ('" . (int)$notify[$i] . "', 
                                                          '" . (int)$OSCOM_Customer->getID() . "', 
                                                          now())
                                                 " );
                                  }
                                }
                                osc_redirect(osc_href_link($PHP_SELF, osc_get_all_get_params(array('action', 'notify'))));
                              } else {
                                $_SESSION['navigation']->set_snapshot();
                                osc_redirect(osc_href_link('login.php', '', 'SSL'));
                              }
      break;
      case 'notify_remove' :  if ($OSCOM_Customer->isLoggedOn() && isset($_GET['products_id'])) {

                                $Qcheck = $OSCOM_PDO->prepare('select count(*) as count 
                                                              from :table_products_notifications
                                                              where products_id = :products_id
                                                              and customers_id = :customers_id
                                                            ');
                                $Qcheck->bindInt(':products_id', (int)$_GET['products_id']);
                                $Qcheck->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );

                                $Qcheck->execute();
                                $check = $Qcheck->fetch();

                                if ($check['count'] > 0) {
                                  osc_db_query("delete from products_notifications
                                                where products_id = '" . (int)$_GET['products_id'] . "' 
                                                and customers_id = '" . (int)$OSCOM_Customer->getID() . "'
                                               ");
                                }
                                osc_redirect(osc_href_link($PHP_SELF, osc_get_all_get_params(array('action'))));
                              } else {
                                $_SESSION['navigation']->set_snapshot();
                                osc_redirect(osc_href_link('login.php', '', 'SSL'));
                              }
      break;
      case 'cust_order' :     if ($OSCOM_Customer->isLoggedOn() && isset($_GET['pid'])) {
                                if (osc_has_product_attributes($_GET['pid'])) {
                                  osc_redirect(osc_href_link('product_info.php', 'products_id=' . $_GET['pid']));
                                } else {
// minimum to take an order : add quantity defined in administration panel
                                  $_SESSION['cart']->add_cart($_GET['pid'], $_SESSION['cart']->get_quantity($_GET['pid'])+ (int)$min_quantity);
                                }
                              }
                              osc_redirect(osc_href_link($goto, osc_get_all_get_params($parameters)));
      break;
    }
  }

// include the who's online functions
  require('includes/functions/whos_online.php');
  osc_update_whos_online();

// include the password crypto functions
  require('includes/functions/password_funcs.php');

// include validation functions (right now only email address)
  require('includes/functions/validations.php');

// split-page-results
  require('includes/classes/split_page_results.php');

// include  categories
  require ('includes/classes/category_tree.php');
  $OSCOM_CategoryTree = new category_tree();

//  if (file_exists('includes/local/configure.php')) include('includes/local/configure.php');

// template
  require('includes/classes/template.php');
  $OSCOM_Template = new template();

  if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . '/languages/'. $_SESSION['language'] . '.php') ) {
    require_once(DIR_WS_TEMPLATE . SITE_THEMA . '/languages/'. $_SESSION['language'] . '.php');
    if (file_exists(DIR_WS_LANGUAGES . $_SESSION['language'] . '.php') ) {
      require_once(DIR_WS_LANGUAGES . $_SESSION['language'] . '.php');
    } else {
      require_once(DIR_WS_TEMPLATE . SITE_THEMA . '/languages/'. $_SESSION['language'] . '.php');
    }
  } else {
    require_once(DIR_WS_LANGUAGES . $_SESSION['language'] . '.php');
  }

// definie la langues et les variables particulieres a lire
  setlocale(LC_NUMERIC, $_system_locale_numeric); // Prevent LC_ALL from setting LC_NUMERIC to a locale with 1,0 float/decimal values instead of 1.0 (see bug #634)

 // Catalog offline / hors ligne
  if(STORE_OFFLINE == 'true') {
    $allowed_ip = false;
    $ips = explode(',',STORE_OFFLINE_ALLOW);
    foreach($ips as $ip) {
        if(trim($ip) == $_SERVER['REMOTE_ADDR']) {
          $allowed_ip = true;
        break;
        }
      }
    if(!$allowed_ip)
      osc_redirect(HTTP_SERVER . DIR_WS_HTTP_CATALOG .'offline.html');
  } 

// auto activation et expirations des bannieres
// auto activate and expire banners
  if (constant(STORE_DISPLAY_BANNERS) && (STORE_DISPLAY_BANNERS == 'true')) {
    require('includes/functions/banner.php');
    osc_activate_banners();
    osc_expire_banners();
  }

// auto expiration des produits en promotion
// auto expire special products
  require('includes/functions/specials.php');
  osc_scheduled_specials();
  osc_expire_specials();

// auto expiration des produits en promotion
// auto expire special products
  require('includes/functions/products_heart.php');
  osc_scheduled_products_heart();
  osc_expire_products_heart();

// auto activation et expirations des pages d'introduction et d'accueil
  require('includes/functions/page_manager.php');
  osc_activate_page_manager();
  osc_expire_page_manager();


// inclure la classe de page_manager (Page d'introduction, acceuil, contact et informations)
  include('includes/classes/page_manager.php');
  $page_manager = new osc_display_page_manager();

// include odoo webservice
  if (ODOO_ACTIVATE_WEB_SERVICE == 'true') {
    require('includes/classes/odoo.php');
    $OSCOM_ODOO = new Odoo();
  }

// calculate category path
  if (isset($_GET['cPath'])) {
    $cPath = $_GET['cPath'];
  } elseif (isset($_GET['products_id']) && !isset($_GET['manufacturers_id'])) {
    $cPath = osc_get_product_path($_GET['products_id']);
  } else {
    $cPath = '';
  }

  if ( !empty($cPath) ) {
    $cPath_array = osc_parse_category_path($cPath);
    $cPath = implode('_', $cPath_array);
    $current_category_id = $cPath_array[(sizeof($cPath_array)-1)];
  } else {
    $current_category_id = 0;
  }

// include the breadcrumb class and start the breadcrumb trail

  require('includes/classes/breadcrumb.php');
  $OSCOM_Breadcrumb = new breadcrumb();

  $OSCOM_Breadcrumb->add(HEADER_TITLE_TOP, HTTP_SERVER);
  $OSCOM_Breadcrumb->add(HEADER_TITLE_CATALOG, osc_href_link('index.php'));

// add category names or the manufacturer name to the breadcrumb trail
  if (isset($cPath_array)) {
    for ($i=0, $n=sizeof($cPath_array); $i<$n; $i++) {

      $Qcategories = $OSCOM_PDO->prepare('select categories_name
                                          from :table_categories_description
                                          where categories_id = :categories_id
                                          and language_id = :language_id
                                        ');
      $Qcategories->bindInt(':categories_id', (int)$cPath_array[$i]);
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qcategories->execute();

      if ($Qcategories->rowCount() > 0) {
        $categories = $Qcategories->fetch();
        $OSCOM_Breadcrumb->add($categories['categories_name'], osc_href_link('index.php', 'cPath=' . implode('_', array_slice($cPath_array, 0, ($i+1)))));
      } else {
        break;
      }
    }
  } elseif (isset($_GET['manufacturers_id'])) {

    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturers_name
                                           from :table_manufacturers
                                           where manufacturers_id = :manufacturers_id
                                         ');
    $Qmanufacturers->bindInt(':manufacturers_id', (int)$_GET['manufacturers_id']);
    $Qmanufacturers->execute();

    if ( $Qmanufacturers->fetch() !== false ) {
      $OSCOM_Breadcrumb->add($Qmanufacturers->value('manufacturers_name'), osc_href_link('index.php', 'manufacturers_id=' . (int)$_GET['manufacturers_id']));
    }
  }

// add the products name to the breadcrumb trail
  if (isset($_GET['products_id'])) {

    $Qproduct = $OSCOM_PDO->prepare('select products_name
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$_GET['products_id']);
    $Qproduct->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qproduct->execute();

    if ( $Qproduct->fetch() !== false ) {
      $OSCOM_Breadcrumb->add($Qproduct->value('products_name'), osc_href_link('product_info.php', 'cPath=' . $cPath . '&products_id=' . (int)$_GET['products_id']));
    }
  }

  require(DIR_FS_CATALOG . 'includes/classes/hooks.php');
  $OSCOM_Hooks = new hooks('catalog');

  register_shutdown_function('session_write_close');

  if (WEBSITE_MODULE_INSTALLED == 0) {
    echo TEXT_INSTALL;
    echo '   <div style="text-align:center;"><br /><a href="http://www.clicshopping.org/marketplace" target="_blank"><img src="images/logo_clicshopping.png" border="0" height="100" width="100" alt="Market Place"><br />Go to Market Place</a></div>';
    echo '   <div style="align:center; font-size: 10px;padding-top:10px;">ClicShopping(TM) est une marque (trademark) d&eacute;pos&eacute;e par <a href="http://www.e-imaginis.com" target="_blank">e-Imaginis</a>.</div>';
    echo '</div>';
    exit;
  }
