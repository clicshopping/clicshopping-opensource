<?php
/**
 * blog.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Blog');

define('HEADING_TITLE_SEARCH', 'Rechercher');
define('HEADING_TITLE_GOTO', 'Aller à :');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'Catégories / Articles');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_CATEGORIES', 'Catégories');
define('TABLE_HEADING_SORT_ORDER','Ordre de tri');

define('TABLE_HEADING_LAST_MODIFIED','Dernière modification');
define('TABLE_HEADING_CREATED','Créé / modifié');
define('TABLE_HEADING_CUSTOMERS_GROUPS','Groupe Client');

define('TEXT_PRODUCTS_SEO_URL', 'URL SEO du Article :');
define('TEXT_EDIT_CATEGORIES_SEO_URL', 'URL SEO de la catégorie :');
define('TEXT_CATEGORIES_SEO_URL', 'URL SEO de la catégorie :');

define('TEXT_CUSTOMERS_GROUP','Groupe Client :');

define('TEXT_NEW_PRODUCT', 'Nouveaux articles dans &quot;%s&quot;');
define('TEXT_CATEGORIES', 'Catégories :');
define('TEXT_SUBCATEGORIES', 'Sous-catégories :');
define('TEXT_DIVERS_TITLE', 'Divers');
define('TEXT_DESCRIPTION_CATEGORIES','Description de la Catégorie');

define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Métadonnées');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Titre');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_DESCRIPTION_SUMMARY','Résumé');
  define('TEXT_PRODUCTS_KEYWORDS', 'Mots clefs');
define('TEXT_PRODUCTS_TAG_PRODUCT','Tags en relation avec les produits');
define('TEXT_PRODUCTS_TAG_BLOG','Tags en relation avec le blog');

define('TEXT_CREATE','Créer');
define('TEXT_PRODUCTS_AUTHOR','Auteur de l\'article : ');

define('KEYWORDS_GOOGLE_TREND','Tendance des mots clefs selon Google');
define('ANALYSIS_GOOGLE_TOOL','Outil d\'analyse selon Google');
define('POPULARITY_OVERTURE_KEYWORDS','Popularité des mots clefs selon Yahoo Search Marketing');

define('TEXT_PRODUCTS', 'Articles :');

define('TEXT_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_DATE_AVAILABLE', 'Date disponibilité    :');
define('TEXT_LAST_MODIFIED',  'Dernière modification :');

define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Merci de créer une nouvelle catégorie ou un article dans ce niveau.');

define('TEXT_PRODUCT_DATE_ADDED', 'Article ajouté au catalogue le :');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'Article en stock le :');

define('TEXT_PRODUCTS_SORT_ORDER','Ordre de tri');

define('TEXT_ALL_CUSTOMERS','Clients Normaux');
define('NORMAL_CUSTOMER','Client normal');


define('TEXT_YES','oui');
define('TEXT_NO','non');
define('TEXT_CHOOSE','Selectionner');
define('TEXT_PRODUCTS_TWITTER','Souhaitez-vous publier cet article sur twitter ? ');
define('TEXT_NEW_BLOG_TWITTER','Nouveauté ! ');

define('TEXT_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_EDIT_CATEGORIES_ID', 'ID de la catégorie :');
define('TEXT_EDIT_CATEGORIES_NAME', 'Nom de la catégorie :');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Image de la catégorie :');
define('TEXT_EDIT_SORT_ORDER', 'Ordre de tri :');

define('TEXT_INFO_COPY_TO_INTRO', 'Veuillez choisir une nouvelle catégorie dans laquelle vous voulez copier cet article');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Catégories courantes :');
define('TEXT_INFO_HEADING_NEW_CATEGORY', 'Nouvelle catégorie');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Editer catégorie');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Supprimer catégorie');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Déplacer catégorie');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Supprimer l\'article');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Déplacer l\'article');
define('TEXT_INFO_HEADING_COPY_TO', 'Copier vers');
define('TEXT_INFO_HEADING_ARCHIVE','Archivage de l\'article');
define('TEXT_INFO_ARCHIVE_INTRO','Souhaitez vous mettre cet article en archive ? <br /><br />Cet article pourra être réactivé à partir de la section archive. <br /><br /><strong>Note : </strong>L\'archivage permet de faire disparaitre de votre catalogue le article mais il ne le supprime pas. Les clients auront toujours accès au article via les moteurs de recherche mais ils ne pourront pas le commander. Ce système est utile pour le référencement de votre site.');

define('TEXT_DELETE_CATEGORY_INTRO', 'Etes vous sur de vouloir supprimer cette catégorie ?');
define('TEXT_DELETE_PRODUCT_INTRO', 'Etes vous sur de vouloir supprimer définitivement cet article dans cette cat&eacutegorie ?');

define('TEXT_DELETE_WARNING_CHILDS', '<strong>ATTENTION :</strong> Il y a %s (sous-)catégories liées &aacute; cette catégorie !');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>ATTENTION :</strong> Il y a %s articles liées &aacute; cette catégorie !');

define('TEXT_MOVE_PRODUCTS_INTRO', 'Merci de sélectionner la catégorie ou vous voudriez que <strong>%s</strong> soit placé');
define('TEXT_MOVE_CATEGORIES_INTRO', 'Merci de sélectionner la catégorie ou vous voudriez que <strong>%s</strong> soit placé');
define('TEXT_MOVE', 'Déplacer <strong>%s</strong> vers :');

define('TEXT_NEW_CATEGORY_INTRO', 'Merci de compléter les informations suivantes pour la nouvelle catégorie');
define('TEXT_CATEGORIES_NAME', 'Nom de la catégorie :');
define('TEXT_CATEGORIES_NAME_TITLE', 'Nom de la catégorie');
define('TEXT_CATEGORIES_IMAGE', 'Image de la catégorie :');
define('TEXT_SORT_ORDER', 'Ordre de tri :');

define('TEXT_PRODUCTS_STATUS', 'Statut :');

define('TEXT_PRODUCTS_NAME', 'Nom de l\'article');
define('TEXT_PRODUCTS_DESCRIPTION', 'Description du article');


define('ICON_EDIT_CUSTOMERS_GROUP','Appartient à un groupe client');
define('ICON_EDIT_STATUS_DISPLAY_CATALOG_ON','Affichage du champs dans le catalogue');
define('ICON_EDIT_STATUS_DISPLAY_CATALOG_OFF','Affichage du champs dans l\'administration');

define('TEXT_PRODUCTS_PRESENTATION', 'Présentation');

define('TEXT_USER_NAME','Créé / Modifié par : ');

define('TEXT_CATEGORIES_IMAGE_TITLE','Image principale de la catégorie');
define('TEXT_CATEGORIES_IMAGE_VIGNETTE', 'Image vignette de la catégorie');
define('TEXT_CATEGORIES_IMAGE_VISUEL', 'Image actuellement visible sur la catégorie');
define('TEXT_CATEGORIES_DELETE_IMAGE', 'Ne plus afficher d\'image sur cette catégorie');

define('EMPTY_CATEGORY', 'Catégorie vide');

define('TEXT_HOW_TO_COPY', 'Méthode de copie :');
define('TEXT_COPY_AS_LINK', 'Lien article');
define('TEXT_COPY_AS_DUPLICATE', 'Dupliquer article');

define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Erreur : Impossible de lier des articles dans la même cat&eacutegorie.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Erreur : Impossible d\'écrire dans le répertoire images : ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Erreur: Vous ne pouvez pas bougez cette catégorie.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire d\'images n\'existe pas : ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Erreur : La catégorie ne peut pas être déplacée dans la sous-catégorie.');




define('HELP_IMAGE_CATEGORIES', '<li>Veuillez à ne pas mettre plus d\'une image dans la sections "Image vignette de la catégorie".</li><li>Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

define('TITLE_HELP_DESCRIPTION', 'Notes sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');


define('TITLE_HELP_SUBMIT', 'Notes sur la gestion des métadonnées');
define('HELP_SUBMIT', '<li>Le système gère automatiquement la gestion des métadonnées.<br />
<blockquote>Il inscrit automatiquement les éléments dans l\'ordre suivant si les champs ne sont pas renseignées:<br />
Titre : le nom de votre article (ex blouson cuir)<br />
Description : le nom de l\'article (blouson cuir), le nom du article découpé (blouson, cuir), le nom de la catégorie,)<br />
Keywords : le nom du l\'article (blouson cuir), le nom du l\'article découpé (blouson, cuir), le nom de la catégorie<br />
categorie : la catégorie de l\'article<br />
</blockquote></li>
<li>Si vous renseignez les champs, les balises apparaitront de la facon suivante : <br />
<blockquote>
Titre : le titre du champs, le nom de votre article (ex blouson cuir)<br />
Description : le champs description, le nom de l\'article (blouson cuir), le nom du article découpé (blouson, cuir), le nom de la catégorie<br />
Mots clefs : le champs mots clefs, le nom de l\'article (blouson cuir), le nom de l\'article découpé (blouson, cuir), le nom de la catégorie<br />
</blockquote></li>
<li><strong>Veuillez à ne pas mettre trop de mots clefs (maximum 10)</strong> et établissez une cohérence entre chaque champs afin de retourver les mots les plus importants en premier</li>
');


define('TITLE_HELP_OPTIONS', 'Notes sur les options');
define('HELP_OPTIONS', '<li>Les champs supplémentaires ne sont pas pris en compte dans la facture ni dans son calcul.</li><li>Si vous souhaitez créer ou supprimer un champs, vous devez le faire à partir du menu de gestion des champs supplémentaires.</li><li>
Si le champs est vide, celui n\'apparaitra pas dans la partie catalogue.</li>');

define('TITLE_HELP_GENERAL', 'Notes Informatives');
define('HELP_GENERAL', '');



?>
