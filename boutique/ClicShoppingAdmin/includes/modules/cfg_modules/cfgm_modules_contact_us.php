<?php
/*
 * cfgm_contact_us.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 1.0
*/

  class cfgm_modules_contact_us {
    var $code = 'modules_contact_us';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_CONTACT_US_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_contact_us() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_contact_us/';
      $this->title = MODULE_CFG_MODULE_CONTACT_US_MODULES_TITLE;
    }
  }
?>
