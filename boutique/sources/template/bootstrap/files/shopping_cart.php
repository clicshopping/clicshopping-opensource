<?php
/*
 * shopping_cart.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

require($OSCOM_Template->getTemplateHeaderFooter('header'));
require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
  <div class="contentContainer">
    <?php require($OSCOM_Template->getTemplateModules('shopping_cart/shopping_cart')); ?><br />
    <?php echo $OSCOM_Template->getBlocks('modules_shopping_cart'); ?>
  </div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>