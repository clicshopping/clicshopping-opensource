<?php
/*
 * application_top.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  

*/

// Start the clock for the page parse time log
  define('PAGE_PARSE_START_TIME', microtime());

// Set the level of error reporting
  error_reporting(E_ALL | E_STRICT);

// load server configuration parameters
  if (file_exists('includes/local/configure.php')) { // for developers
    include('includes/local/configure.php');
  } else {
    include('includes/configure.php');
  }

// some code to solve compatibility issues
  require('includes/functions/compatibility.php');

// set the type of request (secure or not)
  if ( (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')) || (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == 443)) ) {
    $request_type =  'SSL';
// set the cookie domain
    $cookie_domain = HTTPS_COOKIE_DOMAIN;
    $cookie_path = HTTPS_COOKIE_PATH;
  } else {
    $request_type =  'NONSSL';
    $cookie_domain = HTTP_COOKIE_DOMAIN;
    $cookie_path = HTTP_COOKIE_PATH;
  }

// set php_self in the local scope
  $req = parse_url($_SERVER['SCRIPT_NAME']);
  $PHP_SELF = substr($req['path'], ($request_type == 'SSL') ? strlen(DIR_WS_HTTPS_ADMIN) : strlen(DIR_WS_ADMIN));

// Used in the "Backup Manager" to compress backups
  define('LOCAL_EXE_GZIP', 'gzip');
  define('LOCAL_EXE_GUNZIP', 'gunzip');
  define('LOCAL_EXE_ZIP', 'zip');
  define('LOCAL_EXE_UNZIP', 'unzip');


// Define how do we update currency exchange rates
// Possible values are 'oanda' 'xe' or ''
  define('CURRENCY_SERVER_PRIMARY', 'oanda');
  define('CURRENCY_SERVER_BACKUP', 'xe');

// include the database functions
  require('includes/functions/database_' . DB_DATABASE_TYPE . '.php');

// configuration generale du systeme
  require_once('../includes/config_opc.php');

// make a connection to the database... now
  osc_db_connect() or die('Unable to connect to database server!');

  require('includes/classes/db.php');
  $OSCOM_PDO = db::initialize();

// set the application parameters
  $Qcfg = $OSCOM_PDO->query('select configuration_key as cfgKey, 
                                    configuration_value as cfgValue 
                             from :table_configuration');
//  $Qcfg->setCache('configuration_admin');
  $Qcfg->execute();

  while ( $Qcfg->fetch() ) {
    define($Qcfg->value('cfgKey'), $Qcfg->value('cfgValue'));
  }

// define our general functions used application-wide
  require('includes/functions/general.php');
  require('includes/functions/general_addon.php');
  require('includes/functions/general_b2b.php');
  require('includes/functions/odoo.php');
  require('includes/functions/html_output.php');


  require('includes/classes/DateTime.php');
  $OSCOM_Date = new date_time();

// initialize the logger class
  require('includes/classes/logger.php');

// include shopping cart class
  require('includes/classes/shopping_cart.php');

// define how the session functions will be used
  require('includes/functions/sessions.php');


// set the session name and save path
  session_name('LORAdminID');
  session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
   if (function_exists('session_set_cookie_params')) {
    session_set_cookie_params(0, $cookie_path, $cookie_domain);
  } elseif (function_exists('ini_set')) {
    ini_set('session.cookie_lifetime', '0');
    ini_set('session.cookie_path', $cookie_path);
    ini_set('session.cookie_domain', $cookie_domain);
  }

  @ini_set('session.use_only_cookies', (SESSION_FORCE_COOKIE_USE == 'True') ? 1 : 0);

// lets start our session
  osc_session_start();

// set the language
  if (!isset($_SESSION['language']) || isset($_GET['language'])) {
    include('includes/classes/language.php');
    $lng = new language();

    if (isset($_GET['language']) && osc_not_null($_GET['language'])) {
      $lng->set_language($_GET['language']);
    } else {
      $lng->get_browser_language();
    }

    $_SESSION['language'] = $lng->language['directory'];
    $_SESSION['languages_id'] = $lng->language['id'];
  }

// redirect to login page if administrator is not yet logged in
  if (!isset($_SESSION['admin'])) {
    $redirect = false;

    $current_page = $PHP_SELF;

// if the first page request is to the login page, set the current page to the index page
// so the redirection on a successful login is not made to the login page again
    if ( ($current_page == 'login.php') && !isset($_SESSION['redirect_origin']) ) {
      $current_page = 'index.php';
      $_GET = array();
      $_POST = array();
    }

    if ($current_page != 'login.php') {
      if (!isset($_SESSION['redirect_origin'])) {
        $_SESSION['redirect_origin'] = array('page' => $current_page,
                                             'get' => $_GET,
                                             'post' => $_POST);
      }

// try to automatically login with the HTTP Authentication values if it exists
      if (!isset($_SESSION['auth_ignore'])) {
        if (isset($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']) && !empty($_SERVER['PHP_AUTH_PW'])) {
          $_SESSION['redirect_origin']['auth_user'] = $_SERVER['PHP_AUTH_USER'];
          $_SESSION['redirect_origin']['auth_pw'] = $_SERVER['PHP_AUTH_PW'];
        }
      }

      $redirect = true;
    }

    if (!isset($login_request) || isset($_GET['login_request']) || isset($_POST['login_request']) || isset($_COOKIE['login_request']) || isset($_SESSION['login_request']) || isset($_FILES['login_request']) || isset($_SERVER['login_request'])) {
      $redirect = true;
    }

    if ($redirect == true) {
      osc_redirect(osc_href_link('login.php', (isset($_SESSION['redirect_origin']['auth_user']) ? 'action=process' : '')));
    }

    unset($redirect);
  }

// include the language translations
  $_system_locale_numeric = setlocale(LC_NUMERIC, 0);
  require(DIR_WS_LANGUAGES . $_SESSION['language'] . '.php');
  setlocale(LC_NUMERIC, $_system_locale_numeric); // Prevent LC_ALL from setting LC_NUMERIC to a locale with 1,0 float/decimal values instead of 1.0 (see bug #634)

// add test security
  $current_page = basename($PHP_SELF);
//   $current_page = basename($_SERVER['SCRIPT_FILENAME']);

  if (file_exists(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . $current_page)) {
    include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . $current_page);
  }

// define our localization functions
  require('includes/functions/localization.php');

// Include validation functions (right now only email address)
  require('includes/functions/validations.php');

// include authentification and verification for twitter
  require('includes/functions/twitter.php');

// include ckeditor wisywyg
  require('includes/functions/ckeditor.php');

// select the drop down for the different template  
  require('includes/functions/modules_template.php');

// setup our boxes
  require('includes/classes/table_block.php');
  require('includes/classes/box.php');

// initialize the message stack for output messages
  require('includes/classes/message_stack.php');
  $OSCOM_MessageStack = new messageStack;

// split-page-results
  require('includes/classes/split_page_results.php');

// entry/item info classes
  require('includes/classes/object_info.php');

// email classes
  require('includes/classes/mime.php');
  require('includes/classes/email.php');

// file uploading class
  require('includes/classes/upload.php');

// action recorder
  require('includes/classes/action_recorder.php');

// include mobile class
  require('ext/Mobile-Detect-master/Mobile_Detect.php');
  $OSCOM_DetectMobile = new Mobile_Detect;

// include odoo webservice
  if (ODOO_ACTIVATE_WEB_SERVICE == 'true') {
    require('includes/classes/odoo.php');
    $OSCOM_ODOO = new Odoo();
  }

// calculate category path
  if (isset($_GET['cPath'])) {
    $cPath = $_GET['cPath'];
  } else {
    $cPath = '';
  }

  if (osc_not_null($cPath)) {
    $cPath_array = osc_parse_category_path($cPath);
    $cPath = implode('_', $cPath_array);
    $current_category_id = $cPath_array[(sizeof($cPath_array)-1)];
  } else {
    $current_category_id = 0;
  }

// initialize configuration modules
  require('includes/classes/cfg_modules.php');
  $cfgModules = new cfg_modules();

  require(DIR_FS_CATALOG . 'includes/classes/hooks.php');
  $OSCOM_Hooks = new hooks('clicshoppingadmin');

// the following cache blocks are used in the Tools->Cache section
// ('language' in the filename is automatically replaced by available languages)
  $cache_blocks = array(array('title' => TEXT_CACHE_MANUFACTURERS, 'code' => 'manufacturers', 'file' => 'manufacturers_box-language.cache', 'multiple' => true),
                        array('title' => TEXT_CACHE_UPCOMING, 'code' => 'upcoming', 'file' => 'upcoming-language.cache', 'multiple' => true),
                        array('title' => TEXT_CACHE_ALSO_PURCHASED, 'code' => 'also_purchased', 'file' => 'also_purchased-language.cache', 'multiple' => true),
                        array('title' => TEXT_CACHE_PRODUCTS_RELATED, 'code' => 'products_related', 'file' => 'products_related-language.cache', 'multiple' => true),
                        array('title' => TEXT_CACHE_PRODUCTS_CROSS_SELL, 'code' => 'products_cross_sell', 'file' => 'products_cross_sell-language.cache', 'multiple' => true)
                       );

?>
