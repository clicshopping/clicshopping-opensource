 <?php 
/*
 * index_default.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
 require($OSCOM_Template->getTemplateHeaderFooter('header'));
?>
  <div class="contentContainer">
    <?php echo $OSCOM_Template->getBlocks('modules_front_page'); ?>
  </div>
<?php 
  require($OSCOM_Template->getTemplateHeaderFooter('footer'));