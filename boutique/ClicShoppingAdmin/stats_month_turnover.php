<?php
/*
 * stats_month_turnover.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/



   require ('includes/application_top.php');
// statistiques
  require('includes/functions/statistics.php');
  require_once('calcul_statistics.php');  

  require(DIR_FS_ADMIN  . 'includes/graphs/Artichow/LinePlot.class.php');

  echo osc_hide_session_id() ; 

$graph = new Graph(600, 200);

//$graph->setAntiAliasing(TRUE);

$x = array(
  $month_janvier,
    $month_fevrier, 
    $month_mars, 
    $month_avril, 
    $month_mai, 
    $month_juin, 
    $month_juillet, 
    $month_aout, 
    $month_septembre, 
    $month_octobre, 
    $month_novembre, 
    $month_decembre
);

$plot = new LinePlot($x);

$plot->grid->setNoBackground();

//$plot->title->set(TURN_OVER_CUMUL);

$plot->title->setFont(new TuffyBold(8));
$plot->title->setBackgroundColor(new Color(255, 255, 255, 25));
$plot->title->border->show();
$plot->title->setPadding(3, 3, 3, 3);
$plot->title->move(0, 20);

$plot->setSpace(4, 4, 10, 0);
// deplacement graphique
$plot->setPadding(40, 15, 10, 18);

$plot->setBackgroundGradient(
  new LinearGradient(
    new Color(210, 210, 210),
    new Color(255, 255, 255),
    0
  )
);

$plot->setColor(new Color(0, 0, 150, 20));

$plot->setFillGradient(
  new LinearGradient(
    new Color(150, 150, 210),
    new Color(245, 245, 245),
    0
  )
);

$plot->mark->setType(Mark::CIRCLE);
$plot->mark->border->show();

$plot->label->set($x);
$plot->label->move(5, -15);
//$plot->label->setBackgroundGradient(new LinearGradient(new Color(250, 250, 250, 10), new Color(255, 200, 200, 30), 0));
//$plot->label->border->setColor(new Color(20, 20, 20, 20));
$plot->label->setPadding(-50, -40, 1, 0);

$y = array(
  'JAN',
  'FEV',
  'MARS',
  'AVR',
  'MAI',
  'JUIN',
  'JUIL',
  'AOUT',
  'SEPT',
  'OCT',
  'NOV',
  'DEC'
);

$plot->xAxis->setLabelText($y);
$plot->xAxis->label->setFont(new Tuffy(7));

$graph->add($plot);
$graph->draw();

//require('includes/application_bottom.php');
