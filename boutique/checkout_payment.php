<?php
/*
 * checkout_payment.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the login page
  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($_SESSION['cart']->count_contents() < 1) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }

// if no shipping method has been selected, redirect the customer to the shipping method selection page
  if (!isset($_SESSION['shipping'])) {
    osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
  }

// avoid hack attempts during the checkout procedure by checking the internal cartID
  if (isset($_SESSION['cart']->cartID) && isset($_SESSION['cartID'])) {
    if ($_SESSION['cart']->cartID != $_SESSION['cartID']) {
      osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
    }
  }

// Stock Check
  if ( (STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true') ) {
    $products = $_SESSION['cart']->get_products();

    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
      if (osc_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
        osc_redirect(osc_href_link('shopping_cart.php'));
        break;
      }
    }
  }

// if no billing destination address was selected, use the customers own address as default
  if (!isset($_SESSION['billto'])) {
    $_SESSION['billto'] = $OSCOM_Customer->getDefaultAddressID();
  } else {
// verify the selected billing address
    if ( (is_array($_SESSION['billto']) && empty($_SESSION['billto'])) || is_numeric($_SESSION['billto']) ) {

      $QcheckAddress = $OSCOM_PDO->prepare('select count(*) as total 
                                            from :table_address_book
                                            where customers_id = :customers_id
                                            and address_book_id =  :address_book_id                                   
                                    ');
      $QcheckAddress->bindInt(':customers_id', $OSCOM_Customer->getID());
      $QcheckAddress->bindInt(':address_book_id', (int)$_SESSION['billto'] );
      $QcheckAddress->execute();
      
       if ($QcheckAddress->fetch() === false) {
        $_SESSION['billto'] = $OSCOM_Customer->getDefaultAddressID();
        if (isset($_SESSION['payment'])) unset($_SESSION['payment']);
      }
    }
  }

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

  if (isset($_POST['comments']) && osc_not_null($_POST['comments'])) {
    $_SESSION['comments'] = osc_db_prepare_input($_POST['comments']);
  }


  $total_weight = $_SESSION['cart']->show_weight();
  $total_count = $_SESSION['cart']->count_contents();

// load all enabled payment modules
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment;

  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_payment'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('checkout_shipping.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('checkout_payment.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('checkout_payment'));
