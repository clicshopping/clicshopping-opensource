<?php
/*
 * stats_discount_coupons.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/coupon.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading" witdth="300"><?php echo '&nbsp;' . HEADING_TITLE; ?>
 <?php
    if( isset( $_GET['cID'] ) && osc_not_null( $_GET['cID'] ) ) {
      if( isset( $_GET['custID'] ) && osc_not_null( $_GET['custID'] ) ) echo '<br />'.sprintf( HEADING_ORDERS_LIST, osc_customers_name($_GET['custID']), $_GET['cID'] );
      else echo '<br />'.sprintf( HEADING_CUSTOMERS_LIST, $_GET['cID'] );
     } else echo '<br />'.HEADING_COUPONS_LIST;
 ?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellpadding="5" cellspacing="0">
<?php
  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN- MAX_DISPLAY_SEARCH_RESULTS_ADMIN;
  if( isset( $_GET['cID'] ) && $_GET['cID'] !== '' ) {
    if( isset( $_GET['custID'] ) && (int)$_GET['custID'] !== 0 ) {
?>
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ORDER_TOTAL; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ORDER_DISCOUNT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?></td>
              </tr>
<?php
      $coupons_query_raw = "select o.orders_id,
                                     o.customers_name, 
                                     o.date_purchased, 
                                     s.orders_status_name, 
                                     ot.text as order_total 
                             from orders o inner join discount_coupons_to_orders dcto on dcto.orders_id = o.orders_id  left join orders_total ot on (o.orders_id = ot.orders_id),
                                  orders_status s
                             where o.customers_id = '" . (int)$_GET['custID'] . "' 
                             and o.orders_status = s.orders_status_id 
                             and s.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and ot.class = 'ot_total' and dcto.coupons_id='".$_GET['cID']."' 
                             order by orders_id DESC";

      $coupons_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $coupons_query_raw, $coupons_query_numrows);

      $rows = 0;
      $coupons_query = osc_db_query($coupons_query_raw);

      while ($coupons = osc_db_fetch_array($coupons_query)) {

        $Qot = $OSCOM_PDO->prepare('select ot.text as order_total
                                   from :table_orders_total ot
                                   where ot.orders_id = :orders_id
                                   and ot.class = :class
                                   ');

        $Qot->bindInt(':orders_id', $coupons['orders_id']);
        $Qot->bindValue(':class', 'ot_discount_coupon' );
        $Qot->execute();

        $order_discount = $Qot->fetch();

        $rows++;
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href='<?php echo osc_href_link('orders.php', 'action=edit&oID=' . $coupons['orders_id']); ?>'">
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('orders.php', 'action=edit&oID=' . $coupons['orders_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $coupons['customers_name']; ?></td>
                <td class="dataTableContent" align="right"><?php echo strip_tags($coupons['order_total']); ?></td>
                <td class="dataTableContent" align="right"><?php echo strip_tags($order_discount['order_total']); ?></td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($coupons['date_purchased']); ?></td>
                <td class="dataTableContent" align="right"><?php echo $coupons['orders_status_name']; ?></td>
              </tr>
<?php
      }
    } else {
?>
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" colspan="2"><?php echo TABLE_HEADING_CUSTOMER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MAX_USE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_USE_COUNT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_USE_STILL_AVAIL; ?></td>
              </tr>
<?php
      $coupons_query_raw = "select o.customers_name,
                                   o.customers_id, 
                                   dc.coupons_max_use, 
                                   COUNT(dcto.coupons_id) AS coupons_use_count 
                            from discount_coupons AS dc left join discount_coupons_to_orders AS dcto ON dc.coupons_id = dcto.coupons_id 
                                                        left join orders as o on dcto.orders_id=o.orders_id 
                            where dc.coupons_id='".$_GET['cID']."' 
                            group by dc.coupons_id, 
                                     o.customers_id 
                            order by coupons_use_count desc, 
                                     dc.coupons_id asc";
      $coupons_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $coupons_query_raw, $coupons_query_numrows);

      $rows = 0;

      $coupons_query = osc_db_query($coupons_query_raw);
      while ($coupons = osc_db_fetch_array($coupons_query)) {

        $rows++;
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href='<?php echo osc_href_link('stats_discount_coupons.php', 'custID='.$coupons['customers_id'].'&cID='.$cID.'&origin=' . 'stats_discount_coupons.php' . '?page=' . $_GET['page']); ?>'">
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo '<a href="'.osc_href_link('orders.php', 'cID='.$coupons['customers_id'].'&action=edit&origin=' . 'stats_discount_coupons.php' . '?page=' . $_GET['page']) . '">'.$coupons['customers_name'].'</a>'; ?></td>
                <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_max_use'] == 0 ? ''.TABLE_CONTENT_UNLIMITED.'' : $coupons['coupons_max_use'] ); ?></td>
                <td class="dataTableContent" align="center"><?php echo $coupons['coupons_use_count']; ?>&nbsp;</td>
                <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_max_use'] == 0 ? ''.TABLE_CONTENT_UNLIMITED.'' : ( $coupons['coupons_max_use'] - $coupons['coupons_use_count'] ) ); ?></td>
              </tr>
<?php
      }
    }
  } else {
    require('includes/classes/currencies.php');
    $currencies = new currencies();
?>
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" colspan="2"><?php echo TABLE_HEADING_CODE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PERCENTAGE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_NUM_AVAIL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_USE_COUNT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_NUM_STILL_AVAIL; ?></td>
              </tr>
<?php
    $coupons_query_raw = "select dc.*,
                                COUNT(dcto.coupons_id) AS coupons_use_count 
                          from discount_coupons AS dc left join discount_coupons_to_orders AS dcto ON dc.coupons_id = dcto.coupons_id
                          group by dc.coupons_id 
                          order by coupons_use_count desc, 
                                   dc.coupons_id asc";
    $coupons_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $coupons_query_raw, $coupons_query_numrows);

    $rows = 0;

    $coupons_query = osc_db_query($coupons_query_raw);
    while ($coupons = osc_db_fetch_array($coupons_query)) {

      $rows++;
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" <?php echo ( $coupons['coupons_use_count'] == 0 ? '' : 'onclick="document.location.href=\''.osc_href_link('stats_discount_coupons.php', 'cID='.$coupons['coupons_id'].'&origin=' . 'stats_discount_coupons.php' . '?page=' . $_GET['page']).'\'"' ); ?>>
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo ( $coupons['coupons_use_count'] == 0 ? $coupons['coupons_id'] : '<a href="'.osc_href_link('discount_coupons.php', 'cID='.$coupons['coupons_id'].'&action=edit&origin=' . 'stats_discount_coupons.php' . '?page=' . $_GET['page']) . '">'.$coupons['coupons_id'].'</a>' ); ?></td>
                <td class="dataTableContent">
<?php 
    switch( $coupons['coupons_discount_type'] ) {
      case 'shipping':
        echo ( $coupons['coupons_discount_amount'] * 100 ).'% '.TEXT_DISPLAY_SHIPPING_DISCOUNT;
        break;
      case 'percent':
        echo ( $coupons['coupons_discount_amount'] * 100 ).'%';
        break;
      case 'fixed':
        echo $currencies->format( $coupons['coupons_discount_amount'] );
        break;
    }
?>                
                </td>
                <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_number_available'] == 0 ? ''.TABLE_CONTENT_UNLIMITED.'' : $coupons['coupons_number_available'] ); ?></td>
                <td class="dataTableContent" align="center"><?php echo $coupons['coupons_use_count']; ?>&nbsp;</td>
                <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_number_available'] == 0 ? ''.TABLE_CONTENT_UNLIMITED.'' : ( $coupons['coupons_number_available'] - $coupons['coupons_use_count'] ) ); ?></td>
              </tr>
<?php
    }
   }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $coupons_split->display_count($coupons_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php echo $coupons_split->display_links($coupons_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

