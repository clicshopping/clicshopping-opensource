<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Créer un compte professionnel');

define('HEADING_TITLE', 'Créer un compte professionnel');

define('EMAIL_SUBJECT', 'Demande d\'ouverture d\'un compte professionnel client sur ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");

define('EMAIL_WELCOME', 'Nous vous remercions pour la confiance que vous nous témoigniez en vous enregistrant comme nouveau client sur le site ' . HTTP_COOKIE_DOMAIN . "\n\n" .'<div align="justify">Nous avons pris votre demande d\'inscription pour l\'ouverture d\'un compte professionnel auprès de notre réseau.<br /><br />Vous allez recevoir très rapidement un email vous notifiant la création de votre compte après validation de votre demande par notre service clientèle. <br />Vous recevrez l\'ensemble des informations vous permettant de vous connecter et de réaliser vos achats sur '.utf8_decode(STORE_NAME) .'<br /><br />Vous pourrez alors au titre de client privilégié bénéficier de prix exceptionnels sur l\'ensemble de nos articles en vous connectant sur votre espace client de '. utf8_decode(STORE_NAME).'<br /><br />Pour toute aide sur nos services, n\'hésitez pas à contacter notre service clientèle  : ' . STORE_OWNER_EMAIL_ADDRESS);
define('ADMIN_EMAIL_WELCOME', 'Bonjour,'. "\n\n" . '');
define('ADMIN_EMAIL_TEXT', 'Un nouveau client (mode B2b) vient de s\'enregistrer, vous trouverez les informations ci-dessous :<br />
Nom :' . $_POST['lastname'] . '<br />
Prenom :' . $_POST['firstname'] . '<br />
Compagnie :' . $_POST['company'] . '<br />
E-Mail :' . $_POST['email_address'] . '<br />
');

define('EMAIL_TEXT_COUPON', STORE_NAME . ' se fait un plaisir de vous offrir un coupon remise sur votre prochaine commande que vous pourrez utiliser n\'importe quand sur la boutique (sauf limite de validité).' . "\n\n" . ' Le numéro du coupon est : <strong>');

define('EMAIL_NETWORK',''. "\n\n" . 'Suivez nous en temps reel sur :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');

define('ENTRY_WEBSITE_TEXT_PRO','*');
define('ENTRY_COMPANY_WEBSITE','Site internet :');
?>
