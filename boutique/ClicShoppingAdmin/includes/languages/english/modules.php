<?php
/*
 * modules.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE_MODULES_PAYMENT', 'Payment modules');
define('HEADING_TITLE_MODULES_SHIPPING', 'Shipping modules');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Order total modules');
define('HEADING_TITLE_MODULES_ACTION_RECORDER', 'Action recorder modules');
define('HEADING_TITLE_MODULES_SOCIAL_BOOKMARKS', 'Social networks modules');
define('HEADING_TITLE_MODULES_HEADER_TAGS', 'Header Tag Modules');
define('HEADING_TITLE_MODULES_ADMIN_DASHBOARD', 'Admin Dashboard Modules');

define('TABLE_HEADING_MODULES', 'Modules');
define('TABLE_HEADING_SORT_ORDER', 'Sort Order');
define('TABLE_HEADING_ACTION', 'Action');
define('TEXT_INFO_API_VERSION', 'API Version : ');

define('TEXT_MODULE_DIRECTORY', 'Module Directory:');
?>
