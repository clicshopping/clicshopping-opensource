<?php
/*
 * stats_suppliers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Orders suppliers');

define('TABLE_HEADING_SUPPLIERS_ID','Id Supplers');
define('TABLE_HEADING_SUPPLIERS', 'Supplier');
define('TABLE_HEADING_MANAGER', 'Manager');
define('TABLE_HEADING_EMAIL', 'email');
define('TABLE_HEADING_PHONE','Telephone');
define('TABLE_HEADING_ORDER','Orders Number');
define('TABLE_HEADING_ACTION','Actions');

define('HEADING_TITLE_PRODUCTS_BY_SUPPLIERS','Order detail');
define('TABLE_HEADING_MODEL','Product model');
define('TABLE_HEADING_SUPPLIERS_NAME','Supplier name');
define('TABLE_HEADING_PRODUCTS_NAME','Product name');	
define('TABLE_HEADING_PRODUCTS_OPTIONS','Product option');
define('TABLE_HEADING_PRODUCTS_OPTIONS_VALUES','Option value');
define('TABLE_HEADING_QUANTITY','Quantity');

define('ENTRY_STATUS','order Status');

define('HEADING_TITLE_PRODUCTS_BY_CUSTOMERS','Client detail for a supplier');
define('TABLE_HEADING_CUSTOMERS_id','Id Customer');
define('TABLE_HEADING_CUSTOMERS_MANE','Customer name');

define('PERIOD','Period ');
define('ICON_STATS_SUPPLIERS_EDIT_CUSTOMERS','See the client orders');
define('IMAGE_STATS_SUPPLIERS_PDF','Edit the pdf report');
define('IMAGE_STATS_SUPPLIERS_EDIT','See the orders for this suppliers');
define('IMAGE_ICON_INFO','All the orders for this client');
define('ICON_EDIT_ORDERS','See all client orders');
define('ICON_EDIT_PRODUCTS','More informations for this product');

define('TABLE_HEADING_PRODUCTS', 'products');

define('TEXT_ALL_STATUS','Tous les status');

define('ENTRY_START_DATE','Start Analyse'); 
define('ENTRY_TO_DATE','End Analyse');
define('ENTRY_SUBMIT','Go');


?>

