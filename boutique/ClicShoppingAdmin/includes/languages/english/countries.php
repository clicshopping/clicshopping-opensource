<?php
/**
 * countires.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Countries');

define('TABLE_HEADING_COUNTRY_NAME', 'Country');
define('TABLE_HEADING_COUNTRY_CODES', 'ISO Codes');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_COUNTRY_STATUS','Status');

define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_COUNTRY_NAME', 'Name:');
define('TEXT_INFO_COUNTRY_CODE_2', 'ISO Code (2):');
define('TEXT_INFO_COUNTRY_CODE_3', 'ISO Code (3):');
define('TEXT_INFO_ADDRESS_FORMAT', 'Address Format:');
define('TEXT_INFO_INSERT_INTRO', 'Please enter the new country with its related data');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this country?');
define('TEXT_INFO_HEADING_NEW_COUNTRY', 'New Country');
define('TEXT_INFO_HEADING_EDIT_COUNTRY', 'Edit Country');
define('TEXT_INFO_HEADING_DELETE_COUNTRY', 'Delete Country');
?>
