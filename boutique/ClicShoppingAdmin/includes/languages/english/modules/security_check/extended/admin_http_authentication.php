<?php
/**
 * admin_http_authentification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_ADMIN_HTTP_AUTHENTICATION_ERROR', 'HTTP Authentication has not been set up for the osCommerce Administration Tool - please set this up in your web server configuration to further protect the Administration Tool from unauthorized access.');
?>
