<?php
/**
 * defeult_currency.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('ERROR_NO_DEFAULT_CURRENCY_DEFINED', 'Erreur: La monnaie par défaut n\a pas été sélectionnée.  Veuillez faire la rectification: Configuration / Divers / Devises');
?>
