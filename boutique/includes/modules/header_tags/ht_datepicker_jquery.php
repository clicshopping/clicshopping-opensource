<?php
/*  ht_datepicker_jquery
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  class ht_datepicker_jquery {
    var $code = 'ht_datepicker_jquery';
    var $group = 'footer_scripts';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function ht_datepicker_jquery() {
      $this->title = MODULE_HEADER_TAGS_DATEPICKER_JQUERY_TITLE;
      $this->description = MODULE_HEADER_TAGS_DATEPICKER_JQUERY_DESCRIPTION;

      if ( defined('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_STATUS') ) {
        $this->sort_order = MODULE_HEADER_TAGS_DATEPICKER_JQUERY_SORT_ORDER;
        $this->enabled = (MODULE_HEADER_TAGS_DATEPICKER_JQUERY_STATUS == 'True');
      }
    }

    function execute() {
      global $PHP_SELF, $OSCOM_Template;

      if (osc_not_null(MODULE_HEADER_TAGS_DATEPICKER_JQUERY_PAGES)) {
        $pages_array = array();

        foreach (explode(';', MODULE_HEADER_TAGS_DATEPICKER_JQUERY_PAGES) as $page) {
          $page = trim($page);

          if (!empty($page)) {
            $pages_array[] = $page;
          }
        }

        if (in_array(basename($PHP_SELF), $pages_array)) {
          $OSCOM_Template->addBlock('<link rel="stylesheet" href="'.  DIR_WS_SOURCES .'javascript/jquery/datepicker/less/datepicker.less" />' . "\n", 'header_tags');

          $OSCOM_Template->addBlock('<script src="' . DIR_WS_SOURCES .'javascript/jquery/datepicker/js/bootstrap-datepicker.js"></script>' . "\n", $this->group);
          $OSCOM_Template->addBlock('<script src="' . DIR_WS_SOURCES .'javascript/jquery/datepicker/locales/' . BOOTSTRAP_DATEPICKER_I18N_CODE . '">"></script>' . "\n", $this->group);
          $OSCOM_Template->addBlock('<script>var nowTemp = new Date(); var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);</script>', $this->group);
        }
      }
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_STATUS');
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Datepicker jQuery Module', 'MODULE_HEADER_TAGS_DATEPICKER_JQUERY_STATUS', 'True', 'Do you want to enable the Datepicker module?', '6', '1', 'osc_cfg_select_option(array(\'True\', \'False\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Pages', 'MODULE_HEADER_TAGS_DATEPICKER_JQUERY_PAGES', '" . implode(';', $this->get_default_pages()) . "', 'The pages to add the Datepicker jQuery Scripts to.', '6', '0', 'ht_datepicker_jquery_show_pages', 'ht_datepicker_jquery_edit_pages(', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_HEADER_TAGS_DATEPICKER_JQUERY_SORT_ORDER', '910', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
      osc_db_query("update configuration set configuration_value = '1' where configuration_key = 'WEBSITE_MODULE_INSTALLED'");
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_STATUS',
                   'MODULE_HEADER_TAGS_DATEPICKER_JQUERY_SORT_ORDER',
                   'MODULE_HEADER_TAGS_DATEPICKER_JQUERY_PAGES'
                   );
    }

    function get_default_pages() {
        return array('advanced_search.php',
                    'account_edit.php',
                    'create_account.php',
                    'create_account_pro.php'
                    );
    }
  }

  function ht_datepicker_jquery_show_pages($text) {
    return nl2br(implode("\n", explode(';', $text)));
  }

  function ht_datepicker_jquery_edit_pages($values, $key) {
    global $PHP_SELF;

    $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $files_array = array();
    if ($dir = @dir(DIR_FS_CATALOG)) {
      while ($file = $dir->read()) {
        if (!is_dir(DIR_FS_CATALOG . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $files_array[] = $file;
          }
        }
      }
      sort($files_array);
      $dir->close();
    }

    $values_array = explode(';', $values);

    $output = '';
    foreach ($files_array as $file) {
      $output .= osc_draw_checkbox_field('ht_datepicker_jquery_file[]', $file, in_array($file, $values_array)) . '&nbsp;' . osc_output_string($file) . '<br />';
    }

    if (!empty($output)) {
      $output = '<br />' . substr($output, 0, -6);
    }

    $output .= osc_draw_hidden_field('configuration[' . $key . ']', '', 'id="htrn_files"');

    $output .= '<script>
                function htrn_update_cfg_value() {
                  var htrn_selected_files = \'\';

                  if ($(\'input[name="ht_datepicker_jquery_file[]"]\').length > 0) {
                    $(\'input[name="ht_datepicker_jquery_file[]"]:checked\').each(function() {
                      htrn_selected_files += $(this).attr(\'value\') + \';\';
                    });

                    if (htrn_selected_files.length > 0) {
                      htrn_selected_files = htrn_selected_files.substring(0, htrn_selected_files.length - 1);
                    }
                  }

                  $(\'#htrn_files\').val(htrn_selected_files);
                }

                $(function() {
                  htrn_update_cfg_value();

                  if ($(\'input[name="ht_datepicker_jquery_file[]"]\').length > 0) {
                    $(\'input[name="ht_datepicker_jquery_file[]"]\').change(function() {
                      htrn_update_cfg_value();
                    });
                  }
                });
                </script>';

    return $output;
  }
?>