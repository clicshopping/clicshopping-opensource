<?php
/*
 * password_forgotten.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('EMAIL_PASSWORD_REMINDER_SUBJECT', ' '. STORE_NAME . ' - Information concernant votre nouveau mot de passe');
define('EMAIL_PASSWORD_REMINDER_BODY', 'Bonjour, ' . "\n\n" . 'Veuillez trouver ci-dessous votre nouveau mot de passe pour vous connecter sur ' . STORE_NAME . "\n\n" . 'Votre nom d\'utilisateur : %s' . "\n\n" . 'Votre nouveau mot de passe : %s' . "\n\n".'Très cordialement,'. "\n\n" . '<strong>L\'équipe '.STORE_NAME.'</strong>'. "\n\n" . '
<u><font size="2">Avis de confidentialité :</u>' . "\n\n" . 'Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans être le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re�u ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n\n" . '
<p>Conformément à la Loi dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</font>
</div>');

define('TEXT_NEW_PASSWORD', 'Un e-mail avec un nouveau mot de passe vient d\'être envoyé à');
?>
