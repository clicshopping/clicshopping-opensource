<?php
/**
 * pi_products_info.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license Licensed under GPL and MIT
 * @version $Id: 
*/

define('TABLE_HEADING','');
define('TABLE_HEADING_NEW_PRODUCTS','News et collections');

define('PRODUCTS_NOT_SELL','Product no available');

define('TEXT_CLICK_TO_ENLARGE', 'Click to enlarge');
define('TEXT_CURRENT_REVIEWS', 'Current Reviews:');
define('TEXT_DATE_ADDED', 'This product was added to our catalog on %s.');
define('TEXT_DATE_AVAILABLE', '<p style="color:#ff0000;"><strong>This product will be in stock on %s.</strong></style>');
define('TEXT_MORE_INFORMATION', 'For more information, please visit this products <a href="%s" target="_blank"><u>webpage</u></a>.');

define('TEXT_PRODUCT_MODEL', 'Model : ');
define('TEXT_PRODUCT_NOT_FOUND', 'Product not found!');
define('TEXT_PRODUCT_OPTIONS', 'Available Options :');
define('BUTTON_IN_CART','Add Shopping cart');

?>
