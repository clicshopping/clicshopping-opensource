<?php
/**
 * security_checks.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Security Checks');

define('TABLE_HEADING_MODULES', 'Modules');
define('TABLE_HEADING_INFO', 'Info');
?>