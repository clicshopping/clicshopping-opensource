        </div>
      </div>
    </div>
    <div id="menu">
      <nav>
        <h2><i class="fa fa-reorder"></i>ClicShopping</h2>
        <ul>
<!--
          <li>
            <a href="index.php"><?php echo MENU_HOME; ?></a>
          </li>
-->
          <li>
            <a href="#"><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></a>
            <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
            <ul>
              <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
<?php
// ----------------------
// Configuration
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_STORE; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_STORE; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="configuration.php?gID=1"><?php echo MENU_CONFIGURATION_GENERAL; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=25"><?php echo MENU_CONFIGURATION_LEGACY; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=2"><?php echo MENU_CONFIGURATION_BANK; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=34"><?php echo MENU_CONFIGURATION_SEO; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=3"><?php echo MENU_CONFIGURATION_MAXIMUM; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=7"><?php echo MENU_CONFIGURATION_SHIPPING; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=9"><?php echo MENU_CONFIGURATION_STOCK; ?></a>
                    </li>
                    <li>
                      <a href="products_quantity_unit.php"><?php echo MENU_CONFIGURATION_PRODUCTS_QUANTITY_UNIT; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=12"><?php echo MENU_CONFIGURATION_MAIL; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=13"><?php echo MENU_CONFIGURATION_DOWNLOAD; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>


<?php
// ----------------------
// Configuration Administration
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_STORE_ADMINISTRATION; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_STORE_ADMINISTRATION; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="administrators.php"><?php echo MENU_CONFIGURATION_ADMINISTRATOR; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=dashboard"><?php echo BOX_MODULES_ADMIN_DASHBOARD; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=21"><?php echo MENU_CONFIGURATION_MAXIMUM; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=23"><?php echo MENU_CONFIGURATION_IMAGES; ?></a>
                    </li>
                    <li>
                      <a href="orders_status_invoice.php"><?php echo MENU_PRINT_ORDERS_STATUS_INVOICE; ?></a>
                    </li>
                    <li>
                      <a href="orders_status_tracking.php"><?php echo MENU_CONFIGURATION_ORDERS_STATUS_TRACKING; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=26"><?php echo MENU_EDIT_ORDERS_STATUS_INVOICE; ?></a>
                    </li>
                    <li>
                      <a href="orders_status.php"><?php echo MENU_LOCALIZATION_ORDERS_STATUS; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=24"><?php echo MENU_CONFIGURATION_QUICK_UPDATES; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=36"><?php echo MENU_CONFIGURATION_EXPORT; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=44"><?php echo MENU_CONFIGURATION_WEBSERVICE; ?></a>
                    </li>
                  </ul>
                </ul>
               </li>
<?php
// ----------------------
// Configuration B2C
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_B2C; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_B2C; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="configuration.php?gID=22"><?php echo MENU_CONFIGURATION_B2C_GESTION; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=5"><?php echo MENU_CONFIGURATION_CUSTOMERS; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=16"><?php echo MENU_CONFIGURATION_B2C_MINIMUM; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Configuration B2B
// ---------------------
  if (MODE_B2B_B2C == 'true') {
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_B2B; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_B2B; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="configuration.php?gID=17"><?php echo MENU_CONFIGURATION_B2B_GESTION; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=18"><?php echo MENU_CONFIGURATION_B2B_CUSTOMERS; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=19"><?php echo MENU_CONFIGURATION_B2B_MINIMUM; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=20"><?php echo MENU_CONFIGURATION_B2B_MAXIMUM; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
  }
// ----------------------
// Modules
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_MODULES; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_MODULES; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="modules.php?set=action_recorder"><?php echo MENU_CONFIGURATION_MODULES_ACTION_RECORDER; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=payment"><?php echo MENU_CONFIGURATION_MODULES_PAYMENT; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=shipping"><?php echo MENU_CONFIGURATION_MODULES_SHIPPING; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=order_total"><?php echo MENU_CONFIGURATION_MODULES_ORDERTOTAL; ?></a>
                    </li>
                    <li>
                      <a href="modules_hooks.php"><?php echo MENU_CONFIGURATION_MODULES_HOOKS; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Referencement
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_REFERENCEMENT_STATS; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_REFERENCEMENT_STATS; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="page_submit.php"><?php echo MENU_CONFIGURATION_REFERENCEMENT_META_DATA; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=social_bookmarks"><?php echo MENU_CONFIGURATION_MODULES_SOCIAL_BOOKMARKS; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=header_tags"><?php echo MENU_CONFIGURATION_MODULES_HEADER_TAG; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Lieux et taxes
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_LOCATION_AND_TAXES; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_LOCATION_AND_TAXES; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="countries.php"><?php echo MENU_TAXES_COUNTRIES; ?></a>
                    </li>
                    <li>
                      <a href="zones.php"><?php echo MENU_TAXES_ZONES; ?></a>
                    </li>
                    <li>
                      <a href="geo_zones.php"><?php echo MENU_TAXES_GEO_ZONE; ?></a>
                    </li>
                    <li>
                      <a href="tax_classes.php"><?php echo MENU_TAXES_TAX_CLASSES; ?></a>
                    </li>
                    <li>
                      <a href="tax_rates.php"><?php echo MENU_TAXES_TAX_RATES; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Divers
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_LOCALIZATION; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_LOCALIZATION; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="currencies.php"><?php echo MENU_LOCALIZATION_CURRENCIES; ?></a>
                    </li>
                    <li>
                      <a href="languages.php"><?php echo MENU_LOCALIZATION_LANGUAGES; ?></a>
                    </li>
                    <li>
                      <a href="<?php echo 'template_email.php'; ?>"><?php echo MENU_TOOLS_TEMPLATE_EMAIL; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Session et cache
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_SESSION; ?></a>
                <h2><i class="fa fa-sun-o"></i><?php echo BOX_HEADING_CONFIGURATION; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-sun-o"></i><?php echo MENU_CONFIGURATION_SESSION; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="configuration.php?gID=11"><?php echo MENU_CONFIGURATION_SESSION_CACHE; ?></a>
                    </li>
                    <li>
                      <a href="cache.php"><?php echo MENU_CONFIGURATION_SESSION_CONTROLE_CACHE; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=14"><?php echo MENU_CONFIGURATION_SESSION_GZIP; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=10"><?php echo MENU_CONFIGURATION_SESSION_LOGGING; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=15"><?php echo MENU_CONFIGURATION_SESSION_SESSION; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
            </ul>
          </li>
<!------------------
  Menu 2
-------------------->
          <li>
            <a href="#"><i class="fa fa-book"></i><?php echo BOX_HEADING_CATALOG; ?></a>
            <h2><i class="fa fa-book"></i><?php echo BOX_HEADING_CATALOG; ?></h2>
            <ul>
              <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_CATALOG; ?></h2>
              <ul>
                <li>
                  <a href="categories.php"><?php echo MENU_CATALOG_CATEGORIES_PRODUCTS ?></a>
                </li>
                <li>
                  <a href="quick_updates.php"><?php echo MENU_CATALOG_PRICE_UPDATE; ?></a>
                </li>
                <li>
                  <a href="products_attributes.php"><?php echo MENU_CATALOG_PRODUCTS_ATTRIBUTES; ?></a>
                </li>
                <li>
                  <a href="products_extra_fields.php"><?php echo MENU_CATALOG_PRODUCTS_EXTRA_FIELDS; ?></a>
                </li>
                <li>
                  <a href="manufacturers.php"><?php echo MENU_CATALOG_MANUFACTURERS; ?></a>
                </li>

                <li>
                  <a href="suppliers.php"><?php echo MENU_CATALOG_SUPPLIERS; ?></a>
                </li>
                <li>
                  <a href="products_archive.php"><?php echo MENU_CATALOG_PRODUCTS_ARCHIVES; ?></a>
                </li>
              </ul>
            </ul>
          </li>
<!------------------
    Menu 3
-------------------->

          <li>
            <a href="#"><i class="fa fa-female"></i><?php echo BOX_HEADING_CUSTOMERS; ?></a>
            <h2><i class="fa fa-fa-female"></i><?php echo BOX_HEADING_CUSTOMERS; ?></h2>
            <ul>
              <h2><i class="fa fa-female"></i><?php echo BOX_HEADING_CUSTOMERS; ?></h2>
              <ul>
                <li>
                  <a href="orders.php"><?php echo MENU_CUSTOMERS_ORDERS; ?></a>
                </li>
                <li>
                  <a href="customers.php"><?php echo MENU_CUSTOMERS; ?></a>
                </li>
<?php
  if (MODE_B2B_B2C =='true') {
?>
                <li>
                  <a href="members.php"><?php echo MENU_CUSTOMERS_MEMBERS_B2B; ?></a>
                </li>
                <li>
                  <a href="customers_groups.php"><?php echo MENU_CUSTOMERS_GROUP_B2B; ?></a>
                </li>
<?php
  }
?>
                <li>
                  <a href="reviews.php"><?php echo MENU_CATALOG_REVIEWS; ?></a>
                </li>
                <li>
                  <a href="mail.php"><?php echo MENU_CUSTOMERS_MAIL; ?></a>
                </li>
                <li>
                  <a href="contact_customers.php"><?php echo MENU_CONTACT_CUSTOMERS; ?></a>
                </li>
              </ul>
            </ul>
          </li>
<!------------------
  Menu 3 Marketing
-------------------->
          <li>
            <a href="#"><i class="fa fa-shopping-cart"></i><?php echo BOX_HEADING_MARKETING; ?></a>
            <h2><i class="fa fa-shopping-cart"></i><?php echo BOX_HEADING_MARKETING; ?></h2>
            <ul>
              <h2><i class="fa fa-shopping-cart"></i><?php echo BOX_HEADING_MARKETING; ?></h2>
              <ul>
                <li>
                  <a href="specials.php"><?php echo MENU_CATALOG_SPECIALS; ?></a>
                </li>
                <li>
                  <a href="discount_coupons.php"><?php echo MENU_MARKETING_DISCOUNT_COUPONS; ?></a>
                </li>
                <li>
                  <a href="products_heart.php"><?php echo MENU_MARKETING_PRODUCTS_HEART; ?></a>
                </li>
                <li>
                  <a href="products_featured.php"><?php echo MENU_MARKETING_PRODUCTS_FEATURED; ?></a>
                </li>
                <li>
                  <a href="products_related.php"><?php echo MENU_MARKETING_PRODUCTS_RELATED; ?></a>
                </li>
                <li>
                  <a href="banner_manager.php"><?php echo MENU_MARKETING_BANNER_MANAGER; ?></a>
                </li>
              </ul>
            </ul>
          </li>
<!------------------
 Menu 4 Communication
-------------------->
          <li>
            <a href="#"><i class="fa fa-microphone"></i><?php echo BOX_HEADING_COMMUNICATION; ?></a>
            <h2><i class="fa fa-microphone"></i><?php echo BOX_HEADING_COMMUNICATION; ?></h2>
            <ul>
              <h2><i class="fa fa-microphone"></i><?php echo BOX_HEADING_COMMUNICATION; ?></h2>
              <ul>
                <li>
                  <a href="newsletters.php"><?php echo MENU_COMMUNICATION_NEWSLETTER; ?></a>
                </li>
                <li>
                  <a href="<?php echo 'page_manager.php'; ?>"><?php echo MENU_COMMUNICATION_PAGE_INFORMATION; ?></a>
                </li>
                <li>
                  <a href="<?php echo 'blog.php'; ?>"><?php echo MENU_COMMUNICATION_BLOG; ?></a>
                </li>
              </ul>
            </ul>
          </li>
<!------------------
    Menu 5 rapport stats
-------------------->
          <li>
            <a href="#"><i class="fa fa-briefcase"></i><?php echo BOX_HEADING_REPORTS; ?></a>
            <h2><i class="fa fa-briefcase"></i><?php echo BOX_HEADING_REPORTS; ?></h2>
            <ul>
              <h2><i class="fa fa-briefcase"></i><?php echo BOX_HEADING_REPORTS; ?></h2>
<?php
// ----------------------
// Rapport Statistiques
// ---------------------
?>

              <li>
                <a href="#"><i class="fa fa-briefcase"></i><?php echo MENU_REPORTS_STATS; ?></a>
                <h2><i class="fa fa-briefcase"></i><?php echo BOX_HEADING_REPORTS; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-briefcase"></i><?php echo MENU_REPORTS_STATS; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="stats_products_purchased.php"><?php echo MENU_REPORTS_PRODUCTS_PURCHASED; ?></a>
                    </li>
                    <li>
                      <a href="stats_products_viewed.php"><?php echo MENU_REPORTS_PRODUCTS_VIEWED; ?></a>
                    </li>
                    <li>
                      <a href="stats_products_no_viewed.php"><?php echo MENU_REPORTS_PRODUCTS_NO_VIEWED; ?></a>
                    </li>
                    <li>
                      <a href="stats_products_no_purchased.php"><?php echo MENU_REPORTS_PRODUCTS_NO_PURCHASED; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Rapport rapport gestion financiere
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-briefcase"></i><?php echo MENU_REPORTS_STATS_FINANCIAL_MANAGEMENT; ?></a>
                <h2><i class="fa fa-briefcase"></i><?php echo BOX_HEADING_REPORTS; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-briefcase"></i><?php echo MENU_REPORTS_STATS_FINANCIAL_MANAGEMENT; ?></h2>
                  </li>
                  <ul>
<?php
  if (MODE_B2B_B2C =='true') {
?>
                      <li>
                        <a href="stats_margin_report.php"><?php echo MENU_REPORTS_MARGIN_REPORT; ?></a>
                      </li>
<?php
  }
?>
                    <li>
                      <a href="<?php echo 'stats_discount_coupons.php'; ?>"><?php echo MENU_REPORTS_DISCOUNT; ?></a>
                    </li>
                    <li>
                      <a href="stats_customers.php"><?php echo MENU_REPORTS_ORDERS_TOTAL; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Rapport gestion produit
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-briefcase"></i><?php echo MENU_REPORTS_STATS_PRODUCTS_MANAGEMENT; ?></a>
                <h2><i class="fa fa-briefcase"></i><?php echo BOX_HEADING_REPORTS; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-briefcase"></i><?php echo MENU_REPORTS_STATS_PRODUCTS_MANAGEMENT; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="stats_products_notification.php"><?php echo MENU_REPORTS_PRODUCTS_NOTIFICATIONS; ?></a>
                    </li>
                    <li>
                      <a href="stats_low_stock.php"><?php echo MENU_REPORTS_PRODUCTS_LOW_STOCK; ?></a>
                    </li>
                    <li>
                      <a href="products_expected.php"><?php echo MENU_REPORTS_PRODUCTS_EXPECTED; ?></a>
                    </li>
<?php
  if (MODE_B2B_B2C =='true') {
?>
                    <li>
                      <a href="stats_products_wharehouse.php"><?php echo MENU_REPORTS_PRODUCTS_WHAREHOUSE; ?></a>
                    </li>
<?php
  }
?>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// autres menu rapport
// ---------------------
?>
              <ul>
                <li>
                  <a href="<?php echo 'stats_suppliers.php'; ?>"><?php echo MENU_REPORTS_SUPPLIERS; ?></a>
                </li>
                <li>
                  <a href="<?php echo 'stats_newsletter_no_account.php'; ?>"><?php echo MENU_REPORTS_NEWSLETTER_NO_ACCOUNT; ?></a>
                </li>
                <li>
                  <a href="<?php echo 'stats_email_validation.php'; ?>"><?php echo MENU_REPORTS_EMAIL_VALIDATION; ?></a>
                </li>
              </ul>
            </ul>
          </li>

<!------------------
  Menu 6
-------------------->
          <li>
            <a href="#"><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></a>
            <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
            <ul>
              <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
<?php
// ----------------------
// Menu configuration du design
// ---------------------
?>

              <li>
                <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_CONFIGURATION_DESIGN; ?></a>
                <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-laptop"></i><?php echo MENU_CONFIGURATION_DESIGN; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="configuration.php?gID=43"><?php echo MENU_CONFIGURATION_DESIGN; ?></a>
                    </li>
                    <li>
                      <a href="configuration.php?gID=4"><?php echo MENU_CONFIGURATION_IMAGES; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
<?php
// ----------------------
// Menu Listing
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_LISTING; ?></a>
                <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_LISTING; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="modules.php?set=modules_products_new"><?php echo MENU_DESIGN_PRODUCTS_NEW; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=modules_products_heart"><?php echo MENU_DESIGN_PRODUCTS_HEART; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=modules_products_featured"><?php echo MENU_DESIGN_PRODUCTS_FEATURED; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=modules_products_special"><?php echo MENU_DESIGN_PRODUCTS_SPECIAL; ?></a>
                    </li>
                  </ul>
                  <li>
                    <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_PRODUCTS_LISTING; ?></a>
                    <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                    <ul>
                      <li>
                        <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_PRODUCTS_LISTING; ?></h2>
                      </li>
                      <ul>
                        <li>
                          <a href="modules.php?set=modules_products_listing"><?php echo MENU_DESIGN_PRODUCTS_LISTING; ?></a>
                        </li>
                        <li>
                          <a href="configuration.php?gID=8"><?php echo MENU_DESIGN_PRODUCTS_LISTING_SORT_ORDER; ?></a>
                        </li>
                      </ul>
                    </ul>
                  </li>
                </ul>

<?php
// ----------------------
// Menu Design Index& categories
// ---------------------
?>
                <li>
                  <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_INDEX; ?></a>
                  <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                  <ul>
                    <li>
                      <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_INDEX; ?></h2>
                    </li>
                    <ul>
                      <li>
                        <a href="modules.php?set=modules_front_page"><?php echo MENU_DESIGN_NEW; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_index_categories"><?php echo MENU_DESIGN_CATEGORIES; ?></a>
                      </li>
                    </ul>
                  </ul>
                </li>
<?php
// ----------------------
// Menu Design Index& categories
// ---------------------
?>
                <li>
                  <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_CUSTOMERS; ?></a>
                  <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                  <ul>
                    <li>
                      <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_CUSTOMERS; ?></h2>
                    </li>
                    <ul>
                      <li>
                        <a href="modules.php?set=modules_create_account"><?php echo MENU_DESIGN_CREATE_ACCOUNT; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_create_account_pro"><?php echo MENU_DESIGN_CREATE_ACCOUNT_PRO; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_login"><?php echo MENU_DESIGN_LOGIN_CUSTOMERS; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_account_customers"><?php echo MENU_DESIGN_ACCOUNT_CUSTOMERS; ?></a>
                      </li>
                    </ul>
                  </ul>
                </li>
<?php
// ----------------------
// Menu Design Procedure commande
// ---------------------
?>
                <li>
                  <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_PAYMENT_PROCESS; ?></a>
                  <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                  <ul>
                    <li>
                      <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_PAYMENT_PROCESS; ?></h2>
                    </li>
                    <ul>
                      <li>
                        <a href="modules.php?set=modules_shopping_cart"><?php echo MENU_DESIGN_SHOPPING_CART; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_checkout_shipping"><?php echo MENU_DESIGN_SHIPPING; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_checkout_payment"><?php echo MENU_DESIGN_PAYMENT; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_checkout_confirmation"><?php echo MENU_DESIGN_CONFIRMATION; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_checkout_success"><?php echo MENU_DESIGN_SUCESS; ?></a>
                      </li>
                    </ul>
                  </ul>
                </li>
<?php
// ----------------------
// Menu Design communication
// ---------------------
?>
                <li>
                  <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_COMMUNICATION; ?></a>
                  <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                  <ul>
                    <li>
                      <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_COMMUNICATION; ?></h2>
                    </li>
                    <ul>
                      <li>
                        <a href="modules.php?set=modules_contact_us"><?php echo MENU_DESIGN_CONTACT_US; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_blog"><?php echo MENU_DESIGN_BLOG; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_blog_content"><?php echo MENU_DESIGN_BLOG_CONTENT; ?></a>
                      </li>
                      <li>
                        <a href="configuration.php?gID=32"><?php echo MENU_DESIGN_COMMENTS; ?></a>
                      </li>
                    </ul>
                  </ul>
                </li>
<?php
// ----------------------
// Menu Design divers
// ---------------------
?>
                <li>
                  <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_MISCELLANEOUS; ?></a>
                  <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                  <ul>
                    <li>
                      <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_MISCELLANEOUS; ?></h2>
                    </li>
                    <ul>
                      <li>
                        <a href="modules.php?set=modules_advanced_search"><?php echo MENU_DESIGN_ADVANCED_SEARCH; ?></a>
                      </li>
                      <li>
                        <a href="modules.php?set=modules_sitemap"><?php echo MENU_DESIGN_SITEMAP; ?></a>
                      </li>
                    </ul>
                  </ul>
                </li>
              </li>
<?php
// ----------------------
// Menu Design Description produits
// ---------------------
?>
              <ul>
                <li>
                  <a href="modules.php?set=modules_products_info"><?php echo MENU_DESIGN_PRODUCTS_INFO; ?></a>
                </li>
              </ul>
<?php
// ----------------------
// Menu Design Gabarits
// ---------------------
?>
              <li>
                <a href="#"><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_LAYOUT; ?></a>
                <h2><i class="fa fa-laptop"></i><?php echo BOX_HEADING_DESIGN; ?></h2>
                <ul>
                  <li>
                    <h2><i class="fa fa-laptop"></i><?php echo MENU_DESIGN_LAYOUT; ?></h2>
                  </li>
                  <ul>
                    <li>
                      <a href="modules.php?set=modules_boxes"><?php echo MENU_DESIGN_BOXES; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=modules_header"><?php echo MENU_DESIGN_HEADER; ?></a>
                    </li>
                    <li>
                      <a href="modules.php?set=modules_footer"><?php echo MENU_DESIGN_FOOTER; ?></a>
                    </li>
                  </ul>
                </ul>
              </li>
            </ul>
          </li>
<!------------------
    Menu 7 Outils
-------------------->
        <li>
          <a href="#"><i class="fa fa-wrench "></i><?php echo BOX_HEADING_TOOLS; ?></a>
          <h2><i class="fa fa-wrench "></i><?php echo BOX_HEADING_TOOLS; ?></h2>
          <ul>
            <h2><i class="fa fa-wrench "></i><?php echo BOX_HEADING_TOOLS; ?></h2>
<?php
// ----------------------
// Menu Sauvegarde base de donnees
// ---------------------
?>
            <li>
              <a href="#"><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_BACKUP; ?></a>
              <h2><i class="fa fa-wrench "></i><?php echo BOX_HEADING_TOOLS; ?></h2>
              <ul>
                <li>
                  <h2><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_BACKUP; ?></h2>
                </li>
                <ul>
                  <li>
                    <a href="backup.php"><?php echo MENU_TOOLS_BACKUP; ?></a>
                  </li>
                  <li>
                    <a href="database_tables.php"><?php echo MENU_TOOLS_DATABASE_TABLE; ?></a>
                  </li>
                </ul>
              </ul>
            </li>
<?php
// ----------------------
// Menu export des donnees
// ---------------------
?>
            <li>
              <a href="#"><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_EXPORT; ?></a>
              <h2><i class="fa fa-wrench "></i><?php echo BOX_HEADING_TOOLS; ?></h2>
              <ul>
                <li>
                  <h2><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_EXPORT; ?></h2>
                </li>
                <ul>
                  <li>
                    <a href="export_price_comparison.php"><?php echo MENU_TOOLS_EXPORT_COMPARATEUR; ?></a>
                  </li>
                  <li>
                    <a href="export_data.php"><?php echo MENU_TOOLS_EXPORT_DATAS; ?></a>
                  </li>
                  <li>
                    <a href="import.php"><?php echo MENU_TOOLS_IMPORT_DATAS; ?></a>
                  </li>
                </ul>
              </ul>
            </li>
<?php
// ----------------------
// Menu securite
// ---------------------
?>
            <li>
              <a href="#"><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_SECURITY; ?></a>
              <h2><i class="fa fa-wrench "></i><?php echo BOX_HEADING_TOOLS; ?></h2>
              <ul>
                <li>
                  <h2><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_SECURITY; ?></h2>
                </li>
                <ul>
                  <li>
                    <a href="server_info.php"><?php echo MENU_TOOLS_SECURITY_SERVER_INFO; ?></a>
                  </li>
                  <li>
                    <a href="security_checks.php"><?php echo MENU_TOOLS_SECURITY_CLICSHOPPING; ?></a>
                  </li>
                  <li>
                    <a href="action_recorder.php"><?php echo MENU_TOOLS_SECURITY_RECORDER; ?></a>
                  </li>
                </ul>
              </ul>
            </li>
<?php
  // ----------------------
  // Menu edit HTML
  // ---------------------
?>
            <li>
              <a href="#"><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_EDITOR_HTML; ?></a>
              <h2><i class="fa fa-wrench "></i><?php echo BOX_HEADING_EDITOR_HTML; ?></h2>
              <ul>
                <li>
                  <h2><i class="fa fa-wrench "></i><?php echo MENU_TOOLS_EDITOR_HTML; ?></h2>
                </li>
                <ul>
                  <li>
                    <a href="edit_css.php"><?php echo MENU_TOOLS_EDIT_CSS; ?></a>
                  </li>
                  <li>
                    <a href="edit_html.php"><?php echo MENU_TOOLS_EDIT_HTML; ?></a>
                  </li>
                  <li>
                    <a href="edit_template_products.php"><?php echo MENU_TOOLS_EDIT_TEMPLATE_PRODUCTS; ?></a>
                  </li>
                </ul>
              </ul>
            </li>
<?php
// ----------------------
// Menu tools divers
// ---------------------
?>
              <ul>
                <li>
                  <a href="modules_upload.php"><?php echo MENU_TOOLS_MODULE_INSTALLATION; ?></a>
                </li>
                <li>
                  <a href="whos_online.php"><?php echo MENU_TOOLS_WHOS_ONLINE; ?></a>
                </li>
              </ul>
            </ul>
          </li>
<!------------------
    Menu 8 Support
-------------------->
          <li>
            <a href="#"><i class="fa fa-comments"></i><?php echo BOX_HEADING_HELP; ?></a>
            <h2><i class="fa fa-comments"></i><?php echo BOX_HEADING_HELP; ?></h2>
            <ul>
              <li>
                <h2><i class="fa fa-comments"></i><?php echo BOX_HEADING_HELP; ?></h2>
              </li>
              <ul>
                <li>
                  <a href="http://www.clicshopping.net" target="_blank"><?php echo MENU_HELP_IMAGINIS; ?></a>
                </li>
                <li>
                  <a href="http://www.clicshopping.org" target="_blank"><?php echo MENU_MARKETPLACE; ?></a>
                </li>
                <li>
                  <a href="credit.php"><?php echo MENU_HELP_CREDIT; ?></a>
                </li>
              </ul>
            </ul>
          </li>
        </ul>
      </nav>
    </div>