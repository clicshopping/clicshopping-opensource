<?php
/**
 * newsletters.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

define('HEADING_TITLE', 'Newsletter Manager');

define('TABLE_HEADING_NEWSLETTERS', 'Newsletters');
define('TABLE_HEADING_SIZE', 'Size');
define('TABLE_HEADING_MODULE', 'Module');
define('TABLE_HEADING_SENT', 'Sent');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_LANGUAGE','Language');
define('TABLE_HEADING_B2B','Customer group');

define('TEXT_ALL_LANGUAGES','All languages');
define('TEXT_ALL_CUSTOMERS','Normal Customer'); 
define('TEXT_NEWSLETTER_CUSTOMERS_GROUP','Customers group');
define('TEXT_NEWSLETTER_LANGUAGE','Customer type referred');
define('TEXT_NEWSLETTER_CREATE_FILE_HTML','Do you want create a html file ? *');
define('TEXT_NEWSLETTER_TWITTER','Do you wish publish this newsletter on Twitter ? ');
define('TEXT_NEWSLETTER_CUSTOMER_NO_ACCOUNT','do you wish include the customer without no account ? ');

define('TEXT_YES','Yes');
define('TEXT_NO','No');

define('TEXT_NEWSLETTER_MODULE', 'Module:');
define('TEXT_NEWSLETTER_TITLE', 'Newsletter Title:');
define('TEXT_NEWSLETTER_CONTENT', 'Content:');

define('TEXT_NEWSLETTER_DATE_ADDED', 'Date Added:');
define('TEXT_NEWSLETTER_DATE_SENT', 'Date Sent:');

define('TEXT_INFO_DELETE_INTRO', 'Are you sure that you want to delete this newsletter?');

define('TEXT_PLEASE_WAIT', 'Please wait and do not interrupt this process !<br /><br />Sending emails...<br /><br />The treatment may be longer or shorter depending on the number of mail to deal');
define('TEXT_FINISHED_SENDING_EMAILS', 'Finished sending e-mails!');

define('ERROR_NEWSLETTER_TITLE', 'Error: Newsletter title required');
define('ERROR_NEWSLETTER_MODULE', 'Error: Newsletter module required');
define('ERROR_REMOVE_UNLOCKED_NEWSLETTER', 'Error: Please lock the newsletter before deleting it.');
define('ERROR_EDIT_UNLOCKED_NEWSLETTER', 'Error: Please lock the newsletter before editing it.');
define('ERROR_SEND_UNLOCKED_NEWSLETTER', 'Error: Please lock the newsletter before sending it.');

define('TITLE_HELP_DESCRIPTION', 'Note on the use of the wysiwyg');
define('HELP_DESCRIPTION', '<li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock more)</li>
<li>We advise you create a file only for normal customers. This file will be visible to all who have access to the directory boutique/pub.</li>
<li>The twitter publication is allowed only if you create a html file on the server.</li>
');
define('IMAGE_BUTTON_CONTINUE','Send');

?>
