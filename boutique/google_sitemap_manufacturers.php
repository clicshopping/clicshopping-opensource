<?php
/*
 * google_sitemap_manufacturers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');


    $manufacturer_array = array();

    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturers_id,
                                           coalesce(NULLIF(last_modified, :last_modified),
                                                           date_added) as last_modified
                                            from :table_manufacturers
                                            where manufacturers_status = :manufacturers_status
                                            order by last_modified DESC
                                            ');

    $Qmanufacturers->bindValue(':last_modified', '');
    $Qmanufacturers->bindValue(':manufacturers_status', '0');
    $Qmanufacturers->execute();

    while ($manufacturers = $Qmanufacturers->fetch() ) {
      $manufacturer_array[$manufacturers['manufacturers_id']]['loc'] = osc_href_link('index.php', 'manufacturers_id=' . (int)$manufacturers['manufacturers_id'], 'NONSSL', false);
      $manufacturer_array[$manufacturers['manufacturers_id']]['lastmod'] = $manufacturers['last_modified'];
      $manufacturer_array[$manufacturers['manufacturers_id']]['changefreq'] = 'weekly';
      $manufacturer_array[$manufacturers['manufacturers_id']]['priority'] = '0.5';
    }

    foreach ($manufacturer_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
