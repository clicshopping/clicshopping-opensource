<?php
/**
 * geo_zones.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Zones fiscales');

define('TABLE_HEADING_COUNTRY', 'Pays');
define('TABLE_HEADING_COUNTRY_ZONE', 'Zone');
define('TABLE_HEADING_TAX_ZONES', 'Zones fiscales');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_TAX_DESCRIPTION','Description');
define('TABLE_HEADING_NUMBER_ZONE','Nombre de zones');

define('TEXT_INFO_HEADING_NEW_ZONE', 'Nouvelle zone');
define('TEXT_INFO_NEW_ZONE_INTRO', 'Merci de compléter les informations sur la zone');

define('TEXT_INFO_HEADING_EDIT_ZONE', 'Editer zone');
define('TEXT_INFO_EDIT_ZONE_INTRO', 'Merci d\'effectuer les changements nécessaires');

define('TEXT_INFO_HEADING_DELETE_ZONE', 'Supprimer zone');
define('TEXT_INFO_DELETE_ZONE_INTRO', 'Etes vous sur de vouloir supprimer cette zone ?');

define('TEXT_INFO_HEADING_NEW_SUB_ZONE', 'Nouvelle sous-zone');
define('TEXT_INFO_NEW_SUB_ZONE_INTRO', 'Merci de compléter les informations sur la sous-zone');

define('TEXT_INFO_HEADING_EDIT_SUB_ZONE', 'Editer sous-zone');
define('TEXT_INFO_EDIT_SUB_ZONE_INTRO', 'Merci d\'effectuer les changements nécessaires');

define('TEXT_INFO_HEADING_DELETE_SUB_ZONE', 'Supprimer sous-zone');
define('TEXT_INFO_DELETE_SUB_ZONE_INTRO', 'Etes vous sur de vouloir supprimer cette sous-zone ?');

define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_INFO_ZONE_NAME', 'Nom de la zone :');
define('TEXT_INFO_NUMBER_ZONES', 'Nombre de zones :');
define('TEXT_INFO_ZONE_DESCRIPTION', 'Description :');
define('TEXT_INFO_COUNTRY', 'Pays :');
define('TEXT_INFO_COUNTRY_ZONE', 'Zone :');
define('TYPE_BELOW', 'Toutes les zones');
define('PLEASE_SELECT', 'Toutes les zones');
define('TEXT_ALL_COUNTRIES', 'Tous les pays');
?>