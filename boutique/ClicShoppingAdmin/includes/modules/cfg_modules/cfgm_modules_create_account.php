<?php
/*
 * cfgm_modules_create_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 1.0
*/

  class cfgm_modules_create_account {
    var $code = 'modules_create_account';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_CREATE_ACCOUNT_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_create_account() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_create_account/';
      $this->title = MODULE_CFG_MODULE_CREATE_ACCOUNT_MODULES_TITLE;
    }
  }
?>
