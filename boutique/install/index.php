<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  require('includes/application.php');

  $page_contents = 'step_1.php';
  
  if (isset($_GET['step']) && is_numeric($_GET['step'])) {
    switch ($_GET['step']) {
      case '2':
        $page_contents = 'step_2.php';
        break;
            
      case '3':
        $page_contents = 'step_3.php';
        break;    
        
      case '4':
        $page_contents = 'step_4.php';
        break;

      case '5':
        $page_contents = 'step_5.php';
        break;

      case '6':
        $page_contents = 'step_6.php';
        break;
    }
  }

  require('templates/main_page.php');
?>
