<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

// Set the level of error reporting
  error_reporting(E_ALL & ~E_NOTICE);

// set default timezone if none exists (PHP 5.3 throws an E_WARNING)
  if ((strlen(ini_get('date.timezone')) < 1) && function_exists('date_default_timezone_set')) {
    date_default_timezone_set(@date_default_timezone_get());
  }

  if (isset($_GET['language'])) {
    setcookie('Lor_Language', $_GET['language']);

    $language = $_GET['language'];
  } elseif (isset($_COOKIE['Lor_Language'])) {
    $language = $_COOKIE['Lor_Language'];
  } else {
    $language = 'english';
  }

  require('includes/functions/general.php');
  require('includes/functions/database.php');
  require('includes/functions/html_output.php');
