<?php
/**
 * action_recorder.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
  $directory_array = array();
  if ($dir = @dir(DIR_FS_CATALOG_MODULES . 'action_recorder/')) {
    while ($file = $dir->read()) {
      if (!is_dir(DIR_FS_CATALOG_MODULES . 'action_recorder/' . $file)) {
        if (substr($file, strrpos($file, '.')) == $file_extension) {
          $directory_array[] = $file;
        }
      }
    }
    sort($directory_array);
    $dir->close();
  }

  for ($i=0, $n=sizeof($directory_array); $i<$n; $i++) {
    $file = $directory_array[$i];

    if (file_exists(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/action_recorder/' . $file)) {
      include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/action_recorder/' . $file);
    }

    include(DIR_FS_CATALOG_MODULES . 'action_recorder/' . $file);

    $class = substr($file, 0, strrpos($file, '.'));
    if (class_exists($class)) {
      ${$class} = new $class;
    }
  }

  $modules_array = array();
  $modules_list_array = array(array('id' => '', 'text' => TEXT_ALL_MODULES));

  $QModules = $OSCOM_PDO->prepare('select distinct module
                                   from :table_action_recorder
                                   order by module
                                   ');
  $QModules->execute();

  while ($modules = $QModules->fetch() ) {
    $modules_array[] = $modules['module'];

    $modules_list_array[] = array('id' => $modules['module'],
                                  'text' => (is_object(${$modules['module']}) ? ${$modules['module']}->title : $modules['module']));
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'expire':
        $expired_entries = 0;

        if (isset($_GET['module']) && in_array($_GET['module'], $modules_array)) {
          if (is_object(${$_GET['module']})) {
            $expired_entries += ${$_GET['module']}->expireEntries();
          } else {
            $Qdelete = $OSCOM_PDO->prepare('delete from action_recorder
                                            where module = :module
                                          ');
            $Qdelete->bindValue(':module',  osc_db_input($_GET['module']));
            $Qdelete->execute();

            $expired_entries += osc_db_affected_rows();
          }
        } else {
          foreach ($modules_array as $module) {
            if (is_object(${$module})) {
              $expired_entries += ${$module}->expireEntries();
            }
          }
        }

        $OSCOM_MessageStack->add_session(sprintf(SUCCESS_EXPIRED_ENTRIES, $expired_entries), 'success');

        osc_redirect(osc_href_link('action_recorder.php'));

        break;
    }
  }
  require('includes/header.php');
?>

  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
             <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/modules_action_recorder.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <span class="col-sm-6 pull-right">
<?php
  echo osc_draw_form('search', 'action_recorder.php', '', 'get');
  echo osc_draw_input_field('search','','id="search" placeholder="'.TEXT_FILTER_SEARCH.'"');
  echo osc_draw_hidden_field('module') . osc_hide_session_id() . '</form>';
?>
                  </span>
                </td>
              </tr>
              <tr>
                <td class="smallText" align="right">
<?php
  echo osc_draw_form('filter', 'action_recorder.php', '', 'get');
  echo osc_draw_pull_down_menu('module', $modules_list_array, null, 'onchange="this.form.submit();"');
  echo osc_draw_hidden_field('search') . osc_hide_session_id() . '</form>';
?>
                </td>
              </tr>
            </table></td>
            <td class="pageHeading" align="right"><?php echo '<a href="' . osc_href_link('action_recorder.php', 'action=expire' . (isset($_GET['module']) && in_array($_GET['module'], $modules_array) ? '&module=' . $_GET['module'] : '')) . '">' . osc_image_button('button_delete_big.gif', IMAGE_DELETE) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" width="20">&nbsp;</td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MODULE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_INFO_IDENTIFIER; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_DATE_ADDED; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $filter = array();

  if (isset($_GET['module']) && in_array($_GET['module'], $modules_array)) {
    $filter[] = " module = '" . $_GET['module'] . "' ";
  }

  if (isset($_GET['search']) && !empty($_GET['search'])) {
    $filter[] = " identifier like '%" . $_GET['search'] . "%' ";
  }

  $actions_query_raw = "select * 
                        from action_recorder " . (!empty($filter) ? "
                        where " . implode(" and ", $filter) : "") . " 
                        order by date_added desc";

  $actions_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $actions_query_raw, $actions_query_numrows);

  $actions_query = osc_db_query($actions_query_raw);

  while ($actions = osc_db_fetch_array($actions_query)) {
    $module = $actions['module'];

    $module_title = $actions['module'];
    if (is_object(${$module})) {
      $module_title = ${$module}->title;
    }

    if ((!isset($_GET['aID']) || (isset($_GET['aID']) && ($_GET['aID'] == $actions['id']))) && !isset($aInfo)) {

      $QactionsExtra = $OSCOM_PDO->prepare("select identifier
                                            from action_recorder
                                            where id = :id
                                           ");

      $QactionsExtra->bindInt(':id', (int)$actions['id']);
      $QactionsExtra->execute();

      $actions_extra = $QactionsExtra->fetch();

      $aInfo_array = array_merge($actions, $actions_extra, array('module' => $module_title));
      $aInfo = new objectInfo($aInfo_array);
    }

    if ( (isset($aInfo) && is_object($aInfo)) && ($actions['id'] == $aInfo->id) ) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    } else {
//      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('action_recorder.php', osc_get_all_get_params(array('aID')) . 'aID=' . $actions['id']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_IMAGES . 'icons/' . (($actions['success'] == '1') ? 'tick.gif' : 'cross.gif')); ?></td>
                <td class="dataTableContent"><?php echo $module_title; ?></td>
                <td class="dataTableContent"><?php echo osc_output_string_protected($actions['user_name']) . ' [' . (int)$actions['user_id'] . ']'; ?></td>
               <td class="dataTableContent">
<?php 
  if (!empty($aInfo->identifier)) {
    echo		 '<a href="' . osc_href_link('action_recorder.php', 'search=' . $aInfo->identifier) . '"><u>' . osc_output_string_protected($aInfo->identifier) . '</u></a>';
  } else {
    echo  'empty';
  }
?>
                <td class="dataTableContent" align="right"><?php echo $OSCOM_Date->getShortTime($actions['date_added']); ?></td>
                <td class="dataTableContent" align="right"><?php if ( (isset($aInfo) && is_object($aInfo)) && ($actions['id'] == $aInfo->id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('action_recorder.php', osc_get_all_get_params(array('aID')) . 'aID=' . $actions['id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
            </tr>
<?php
  }
?>
              <tr>
                <td colspan="6"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $actions_split->display_count($actions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                    <td class="smallText"  align="right"><?php echo $actions_split->display_links($actions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], (isset($_GET['module']) && in_array($_GET['module'], $modules_array) && is_object(${$_GET['module']}) ? 'module=' . $_GET['module'] : null) . (isset($_GET['search']) && !empty($_GET['search']) ? '&search=' . $_GET['search'] : null)); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    default:
    break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>