<?php
/*
 * stats_products_validation.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

define('HEADING_TITLE', 'Analyse de la validité des domaines des emails');

define('TABLE_HEADING_CUSTOMERS_ID', 'Id clients');
define('TABLE_HEADING_FIRST_NAME', 'Prénom');
define('TABLE_HEADING_LAST_NAME', 'Nom');
define('TABLE_HEADING_ADDRESS_EMAIL', 'Email');
define('TABLE_HEADING_VALIDATE_EMAIL', 'Analyse');
define('TABLE_HEADING_CLEAR', 'Supprimer');
define('TEXT_SUCCESS_DOMAIN','domaine de l\'email valide');
define('TEXT_NO_SUCCESS_DOMAIN','<font color="#FF0000"><strong>domaine de l\'email non valide<strong></font>');
define('IMAGE_RESET_EMAIL','Remettre à zèro les emails');
define('IMAGE_ANALYSE','Analyser les emails des clients');
define('TITLE_AIDE_EMAIL','<strong>Note sur l\'analyse:</strong>');
define('TITLE_TEXT_AIDE_EMAIL','<li>L\'analyse porte uniquement sur le domaine de l\'email (pour le moment).</li>
<li>Plus vous avez d\'email, plus le process sera long dans l\'analyse.</li>
<li>Si le domaine de l\'email est non valide, il sera automatiquement retiré de l\'envoi des emails ou de la newsletter.</li>
<li>Si vous avez de nombreux contacts, veuillez ne pas réitérer l\'analyse de trop nombreuses fois (1 fois par mois).</li>
<li>Les emails non valide ne seront pas ré-analyser une seconde fois sauf si vous réinitialisez l\'analyse.</li>
<li>Le résultat de l\'analyse est affiché dans le listing de la fiche des clients.</li>
<li>Vous pouvez annuler le process à tout moment.</li>
');
?>