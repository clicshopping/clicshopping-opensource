<?php
/**
 * french.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

// look in your $PATH_LOCALE/locale directory for available locales..
// on RedHat6.0 I used 'en_US'
// on FreeBSD 4.0 I use 'en_US.ISO_8859-1'
// this may not work under win32 environments..
//setlocale(LC_TIME, 'fr_FR.ISO_8859-1');
setlocale(LC_TIME, 'fr_FR.utf8');
define('DATE_FORMAT_SHORT', '%d/%m/%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
define('DATE_FORMAT', 'd/m/Y'); // this is used for date()
define('PHP_DATE_TIME_FORMAT', 'd/m/Y H:i:s'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('JQUERY_DATEPICKER_I18N_CODE', 'fr');
define('JQUERY_DATEPICKER_FORMAT', 'dd/mm/yy');
////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function osc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

// Global entries for the <html> tag
define('HTML_PARAMS','dir="ltr" lang="fr"');

// charset for web pages and emails
define('CHARSET', 'UTF-8');

// page title
define('TITLE', 'Administration '.STORE_NAME.'');

// header text in includes/header.php
define('HEADER_TITLE_TOP', 'Administration');
define('HEADER_TITLE_SUPPORT_SITE', ' ');
define('HEADER_TITLE_ONLINE_CATALOG', 'Catalogue en ligne');
define('HEADER_TITLE_ADMINISTRATION', 'Administration');
define('HEADER_TITLE_LOGOFF', 'Quitter');

// text for gender
define('MALE', 'Mr.');
define('FEMALE', 'Mme.');

// text for date of birth example
define('DOB_FORMAT_STRING', 'jj/mm/aaaa');

// menu de la page d'accueil
define('BOX_CONFIGURATION_MYSTORE', 'Ma Boutique');
define('BOX_CONFIGURATION_LOGGING', 'Se connecter');
define('BOX_CONFIGURATION_CACHE', 'Cache');
define('BOX_MODULES_ADMIN_DASHBOARD', 'Tableau de bord');
define('BOX_HEADING_MODULES', 'Modules');
define('BOX_MODULES_ACTION_RECORDER', 'Enregistrement des actions');
define('BOX_MODULES_HEADER_TAGS', 'Référencement');
define('BOX_MODULES_SOCIAL_BOOKMARKS', 'Réseaux sociaux');


define('BOX_MODULES_PAYMENT', 'Paiement');
define('BOX_MODULES_SHIPPING', 'Expédition');
define('BOX_MODULES_ORDER_TOTAL', 'Montant Total');
define('BOX_CATALOG_MANUFACTURERS', 'Marques');
define('BOX_CUSTOMERS_CUSTOMERS', 'Clients');
define('BOX_CUSTOMERS_ORDERS', 'Commandes');
define('BOX_TAXES_COUNTRIES', 'Pays');
define('BOX_TAXES_GEO_ZONES', 'Zones fiscales');
define('BOX_LOCALIZATION_CURRENCIES', 'Devises');
define('BOX_LOCALIZATION_LANGUAGES', 'Langues');

// menu accueil et support
define('MENU_HOME', 'Accueil');
define('MENU_HOME_STORE', 'Boutique');
define('MENU_HOME_ADMIN', 'Administration');
define('MENU_HELP', 'Support');
define('MENU_HELP_CREDIT','Crédit');

// menu configuration
define('BOX_HEADING_CONFIGURATION', 'Configuration');
define('MENU_CONFIGURATION_GENERAL', 'Configuration générale');
define('MENU_CONFIGURATION_ADMINISTRATOR','Membres administration');
define('MENU_CONFIGURATION_MAXIMUM', 'Gestion des valeurs maximum - minimum');
define('MENU_CONFIGURATION_LEGACY','Législation réglementaire');
define('MENU_CONFIGURATION_STORE', 'Ma boutique');
define('MENU_CONFIGURATION_MINIMUM', 'Gestion des valeurs minimales');
define('MENU_CONFIGURATION_BANK', 'Gestion des cartes de crédit');
define('MENU_CONFIGURATION_IMAGES', 'Gestion des images');
define('MENU_CONFIGURATION_CUSTOMERS', 'Détails clients');
define('MENU_CONFIGURATION_SHIPPING', 'Expéditions & Emballages');
define('MENU_CONFIGURATION_INVOICE', 'Facturation & Bons expédition');
define('MENU_CONFIGURATION_LISTING_PRODUCTS', 'Liste des produits');
define('MENU_CONFIGURATION_STOCK', 'Gestion du stock');
define('MENU_CONFIGURATION_MAIL', 'Gestion des emails');
define('MENU_CONFIGURATION_DOWNLOAD', 'Téléchargement');
define('MENU_CONFIGURATION_REFERENCEMENT_META_DATA','Gestion des Metas données');
define('MENU_CONFIGURATION_MODULES', 'Modules');
define('MENU_CONFIGURATION_MODULES_ACTION_RECORDER','Securité');
define('MENU_CONFIGURATION_MODULES_HOOKS','Hooks');
define('MENU_CONFIGURATION_MODULES_SOCIAL_BOOKMARKS','Activation Réseaux sociaux');
define('MENU_CONFIGURATION_MODULES_HEADER_TAG','Activation  Metas Données');
define('MENU_CONFIGURATION_MODULES_PAYMENT', 'Paiements');
define('MENU_CONFIGURATION_MODULES_SHIPPING', 'Livraisons');
define('MENU_CONFIGURATION_MODULES_ORDERTOTAL', 'Total commande');
define('MENU_CONFIGURATION_B2C', 'Gestion clients B2C');
define('MENU_CONFIGURATION_B2C_MINIMUM', 'Valeurs minimum');
define('MENU_CONFIGURATION_B2B', 'Gestion clients B2B');
define('MENU_CONFIGURATION_QUICK_UPDATES','Mise à Jour Rapide');
define('MENU_CONFIGURATION_B2C_GESTION', 'Configuration');
define('MENU_CONFIGURATION_B2B_GESTION', 'Configuration');
define('MENU_CONFIGURATION_B2B_CUSTOMERS', 'Détails clients');
define('MENU_CONFIGURATION_B2B_MINIMUM', 'Valeurs minimum');
define('MENU_CONFIGURATION_B2B_MAXIMUM', 'Valeurs maximum');
define('MENU_CONFIGURATION_SESSION', 'Session & Cache');
define('MENU_CONFIGURATION_SESSION_CACHE', 'Cache');
define('MENU_CONFIGURATION_SESSION_CONTROLE_CACHE', 'Contr&ocirc;le du cache');
define('MENU_CONFIGURATION_SESSION_GZIP', 'Compression GZip');
define('MENU_CONFIGURATION_SESSION_LOGGING', 'Logging');
define('MENU_CONFIGURATION_SESSION_SESSION', 'Session');
define('MENU_CONFIGURATION_SEO','SEO gestion des URL du site');
define('MENU_CONFIGURATION_PRODUCTS_INFO','Affichage description des produits');
define('MENU_CONFIGURATION_EXPORT','Export / import des données');
define('MENU_CONFIGURATION_SOCIAL_WEB','Configuration du web social');
define('MENU_CONFIGURATION_PRODUCTS_QUANTITY_UNIT','Gestion des types de quantité');
define('MENU_CONFIGURATION_WEBSERVICE','Web Service');



// Menu Référencement et Statistiques
define('MENU_CONFIGURATION_REFERENCEMENT_STATS', 'Référencement / Metas données');

// menu Configuration BackOffice
define('MENU_CONFIGURATION_STORE_ADMINISTRATION','Mon administration');
define('MENU_PRINT_ORDERS_STATUS_INVOICE', 'Statut impression commandes/factures PDF');
define('MENU_EDIT_ORDERS_STATUS_INVOICE', 'Type édition commandes/factures PDF');
define('MENU_CONFIGURATION_ORDERS_STATUS_TRACKING','Gestion du tracking pour les expéditions');


// menu catalogue
define('BOX_HEADING_CATALOG', 'Catalogue');
define('MENU_CATALOG_CATEGORIES_PRODUCTS', 'Catégories/Produits');
define('MENU_CATALOG_PRICE_UPDATE', 'Mise à jour rapide');
define('MENU_CATALOG_PRODUCTS_ATTRIBUTES', 'Gestion des options produits');
define('MENU_CATALOG_MANUFACTURERS', 'Gestion des marques');
define('MENU_CATALOG_PRODUCTS_EXTRA_FIELDS', 'Gestion des champs supplémentaires');
define('MENU_CATALOG_SUPPLIERS', 'Gestion des fournisseurs achats');
define('MENU_CATALOG_PRODUCTS_ARCHIVES','Gestion des archives des produits');

// menu clients
define('BOX_HEADING_CUSTOMERS', 'Clients');
define('MENU_CUSTOMERS_ORDERS', 'Commandes');
define('MENU_CUSTOMERS', 'Clients');
define('MENU_CUSTOMERS_MEMBERS_B2B', 'Clients à valider');
define('MENU_CUSTOMERS_GROUP_B2B', 'Groupe clients');
define('MENU_CUSTOMERS_MAIL', 'Envoyer un e-mail');
define('MENU_CATALOG_REVIEWS', 'Commentaires des clients');
define('MENU_CONTACT_CUSTOMERS','Contact service clients');

// menu marketing
define('BOX_HEADING_MARKETING','Marketing');
define('MENU_CATALOG_SPECIALS', 'Promotions des produits');
define('MENU_MARKETING_PRODUCTS_HEART','Coups de coeur');
define('MENU_MARKETING_PRODUCTS_FEATURED','Produits sélectionnés');
define('MENU_MARKETING_PRODUCTS_RELATED','Produits croisés & similaires');
define('MENU_MARKETING_BANNER_MANAGER', 'Gestion des bannières');
define('MENU_MARKETING_DISCOUNT_COUPONS','Coupons / Réductions');
define('MENU_MARKETING_META_DATA','Gestion Métadonnées');
define('MENU_REFERENCEMENT','Cyber marketing');

// menu Communication
define('BOX_HEADING_COMMUNICATION','Communication');
define('MENU_COMMUNICATION_NEWSLETTER', 'Newsletters');
define('MENU_COMMUNICATION_PAGE_INFORMATION','Page accueil & Informations');
define('MENU_COMMUNICATION_BLOG','Blog');

// menu design

define('BOX_HEADING_DESIGN','Design');

define('MENU_DESIGN_ADVANCED_SEARCH','Recherche avançée');
define('MENU_DESIGN_ACCOUNT_CUSTOMERS','Information clients');
define('MENU_CONFIGURATION_DESIGN','Configuration générale design');
define('MENU_DESIGN_BOXES','Boxes gauche et droite');
define('MENU_DESIGN_BLOG','Blog');
define('MENU_DESIGN_BLOG_CONTENT','Contenu du Blog');
define('MENU_DESIGN_CATEGORIES','Catégories');
define('MENU_DESIGN_COMMENTS','Commentaires');
define('MENU_DESIGN_COMMUNICATION','Communication');
define('MENU_DESIGN_CONFIRMATION','Confirmation de commande');
define('MENU_DESIGN_CREATE_ACCOUNT','Création de compte client (B2C)');
define('MENU_DESIGN_CREATE_ACCOUNT_PRO','Création de compte client professionnel (B2B)');
define('MENU_DESIGN_CUSTOMERS','Compte Clients');
define('MENU_DESIGN_CONTACT_US','Nous contacter');
define('MENU_DESIGN_FOOTER','Pied de page du catalogue');
define('MENU_DESIGN_HEADER','Entête du catalogue');
define('MENU_DESIGN_INDEX','Accueil / Catégories');
define('MENU_DESIGN_LAYOUT','Gabarits');
define('MENU_DESIGN_LOGIN_CUSTOMERS','Connexion clients');
define('MENU_DESIGN_MISCELLANEOUS','Divers');
define('MENU_DESIGN_PRODUCTS_FEATURED', 'Listing des sélections');
define('MENU_DESIGN_PRODUCTS_HEART','Listing des coups de coeur');
define('MENU_DESIGN_PAYMENT','Choix paiement');
define('MENU_DESIGN_PAYMENT_PROCESS','Procédure des commandes');
define('MENU_DESIGN_PRODUCTS_INFO', 'Fiche description des produits');
define('MENU_DESIGN_PRODUCTS_INFO_PRODUCTS_RELATED','Produits similaires');
define('MENU_DESIGN_PRODUCTS_INFO_CROSS_SELL','Produits croisés');
define('MENU_DESIGN_PRODUCTS_INFO_ALSO_PURCHASED','Ont également acheté');
define('MENU_DESIGN_LISTING','Listing');
define('MENU_DESIGN_PRODUCTS_LISTING_SORT_ORDER','Affichage des ordres de tri');
define('MENU_DESIGN_NEW','Accueil');
define('MENU_DESIGN_NEW_PRODUCTS', 'Accueil Nouveautés');
define('MENU_DESIGN_PRODUCTS_SPECIAL', 'Listing des Promotions');
define('MENU_DESIGN_PRODUCTS_LISTING', 'Listing des produits');
define('MENU_DESIGN_PRODUCTS_NEW', 'Listing des Nouveautés');
define('MENU_DESIGN_SPECIAL','Accueil promotions');
define('MENU_DESIGN_SHOPPING_CART','Panier des commandes');
define('MENU_DESIGN_SHIPPING','Choix expédition');
define('MENU_DESIGN_SITEMAP','Cartographie du site');
define('MENU_DESIGN_SUCESS','Succès de commande');

// menu lieu & taxes
define('BOX_HEADING_LOCATION_AND_TAXES', 'Lieux & Taxes');
define('MENU_TAXES_COUNTRIES', 'Pays');
define('MENU_TAXES_ZONES', 'Départements ou Etats');
define('MENU_TAXES_GEO_ZONE', 'Zones fiscales');
define('MENU_TAXES_TAX_CLASSES', 'Classes fiscales');
define('MENU_TAXES_TAX_RATES', 'Taux fiscaux');

// menu localisation
define('BOX_HEADING_LOCALIZATION', 'Divers');
define('MENU_LOCALIZATION_CURRENCIES', 'Devises');
define('MENU_LOCALIZATION_LANGUAGES', 'Langues');
define('MENU_LOCALIZATION_ORDERS_STATUS', 'Statut des commandes');


// menu rapports
define('BOX_HEADING_REPORTS', 'Rapports');
define('MENU_REPORTS_EMAIL_VALIDATION','Analyse des emails');
define('MENU_REPORTS_FINANCIAL','Rapports financiers');
define('MENU_REPORTS_MARGIN_REPORT','Rapport de marges');
define('MENU_REPORTS_PRODUCTS_NO_VIEWED', 'Produits non consultés');
define('MENU_REPORTS_PRODUCTS_VIEWED', 'Meilleures consultations');
define('MENU_REPORTS_PRODUCTS_NO_PURCHASED', 'Produits non vendus');
define('MENU_REPORTS_PRODUCTS_PURCHASED', 'Meilleures ventes');
define('MENU_REPORTS_ORDERS_TOTAL', 'Meilleurs Paniers');
define('MENU_REPORTS_PRODUCTS_EXPECTED', 'Produits en arrivage');
define('MENU_REPORTS_PRODUCTS_LOW_STOCK', 'Produits hors stock');
define('MENU_REPORTS_PRODUCTS_NOTIFICATIONS','Surveillance des produits');
define('MENU_REPORTS_SUPPLIERS','Commandes aux Fournisseurs');
define('MENU_REPORTS_DISCOUNT','Rapports coupons');
define('MENU_REPORTS_NEWSLETTER_NO_ACCOUNT','Souscription Newsletter sans compte');
define('MENU_REPORTS_STATS','Rapports statistiques produits');
define('MENU_REPORTS_STATS_FINANCIAL_MANAGEMENT','Rapport gestion financière');
define('MENU_REPORTS_STATS_PRODUCTS_MANAGEMENT','Rapport gestion produits');
define('MENU_REPORTS_PRODUCTS_WHAREHOUSE','Entrepot des produits');

// Menu Market Place
define('MENU_MARKETPLACE','Market Place');

// menu outils
define('BOX_HEADING_TOOLS', 'Outils');
define('BOX_HEADING_EDITOR_HTML', 'Editeur HTML');

define('MENU_TOOLS_BACKUP', 'Sauvegarder la base de données');
define('MENU_TOOLS_DATABASE_TABLE','Analyse base de données');
define('MENU_TOOLS_DEFINE_LANGUAGE', 'Editer les fichiers langues');
define('MENU_TOOLS_EDITOR_HTML','Editeur HTML');
define('MENU_TOOLS_EDIT_CSS','Editeur CSS');
define('MENU_TOOLS_EDIT_HTML','Editeur html Contenu');
define('MENU_TOOLS_EDIT_TEMPLATE_PRODUCTS','Editeur HTMl template produits');
define('MENU_TOOLS_EXPORT','Exportation des données');
define('MENU_TOOLS_EXPORT_COMPARATEUR','Comparateurs de prix');
define('MENU_TOOLS_EXPORT_DATAS','Export des données');
define('MENU_TOOLS_FILE_MANAGER', 'Gestionnaire de fichiers');
define('MENU_TOOLS_IMPORT_DATAS','Import des données');
define('MENU_TOOLS_MODULE_INSTALLATION','Installateur de modules');
define('MENU_TOOLS_SEC_DIR_PERMISSIONS','Permissions de sécurité des répertoires');
define('MENU_TOOLS_SECURITY', 'Informations Sécurité');
define('MENU_TOOLS_SECURITY_SERVER_INFO','information Serveur');
define('MENU_TOOLS_SECURITY_CLICSHOPPING','Information ClicShopping');
define('MENU_TOOLS_SECURITY_RECORDER', 'Surveillance des actions');
define('MENU_TOOLS_WHOS_ONLINE', 'Clients en ligne');
define('MENU_TOOLS_TEMPLATE_EMAIL','Template email');

//menu Support
define('MENU_HELP_IMAGINIS','Aide ClicShopping');
define('BOX_HEADING_HELP','Support');
define('MENU_EMAIL_ACCESS','Mes emails');

// menu groupe client
define('BOX_CATALOG_CATEGORIES_PRICEUPDATE', 'Mise à jour rapide');
define('BOX_CUSTOMERS_APPROVAL', 'Clients en attente');
define('BOX_CUSTOMERS_GROUPS', 'Groupes clients');

// javascript messages
define('JS_ERROR', 'Des erreurs sont survenues durant le traitement de votre formulaire !\nMerci de faire les corrections suivantes :\n\n');

define('JS_OPTIONS_VALUE_PRICE', '* Le nouvel attribut produit nécessite un prix\n');
define('JS_OPTIONS_VALUE_PRICE_PREFIX', '* Le nouvel attribut produit nécessite un préfixe de prix\n');

define('JS_PRODUCTS_NAME', '*  Le nouveau produit nécessite un nom\n');
define('JS_PRODUCTS_DESCRIPTION', '* Le nouveau produit nécessite une description\n');
define('JS_PRODUCTS_PRICE', '* Le nouveau produit nécessite un prix\n');
define('JS_PRODUCTS_WEIGHT', '* Le nouveau produit nécessite un poids\n');
define('JS_PRODUCTS_QUANTITY', '* Le nouveau produit nécessite une quantité\n');
define('JS_PRODUCTS_MODEL', '* Le nouveau produit nécessite un modèle\n');
define('JS_PRODUCTS_IMAGE', '* Le nouveau produit nécessite une image\n');

define('JS_SPECIALS_PRODUCTS_PRICE', '* Un nouveau prix pour ce produit doit être fixé\n');

define('JS_GENDER', '* Le champ \' GENRE \' ne peut être vide.\n');
define('JS_FIRST_NAME', '* Le champ \' PRENOM \' doit avoir au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caractères.\n');
define('JS_LAST_NAME', '* Le champ \' NOM \' doit avoir au moins ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caractères.\n');
define('JS_DOB', '* Le champ \' DATE DE NAISSANCE \' doit être au format xx/xx/xxxx (Jour/Mois/Année).\n');
define('JS_EMAIL_ADDRESS', '* Le champ \'ADRESSE E-MAIL\' n\'est pas correcte.\n');
define('JS_ADDRESS', '* Le champ \'ADRESSE POSTALE\' doit avoir au moins ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caractères.\n');
define('JS_POST_CODE', '* Le champ \'CODE POSTAL\' doit avoir au moins ' . ENTRY_POSTCODE_MIN_LENGTH . ' caractères.\n');
define('JS_CITY', '* Le champ \'VILLE\' doit avoir au moins ' . ENTRY_CITY_MIN_LENGTH . ' caractères.\n');
define('JS_STATE', '* Le champ \'DEPARTEMENT\' doit être précisé.\n');
define('JS_STATE_SELECT', '-- Sélectionnez --');
define('JS_ZONE', '* L\'entrée \'Etat\' doit être choisie parmi la liste pour ce pays.');
define('JS_COUNTRY', '* Le champ \'PAYS\'doit être précisé.');
define('JS_TELEPHONE', '* Le champ \'NUMERO DE TELEPHONE\' doit avoir au moins ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caractères.\n');
define('JS_CELLULAR_PHONE', '* Le champ \'NUMERO DE CELLULAIRE\' doit avoir au moins ' . ENTRY_CELLULAR_PHONE_MIN_LENGTH . ' caractères.\n');

define('JS_PASSWORD', '* Le champ \'MOT DE PASSE\' et \'Confirmation\' doivent avoir au moins ' . ENTRY_PASSWORD_MIN_LENGTH . ' caractères.\n');

define('JS_ORDER_DOES_NOT_EXIST', 'Le numéro de commande %s n\'existe pas !');

define('CATEGORY_PERSONAL', 'Données personnelles');
define('CATEGORY_ADDRESS', 'Adresse');
define('CATEGORY_ADDRESS_DEFAULT', 'Adresse principale');
define('CATEGORY_CONTACT', 'Contact téléphoniques');
define('CATEGORY_COMPANY', 'Détail sur la société');
define('CATEGORY_OPTIONS', 'Options');
define('CATEGORY_NEWSLETTER', 'Inscription aux newsletters');
define('CATEGORY_GROUP_CUSTOMER', 'Type de client');
define('CATEGORY_ORDER_TAXE', 'Mode de facturation');
define('CATEGORY_ORDER_TAXE_GROUP', 'Mode de facturation autorisé sur le groupe');
define('CATEGORY_SHIPPING_CUSTOMER', 'Mode de livraison autorisé');
define('CATEGORY_SHIPPING_CUSTOMER_GROUP', 'Mode de livraison autorisé sur le groupe');
define('CATEGORY_ORDER_CUSTOMER', 'Mode de paiement autorisé');
define('CATEGORY_ORDER_CUSTOMER_GROUP', 'Mode de paiement autorisé sur le groupe');

define('ENTRY_GENDER', 'Civilité :');
define('ENTRY_GENDER_ERROR', '&nbsp;<span class="errorText">requis</span>');
define('ENTRY_FIRST_NAME', 'Prénom :');
define('ENTRY_FIRST_NAME_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caract.</span>');
define('ENTRY_LAST_NAME', 'Nom :');
define('ENTRY_LAST_NAME_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caract.</span>');
define('ENTRY_DATE_OF_BIRTH', 'Date de naissance :');
define('ENTRY_DATE_OF_BIRTH_ERROR', '&nbsp;<span class="errorText">Format : JJ/MM/AAAA</span>');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail :');
define('ENTRY_EMAIL_ADDRESS_ERROR', '&nbsp;<span class="errorText">Votre adresse email n\'est pas correcte </span>');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', '&nbsp;<span class="errorText">L\'adresse électronique ne semble pas être valide!</span>');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', '&nbsp;<span class="errorText">Cette adresse électronique existe déjà!</span>');
define('ENTRY_COMPANY', 'Nom de la société :');
define('ENTRY_COMPANY_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_COMPANY_MIN_LENGTH . ' caract.</span>');
define('ENTRY_STREET_ADDRESS', 'Adresse :');
define('ENTRY_STREET_ADDRESS_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caract.</span>');
define('ENTRY_SUBURB', 'Complément d\'adresse :');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_POST_CODE', 'Code postal :');
define('ENTRY_POST_CODE_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_POSTCODE_MIN_LENGTH . ' caract.</span>');
define('ENTRY_CITY', 'Ville :');
define('ENTRY_CITY_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_CITY_MIN_LENGTH . ' caract.</span>');
define('ENTRY_STATE', 'Département ou Etat :');
define('ENTRY_STATE_ERROR', '&nbsp;<span class="errorText">requis</span>');
define('ENTRY_COUNTRY', 'Pays :');
define('ENTRY_COUNTRY_ERROR', '');
define('ENTRY_TELEPHONE_NUMBER', 'Numéro de téléphone:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', '&nbsp;<span class="errorText">min. ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caract.</span>');

define('ENTRY_CELLULAR_PHONE_NUMBER', 'Numéro de téléphone cellulaire:');
define('ENTRY_CELLULAR_PHONE_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER', 'Numéro de fax:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_TELEPHONE','Téléphone');

define('ENTRY_NEWSLETTER', 'Bulletin d\'informations :');
define('ENTRY_NEWSLETTER_YES', 'Abonné');
define('ENTRY_NEWSLETTER_NO', 'Non abonné');
define('ENTRY_NEWSLETTER_ERROR', '');
define('ENTRY_NEWSLETTER_LANGUAGE', 'Langue de correspondance ');

// TVA, Siret et Code APE
define('ENTRY_ADDRESS_NUMBER','Adresse No');
define('ENTRY_ADRESS_DEFAULT','(Adresse principale)');
define('ENTRY_SIRET', 'Numéro d\'immatriculation :');
define('ENTRY_SIRET_EXEMPLE', '(ex : No immatricultion de la société)');
define('ENTRY_APE', 'Numéro de nomenclature (ou autre) :');
define('ENTRY_APE_EXEMPLE', '(ex : Autre numeacute;ro immatriculation (ex code APE)');
define('ENTRY_TVA', 'Numéro de TVA Intracom (Europe) :');
define('ENTRY_TVA_ISO_ERROR', '&nbsp;<span class="errorText">Le code pays de deux lettres ne semble pas être correct.</span>');

// Groupe Client
define('ENTRY_CUSTOMERS_GROUP_NAME', 'Groupe client :');
define('ENTRY_GROUPS_NAME', 'Nom :');

// images
define('IMAGE_ANI_SEND_EMAIL', 'Envoyer un courrier électronique');
define('IMAGE_ARCHIVE','Archiver');
define('IMAGE_ARCHIVE_TO','Archiver');
define('IMAGE_BACK', 'Retour');
define('IMAGE_BACKUP', 'Sauvegarde');
define('IMAGE_BATCH_PRINT_ORDER','Impressions en masse');
define('IMAGE_CANCEL', 'Annuler');
define('IMAGE_CONFIRM', 'Confirmer');
define('IMAGE_COPY', 'Copier');
define('IMAGE_COPY_TO', 'Copier vers');
define('IMAGE_DETAILS', 'Détails');
define('IMAGE_DELETE', 'Supprimer');
define('IMAGE_EDIT', 'Editer');
define('IMAGE_EMAIL', 'Courrier électronique');
define('IMAGE_FILE_MANAGER', 'Gestionnaire de fichiers');
define('IMAGE_HELP','Aide');
define('ICON_EDIT_CUSTOMER','Editer le client');
define('ICON_EDIT_ORDER','Editer la commande');
define('IMAGE_ICON_STATUS_GREEN', 'Actif');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Activer');
define('IMAGE_ICON_STATUS_RED', 'Inactif');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'Désactiver');
define('IMAGE_ICON_INFO', 'Activer');
define('IMAGE_INSERT', 'Insérer');
define('IMAGE_LOCK', 'Verouilller');
define('IMAGE_MODULE_INSTALL', 'Installer le module');
define('IMAGE_MODULE_REMOVE', 'Supprimer le module');
define('IMAGE_MOVE', 'Déplacer');
define('IMAGE_NEW_BANNER', 'Nouvelle bannière');
define('IMAGE_NEW_BLOG','Nouvel Article');
define('IMAGE_NEW_CATEGORY', 'Nouvelle catégorie');
define('IMAGE_NEW_COUNTRY', 'Nouveau pays');
define('IMAGE_NEW_CURRENCY', 'Nouvelle devise');
define('IMAGE_NEW_FILE', 'Nouveau fichier');
define('IMAGE_NEW_FOLDER', 'Nouveau dossier');
define('IMAGE_NEW_LANGUAGE', 'Nouvelle Langue');
define('IMAGE_NEW_NEWSLETTER', 'Nouveau bulletin d\'informations');
define('IMAGE_NEW_PRODUCT', 'Nouveau Produit');
define('IMAGE_NEW_TAX_CLASS', 'Nouvelle classe fiscale');
define('IMAGE_NEW_TAX_RATE', 'Nouveau taux fiscal');
define('IMAGE_NEW_TAX_ZONE', 'Nouvelle zone fiscale');
define('IMAGE_NEW_ZONE', 'Nouvelle zone');
define('IMAGE_ORDERS', 'Commandes');
define('IMAGE_ORDERS_ODOO','Commande enregistrée dans Odoo');
define('IMAGE_ORDERS_INVOICE_ODOO','Facture comptabilisée dans Odoo');
define('IMAGE_ORDERS_INVOICE_MANUAL_ODOO', 'Facture enrgistrée dans Odoo');
define('IMAGE_ORDERS_INVOICE_CANCEL_ODOO', 'Facture annulée dans Odoo');
define('IMAGE_ORDERS_HISTORY','Historique');
define('IMAGE_ORDERS_INVOICE', 'Facture');
define('IMAGE_ORDERS_PACKINGSLIP', 'Bon de Livraison');
define('IMAGE_CUSTOMERS', 'Clients');
define('IMAGE_NEWSLETTER', 'Newsletters');
define('IMAGE_REVIEWS', 'Avis des clients');
define('IMAGE_PREVIEW', 'Prévisualiser');
define('ICON_PREVIEW_CATALOG','Prévisualisation Catalogue');
define('IMAGE_RESTORE', 'Restaurer');
define('IMAGE_RESET', 'Réinitialiser');
define('IMAGE_SAVE', 'Sauvegarder');
define('IMAGE_SEARCH', 'rechercher');
define('IMAGE_SELECT', 'Choisir');
define('IMAGE_SEND', 'Envoyer');
define('IMAGE_SEND_EMAIL', 'Envoyer un courrier électronique');
define('IMAGE_UNLOCK', 'Déverrouiller');
define('IMAGE_UPDATE', 'Mettre à jour');
define('IMAGE_UPDATE_CURRENCIES', 'Mettre à jour le taux de change');
define('IMAGE_UPLOAD', 'Transférer');
define('IMAGE_CATEGORIES', 'Catégories');
define('IMAGE_SPECIALS', 'Promotions produits');
define('IMAGE_MANUFACTURERS', 'Gestion des marques');
define('IMAGE_PRODUCTS_EXPECTED', 'Gestion des arrivées en stock');
define('IMAGE_UNPACK','Désarchiver');
define('IMAGE_BACKUP_DB', 'Sauvegarder la base de données');
define('IMAGE_BANNER', 'Gestion des bannières');
define('IMAGE_WHOS_ONLINE', 'Clients en ligne');

// Bouton groupe client
define('IMAGE_NEW_GROUP', 'Nouveau groupe');
define('IMAGE_UPDATEALLPRICE', 'Mettre à jour tous les prix du groupe');

define('ICON_EDIT','Editer');
define('ICON_CROSS', 'Faux');
define('ICON_CURRENT_FOLDER', 'Dossier courant');
define('ICON_DELETE', 'Supprimer');
define('ICON_ERROR', 'Erreur');
define('ICON_FILE', 'Fichier');
define('ICON_FILE_DOWNLOAD', 'Télécharger');
define('ICON_FOLDER', 'Répertoire');
define('ICON_LOCKED', 'Verrouillé');
define('ICON_PREVIOUS_LEVEL', 'Niveau précédent');
define('ICON_PREVIEW', 'Prévisualiser');
define('ICON_STATISTICS', 'Statistiques');
define('ICON_SUCCESS', 'Succès');
define('ICON_TICK', 'Vrai');
define('ICON_UNLOCKED', 'Déverrouillé');
define('ICON_WARNING', 'Attention');
define('ICON_HELP', 'Aide');

// constants for use in osc_prev_next_display function
define('TEXT_RESULT_PAGE', 'Page %s sur %d');
define('TEXT_DISPLAY_NUMBER_OF_BANNERS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> bannières)');
define('TEXT_DISPLAY_NUMBER_OF_COUNTRIES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> pays)');
define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> clients)');
define('TEXT_DISPLAY_NUMBER_OF_CURRENCIES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> devises)');
define('TEXT_DISPLAY_NUMBER_OF_LANGUAGES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> langues)');
define('TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> marques)');
define('TEXT_DISPLAY_NUMBER_OF_SUPPLIERS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> fournisseurs)');
define('TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> bulletins d\'informations)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> commandes)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> statuts commandes)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> produits)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_EXPECTED', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> produits en attente)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> avis clients des produits)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> produits en promotion)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_HEART', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> produits en coups de coeur)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_CLASSES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> classes fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_ZONES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> zones fiscales)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_RATES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> taux fiscal)');
define('TEXT_DISPLAY_NUMBER_OF_ZONES', 'Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> zones)');
define('TEXT_DISPLAY_NUMBER_OF_ENTRIES','Affiche <strong>%d</strong> to <strong>%d</strong> (sur <strong>%d</strong> surveillances des actions sur le site)'); 
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_QUANTITY_UNIT_STATUS','Affiche <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> types de quantité de commande)');
define('PREVNEXT_BUTTON_PREV', '&lt;&lt;');
define('PREVNEXT_BUTTON_NEXT', '&gt;&gt;');

define('TEXT_DEFAULT', 'défaut');
define('TEXT_SET_DEFAULT', 'mettre par défaut');
define('TEXT_FIELD_REQUIRED', '&nbsp;<span class="fieldRequired">* Requis</span>');
define('TEXT_SORT_ALL', 'Tri ');
define('TEXT_DESCENDINGLY', 'descendant');
define('TEXT_ASCENDINGLY', 'ascendant');
define('TEXT_IMAGE_PREVIEW','Prévisualiser');
define('TEXT_OTHER', 'Autres');
define('TEXT_LEGEND', 'Légende : ');
define('TEXT_ALL_GROUPS', 'Tous les groupes clients');

define('TEXT_CACHE_CATEGORIES', 'Boite catégories');
define('TEXT_CACHE_MANUFACTURERS', 'Boite marques');
define('TEXT_CACHE_ALSO_PURCHASED', 'Module d\'achat supplémentaire');
define('TEXT_CACHE_PRODUCTS_RELATED','Module des produits complémentaires');
define('TEXT_CACHE_PRODUCTS_CROSS_SELL','Module des produits croisés');
define('TEXT_CACHE_NEW', 'Nouveaux produits');
define('TEXT_CACHE_UPCOMING', 'Produits à venir');

define('TEXT_NONE', '--Aucun--');
define('TEXT_TOP', 'Haut');

define('TEXT_TWITTER_MESSAGE','Votre message a bien été envoyé sur twitter<br />');
define('TEXT_TWITTER_PROFILE',' Voir votre profil');
define('TEXT_ERROR_TWITTER_POST','Erreur, veuillez insérer un message et réessayer.<br />');

define('TEXT_HEADER_USER_ADMINISTRATOR','Administrateur');
define('TEXT_HEADER_LOGOFF_ADMINISTRATOR', 'Déconnexion');
define('TEXT_HEADER_NUMBER_OF_CUSTOMERS', '%s Client(s) en ligne');
define('TEXT_HEADER_ICON_NUMBER_OF_CUSTOMERS', 'Afficher les clients en ligne');
define('TEXT_HELP_WYSIWYG','Information sur l\'aide à l\'utilisation du Wysiwyg en image');
define('TEXT_CLOSE','Fermer');

define('ERROR_DESTINATION_DOES_NOT_EXIST', 'Erreur : le chemin cible n\'existe pas.');
define('ERROR_DESTINATION_NOT_WRITEABLE', 'Erreur : Impossible d\'écrire dans le répertoire cible.');
define('ERROR_FILE_NOT_SAVED', 'Erreur : fichier transférer non sauvegardé.');
define('ERROR_FILETYPE_NOT_ALLOWED', 'Erreur : type de fichier transféré non-permis.');
define('SUCCESS_FILE_SAVED_SUCCESSFULLY', 'Succès : Le fichier transféré a été sauvegardé avec succès.');
define('WARNING_NO_FILE_UPLOADED', 'Informations : Aucune image ou fichier de transféré sur le serveur.');
define('WARNING_EDIT_CUSTOMERS', 'Des champs comportent des erreurs, veuillez rectifier.');
define('ERROR_FILE_NOT_WRITEABLE','Impossible d\'écrire dans ce fichier');
define('ERROR_BANNER_TITLE', 'Erreur: Titre de la bannière requis');
define('ERROR_BANNER_GROUP', 'Erreur: Groupe de la bannière requis');
define('ERROR_NO_DEFAULT_LANGUAGE_DEFINED', 'Erreur: Il n\'y a pas de langue par défaut. Merci d\'en choisir une dans: Administration Outils->Localisation->Langues');

// Mode de facturation des clients
define('OPTIONS_ORDER', 'Facturation :');
define('OPTIONS_ORDER_NATIONAL', 'National');
define('OPTIONS_ORDER_CEE', 'Union Européenne');
define('OPTIONS_ORDER_INTERNATIONAL', 'International');
define('OPTIONS_ORDER_TAXE', 'Assujetti à la taxe');
define('OPTIONS_ORDER_NO_TAXE', 'Non assujetti à la taxe');

// Nom affiché dans le menu déroulant pour client par défaut
define('VISITOR_NAME','Client Normal');

// Affichage sur different champ pour indiquer le montant en HT ou en TTC
define('TAX_INCLUDED', 'Toutes taxes');
define('TAX_EXCLUDED', 'Hors taxes');

// Menu des onglets
define('TAB_GENERAL', 'Général');
define('TAB_ORDERS', 'Facturation');
define('TAB_SHIPPING', 'Livraison');
define('TAB_CATEGORIE', 'Catégorie');
define('TAB_PRICE', 'Prix');
define('TAB_DESC', 'Description');
define('TAB_IMG', 'Visuel');
define('TAB_REF', 'Metadonnées');
define('TAB_SOCIETE', 'Société');
define('TAB_ADRESSE_BOOK', 'Carnet');
define('TAB_CODE_HTML', 'Code HTML');
define('TAB_ORDERS_DETAILS', 'Commande');
define('TAB_STATUT', 'Statut');
define('TAB_OPTIONS','Options');

// Facture Y = Année / m = mois / d = jours  (Voir aussi fonction osc_datereference_short() dans general.php)
define('DATE_FORMAT_REFERENCE', 'Ym');

// Discount coupon
define('FIXED_AMOUNT','Montant fixe');
define('PERCENTAGE_DISCOUNT',' Remise sur pourcentage');
define('PRICE_TOTAL','Total du prix');
define('PRODUCT_QUANTITY','Quantité de produit');


define('KEYWORDS_GOOGLE_TREND','Tendance des mots clefs selon Google');
define('ANALYSIS_GOOGLE_TOOL','Outil d\'analyse selon Google');

// Succes / error
define('SUCCESS_STATUS_UPDATED','Mise à jour fait avec succès');
define('ERROR_UNKNOWN_STATUS_FLAG','Erreur Inconnue');

// Dynamic Template system
define('ALL_PAGES', 'Toutes les pages.');
define('ONE_BY_ONE', 'Ou une par une :');
define('CHECK_ALL', 'Tout selectionner');
define('DESELECT_ALL', 'Tout Supprimer');

define('SELECT_DATAS','--- Séléctionner ---');

// No script
define('NO_SCRIPT_TEXT', "<p><strong>JavaScript ne semble pas activé.</strong></p><p>Vous devez activer le javascript dans votre navigateur pour utiliser correctement ClicShopping. <a href='http://www.enable-javascript.com/' target='_blank' rel='nofollow'>Veuillez cliquer-ici pour une aide en ligne</a>.</p>");

define('TITLE_HELP_GENERAL','Aide');
?>