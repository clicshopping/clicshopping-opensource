<?php
/**
 * customers_group.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Groupe Clients');
define('HEADING_TITLE_EDIT', 'Edition du Groupe Clients');
define('HEADING_TITLE_NEW', 'Nouveau Groupe Clients');

if (B2B == 'true'){
define('HEADING_TITLE_CATEGORIE', 'Majoration sur une séléction de catégorie');
define('ENTRY_CATEGORIES_DISCOUNT', 'Majoration en % par défaut :');
define('TITLE_DEFAULT_DISCOUNT', 'Majoration par défaut sur les produits');
define('TITLE_CATEGORIES_DISCOUNT', 'Nom et majoration par défaut sur une catégorie');
} else {
define('HEADING_TITLE_CATEGORIE', 'Promotion sur une séléction de catégorie');
define('ENTRY_CATEGORIES_DISCOUNT', 'Promotion en % par défaut :');
define('TITLE_DEFAULT_DISCOUNT', 'Promotion par défaut sur les produits');
define('TITLE_CATEGORIES_DISCOUNT', 'Nom et promotion par défaut sur une catégorie');
}

define('HEADING_TITLE_SEARCH', 'Rechercher');

define('TABLE_HEADING_NAME', 'Nom');

if (B2B == 'true'){
define('TABLE_HEADING_DISCOUNT', 'Promotion/Majoration par défaut');
define('TABLE_HEADING_CATEGORIES_DISCOUNT', 'Promotion/Majoration sur une catégorie');
} else {
define('TABLE_HEADING_DISCOUNT', 'Remise par défaut');
define('TABLE_HEADING_CATEGORIES_DISCOUNT', 'Promotion/Majoration sur une catégorie');
}

define('TABLE_HEADING_QUANTITY_DEFAULT','Quantité de commande du groupe');
define('TABLE_HEADING_COLOR', 'Code couleur');
define('TABLE_HEADING_ACTION', 'Action');
define('ENTRY_GROUP_CATEGORIES', 'Catégorie :');
define('SELECT_CATEGORY', '1- Séléctionner une Catégorie');

define('TEXT_CATEGORIES', 'Catégories');
define('TEXT_CATEGORIES_NO_NEW', 'Après enregistrement de ce nouveau groupe clients vous aurez la possibilité de personnaliser des remises différentes sur les catégories de votre choix');
define('TEXT_PRODUCTS_UPDATED', '');
define('TEXT_QTY_UPDATED', 'Prix mis à jour concernant le groupe');

define('TITLE_DEFAULT_QUANTITY','Quantité minimale de commande');
define('ENTRY_TEXT_QUANTITY_DEFAULT','Quantité minimale de commande de ce groupe');
define('ENTRY_TEXT_QUANTITY_NOTE', '&nbsp;<span class="errorText">* Voir note</span>');

define('ENTRY_GROUPS_NAME_ERROR', 'Aucun enregistrement n\'a pu �tre effectué car vous n\'aviez pas informer de nom pour le groupe');
define('ENTRY_GROUPS_NAME_ERROR_ZERO', 'Attention % champs est/sont exigé(s) et doit �tre différent de zéro');
define('ENTRY_GROUPS_CATEGORIE_ERROR', 'Aucun enregistrement n\'a plus �tre effectué car vous n\'avez pas sélectionné de catégorie');

define('TEXT_DELETE_INTRO', 'Etes vous certain de vouloir supprimer ce groupe ?');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Suppression du groupe');
define('TEXT_IMPOSSIBLE_DELETE', 'Vous ne pouvez pas supprimer le groupe suivant !');
define('TYPE_BELOW', 'Type');
define('PLEASE_SELECT', 'Sélectionner');
define('ENTRY_COLOR_BAR', 'Code couleur :');

define('TITLE_GROUP_TAX', 'Mode d\'affichage des prix sur les produits');
define('ENTRY_GROUP_TAX', 'Affichage des prix :');
define('TEXT_GROUP_TAX_INC', 'Toutes taxes');
define('TEXT_GROUP_TAX_EX', 'Hors Taxes');
define('ENTRY_GROUP_TAX_NOTE', '&nbsp;<span class="errorText">* Veuillez lire la note ci-dessous</span>');

define('TITLE_GROUP_NAME', 'Nom et code couleur du groupe clients');
define('ENTRY_VALEUR_DISCOUNT', 'Valeur <i>(%)</i> :');
define('ENTRY_VALEUR_DISCOUNT_NOTE', '&nbsp;<span class="errorText">* Veuillez ne pas ajouter les symboles suivants : + - ou %</span>');

define('TITLE_ORDER_CUSTOMER_DEFAULT', 'Mode de facturation par défaut');
define('TITLE_GROUP_PAIEMENT_DEFAULT', 'Mode de paiement à autoriser par défaut');
define('TITLE_GROUP_SHIPPING_DEFAULT', 'Mode de livraison à autoriser par défaut');
define('TITLE_LIST_CATEGORIES_DISCOUNT', 'Liste des catégories en action');

define('AIDE_TITLE_ONGLET_GENERAL', 'Note sur la gestion des groupes B2B');
define('AIDE_GROUP_TAX', '<li>En sélectionnant " <i>Non assujettie à la Taxe</i> " dans l\'onglet facturation, la sélection du mode d\'affichage des prix produits ce positionnera automatiquement en mode Hors Taxes à l\'enregistrement.</li>
<li>Si la quantité minimale de commande est égale à 0, ce sera la quantité par défaut générale qui sera prise en compte. La quantité minimale par produit ne s\'applique pas dans le cadre du mode B2B</li>');

define('AIDE_TITLE_ONGLET_FACTURATION', 'Note sur la gestion des groupes B2B');
define('AIDE_ORDER_TAX', '<li>En sélectionnant " <i>Non assujettie à la Taxe</i> ", la sélection du mode d\'affichage des prix produits dans l\'onglet général ce positionnera automatiquement en mode Hors Taxes à l\'enregistrement.</li>');
?>