<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Shipping &amp; Returns');
define('HEADING_TITLE', 'Shipping &amp; Returns');

define('TEXT_INFORMATION', 'Put here your Shipping &amp; Returns information.');
?>