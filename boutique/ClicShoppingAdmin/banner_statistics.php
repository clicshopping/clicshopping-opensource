<?php
/*
 * banner_statistics.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

  require('includes/application_top.php');

  $type = (isset($_GET['type']) ? $_GET['type'] : '');

  $banner_extension = osc_banner_image_extension();

// check if the graphs directory exists
  $dir_ok = false;
  if (function_exists('imagecreate') && osc_not_null($banner_extension)) {
    if (is_dir(DIR_WS_IMAGES . 'graphs')) {
      if (osc_is_writable(DIR_WS_IMAGES . 'graphs')) {
        $dir_ok = true;
      } else {
        $OSCOM_MessageStack->add(ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE, 'error');
      }
    } else {
      $OSCOM_MessageStack->add(ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST, 'error');
    }
  }

  $Qbanner = $OSCOM_PDO->prepare('select banners_title
                                  from :table_banners
                                   where banners_id = :banners_id
                                ');
  $Qbanner->bindInt(':banners_id', (int)$_GET['bID'] );
  $Qbanner->execute();

  $banner = $Qbanner->fetch();

  $years_array = array();

  $Qyears = $OSCOM_PDO->prepare('select distinct year(banners_history_date) as banner_year
                                 from :table_banners_history
                                 where banners_id = :banners_id
                                ');
  $Qyears->bindInt(':banners_id', (int)$_GET['bID'] );
  $Qyears->execute();

  while ($years = $Qbanner->fetch()) {
    $years_array[] = array('id' => $years['banner_year'],
                           'text' => $years['banner_year']);
  }

  $months_array = array();
  for ($i=1; $i<13; $i++) {
    $months_array[] = array('id' => $i,
                            'text' => strftime('%B', mktime(0,0,0,$i)));
  }

  $type_array = array(array('id' => 'daily',
                            'text' => STATISTICS_TYPE_DAILY),
                      array('id' => 'monthly',
                            'text' => STATISTICS_TYPE_MONTHLY),
                      array('id' => 'yearly',
                            'text' => STATISTICS_TYPE_YEARLY));

  require('includes/header.php');
?>

<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr><?php echo osc_draw_form('year', 'banner_statistics.php', '', 'get'); ?>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/banner_statistics.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td align="right"><table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="smallText" valign="top"><?php echo TITLE_TYPE . ' ' . osc_draw_pull_down_menu('type', $type_array, (osc_not_null($type) ? $type : 'daily'), 'onchange="this.form.submit();"'); ?><noscript><input type="submit" value="GO"></noscript></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '5', '1'); ?></td>
            <td class="smallText" valign="top">
<?php
  switch ($type) {
    case 'yearly': break;
    case 'monthly':
      echo TITLE_YEAR . ' ' . osc_draw_pull_down_menu('year', $years_array, (isset($_GET['year']) ? $_GET['year'] : date('Y')), 'onchange="this.form.submit();"') . '<noscript><input type="submit" value="GO"></noscript>';
      break;
    default:
    case 'daily':
      echo TITLE_MONTH . ' ' . osc_draw_pull_down_menu('month', $months_array, (isset($_GET['month']) ? $_GET['month'] : date('n')), 'onchange="this.form.submit();"') . '<noscript><input type="submit" value="GO"></noscript><br />' . TITLE_YEAR . ' ' . osc_draw_pull_down_menu('year', $years_array, (isset($_GET['year']) ? $_GET['year'] : date('Y')), 'onchange="this.form.submit();"') . '<noscript><input type="submit" value="GO"></noscript>';
      break;
  }
?>
            </td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '5', '1'); ?></td>
            <td><?php echo '<a href="' . osc_href_link('banner_manager.php', 'page=' . $_GET['page'] . '&bID=' . $_GET['bID']) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
           </tr>
        </table></td>
          <?php echo osc_draw_hidden_field('page', $_GET['page']) . osc_draw_hidden_field('bID', $_GET['bID']) . osc_hide_session_id(); ?></form></tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td align="center">
<?php
  if (function_exists('imagecreate') && ($dir_ok == true) && osc_not_null($banner_extension)) {
    $banner_id = (int)$_GET['bID'];

    switch ($type) {
      case 'yearly':
        include('includes/graphs/banner_yearly.php');
        echo osc_image(DIR_WS_IMAGES . 'graphs/banner_yearly-' . $banner_id . '.' . $banner_extension);
        break;
      case 'monthly':
        include('includes/graphs/banner_monthly.php');
        echo osc_image(DIR_WS_IMAGES . 'graphs/banner_monthly-' . $banner_id . '.' . $banner_extension);
        break;
      default:
      case 'daily':
        include('includes/graphs/banner_daily.php');
        echo osc_image(DIR_WS_IMAGES . 'graphs/banner_daily-' . $banner_id . '.' . $banner_extension);
        break;
    }
?>
          <table border="0" width="600" cellspacing="0" cellpadding="2">
            <tr class="dataTableHeadingRow">
             <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SOURCE; ?></td>
             <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_VIEWS; ?></td>
             <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_CLICKS; ?></td>
           </tr>
<?php
    for ($i=0, $n=sizeof($stats); $i<$n; $i++) {
      echo '            <tr class="dataTableRow">' . "\n" .
           '              <td class="dataTableContent">' . $stats[$i][0] . '</td>' . "\n" .
           '              <td class="dataTableContent" align="right">' . number_format($stats[$i][1]) . '</td>' . "\n" .
           '              <td class="dataTableContent" align="right">' . number_format($stats[$i][2]) . '</td>' . "\n" .
           '            </tr>' . "\n";
    }
?>
          </table>
<?php
  } else {
    include('includes/functions/html_graphs.php');

    switch ($type) {
      case 'yearly':
        echo osc_banner_graph_yearly($_GET['bID']);
        break;
      case 'monthly':
        echo osc_banner_graph_monthly($_GET['bID']);
        break;
      default:
      case 'daily':
        echo osc_banner_graph_daily($_GET['bID']);
        break;
    }
  }
?>
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
