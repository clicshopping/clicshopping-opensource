<?php
/**
 * geo_zones.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Tax Zones');

define('TABLE_HEADING_COUNTRY', 'Country');
define('TABLE_HEADING_COUNTRY_ZONE', 'Zone');
define('TABLE_HEADING_TAX_ZONES', 'Tax Zones');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_TAX_DESCRIPTION','Description');
define('TABLE_HEADING_NUMBER_ZONE','Number of Zones');

define('TEXT_INFO_HEADING_NEW_ZONE', 'New Zone');
define('TEXT_INFO_NEW_ZONE_INTRO', 'Please enter the new zone information');

define('TEXT_INFO_HEADING_EDIT_ZONE', 'Edit Zone');
define('TEXT_INFO_EDIT_ZONE_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_HEADING_DELETE_ZONE', 'Delete Zone');
define('TEXT_INFO_DELETE_ZONE_INTRO', 'Are you sure you want to delete this zone?');

define('TEXT_INFO_HEADING_NEW_SUB_ZONE', 'New Sub Zone');
define('TEXT_INFO_NEW_SUB_ZONE_INTRO', 'Please enter the new sub zone information');

define('TEXT_INFO_HEADING_EDIT_SUB_ZONE', 'Edit Sub Zone');
define('TEXT_INFO_EDIT_SUB_ZONE_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_HEADING_DELETE_SUB_ZONE', 'Delete Sub Zone');
define('TEXT_INFO_DELETE_SUB_ZONE_INTRO', 'Are you sure you want to delete this sub zone?');

define('TEXT_INFO_DATE_ADDED', 'Date Added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_ZONE_NAME', 'Zone Name:');
define('TEXT_INFO_NUMBER_ZONES', 'Number of Zones:');
define('TEXT_INFO_ZONE_DESCRIPTION', 'Description:');
define('TEXT_INFO_COUNTRY', 'Country:');
define('TEXT_INFO_COUNTRY_ZONE', 'Zone:');
define('TYPE_BELOW', 'All Zones');
define('PLEASE_SELECT', 'All Zones');
define('TEXT_ALL_COUNTRIES', 'All Countries');
?>
