<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Créer un compte');
define('HEADING_TITLE', 'Créer un compte');

define('EMAIL_SUBJECT', 'Demander une ouverture de compte sur ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");

define('ADMIN_EMAIL_WELCOME', 'Bonjour,'. "\n\n" . '');
define('ADMIN_EMAIL_TEXT', 'Un nouveau client (mode B2C) vient de s\'enregistrer, vous trouverez les informations ci-dessous :<br />
Nom :' . $_POST['lastname'] . '<br />
Prenom :' . $_POST['firstname'] . '<br />
Compagnie :' . $_POST['company'] . '<br />
E-Mail :' . $_POST['email_address'] . '<br />
');

define('EMAIL_NETWORK',''. "\n\n" . 'Suivez nous en temps reel sur :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');
?>
