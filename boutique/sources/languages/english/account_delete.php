<?php
/**
 *
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */
define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Delete Account');
define('HEADING_TITLE', 'delete Account');
define('TEXT_MODULE_ACCOUNT_CUSTOMERS_DELETE_ACCOUNT_INTRO','By confirming this action, your account will be permanently deleted');
define('TEXT_MODULE_ACCOUNT_CUSTOMERS_DELETE_ACCOUNT_CHECKBOX', 'Confirm you request to delete your account : ');
define('EMAIL_TEXT_SUBJECT','Account deleted on '.STORE_NAME);
define('EMAIL_TEXT_MESSAGE','We confirm you that your request has been realised on '.STORE_NAME. '<br /><br />We hope to see you soon.<br /><br />Regards, '. STORE_NAME .' team');

?>