<?php
  /**
   * cfm_modules_footer_suffix.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_modules_footer_suffix {
    var $code = 'modules_footer_suffix';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_FOOTER_SUFFIX_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_footer_suffix() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_footer_suffix/';
      $this->title = MODULE_CFG_MODULE_FOOTER_SUFFIX_TITLE;
    }
  }
?>
