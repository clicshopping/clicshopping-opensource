<?php
/**
 * manufacturers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @license GNU Public License V2.0
 * @version $Id: manufacturers.php 

 */

  $comp = array("Exportation des marques XML");

  $header = 'Content-Type: text/xml';

  $head = '<?xml version="1.0" encoding="UTF-8"?>' .chr(10) . '<manufacturers lang="'.$language_code.'" date="'.  date('Y-m-d H:i'). '" GMT="+1" version="2.0">'.chr(10); 
  
  $output .= '<manufacturers place="'. $manufacturers['manufacturers_id'].'">'."\n";
  $output .= '<manufacturers_name><![CDATA[' . $manufacturers['manufacturers_name'] .']]></manufacturers_name>'.chr(10);
  $output .= '<manufacturers_image><![CDATA[' . $manufacturers['manufacturers_image'] .']]></manufacturers_image>'.chr(10);
  $output .= '<manufacturers_url><![CDATA[' . $manufacturers['manufacturers_url'] .']]></manufacturers_url>'.chr(10);
  $output .= '</manufacturers>';
  $foot = '</manufacturers>';
?>
