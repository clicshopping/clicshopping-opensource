<?php
/**
 * pdf.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class PDF extends FPDF {

    function RoundedRect($x, $y, $w, $h,$r, $style = '') {
      $k = $this->k;
      $hp = $this->h;

      if ($style=='F') {
        $op='f';
      } else if ($style=='FD' or $style=='DF') {
        $op='B';
      } else {
        $op='S';
      }

      $MyArc = 4/3 * (sqrt(2) - 1);
      $this->_out(sprintf('%.2f %.2f m',($x+$r)*$k,($hp-$y)*$k ));
      $xc = $x+$w-$r;
      $yc = $y+$r;
      $this->_out(sprintf('%.2f %.2f l', $xc*$k,($hp-$y)*$k ));
      $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
      $xc = $x+$w-$r;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-$yc)*$k));
      $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
      $xc = $x+$r;
      $yc = $y+$h-$r;
      $this->_out(sprintf('%.2f %.2f l',$xc*$k,($hp-($y+$h))*$k));
      $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
      $xc = $x+$r;
      $yc = $y+$r;
      $this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$yc)*$k ));
      $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
      $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
      $h = $this->h;
      $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k,
      $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

    function Header() {
      global $oID;

      $date = strftime('%A, %d %B %Y');


// Logo
        $this->Image(HTTP_SERVER . '/' . DIR_WS_HTTP_CATALOG . DIR_WS_IMAGES . 'logos/invoice/'. INVOICE_LOGO,5,10,50);

// Nom de la compagnie
        $this->SetX(0);
        $this->SetY(10);
        $this->SetFont('Arial','B',10);
        $this->SetTextColor(INVOICE_RGB);
        $this->Ln(0);
        $this->Cell(125);
        $this->MultiCell(100, 3.5, utf8_decode(STORE_NAME),0,'L');

// Adresse de la compagnie
        $this->SetX(0);
        $this->SetY(15);
        $this->SetFont('Arial','',8);
        $this->SetTextColor(INVOICE_RGB);
        $this->Ln(0);
        $this->Cell(125);
        $this->MultiCell(100, 3.5, utf8_decode(STORE_NAME_ADDRESS),0,'L'); 

// Email
        $this->SetX(0);
        $this->SetY(30);
        $this->SetFont('Arial','',8);
        $this->SetTextColor(INVOICE_RGB);
        $this->Ln(0);
        $this->Cell(-3);
        $this->MultiCell(100, 3.5, utf8_decode(ENTRY_EMAIL) . STORE_OWNER_EMAIL_ADDRESS,0,'L');

// Website
        $this->SetX(0);
        $this->SetY(34);
        $this->SetFont('Arial','',8);
        $this->SetTextColor(INVOICE_RGB);
        $this->Ln(0);
        $this->Cell(-3);
        $this->MultiCell(100, 3.5, ENTRY_HTTP_SITE . HTTP_SERVER,0,'L');

    }

    function Footer() {
// Remerciement
      $this->SetY(-55);
      $this->SetFont('Arial','B',8);
      $this->SetTextColor(INVOICE_RGB);
      $this->Cell(0,10, utf8_decode(THANK_YOU_CUSTOMER), 0,0,'C');

// Proprieties Legal
        $this->SetY(-45);
        $this->SetFont('Arial','',7);
        $this->SetTextColor(INVOICE_RGB);
        $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE), 0,0,'C');

        $this->SetY(-40);
        $this->SetFont('Arial','',7);
        $this->SetTextColor(INVOICE_RGB);
        $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE_NEXT), 0,0,'C');

        $this->SetY(-35);
        $this->SetFont('Arial','',7);
        $this->SetTextColor(INVOICE_RGB);
        $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE_NEXT1), 0,0,'C');

// Informations de la compagnie
        $this->SetY(-25);
        $this->SetFont('Arial','',8);
        $this->SetTextColor(INVOICE_RGB);
        $this->Cell(0,10, utf8_decode(ENTRY_INFO_SOCIETE), 0,0,'C');

        $this->SetY(-20);
        $this->SetFont('Arial','',8);
        $this->SetTextColor(INVOICE_RGB);
        $this->Cell(0,10, utf8_decode(ENTRY_INFO_SOCIETE_NEXT), 0,0,'C');

// Autres informations (champ libre) sur la compagnie
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->SetTextColor(INVOICE_RGB);
        $this->Cell(0,10, utf8_decode(SHOP_DIVERS), 0,0,'C');
    }
  }