<?php
/*
 * discountcoupons_exclusions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  class coupons_exclusions {

    var $type, $display_fields, $coupons_id, $table_name, $selected_options, $all_options;

    function coupons_exclusions( $coupons_id, $type = 'products' ) {
//check that the coupon is not null or empty
      if( osc_not_null( $coupons_id ) ) {
        $this->coupons_id = osc_db_input( $coupons_id );
      } else {
        return false;
      }
//check that the table definition for the type exists

      if( !empty($type)  ) {

        $this->type = $type;
        $this->table_name = 'discount_coupons_to_'. $type;

      } else {
        return false;
      }

      $this->selected_options = array();
      $this->all_options = array();
    }

    function save( $selected_options = array() ) {
      global $OSCOM_PDO;
      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from '.$this->table_name.'
                                      where coupons_id = :coupons_id
                                    ');

      $Qdelete->bindValue(':coupons_id', $this->coupons_id);
      $Qdelete->execute();

      if( is_array( $selected_options ) && count( $selected_options ) > 0 ) {
        foreach ($selected_options as $ids ) {

          if  ($this->table_name == 'discount_coupons_to_categories') {
            $OSCOM_PDO->save($this->table_name, [
                'coupons_id' => $this->coupons_id,
                'categories_id' => $ids
              ]
            );
          }

          if  ($this->table_name == 'discount_coupons_to_customers') {
            $OSCOM_PDO->save($this->table_name, [
                'coupons_id' => $this->coupons_id,
                'customers_id' => $ids
              ]
            );
          }

          if  ($this->table_name == 'discount_coupons_to_manufacturers') {
            $OSCOM_PDO->save($this->table_name, [
                'coupons_id' => $this->coupons_id,
                'manufacturers_id' => $ids
              ]
            );
          }

          if  ($this->table_name == 'discount_coupons_to_orders') {
            $OSCOM_PDO->save($this->table_name, [
                'coupons_id' => $this->coupons_id,
                'orders_id' => $ids
              ]
            );
          }

          if  ($this->table_name == 'discount_coupons_to_products') {
            $OSCOM_PDO->save($this->table_name, [
                'coupons_id' => $this->coupons_id,
                'products_id' => $ids
              ]
            );
          }

          if  ($this->table_name == 'discount_coupons_to_zones') {
            $OSCOM_PDO->save($this->table_name, [
                'coupons_id' => $this->coupons_id,
                'geo_zone_id' => $ids
              ]
            );
          }
        }
      } else return false;
    }

    function osc_get_selected_options( $sql, $separator = ' :: '/*category exclusions*/, $category_separator = '->', $category_path = true/*end category exclusions*/ ) {
      global $OSCOM_PDO;
      
      if( !osc_not_null( $sql ) ) return false;

      $result = $OSCOM_PDO->query($sql);

        $selected_ids = array();
        if( $result->rowCount() > 0 ) {
          while( $row = $result->fetch()  ) {
            $id = $row['id'];
            unset( $row['id'] ); //don't display in the list

//category exclusions
            if( $category_path ) {
              $path = '';
//get the category path
              $categories = array();
              osc_get_parent_categories( $categories, $id );
              $categories = array_reverse( $categories );
              foreach( $categories as $cat ) {
                $path .= osc_get_categories_name( $cat, (int)$_SESSION['languages_id'] ).$category_separator;
              }
              $row['categories_name'] = $path.$row['categories_name'];
            }
//end category exclusions

            $selected_ids[] = $id;
            $this->selected_options[] = array( 'text' => implode( $row, $separator ), 'id' => $id );
          }
          sort( $this->selected_options );
        }
      return $selected_ids;
    }

    function osc_get_all_options( $sql, $separator = ' :: '/*category exclusions*/, $category_separator = '->', $category_path = true/*end category exclusions*/, $selected_ids = array() ) {
      global $OSCOM_PDO;
       
       if( !osc_not_null( $sql ) ) return false;
         $result = $OSCOM_PDO->query($sql);
        if( $result->rowCount() > 0 ) {
          while( $row = $result->fetch() ) {

            $id = $row['id'];

//category exclusions
            if( $category_path ) {
              $path = '';
//get the category path
              $categories = array();
              osc_get_parent_categories( $categories, $id );
              $categories = array_reverse( $categories );

              foreach( $categories as $cat ) {
                $path .= osc_get_categories_name( $cat, (int)$_SESSION['languages_id'] ).$category_separator;
              }

              $row['categories_name'] = $path.$row['categories_name'];
            }
//end category exclusions

            unset( $row['id'] ); //don't display in the list
            $this->all_options[] = array( 'text' => implode( $row, $separator ), 'id' => $id );
          }

         sort( $this->all_options );
         }
      return true;
    }

    function display() {
      $return = '
<script language="javascript" type="text/javascript"><!--

function updateSelect( to_select, from_select ) {
   for( var i = 0; i < from_select.options.length; i++ ) {
      if( from_select.options[i].selected ) {
        var newOption = new Option( from_select.options[i].text, from_select.options[i].value )
        to_select.options[ to_select.options.length ] = newOption;
      }
   }
   deleteOptions( from_select );
}

function deleteOptions( delete_select ) {
  for( var i = 0; i < delete_select.options.length; i++ ) {
    if( delete_select.options[i].selected ) {
      delete_select.options[i] = null;
      i=-1;
    }
  }
}

function selectAll( to_select, from_select ) {
  for( var i=0; i < from_select.options.length; i++ ) {
    from_select.options[i].selected = true;
  }
  updateSelect( to_select, from_select );
}

function form_submission( to_select ) {
  for( var i=0; i < to_select.options.length; i++ ) {
    to_select.options[i].selected = true;
  }
}

//--></script>

'
        . HTMl::form('choose'.$this->type, osc_href_link('discount_coupons_exclusions.php', 'cID='.$this->coupons_id.'&type='.$_GET['type'],'SSL'), 'post', 'onsubmit="form_submission( document.getElementById(\'selected_'.$_GET['type'].'\') )"' ).'
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" class="main">'.HEADING_AVAILABLE.'</td>
      <td align="center">&nbsp;</td>
      <td align="center" class="main">'.HEADING_SELECTED.'</td>
    </tr>

    <tr>
      <td rowspan="5" align="center">'.osc_draw_pull_down_menu('available_'.$this->type.'[]', $this->all_options, '', 'size="20" multiple style="width: 300px" id="available_'.$this->type.'"').'</td>
      <td align="center"><input name="choose_all" type="button" id="choose_all" value="' . TEXT_CHOOSE_ALL . '&gt;" onclick="selectAll( document.getElementById(\'selected_'.$this->type.'\'), document.getElementById(\'available_'.$this->type.'\') )"></td>
      <td rowspan="5" align="center">'.osc_draw_pull_down_menu('selected_'.$this->type.'[]', $this->selected_options, '', 'size="20" multiple style="width: 300px" id="selected_'.$this->type.'"').'</td>
    </tr>
    <tr>
      <td align="center"><input name="add" type="button" id="add" value="&gt; &gt;" onclick="updateSelect( document.getElementById(\'selected_'.$this->type.'\'), document.getElementById(\'available_'.$this->type.'\') )"></td>
    </tr>
    <tr>
      <td align="center"><input name="subtract" type="button" id="subtract" value="&lt; &lt;" onclick="updateSelect( document.getElementById(\'available_'.$this->type.'\'), document.getElementById(\'selected_'.$this->type.'\') )"></td>
    </tr>
    <tr>
      <td align="center"><input name="remove_all" type="button" id="remove_all" value="&lt; ' . TEXT_REMOVE .'" onclick="selectAll( document.getElementById(\'available_'.$this->type.'\'), document.getElementById(\'selected_'.$this->type.'\') )"></td>
    </tr>
    <tr>
      <td align="center"><input name="action" type="submit" id="action" value="' . TEXT_SAVE .'"></td>
    </tr>
  </table>

</form>

';
      return $return;
    }
  }

