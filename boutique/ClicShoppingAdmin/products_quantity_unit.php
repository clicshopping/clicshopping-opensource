<?php
/**
 * products_quantity_unit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':
      case 'save':
        if (isset($_GET['oID'])) $products_quantity_unit_id = osc_db_prepare_input($_GET['oID']);

        $languages = osc_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $products_quantity_unit_title_array = $_POST['products_quantity_unit_title'];
          $language_id = $languages[$i]['id'];

          $sql_data_array = array('products_quantity_unit_title' => osc_db_prepare_input($products_quantity_unit_title_array[$language_id]));

          if ($action == 'insert') {
            if (empty($products_quantity_unit_id)) {

              $QnextId = $OSCOM_PDO->prepare('select max(products_quantity_unit_id) as products_quantity_unit_id 
                                              from :table_products_quantity_unit
                                            ');
              $QnextId->execute();

              $next_id = $QnextId->fetch();

              $products_quantity_unit_id = $next_id['products_quantity_unit_id'] + 1;
            }

            $insert_sql_data = array('products_quantity_unit_id' => $products_quantity_unit_id,
                                     'language_id' => $language_id);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            $OSCOM_PDO->save('products_quantity_unit', $sql_data_array);

          } elseif ($action == 'save') {

            $OSCOM_PDO->save('products_quantity_unit', $sql_data_array, ['products_quantity_unit_id' => (int)$products_quantity_unit_id,
                                                                         'language_id' => (int)$language_id
                                                                        ]
                            );
          }
        }

        if (isset($_POST['default']) && ($_POST['default'] == 'on')) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', $products_quantity_unit_id);
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID');
          $Qupdate->execute();
        }

        osc_redirect(osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $products_quantity_unit_id));

      break;
      case 'deleteconfirm':
        $oID = osc_db_prepare_input($_GET['oID']);

        $QcfgProductsUnitQuantityStatus = $OSCOM_PDO->prepare("select configuration_value  
                                                               from :table_configuration
                                                               where configuration_key = :configuration_key
                                                              ");
        $QcfgProductsUnitQuantityStatus->bindValue(':configuration_key', DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID );
        $QcfgProductsUnitQuantityStatus->execute();

        $products_unit_quantity_status = $QcfgProductsUnitQuantityStatus->fetch();

        if ($products_unit_quantity_status['configuration_value'] == $oID) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', '' );
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID');
          $Qupdate->execute();
        }

        $Qdelete = $OSCOM_PDO->prepare('delete from :table_products_quantity_unit
                                        where products_quantity_unit_id = :products_quantity_unit_id
                                        ');
        $Qdelete->bindInt(':products_quantity_unit_id', $oID);
        $Qdelete->execute();


        osc_redirect(osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page']));
      break;
      case 'delete':
        $oID = osc_db_prepare_input($_GET['oID']);

        $Qstatus = $OSCOM_PDO->prepare("select count(*) as count  
                                         from :table_products
                                         where products_quantity_unit_id = :products_quantity_unit_id
                                        ");
        $Qstatus->bindValue(':products_quantity_unit_id',(int)$oID );
        $Qstatus->execute();

        $status = $Qstatus->fetch();

        $remove_status = true;
        if ($oID == DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID) {
          $remove_status = false;
          $OSCOM_MessageStack->add(ERROR_REMOVE_DEFAULT_PRODUCTS_UNIT_QUANTITY_STATUS, 'error');
        } elseif ($status['count'] > 0) {
          $remove_status = false;
          $OSCOM_MessageStack->add(ERROR_STATUS_USED_IN_PRODUCTS_UNIT_QUANTITY, 'error');
        } 
        break;      
    }
  }

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_unit.png', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
  <span class="pull-right"><?php echo '<a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_order_status.gif', IMAGE_INSERT) . '</a>'; ?></span>
<?php
  }
?>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $products_quantity_unit_query_raw = "select *
                                       from products_quantity_unit
                                       where language_id = '" . (int)$_SESSION['languages_id'] . "' 
                                       order by products_quantity_unit_id
                                      ";
  $products_quantity_unit_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_quantity_unit_query_raw, $products_quantity_unit_query_numrows);
  
  $products_quantity_unit_query = osc_db_query($products_quantity_unit_query_raw);
  
  while ($products_quantity_unit = osc_db_fetch_array($products_quantity_unit_query)) {
    if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $products_quantity_unit['products_quantity_unit_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
      $oInfo = new objectInfo($products_quantity_unit);
    }

    if (isset($oInfo) && is_object($oInfo) && ($products_quantity_unit['products_quantity_unit_id'] == $oInfo->products_quantity_unit_id)) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->products_quantity_unit_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $products_quantity_unit['products_quantity_unit_id']) . '\'">' . "\n";
    }

    if (DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID == $products_quantity_unit['products_quantity_unit_id']) {
      echo '                <td class="dataTableContent"><strong>' . $products_quantity_unit['products_quantity_unit_title'] . ' (' . TEXT_DEFAULT . ')</strong></td>' . "\n";
    } else {
      echo '                <td class="dataTableContent">' . $products_quantity_unit['products_quantity_unit_title'] . '</td>' . "\n";
    }
?>
                <td class="dataTableContent" align="right">
<?php

        if ($products_quantity_unit['products_quantity_unit_id'] > 1) {
                  echo '<a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $products_quantity_unit['products_quantity_unit_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
        }
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $products_quantity_unit['products_quantity_unit_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($oInfo) && is_object($oInfo) && ($products_quantity_unit['products_quantity_unit_id'] == $oInfo->products_quantity_unit_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $products_quantity_unit['products_quantity_unit_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $products_quantity_unit_split->display_count($products_quantity_unit_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS_QUANTITY_UNIT_STATUS); ?></td>
                    <td class="smallText" align="right"><?php echo $products_quantity_unit_split->display_links($products_quantity_unit_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status_products_quantity_unit', 'products_quantity_unit.php', 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_PRODUCTS_QUANTITY_UNIT_INTRO);

      $products_quantity_unit_inputs_string = '';
      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $products_quantity_unit_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('products_quantity_unit_title[' . $languages[$i]['id'] . ']');
      }

      $contents[] = array('text' => '<br />' . TEXT_INFO_PRODUCTS_UNIT_QUANTITY_NAME . $products_quantity_unit_inputs_string);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_PRODUCTS_QUANTITY_UNIT . '</strong>');

      $contents = array('form' => osc_draw_form('status_products_quantity_unit', 'products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->products_quantity_unit_id  . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);

      $products_quantity_unit_inputs_string = '';
      $languages = osc_get_languages();

     for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $products_quantity_unit_inputs_string .= '<br />' .osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('products_quantity_unit_title[' . $languages[$i]['id'] . ']', osc_get_products_quantity_unit_title($oInfo->products_quantity_unit_id, $languages[$i]['id']));
      }

      $contents[] = array('text' => '<br />' . TEXT_INFO_PRODUCTS_UNIT_QUANTITY_NAME .  $products_quantity_unit_inputs_string);
      if (DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID != $oInfo->products_quantity_unit_id) $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->products_quantity_unit_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;
       
      
      
      
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_DELETE . '</strong>');

      $contents = array('form' => osc_draw_form('status_products_quantity_unit', 'products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->products_quantity_unit_id  . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $oInfo->products_quantity_unit_title . '</strong>');


      if ($remove_status) {
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->products_quantity_unit_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      } else {
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('products_quantity_unit.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->products_quantity_unit_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      }
    break;
    default:

      break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

