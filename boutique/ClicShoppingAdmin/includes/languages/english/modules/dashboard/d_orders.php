<?php
/**
 * d_orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_ORDERS_TITLE', 'Orders');
define('MODULE_ADMIN_DASHBOARD_ORDERS_DESCRIPTION', 'Show the latest orders');
define('MODULE_ADMIN_DASHBOARD_ORDERS_TOTAL', 'Total');
define('MODULE_ADMIN_DASHBOARD_ORDERS_DATE', 'Date');
define('MODULE_ADMIN_DASHBOARD_ORDERS_ORDER_STATUS', 'Status');
define('MODULE_ADMIN_DASHBOARD_ORDERS_ORDER_ACTION','Actions');
define('MODULE_ADMIN_DASHBOARD_ORDERS_ODOO_STATUS','Odoo');

?>
