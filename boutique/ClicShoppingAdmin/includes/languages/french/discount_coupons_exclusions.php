<?php
/**
 * discount_coupons_exclusions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Gestion des exclusions pour le coupon : %s');
define('HEADING_TITLE_VIEW_MANUAL', 'Click here to read the Discount Coupon Codes manual for help editing coupons.');
if( isset( $_GET['type'] ) && $_GET['type'] != '' ) {
	switch( $_GET['type'] ) {
		//category exclusions
		case 'categories':
			$heading_available = 'Ce coupon peut être appliqué à ce produit dans cette catégorie.';
			$heading_selected = 'Ce coupon ne peut pas être appliqué à ce produit dans cette catégorie.';
			break;
		//end category exclusions
		//manufacturer exclusions
		case 'manufacturers':
			$heading_available = 'Ce coupon peut être appliqué à ce produit dans cette catégorie pour cette marque.';
			$heading_selected = 'Ce coupon ne peut pas être appliqué à ce produit dans cette catégorie pour cette marque.';
			break;
		//end manufacturer exclusions
    //customer exclusions
		case 'customers':
			$heading_available = 'Ce coupon peut être utilisé pour ces clients.';
			$heading_selected = 'Ce coupon ne peut pas être utilisé pour ces clients.';
			break;
		//end customer exclusions
		//product exclusions
		case 'products':
      $heading_available = 'Ce coupon peut être utilisé pour ces produits.';
			$heading_selected = 'Ce coupon ne peut pas être utilisé pour ces produits.';
			break;
		//end product exclusions
    //shipping zone exclusions
    case 'zones' :
      $heading_available = 'Ce coupon peut être utilisé pour cette zone d\'expédition.';
      $heading_selected = 'Ce coupon ne peut pas être utilisé pour cette zone d\'expédition.';
      break;
    //end zone exclusions
	}
}
define('HEADING_AVAILABLE', $heading_available);
define('HEADING_SELECTED', $heading_selected);

define('TEXT_SAVE','Sauvegarder');
define('TEXT_REMOVE','Tout surpprimer');
define('TEXT_CHOOSE_ALL','tout choisir ');

define('MESSAGE_DISCOUNT_COUPONS_EXCLUSIONS_SAVED', 'Exclusion sauvegardee.');

define('ERROR_DISCOUNT_COUPONS_NO_COUPON_CODE', 'Aucun coupon sélectionné.' );
define('ERROR_DISCOUNT_COUPONS_INVALID_TYPE', 'Il n\'est pas possible de créer une exclusion pour cette demande.');
define('ERROR_DISCOUNT_COUPONS_SELECTED_LIST', 'l y a une erreur concernant l\'exlusion du '.$_GET['type'].'.');
define('ERROR_DISCOUNT_COUPONS_ALL_LIST', 'Il y a une erreur concernant la disponibilit� du  '.$_GET['type'].'.');
define('ERROR_DISCOUNT_COUPONS_SAVE', 'Erreur lors de la sauvegarde concernant de cette nouvelle exclusion.');

?>
