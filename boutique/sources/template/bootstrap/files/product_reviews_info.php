<?php
/*
 * products_reviews_info.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

// ---------------------- 
// --- Image   -----  
// ---------------------- 
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>

<div class="contentContainer">
  <div>
    <span class="pull-right"><?php echo sprintf(TEXT_REVIEW_DATE_ADDED, $date_added); ?></span>
    <h3><?php echo $products_name ?></h3>
    <h3><?php echo sprintf(TEXT_REVIEW_BY, osc_output_string_protected($customers_name)); ?></h3>
  </div>
  <div class="pull-right" style="width: <?php echo SMALL_IMAGE_WIDTH+150; ?>px; text-align: center;">
    <div>
      <?php echo $products_image; ?>
      <div class="clearfix"></div>
      <div>
<?php
  echo  TEXT_PRICE . ' ' . $product_price .'<br />';
  echo $button_small_view_details;
?>
      </div>
    </div>
  </div>
<?php
// ---------------------- 
// --- Review   -----  
// ---------------------- 
?>
  <div class="contentText">
    <?php echo osc_break_string(nl2br(osc_output_string_protected($reviews_text)), 60, '-<br />') . '<br /><br /><i>' . osc_draw_stars($reviews_rating), sprintf(TEXT_OF_5_STARS, $products['reviews_rating']) . '</i>'; ?>
  </div>
<?php
// ---------------------- 
// --- button   -----  
// ---------------------- 
?>
  <div class="buttonSet">
  <br />
  <br />
    <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td class="main"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('product_reviews.php', osc_get_all_get_params(array('reviews_id'))),'info'); ?>
        </td>
        <td class="main" align="right"><?php echo osc_draw_button(IMAGE_BUTTON_WRITE_REVIEW, null, osc_href_link('product_reviews_write.php', osc_get_all_get_params(array('reviews_id'))), 'success'); ?>
        </td>
      </tr>
    </table></td>
  </div>
</div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>