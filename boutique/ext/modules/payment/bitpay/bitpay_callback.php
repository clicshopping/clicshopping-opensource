<?php
/*
   * bitpay_callback.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @copyright Portions Copyright 2003 bit-pay
   * @license GNU Public License V2.0
   * @version $Id:
*/
  chdir('../../../../');
  require('includes/application_top.php');

  require ('/ext/modules/payment/bitpay/bp_lib.php');

  $response = bpVerifyNotification(MODULE_PAYMENT_BITPAY_APIKEY);

  if (is_string($response)) {
    bpLog( 'bitpay callback error: $response');
  } else {

    $order_id = $response['posData'];
    switch($response['status']) {
    case 'paid':
      case 'confirmed':
      case 'complete':
      osc_db_query("update orders
                    set orders_status = " . MODULE_PAYMENT_BITPAY_PAID_STATUS_ID . "
                    where orders_id = " . intval($order_id)
                   );
      break;
    case 'invalid':
    case 'expired':
      break;
    case 'new':
    	break;
        default:
            bpLog('INFO: Receieved unknown IPN status of ' . $response['status'] . ' for order_id = ' . $order_id);
            break;
    }
  }
?>
