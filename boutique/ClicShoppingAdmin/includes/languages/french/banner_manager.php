<?php
/**
 * banner_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

// FCKeditor
define('ALERTE_NOTICE_IMAGE', 'Ne pas mettre plus d\'une image dans chaque section.');
define('NOTICE_IMAGE', 'Si une image existe déjà, veuillez d\'abord la sélectionner avant d\'en choisir une autre.');
define('TEXT_DELETE_IMAGE', 'Supprimer l\'image actuellement visible du serveur');

define('HEADING_TITLE', 'Gestionnaire de bannières');
define('HEADING_TITLE_SEARCH','Rechercher');

define('TABLE_HEADING_BANNERS', 'Bannières Catalogue');
define('TABLE_HEADING_BANNERS_ADMIN', 'Bannières Admin');
define('TABLE_HEADING_GROUPS', 'Groupes');
define('TABLE_HEADING_STATISTICS', 'Affichages / Clics');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_CUSTOMERS_GROUP','Groupes Client');
define('TABLE_HEADING_LANGUAGE','langues');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_BANNERS_TITLE', 'Titre bannière catalogue:');
define('TEXT_BANNERS_TITLE_ADMIN', 'Titre bannière administration:');
define('TEXT_BANNERS_URL', 'URL de redirection :');
define('TEXT_BANNERS_GROUP', 'Groupe :');
define('TEXT_BANNERS_TARGET', 'Réaction au clic :');
define('TEXT_BANNERS_SAME_WINDOWS','Même fenêtre');
define('TEXT_BANNERS_NEW_WINDOWS','Nouvelle fenêtre');
define('TEXT_BANNERS_NEW_GROUP', 'Nouveau groupe :');

define('TEXT_BANNERS_CUSTOMERS_GROUP','Groupe de client pour affichage');
define('TEXT_BANNERS_LANGUAGE','Langue d\'affichage');
define('TEXT_ALL_LANGUAGES','Toutes les langues');
define('NORMAL_CUSTOMER','Client normal');
define('TEXT_ALL_CUSTOMERS','Clients Normaux');

define('TEXT_BANNERS_IMAGE', 'Image de la bannière');
define('TEXT_BANNERS_IMAGE_LOCAL', 'Image sur serveur :');
define('TEXT_BANNERS_IMAGE_TARGET', 'Dossier de sauvegarde :');
define('TEXT_BANNERS_IMAGE_DELETE', 'Ne plus afficher d\'image sur cette bannière');
define('TEXT_BANNERS_HTML_TEXT', 'Code HTML :');
define('TEXT_BANNERS_EXPIRES_ON', 'Expire le :');
define('TEXT_BANNERS_OR_AT', 'ou');
define('TEXT_BANNERS_IMPRESSIONS', 'nombres d\'impressions :');
define('TEXT_BANNERS_SCHEDULED_AT', 'Planifié le :');
define('TEXT_BANNERS_BANNER_NOTE', '<li>Veuillez ne pas saisir l\'utilisation d\'une image lors d\'une utilisation d\'un code HTML..</li>');
define('TEXT_BANNERS_INSERT_NOTE', '<li>Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li><li>Les images ne sont pas prioritaires dans le cas d\'utilisation d\'un code HTML</li>');
define('TEXT_BANNERS_EXPIRCY_NOTE', '<u>Date d\'expiration</u> :<ul><li>Veuillez saisir un seul des 2 champs.</li><li>Pour laisser la bannière active, sans expiration automatique, laisser ces champs vides.</li></ul>');
define('TEXT_BANNERS_SCHEDULE_NOTE', '<u>Date de planification</u> :<ul><li>Si la date de planification est précisée, la bannière ne sera pas affichée avant la date prévue.</li><li>Toutes les bannières planifiées seront marquées inactives jusqu\'à ce que la date de planification soit atteinte.</li><li>Les bannnières sont automatiquement mises à jour (la partie catalogue de la boutique gère cet élément), il est inutile de changer le statut de la bannière après son enregistrement.</li></ul>');

define('TEXT_BANNERS_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_BANNERS_SCHEDULED_AT_DATE', 'Planifié à: <strong>%s</strong>');
define('TEXT_BANNERS_EXPIRES_AT_DATE', 'Expire le : <strong>%s</strong>');
define('TEXT_BANNERS_EXPIRES_AT_IMPRESSIONS', 'Expire au bout de : <strong>%s</strong> affichages');
define('TEXT_BANNERS_STATUS_CHANGE', 'Changement du statut : %s');

define('TEXT_BANNERS_DATA', 'D<br />A<br />T<br />A');
define('TEXT_BANNERS_LAST_3_DAYS', 'Les 3 derniers jours');
define('TEXT_BANNERS_BANNER_VIEWS', 'Nombre d\'affichages');
define('TEXT_BANNERS_BANNER_CLICKS', 'Nombre de clics');

define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer cette bannière ?');
define('TEXT_INFO_DELETE_IMAGE', 'Supprimer l\'image de la bannière');

define('SUCCESS_BANNER_INSERTED', 'Succès : La bannière a été insérée.');
define('SUCCESS_BANNER_UPDATED', 'Succès : La bannière a été mise à jour.');
define('SUCCESS_BANNER_REMOVED', 'Succès : La bannière a été supprimée.');
define('SUCCESS_BANNER_STATUS_UPDATED', 'Succès : Le statut de la bannière a été mis à jour.');

define('ERROR_BANNER_TITLE_REQUIRED', 'Erreur : Titre de bannière requis.');
define('ERROR_BANNER_GROUP_REQUIRED', 'Erreur : Groupe de bannière requis.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire cible n\'existe pas: %s');
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Erreur : Le fichier n\'est pas mode écriture : %s');
define('ERROR_IMAGE_DOES_NOT_EXIST', 'Erreur : L\'image n\'existe pas.');
define('ERROR_IMAGE_IS_NOT_WRITEABLE', 'Erreur : L\'image ne peut pas être supprimée.');
define('ERROR_UNKNOWN_STATUS_FLAG', 'Erreur : Statut flag inconnu.');

define('ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire de bannières n\'existe pas. Créer un répertoire \'graphs\' dans \'images\'.');
define('ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE', 'Erreur : Le répertoire de bannières n\'est pas autorisé en écriture.');

define('TITLE_BANNERS_GENERAL', 'Informations générales');
define('TITLE_BANNERS_GROUPE', 'Groupe');
define('TITLE_BANNERS_DATE', 'Date');
define('TITLE_BANNERS_IMAGE', 'Image bannière');
define('TITLE_BANNERS_HTML', 'Code HTML d\'une bannière');

define('TITLE_AIDE_BANNERS_IMAGE', 'Note sur l\'image d\'une bannière');
define('TITLE_AIDE_BANNERS_DATE', 'Note sur l\'utilisation des dates d\'une bannière');
define('TITLE_AIDE_BANNERS_HTML_TEXT', 'Note sur le code HTML d\'une bannière');

define('ICON_VIEW_BANNER', 'Voir la bannière');

define('SET_INACTIVE','Inactif');
define('SET_ACTIVE','Actif');
?>