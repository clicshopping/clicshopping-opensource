<?php
/*
 * xml_rpc_create_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// update newsletter
  if ($newsletter == 1) $newsletter = 0;

// **********************************
// write data
// **********************************
  $values = array (
                    "ref" => new xmlrpcval('WebStore - ' . $customer_id, "string"),
                    "comment" => new xmlrpcval('Website Registration - Customer B2C', "string"),
                    "name"    => new xmlrpcval( $lastname . ' ' . $firstname, "string"),
                    "email"  => new xmlrpcval($email_address, "string"),
                    "tz"      => new xmlrpcval("Europe/Paris", "string"),
                    "opt_out"  => new xmlrpcval($newsletter, "double"),
                  );
  $OSCOM_ODOO->createOdoo($values, "res.partner");

?>