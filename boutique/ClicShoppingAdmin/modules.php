<?php
/*
 * modules.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require('includes/application_top.php');
     
  $set = (isset($_GET['set']) ? $_GET['set'] : '');

  $modules = $cfgModules->getAll();

  if (empty($set) || !$cfgModules->exists($set)) {
    $set = $modules[0]['code'];
  }

  $module_type = $cfgModules->get($set, 'code');
  $module_directory = $cfgModules->get($set, 'directory');
  $module_language_directory = $cfgModules->get($set, 'language_directory');
  $module_key = $cfgModules->get($set, 'key');
  define('HEADING_TITLE', $cfgModules->get($set, 'title'));
  $template_integration = $cfgModules->get($set, 'template_integration');
  $language_template_module_directory = $cfgModules->get($set, 'languageTemplateModuleDirectory');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'save':
          foreach( $_POST['configuration'] as $key => $value ) {
// Start Dynamic Template System              
          if((is_array($value)) && (!empty($value))){

            $pages = '';
            $count = count($value);
            for($i=0 ; $i<$count; $i++){

              $pages = "$pages$value[$i]";


              $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                              set configuration_value = :configuration_value
                                              where configuration_key = :configuration_key
                                            ');
              $Qupdate->bindValue(':configuration_value', $pages);
              $Qupdate->bindValue(':configuration_key', $key);
              $Qupdate->execute();

// END Dynamic Template System
            }
          } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                              set configuration_value = :configuration_value
                                              where configuration_key = :configuration_key
                                            ');
            $Qupdate->bindValue(':configuration_value', $value);
            $Qupdate->bindValue(':configuration_key', $key);
            $Qupdate->execute();

          }
        }

//clear cache
        osc_cache_clear('configuration.cache');

        osc_redirect(osc_href_link('modules.php', 'set=' . $set . '&module=' . $_GET['module']));
        break;
      case 'install':
      case 'remove':
        $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
        $class = basename($_GET['module']);
        if (file_exists($module_directory . $class . $file_extension)) {
          include($module_directory . $class . $file_extension);
          $module = new $class;
          if ($action == 'install') {
            if ($module->check() > 0) { // remove module if already installed
              $module->remove();
            }

            $module->install();

            $modules_installed = explode(';', constant($module_key));

            if (!in_array($class . $file_extension, $modules_installed)) {
              $modules_installed[] = $class . $file_extension;
            }

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                           set configuration_value = :configuration_value
                                           where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_value', implode(';', $modules_installed));
            $Qupdate->bindValue(':configuration_key', $module_key);
            $Qupdate->execute();

//clear cache
            osc_cache_clear('configuration.cache');

            osc_redirect(osc_href_link('modules.php', 'set=' . $set . '&module=' . $class));
          } elseif ($action == 'remove') {
            $module->remove();

            $modules_installed = explode(';', constant($module_key));
            if (in_array($class . $file_extension, $modules_installed)) {
              unset($modules_installed[array_search($class . $file_extension, $modules_installed)]);
            }

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                           set configuration_value = :configuration_value
                                           where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_value', implode(';', $modules_installed));
            $Qupdate->bindValue(':configuration_key', $module_key);
            $Qupdate->execute();

//clear cache
            osc_cache_clear('configuration.cache');

            osc_redirect(osc_href_link('modules.php', 'set=' . $set));
          }
        }

//clear cache
        osc_cache_clear('configuration.cache');

        osc_redirect(osc_href_link('modules.php', 'set=' . $set . '&module=' . $class));
        break;
    }
  }

  require('includes/header.php');
?>

  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_unit.png', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>

<?php
  if (isset($_GET['list'])) {
    echo '            <span class="pull-right"><a href="' . osc_href_link('modules.php', 'set=' . $set) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a></span>';
  } else {
    echo '            <span class="pull-right"><a href="' . osc_href_link('modules.php', 'set=' . $set . '&list=new') . '">' . osc_image_button('button_install.png', IMAGE_MODULE_INSTALL) . '</a></span>';
  }
?>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MODULES; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SORT_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $modules_installed = (defined($module_key) ? explode(';', constant($module_key)) : array());
  $new_modules_counter = 0;

  $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
  $directory_array = array();
  if ($dir = @dir($module_directory)) {
    while ($file = $dir->read()) {
      if (!is_dir($module_directory . $file)) {
        if (substr($file, strrpos($file, '.')) == $file_extension) {
          if (isset($_GET['list']) && ($_GET['list'] = 'new')) {
            if (!in_array($file, $modules_installed)) {
              $directory_array[] = $file;
            }
          } else {
            if (in_array($file, $modules_installed)) {
              $directory_array[] = $file;
            } else {
              $new_modules_counter++;
            }
          }
        }
      }
    }
    sort($directory_array);
    $dir->close();
  }

  $installed_modules = array();
  for ($i=0, $n=sizeof($directory_array); $i<$n; $i++) {
    $file = $directory_array[$i];

    if (file_exists($module_language_directory . $_SESSION['language'] . '/modules/' . $module_type . '/' . $file) ) {
      include($module_language_directory . $_SESSION['language'] . '/modules/' . $module_type . '/' . $file);
    } else {
      include(DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/languages/' . $_SESSION['language'] . '/modules/' . $module_type . '/' . $file);
    }

    include($module_directory . $file);

    $class = substr($file, 0, strrpos($file, '.'));
    if (class_exists($class)) {
      $module = new $class;
      if ($module->check() > 0) {
        if (($module->sort_order > 0) && !isset($installed_modules[$module->sort_order])) {
          $installed_modules[$module->sort_order] = $file;
        } else {
          $installed_modules[] = $file;
        }
      }

      if ((!isset($_GET['module']) || (isset($_GET['module']) && ($_GET['module'] == $class))) && !isset($mInfo)) {
        $module_info = array('code' => $module->code,
                             'title' => $module->title,
                             'description' => $module->description,
                             'status' => $module->check(),
                             'signature' => (isset($module->signature) ? $module->signature : null),
                             'api_version' => (isset($module->api_version) ? $module->api_version : null));

        $module_keys = $module->keys();

        $keys_extra = array();

        for ($j=0, $k=sizeof($module_keys); $j<$k; $j++) {

          $keyValue = $OSCOM_PDO->prepare('select configuration_title,
                                                  configuration_value,
                                                  configuration_description,
                                                  use_function,
                                                  set_function
                                           from :table_configuration
                                           where configuration_key = :configuration_key
                                          ');
          $keyValue->bindValue(':configuration_key', $module_keys[$j]);
          $keyValue->execute();

          $key_value = $keyValue->fetch();

          $keys_extra[$module_keys[$j]]['title'] = $key_value['configuration_title'];
          $keys_extra[$module_keys[$j]]['value'] = $key_value['configuration_value'];
          $keys_extra[$module_keys[$j]]['description'] = $key_value['configuration_description'];
          $keys_extra[$module_keys[$j]]['use_function'] = $key_value['use_function'];
          $keys_extra[$module_keys[$j]]['set_function'] = $key_value['set_function'];
        }

        $module_info['keys'] = $keys_extra;

        $mInfo = new objectInfo($module_info);
      }

      if (isset($mInfo) && is_object($mInfo) && ($class == $mInfo->code) ) {
        if ($module->check() > 0) {
          echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('modules.php', 'set=' . $set . '&module=' . $class . '&action=edit') . '\'">' . "\n";
        } else {
          echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
        }
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('modules.php', 'set=' . $set . (isset($_GET['list']) ? '&list=new' : '') . '&module=' . $class) . '\'">' . "\n";
      }
?>
                <td class="dataTableContent"><?php echo $module->title; ?></td>
                <td class="dataTableContent" align="center"><?php if (in_array($module->code . $file_extension, $modules_installed) && is_numeric($module->sort_order)) echo $module->sort_order; ?></td>
                <td class="dataTableContent" align="right">
<?php
                  if ($module->check() > 0) { echo '<a href="' . osc_href_link('modules.php', 'set=' . $set . '&module=' . $class . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' . osc_draw_separator('pixel_trans.gif', '6', '16') . '<a href="' . osc_href_link('modules.php', 'set=' . $set . '&module=' . $class . '&action=remove') . '">' . osc_image(DIR_WS_ICONS . 'remove.gif', IMAGE_MODULE_REMOVE) . '</a>'; } else { echo '<a href="' . osc_href_link('modules.php', 'set=' . $set . '&module=' . $class . '&action=install') . '">' . osc_image(DIR_WS_ICONS . 'install.gif', IMAGE_MODULE_INSTALL) . '</a>'; }
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');

                 if (isset($mInfo) && is_object($mInfo) && ($class == $mInfo->code) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . osc_href_link('modules.php', 'set=' . $set . (isset($_GET['list']) ? '&list=new' : '') . '&module=' . $class) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    }
  }

  if (!isset($_GET['list'])) {
    ksort($installed_modules);

    $Qcheck = $OSCOM_PDO->prepare('select configuration_value
                                   from :table_configuration
                                   where configuration_key = :configuration_key
                                 ');
    $Qcheck->bindValue(':configuration_key', $module_key);
    $Qcheck->execute();

    if ($Qcheck->rowCount() > 0 ) {
      $check = $Qcheck->fetch();

      if ($check['configuration_value'] != implode(';', $installed_modules)) {

        $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value,
                                            last_modified = now()
                                        where configuration_key = :configuration_key
                                      ');
        $Qupdate->bindValue(':configuration_value', implode(';', $installed_modules));
        $Qupdate->bindValue(':configuration_key', $module_key);
        $Qupdate->execute();

      }
    } else {

      osc_db_query("insert into configuration (configuration_title,
                                                             configuration_key, 
                                                             configuration_value, 
                                                             configuration_description, 
                                                             configuration_group_id, 
                                                             sort_order, 
                                                             date_added) 
                    values ('Installed Modules', 
                            '" . $module_key . "', 
                            '" . implode(';', $installed_modules) . "', 
                            'This is automatically updated. No need to edit.', 
                            '6', 
                            '0', 
                            now())
                 ");
    }

    if ($template_integration == true) {

      $Qcheck = $OSCOM_PDO->prepare('select configuration_value
                                     from :table_configuration
                                     where configuration_key = :configuration_key
                                   ');
      $Qcheck->bindValue(':configuration_key', 'TEMPLATE_BLOCK_GROUPS');
      $Qcheck->execute();

      if ($Qcheck->rowCount() > 0 ) {
        $check =$Qcheck->fetch();

        $tbgroups_array = explode(';', $check['configuration_value']);
        if (!in_array($module_type, $tbgroups_array)) {
          $tbgroups_array[] = $module_type;
          sort($tbgroups_array);

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value,
                                            last_modified = now()
                                        where configuration_key = :configuration_key
                                      ');
          $Qupdate->bindValue(':configuration_value', implode(';', $tbgroups_array));
          $Qupdate->bindValue(':configuration_key', 'TEMPLATE_BLOCK_GROUPS');
          $Qupdate->execute();

        }
      } else {

      osc_db_query("insert into configuration (configuration_title,
                                                             configuration_key, 
                                                             configuration_value, 
                                                             configuration_description, 
                                                             configuration_group_id, 
                                                             sort_order, 
                                                             date_added) 
                    values ('Installed Modules', 
                            'TEMPLATE_BLOCK_GROUPS',
                            '" . $module_type ."', 
                            'This is automatically updated. No need to edit.', 
                            '6', 
                            '0', 
                            now())
                 ");
      }
    }
  }
?>
              <tr>
                <td colspan="3" class="smallText"><?php echo TEXT_MODULE_DIRECTORY . ' ' . $module_directory; ?></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'edit':
      $keys = '';
      foreach( $mInfo->keys as $key => $value ) {
        $keys .= '<strong>' . $value['title'] . '</strong><br />' . $value['description'] . '<br />';

        if ($value['set_function']) {
          eval('$keys .= ' . $value['set_function'] . "'" . $value['value'] . "', '" . $key . "');");
        } else {
          $keys .= osc_draw_input_field('configuration[' . $key . ']', $value['value']);
        }
        $keys .= '<br /><br />';
      }
      $keys = substr($keys, 0, strrpos($keys, '<br /><br />'));

      $heading[] = array('text' => '<strong>' . $mInfo->title . '</strong>');

      $contents = array('form' => osc_draw_form('modules', 'modules.php', 'set=' . $set . '&module=' . $_GET['module'] . '&action=save'));
      $contents[] = array('text' => $keys);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . ' <a href="' . osc_href_link('modules.php', 'set=' . $set . '&module=' . $_GET['module']) . '"></div><div class="pull-right" style="padding-right:50px; padding-top:10px;">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;
    default:
      break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
  
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require('includes/footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php 
  require('includes/application_bottom.php'); 
