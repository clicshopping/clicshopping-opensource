<?php
/**
 * session_storage.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Le répertoire de session n\'existe pas: ' . session_save_path() . '. Veuillez créer le répertoire de session pour un bon fonctionnement.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Je suis capable d\'écrire dans le répertoire de session : ' . session_save_path() . '. Les sessions ne fonctionneront pas si les bonnes permissions n\'ont pas été mises.');
?>
