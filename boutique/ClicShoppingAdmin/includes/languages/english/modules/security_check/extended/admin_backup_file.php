<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SECURITY_CHECK_EXTENDED_ADMIN_BACKUP_FILE_HTTP_200', 'I am able to access a backup file in the ' . DIR_FS_BACKUP . ' directory - please disable public access to this directory in your web server configuration.');
?>
