<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Connexion');
define('HEADING_TITLE', 'Ouvrir un compte');
define('TEXT_NEW_CUSTOMER_PRIVACY','<strong>Sécurisation des données</strong><br /><br />' . STORE_NAME . ' s\'engage à sécuriser l\'ensemble de vos données personnelles et à les garder strictement confidentiels. Nous ne vendons pas les adresses emails à des des sociétés de e-marketing.');
define('HEADING_NEW_CUSTOMER', 'Vous êtes Nouveau Client');

define('TEXT_NEW_CUSTOMER', 'Bienvenue sur ' . STORE_NAME . ' !');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', '<strong>Veuillez choisir l\'une des options qui vous sont présentées ci-dessous .</strong><br />
Vous pourrez commander rapidement nos produits, suivre vos commandes passées, éditer vos factures, utiliser le service de suivi de l\'évolution des prix pour être informé d\'un changement, donner vos avis sur nos produits, gérer votre compte et vos adresses...');

define('TEXT_LOGIN_ERROR', ' <strong>ERREUR :</strong></font> Personne ne correspond à cet email et ce mot de passe.');
define('TEXT_VISITORS_CART', ' <strong>NOTE :</strong></font> Le contenu de votre panier visiteur sera ajouté dès que vous vous serez authentifié dans notre espace client. <a href="javascript:session_win();">[Plus d\'infos]</a>');?>
