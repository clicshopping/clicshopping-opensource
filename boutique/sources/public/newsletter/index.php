<?php
/**
 * index.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: index.php 
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Newsletter</title>
<meta name="Description" content="Newsletters of ClicShopping</description" />
<style type="text/css">
a img {
border:0;
}
</style>
</head>
<body>
<p align="center"><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST']; ?>"><img src="../../image/logos/invoice/invoice_logo.png"></a></p>
<h2 align="center">Newsletters</h2>
<blockquote><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST']; ?>">Index</a></blockquote>
<?php
  $template_directory = getcwd();   
  if ($handle = opendir($template_directory)) {
  $i=1;
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != ".." && $file != "index.php" && $file != ".htaccess") {
            echo '<blockquote>'.$i.' - <a href='.$file.'>'.$file.'</a></blockquote>';
        $i++;
        }
    }
    closedir($handle);
  }
?>
</body>
</html>
