<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Nouveaux produits');
define('HEADING_TITLE', 'Nouveaux produits');

define('TEXT_DATE_ADDED', 'En vente depuis le :');
define('TEXT_MANUFACTURER', 'Marque :');
define('TEXT_PRICE', 'Prix :');
define('TEXT_PRODUCT_OPTIONS','Options possibles ');
define('TEXT_NO_PRODUCTS','Aucune nouveautés en ce moment');
?>