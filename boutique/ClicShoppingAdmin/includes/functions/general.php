<?php
/**
 * general.php 
 * @copyright Copyright 2008 - ClicShopping http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: general.php 
*/



/**
 * Get the installed version number
 *
 * @param string $v get the clicshopping_verion
 * @access public
 */

  function osc_get_version() {
    static $v;
    if (!isset($v)) {
//      $v = trim(implode('', file(DIR_FS_CATALOG . 'includes/version.php')));
      $v = PROJECT_VERSION;
    }
    return $v;
  }



/**
 * Redirect to a URL address
 *
 * @param string $url The URL address to redirect to another page or site
 * @access public
 */
////
  function osc_redirect($url) {
    global $logger;

    if ( (strstr($url, "\n") != false) || (strstr($url, "\r") != false) ) {
      osc_redirect(osc_href_link('index.php', '', 'SSL', false));
    }

    if ( strpos($url, '&amp;') !== false ) {
      $url = str_replace('&amp;', '&', $url);
    }

    header('Location: ' . $url);

    if (STORE_PAGE_PARSE_TIME == 'true') {
      if (!is_object($logger)) $logger = new logger;
      $logger->timer_stop();
    }

    exit;
  }

/**
 * Parse the data used in the html tags to ensure the tags will not break
 *
 * @param string $data, $parse
 * @return string $data, $parse
 * @access public
 */
  function osc_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }


/**
 * Parse and output a user submited value
 *
 * @param string $string The string to parse and output
 * @param array $translate An array containing the characters to parse
 * @access public
 */
  function osc_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return osc_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return osc_parse_input_field_data($string, $translate);
      }
    }
  }

/**
 * Strictly parse and output a user submited value
 *
 * @param string $string The string to strictly parse and output
 * @access public
 */
  function osc_output_string_protected($string) {
    return osc_output_string($string, false, true);
  }

/**
 * remplace les + par un espace
 *
 * @param string $string
 * @return string $string, 
 * @access public
 */
  function osc_sanitize_string($string) {
    $patterns = array ('/ +/','/[<>]/');
    $replace = array (' ', '_');
    return preg_replace($patterns, $replace, trim($string));
  }

/**
 *  remplace les espaces par un +
 *
 * @param string $string
 * @return string $string, 
 * @access public
 */

  function osc_replace_string($string) {
    $string = preg_replace("/ /", "+", $string);  
    return preg_replace("/[<>]/", '_', $string);
  }

/**
 *  Retun a customer fisrstneme and lastanme value
 *
 * @param string $customers_id
 * @return string $customers_values['customers_firstname'], $customers_values['customers_lastname']
 * @access public
 */
  function osc_customers_name($customers_id) {
    global $OSCOM_PDO;

    $Qcustomers = $OSCOM_PDO->prepare('select customers_firstname,
                                              customers_lastname
                                      from :table_customers
                                      where customers_id = :customers_id
                                    ');
    $Qcustomers->bindInt(':customers_id', (int)$customers_id);
    $Qcustomers->execute();

    $customers_values = $Qcustomers->fetch();

    return $customers_values['customers_firstname'] . ' ' . $customers_values['customers_lastname'];
  }

/**
 *  Return catagories path
 *
 * @param string $current_category_id
 * @return string $cPath_new, 
 * @access public
 */
  function osc_get_path($current_category_id = '') {
    global $cPath_array, $OSCOM_PDO;

    if ($current_category_id == '') {
      $cPath_new = implode('_', $cPath_array);
    } else {
      if (sizeof($cPath_array) == 0) {
        $cPath_new = $current_category_id;
      } else {
        $cPath_new = '';

        $QlastCategory = $OSCOM_PDO->prepare('select parent_id
                                              from :table_categories
                                              where categories_id = :categories_id
                                            ');
        $QlastCategory->bindInt(':categories_id', (int)$cPath_array[(sizeof($cPath_array)-1)]);
        $QlastCategory->execute();

        $last_category = $QlastCategory->fetch();


        $QcurrentCategory = $OSCOM_PDO->prepare('select parent_id
                                                 from :table_categories
                                                 where categories_id = :categories_id
                                                ');
        $QcurrentCategory->bindInt(':categories_id', (int)$current_category_id);
        $QcurrentCategory->execute();

        $current_category = $QcurrentCategory->fetch();

        if ($last_category['parent_id'] == $current_category['parent_id']) {
          for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        } else {
          for ($i = 0, $n = sizeof($cPath_array); $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }

        $cPath_new .= '_' . $current_category_id;

        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
      }
    }

    return 'cPath=' . $cPath_new;
  }

/**
 *  Return url
 *
 * @param string $exclude_array
 * @return string $get_url, 
 * @access public
 */
  function osc_get_all_get_params($exclude_array = '') {

    if ($exclude_array == '') $exclude_array = array();

    $get_url = '';

    foreach ($_GET as $key => $value) {
      if (($key != session_name()) && ($key != 'error') && (!in_array($key, $exclude_array))) $get_url .= $key . '=' . $value . '&';
    }

    return $get_url;
  }

/**
 * category tree
 * 
 * @param string $parent_id, $spacing, $exclude, $category_tree_array , $include_itself
 * @return string $category_tree_array, the tree of category
 * @access public
 */
  function osc_get_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $OSCOM_PDO;

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {

      $QCategory = $OSCOM_PDO->prepare('select cd.categories_name
                                        from :table_categories_description cd
                                        where cd.language_id = :language_id
                                        and cd.categories_id = :categories_id
                                       ');
      $QCategory->bindInt(':categories_id', (int)$parent_id);
      $QCategory->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $QCategory->execute();

      $category = $QCategory->fetch();

      $category_tree_array[] = array('id' => $parent_id,
                                     'text' => $category['categories_name']);
    }

    $QCategory = $OSCOM_PDO->prepare('select c.categories_id,
                                             cd.categories_name,
                                             c.parent_id
                                      from :table_categories c,
                                           :table_categories_description cd
                                      where c.categories_id = cd.categories_id
                                      and cd.language_id = :language_id
                                      and c.parent_id = :parent_id
                                      order by c.sort_order,
                                               cd.categories_name
                                      ');
    $QCategory->bindInt(':parent_id', (int)$parent_id);
    $QCategory->bindInt(':language_id',(int)$_SESSION['languages_id']);
    $QCategory->execute();

    while ($categories = $QCategory->fetch()) {
      if ($exclude != $categories['categories_id']) $category_tree_array[] = array('id' => $categories['categories_id'], 'text' => $spacing . $categories['categories_name']);
      $category_tree_array = osc_get_category_tree($categories['categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
  }
/*
  function osc_draw_products_pull_down($name, $parameters = '', $exclude = '') {
    global $currencies;

    if ($exclude == '') {
      $exclude = array();
    }

    $select_string = '<select name="' . $name . '"';

    if ($parameters) {
      $select_string .= ' ' . $parameters;
    }

    $select_string .= '>';

    $products_query = osc_db_query("select p.products_id, pd.products_name, p.products_price from products p, products_description pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' order by products_name");
    while ($products = osc_db_fetch_array($products_query)) {
      if (!in_array($products['products_id'], $exclude)) {
        $select_string .= '<option value="' . $products['products_id'] . '">' . $products['products_name'] . ' (' . $currencies->format($products['products_price']) . ')</option>';
      }
    }

    $select_string .= '</select>';

    return $select_string;
  }
*/

  function osc_format_system_info_array($array) {

    $output = '';
    foreach ($array as $section => $child) {
      $output .= '[' . $section . ']' . "\n";
      foreach ($child as $variable => $value) {
        if (is_array($value)) {
          $output .= $variable . ' = ' . implode(',', $value) ."\n";
        } else {
          $output .= $variable . ' = ' . $value . "\n";
        }
      }

    $output .= "\n";
    }
    return $output;

  }


/**
 * products options
 * 
 * @param string $options_id
 * @return string $values_values['products_options_values_name'], the value of the option name
 * @access public
 */
  function osc_options_name($options_id) {
    global $OSCOM_PDO;

    $options = $OSCOM_PDO->prepare('select products_options_name
                                    from :table_products_options
                                    where products_options_id = :products_options_id
                                    and language_id = :language_id
                                   ');
    $options->bindInt(':products_options_id', (int)$options_id);
    $options->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $options->execute();

    $options_values = $options->fetch();

    return $options_values['products_options_name'];
  }


/**
 * products options name
 * 
 * @param string $values_id
 * @return string $values_values['products_options_values_name'], the name value of the option name
 * @access public
 */
  function osc_values_name($values_id) {
    global $OSCOM_PDO;

    $values = $OSCOM_PDO->prepare('select products_options_values_name
                                    from :table_products_options_values
                                    where products_options_values_id = :products_options_values_id
                                    and language_id = :language_id
                                   ');
    $values->bindInt(':products_options_values_id', (int)$values_id);
    $values->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $values->execute();

    $values_values = $values->fetch();

    return $values_values['products_options_values_name'];
  }


/**
 * products info image
 * 
 * @param string $image, $alt, $width, $height
 * @return string $image, the image value
 * @access public
 */

  function osc_info_image($image, $alt, $width = '', $height = '') {
    if (osc_not_null($image) && (file_exists(DIR_FS_CATALOG_IMAGES . $image)) ) {
      $image = osc_image(DIR_WS_CATALOG_IMAGES . $image, $alt, $width, $height);
    } else {
      $image = osc_image(DIR_WS_CATALOG . DIR_WS_IMAGES . 'nophoto.png', TEXT_IMAGE_NONEXISTENT);
    }

    return $image;
  }


/**
 * Break a word in a string if it is longer than a specified length ($len)
 *
 * @param string $string
 * @param string $len
 * @param string $break_char
 * @access public
 */

  function osc_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }


/**
 * Country name name
 * 
 * @param string country_id
 * @return string $country['countries_name'] the country name
 * @access public
 */
  function osc_get_country_name($country_id) {
    global $OSCOM_PDO;
    
    $Qcountry = $OSCOM_PDO->prepare('select countries_name
                                     from :table_countries
                                     where countries_id = :countries_id
                                     and status = 1
                                   ');
    $Qcountry->bindInt(':countries_id',  (int)$country_id);
    $Qcountry->execute();

    $customers_group = $Qcountry->fetch();


    if ( $Qcountry->fetch() === false) {
      return $country_id;
    } else {
      return $Qcountry->value('countries_name');
    }
  }


/**
 *  Returns the zone (State/Province) name
 *  TABLES: zones
 * @param string $countries_id
 * @param string $zone_id
 * @param string $default_zone
 * @access public
 */
////
  function osc_get_zone_name($country_id, $zone_id, $default_zone) {
    global $OSCOM_PDO;
    

    $Qzone = $OSCOM_PDO->prepare('select zone_name
                                  from :table_zones
                                  where zone_country_id = :zone_country_id
                                  and zone_id = :zone_id
                                ');
    $Qzone->bindInt(':zone_country_id', (int)$country_id);
    $Qzone->bindInt(':zone_id', (int)$zone_id);
    $Qzone->execute();

    if ($Qzone->fetch() !== false) {
      return $Qzone->value('zone_name');
    } else {
      return $default_zone;
    }
  }

/**
 * Not null
 * 
 * @param string $value
 * @return stringtrue or false
 * @access public
 */
  function osc_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if ( (is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }

/**
 * Detect the user agent
 * 
 * @param string $component
 * @return string HTTP_USER_AGENT, $component, the user agent and component
 * @access public
 */
  function osc_browser_detect($component) {

    return stristr($_SERVER['HTTP_USER_AGENT'], $component);
  }

/**
 * Drop down of the class title
 * 
 * @param string $parameters, $selected
 * @return string $select_string, the drop down f the title class
 * @access public
 */
  function osc_tax_classes_pull_down($parameters, $selected = '') {
    global $OSCOM_PDO;

    $select_string = '<select ' . $parameters . '>';

    $Qclasses = $OSCOM_PDO->prepare('select tax_class_id,
                                            tax_class_title
                                     from :table_tax_class
                                     order by tax_class_title
                                   ');
    $Qclasses->execute();

    while ($classes = $Qclasses->fetch()) {
      $select_string .= '<option value="' . $classes['tax_class_id'] . '"';
      if ($selected == $classes['tax_class_id']) $select_string .= ' SELECTED';
      $select_string .= '>' . $classes['tax_class_title'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

/**
 * Drop down of the class title
 * 
 * @param string $parameters, $selected
 * @return string $select_string, the drop down of the zone name
 * @access public
 */
  function osc_geo_zones_pull_down($parameters, $selected = '') {
    global $OSCOM_PDO;

    $select_string = '<select ' . $parameters . '>';

    $QgeoZones = $OSCOM_PDO->prepare('select geo_zone_id,
                                              geo_zone_name
                                      from :table_geo_zones
                                      order by geo_zone_name
                                     ');
    $QgeoZones->execute();

    while ($zones = $QgeoZones->fetch() ) {
      $select_string .= '<option value="' . $zones['geo_zone_id'] . '"';
      if ($selected == $zones['geo_zone_id']) $select_string .= ' SELECTED';
      $select_string .= '>' . $zones['geo_zone_name'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

/**
 * Drop down of the class title
 * 
 * @param string $geo_zone_id
 * @return string $geo_zone_name the drop down of the zone name
 * @access public
 */
  function osc_get_geo_zone_name($geo_zone_id) {
    global $OSCOM_PDO;
    
    $Qzones = $OSCOM_PDO->prepare('select geo_zone_name
                                   from :table_geo_zones
                                   where geo_zone_id = :geo_zone_id
                                   ');
    $Qzones->bindInt(':geo_zone_id', (int)$geo_zone_id);
    $Qzones->execute();

    if ($Qzones->fetch() === false) {
      $geo_zone_name = $geo_zone_id;
    } else {
      $geo_zone_name = $Qzones->value('geo_zone_name');
    }

    return $geo_zone_name;
  }

/**
 * Adress format of customer
 * 
 * @param string $address_format_id, $address, $html, $boln, $eoln
 * @return string $address, the adress of the customer
 * @access public
 */
  function osc_address_format($address_format_id, $address, $html, $boln, $eoln) {
    global $OSCOM_PDO;

    $QaddressFormat = $OSCOM_PDO->prepare('select address_format as format
                                            from :table_address_format
                                            where address_format_id = :address_format_id
                                          ');
    $QaddressFormat->bindInt(':address_format_id', (int)$address_format_id);
    $QaddressFormat->execute();

    $address_format = $QaddressFormat->fetch();

    $company = osc_output_string_protected($address['company']);
    $piva = osc_output_string_protected($address['piva']);
    $cf = osc_output_string_protected($address['cf']);

    if (isset($address['firstname']) && osc_not_null($address['firstname'])) {
      $firstname = osc_output_string_protected($address['firstname']);
      $lastname = osc_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && osc_not_null($address['name'])) {
      $firstname = osc_output_string_protected($address['name']);
      $lastname = '';
    } else {
      $firstname = '';
      $lastname = '';
    }
    $street = osc_output_string_protected($address['street_address']);
    $suburb = osc_output_string_protected($address['suburb']);
    $city = osc_output_string_protected($address['city']);
    $state = osc_output_string_protected($address['state']);
    if (isset($address['country_id']) && osc_not_null($address['country_id'])) {
      $country = osc_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && osc_not_null($address['zone_id'])) {
        $state = osc_get_zone_code($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && osc_not_null($address['country'])) {
      $country = osc_output_string_protected($address['country']);
    } else {
      $country = '';
    }
    $postcode = osc_output_string_protected($address['postcode']);
    $zip = $postcode;

    if ($html) {
// HTML Mode
      $HR = '<hr />';
      $hr = '<hr />';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br />';
        $cr = '<br />';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    $streets = $street;
    if ($suburb != '') $streets = $street . $cr . $suburb;
    if ($country == '') $country = osc_output_string_protected($address['country']);
    if ($state != '') $statecomma = $state . ', ';

    $fmt = $address_format['format'];
    eval("\$address = \"$fmt\";");

    if ( (ACCOUNT_COMPANY == 'true') && (osc_not_null($company)) ) {
      $address = $company . $cr . $piva . $cr . $address;
    }

    return $address;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Function    : osc_get_zone_code
  //
  // Arguments   : country           country code string
  //               zone              state/province zone_id
  //               def_state         default string if zone==0
  //
  // Return      : state_prov_code   state/province code
  //
  // Description : Function to retrieve the state/province code (as in FL for Florida etc)
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////




/**
 * Zone code of the country
 * 
 * @param string $country, $zone, $def_state
 * @return string $state_prov_code, the zone code of the state
 * @access public
 */
  function osc_get_zone_code($country, $zone, $def_state) {
    global $OSCOM_PDO;

    $QstateProv = $OSCOM_PDO->prepare('select zone_code
                                        from :table_zones
                                        where zone_country_id = :zone_country_id
                                        and zone_id = :zone_id
                                      ');
    $QstateProv->bindInt(':zone_country_id', (int)$country);
    $QstateProv->bindInt(':zone_id', (int)$zone);

    $QstateProv->execute();

    if ($QstateProv->fetch() === false) {
      $state_prov_code = $def_state;
    }
    else {
      $state_prov_code = $QstateProv->value('zone_code');
    }
    
    return $state_prov_code;
  }




/**
 * osc_get_uprid
 * 
 * @param string $prid, $params
 * @return string $uprid, 
 * @access public
 */
  function osc_get_uprid($prid, $params) {
    $uprid = $prid;
    if ( (is_array($params)) && (!strstr($prid, '{')) ) {
      foreach ( $params as $option => $value ) {
        $uprid = $uprid . '{' . $option . '}' . $value;
      }
    }

    return $uprid;
  }

/**
 * osc_get_prid
 * 
 * @param string $uprid
 * @return string $pieces, 
 * @access public
 */
  function osc_get_prid($uprid) {
    $pieces = explode('{', $uprid);

    return $pieces[0];
  }

/**
 * the language
 * 
 * @param string 
 * @return string $languages_array, 
 * @access public
 */
  function osc_get_languages() {
    global $OSCOM_PDO;

    $Qlanguages = $OSCOM_PDO->prepare('select languages_id,
                                              name,
                                              code,
                                              image,
                                              directory
                                       from :table_languages
                                       order by sort_order
                                      ');
    $Qlanguages->execute();

    while ($languages = $Qlanguages->fetch() ) {
      $languages_array[] = array('id' => $languages['languages_id'],
                                 'name' => $languages['name'],
                                 'code' => $languages['code'],
                                 'image' => $languages['image'],
                                 'directory' => $languages['directory']);
    }

    return $languages_array;
  }

/**
 * the category name
 * 
 * @param string  $category_id, $language_id
 * @return string $category['categories_name'],  name of the categorie
 * @access public
 */
  function osc_get_category_name($category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qcategory = $OSCOM_PDO->prepare('select categories_name
                                      from :table_categories_description
                                      where categories_id = :categories_id
                                      and language_id = :language_id
                                     ');
    $Qcategory->bindInt(':categories_id', (int)$category_id);
    $Qcategory->bindInt(':language_id', (int)$language_id);
    $Qcategory->execute();

    $category = $Qcategory->fetch();

    return $category['categories_name'];
  }

/**
 * the status name
 * 
 * @param string  $orders_status_id, $language_id
 * @return string $orders_status['orders_status_name'],  name of the status
 * @access public
 */
  function osc_get_orders_status_name($orders_status_id, $language_id = '') {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QordersStatus = $OSCOM_PDO->prepare('select orders_status_name
                                          from :table_orders_status
                                          where orders_status_id = :orders_status_id
                                          and language_id = :language_id
                                         ');
    $QordersStatus->bindInt(':orders_status_id', (int)$orders_status_id);
    $QordersStatus->bindInt(':language_id', (int)$language_id);
    $QordersStatus->execute();

    $orders_status = $QordersStatus->fetch();

    return $orders_status['orders_status_name'];
  }

/**
 * the name order status
 * 
 * @param string  
 * @return string orders_status_array,  name of the order status
 * @access public
 */
  function osc_get_orders_status() {
    global $OSCOM_PDO;

    $orders_status_array = array();

    $QordersStatus = $OSCOM_PDO->prepare('select orders_status_id,
                                                 orders_status_name
                                          from :table_orders_status
                                          where language_id = :language_id
                                          order by orders_status_id
                                         ');
    $QordersStatus->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $QordersStatus->execute();

    while ($orders_status = $QordersStatus->fetch() ) {
      $orders_status_array[] = array('id' => $orders_status['orders_status_id'],
                                     'text' => $orders_status['orders_status_name']);
    }

    return $orders_status_array;
  }


/**
 * Name of the products
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_name'], name of the product
 * @access public
 */
  function osc_get_products_name($product_id, $language_id = 0) {
    global $OSCOM_PDO;

    if ($language_id == 0) $language_id = $_SESSION['languages_id'];

    $Qproduct = $OSCOM_PDO->prepare('select products_name
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);
    $Qproduct->execute();

    $product = $Qproduct->fetch();

    return $product['products_name'];
  }


/**
 * Description Name
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_description'], description name
 * @access public
 */
  function osc_get_products_description($product_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qproduct = $OSCOM_PDO->prepare('select products_description
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);
    $Qproduct->execute();

    $product = $Qproduct->fetch();

    return $product['products_description'];
  }


/**
 * url of the product
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_url'], url of the product
 * @access public
 */
  function osc_get_products_url($product_id, $language_id) {
    global $OSCOM_PDO;

   if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qproduct = $OSCOM_PDO->prepare('select products_url
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);
    $Qproduct->execute();

    $product = $Qproduct->fetch();

    return $product['products_url'];
  }


/**
 * Return the manufacturers URL in the needed language
 * 
 * @param string $manufacturer_id, $language_id
 * @return string $manufacturer['manufacturers_url'], url of manufacturers
 * @access public
 */
  function osc_get_manufacturer_url($manufacturer_id, $language_id) {
   global $OSCOM_PDO;

   if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qmanufacturer = $OSCOM_PDO->prepare('select manufacturers_url
                                         from :table_manufacturers_info
                                         where manufacturers_id = :manufacturers_id
                                         and languages_id = :language_id
                                        ');
    $Qmanufacturer->bindInt(':manufacturers_id', (int)$manufacturer_id);
    $Qmanufacturer->bindInt(':language_id', (int)$language_id);
    $Qmanufacturer->execute();

    $manufacturer = $Qmanufacturer->fetch();

    return $manufacturer['manufacturers_url'];
  }


////
// Count how many products exist in a category
// TABLES: products, products_to_categories, categories
  function osc_products_in_category_count($categories_id, $include_deactivated = false) {
    global $OSCOM_PDO;

    $products_count = 0;

    if ($include_deactivated) {

      $Qproducts = $OSCOM_PDO->prepare('select count(*) as total
                                        from :table_products p,
                                             :table_products_to_categories p2c
                                        where p.products_id = p2c.products_id
                                        and p2c.categories_id = :categories_id
                                        ');
      $Qproducts->bindInt(':categories_id', (int)$categories_id);

    } else {

      $Qproducts = $OSCOM_PDO->prepare('select count(*) as total
                                        from :table_products p,
                                             :table_products_to_categories p2c
                                        where p.products_id = p2c.products_id
                                         and p.products_status = :products_status
                                        and p2c.categories_id = :categories_id
                                        ');
      $Qproducts->bindInt(':categories_id', (int)$categories_id);
      $Qproducts->bindValue(':products_status', '1');

    }

    $Qproducts->execute();
    $products = $Qproducts->fetch();

    $products_count += $products['total'];

    $Qhilds = $OSCOM_PDO->prepare('select categories_id
                                    from :table_categories
                                    where parent_id = :parent_id
                                  ');
    $Qhilds->bindInt(':parent_id',(int)$categories_id );
    $Qhilds->execute();


    if ($Qhilds->fetch() !== false) {
      while ($childs = $Qhilds->fetch() ) {
        $products_count += osc_products_in_category_count($childs['categories_id'], $include_deactivated);
      }
    }

    return $products_count;
  }

////
// Count how many subcategories exist in a category
// TABLES: categories
  function osc_childs_in_category_count($categories_id) {
    global $OSCOM_PDO;

    $categories_count = 0;

    $Qcategories = $OSCOM_PDO->prepare('select categories_id
                                        from :table_categories
                                        where parent_id = :categories_id
                                       ');
    $Qcategories->bindInt(':categories_id', (int)$categories_id);
    $Qcategories->execute();

    while ($categories = $Qcategories->fetch() ) {
      $categories_count++;
      $categories_count += osc_childs_in_category_count($categories['categories_id']);
    }

    return $categories_count;
  }

////
// Returns an array with countries
// TABLES: countries
  function osc_get_countries($default = '') {
    global $OSCOM_PDO;

    $countries_array = array();

    if ($default) {
      $countries_array[] = array('id' => '',
                                 'text' => $default);
    }

    $Qcountries = $OSCOM_PDO->prepare('select countries_id,
                                              countries_name
                                      from countries
                                      where status = :status
                                      order by countries_name
                                     ');
    $Qcountries->bindValue(':status', '1');
    $Qcountries->execute();

    while ($countries = $Qcountries->fetch() ) {
      $countries_array[] = array('id' => $countries['countries_id'],
                                 'text' => $countries['countries_name']);
    }

    return $countries_array;
  }


////
// return an array with country zones
  function osc_get_country_zones($country_id) {
    global $OSCOM_PDO;

    $zones_array = array();

    $Qzones = $OSCOM_PDO->prepare('select zone_id,
                                          zone_name
                                   from :table_zones
                                   where zone_country_id = :zone_country_id
                                   order by zone_name
                                  ');
    $Qzones->bindValue(':zone_country_id',  (int)$country_id);
    $Qzones->execute();

    while ($zones = $Qzones->fetch() ) {
      $zones_array[] = array('id' => $zones['zone_id'],
                             'text' => $zones['zone_name']);
    }

    return $zones_array;
  }

  function osc_prepare_country_zones_pull_down($country_id = '') {

    $zones = osc_get_country_zones($country_id);

    if (sizeof($zones) > 0) {
      $zones_select = array(array('id' => '', 'text' => PLEASE_SELECT));
      $zones = array_merge($zones_select, $zones);
    } else {
      $zones = array(array('id' => '', 'text' => TYPE_BELOW));
    }

    return $zones;
  }

////
// Get list of address_format_id's
  function osc_get_address_formats() {
    global $OSCOM_PDO;

    $address_format_array = array();

    $QaddressFormat = $OSCOM_PDO->prepare('select address_format_id
                                           from :table_address_format
                                           order by address_format_id
                                         ');
    $QaddressFormat->execute();

    while ($address_format_values = $QaddressFormat-fetch() ) {
      $address_format_array[] = array('id' => $address_format_values['address_format_id'],
                                      'text' => $address_format_values['address_format_id']);
    }
    return $address_format_array;
  }

////
// Alias function for Store configuration values in the Administration Tool
  function osc_cfg_pull_down_country_list($country_id) {
    return osc_draw_pull_down_menu('configuration_value', osc_get_countries(), $country_id);
  }


// Pull drop down customer zone list
  function osc_cfg_pull_down_zone_list($zone_id) {
    return osc_draw_pull_down_menu('configuration_value', osc_get_country_zones(STORE_COUNTRY), $zone_id);
  }

// Pull drop down taxe class
  function osc_cfg_pull_down_tax_classes($tax_class_id, $key = '') {
    global $OSCOM_PDO;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));

    $QtaxClass = $OSCOM_PDO->prepare('select tax_class_id,
                                             tax_class_title
                                      from :table_tax_class
                                      order by tax_class_title
                                    ');

    $QtaxClass->execute();

    while ($tax_class =  $QtaxClass->fetch() ) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

    return osc_draw_pull_down_menu($name, $tax_class_array, $tax_class_id);
  }

/**
 * Function to read in text area in admin
 * 
 * @param string text
 * @return string zone['zone_name'], the zone name of the country
 * @access public 
 */
 
 function osc_cfg_textarea($text) {
    return osc_draw_textarea_field('configuration_value', false, 35, 5, $text);
  }

/**
 * Function select a zone
 * 
 * @param string text
 * @return string zone['zone_name'], the zone name of the country
 * @access public 
 */
  function osc_cfg_get_zone_name($zone_id) {
    global $OSCOM_PDO;
    

    $Qzone = $OSCOM_PDO->prepare('select zone_name
                                  from :table_zones
                                  where zone_id = :zone_id
                                 ');
    $Qzone->bindInt(':zone_id', (int)$zone_id );
    $Qzone->execute();

    if ($Qzone->fetch() === false) {
      return $zone_id;
    } else {
      return $Qzone->value('zone_name');
    }
  }

/**
 * Status modification of banners - Sets the status of a banner
 * 
 * @param string banners_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_banner_status($banners_id, $status) {
    if ($status == '1') {
      return osc_db_query("update banners
                           set status = '1', 
                               expires_impressions = NULL, 
                               expires_date = NULL, 
                               date_status_change = NULL 
                           where banners_id = '" . (int)$banners_id . "'
                          ");
    } elseif ($status == '0') {
      return osc_db_query("update banners
                           set status = '0', 
                           date_status_change = now() 
                           where banners_id = '" . (int)$banners_id . "'
                         ");
    } else {
      return -1;
    }
  }



/**
 * Status products - Sets the status of a product
 * 
 * @param string products_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_product_status($products_id, $status) {
    if ($status == '1') {
      return osc_db_query("update products
                           set products_status = '1', 
                               products_last_modified = now()  
                           where products_id = '" . (int)$products_id . "'
                         ");
    } elseif ($status == '0') {
      return osc_db_query("update products
                           set products_status = '0', 
                               products_last_modified = now() 
                           where products_id = '" . (int)$products_id . "'
                          ");
    } else {
      return -1;
    }
  }



/**
 * Status products reviews -  Sets the status of a reviewt
 * 
 * @param string reviews_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_reviews_status($reviews_id, $status) {
    if ($status == '1') {
      return osc_db_query("update reviews
                           set status = '1', 
                               last_modified = now() 
                           where reviews_id = '" . (int)$reviews_id . "'
                         ");
    } elseif ($status == '0') {
      return osc_db_query("update reviews
                           set status = '0', 
                               last_modified = now() 
                           where reviews_id = '" . (int)$reviews_id . "'
                         ");
    } else {
      return -1;
    }
  }	

/**
 * Status products specials products - Sets the status of a product on special
 * 
 * @param string specials_id, status
 * @return string status on or off
 * @access public 
 */
  
  function osc_set_specials_status($specials_id, $status) {
    if ($status == '1') {
      return osc_db_query("update specials
                           set status = '1', 
                               scheduled_date = NULL, 
                               expires_date = NULL, 
                               date_status_change = NULL 
                           where specials_id = '" . (int)$specials_id . "'
                          ");
    } elseif ($status == '0') {
      return osc_db_query("update specials
                          set status = '0', 
                              date_status_change = now() 
                          where specials_id = '" . (int)$specials_id . "'
                         ");
    } else {
      return -1;
    }
  }



/**
 * Wrapper function for set_time_limit(), which can't be used in safe_mode
 *
 * @param int $limit The limit to set the maximium execution time to
 * @access public
 */
////
// Sets timeout for the current script.
// Cant be used in safe mode.
  function osc_set_time_limit($limit) {
    if (!get_cfg_var('safe_mode')) {
      set_time_limit($limit);
    }
  }

////
// Alias function for Store configuration values in the Administration Tool
  function osc_cfg_select_option($select_array, $key_value, $key = '') {
    $string = '';

    for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
      $name = ((osc_not_null($key)) ? 'configuration[' . $key . ']' : 'configuration_value');

      $string .= '<br /><input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';

      if ($key_value == $select_array[$i]) $string .= ' checked="checked"';

      $string .= ' /> ' . $select_array[$i];
    }

    return $string;
  }

////
// Alias function for module configuration keys
  function osc_mod_select_option($select_array, $key_name, $key_value) {
    foreach ( $select_array as $key => $value ) {
      if (is_int($key)) $key = $value;
      $string .= '<br /><input type="radio" name="configuration[' . $key_name . ']" value="' . $key . '"';
      if ($key_value == $key) $string .= ' checked="checked"';
      $string .= ' /> ' . $value;
    }

    return $string;
  }


/**
 * Retrieve web server and database server information
 * return $data, array og php.ini information
 * @access public
 */
  function osc_get_system_information() {
    $db_query = osc_db_query("select now() as datetime");
    $db = osc_db_fetch_array($db_query);

    @list($system, $host, $kernel) = preg_split('/[\s,]+/', @exec('uname -a'), 5);

    $data = array();

    $data['clicshopping']  = array('version' => osc_get_version());

    $data['system'] = array('date' => date('Y-m-d H:i:s O T'),
                            'os' => PHP_OS,
                            'kernel' => $kernel,
                            'uptime' => @exec('uptime'),
                            'http_server' => $_SERVER['SERVER_SOFTWARE']);

    $data['mysql']  = array('version' => osc_db_get_server_info(),
                            'date' => $db['datetime']);

    $data['php']    = array('version' => PHP_VERSION,
                            'zend' => zend_version(),
                            'sapi' => PHP_SAPI,
                            'int_size' => defined('PHP_INT_SIZE') ? PHP_INT_SIZE : '',
                            'safe_mode' => (int) @ini_get('safe_mode'),
                            'open_basedir' => (int) @ini_get('open_basedir'),
                            'memory_limit' => @ini_get('memory_limit'),
                            'error_reporting' => error_reporting(),
                            'display_errors' => (int)@ini_get('display_errors'),
                            'allow_url_fopen' => (int) @ini_get('allow_url_fopen'),
                            'allow_url_include' => (int) @ini_get('allow_url_include'),
                            'file_uploads' => (int) @ini_get('file_uploads'),
                            'upload_max_filesize' => @ini_get('upload_max_filesize'),
                            'post_max_size' => @ini_get('post_max_size'),
                            'disable_functions' => @ini_get('disable_functions'),
                            'disable_classes' => @ini_get('disable_classes'),
                            'enable_dl'	=> (int) @ini_get('enable_dl'),
                            'magic_quotes_gpc' => (int) @ini_get('magic_quotes_gpc'),
                            'register_globals' => (int) @ini_get('register_globals'),
                            'filter.default'   => @ini_get('filter.default'),
                            'zend.ze1_compatibility_mode' => (int) @ini_get('zend.ze1_compatibility_mode'),
                            'unicode.semantics' => (int) @ini_get('unicode.semantics'),
                            'zend_thread_safty'	=> (int) function_exists('zend_thread_id'),
                            'extensions' => get_loaded_extensions());

    return $data;
  }

  function osc_generate_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $OSCOM_PDO;

    if (!is_array($categories_array)) $categories_array = array();

    if ($from == 'product') {

      $Qcategories = $OSCOM_PDO->prepare('select categories_id
                                          from :table_products_to_categories
                                          where products_id = :products_id
                                        ');
      $Qcategories->bindInt(':products_id',  (int)$id);
      $Qcategories->execute();

      while ($categories = $Qcategories->fetch()) {
        if ($categories['categories_id'] == '0') {
          $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
        } else {

          $Qcategory = $OSCOM_PDO->prepare('select cd.categories_name,
                                                     c.parent_id
                                              from :table_categories c,
                                                  :table_categories_description cd
                                             where c.categories_id = :categories_id
                                             and c.categories_id = cd.categories_id
                                             and cd.language_id = :language_id
                                            ');
          $Qcategory->bindInt(':categories_id', (int)$categories['categories_id'] );
          $Qcategory->bindInt(':language_id', (int)$_SESSION['languages_id'] );

          $Qcategory->execute();

          $category = $Qcategory->fetch();

          $categories_array[$index][] = array('id' => $categories['categories_id'], 'text' => $category['categories_name']);
          if ( (osc_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = osc_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
          $categories_array[$index] = array_reverse($categories_array[$index]);
        }
        $index++;
      }
    } elseif ($from == 'category') {

      $Qcategories = $OSCOM_PDO->prepare('select cd.categories_name,
                                                 c.parent_id
                                          from :table_categories c,
                                               :table_categories_description cd
                                          where c.categories_id = :categories_id
                                          and c.categories_id = cd.categories_id
                                          and cd.language_id = :language_id
                                        ');
      $Qcategories->bindInt(':categories_id',  (int)$id);
      $Qcategories->bindInt(':language_id',  (int)$_SESSION['languages_id']);
      $Qcategories->execute();

      $category = $Qcategories->fetch();

      $categories_array[$index][] = array('id' => $id, 'text' => $category['categories_name']);
      if ( (osc_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = osc_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
    }

    return $categories_array;
  }

  function osc_output_generated_category_path($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = osc_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -16) . '<br />';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -6);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

  function osc_get_generated_category_path_ids($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = osc_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '_';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br />';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -6);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

/**
 *  remove category
 * 
 * @param string $category_id
 * @return string 
 * @access public
 */
  function osc_remove_category($category_id) {
    global $OSCOM_PDO;

    $QcategoriesImage = $OSCOM_PDO->prepare('select categories_image
                                             from :table_categories
                                             where categories_id = :categories_id
                                           ');
    $QcategoriesImage->bindInt(':categories_id',  (int)$category_id);

    $QcategoriesImage->execute();

    $category_image = $QcategoriesImage->fetch();

// Controle si l'image est utilise sur une autre categorie
    $QduplicateImage = $OSCOM_PDO->prepare('select count(*) as total
                                           from :table_categories
                                           where categories_image = :categories_image
                                           ');
    $QduplicateImage->bindValue(':categories_image', osc_db_input($category_image['categories_image']));

    $QduplicateImage->execute();

    $duplicate_image = $QduplicateImage->fetch();

// Controle si l'image est utilise sur une autre categorie du blog
    $QduplicateBlogImage = $OSCOM_PDO->prepare('select count(*) as total
                                                from :table_blog_categories
                                                where blog_categories_image = :blog_categories_image
                                               ');
    $QduplicateBlogImage->bindValue(':blog_categories_image', osc_db_input($category_image['categories_image']));

    $QduplicateBlogImage->execute();

    $duplicate_blog_image = $QduplicateBlogImage->fetch();


// Controle si l'image est utilise sur les descriptions d'un blog
    $QduplicateImageBlogCategoriesDescription = $OSCOM_PDO->prepare('select count(*) as total
                                                                     from :table_blog_categories_description
                                                                     where blog_categories_description like :blog_categories_description
                                                                   ');
    $QduplicateImageBlogCategoriesDescription->bindValue(':blog_categories_description', '%'. $category_image['categories_image'].'%' );

    $QduplicateImageBlogCategoriesDescription->execute();

    $duplicate_image_blog_categories_description = $QduplicateImageBlogCategoriesDescription->fetch();

// Controle si l'image est utilise le visuel d'un produit
    $QduplicateImageProducts = $OSCOM_PDO->prepare('select count(*) as total
                                                    from :table_products
                                                    where products_image = :products_image
                                                    or products_image_zoom = :products_image_zoom
                                                   ');
    $QduplicateImageProducts->bindValue(':products_image',  osc_db_input($category_image['categories_image']) );
    $QduplicateImageProducts->bindValue(':products_image_zoom', osc_db_input($category_image['categories_image']) );

    $QduplicateImageProducts->execute();

    $duplicate_image_product = $QduplicateImageProducts->fetch();


// Controle si l'image est utilise sur les descriptions d'un produit
    $QduplicateImageProductDescription = $OSCOM_PDO->prepare('select count(*) as total
                                                                     from :table_products_description
                                                                     where products_description like :blog_categories_description
                                                                   ');
    $QduplicateImageProductDescription->bindValue(':blog_categories_description', '%'. $category_image['categories_image'].'%' );

    $QduplicateImageProductDescription->execute();

    $duplicate_image_product_description = $QduplicateImageProductDescription->fetch();

// Controle si l'image est utilisee sur une banniere
    $QduplicateImageBanners = $OSCOM_PDO->prepare('select count(*) as total
                                                    from :table_banners
                                                    where banners_image = :banners_image
                                                   ');
    $QduplicateImageBanners->bindValue(':banners_image', osc_db_input($category_image['categories_image']) );

    $QduplicateImageBanners->execute();

    $duplicate_image_banners = $QduplicateImageBanners->fetch();

// Controle si l'image est utilisee sur les fabricants
    $QduplicateImageManufacturers = $OSCOM_PDO->prepare('select count(*) as total
                                                        from :table_manufacturers
                                                        where manufacturers_image = :manufacturers_image
                                                       ');
    $QduplicateImageManufacturers->bindValue(':manufacturers_image', osc_db_input($category_image['categories_image']) );

    $QduplicateImageManufacturers->execute();

    $duplicate_image_manufacturers = $QduplicateImageManufacturers->fetch();

// Controle si l'image est utilisee sur les fournisseurs
    $QduplicateImageSuppliers = $OSCOM_PDO->prepare('select count(*) as total
                                                    from :table_suppliers
                                                    where suppliers_image = :suppliers_image
                                                   ');
    $QduplicateImageSuppliers->bindValue(':suppliers_image', osc_db_input($category_image['categories_image']) );

    $QduplicateImageSuppliers->execute();

    $duplicate_image_suppliers = $QduplicateImageSuppliers->fetch();


    if (($duplicate_image['total'] < 2) &&
        ($duplicate_blog_image== 0) &&
        ($duplicate_image_blog_categories_description== 0) && 
        ($duplicate_image_product['total'] == 0) && 
        ($duplicate_image_product_description['total'] == 0) && 
        ($duplicate_image_banners['total'] == 0) && 
        ($duplicate_image_manufacturers['total'] == 0) && 
        ($duplicate_image_suppliers['total'] == 0)) {

// delete categorie image
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_image['categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_image['categories_image']);
      }
    }

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_categories
                                    where categories_id = :categories_id
                                  ');
    $Qdelete->bindInt(':categories_id', (int)$category_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_categories_description
                                    where categories_id = :categories_id
                                  ');
    $Qdelete->bindInt(':categories_id', (int)$category_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_to_categories
                                    where categories_id = :categories_id
                                  ');
    $Qdelete->bindInt(':categories_id', (int)$category_id);
    $Qdelete->execute();


    if (USE_CACHE == 'true') {
      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $language_id = $languages[$i]['id'];
        osc_cache_clear('category_tree-' . $language_id  .'.cache');
      }
      osc_reset_cache_block('also_purchased');
      osc_reset_cache_block('upcoming');
      osc_reset_cache_block('products_cross_sell');
      osc_reset_cache_block('products_related');
    }
  }


/**
 * product : remove product
 * 
 * @param string $product_id
 * @return 
 * @access public 
 */
  function osc_remove_product($product_id) {
    global $OSCOM_PDO;

    $QproductImage = $OSCOM_PDO->prepare('select products_image,
                                                 products_image_zoom
                                                 products_image_medium
                                         from :table_products
                                         where products_id = :products_id
                                         ');
    $QproductImage->bindValue(':products_id', (int)$product_id );

    $QproductImage->execute();

    $product_image = $QproductImage->fetch();


// Controle si l'image est utilisee le visuel d'un autre produit
    $QduplicateImage = $OSCOM_PDO->prepare('select count(*) as total
                                           from :table_products
                                           where products_image = :products_image
                                           or products_image_zoom = :products_image_zoom
                                           or products_image_medium = :products_image_medium
                                          ');
    $QduplicateImage->bindValue(':products_image',osc_db_input($product_image['products_image']) );
    $QduplicateImage->bindValue(':products_image_zoom', osc_db_input($product_image['products_image_zoom']) );
    $QduplicateImage->bindValue(':products_image_medium', osc_db_input($product_image['products_image_medium']) );

    $QduplicateImage->execute();

    $duplicate_image = $QduplicateImage->fetch();

// Controle si l'image est utilisee sur une categorie
    $QduplicateImageCategories = $OSCOM_PDO->prepare('select count(*) as total
                                                     from :table_categories
                                                     where categories_image = :products_image
                                                     or categories_image = :products_image_zoom
                                                     or categories_image = :products_image_medium
                                                    ');
    $QduplicateImageCategories->bindValue(':products_image',osc_db_input($product_image['products_image']) );
    $QduplicateImageCategories->bindValue(':products_image_zoom', osc_db_input($product_image['products_image_zoom']) );
    $QduplicateImageCategories->bindValue(':products_image_medium', osc_db_input($product_image['products_image_medium']) );

    $QduplicateImageCategories->execute();

    $duplicate_image_categories = $QduplicateImageCategories->fetch();


// Controle si l'image est utiliee sur les descriptions d'un produit
    $QduplicateImageProductDescription = $OSCOM_PDO->prepare('select count(*) as total
                                                               from :table_products_description
                                                               where products_description like :products_description
                                                               or products_description like :products_description1
                                                               or products_description like :products_description2
                                                              ');
    $QduplicateImageProductDescription->bindValue(':products_description', '%'. $product_image['products_image'].'%' );
    $QduplicateImageProductDescription->bindValue(':products_description1', '%'. $product_image['products_image_zoom'].'%' );
    $QduplicateImageProductDescription->bindValue(':products_description2', '%'. $product_image['products_image_medium'].'%' );

    $QduplicateImageProductDescription->execute();

    $duplicate_image_product_description = $QduplicateImageProductDescription->fetch();


// Controle si l'image est utilisee sur une banniere
    $QduplicateImageBanners = $OSCOM_PDO->prepare('select count(*) as total
                                                     from :table_banners
                                                     where banners_image = :products_image
                                                     or banners_image = :products_image_zoom
                                                     or banners_image = :products_image_medium
                                                    ');
    $QduplicateImageBanners->bindValue(':products_image',osc_db_input($product_image['products_image']) );
    $QduplicateImageBanners->bindValue(':products_image_zoom', osc_db_input($product_image['products_image_zoom']) );
    $QduplicateImageBanners->bindValue(':products_image_medium', osc_db_input($product_image['products_image_medium']) );

    $QduplicateImageBanners->execute();

    $duplicate_image_banners = $QduplicateImageBanners->fetch();


// Controle si l'image est utilisee sur les fabricants
    $QduplicateImageManufacturers = $OSCOM_PDO->prepare('select count(*) as total
                                                         from :table_manufacturers
                                                         where manufacturers_image = :products_image
                                                         or manufacturers_image = :products_image_zoom
                                                         or manufacturers_image = :products_image_medium
                                                        ');
    $QduplicateImageManufacturers->bindValue(':products_image',osc_db_input($product_image['products_image']) );
    $QduplicateImageManufacturers->bindValue(':products_image_zoom', osc_db_input($product_image['products_image_zoom']) );
    $QduplicateImageManufacturers->bindValue(':products_image_medium', osc_db_input($product_image['products_image_medium']) );

    $QduplicateImageManufacturers->execute();

    $duplicate_image_manufacturers = $QduplicateImageManufacturers->fetch();


// Controle si l'image est utilisee sur les fabricants
    $QduplicateImageSuppliers = $OSCOM_PDO->prepare('select count(*) as total
                                                     from :table_suppliers
                                                     where suppliers_image = :products_image
                                                     or suppliers_image = :products_image_zoom
                                                     or suppliers_image = :products_image_medium
                                                    ');
    $QduplicateImageSuppliers->bindValue(':products_image',osc_db_input($product_image['products_image']) );
    $QduplicateImageSuppliers->bindValue(':products_image_zoom', osc_db_input($product_image['products_image_zoom']) );
    $QduplicateImageSuppliers->bindValue(':products_image_medium', osc_db_input($product_image['products_image_medium']) );

    $QduplicateImageSuppliers->execute();

    $duplicate_image_suppliers = $QduplicateImageSuppliers->fetch();

    if (($duplicate_image['total'] < 2) &&
       ($duplicate_image_categories['total'] == 0) &&
       ($duplicate_image_product_description['total'] == 0) &&
       ($duplicate_image_banners['total'] == 0) &&
       ($duplicate_image_manufacturers['total'] == 0) &&
       ($duplicate_image_suppliers['total'] == 0)) {
// delete product image and product image zoom
      if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_image']);
      }
      if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_image_zoom'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_image_zoom']);
      }
      if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_image_medium'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_image_medium']);
      }
    }

// delete barcode
    $barcode = $product_image['products_model'].'_'.$product_image['products_ean'];
    if (file_exists(DIR_FS_CATALOG_IMAGES . 'barcode/' . $barcode.'_barcode.' . BAR_CODE_EXTENSION)) {
       @unlink(DIR_FS_CATALOG_IMAGES . 'barcode/' . $barcode.'_barcode.' . BAR_CODE_EXTENSION);
    }

    $QproductImage = $OSCOM_PDO->prepare('select image
                                          from :table_products_images
                                          where products_id = :products_id
                                         ');
    $QproductImage->bindValue(':products_id', (int)$product_id );

    $QproductImage->execute();

    if ($QproductImage->fetch() !== false) {
      while ($product_images = $QproductImage->fetch() ) {

        $QduplicateImage = $OSCOM_PDO->prepare('select count(*) as total
                                                from :table_products_images
                                                where image = :image
                                              ');
        $QduplicateImage->bindValue(':image',$product_image['products_image'] );

        $QduplicateImage->execute();

        $duplicate_image = $QduplicateImage->fetch();

        if ($duplicate_image['total'] < 2) {
          if (file_exists(DIR_FS_CATALOG_IMAGES . $product_images['image'])) {
            @unlink(DIR_FS_CATALOG_IMAGES . $product_images['image']);
          }
        }
      }

      $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_images
                                    where products_id = :products_id
                                  ');
      $Qdelete->bindInt(':products_id', (int)$product_id);
      $Qdelete->execute();
    }

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_specials
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_featured
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_heart
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_to_categories
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_description
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_attributes
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_to_products_extra_fields
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_notifications
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_discount_coupons_to_products
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_groups
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id);
    $Qdelete->execute();

    osc_db_query("delete from customers_basket where products_id = '" . (int)$product_id . "' or products_id like '" . (int)$product_id . "{%'");
    osc_db_query("delete from customers_basket_attributes where products_id = '" . (int)$product_id . "' or products_id like '" . (int)$product_id . "{%'");


// products related master
    $QproductsMasterRelated= $OSCOM_PDO->prepare('select products_related_id
                                                  from :table_products_related
                                                  where products_related_id_master = :products_related_id_master
                                              ');
    $QproductsMasterRelated->bindInt(':products_related_id_master',(int)$product_id );

    $QproductsMasterRelated->execute();

    while ($product_related_master = $QproductsMasterRelated->fetch() ) {
      $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_related
                                    where products_related_id = :products_related_id
                                  ');
      $Qdelete->bindInt(':products_related_id', (int)$product_related_master['products_related_id']);
      $Qdelete->execute();
    }

// products related slave
    $QproductsRelatedSlave = $OSCOM_PDO->prepare('select products_related_id
                                                  from :table_products_related
                                                  where products_related_id_slave = :products_related_id_slave
                                              ');
    $QproductsRelatedSlave->bindInt(':products_related_id_slave',(int)$product_id );

    $QproductsRelatedSlave->execute();

    while ($product_related_slave = $QproductsRelatedSlave->fetch() ) {
      $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_related
                                    where products_related_id = :products_related_id
                                  ');
      $Qdelete->bindInt(':products_related_id', (int)$product_related_slave['products_related_id']);
      $Qdelete->execute();
    }

// review
    $QproductsReviews = $OSCOM_PDO->prepare('select reviews_id
                                             from :table_reviews
                                             where products_id = :products_id
                                            ');
    $QproductsReviews->bindValue(':products_id', (int)$product_id );
    $QproductsReviews->execute();


    while ($product_reviews = $QproductsReviews->fetch() ) {
      $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_reviews_description
                                    where reviews_id = :reviews_id
                                  ');
      $Qdelete->bindInt(':reviews_id', (int)$product_reviews['reviews_id']);
      $Qdelete->execute();
    }

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_reviews
                                    where products_id = :products_id
                                  ');
    $Qdelete->bindInt(':products_id', (int)$product_id );
    $Qdelete->execute();

    if (USE_CACHE == 'true') {

      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $language_id = $languages[$i]['id'];
        osc_cache_clear('category_tree-' . $language_id  .'.cache');
      }

      osc_reset_cache_block('also_purchased');
      osc_reset_cache_block('products_cross_sell');
      osc_reset_cache_block('products_related');
      osc_reset_cache_block('upcoming');
    }
  }

  function osc_remove_order($order_id, $restock = false) {
    global $OSCOM_PDO;

      $Qorder = $OSCOM_PDO->prepare('select products_id,
                                            products_quantity
                                     from :table_orders_products
                                     where orders_id = :orders_id
                                    ');
      $Qorder->bindValue(':orders_id', (int)$order_id );
      $Qorder->execute();

    if ($restock == 'on') {
      while ($order = $Qorder->fetch()) {
        osc_db_query("update products set products_quantity = products_quantity + " . $order['products_quantity'] . ",
                                          products_ordered = products_ordered - " . $order['products_quantity'] . "
                      where products_id = '" . (int)$Qorder->value('products_id') . "'
                     ");
      }
    }

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_orders
                                    where orders_id = :orders_id
                                    ');
    $Qdelete->bindInt(':orders_id', (int)$order_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_orders_products
                                    where orders_id = :orders_id
                                    ');
    $Qdelete->bindInt(':orders_id', (int)$order_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_products_groups
                                    where products_id = :products_id
                                    ');
    $Qdelete->bindInt(':products_id', (int)$Qorder->value('products_id'));
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_orders_products_attributes
                                    where orders_id = :orders_id
                                    ');
    $Qdelete->bindInt(':orders_id', (int)$order_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_orders_status_history
                                    where orders_id = :orders_id
                                    ');
    $Qdelete->bindInt(':orders_id', (int)$order_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_orders_total
                                    where orders_id = :orders_id
                                    ');
    $Qdelete->bindInt(':orders_id', (int)$order_id);
    $Qdelete->execute();

  }

  function osc_reset_cache_block($cache_block) {
    global $cache_blocks;

    for ($i=0, $n=sizeof($cache_blocks); $i<$n; $i++) {
      if ($cache_blocks[$i]['code'] == $cache_block) {
        if ($cache_blocks[$i]['multiple']) {
          if ($dir = @opendir(DIR_FS_CACHE)) {
            while ($cache_file = readdir($dir)) {
              $cached_file = $cache_blocks[$i]['file'];
              $languages = osc_get_languages();
              for ($j=0, $k=sizeof($languages); $j<$k; $j++) {
                $cached_file_unlink = preg_replace('/-language/', '-' . $languages[$j]['directory'], $cached_file);
                if (preg_match('/^' . $cached_file_unlink . '/', $cache_file)) {
                  @unlink(DIR_FS_CACHE . $cache_file);
                }
              }
            }
            closedir($dir);
          }
        } else {
          $cached_file = $cache_blocks[$i]['file'];
          $languages = osc_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $cached_file = preg_replace('/-language/', '-' . $languages[$i]['directory'], $cached_file);
            @unlink(DIR_FS_CACHE . $cached_file);
          }
        }
        break;
      }
    }
  }

/**
 * Parse file permissions to a human readable layout
 *
 * @param int $mode The file permission to parse
 * @access public
 */
  function osc_get_file_permissions($mode) {
// determine type
    if ( ($mode & 0xC000) == 0xC000) { // unix domain socket
      $type = 's';
    } elseif ( ($mode & 0x4000) == 0x4000) { // directory
      $type = 'd';
    } elseif ( ($mode & 0xA000) == 0xA000) { // symbolic link
      $type = 'l';
    } elseif ( ($mode & 0x8000) == 0x8000) { // regular file
      $type = '-';
    } elseif ( ($mode & 0x6000) == 0x6000) { //bBlock special file
      $type = 'b';
    } elseif ( ($mode & 0x2000) == 0x2000) { // character special file
      $type = 'c';
    } elseif ( ($mode & 0x1000) == 0x1000) { // named pipe
      $type = 'p';
    } else { // unknown
      $type = '?';
    }

// determine permissions
    $owner['read']    = ($mode & 00400) ? 'r' : '-';
    $owner['write']   = ($mode & 00200) ? 'w' : '-';
    $owner['execute'] = ($mode & 00100) ? 'x' : '-';
    $group['read']    = ($mode & 00040) ? 'r' : '-';
    $group['write']   = ($mode & 00020) ? 'w' : '-';
    $group['execute'] = ($mode & 00010) ? 'x' : '-';
    $world['read']    = ($mode & 00004) ? 'r' : '-';
    $world['write']   = ($mode & 00002) ? 'w' : '-';
    $world['execute'] = ($mode & 00001) ? 'x' : '-';

// adjust for SUID, SGID and sticky bit
    if ($mode & 0x800 ) $owner['execute'] = ($owner['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x400 ) $group['execute'] = ($group['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x200 ) $world['execute'] = ($world['execute'] == 'x') ? 't' : 'T';

    return $type .
           $owner['read'] . $owner['write'] . $owner['execute'] .
           $group['read'] . $group['write'] . $group['execute'] .
           $world['read'] . $world['write'] . $world['execute'];
  }

/*
 * Recursively remove a directory or a single file
 *
 * @param string $source The source to remove
 * @access public
 */
  function osc_remove($source) {
    global $OSCOM_MessageStack, $osc_remove_error;

    if (isset($osc_remove_error)) $osc_remove_error = false;

    if (is_dir($source)) {
      $dir = dir($source);
      while ($file = $dir->read()) {
        if ( ($file != '.') && ($file != '..') ) {
          if (osc_is_writable($source . '/' . $file)) {
            osc_remove($source . '/' . $file);
          } else {
            $OSCOM_MessageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source . '/' . $file), 'error');
            $osc_remove_error = true;
          }
        }
      }
      $dir->close();

      if (osc_is_writable($source)) {
        rmdir($source);
      } else {
        $OSCOM_MessageStack->add(sprintf(ERROR_DIRECTORY_NOT_REMOVEABLE, $source), 'error');
        $osc_remove_error = true;
      }
    } else {
      if (osc_is_writable($source)) {
        unlink($source);
      } else {
        $OSCOM_MessageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source), 'error');
        $osc_remove_error = true;
      }
    }
  }

////
// Output the tax percentage with optional padded decimals
  function osc_display_tax_value($value, $padding = TAX_DECIMAL_PLACES) {
    if (strpos($value, '.')) {
      $loop = true;
      while ($loop) {
        if (substr($value, -1) == '0') {
          $value = substr($value, 0, -1);
        } else {
          $loop = false;
          if (substr($value, -1) == '.') {
            $value = substr($value, 0, -1);
          }
        }
      }
    }

    if ($padding > 0) {
      if ($decimal_pos = strpos($value, '.')) {
        $decimals = strlen(substr($value, ($decimal_pos+1)));
        for ($i=$decimals; $i<$padding; $i++) {
          $value .= '0';
        }
      } else {
        $value .= '.';
        for ($i=0; $i<$padding; $i++) {
          $value .= '0';
        }
      }
    }

    return $value;
  }

// Email function
  function osc_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
    if (SEND_EMAILS != 'true') return false;

    // Instantiate a new mail object
    $message = new email(array('X-Mailer: ClicShopping'));

    // Build the text version
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

  function osc_get_tax_class_title($tax_class_id) {
    global $OSCOM_PDO;
    
    if ($tax_class_id == '0') {
      return TEXT_NONE;
    } else {
      $Qclasses = $OSCOM_PDO->prepare('select tax_class_title
                                     from :table_tax_class
                                     where tax_class_id = :tax_class_id
                                     ');
      $Qclasses->bindInt(':tax_class_id', (int)$tax_class_id);
      $Qclasses->execute();

      return $Qclasses->value('tax_class_title');
    }
  }

/**
 * Return an image type that the server supports
 *
 * @access public
 */
  function osc_banner_image_extension() {
    if (function_exists('imagetypes')) {
      if (imagetypes() & IMG_PNG) {
        return 'png';
      } elseif (imagetypes() & IMG_JPG) {
        return 'jpg';
      } elseif (imagetypes() & IMG_GIF) {
        return 'gif';
      }
    } elseif (function_exists('imagecreatefrompng') && function_exists('imagepng')) {
      return 'png';
    } elseif (function_exists('imagecreatefromjpeg') && function_exists('imagejpeg')) {
      return 'jpg';
    } elseif (function_exists('imagecreatefromgif') && function_exists('imagegif')) {
      return 'gif';
    }

    return false;
  }

////
// Wrapper function for round() for php3 compatibility
  function osc_round($value, $precision) {
    return round($value, $precision);
  }

////
// Add tax to a products price
  function osc_add_tax($price, $tax, $override = false) {
    if ( ( (DISPLAY_PRICE_WITH_TAX == 'true') || ($override == true) ) && ($tax > 0) ) {
      return $price + osc_calculate_tax($price, $tax);
    } else {
      return $price;
    }
  }

// Calculates Tax rounding the result
  function osc_calculate_tax($price, $tax) {
    return $price * $tax / 100;
  }

////
// Returns the tax rate for a zone / class
// TABLES: tax_rates, zones_to_geo_zones
  function osc_get_tax_rate($class_id, $country_id = -1, $zone_id = -1) {
    global $OSCOM_PDO;

    if ( ($country_id == -1) && ($zone_id == -1) ) {
      $country_id = STORE_COUNTRY;
      $zone_id = STORE_ZONE;
    }

    $Qtax = $OSCOM_PDO->prepare('select SUM(tax_rate) as tax_rate
                                from :table_tax_rates tr left join :table_zones_to_geo_zones za ON tr.tax_zone_id = za.geo_zone_id
                                                                left join :table_geo_zones tz ON tz.geo_zone_id = tr.tax_zone_id
                                WHERE (za.zone_country_id IS NULL OR za.zone_country_id = 0
                                                                  OR za.zone_country_id = :zone_country_id)
                                AND (za.zone_id IS NULL OR za.zone_id = 0
                                                        OR za.zone_id = :zone_id)
                                AND tr.tax_class_id = :tax_class_id
                                GROUP BY tr.tax_priority
                                ');
    $Qtax->bindInt(':zone_country_id',  (int)$country_id);
    $Qtax->bindInt(':zone_id', (int)$zone_id);
    $Qtax->bindInt(':tax_class_id',  (int)$class_id);
    $Qtax->execute();


    if ($Qtax->fetch() !== false) {
      $tax_multiplier = 0;
      while ($tax = $Qtax->fetch() ) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

////
// Returns the tax rate for a tax class
// TABLES: tax_rates
  function osc_get_tax_rate_value($class_id) {
    return osc_get_tax_rate($class_id, -1, -1);
  }

  function osc_call_function($function, $parameter, $object = '') {
    if ($object == '') {
      return call_user_func($function, $parameter);
    } else {
      return call_user_func(array($object, $function), $parameter);
    }
  }

  function osc_get_zone_class_title($zone_class_id) {
    global $OSCOM_PDO;

    if ($zone_class_id == '0') {
      return TEXT_NONE;

    } else {

      $Qclasses= $OSCOM_PDO->prepare('select geo_zone_name
                                     from :table_geo_zones
                                     where geo_zone_id = :geo_zone_id
                                    ');
      $Qclasses->bindInt(':geo_zone_id',(int)$zone_class_id );

      $Qclasses->execute();

      $classes = $Qclasses->fetch();

      return $classes['geo_zone_name'];
    }
  }

  function osc_cfg_pull_down_zone_classes($zone_class_id, $key = '') {
    global $OSCOM_PDO;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $zone_class_array = array(array('id' => '0',
                                    'text' => TEXT_NONE));

    $QzoneClass = $OSCOM_PDO->prepare('select geo_zone_id,
                                              geo_zone_name
                                       from :table_geo_zones
                                       order by geo_zone_name
                                      ');

    $QzoneClass->execute();

    while ($zone_class = $QzoneClass->fetch() ) {
      $zone_class_array[] = array('id' => $zone_class['geo_zone_id'],
                                  'text' => $zone_class['geo_zone_name']);
    }

    return osc_draw_pull_down_menu($name, $zone_class_array, $zone_class_id);
  }

// Payment module 
  function osc_cfg_pull_down_order_statuses($order_status_id, $key = '') {
    global $OSCOM_PDO;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $statuses_array = array(array('id' => '0', 'text' => TEXT_DEFAULT));


    $Qstatuses = $OSCOM_PDO->prepare('select orders_status_id,
                                              orders_status_name
                                      from :table_orders_status
                                      where language_id = :language_id
                                      order by orders_status_name
                                      ');
    $Qstatuses->bindInt(':language_id',$_SESSION['languages_id']);
    $Qstatuses->execute();

    while ($statuses = $Qstatuses->fetch() ) {
      $statuses_array[] = array('id' => $statuses['orders_status_id'],
                                'text' => $statuses['orders_status_name']);
    }

    return osc_draw_pull_down_menu($name, $statuses_array, $order_status_id);
  }

  function osc_get_order_status_name($order_status_id, $language_id = '') {
    global $OSCOM_PDO;

    if ($order_status_id < 1) return TEXT_DEFAULT;

    if (!is_numeric($language_id)) $language_id = $_SESSION['languages_id'];

    $Qstatus = $OSCOM_PDO->prepare('select orders_status_name
                                      from :table_orders_status
                                      where language_id = :language_id
                                      and orders_status_id = :orders_status_id
                                      ');
    $Qstatus->bindInt(':language_id', $_SESSION['languages_id']);
    $Qstatus->bindInt(':orders_status_id', (int)$order_status_id );
    $Qstatus->execute();

    $status = $Qstatus->fetch();

    return $status['orders_status_name'];
  }


////
// Return a random value
  function osc_rand($min = null, $max = null) {
    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

// nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
  function osc_convert_linefeeds($from, $to, $string) {
    return str_replace($from, $to, $string);
  }

  function osc_string_to_int($string) {
    return (int)$string;
  }

/**
 * Parse a category path to avoid loops with duplicate values -Parse and secure the cPath parameter values
 *
 * @param string $tmp_array 
 * @access public
 */
////

  function osc_parse_category_path($cPath) {
// make sure the category IDs are integers
    $cPath_array = array_map('osc_string_to_int', explode('_', $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i=0; $i<$n; $i++) {
      if (!in_array($cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $cPath_array[$i];
      }
    }

    return $tmp_array;
  }

  function osc_validate_ip_address($ip_address) {
    return filter_var($ip_address, FILTER_VALIDATE_IP, array('flags' => FILTER_FLAG_IPV4));
  }

  function osc_get_ip_address() {
    $ip_address = '0.0.0.0';
    $ip_addresses = array();

    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      foreach ( array_reverse(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])) as $x_ip ) {
        $x_ip = trim($x_ip);

        if (osc_validate_ip_address($x_ip)) {
          $ip_addresses[] = $x_ip;
        }
      }
    }

    if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip_addresses[] = $_SERVER['HTTP_CLIENT_IP'];
    }

    if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && !empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
      $ip_addresses[] = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }

    if (isset($_SERVER['HTTP_PROXY_USER']) && !empty($_SERVER['HTTP_PROXY_USER'])) {
      $ip_addresses[] = $_SERVER['HTTP_PROXY_USER'];
    }

    $ip_addresses[] = $_SERVER['REMOTE_ADDR'];

    foreach ( $ip_addresses as $ip ) {
      if (!empty($ip) && osc_validate_ip_address($ip)) {
        $ip_address = $ip;
        break;
      }
    }

    return $ip_address;
  }

////
// Wrapper function for is_writable() for Windows compatibility
  function osc_is_writable($file) {
    if (strtolower(substr(PHP_OS, 0, 3)) === 'win') {
      if (file_exists($file)) {
        $file = realpath($file);
        if (is_dir($file)) {
          $result = @tempnam($file, 'clic');
          if (is_string($result) && file_exists($result)) {
            unlink($result);
            return (strpos($result, $file) === 0) ? true : false;
          }
        } else {
          $handle = @fopen($file, 'r+');
          if (is_resource($handle)) {
            fclose($handle);
            return true;
          }
        }
      } else{
        $dir = dirname($file);
        if (file_exists($dir) && is_dir($dir) && osc_is_writable($dir)) {
          return true;
        }
      }
      return false;
    } else {
      return is_writable($file);
    }
  }
