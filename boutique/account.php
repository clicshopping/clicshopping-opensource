<?php
/*
 * account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('account.php', '', 'SSL'));

  $Qorders = $OSCOM_PDO->prepare('select o.orders_id,
                                       o.date_purchased,
                                       o.delivery_name,
                                       o.delivery_country,
                                       o.billing_name,
                                       o.billing_country,
                                       ot.text as order_total,
                                       s.orders_status_name
                                   from :table_orders o,
                                        :table_orders_total ot,
                                        :table_orders_status s
                                   where o.customers_id = :customers_id
                                   and o.orders_id = ot.orders_id
                                   and ot.class = :class
                                   and o.orders_status = s.orders_status_id
                                   and s.language_id = :language_id
                                   and s.public_flag = :public_flag
                                   order by orders_id desc limit 5
                                  ');
  $Qorders->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
  $Qorders->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $Qorders->bindValue(':public_flag', '1');
  $Qorders->bindValue(':class', 'ot_total');

  $Qorders->execute();

  require($OSCOM_Template->getTemplateFiles('account'));
