<?php
/**
 * currencies.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

////
// Class to handle currencies
// TABLES: currencies
  class currencies {
    var $currencies;

// class constructor
    function currencies() {
      global $OSCOM_PDO;

      $this->currencies = array();

      $Qcurrencies = $OSCOM_PDO->prepare('select code,
                                               title,
                                               symbol_left,
                                               symbol_right,
                                               decimal_point,
                                               thousands_point,
                                               decimal_places,
                                              value
                                        from :table_currencies
                                       ');
      $Qcurrencies->execute();

      while ($Qcurrencies->fetch()) {
        $this->currencies[$Qcurrencies->value('code')] = array('title' => $Qcurrencies->value('title'),
                                                               'symbol_left' => $Qcurrencies->value('symbol_left'),
                                                               'symbol_right' => $Qcurrencies->value('symbol_right'),
                                                               'decimal_point' => $Qcurrencies->value('decimal_point'),
                                                               'thousands_point' => $Qcurrencies->value('thousands_point'),
                                                               'decimal_places' => $Qcurrencies->valueInt('decimal_places'),
                                                               'value' => $Qcurrencies->valueDecimal('value'));
      }
    }

// class methods
    function format($number, $calculate_currency_value = true, $currency_type = '', $currency_value = '') {

      if (empty($currency_type)) $currency_type = $_SESSION['currency'];

      if ($calculate_currency_value == true) {
        $rate = (osc_not_null($currency_value)) ? $currency_value : $this->currencies[$currency_type]['value'];
        $format_string = $this->currencies[$currency_type]['symbol_left'] . number_format(osc_round($number * $rate, $this->currencies[$currency_type]['decimal_places']), $this->currencies[$currency_type]['decimal_places'], $this->currencies[$currency_type]['decimal_point'], $this->currencies[$currency_type]['thousands_point']) . $this->currencies[$currency_type]['symbol_right'];
// if the selected currency is in the european euro-conversion and the default currency is euro,
// the currency will displayed in the national currency and euro currency
        if ( (DEFAULT_CURRENCY == 'EUR') && ($currency_type == 'DEM' || $currency_type == 'BEF' || $currency_type == 'LUF' || $currency_type == 'ESP' || $currency_type == 'FRF' || $currency_type == 'IEP' || $currency_type == 'ITL' || $currency_type == 'NLG' || $currency_type == 'ATS' || $currency_type == 'PTE' || $currency_type == 'FIM' || $currency_type == 'GRD') ) {
          $format_string .= ' <small>[' . $this->format($number, true, 'EUR') . ']</small>';
        }
      } else {
        $format_string = $this->currencies[$currency_type]['symbol_left'] . number_format(osc_round($number, $this->currencies[$currency_type]['decimal_places']), $this->currencies[$currency_type]['decimal_places'], $this->currencies[$currency_type]['decimal_point'], $this->currencies[$currency_type]['thousands_point']) . $this->currencies[$currency_type]['symbol_right'];
      }

      return $format_string;
    }

    function calculate_price($products_price, $products_tax, $quantity = 1) {

      return osc_round(osc_add_tax($products_price, $products_tax), $this->currencies[$_SESSION['currency']]['decimal_places']) * $quantity;
    }

    function is_set($code) {
      if (isset($this->currencies[$code]) && osc_not_null($this->currencies[$code])) {
        return true;
      } else {
        return false;
      }
    }

    function get_value($code) {
      return $this->currencies[$code]['value'];
    }

    function get_decimal_places($code) {
      return $this->currencies[$code]['decimal_places'];
    }

// Formatage du prix du produit
// Add a tag after the price ex 100 euros HT or TTC
    function display_price($products_price, $products_tax, $quantity = 1) {
      global $tag, $PHP_SELF, $OSCOM_Customer, $OSCOM_DetectMobile;

// Marketing : increase the price on the mobile phone and tablet
// see shopping_cart cart class - currencies.php - order.php
      if ($PHP_SELF != 'checkout_confirmation.php') { 
        if (PRICE_MOBILE_ACCEPT == 'true') { 
          if ($OSCOM_Customer->getCustomersGroupID() == 0) {  
            if ($OSCOM_DetectMobile->isMobile() || $OSCOM_DetectMobile->isTablet()) {
              if ( (PRICE_MOBILE_POURCENTAGE != 0) || (PRICE_MOBILE_POURCENTAGE != '') ) {
                $products_price = $products_price * (1+(PRICE_MOBILE_POURCENTAGE/100));
              }
            }
          }
        }
      } // end PHP_SELF

      if ((($OSCOM_Customer->getCustomersGroupID() == '0') && (DISPLAY_PRODUCT_PRICE_VALUE_TAX == 'true')) || (($OSCOM_Customer->getCustomersGroupID()!= '0') && (DISPLAY_PRODUCT_PRICE_VALUE_TAX_PRO == 'true'))) {

      return $this->format(osc_add_tax($products_price, $products_tax) * $quantity). ' '.$tag.'';

// MAJ derniere version non fonctionnelle
//      return $this->format($this->calculate_price($products_price, $products_tax, $quantity) );
      } else {

// do not display if the price 0
        if (NOT_DISPLAY_PRICE_ZERO == 'false') {
          if ($products_price > 0) { 
            return $this->format(osc_add_tax($products_price, $products_tax) * $quantity);
          } else { 
            return '';
          } 
        } else {
            return $this->format(osc_add_tax($products_price, $products_tax) * $quantity);
        } // END NOT_DISPLAY_PRICE_ZERO
       }
     }

// Product Price per kilo calculation
// Calcul du prix du produit au kilo
    function display_price_kilo($products_price, $products_weight, $value, $products_tax, $quantity = 1) {	
      global  $tag,  $OSCOM_Customer;
      
      if (($products_weight > '0') && ($products_weight > '0') && ($value == '1')) {
        $products_price_kilo = round(($products_price / $products_weight),2);
    
        if ((($$OSCOM_Customer->getCustomersGroupID() == '0') && (DISPLAY_PRODUCT_PRICE_VALUE_TAX == 'true')) || (($OSCOM_Customer->getCustomersGroupID() != '0') && (DISPLAY_PRODUCT_PRICE_VALUE_TAX_PRO == 'true'))) {
          return $this->format(osc_add_tax($products_price_kilo, $products_tax) * $quantity). ' '.$tag.'';
        } else {
          return $this->format(osc_add_tax($products_price_kilo, $products_tax) * $quantity);
        }
      } else {
        return false;
      }
    }
  }
