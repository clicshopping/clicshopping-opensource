<?php
/*
 * contact_us.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('contact') ) {
    echo $OSCOM_MessageStack->get('contact');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Success  -----  
// ---------------------- 

  if (isset($_GET['action']) && ($_GET['action'] == 'success')) {
?>

<div class="contentContainer">
  <div class="contentText"><?php echo TEXT_SUCCESS; ?></div>
  <div class="pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, osc_href_link('index.php'), 'success', null, null); ?></div>
</div>

<?php
  } else {
// ---------------------- 
// --- Contact  -----  
// ---------------------- 
?>
  <div class="contentContainer"><?php echo $OSCOM_Template->getBlocks('modules_contact_us'); ?></div>

<?php
  }
  require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>