<?php
/*
 * stats_low_stock.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if ($_GET['page'] > 1) $rows = $_GET['page'] * 20 - 20;
  
  $products_query_raw = "select p.products_id, 
                                p.products_quantity, 
                                p.products_model, 
                                pd.products_name,
                                p.products_image,       
                                p.products_wharehouse_time_replenishment,
                                p.products_wharehouse,
                                p.products_wharehouse_row,
                                p.products_wharehouse_level_location,
                                p.products_packaging
                         from products p,
                              products_description pd
                         where p.products_id = pd.products_id 
                         and pd.language_id = '".$_SESSION['languages_id'] ."'
                         and p.products_quantity < '" .STOCK_REORDER_LEVEL ."'
                         group by pd.products_id
                        order by pd.products_name ASC";
  
  
  $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_query_raw, $products_query_numrows);


  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>



<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_customers.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
  <span style="text-align:center">
    <?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?><br />
    <?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
  </span>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>




<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="20"></td> 
                <td width="50"></td>  

                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MODEL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WAHREHOUSE_TIME_REPLENISHMENT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WHAREHOUSE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WHAREHOUSE_ROW; ?></td>
               <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WHAREHOUSE_LEVEL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_QTY_LEFT; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $products_query = osc_db_query($products_query_raw);
  while ($products = osc_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>          

          <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
            <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', '&pID=' . $products['products_id'] . '&origin=stats_products_low_stock.php?page=' . $_GET['page']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
            <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
            <td class="smallText dataTableContent">&nbsp;<?php echo  $products['products_name']; ?>&nbsp;</td>
            <td class="smallText dataTableContent">&nbsp;<?php echo  $products['products_model']; ?>&nbsp;</td>
            <td class="smallText dataTableContent">&nbsp;<?php echo  $products['products_wharehouse_time_replenishment']; ?>&nbsp;</td>
            <td class="smallText dataTableContent">&nbsp;<?php echo  $products['products_wharehouse']; ?>&nbsp;</td>
            <td class="smallText dataTableContent">&nbsp;<?php echo  $products['products_wharehouse_row']; ?>&nbsp;</td>
            <td class="smallText dataTableContent">&nbsp;<?php echo  $products['products_wharehouse_level_location']; ?>&nbsp;</td>
            <td class="smallText dataTableContent" align="center"><strong><?php echo  $products['products_quantity']; ?></strong></td>
            <td class="dataTableContent" align="right"> <?php echo '<a href="' . osc_href_link('categories.php', 'action=new_product&pID=' . $products['products_id']). '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) .'</a>'; ?></td>
          </tr>
<?php
  }
?>
          </table></td>
        </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>    
<?php
  require('includes/footer.php');
  require('includes/application_bottom.php');

    