<?php
/**
 * whos_online.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  $xx_mins_ago = (time() - 900);

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

// remove entries that have expired

  $Qdelete = $OSCOM_PDO->prepare('delete 
                                  from :table_whos_online
                                  where time_last_click = :time_last_click 
                                ');
  $Qdelete->bindValue(':time_last_click', $xx_mins_ago);
  $Qdelete->execute();

  require('includes/header.php');

// more standard use of get vars on refresh
  if(  isset($_GET['refresh'])&& is_numeric($_GET['refresh'])  ){  
    echo '<meta http-equiv="refresh" content="' . htmlspecialchars($_GET['refresh']) . ';URL=' . 'whos_online.php' . '?' . htmlspecialchars($_SERVER['QUERY_STRING']) . '">';
  }
?>
<tr>
  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
</tr>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/whos_online.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
        <tr>
          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ONLINE; ?></td>
                <td class="dataTableHeadingContent" style="text-align:center;"><?php echo TABLE_HEADING_CUSTOMER_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FULL_NAME; ?></td>
                <td class="dataTableHeadingContent" style="text-align:center;"><?php echo TABLE_HEADING_IP_ADDRESS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ENTRY_TIME; ?></td>
                <td class="dataTableHeadingContent" style="text-align:center;"><?php echo TABLE_HEADING_LAST_CLICK; ?></td>
                <td class="dataTableHeadingContent" style="width:150px;"><?php echo TABLE_HEADING_LAST_PAGE_URL; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_USER_AGENT; ?>&nbsp;</td>
              </tr>
<?php
  $QwhosOnline = $OSCOM_PDO->prepare('select distinct customer_id,
                                                      full_name,
                                                      ip_address,
                                                      time_entry,
                                                      time_last_click,
                                                      last_page_url,
                                                      session_id,
                                                      user_agent,
                                                      http_referer
                                       from :table_whos_online
                                       ');

  $QwhosOnline->execute();

  while ($whos_online = $QwhosOnline->fetch() ) {
    $time_online = (time() - $whos_online['time_entry']);
    if ((!isset($_GET['info']) || (isset($_GET['info']) && ($_GET['info'] == $whos_online['session_id']))) && !isset($info)) {
      $info = new ObjectInfo($whos_online);
    }

    if (isset($info) && ($whos_online['session_id'] == $info->session_id)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('whos_online.php', osc_get_all_get_params(array('info', 'action')) . 'info=' . $whos_online['session_id']) . '\'">' . "\n";
    }
    $ip_address = $whos_online['ip_address'];
?>
                <td class="dataTableContent"><?php echo gmdate('H:i:s', $time_online); ?></td>
                <td class="dataTableContent"><?php echo $whos_online['customer_id']; ?></td>
                <td class="dataTableContent"><?php echo $whos_online['full_name']; ?></td>
                <td class="dataTableContent"><?php echo '<a href="http://ip-lookup.net/index.php?ip='. urlencode($ip_address). '" title="Lookup" target="_blank">'. $ip_address .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo date('H:i:s', $whos_online['time_entry']); ?></td>
                <td class="dataTableContent" style="text-align:center;"><?php echo date('H:i:s', $whos_online['time_last_click']); ?></td>
                <td class="dataTableContent" style="width:150px;">
<?php
    if (preg_match('/^(.*)osCsid=[A-Z0-9,-]+[&]*(.*)/i', $whos_online['last_page_url'], $array)) {
      echo $array[1] . $array[2];
    } else {
      echo $whos_online['last_page_url']; }
?>
                </td>
                <td class="dataTableContent"><?php echo  $whos_online['user_agent']; ?></td>
              </tr>
<?php
  }
?>
              <tr>
                <td class="smallText" colspan="7"><?php echo sprintf(TEXT_NUMBER_OF_CUSTOMERS, $QwhosOnline->rowCount() ); ?></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  if (isset($info)) {
    $heading[] = array('text' => '<strong>' . TABLE_HEADING_SHOPPING_CART . '</strong>');

    if ( $info->customer_id > 0 ) {

      $Qproducts = $OSCOM_PDO->prepare('select cb.customers_basket_quantity,
                                               cb.products_id,
                                               pd.products_name
                                        from :table_customers_basket cb,
                                              :table_products_description pd
                                        where cb.customers_id = :customers_id
                                        and cb.products_id = pd.products_id
                                        and pd.language_id = :language_id
                                        
                                       ');

      $Qproducts->bindInt(':customers_id', (int)$info->customer_id );
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->execute();


      if ($Qproducts->rowCount() > 0 ) {
        $shoppingCart = new shoppingCart();

        while ( $products = $Qproducts->fetch() ) {
          $contents[] = array('text' => $products['customers_basket_quantity'] . ' x ' . $products['products_name']);

          $attributes = array();

          if ( strpos($products['products_id'], '{') !== false ) {
            $combos = array();
            preg_match_all('/(\{[0-9]+\}[0-9]+){1}/', $products['products_id'], $combos);

            foreach ( $combos[0] as $combo ) {
              $att = array();
              preg_match('/\{([0-9]+)\}([0-9]+)/', $combo, $att);

              $attributes[$att[1]] = $att[2];
            }
          }

          $shoppingCart->add_cart(osc_get_prid($products['products_id']), $products['customers_basket_quantity'], $attributes);
        }

        $contents[] = array('text' => osc_draw_separator('pixel_black.gif', '100%', '1'));
        $contents[] = array('align' => 'right', 'text'  => TEXT_SHOPPING_CART_SUBTOTAL . ' ' . $currencies->format($shoppingCart->show_total()));
      } else {
        $contents[] = array('text' => '&nbsp;');
      }
    } else {
      $contents[] = array('text' => 'N/A');
    }
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php
 require('includes/footer.php');
 require('includes/application_bottom.php');

