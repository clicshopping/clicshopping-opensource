<?php
/*
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');


//  if (strlen($_POST['keywords']) >= 3){

    $search = strtolower($_GET["q"]);
    $search = $search;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) { 
// Clients en mode B2B
// Attention !  le nbr de recherche de produit a ete limite a 10 afin d'rviter au serveur de crrer trop de requete
// Plus cette limite est grande plus les possibilite de recherche seront longue et rtendue et prendre de la charge sur le serveur
// sinon modifier cette synthaxe LIKE '%" . $_POST['keywords'] . "%'   

      $QProducts = $OSCOM_PDO->prepare('select distinct p.products_id,
                                                            p.products_status,
                                                            pd.products_name,
                                                            pd.language_id,
                                                            pg.products_group_view
                                       from :table_products p,
                                            :table_products_description pd
                                            :table_products_groups pg
                                       where p.products_status = :products_status
                                       and pg.customers_group_id = :customers_group_id
                                       and pg.products_group_view = :products_group_view
                                       and p.products_id = pd.products_id
                                       and pd.language_id = :language_id
                                       and p.products_id = pg.products_id
                                       and p.products_archive = :products_archive
                                       and products_name LIKE '%" . :search . "%'
                                       limit 10;
                                     ');
      $QProducts->bindInt(':products_status', '1');
      $QProducts->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
      $QProducts->bindValue(':products_group_view', '1');
      $QProducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $QProducts->bindValue(':products_archive', '0');
      $QProducts->bindValue(':search', $search );

    } else {  

      $QProducts = $OSCOM_PDO->prepare('select distinct p.products_id,
                                                        p.products_status,
                                                        p.products_view,
                                                        pd.products_name,
                                                        pd.language_id 
                                       from :table_products p,
                                            :table_products_description pd
                                       where p.products_status = :products_status
                                       and p.products_view = :products_view
                                       and p.products_id = pd.products_id
                                       and pd.language_id = :language_id
                                       and p.products_archive = :products_archive
                                       and products_name LIKE '%" . :search . "%'
                                       limit 10;
                                      ');
      $QProducts->bindInt(':products_status', '1');
      $QProducts->bindValue(':products_view', '1');
      $QProducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $QProducts->bindValue(':products_archive', '0');
      $QProducts->bindValue(':search', $search );

    }                    

   $QProducts->execute();

   $products = $QProducts->fetch();      

    while($data_products = mysql_fetch_assoc($products)) { 
     $products_name = $data_products['products_name'];
      echo $products_name . "\n";
    } 
