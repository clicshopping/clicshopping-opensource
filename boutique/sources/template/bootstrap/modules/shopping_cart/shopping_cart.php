<?php
/*
 Fichier Source : shopping_cart.php
 Fichier impacte :  shopping_cart.php
 Autres fichiers possible : non 
 
*/

  if ($_SESSION['cart']->count_contents() > 0) {
    $any_out_of_stock = 0;
    $products = $_SESSION['cart']->get_products();

    echo osc_draw_form('cart_quantity', osc_href_link('shopping_cart.php', 'action=update_product'));
?>
  <div class="clearfix"></div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <div class="contentText">
     <table border="0" cellspacing="2" cellpadding="2" width="100%"  class="tableShoppingCartHeading">
       <tr>
         <td align="left" class="productListing-heading" width="80"><strong></strong></td>
         <td align="center" class="productListing-heading" width="500"><strong><?php echo TABLE_HEADING_PRODUCTS; ?></strong></td>
         <td align="center" class="productListing-heading" width="170"><strong><?php echo TABLE_HEADING_QUANTITY; ?></strong></td>
         <td align="right" class="productListing-heading" width="150"><strong><?php echo TABLE_HEADING_TOTAL; ?></strong></td>
        </tr>
      </table>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <table border="0" width="100%" cellspacing="0" cellpadding="0">
<?php
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
// Push all attributes information in an array
      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        foreach($products[$i]['attributes'] as $option => $value) {
          echo osc_draw_hidden_field('id[' . $products[$i]['id'] . '][' . $option . ']', $value);

          $Qattributes = $OSCOM_PDO->prepare('select popt.products_options_name,
                                                     poval.products_options_values_name,
                                                     pa.options_values_price,
                                                     pa.price_prefix,
                                                     pa.products_attributes_reference
                                              from :table_products_options popt,
                                                   :table_products_options_values  poval,
                                                   :table_products_attributes  pa
                                              where pa.products_id = :products_id
                                              and pa.options_id = :options_id
                                              and pa.options_id = popt.products_options_id
                                              and pa.options_values_id = :options_values_id
                                              and pa.options_values_id = poval.products_options_values_id
                                              and popt.language_id = :language_id
                                              and poval.language_id = :language_id
                                              ');
          $Qattributes->bindInt(':products_id',  (int)$products[$i]['id'] );
          $Qattributes->bindInt(':options_id',  (int)$option );
          $Qattributes->bindValue(':options_values_id',  (int)$value );
          $Qattributes->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
          $Qattributes->execute();

          $attributes_values = $Qattributes ->fetch();

          $products[$i][$option]['products_options_name'] = $attributes_values['products_options_name'];
          $products[$i][$option]['options_values_id'] = $value;
          $products[$i][$option]['products_options_values_name'] = $attributes_values['products_options_values_name'];
          $products[$i][$option]['options_values_price'] = $attributes_values['options_values_price'];
          $products[$i][$option]['price_prefix'] = $attributes_values['price_prefix'];
          $products[$i][$option]['products_attributes_reference'] = $attributes_values['products_attributes_reference'];
        }
      }    
    }
 
    $products_name = NULL;
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
      echo '      <tr>';

      $products_name = '<table border="0" cellspacing="2" cellpadding="2"  class="tableShoppingCartContent">' .
                       '  <tr>' .
                       '    <td align="center" widht="100"> <a href="' . osc_href_link('shopping_cart.php', 'products_id=' . $products[$i]['id'] . '&action=remove_product') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_BUTTON_REMOVE_PRODUCT) . '</a>&nbsp;&nbsp;&nbsp;</td>' .
                       '    <td align="center"><a href="' . osc_href_link('product_info.php', 'products_id=' . $products[$i]['id']) . '">' . osc_image(DIR_WS_IMAGES . $products[$i]['image'], $products[$i]['name'], 50, 50, null, null) . '</a>&nbsp;&nbsp;&nbsp;</td>' .
                       '    <td valign="top" widht="500"><a href="' . osc_href_link('product_info.php', 'products_id=' . $products[$i]['id']) . '"><strong>' . $products[$i]['name'] . '</strong></a>';

      if (STOCK_CHECK == 'true') {
// select the good qty in B2B ti decrease the stock (see checkout_process to update stock)
        if ($OSCOM_Customer->getCustomersGroupID() != '0') {

          $QproductsQuantityCustomersGroupQuery = $OSCOM_PDO->prepare('select products_quantity_fixed_group
                                                                       from :table_products_groups
                                                                       where products_id = :products_id
                                                                       and customers_group_id =  :customers_group_id
                                                                      ');
          $QproductsQuantityCustomersGroupQuery->bindInt(':products_id',  (int)osc_get_prid($products[$i]['id']) );
          $QproductsQuantityCustomersGroupQuery->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
          $QproductsQuantityCustomersGroupQuery->execute();

          $products_quantity_customers_group = $QproductsQuantityCustomersGroupQuery->fetch();

// do the exact qty in function the customer group and product          
          $products_quantity_customers_group[$i] = $products_quantity_customers_group['products_quantity_fixed_group'];
        } else {
          $products_quantity_customers_group[$i] = 1;
        }

         $stock_check = osc_check_stock($products[$i]['id'], $products[$i]['quantity'] * $products_quantity_customers_group[$i]);

         if (osc_not_null($stock_check)) {
          $any_out_of_stock = 1;
          $products_name .= '<br />' . $stock_check .'<br />';
        }
      }

      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
         foreach($products[$i]['attributes'] as $option => $value) {
          $products_name .= '<br /><small><i> - ' . $products[$i][$option]['products_options_name'] . ' :  ' . $products[$i][$option]['products_options_values_name'] .  ' ('. $products[$i][$option]['products_attributes_reference'] .') ' . ' - ' .  $currencies->display_price($products[$i][$option]['options_values_price'],osc_get_tax_rate($products[$i]['tax_class_id']), '1') .'</i></small>';
        }
      }

      $products_name .= '    </td>' .
                        '  </tr>' .
                        '</table>';

         $button_update = osc_draw_button(IMAGE_BUTTON_UPDATE_CART, null, null,'info',null,'xs');

      echo '        <td valign="top" width="60%">' . $products_name . '</td>' .
           '         <td valign="middle" style=text-align:right;">' . osc_draw_input_field('cart_quantity[]', $products[$i]['quantity'], 'style="width: 70px;"') .'&nbsp;&nbsp;' . osc_draw_hidden_field('products_id[]', $products[$i]['id']) .'</td>' .
           '         <td valign="middle" style=text-align:left;">'. $button_update .'</span></td>' .
           '         <td align="right" valign="middle"><strong>' . $currencies->display_price($products[$i]['final_price'], osc_get_tax_rate($products[$i]['tax_class_id']), $products[$i]['quantity']) . '</strong></td>' .
           '      </tr>';
    }
?>
    </table>
    <div class=""row">
        <p class="shoppingCartSubTotal"><?php echo SUB_TITLE_SUB_TOTAL . $currencies->format($_SESSION['cart']->show_total()); ?></p>
  </div>
<?php
    if ($any_out_of_stock == 1) {
      if (STOCK_ALLOW_CHECKOUT == 'true') {
?>
    <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CAN_CHECKOUT; ?></p><br />
<?php
      } else {
?>
    <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CANT_CHECKOUT; ?></p><br />
<?php
      }
    }
?>
  </div>

  <div class="row">
      <div class="col-lg-6">
<?php
    $back = sizeof($_SESSION['navigation']->path)-2;
    if (isset($_SESSION['navigation']->path[$back])) {
      echo osc_draw_button(IMAGE_BUTTON_CONTINUE_SHOPPING, null, osc_href_link($_SESSION['navigation']->path[$back]['page'], osc_array_to_string($_SESSION['navigation']->path[$back]['get'], array('action')), $_SESSION['navigation']->path[$back]['mode']), 'info');
    }
 ?>
      </div>
      <div class="col-lg-6" style="text-align:right;"><?php echo osc_draw_button(IMAGE_BUTTON_CHECKOUT, null, osc_href_link('checkout_shipping.php', '', 'SSL'), 'success'); ?>
    </div>
  </div>
<?php
    $initialize_checkout_methods = $payment_modules->checkout_initialization_method();
    if (!empty($initialize_checkout_methods)) {
?>
  <p class="shoppingtextAlternativeCheckout"><?php echo TEXT_ALTERNATIVE_CHECKOUT_METHODS; ?></p>
<?php
     foreach($initialize_checkout_methods as $value) {
?>
  <p align="right"><?php echo $value; ?></p>
<?php
      }
    }
?>
</form>
<div class="contentText">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <div class="shoppingCartInformationSave">
   <?php echo SHOPPING_CART_INFORMATION_SAVE; ?>
  </div>
</div>
<?php
  } else {
?>
  <div class="clearfix"></div>
  <div class="contentText">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <?php echo TEXT_CART_EMPTY; ?>
  </div>

  <div class="buttonSet pull-right">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, '', osc_href_link('index.php'), 'success'); ?>
  </div>
<?php
  }
?>