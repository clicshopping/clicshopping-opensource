<?php
  /**
   * cfm_modules_login.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_modules_login {
    var $code = 'modules_login';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_LOGIN_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_login() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_login/';
      $this->title = MODULE_CFG_MODULE_LOGIN_TITLE;
    }
  }

?>
