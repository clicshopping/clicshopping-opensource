<?php
/*
 * checkout_shipping.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');
  require('includes/classes/http_client.php');


// if the customer is not logged on, redirect them to the login page
  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }
  // if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($_SESSION['cart']->count_contents() < 1) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }


// Verify if a street address exist concernant the customer
    $QaddressCustomer = $OSCOM_PDO->prepare('select entry_street_address
                                              from :table_address_book
                                              where customers_id = :customers_id
                                              and address_book_id =  :address_book_id 
                                            ');
    $QaddressCustomer->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    $QaddressCustomer->bindInt(':address_book_id',  (int)$OSCOM_Customer->getDefaultAddressID() );
    $QaddressCustomer->execute();
    $addressCustomer = $QaddressCustomer->fetch();

    if (empty( $addressCustomer['entry_street_address'])) {
      osc_redirect(osc_href_link('checkout_shipping_address.php','newcustomer=1','SSL'));
    }

//check if address id exist else go shipping_address for new default address
  if (!$OSCOM_Customer->getDefaultAddressID()) {
      osc_redirect(osc_href_link('checkout_shipping_address.php','newcustomer=1','SSL'));
  }

// if no shipping destination address was selected, use the customers own address as default
  if (!isset($_SESSION['sendto'])) {
    $_SESSION['sendto'] = $OSCOM_Customer->getDefaultAddressID();
  } else {
// verify the selected shipping address
    if ( (is_array($_SESSION['sendto']) && empty($_SESSION['sendto'])) || is_numeric($_SESSION['sendto']) ) {

      $QcheckAddress = $OSCOM_PDO->prepare('select address_book_id 
                                            from :table_address_book
                                            where customers_id = :customers_id
                                            and address_book_id =  :address_book_id                                   
                                           ');
      $QcheckAddress->bindInt(':customers_id', $OSCOM_Customer->getID());
      $QcheckAddress->bindInt(':address_book_id', (int)$_SESSION['sendto'] );
      $QcheckAddress->execute();

     if ($QcheckAddress->fetch() === false) {
        $_SESSION['sendto'] = $OSCOM_Customer->getDefaultAddressID();
        if (isset($_SESSION['shipping'])) unset($_SESSION['shipping']);
      }
    }
  }

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// register a random ID in the session to check throughout the checkout procedure
// against alterations in the shopping cart contents
  if (isset($_SESSION['cartID']) && ($_SESSION['cartID'] != $_SESSION['cart']->cartID) && isset($_SESSION['shipping'])) {
    unset($_SESSION['shipping']);
  }

  $_SESSION['cartID'] = $_SESSION['cart']->cartID = $_SESSION['cart']->generate_cart_id();

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
  if ($order->content_type == 'virtual') {
    $_SESSION['shipping'] = false;
    $_SESSION['sendto'] = false;
    osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
  }

  $total_weight = $_SESSION['cart']->show_weight();
  $total_count = $_SESSION['cart']->count_contents();

// load all enabled shipping modules
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping;

  if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
    $pass = false;

    switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
      case 'national':
        if ($order->delivery['country_id'] == STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'international':
        if ($order->delivery['country_id'] != STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'both':
        $pass = true;
        break;
    }

    $free_shipping = false;
    if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
      $free_shipping = true;

      include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/order_total/ot_shipping.php');
    }
  } else {
    $free_shipping = false;
  }

// process the selected shipping method
  if ( isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken']) ) {
    if (osc_not_null($_POST['comments'])) {
      $_SESSION['comments'] = osc_db_prepare_input($_POST['comments']);
    }


    if ( (osc_count_shipping_modules() > 0) || ($free_shipping == true) ) {
      if ( (isset($_POST['shipping'])) && (strpos($_POST['shipping'], '_')) ) {
        $_SESSION['shipping'] = $_POST['shipping'];

        list($module, $method) = explode('_', $_SESSION['shipping']);
        if ( is_object($GLOBALS[$module]) || ($_SESSION['shipping'] == 'free_free') ) {
          if ($_SESSION['shipping'] == 'free_free') {
            $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
            $quote[0]['methods'][0]['cost'] = '0';
          } else {
            $quote = $shipping_modules->quote($method, $module);
          }
          if (isset($quote['error'])) {
            unset($_SESSION['shipping']);
          } else {
            if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {
              $_SESSION['shipping'] = array('id' => $_SESSION['shipping'],
                                            'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                                            'cost' => $quote[0]['methods'][0]['cost']);

              osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
            }
          }
        } else {
          unset($_SESSION['shipping']);
        }
      }
    } else {
      if ( defined('SHIPPING_ALLOW_UNDEFINED_ZONES') && (SHIPPING_ALLOW_UNDEFINED_ZONES == 'False') ) {
          unset($_SESSION['shipping']);
      } else {
        $shipping = false;

        osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
      }
    }
  }

// get all available shipping quotes
  $quotes = $shipping_modules->quote();

// if no shipping method has been selected, automatically select the first method.
// if the modules status was changed when none were available, to save on implementing
// a javascript force-selection method, also automatically select the first shipping
// method if more than one module is now enabled
  if ( !isset($_SESSION['shipping']) || ( isset($_SESSION['shipping']) && ($_SESSION['shipping'] == false) && (osc_count_shipping_modules() > 1) ) ) $_SESSION['shipping']  = $shipping_modules->cheapest();

  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_shipping'));

  if ( defined('SHIPPING_ALLOW_UNDEFINED_ZONES') && (SHIPPING_ALLOW_UNDEFINED_ZONES == 'False') && !$OSCOM_Customer->isLoggedOn() && ($shipping == false) ) {
    $OSCOM_MessageStack->addError('checkout_address', ERROR_NO_SHIPPING_AVAILABLE_TO_SHIPPING_ADDRESS);

    osc_redirect(osc_href_link('checkout_shipping_address.php', '', 'SSL'));
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('checkout_shipping.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('checkout_shipping.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('checkout_shipping'));
