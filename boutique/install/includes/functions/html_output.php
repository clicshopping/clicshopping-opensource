<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  function osc_draw_input_field($name, $value = null, $parameters = null, $override = true, $type = 'text') {
    $field = '<input type="' . osc_output_string($type) . '" class="form-control" name="' . osc_output_string($name) . '" id="' . osc_output_string($name) . '"';
    if ( ($key = $GLOBALS[$name]) || ($key = $GLOBALS['$_GET'][$name]) || ($key = $GLOBALS['$_POST'][$name]) || ($key = $_SESSION[$name]) && ($override) ) {
      $field .= ' value="' . osc_output_string($key) . '"';
    } elseif ($value != '') {
      $field .= ' value="' . osc_output_string($value) . '"';
    }
    if ($parameters) $field.= ' ' . $parameters;
    $field .= '>';

    return $field;
  }

  function osc_draw_password_field($name, $parameters = null) {
    return osc_draw_input_field($name, null, $parameters, false, 'password');
  }

  function osc_draw_hidden_field($name, $value) {
    return '<input type="hidden" name="' . osc_output_string($name) . '" value="' . osc_output_string($value) . '">';
  }

/**
* Outputs a form pull down menu field
*
* @param string $name The name of the pull down menu field
* @param array $values Defined values for the pull down menu field
* @param string $default The default value for the pull down menu field
* @param string $parameters Additional parameters for the pull down menu field
* @access public
*/

  function osc_draw_select_menu($name, $values, $default = null, $parameters = null) {
    $group = false;

    if ( isset($_GET[$name]) ) {
      $default = $_GET[$name];
    } elseif ( isset($_POST[$name]) ) {
      $default = $_POST[$name];
    }

    $field = '<select class="form-control" name="' . osc_output_string($name) . '"';

    if ( strpos($parameters, 'id=') === false ) {
      $field .= ' id="' . osc_output_string($name) . '"';
    }

    if ( !empty($parameters) ) {
      $field .= ' ' . $parameters;
    }

    $field .= '>';

    for ( $i=0, $n=count($values); $i<$n; $i++ ) {
      if ( isset($values[$i]['group']) ) {
        if ( $group != $values[$i]['group'] ) {
          $group = $values[$i]['group'];

          $field .= '<optgroup label="' . osc_output_string($values[$i]['group']) . '">';
        }
      }

      $field .= '<option value="' . osc_output_string($values[$i]['id']) . '"';

      if ( isset($default) && ((!is_array($default) && ((string)$default == (string)$values[$i]['id'])) || (is_array($default) && in_array($values[$i]['id'], $default))) ) {
        $field .= ' selected="selected"';
      }

      if ( isset($values[$i]['params']) ) {
        $field .= ' ' . $values[$i]['params'];
      }

      $field .= '>' . osc_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';

      if ( ($group !== false) && (($group != $values[$i]['group']) || !isset($values[$i+1])) ) {
        $group = false;

        $field .= '</optgroup>';
      }
    }

    $field .= '</select>';

    return $field;
  }

  function osc_draw_time_zone_select_menu($name, $default = null) {
    if ( !isset($default) ) {
      $default = date_default_timezone_get();
    }

    $time_zones_array = array();

    foreach ( timezone_identifiers_list() as $id ) {
      $tz_string = str_replace('_', ' ', $id);

      $id_array = explode('/', $tz_string, 2);

      $time_zones_array[$id_array[0]][$id] = isset($id_array[1]) ? $id_array[1] : $id_array[0];
    }

    $result = array();

    foreach ( $time_zones_array as $zone => $zones_array ) {
      foreach ( $zones_array as $key => $value ) {
        $result[] = array('id' => $key,
                          'text' => $value,
                          'group' => $zone);
      }
    }

    return osc_draw_select_menu($name, $result, $default);
  }

/**
 * Creates a aui jquery button
 * Output a jQuery UI Button
 * @param string $title , title of the button
 * @param string $$icon, ui icon of the button
 * @param string $link, link
 * @param string $priotiy; primary or secondary
 * @param string $param primary or secondaty
 * @param string $id, stylesheet of the button via an a id
 * @param string $target, _blank, self, _top,_new,_parent   of the link
 * @param return $button, the button
 * osc_draw_button('HOME', 'triangle-1-n', 'http://www.website.com/home.html', null, null, null, 'parent')
 * @access public
 */
////
//-------------------------
// Output a Bootstrap Button
// ------------------------
// 
    function osc_draw_button($title = null, $icon = null, $link = null, $style = null, $params = null,  $size = null, $target = null) {
      $types = array('submit', 'button', 'reset');
      $styles = array('primary', 'info', 'success', 'warning', 'danger', 'inverse', 'link', 'new', 'secondary');
      $size_button = array('lg', 'sm', 'xs');

      if ( !isset($params['type']) ) {
        $params['type'] = 'submit';
      }

      if ( !in_array($params['type'], $types) ) {
        $params['type'] = 'submit';
      }

      if ( ($params['type'] == 'submit') && isset($link) ) {
        $params['type'] = 'button';
      }

      if ( isset($style) && !in_array($style, $styles) ) {
        unset($style);
      }

      if ( isset($size) && !in_array($size, $size_button) ) {
        unset($size);
      }

      $button = '';

      if ( ($params['type'] == 'button') && isset($link) ) {
        $button .= '<a href="' . $link . '"';

       if ( isset($params['newwindow']) && !isset($target) ) {
          $button .= ' target="_blank"';
  // link target
        if (isset($target)) {
          if ($target == 'parent' ){
            $button .= ' target="_parent"';
          }
          if ($target == 'blank' ){
            $button .= ' target="_blank"';
          }
          if ($target == 'top' ){
            $button .= ' target="_top"';
          }
          if ($target == 'self' ){
            $button .= ' target="_self"';
          }
          if ($target == 'new' ){
           $button .= ' target="_new"';
          }
        }
      }


      } else {
        $button .= '<button type="' . osc_output_string($params['type']) . '"';
      }

      if ( isset($params['params']) ) {
        $button .= ' ' . $params['params'];
      }

      $button .= ' class="btn';

      if ( isset($style) ) {
        $button .= ' btn-' . $style;
      }


      if ( isset($size) ) {
        $button .= ' btn-' . $size;
      }

      $button .= '">';

      if ( isset($icon) ) {
        if ( !isset($params['iconpos']) ) {
          $params['iconpos'] = 'left';
        }

        if ( $params['iconpos'] == 'left' ) {
          $button .= '<span class="glyphicon-' . $icon;

          if ( isset($style) ) {
            $button .= ' glyphicon-white';
          }

          $button .= '"></span> ';
        }
      }

      $button .= $title;

      if ( isset($icon) && ($params['iconpos'] == 'right') ) {
        $button .= ' <span class="glyphicon-' . $icon;

        if ( isset($style) ) {
          $button .= ' glyphicon-white';
        }

        $button .= '"></span>';
      }

      if ( ($params['type'] == 'button') && isset($link) ) {
        $button .= '</a>';
      } else {
        $button .= '</button>';
      }

      return $button;
    }

/**
 * Outputs a form selection field (checkbox/radio)
 *
 * @param string $name The name and indexed ID of the selection field
 * @param string $type The type of the selection field (checkbox/radio)
 * @param mixed $values The value of, or an array of values for, the selection field
 * @param string $default The default value for the selection field
 * @param string $parameters Additional parameters for the selection field
 * @param string $separator The separator to use between multiple options for the selection field
 * @access public
 */
////
// Output a selection field - alias function for osc_draw_checkbox_field() and osc_draw_radio_field()
  function osc_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {

    $selection = '<input type="' . osc_output_string($type) . '" name="' . osc_output_string($name) . '"';

    if (osc_not_null($value)) $selection .= ' value="' . osc_output_string($value) . '"';

    if ( ($checked == true) || (isset($_GET[$name]) && is_string($_GET[$name]) && (($_GET[$name] == 'on') || ($_GET[$name] == $value))) || (isset($_POST[$name]) && is_string($_POST[$name]) && (($_POST[$name] == 'on') || ($_POST[$name] == $value))) ) {
      $selection .= ' checked="checked"';
    }

    if (osc_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= '>';

    return $selection;
  }


function osc_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {

$field = '<select name="' . osc_output_string($name) . '"';

if (osc_not_null($parameters)) $field .= ' ' . $parameters;

$field .= '>';

if (empty($default) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
if (isset($_GET[$name]) && is_string($_GET[$name])) {
$default = $_GET[$name];
} elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
$default = $_POST[$name];
}
}

for ($i=0, $n=sizeof($values); $i<$n; $i++) {
$field .= '<option value="' . osc_output_string($values[$i]['id']) . '"';
if ($default == $values[$i]['id']) {
$field .= ' selected="selected"';
//        $field .= ' SELECTED';
}

$field .= '>' . osc_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
}
$field .= '</select>';

if ($required == true) $field .= TEXT_FIELD_REQUIRED;

return $field;
}
?>