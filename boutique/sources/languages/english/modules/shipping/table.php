<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('MODULE_SHIPPING_TABLE_TEXT_TITLE', 'Table Rate');
define('MODULE_SHIPPING_TABLE_TEXT_DESCRIPTION', 'Table Rate<br /><br /><font color="#FF0000"><strong>Becarefull !</strong> Uninstalling this module will make all eventual modifications lost');
define('MODULE_SHIPPING_TABLE_TEXT_WAY', 'Best Way');
define('MODULE_SHIPPING_TABLE_TEXT_WEIGHT', 'Weight');
define('MODULE_SHIPPING_TABLE_TEXT_AMOUNT', 'Amount');
?>
