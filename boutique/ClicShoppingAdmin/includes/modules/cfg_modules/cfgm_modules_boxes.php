<?php
  /**
   * cfm_modules_boxes.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_modules_boxes {
    var $code = 'modules_boxes';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_BOXES_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_boxes() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_boxes/';
      $this->title = MODULE_CFG_MODULE_BOXES_TITLE;
    }
  }
?>
