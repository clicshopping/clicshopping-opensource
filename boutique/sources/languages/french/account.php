<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Mon compte');
define('HEADING_TITLE', 'Informations sur mon compte');

define('OVERVIEW_TITLE', 'Visualiser');
define('OVERVIEW_SHOW_ALL_ORDERS', '(Afficher toutes mes commandes)');
define('OVERVIEW_PREVIOUS_ORDERS', 'Commandes précédente');

define('MY_SUPPORT','Contater le support');
define('MY_ORDERS_TITLE', 'Mes commandes');
define('MY_ORDERS_VIEW', 'Voir les commandes que j\'ai effectuées.');

define('MIN_QTY_ORDER_PRODUCT','Qté min : ');

// ONLY HERE
  if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_INFORMATION', 'Modifier les informations de votre société.');
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_ADDRESS_BOOK', 'Modifier vos adresses de livraisons et contacts commerciaux.');
  } else {
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_INFORMATION', 'Afficher ou modifier les informations de mon compte.');
    define('MODULE_ACCOUNT_CUSTOMERS_MY_ACCOUNT_ADDRESS_BOOK', 'Voir ou changer mon carnet d\'adresse.');
  }
?>