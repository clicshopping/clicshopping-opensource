<?php
/**
 * products_heart.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Coups de coeur');

define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Prix du produits');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_PRODUCTS_GROUP','Groupe client');
define('TABLE_HEADING_EXPIRES_DATE','Date expiration');
define('TABLE_HEADING_ARCHIVE','Produits archivés');
define('TABLE_HEADING_SCHEDULED_DATE','Date début');
define('TABLE_HEADING_MODEL','Réf');

define('TEXT_PRODUCTS_HEART_PRODUCT', 'Produit :');
define('TEXT_PRODUCTS_HEART_EXPIRES_DATE', 'Date d\'expiration :');
define('TEXT_PRODUCTS_HEART_GROUPS', 'Groupe Client :');
define('TEXT_PRODUCTS_HEART_SCHEDULED_DATE','Date début');
define('TEXT_NEW_PRODUCTS_HEART_TWITTER','Notre coups de coeur sur '. STORE_NAME  .' ! ');
define('TEXT_PRODUCTS_HEART_TWITTER','Souhaitez vous publier ce coup de coeur sur Twitter ? ');
define('TEXT_YES','Oui');
define('TEXT_NO','non');
define('TEXT_STATUS','Statut du produit');

define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'dernière modification :');
define('TEXT_INFO_NEW_PRICE', 'Nouveau Prix :');
define('TEXT_INFO_ORIGINAL_PRICE', 'Prix original :');
define('TEXT_INFO_PERCENTAGE', 'Pourcentage :');
define('TEXT_INFO_SCHEDULE_DATE', 'Débt le :');
define('TEXT_INFO_EXPIRES_DATE', 'Expire le :');
define('TEXT_INFO_STATUS_CHANGE', 'Changement de statut :');

define('TEXT_INFO_HEADING_DELETE_PRODUCTS_HEART', 'Supprimer le coup de coeur');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer ce coup de coeur ?');

define('TITLE_PRODUCTS_HEART_GENERAL', 'Informations sur le produit');
define('TITLE_PRODUCTS_HEART_GROUPE', 'Prix et Groupe client');
define('TITLE_PRODUCTS_HEART_DATE', 'Date d\'affichage');
define('TITLE_AIDE_PRODUCTS_HEART_PRICE', 'Note sur les coups de coeur');

define('TEXT_AIDE_PRODUCTS_HEART_PRICE', '<ul><li>Laissez la date d\'échéance vide pour aucune expiration</li>
<li>La date de début a des droits supérieurs sur la date d\'expiration.</li>
<li>Si vous souhaitez réinitialiser les dates, veuillez changer de statut dans la page générale.</li>
<li>Les coups de coeur sont automatiquement mises à jour (la partie catalogue de la boutique gère cet élément), il est inutile de changer le statut du coup de coeur après son enregistrement.</li>
<li>La publication d\'une information sur Twitter ne fonctionne qu\'à la création du produit (avec un statut sur on et avec un groupe client normal). Si le nom de votre boutique + URL de votre produit fait plus de 140 caractères, le coup de coeur ne sera pas publié (contrainte forte)</li>
</ul>');
?>
