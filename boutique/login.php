<?php
/*
 * login.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)
  if (session_status() !== PHP_SESSION_ACTIVE) {
    if ( !isset($_GET['cookie_test']) ) {
      $all_get = osc_get_all_get_params();

      osc_redirect(osc_href_link('login.php', $all_get . (empty($all_get) ? '' : '&') . 'cookie_test=1', 'SSL'));
    }

    osc_redirect(osc_href_link('cookie_usage.php'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('login'));

  if ( isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken']) ) {
        $error = false;        

        $email_address = isset($_POST['email_address']) ? trim($_POST['email_address']) : null;
        $password = isset($_POST['password']) ? trim($_POST['password']) : null;

// Check if email exists

        $Qcheck = $OSCOM_PDO->prepare('select customers_id, 
                                              customers_password 
                                       from :table_customers 
                                       where customers_email_address = :customers_email_address
                                      ');
        $Qcheck->bindValue(':customers_email_address', $email_address);
        $Qcheck->execute();
 
        if ( $Qcheck->fetch() === false ) {
          $error = true;
        } else {
// Check that password is good
          if ( !osc_validate_password($password, $Qcheck->value('customers_password')) ) {
            $error = true;
          } else {
            if ( SESSION_RECREATE == 'True' ) {
              osc_session_recreate();
            }

// migrate old hashed password to new phpass password
            if ( osc_password_type($Qcheck->value('customers_password')) != 'phpass' ) {

              $Qupdate = $OSCOM_PDO->prepare('update :table_customers 
                                              set customers_password = :customers_password 
                                              where customers_id = :customers_id
                                            ');
              $Qupdate->bindValue(':customers_password', osc_encrypt_password($password));
              $Qupdate->bindInt(':customers_id', $Qcheck->valueInt('customers_id'));
              $Qupdate->execute();

            }

            $OSCOM_Customer->setData($Qcheck->valueInt('customers_id'));

// B2B - B2C
            $customers_group_id = $OSCOM_Customer->getCustomersGroupID();

            if ($customers_group_id > 0) {
              $OSCOM_Customer->setData($customers_group_id);
            }

            $Qupdate = $OSCOM_PDO->prepare('update :table_customers_info 
                                            set customers_info_date_of_last_logon = now(), 
                                                customers_info_number_of_logons = customers_info_number_of_logons+1, 
                                                password_reset_key = null, 
                                                password_reset_date = null 
                                            where customers_info_id = :customers_info_id
                                          ');
            $Qupdate->bindInt(':customers_info_id', $Qcheck->valueInt('customers_id'));
            $Qupdate->execute();

// reset session token
        $_SESSION['sessiontoken'] = md5(osc_rand() . osc_rand() . osc_rand() . osc_rand());

// restore cart contents
        $_SESSION['cart']->restore_contents();

        if (sizeof($_SESSION['navigation']->snapshot) > 0) {
          $origin_href = osc_href_link($_SESSION['navigation']->snapshot['page'], osc_array_to_string($_SESSION['navigation']->snapshot['get'], array(session_name())), $_SESSION['navigation']->snapshot['mode']);
          $_SESSION['navigation']->clear_snapshot();
          osc_redirect($origin_href);
        } else {
          osc_redirect(osc_href_link('account.php'));
        }
      }
    }
  }

  if ($error === true) {
    $OSCOM_MessageStack->addError('login', TEXT_LOGIN_ERROR);
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('login.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('login'));
