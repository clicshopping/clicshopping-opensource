<?php
/*
 * account_newletter.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  echo osc_draw_form('account_newsletter', osc_href_link('account_newsletters.php', '', 'SSL'), 'post', '', true) . osc_draw_hidden_field('action', 'process');
// ----------------------
// --- Title   -----  
// ---------------------- 
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<div class="contentContainer">
  <h3><?php echo MY_NEWSLETTERS_TITLE; ?></h3>
  <div class="contentText">
    <div class="checkbox">
      <label>
        <?php echo osc_draw_checkbox_field('newsletter_general', '1', (($newsletter['customers_newsletter'] == '1') ? true : false)); ?>
        <strong><?php echo MY_NEWSLETTERS_GENERAL_NEWSLETTER; ?></strong><br /><?php echo MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION; ?>
      </label>
    </div>
  </div>
<?php
// ---------------------- 
// --- Button   -----  
// ---------------------- 
?>
  <div class="buttonSet row">
    <div class="col-xs-6"><?php  echo osc_draw_button(IMAGE_BUTTON_BACK,null, osc_href_link('account.php', '', 'SSL'), 'primary', null, null);  ?></div>
    <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null);  ?></div>
  </div>
</div>
</form>
<?php   require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>