<?php
/**
 * action_recorder.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE','Surveillance des actions sur le site');
define('TABLE_HEADING_MODULE','Module');
define('TABLE_HEADING_CUSTOMER','Clients');
define('TEXT_FILTER_SEARCH', 'Rechercher :');
define('TEXT_ALL_MODULES', '-- Tous les modules --');  	
define('TABLE_HEADING_DATE_ADDED','Date ajout');  	
define('TABLE_HEADING_ACTION','Action'); 
define('TEXT_INFO_IDENTIFIER','Information client : ');
define('TEXT_INFO_DATE_ADDED','date ajout : ');
define('SUCCESS_EXPIRED_ENTRIES','Succès');
?>