<?php
/*
 * google_sitemap_products.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $blog_content_array = array();

    $QblogCategories = $OSCOM_PDO->prepare('select blog_content_id,
                                            coalesce(NULLIF(blog_content_last_modified, :last_modified),
                                                            blog_content_date_added) as last_modified
                                            from :table_blog_content
                                            where blog_content_status = :blog_content_status
                                            and customers_group_id = :customers_group_id
                                            order by last_modified DESC
                                          ');

    $QblogCategories->bindValue(':last_modified', '');
    $QblogCategories->bindValue(':blog_content_status', '1');
    $QblogCategories->bindValue(':customers_group_id', '0');
    $QblogCategories->execute();

    while ($blog_content = $QblogCategories->fetch()) {
      $blog_content_array[$blog_content['blog_content_id']]['loc'] = osc_href_link('blog_content.php', 'blog_content_id=' . (int)$blog_content['blog_content_id'], 'NONSSL', false);
      $blog_content_array[$blog_content['blog_content_id']]['lastmod'] = $blog_content['last_modified'];
      $blog_content_array[$blog_content['blog_content_id']]['changefreq'] = 'weekly';
      $blog_content_array[$blog_content['blog_content_id']]['priority'] = '0.5';
    }

    foreach ($blog_content_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
