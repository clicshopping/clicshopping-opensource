<?php
/*
* edit_css.php
* @copyright Copyright 2008 - http://www.e-imaginis.com
* @copyright Portions Copyright 2003 osCommerce
* @license GNU Public License V2.0
* @version $Id:
*/


  define('HEADING_TITLE_CSS','Editeur CSS');

  define('TEXT_SELECT_FILES','Sélection votre css : ');
  define('TITLE_HELP_EDIT_CSS_IMAGE','Aide sur l\'éditeur du CSS');
  define('TITLE_HELP_EDIT_CSS', 'Aide sur l\'éditeur du CSS');
  define('TEXT_HELP_EDIT_CSS','Pour des raisons de sécurit&eacute, les permissions sont d\'origine non permises dans l\'éditeur de css. Il est important de comprendre
  que changer les permissions des fichiers peut entrainer un problème de sécurité de votre application. Il est préférable de télécharger votre fichier par FTP et de le changer sur votre ordinateur<br />
  Dans le cas ou vous souhaiteriez réaliser des opérations de modification des fichiers à partir de l\'éditeur du css, vous devez suivre cette procédure.<br />
  - Droits sur les répertoires : chmod 755<br />
  - Droits sur les fichiers : chmod 666</br />
  - Après vos modifications, veuillez changer vos droits sur les fichiers en chmod 644.
  - Veuillez noter que le configuration de votre serveur aura une influence sur les autorisations que vous aurez droit de faire ou pas. Nous vous invitons à consulter dans <a href="http://clicshopping.org/marketplace/blog.php" target="_blank">notre guide</a>
  pour de plus amples explications.');

?>