<?php
/*
 * checkout_payment.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require($OSCOM_Template->getTemplateHeaderFooter('header'));

   echo $payment_modules->javascript_validation();
   require($OSCOM_Template->getTemplateFiles('breadcrumb'));

   echo osc_draw_form('checkout_payment', osc_href_link('checkout_confirmation.php', '', 'SSL'), 'post', 'onsubmit="return check_form();"', true);
?>
<div class="contentContainer">
<?php
  if (isset($_GET['payment_error']) && is_object(${$_GET['payment_error']}) && ($error = ${$_GET['payment_error']}->get_error())) {
?>
  <div class="contentText">
    <?php echo '<strong>' . osc_output_string_protected($error['title']) . '</strong>'; ?>

    <p class="messageStackError"><?php echo osc_output_string_protected($error['error']); ?></p>
  </div>
  <div class="contentText">
   <?php echo TEXT_CONDITIONS_DESCRIPTION . '<br /><br />' . osc_draw_checkbox_field('conditions', '1', false, 'id="conditions"') . '<label for="conditions">&nbsp;' . TEXT_CONDITIONS_CONFIRM . '</label>'; ?>
  </div>
<?php
  }
?>
  <h3><?php echo TABLE_HEADING_BILLING_ADDRESS; ?></h3>
  <div class="contentText">
    <div class="infoBoxContainer" style="float: right;">
      <div class="infoBoxHeading"><?php echo TITLE_BILLING_ADDRESS; ?></div>
      <div class="infoBoxContents">
        <?php echo osc_address_label($OSCOM_Customer->getID(), $_SESSION['billto'], true, ' ', '<br />'); ?>
      </div>
    </div>      
    <?php echo TEXT_SELECTED_BILLING_DESTINATION; ?><br /><br /> 
<?php
// Autorise l'ajout dans le carnet d'adresse des clients B2B ou clients normaux
  if (osc_count_customers_modify_address_default() == 1 && (ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG == 'true' || ODOO_ACTIVATE_WEB_SERVICE == 'false') ) {

?>
       <span class="smallText"><?php echo osc_draw_button(IMAGE_BUTTON_CHANGE_ADDRESS, null, osc_href_link('checkout_payment_address.php', '', 'SSL'), 'primary'); ?></span>
<?php
  }
?>
  </div>     
  <div style="clear: both;"></div>
  <h3><?php echo TABLE_HEADING_PAYMENT_METHOD; ?></h3>
<?php
  $selection = $payment_modules->selection();
  if (sizeof($selection) > 1) {
?>
  <div class="contentText">
    <div style="pull-right;"><?php echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?> </div>
    <?php echo TEXT_SELECT_PAYMENT_METHOD; ?>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<?php
    } elseif ($free_shipping == false) {
?>
  <div class="contentText">
    <?php echo TEXT_ENTER_PAYMENT_INFORMATION; ?>
  </div>
<?php
    }
?>
  <div class="contentText">

    <table class="table table-striped table-condensed table-hover">
      <tbody>
<?php
  $radio_buttons = 0;
  for ($i=0, $n=sizeof($selection); $i<$n; $i++) {
?>
      <tr>
        <td><strong><?php echo $selection[$i]['module']; ?></strong></td>
        <td align="right">
<?php
    if (sizeof($selection) > 1) {
      echo osc_draw_radio_field('payment', $selection[$i]['id'], (isset($_SESSION['payment']) && ($selection[$i]['id'] == $_SESSION['payment'])), 'required aria-required="true"');
    } else {
      echo osc_draw_hidden_field('payment', $selection[$i]['id']);
    }
?>
        </td>
      </tr>
<?php
    if (isset($selection[$i]['error'])) {
?>
      <tr>
        <td colspan="2"><?php echo $selection[$i]['error']; ?></td>
      </tr>
<?php
    } elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
?>
      <tr>
        <td colspan="2"><table border="0" cellspacing="0" cellpadding="2">
<?php
      for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
?>
          <tr>
            <td><?php echo $selection[$i]['fields'][$j]['title']; ?></td>
            <td><?php echo $selection[$i]['fields'][$j]['field']; ?></td>
          </tr>
<?php
      }
?>
        </table></td>
      </tr>
<?php
    }
?>

<?php
    $radio_buttons++;
  }
?>
      </tbody>
    </table>
  </div>
   <div style="padding-top:10px;"></div>
   <div class="hr"></div>
<?php
// discount coupons 
   if(MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true') {
?>
  <h3><?php echo TABLE_HEADING_COUPON; ?></h3>
  <div class="contentText">
     <blockquote><label for="inputCoupon"><?php echo TEXT_COUPON; ?></label></blockquote>
    <span class="col-md-2 pull-left">
      <?php echo ENTRY_DISCOUNT_COUPON; ?>
    </span>
    <span class="col-md-2" style="box-shadow: rgba(0, 0, 0, 0.270588) 0 0 4px;">
       <?php echo osc_draw_input_field('coupon', '', 'id="inputCoupon" class="input-sm" placeholder="' . ENTRY_DISCOUNT_COUPON . '"' ); ?>
    </span>
  </div>
<?php
   }
// End discount
?>
   <div class="clearfix" style="padding-top:20px;"></div>
   <div class="hr"></div>
  <div class="contentText">
    <div class="form-group">
      <label for="inputComments">
         <h3><?php echo TABLE_HEADING_COMMENTS; ?></h3>
       </label>
       <div><?php echo osc_draw_textarea_field('comments', 'soft', '60', '5', (isset($_SESSION['comments']) ? $_SESSION['comments'] : ''), 'id="inputComments" placeholder="' . TABLE_HEADING_COMMENTS . '"') ?></div>
    </div>
  </div>
  <div class="contentText">
<?php
// ################### Changement de place des conditions d\'acceptation ##############
  if (DISPLAY_CONDITIONS_ON_CHECKOUT == 'true') {
?>
  <h3><?php echo TABLE_HEADING_CONDITIONS; ?></h3>
  <div class="contentText">
    <div class="checkbox">
      <label>
        <?php echo TEXT_CONDITIONS_DESCRIPTION . '<br /><br />' . osc_draw_checkbox_field('conditions', '1', false, 'id="conditions"');?>
        <label for="conditions">&nbsp;<br /><?php echo  TEXT_CONDITIONS_CONFIRM; ?></label>
      </label>
    </div>
  </div>
<?php
  }
?>
  </div>
  <div class="pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null); ?>
  </div>
</div>
</form>
<div class="contentContainer"><?php echo $OSCOM_Template->getBlocks('modules_checkout_payment'); ?></div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
