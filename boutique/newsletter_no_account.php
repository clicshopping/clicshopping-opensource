<?php
/**
 * newlsetter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('newsletter_no_account'));

   if (isset($_GET['action']) && ($_GET['action'] == 'send') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {

    $error = false;

    $firstname = osc_db_prepare_input($_POST['firstname']);
    $lastname = osc_db_prepare_input($_POST['lastname']);
    $email_address = osc_db_prepare_input($_POST['email_address']);
    $email_address_confirm = osc_db_prepare_input($_POST['email_address_confirm']);
    $number_email_confirmation = osc_db_prepare_input($_POST['number_email_confirmation']);	
    $confirmation = osc_db_prepare_input($_POST['confirmation']);
       
// Controle entree du prenom
    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;
      $OSCOM_MessageStack->addError('newsletter_no_account', ENTRY_FIRST_NAME_ERROR);
    }

// Controle entree du nom de famille
    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
      $error = true;
      $OSCOM_MessageStack->addError('newsletter_no_account', ENTRY_LAST_NAME_ERROR);
    }


// Controle entree adresse e-mail
    if (osc_validate_email($email_address) == false) {
      $error = true;
      $OSCOM_MessageStack->addError('newsletter_no_account', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    } elseif ($email_address != $email_address_confirm) {
      $error = true;
      $OSCOM_MessageStack->addError('newsletter_no_account', ENTRY_EMAIL_ADDRESS_CONFIRM_NOT_MATCHING);

    } else {
// email account verify

      $QcheckEmail = $OSCOM_PDO->prepare('select count(*) as total 
                                          from :table_customers 
                                          where customers_email_address = :customers_email_address
                                        ');
      $QcheckEmail->bindValue(':customers_email_address', $email_address);
      $QcheckEmail->execute();

// email with no account verify

      $QcheckEmailNoAccount = $OSCOM_PDO->prepare('select count(*) as total 
                                                  from :table_newsletter_no_account
                                                  where customers_email_address = :customers_email_address
                                                ');
      $QcheckEmailNoAccount->bindValue(':customers_email_address', $email_address);
      $QcheckEmailNoAccount->execute();

      if ($QcheckEmail->value('total') > 0 || $QcheckEmailNoAccount->value('total') > 0) {
        $error = true;
        $OSCOM_MessageStack->addError('newsletter_no_account', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
      }
    }

     $actionRecorder = new actionRecorder('ar_newsletter_no_account', ($OSCOM_Customer->isLoggedOn() ? $OSCOM_Customer->getID() : null), $name);


     if (!$actionRecorder->canPerform()) {
       $error = true;
       $actionRecorder->record(false);
       $OSCOM_MessageStack->addError('newsletter_no_account', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_NEWSLETTER_NO_ACCOUNT_EMAIL_MINUTES') ? (int)MODULE_ACTION_RECORDER_NEWSLETTER_NO_ACCOUNT_EMAIL_MINUTES : 50)));
     }

     if ($error == false) {

       if (osc_validate_number_email($number_email_confirmation)) {

          if ($error == false) {
            $sql_data_array = array('customers_firstname' => $firstname,
                                  'customers_lastname' => $lastname,
                                  'customers_email_address' => $email_address,
                                  'customers_newsletter' => '1',
                                  'customers_date_added' => 'now()',
                                  'languages_id' => $_SESSION['languages_id']
                                 );
            $OSCOM_PDO->save('newsletter_no_account', $sql_data_array);

// build the message content
          $name = $firstname . ' ' . $lastname;
          $message = utf8_decode(EMAIL_WELCOME);
          $email_welcome = html_entity_decode($message);

          $message = utf8_decode(EMAIL_WARNING);
          $email_warning = html_entity_decode($message);

          $message = utf8_decode(EMAIL_SUBJECT);
          $email_subject = html_entity_decode($message);

          $email_text .= $email_welcome . $email_warning ;
          osc_mail($name, $email_address, $email_subject, $email_text, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

          $actionRecorder->record();

          osc_redirect(osc_href_link('newsletter_no_account.php', 'action=success'));
        }
      } else {
        $error = true;
        $OSCOM_MessageStack->addError('newsletter_no_account', ENTRY_EMAIL_ADDRESS_CHECK_ERROR_NUMBER);
      }
    }
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('newsletter_no_account.php', '', 'NONSSL'));

// number for the antispam
   $number_confirmation = '(2 + '.EMAIL_CONFIRMATION_NUMBER.') x 1';

  require($OSCOM_Template->getTemplateFiles('newsletter_no_account'));
