<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Commande');
define('NAVBAR_TITLE_2', 'Livraison');

define('HEADING_TITLE', 'Information expédition');

define('TABLE_HEADING_SHIPPING_ADDRESS', 'Adresse d\'expédition');
define('TEXT_CHOOSE_SHIPPING_DESTINATION', 'Veuillez choisir l\'adresse ou vous souhaiteriez l\'expédition de votre commande.');
define('TITLE_SHIPPING_ADDRESS', 'Adresse d\'expédition :');

define('TABLE_HEADING_SHIPPING_METHOD', 'Méthode d\'expédition');
define('TEXT_CHOOSE_SHIPPING_METHOD', 'Veuillez choisir la méthode d\'expédition préférée à utiliser pour cette commande.');
define('TITLE_PLEASE_SELECT', 'Veuillez choisir');
define('TEXT_ENTER_SHIPPING_INFORMATION', 'C\'est actuellement la seule méthode d\'expédition disponible pour cette commande.');

define('TABLE_HEADING_COMMENTS', 'Ajoutez des commentaires au sujet de votre commande');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continuer la commande');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'pour choisir la méthode de paiement.');
?>