<?php
/*
 * orders_status.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Statut des commandes');

define('TABLE_HEADING_ORDERS_STATUS', 'Statut des commandes');
define('TABLE_HEADING_PUBLIC_STATUS', 'Statut Public');
define('TABLE_HEADING_ACTION', 'Action');


define('TABLE_HEADING_DOWNLOADS_STATUS', 'Statut Téléchargement : ');
define('TABLE_HEADING_PUBLIC_STATUS_DOWNLOAD', 'Statut Public des commandes : ');
define('TEXT_INFO_EDIT_PUBLIC_STATUS_VIEW', 'Visible');
define('TEXT_INFO_EDIT_PUBLIC_STATUS_NOVIEW', 'Invisible');
define('TEXT_INFO_EDIT_PUBLIC_DOWNLOAD_VIEW', 'Visible');
define('TEXT_INFO_EDIT_PUBLIC_DOWNLOAD_NOVIEW', 'Invisible');


define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Statut des commandes :');
define('TEXT_INFO_INSERT_INTRO', 'Merci de compléter cette nouvelle commande avec les données liées');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer ce statut de commande ?');
define('TEXT_INFO_HEADING_NEW_ORDERS_STATUS', 'Nouveau statut de commande');
define('TEXT_INFO_HEADING_EDIT_ORDERS_STATUS', 'Editer statut de commande');
define('TEXT_INFO_HEADING_DELETE_ORDERS_STATUS', 'Supprimer statut de commande');

define('TEXT_SET_PUBLIC_STATUS', 'Montre la commande au client en fonction du niveau du statut de la commande');
define('TEXT_SET_DOWNLOADS_STATUS', 'Permettre le téléchargement de produits virtuels en fonction du statut de la commande');
define('TEXT_SET_SUPPORT_ORDERS_STATUS','Supprime la génération de la commande PDF au client (A appliquer uniquement sur la gestion du statut du support)');

define('ERROR_REMOVE_DEFAULT_ORDER_STATUS', 'Erreur : Le statut par défaut ne peut pas être supprimé. Merci de choisir un autre statut par défaut et de réessayer');
define('ERROR_STATUS_USED_IN_ORDERS', 'Erreur : Ce statut de commande est actuellement utilisé.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Erreur : Ce statut de commande est déjà utilisé dans l\'historique de commande.');
?>