<?php
/*
 * modules_template.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

/**
 * Directory template with a drop down for all template
 * 
 * @param string  all_template
 * @return string configuration_value, $filename_array,  $template_directory, the directory name 
 * @access public
 */
 
  function  osc_cfg_pull_down_all_template_directorylist($value){
 
    $template_directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE;
    $weeds = array('.', '..', '_notes', 'index.php');
    $directories = array_diff(scandir($template_directory), $weeds);
    $filename_array = array();
    foreach($directories as $value) {
      if(is_dir($template_directory.$value)) {
        $filename_array[] = array('id' => $value,
                                  'text' => $value);
      }
    }
  return osc_draw_pull_down_menu('configuration_value', $filename_array,  $value);
  }
