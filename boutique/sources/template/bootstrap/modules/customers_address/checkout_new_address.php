<?php
/**
 * checkout_new_address.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license Licensed under GPL and MIT
 * @version $Id: 
*/


  if (!isset($process)) $process = false;
?>
  <div class="contentText">
<?php
// Clients B2C et B2B
// Nouvelle adresse : Affichage du nom societe par defaut si il existe dans la table customers.
  if (($OSCOM_Customer->getCustomersGroupID() == 0 && ACCOUNT_COMPANY == 'true') || ($OSCOM_Customer->getCustomersGroupID() != 0 && ACCOUNT_COMPANY_PRO == 'true' ) ) {
    $QaccountGroup = $OSCOM_PDO->prepare('select customers_company
                                           from :table_customers
                                           where customers_id = :customers_id
                                         ');
    $QaccountGroup->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    $QaccountGroup->execute();

    $account_group = $QaccountGroup->fetch();
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="customers_company"><?php echo ENTRY_COMPANY; ?></label>
        <div class="controls col-sm-6">
<?php
  echo osc_draw_input_field('company', $account_group['customers_company'],  null, 'id="customers_company" placeholder="'.ENTRY_COMPANY.'"');
  if (($OSCOM_Customer->getCustomersGroupID() == 0 && ENTRY_COMPANY_MIN_LENGTH > 0) || ($OSCOM_Customer->getCustomersGroupID() != 0 && ENTRY_COMPANY_PRO_MIN_LENGTH > 0) ) {
    echo '&nbsp;' . (osc_not_null(ENTRY_COMPANY_TEXT) ? '<span class="text-warning">' . ENTRY_COMPANY_TEXT . '</span>': '');
  }
?>
        </div>
      </div>
    </div>

<?php
  }
// Clients B2C et B2B : Informations general - Nom, prenom, date de naissance, email, telephone et fax

  if (ACCOUNT_GENDER == 'true') {
    $male = $female = false;

    if (isset($gender)) {
      $male = ($gender == 'm');
      $female = !$male;
    } elseif ( !$OSCOM_Customer->hasDefaultAddress() ) {
      $male = ($OSCOM_Customer->getGender() == 'm');
      $female = !$male;
    }
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="gender"><?php echo ENTRY_GENDER; ?></label>
        <div class="controls col-sm-6">
          <?php echo osc_draw_radio_field('gender', 'm', $male) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . osc_draw_radio_field('gender', 'f', $female) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (osc_not_null(ENTRY_GENDER_TEXT) ? '<span class="text-warning">' . ENTRY_GENDER_TEXT . '</span>': ''); ?>
        </div>
      </div>
    </div>
<?php
  }
?>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-4" for="firstname"><?php echo ENTRY_FIRST_NAME; ?></label>
          <div class="controls col-sm-6">

<?php
  echo osc_draw_input_field('firstname', (!$OSCOM_Customer->hasDefaultAddress() ? $OSCOM_Customer->getFirstName() : null), '  id="entry_first_name" placeholder="'.ENTRY_FIRST_NAME.'"');
  if (($OSCOM_Customer->getCustomersGroupID() == 0 && ENTRY_FIRST_NAME_MIN_LENGTH > 0) || ($OSCOM_Customer->getCustomersGroupID() != 0 && ENTRY_FIRST_NAME_PRO_MIN_LENGTH > 0 ) ) {
    echo '&nbsp;' . (osc_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="text-warning">' . ENTRY_FIRST_NAME_TEXT . '</span>': '');
  }
?>
          </div>
        </div>
      </div>

      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_LAST_NAME; ?></label>
          <div class="controls col-sm-6">
<?php
  echo osc_draw_input_field('lastname', (!$OSCOM_Customer->hasDefaultAddress() ? $OSCOM_Customer->getLastName() : null), ' id="entry_last_name" placeholder="'.ENTRY_LAST_NAME.'"');
  if ( ($OSCOM_Customer->getCustomersGroupID() == 0 && ENTRY_LAST_NAME_MIN_LENGTH > 0) || ($OSCOM_Customer->getCustomersGroupID() != 0 && ENTRY_LAST_NAME_PRO_MIN_LENGTH > 0 ) ) {
    echo '&nbsp;' . (osc_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="text-warning">' . ENTRY_LAST_NAME_TEXT . '</span>': '');
  }
?>
          </div>
        </div>
      </div>

    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="telephone"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('entry_telephone', null, ' id="telephone" placeholder="'.ENTRY_TELEPHONE_NUMBER.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;<?php echo (osc_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '&nbsp;<span class="text-warning">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></span>
        </div>
      </div>
    </div>

    
      <div class="contentText">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="row">
        <div class="form-group has-feedback">
            <label class="control-label col-sm-4" for="street_address"><?php echo ENTRY_STREET_ADDRESS; ?></label>
            <div class="controls col-sm-6">
<?php echo osc_draw_input_field('street_address', null, ' id="street_address" placeholder="'.ENTRY_STREET_ADDRESS.'"');
  if ( ($OSCOM_Customer->getCustomersGroupID() == 0 && ENTRY_STREET_ADDRESS_MIN_LENGTH > 0) || ($OSCOM_Customer->getCustomersGroupID() != 0 && ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH > 0 ) ) {
    echo '&nbsp;' . (osc_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="text-warning">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': '');
  }
?>
            </div>
          </div>
        </div>
<?php
  if (ACCOUNT_SUBURB == 'true') {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="suburb"><?php echo ENTRY_SUBURB; ?></label>
        <div class="controls col-sm-6">
          <?php echo osc_draw_input_field('suburb', null, 'id="suburb" placeholder="'.ENTRY_SUBURB.'"') . (osc_not_null(ENTRY_SUBURB_TEXT) ? '&nbsp;<span class="text-warning">' . ENTRY_SUBURB_TEXT . '</span>': ''); ?>
        </div>
      </div>
    </div>
<?php
  }
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="postcode"><?php echo ENTRY_POST_CODE; ?></label>
        <div class="controls col-sm-6">
<?php
  echo osc_draw_input_field('postcode', null, ' id="postcode" placeholder="'.ENTRY_POST_CODE.'"');
  if ( ($OSCOM_Customer->getCustomersGroupID() == 0 && ENTRY_POSTCODE_MIN_LENGTH > 0) || ($OSCOM_Customer->getCustomersGroupID() != 0 && ENTRY_POSTCODE_PRO_MIN_LENGTH > 0 ) ) {
    echo '&nbsp;' . (osc_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="text-warning">' . ENTRY_POST_CODE_TEXT . '</span>': '');
  }
?>
        </div>
      </div>
    </div>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="fform-group has-feedback">
        <label class="control-label col-sm-4" for="city"><?php echo ENTRY_CITY; ?></label>
        <div class="controls col-sm-6">
<?php
  echo osc_draw_input_field('city', null, ' id="city" placeholder="'.ENTRY_CITY.'"');
  if ( ($OSCOM_Customer->getCustomersGroupID() == 0 && ENTRY_CITY_MIN_LENGTH > 0) || ($OSCOM_Customer->getCustomersGroupID() != 0 && ENTRY_CITY_PRO_MIN_LENGTH > 0 ) ) {
    echo '&nbsp;' . (osc_not_null(ENTRY_CITY_TEXT) ? '<span class="text-warning">' . ENTRY_CITY_TEXT . '</span>': '');
  }
?>
        </div>
      </div>
    </div>
<?php
  if (ACCOUNT_STATE == 'true') {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="state"><?php echo ENTRY_STATE; ?></label>
        <div class="controls col-sm-6">

<?php
    if ($process == true) {
      if ($entry_state_has_zones == true) {
        $zones_array = array();
        $zones_query = osc_db_query("select zone_name from zones
                                     where zone_country_id = '" . (int)$country . "' 
                                     order by zone_name
                                   ");
        while ($zones_values = osc_db_fetch_array($zones_query)) {
          $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
        }
        echo osc_draw_pull_down_menu('state', $zones_array);
      } else {
        echo osc_draw_input_field('state', null, 'id="postcode" placeholder="'.ENTRY_STATE.'"');
      }
    } else {
      echo osc_draw_input_field('state', null, 'id="postcode" placeholder="'.ENTRY_STATE.'"');
    }

    if ( (osc_not_null(ENTRY_STATE_TEXT) && ENTRY_STATE_MIN_LENGTH > 0 && $OSCOM_Customer->getCustomersGroupID() == 0) || (osc_not_null(ENTRY_STATE_TEXT) && ENTRY_STATE_PRO_MIN_LENGTH > 0 && $OSCOM_Customer->getCustomersGroupID() != 0) ) {
    echo '&nbsp;<span class="text-warning">' . ENTRY_STATE_TEXT .'</span>';
  }
?>
        </div>
      </div>
    </div>

<?php
  }
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-4" for="country"><?php echo ENTRY_COUNTRY; ?></label>
        <div class="controls col-sm-6">
          <?php echo osc_get_country_list('country', STORE_COUNTRY) . '&nbsp;' . (osc_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="text-warning">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?>
        </div>
      </div>
    </div>
  </div>
</div>
