<?php
/**
 * products_listing.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */

  class ProductsListing {

    protected $products_id;
    protected $products_name;
    protected $products_short_description;
    protected $products_stock;
    protected $image_new_arrival;
    protected $products_flash_discount;
    protected $product_price;
    protected $products_quantity_unit;
    protected $min_order_quantity_products_display;
    protected $input_quantity;
    protected $button_small_view_details;
    protected $submit_button;
    protected $products_image;
    protected $products_model;
    protected $products_manufacturers;
    protected $product_price_kilo;
    protected $products_date_available;
    protected $products_only_shop;
    protected $products_only_web;
    protected $products_packaging;
    protected $products_shipping_delay;
    protected $products_tag;
    protected $products_volume;
    protected $products_weight;
    protected $products_weight_pounds;
    protected $buy_button;
    protected $delete_word;
    protected $size_button;
    protected $products_dimension;
    protected $submit_button_view;

/**
 * Products quantity
 *
 * @param string
 * @return string $products_quantity, products quantity
 * @access private
 */
    Private function setProductsQuantity() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_quantity
                                            from :table_products
                                            where products_status = :products_status
                                            and products_id = :products_id
                                           ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status',   '1' );


      $Qproducts->execute();

      $products = $Qproducts->fetch();

      $products_quantity = $products['products_quantity'];

      return $products_quantity;
    }

/**
 * Display Products quantity
 *
 * @param string
 * @return string $products_quanitity,  products quantity
 * @access public
 */
    Public function getProductsQuantity() {
      return $this->setProductsQuantity();
    }

/**
  * Number of products
  *
  * @param string
  * @return string $product_check['total'], products total
  * @access private
*/
    Private function setProductsCount() {
      global $OSCOM_PDO, $products_id;

      $QproductCheck = $OSCOM_PDO->prepare('select count(*) as total
                                            from :table_products p,
                                                 :table_products_description pd
                                            where p.products_status = :products_status
                                            and p.products_id = :products_id
                                            and pd.products_id = p.products_id
                                            and pd.language_id = :language_id
                                           ');

      $QproductCheck->bindInt(':products_id',   (int)$products_id );
      $QproductCheck->bindInt(':language_id',   (int)$_SESSION['languages_id'] );
      $QproductCheck->bindValue(':products_status',   '1' );

      $QproductCheck->execute();

      return $QproductCheck->valueInt('total');
    }

/**
  * Number of products
  *
  * @param string
  * @return string $product_check['total'], products total
  * @access public
*/
      Public function getProductsCount() {
        return $this->setProductsCount();
      }


/**
  * products name
  *
  * @param string
  * @return string $products_name, name of the product
  * @access private
*/
    Private function setProductsName() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_name
                                        from :table_products p,
                                             :table_products_description pd
                                        where p.products_status = :products_status
                                        and p.products_id = :products_id
                                        and pd.products_id = p.products_id
                                        and pd.language_id = :language_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products_name = osc_output_string_protected($Qproducts->value('products_name'));

      return $products_name;
    }

/**
  * products name
  *
  * @param string
  * @return string $products_name, name of the product
  * @access public
*/
      Public function getProductsName() {
        return $this->setProductsName();
      }

/**
 * products image
 *
 * @param string
 * @return string $products_image, image of the product
 * @access private
 */
    Private function setProductsImage() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_image
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products = $Qproducts->fetch();

      $products_image = osc_output_string_protected($products['products_image']);

      return $products_image;
    }

/**
 * display products image
 *
 * @param string
 * @return string $products_image, image of the product
 * @access public
 */
    Public function getProductsImage() {
      if ($this->setProductsImage() == '') {
        return false;
      } else {
        return $this->setProductsImage();
      }
    }


/**
 * products image medium
 *
 * @param string
 * @return string $products_image_medium, image medium of the product
 * @access private
 */
    Private function setProductsImageMedium() {
      global $OSCOM_PDO, $products_id;


      $Qproducts = $OSCOM_PDO->prepare('select products_image_medium
                                        from :table_products
                                        where products_status = 1
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );

      $Qproducts->execute();

      $products = $Qproducts->fetch();

      $products_image_medium = osc_output_string_protected($products['products_image_medium']);

      return $products_image_medium;
    }

/**
 * display products image medium
 *
 * @param string
 * @return string $products_image_medium, image medium of the product
 * @access public
 */
    Public function getProductsImageMedium() {
      if ($this->setProductsImageMedium() == '') {
        return false;
      } else {
        return $this->setProductsImageMedium();
      }
    }


/**
  * display date available
  *
  * @param string
  * @return string $products_date_available,  product date available
  * @access private
*/
    Private function setProductsDateAvailable() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_date_available
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id);
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();


      $products_date_available = $Qproducts->value('products_date_available');

      return osc_output_string_protected($products_date_available);
    }


/**
 * display date available
 *
 * @param string
 * @return string $products_name, name of the product
 * @access public
 */
      Public function getProductsDateAvailable() {
        return $this->setProductsDateAvailable();
      }


/**
   * Short products description
  *
  * @param string
   * @return string $description_summary, short description of the product
  * @access private
*/
    Private function setProductsShortDescription() {
      global $OSCOM_PDO, $products_id, $delete_word, $products_short_description_number;

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_description_summary
                                        from :table_products p,
                                             :table_products_description pd
                                        where p.products_status = :products_status
                                        and p.products_id = :products_id
                                        and pd.products_id = p.products_id
                                        and pd.language_id = :language_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->bindValue(':products_status',   '1' );

      $Qproducts->execute();

      $products = $Qproducts->fetch();

      $description_summary = $products['products_description_summary'];

      if ($products_short_description_number > 0) {
        $short_description = substr($description_summary, $delete_word, $products_short_description_number);
        $description_summary = osc_break_string(osc_output_string_protected($short_description), $products_short_description_number, '-<br />') . ((strlen($description_summary) >= $products_short_description_number-1) ? ' ...' : '');
      } else {
        $description_summary = '';
      }

      return $description_summary;
    }


/**
  * display products description
  *
  * @param string
  * @return string $products_description, description of the product
  * @access public
*/
    Public function getProductsShortDescription() {
      return $this->setProductsShortDescription();
    }


/**
  * products dimension
  *
  * @param string
  * @return string $products_dimension, dimension of the product
  * @access private
*/
    Private function setProductsDimension() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select  products_dimension_width,
                                                products_dimension_height,
                                                products_dimension_depth
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products = $Qproducts->fetch();


// Affichage de la dimension du produit
      if (($products['products_dimension_width']  != '0.00' || $products['products_dimension_width']   != '0') ||
        ($products['products_dimension_height'] != '0.00' || $products['products_dimension_height'] != '0') ||
        ($products['products_dimension_depth']  != '0.00' || $products['products_dimension_depth']  != '0')
      ) {

        if ($products['products_dimension_width'] == '0.00') {
          $products_dimension_width = '';
        } else {
          $products_dimension_width = $products['products_dimension_width'];
        }

        if ($products['products_dimension_height'] == '0.00') {
          $products_dimension_height = '';
        } else {
          $products_dimension_height = $products['products_dimension_height'];
        }

        if ($products['products_dimension_depth'] == '0.00') {
          $products_dimension_depth = '';
        } else {
          $products_dimension_depth = ' x ' . $products['products_dimension_depth'];
        }

        if (osc_not_null($products['products_dimension_width']) || osc_not_null($products['products_dimension_height'])) $separator = ' x ';
        $products_dimension =  osc_output_string_protected($products_dimension_width  . $separator . $products_dimension_height  . $products_dimension_depth . ' ' .  $products['products_dimension_type']);
      }

      return  osc_output_string_protected($products_dimension);
    }

/**
 * display products dimension
 *
 * @param string
 * @return string $products_dimension, dimension of the product
 * @access public
*/
      Public function getProductsDimension() {
        return $this->setProductsDimension();
      }

/**
  * products Manufacturer
  *
  * @param string
  * @return string $products_manuacturer, manufacturer of the product
  * @access private
*/

    Private function setProductsManufacturer() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select manufacturers_id
                                        from :table_products
                                        where  products_id = :products_id
                                        limit 1
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );

      $Qproducts->execute();
      $products = $Qproducts->fetch();

      $Qmanufacturer = $OSCOM_PDO->prepare('select m.manufacturers_name
                                            from :table_manufacturers m,
                                                 :table_products p
                                            where m.manufacturers_id = :manufacturers_id
                                            and p.products_id = :products_id
                                          ');
      $Qmanufacturer->bindInt(':manufacturers_id', (int)$products['manufacturers_id']);
      $Qmanufacturer->bindInt(':products_id', (int)$products_id );

      $Qmanufacturer->execute();

      if ( $Qmanufacturer->fetch() !== false ) {
        $manufacturer_search = '<a href="'.osc_href_link('advanced_search_result.php', 'keywords='. osc_output_string_protected(utf8_decode($Qmanufacturer->value('manufacturers_name').'&search_in_description=1'), 'NONSSL')) . '">'. $Qmanufacturer->value('manufacturers_name'). '</a>';
      }

      return  $manufacturer_search;
    }

/**
  * display products manufacturer
  *
  * @param string
  * @return string $products_manufacturer, manufacturer of the product
  * @access public
*/
    Public function getProductsManufacturer() {
      return $this->setProductsManufacturer();
    }

/**
* display an message for the product new arrival
*
* @param string $osc_icon_new_arrival_products_display, the image for the new arrival
* @access private
*/

    Private function setProductsNewArrival() {
      global $OSCOM_PDO, $size_button, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_date_added
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');
      $Qproducts->bindInt(':products_id', (int)$products_id  );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products_date_added = $Qproducts->value('products_date_added');

      if (empty($size_button)) {
        $size_button = null;
      }

//  2592000 = 30 days in the unix timestamp format
      $day_new_products = 86400 * DAY_NEW_PRODUCTS_ARRIVAL;
      $today_time = time();

      if ( ($today_time - strtotime($products_date_added)) < $day_new_products) {

        $product_button_new_arrival = osc_draw_button(TEXT_ICON_NEW_PRODUCT, null, null, 'new', null, $size_button);
        $icon_new_arrival_products = '&nbsp' . $product_button_new_arrival;
      }

      return $icon_new_arrival_products;
    }


/**
  * display image ou button type products new arrival (news)
  *
  * @param string
  * @return string $icon_new_arrival_products, product new arrival
  * @access public
*/
      Public function getProductsNewArrival() {
        return $this->setProductsNewArrival();
      }


/**
  * products only shop in product info
  *
  * @param string
  * @return string $products_only_shop, only shop concerning the product
  * @access pRIVATE
*/
    Private function setProductsOnlyTheShop() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select  products_only_shop
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id  );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products_only_shop = $Qproducts->value('products_only_shop');

      return $products_only_shop;
    }


/**
  * display sell only the shop
  *
  * @param string
  * @return string $products_only_shop, sell only the shop
  * @access public
*/
      Public function getProductsOnlyTheShop() {
        return $this->setProductsOnlyTheShop();
      }

/**
  * products only on the web site
  *
  * @param string
  * @return string $products_only_online, products only on the web site
  * @access public
*/
    function setProductsOnlyOnTheWebSite() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_only_online
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                     ');

      $Qproducts->bindInt(':products_id', (int)$products_id  );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products_only_online = $Qproducts->value('products_only_online');

      return $products_only_online;
    }

/**
  * display sell only the shop
  *
  * @param string
  * @return string $products_only_online, products only on the web site
  * @access public
*/
      Public function getProductsOnlyOnTheWebSite() {
        return $this->setProductsOnlyOnTheWebSite();
      }


/**
  * products packaging
  *
  * @param string
  * @return string $products_packaging, packaging concerning the product
  * @access private
*/
    Private function setProductsPackaging() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_packaging
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');
      $Qproducts->bindInt(':products_id',  (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();


      $products_packaging = osc_output_string_protected($Qproducts->value('products_packaging'));

      return $products_packaging;
    }

/**
  * display products packaging
  *
  * @param string
  * @return string $products_packaging, products packaging
  * @access public
*/
      Public function getProductsPackaging() {
        return $this->setProductsPackaging();
      }

/**
  * the products quantity unit type
  *
  * @param string  $products_quantity_unit_id, $language_id
  * @return string $products_quantity_unit_['products quantity unit_title'],  name of the he products quantity unit
  * @access private
*/

    Private function setProductQuantityUnitType() {
      global $OSCOM_PDO, $products_id;

      if (empty($language)) $language_id = $_SESSION['languages_id'];

      $QproductsUnitType = $OSCOM_PDO->prepare('select pq.products_quantity_unit_title
                                                from :table_products p,
                                                     :table_products_quantity_unit pq
                                                where pq.products_quantity_unit_id = p.products_quantity_unit_id
                                                and p.products_id = :products_id
                                                and language_id = :language_id
                                                and p.products_status = :products_status
                                           ');
      $QproductsUnitType->bindInt(':products_id',  (int)$products_id  );
      $QproductsUnitType->bindInt(':language_id',  (int)$language_id );
      $QproductsUnitType->bindValue(':products_status', '1' );

      $QproductsUnitType->execute();

      return $QproductsUnitType->value('products_quantity_unit_title');
    }

/**
  * display products quantity unit type
  *
  * @param string
  * @return string $products_quantity_unit['products_quantity_unit_title'], products quantity unit type
  * @access public
*/
      Public function getProductQuantityUnitType() {
        return $this->setProductQuantityUnitType();
      }


/**
  * products_shipping_delay in product info
  *
  * @param string
  * @return string $products_shipping_delay, delay of the shipping
  * @access private
*/

    Private function setProductsShippingDelay() {
      global $OSCOM_PDO, $products_id;

      if (empty($language)) $language_id = $_SESSION['languages_id'];

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_shipping_delay
                                        from :table_products p,
                                             :table_products_description pd
                                        where p.products_status = :products_status
                                            and p.products_id = :products_id
                                            and pd.products_id = p.products_id
                                            and pd.language_id = :language_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id  );
      $Qproducts->bindInt(':language_id', (int)$language_id );
      $Qproducts->bindValue(':products_status',   '1' );
      $Qproducts->execute();

      $products_shipping_delay = $Qproducts->value('products_shipping_delay');

      if (empty($products['products_shipping_delay'])) {
        $products_shipping_delay = DISPLAY_SHIPPING_DELAY;
      }

      return $products_shipping_delay;
    }


/**
  * products_shipping_delay in product info
  *
  * @param string
  * @return string $products_shipping_delay, delay of the shipping
  * @access private
*/
      Public function getProductsShippingDelay() {
        return $this->setProductsShippingDelay();
      }

/**
* products_head_tag
*
* @param string
* @return string $products_head_tag, tag of the product
* @access private
*/
    Private function setProductsHeadTag() {
      global $OSCOM_PDO, $products_id;

      if (empty($language)) $language_id = $_SESSION['languages_id'];

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_head_tag
                                          from :table_products p,
                                               :table_products_description pd
                                          where p.products_status = :products_status
                                          and p.products_id = :products_id
                                          and pd.products_id = p.products_id
                                          and pd.language_id = :language_id
                                         ');

      $Qproducts->bindValue(':products_status',   '1' );
      $Qproducts->bindInt(':products_id', (int)$products_id  );
      $Qproducts->bindInt(':language_id', (int)$language_id );

      $Qproducts->execute();

      $products_head_tag = osc_output_string_protected($Qproducts->value('products_head_tag'));

      return $products_head_tag;
    }


/**
  * products_shipping_delay in product info
  *
  * @param string
  * @return string $products_shipping_delay, delay of the shipping
  * @access public
*/
      Public function getProductsHeadTag() {
        return $this->setProductsHeadTag();
      }


/**
  * products_url in product info
  *
  * @param string
  * @return string $products_url, url of the product (manufacturer)
  * @access private
*/
    function setProductsURLManufacturer() {
      global $OSCOM_PDO, $products_id;

      if (empty($language)) $language_id = $_SESSION['languages_id'];

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_url
                                        from :table_products p,
                                             :table_products_description pd
                                        where p.products_status = :products_status
                                        and p.products_id = :products_id
                                        and pd.products_id = p.products_id
                                        and pd.language_id = :language_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindInt(':language_id', (int)$language_id );
      $Qproducts->bindValue(':products_status',   '1' );

      $Qproducts->execute();

      $products_url = $Qproducts->value('products_url');

      return $products_url;
    }


/**
  * Display products_url in product info
  *
  * @param string
  * @return string $products_url, url of the product (manufacturer)
  * @access private
*/
      Public function getProductsURLManufacturer() {
        return $this->setProductsURLManufacturer();
      }


/**
  * products in shop and web
  *
  * @param string
  * @return string $products_web, sell web and shop
  * @access private
*/
    private function sepProductsWebAndShop() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select  products_only_shop,
                                                products_only_online
                                          from :table_products
                                          where products_status = :products_status
                                          and products_id = :products_id
                                         ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      if ($Qproducts->value('products_only_shop') != 1 && $Qproducts->value('products_only_online') != 1) {
        $products_web = '';
      }
      return $products_web;
    }


/**
  * Display products in shop and web
  *
  * @param string
  * @return string $products_web, sell web and in the shop
  * @access public
*/
      Public function getProductsWebAndShop()  {
        return $this->sepProductsWebAndShop();
      }


/**
  * products_volume in product info
  *
  * @param string
  * @return string $products_volume, volume of the product
  * @access private
*/
    Private function setproductsVolume() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_volume
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products_volume = osc_output_string_protected($Qproducts->value('products_volume'));

      return $products_volume;
    }

/**
  * Display products_volume
  *
  * @param string
  * @return string $products_volume, volume of the product
  * @access public
*/

      Public function getProductsVolume()  {
        return $this->setproductsVolume();
      }


/**
  * products_weight
  *
  * @param string
  * @return string $products_weight, weight of the product
  * @access public
*/
    Private function setProductsWeightInKg() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_weight
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products = $Qproducts->fetch();

      $products_weight = $Qproducts->value('products_weight');

      if ($products_weight == '0.00') {
        $products_weight = '';
      } else {
        $products_weight = round($products_weight,2);
      }

      return $products_weight;
    }


/**
  * Display products_weight
  *
  * @param string
  * @return string $products_weight, weight of the product
  * @access public
*/
      Public function getProductsWeightInKg()  {
        return $this->setProductsWeightInKg();
      }

/**
  * products_weight_pounds in product info
  *
  * @param string
  * @return string $products_weight, weight of the product
  * @access private
*/
    Private function setProductsWeightInPounds() {
      global $OSCOM_PDO, $products_id;

      $Qproducts = $OSCOM_PDO->prepare('select products_weight_pounds
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products_weight_pounds = $Qproducts->value('products_weight_pounds');

      if ($products_weight_pounds == '0.00') {
        $products_weight_pounds = '';
      } else {
        $products_weight_pounds = round($products_weight_pounds,2);
      }

      return $products_weight_pounds;
    }


/**
  * Display products_weight pounds
  *
  * @param string
  * @return string $products_weight_pounds, weight of the product
  * @access public
*/
    Public function getProductsWeightInPounds()  {
      return $this->setProductsWeightInPounds();
    }


/**
  * Display the normal price by kilo
  *
  * @param string $product_price_kilo_display, the normal price by kilo
  * @access public
*/

    Private function setProductsPriceByKilo() {
      global $OSCOM_PDO, $OSCOM_Customer, $products_id;

// Call currencies class
      $currencies = new currencies;

      $Qproducts = $OSCOM_PDO->prepare('select products_price,
                                               products_price_kilo
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':products_status', '1' );

      $Qproducts->execute();

      $products = $Qproducts->fetch();

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        $products_price = $this->getCustomersGroupPrice();
      } else {
        $products_price = $products['products_price'];
      }


      $products_price_kilo = $products['products_price_kilo'];
      $products_weight = $this->setProductsWeightInKg($products_id);

      if ($products_price_kilo == '1' && $products_weight != '') {
        $product_price_kilo_display =  $currencies->display_price(round($products_price / $products_weight , 2), osc_get_tax_rate($this->getProductsTaxClassId($products_id)) );
      } else {
        $product_price_kilo_display = '';
      }

      if ((PRICES_LOGGED_IN == 'true') && !$OSCOM_Customer->isLoggedOn() ) {
        $product_price_kilo_display = '';
      }


      if (NOT_DISPLAY_PRICE_ZERO == 'false' && $products_price == '0') {
        $product_price_kilo_display = '';
      }

      return $product_price_kilo_display;
    }

/**
  * Display the normal price by kilo
  *
  * @param string $product_price_kilo_display, the normal price by kilo
  * @access public
*/

      Public function getProductsPriceByKilo()  {
        return $this->setProductsPriceByKilo();
      }

/**
  * the products quantity unit title
  *
  * @param string  $products_quantity_unit_id, $language_id
  * @return string $products_quantity_unit_['products quantity unit_title'],  name of the he products quantity unit
  * @access private
*/

    Private function setProductsQuantityByUnit() {
      global $products_id, $OSCOM_PDO;

      if (empty($language)) $language_id = $_SESSION['languages_id'];

      $QproductsQuantityUnit = $OSCOM_PDO->prepare('select pq.products_quantity_unit_title
                                                    from :table_products_quantity_unit pq,
                                                         :table_products p
                                                    where pq.products_quantity_unit_id = p.products_quantity_unit_id
                                                    and p.products_id = :products_id
                                                    and language_id = :language_id
                                                    ');
      $QproductsQuantityUnit->bindInt(':language_id', (int)$language_id);
      $QproductsQuantityUnit->bindInt(':products_id',(int)$products_id);
      $QproductsQuantityUnit->execute();


      return $QproductsQuantityUnit->value('products_quantity_unit_title');
    }

/**
  * the products quantity unit title
  *
  * @param string  $products_quantity_unit_id, $language_id
  * @return string $products_quantity_unit_['products quantity unit_title'],  name of the he products quantity unit
  * @access public
*/
      Public function getProductsQuantityByUnit()  {
        return $this->setProductsQuantityByUnit();
      }


// ---------------------------------------------------------------------------------------------------------------------------------------
// B2B
// ---------------------------------------------------------------------------------------------------------------------------------------



/**
 * products model
 *
 * @param string
 * @return string $products_model, model of the product
 * @access public
 */

    Private function setProductsModel()  {
      global $OSCOM_Customer,$OSCOM_PDO, $products_id;

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {

        $Qproducts = $OSCOM_PDO->prepare('select g.products_model_group
                                            from :table_products p left join :table_products_groups g on p.products_id = g.products_id
                                            where p.products_status = :products_status
                                            and g.customers_group_id = :customers_group_id
                                            and g.products_group_view = :products_group_view
                                            and p.products_id = :products_id
                                           ');

        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
        $Qproducts->bindValue(':products_group_view',  '1' );
        $Qproducts->bindValue(':products_status',   '1' );

        $Qproducts->execute();

        $products = $Qproducts->fetch();

        if (!empty($products['products_model_group'])) {

          $products_model = osc_output_string_protected($products['products_model_group']);

      } else {

        $Qproducts = $OSCOM_PDO->prepare('select products_model
                                            from :table_products
                                            where products_status = :products_status
                                            and products_view = :products_view
                                            and products_id = :products_id
                                           ');
        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindValue(':products_view', '1' );
        $Qproducts->bindValue(':products_status', '1' );
          
          $Qproducts->execute();

          $products = $Qproducts->fetch();

          $products_model = osc_output_string_protected( $products['products_model']);
        }

      } else {

        $Qproducts = $OSCOM_PDO->prepare('select products_model
                                            from :table_products
                                            where products_status = :products_status
                                            and products_view = :products_view
                                            and products_id = :products_id
                                           ');
        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindValue(':products_view', '1' );
        $Qproducts->bindValue(':products_status', '1' );

        $Qproducts->execute();

        $products = $Qproducts->fetch();

        $products_model = osc_output_string_protected($products['products_model']);
      }

      return osc_output_string_protected($products_model);
    }

/**
 * Display products model
 *
 * @param string
 * @return string $products_model, model of the product
 * @access public
 */
      Public function getProductsModel()  {
        return $this->setProductsModel();
      }


/**
  * Auto activate flash discount in product info if avalaible
  *
  * @param string
  * @return string $flash_discount, the product flash discount based on end special end date
  * @access private
*/
    Private function setProductsFlashDiscount() {
      global $OSCOM_PDO, $OSCOM_Customer, $products_id;

      if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B

        $QflashDiscount = $OSCOM_PDO->prepare('select specials_id,
                                                      products_id,
                                                      scheduled_date,
                                                      expires_date
                                              from :table_specials
                                              where products_id = :products_id
                                              and customers_group_id > :customers_group_id
                                              and status = :status
                                              and flash_discount =:flash_discount
                                            ');
        $QflashDiscount->bindInt(':products_id', (int)$products_id  );
        $QflashDiscount->bindValue(':customers_group_id', 0);
        $QflashDiscount->bindValue(':status', 1);
        $QflashDiscount->bindValue(':flash_discount', 1);

      } else {

        $QflashDiscount = $OSCOM_PDO->prepare('select specials_id,
                                                      products_id,
                                                      scheduled_date,
                                                      expires_date,
                                                      flash_discount
                                              from :table_specials
                                              where products_id = :products_id
                                              and customers_group_id = :customers_group_id
                                              and status = :status
                                              and flash_discount =:flash_discount
                                            ');
        $QflashDiscount->bindInt(':products_id', (int)$products_id  );
        $QflashDiscount->bindValue(':customers_group_id', 0);
        $QflashDiscount->bindValue(':status', 1);
        $QflashDiscount->bindValue(':flash_discount', 1);
      }


      $QflashDiscount->execute();
      $flashDiscount = $QflashDiscount->fetch();

      if (date('Y-m-d H:i:s') >= $flashDiscount['scheduled_date'] && date('Y-m-d H:i:s') <= $flashDiscount['expires_date'] ) {

        $start = new DateTime($flashDiscount['scheduled_date'] );
        $end = new DateTime($flashDiscount['expires_date']);
        $diff = $start->diff($end);
        $diffa = $start->diff($end);
        $diffm = $start->diff($end);
        $diffh = $start->diff($end);
        $diffi= $start->diff($end);
        $diffs = $start->diff($end);


        if (!$days) $days =  $diff->d. DAYS;
        if (!$hours) $hours =  $diffh->h. HOURS;
        if (!$minutes) $minutes =  $diffi->i. MINUTES;
        if (!$secondes) $secondes = $diffs->s. SECONDES;


        $products_flash_discount = '';

        if ($days > 1 ) {
          $products_flash_discount = $days . $hours . $minutes .  $secondes;
        } elseif ($hours > 1 ) {
          $products_flash_discount = $hours . $minutes . $secondes;
        } elseif ($minutes > 1 ) {
          $products_flash_discount =  $minutes . $secondes;
        } else {
          $products_flash_discount = $secondes;
        }
      }

      $products_flash_discount = osc_output_string_protected($products_flash_discount);

      return $products_flash_discount;
    }


/**
 * Display flash discount
 *
 * @param string
 * @return string $products_flash_discount, flash discount
 * @access public
 */
      Public function getProductsFlashDiscount()  {
        return $this->setProductsFlashDiscount();
      }

//----------------------------------------------------------------------------------------------------------------------------
// Quantity
//----------------------------------------------------------------------------------------------------------------------------


/**
 * Display the product quantity unit title of the customer group
 *
 * @param string $products_quantity_unit_title
 * @return $products_group_quantity_unit_title, the title of the product unit group (unite,douzaine....)
 * @access private
 */

    Private function setProductQuantityUnitTypeCustomersGroup() {
      global $OSCOM_PDO, $OSCOM_Customer, $products_id;

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {

        $QcustomerGroupPrice = $OSCOM_PDO->prepare('select products_quantity_unit_id_group,
                                                           products_quantity_fixed_group
                                                    from :table_products_groups
                                                    where products_id = :products_id
                                                    and customers_group_id = :customers_group_id
                                    ');
                                    
        $QcustomerGroupPrice->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
        $QcustomerGroupPrice->bindInt(':products_id', (int)$products_id  );
        $QcustomerGroupPrice->execute();

        $customer_group_price =  $QcustomerGroupPrice->fetch();

        $products_quantity_unit_id_group = $customer_group_price['products_quantity_unit_id_group'];
        $products_quantity_fixed_group = $customer_group_price['products_quantity_fixed_group'];

      } else {
        $products_quantity_unit_id_group = '';
        $products_quantity_fixed_group =  '';
      }

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {

        if (empty($language)) $language_id = $_SESSION['languages_id'];

        $QproductsQuantityUnit = $OSCOM_PDO->prepare('select products_quantity_unit_id,
                                                             language_id,
                                                             products_quantity_unit_title
                                                    from :table_products_quantity_unit
                                                    where products_quantity_unit_id = :products_quantity_unit_id
                                                    and language_id = :language_id
                                                  ');

        $QproductsQuantityUnit->bindInt(':products_quantity_unit_id', (int)$products_quantity_unit_id_group);
        $QproductsQuantityUnit->bindInt(':language_id', (int)$language_id );
        $QproductsQuantityUnit->execute();

        $products_quantity_unit = $QproductsQuantityUnit->fetch();

        $products_group_quantity_unit_title = '';

        if ($products_quantity_unit_id_group == 0 ) {
          $products_quantity_fixed_group = '';
          $products_quantity_fixed_group = '';
        } else {
          $products_group_quantity_unit_title = osc_output_string_protected($products_quantity_fixed_group)  .' ' .  $products_quantity_unit['products_quantity_unit_title'];
        }
      } else {
        $products_group_quantity_unit_title = '';
      }

      return $products_group_quantity_unit_title;
    }

/**
 * Display the product quantity unit title of the customer group
 *
 * @param string
 * @return $products_group_quantity_unit_title,, the title of the product unit group
 * @access private
 */

      Public function getProductQuantityUnitTypeCustomersGroup()  {
        return $this->setProductQuantityUnitTypeCustomersGroup();
      }


/**
 * Return a product's minimum order quantity if available and min order quantity > 1
 * in different formular ormis shopping cart -function included in application_top
 *
 * @param string $min_quantity_order,
 * @access private
 * osc_get_products_min_order_quantity
 */
    Private function setProductsMinimumQuantity() {
      global $OSCOM_PDO, $OSCOM_Customer, $products_id;

      $products_id = osc_get_prid( (int)$products_id  );
      $customers_group_id =  $OSCOM_Customer->getCustomersGroupID();

      if ($customers_group_id  == 0) {
        $QproductMinOrder = $OSCOM_PDO->prepare('select products_min_qty_order
                                                    from :table_products
                                                    where products_id = :products_id
                                                  ');
        $QproductMinOrder->bindInt(':products_id', $products_id );

        $QproductMinOrder->execute();
        $product_min_order_values = $QproductMinOrder->fetch();

        if ($product_min_order_values['products_min_qty_order'] > 0.1) {
          $min_quantity_order = (int)$product_min_order_values['products_min_qty_order'];
        } else {
          $min_quantity_order = (int)MAX_MIN_IN_CART;
          if (MAX_MIN_IN_CART > MAX_QTY_IN_CART)  {
            $min_quantity_order = (int)MAX_QTY_IN_CART;
          }
        }
      } else {
        $QcustomersGroupMinOrder = $OSCOM_PDO->prepare('select customers_group_quantity_default
                                                          from :table_customers_groups
                                                          where customers_group_id = :customers_group_id
                                                        ');
        $QcustomersGroupMinOrder->bindInt(':customers_group_id', (int)$customers_group_id );

        $QcustomersGroupMinOrder->execute();
        $customers_group_min_order = $QcustomersGroupMinOrder->fetch();


        $QcustomersProductsGroupMinOrder = $OSCOM_PDO->prepare('select products_quantity_fixed_group
                                                                  from :table_products_groups
                                                                  where customers_group_id = :customers_group_id
                                                                ');
        $QcustomersProductsGroupMinOrder->bindInt(':customers_group_id', (int)$customers_group_id );
        $QcustomersProductsGroupMinOrder->execute();
        $customers_products_group_min_order = $QcustomersProductsGroupMinOrder->fetch();

        if ($customers_products_group_min_order['products_quantity_fixed_group'] > 1) {
          $min_quantity_order = $customers_products_group_min_order['products_quantity_fixed_group'];
        } elseif ($customers_group_min_order['customers_group_quantity_default'] > 1) {
          $min_quantity_order = (int)$customers_group_min_order['customers_group_quantity_default'];
        } else {
          $min_quantity_order = 1;
        }
      }

      return $min_quantity_order;
    }


/**
 * Display a product's minimum order quantity if available and min order quantity > 1
 * in different formular ormis shopping cart -function included in application_top
 *
 * @param string $min_quantity_order,
 * @access private
 * osc_get_products_min_order_quantity
 */
      Public function getProductsMinimumQuantity()  {
        return $this->setProductsMinimumQuantity();
      }


/*
  * Minimum quantity take an order for the client
  *
  * @param string $min_order_quantity_products_display, the min of order product qty
  * @access private
 *  osc_b2b_min_order_quantity_products_display
*/
    Private function setProductsMinimumQuantityToTakeAnOrder() {
      global $OSCOM_Customer, $products_id;

      if ($this->getProductsMinimumQuantity($products_id) >= 1 ) {
        if ($this->setProductsOrderView($products_id) != 0 && $OSCOM_Customer->getCustomersGroupID() != 0 ) {
          $min_order_quantity_products_display =  $this->getProductsMinimumQuantity($products_id);
        } elseif ($this->setProductsOrderView($products_id) != 0 && $OSCOM_Customer->getCustomersGroupID() == 0) {
          $min_order_quantity_products_display =  $this->getProductsMinimumQuantity($products_id);
        } else {
          $min_order_quantity_products_display = '';
        }
      } else {
        $min_order_quantity_products_display = '';
      }

// ok
      if (PRICES_LOGGED_IN == 'true' && !$OSCOM_Customer->isLoggedOn() ) {
        $min_order_quantity_products_display = '';
      }

      return $min_order_quantity_products_display;
    }


/*
  * display Minimum quantity take an order for the client
  *
  * @param string $min_order_quantity_products_display, the min of order product qty
  * @access public
 *  osc_b2b_min_order_quantity_products_display
*/
      Public function getProductsMinimumQuantityToTakeAnOrder()  {
        return $this->setProductsMinimumQuantityToTakeAnOrder();
      }


/**
 * Display the quantity for the customer
 *
 * @param string $input_quantity, the quantity for the customer
 * @access private
 * osc_b2b_display_message($submit_button_view)
 */

    Private function setProductsAllowingToInsertQuantity() {
      global $OSCOM_Customer, $products_id;

      if ( ($OSCOM_Customer->getCustomersGroupID() != 0 && $this->setProductsOrderView($products_id)!= 0)  ) {
        $input_quantity = osc_draw_input_field('cart_quantity',(int)$this->setProductsMinimumQuantityToTakeAnOrder(), ' class="input-mini" maxlength="4" size="4" ') .'&nbsp;&nbsp;';
      } else {
        $input_quantity =  '';
      }

      if (($OSCOM_Customer->getCustomersGroupID() == 0) && ($this->setProductsOrderView($products_id) != 0)) {
        if (PRICES_LOGGED_IN == 'false') {
          $input_quantity = osc_draw_input_field('cart_quantity',(int)$this->setProductsMinimumQuantityToTakeAnOrder(), ' class="input-mini" maxlength="4" size="4"') .'&nbsp;&nbsp;';
        } elseif (PRICES_LOGGED_IN == 'true' && $OSCOM_Customer->isLoggedOn() ) {
          $input_quantity = osc_draw_input_field('cart_quantity',(int)$this->setProductsMinimumQuantityToTakeAnOrder(), ' class="input-mini" maxlength="4" size="4"') .'&nbsp;&nbsp;';
        } else {
          $input_quantity = '';
        }
      }

      if ($this->setProductsMinimumQuantityToTakeAnOrder() == 0 && MAX_MIN_IN_CART == 0) {
        $input_quantity = '';
      }

      return $input_quantity;
    }


/**
 * Display the quantity for the customer
 *
 * @param string $input_quantity, the price of the product or not
 * @access public
 * osc_b2b_display_message($submit_button_view)
 */
      Public function getProductsAllowingToInsertQuantity()  {
        return $this->setProductsAllowingToInsertQuantity();
      }



// ------------------------------------------------------------------------------------------------------
// Message && button
// ------------------------------------------------------------------------------------------------------


/**
 * Orders View
 *
 * @param string
 * @return string $products_orders_view, status of orders view
 * @access private
 */
  Private function setProductsOrderView() {
    global $OSCOM_Customer, $OSCOM_PDO, $products_id;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {
      $Qproducts = $OSCOM_PDO->prepare('select  p.products_id,
                                                  g.orders_group_view,
                                                  p.products_date_added
                                              from :table_products p left join :table_products_groups g on p.products_id = g.products_id
                                              where p.products_status = :products_status
                                              and g.customers_group_id = :customers_group_id
                                              and g.products_group_view = :products_group_view
                                              and p.products_archive = :products_archive
                                              and p.products_id = :products_id
                                              order by p.products_date_added DESC
                                             ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
      $Qproducts->bindValue(':products_group_view', '1' );
      $Qproducts->bindValue(':products_status',   '1' );
      $Qproducts->bindValue(':products_archive',   '0' );

    } else {

      $Qproducts = $OSCOM_PDO->prepare('select  products_id,
                                                  orders_view,
                                                  products_date_added
                                              from :table_products
                                              where products_status = :products_status
                                              and orders_view = :orders_view
                                              and products_archive = :products_archive
                                              and products_id = :products_id
                                              order by products_date_added DESC
                                             ');

      $Qproducts->bindInt(':products_id', (int)$products_id );
      $Qproducts->bindValue(':orders_view', '1' );
      $Qproducts->bindValue(':products_status',   '1' );
      $Qproducts->bindValue(':products_archive',   '0' );
    }

    $Qproducts->execute();
    $products = $Qproducts->fetch();

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {
      $products_orders_view = $products['orders_group_view'];
    } else {
      $products_orders_view = $products['orders_view'];
    }

    return $products_orders_view;
  }


/**
 * Display Orders View
 *
 * @param string
 * @return string $products_orders_view, status of orders view
 * @access private
 */

//verifier par private
  Public function getProductsOrderView() {
    return $this->setProductsOrderView();
  }


  /**
 * Display a message in function the customer group applied
 *
 * @param string $product_price_d, the price of the product or not
 * @access private
 * osc_b2b_display_message($submit_button_view)
 */

    private function setProductsAllowingTakeAnOrderMessage() {
      global $OSCOM_Customer, $products_id;

      if ($OSCOM_Customer->getCustomersGroupID() != 0 && $this->getProductsOrderView($products_id) != 1) {
        $submit_button_view = '<div class="submitButtonView">' . NO_ORDERS_GROUP . '</div>';
      } elseif ($OSCOM_Customer->getCustomersGroupID() == 0 && $this->getProductsOrderView($products_id) != 1) {
        $submit_button_view = '<div class="submitButtonView">' . NO_ORDERS_PUBLIC . '</div>';
      }

      return $submit_button_view;
    }


/**
 * Display a message in function the customer group applied
 *
 * @param string $product_price_d, the price of the product or not
 * @access public
 * osc_b2b_display_message($submit_button_view)
 */
      Public function getProductsAllowingTakeAnOrderMessage()  {
        return $this->setProductsAllowingTakeAnOrderMessage();
      }


/**
 * Button buy now
 *
 * @param string $submit_button , the button
 * @access private
 * osc_b2b_display_button($submit_button)
 */

    Private function setProductsBuyButton() {
      global $OSCOM_Customer, $buy_button, $products_id;

      if ((PRICES_LOGGED_IN == 'true' && !$OSCOM_Customer->isLoggedOn())) {
        $buy_button = '';
      } elseif ($this->setProductsOrderView($products_id) == 0 && $OSCOM_Customer->getCustomersGroupID() == 0) {
        $buy_button = '';
      }

      if (PRICES_LOGGED_IN == 'true' && $OSCOM_Customer->isLoggedOn() ) {
        if ($this->setProductsOrderView($products_id) == 0 &&  $OSCOM_Customer->getCustomersGroupID() == 0) {
          $buy_button = '';
        } elseif ($this->setProductsOrderView($products_id) == 0 && $OSCOM_Customer->getCustomersGroupID() != 0) {
          $buy_button = '';
        }
      } elseif (PRICES_LOGGED_IN == 'false' && $OSCOM_Customer->isLoggedOn()) {
        if ($this->setProductsOrderView($products_id) == 0 &&  $OSCOM_Customer->getCustomersGroupID() == 0) {
          $buy_button = '';
        } elseif ($this->setProductsOrderView($products_id) == 0 && $OSCOM_Customer->getCustomersGroupID() != 0) {
          $buy_button = '';
        }
      }
      return $buy_button;
    }

/**
 * Button buy now
 *
 * @param string $submit_button , the button
 * @access public
 */
      Public function getProductsBuyButton()  {
        return $this->setProductsBuyButton();
      }


/**
 * Products exhausted
 * Check if the required stock is available for display a button Product exhausted
 * If insufficent stock is available return a products exhausted image
 * @param string $product_exhausted, the button
 * @access public
 * osc_check_stock_button_product_exhausted($products_id)
 */

    Private function setProductsExhausted() {
      global $OSCOM_Customer, $OSCOM_PDO, $products_id;

      $products_id = osc_get_prid( (int)$products_id );

      $QproductExhausted = $OSCOM_PDO->prepare('select products_quantity
                                                from :table_products
                                                where products_id = :products_id
                                              ');
      $QproductExhausted->bindInt(':products_id', (int)$products_id);

      $QproductExhausted->execute();
      $product_exhausted_values = $QproductExhausted->fetch();

      $product_exhausted_left = $product_exhausted_values['products_quantity'];

      $product_button_exhausted = osc_product_button_exhausted();


      if ($product_exhausted_left < 1 and STOCK_CHECK == 'true' && STOCK_ALLOW_CHECKOUT == 'false' && PRICES_LOGGED_IN == 'false') {
        $product_exhausted = $product_button_exhausted;
      }

      if (PRICES_LOGGED_IN == 'true' && $OSCOM_Customer->getCustomersGroupID() == 0 && !$OSCOM_Customer->isLoggedOn() && $product_exhausted_left < 1 && STOCK_CHECK == 'true' && STOCK_ALLOW_CHECKOUT == 'false') {
        $product_exhausted = ' ';
      } elseif (PRICES_LOGGED_IN == 'true' && $OSCOM_Customer->getCustomersGroupID() !=0 && $product_exhausted_left < 1 && STOCK_CHECK == 'true' and STOCK_ALLOW_CHECKOUT == 'false')  {
        $product_exhausted = $product_button_exhausted;
      } elseif (PRICES_LOGGED_IN == 'true' && $OSCOM_Customer->getCustomersGroupID() == 0 && $OSCOM_Customer->isLoggedOn() && $product_exhausted_left < 1 && STOCK_CHECK == 'true' && STOCK_ALLOW_CHECKOUT == 'false' ) {
        $product_exhausted = $product_button_exhausted;
      }

      return $product_exhausted;
    }


/**
 * Display Products exhausted
 *
 * @param string $product_exhausted, the button
 * @access public
 */
      Public function getProductsExhausted()  {
        return $this->setProductsExhausted();
      }


// =======================================================================================================================================================
// Price & tax
//=======================================================================================================================================================



/**
 * Tax class
 *
 * @param string
 * @return string $products_tax_class_id, tax class
 * @access private
 */
  Private function setProductsTaxClassId() {
    global $OSCOM_PDO, $products_id;

    $Qproducts = $OSCOM_PDO->prepare('select products_tax_class_id
                                        from :table_products
                                        where products_status = :products_status
                                        and products_id = :products_id
                                       ');

    $Qproducts->bindInt(':products_id', (int)$products_id );
    $Qproducts->bindValue(':products_status',   '1' );


    $Qproducts->execute();

    $products = $Qproducts->fetch();

    $products_tax_class_id = $products['products_tax_class_id'];

    return $products_tax_class_id;
  }

/**
 * Display Tax class
 *
 * @param string
 * @return string $products_tax_class_id, status of orders view
 * @access private
 */
  Private function getProductsTaxClassId() {
    return $this->setProductsTaxClassId();
  }

/**
 * Display the price in different mode B2B or not
 *
 * @param string $product_price_d, the price of the product or not
 * @access private
 * osc_b2b_display_price($product_price_d)
 */

  Private function setCustomersPrice() {
    global $OSCOM_Customer;

    if (PRICES_LOGGED_IN == 'false') {
      $product_price = $this->setCalculPrice();
    }

    if ((PRICES_LOGGED_IN == 'true') && (!$OSCOM_Customer->isLoggedOn())) {
      $product_price = ('<a href="' . osc_href_link('login.php', '','SSL') . '">'  . PRICES_LOGGED_IN_TEXT . '</a>&nbsp;' );
    } else {
      $product_price = $this->setCalculPrice();
    }

    return $product_price;
  }


/**
 * Display the price in different mode B2B or not
 *
 * @param string $product_price_d, the price of the product or not
 * @access public
 * osc_b2b_display_price($product_price_d)
 */
  Public function getCustomersPrice()  {
    return $this->setCustomersPrice();
  }


/**
 * Return a product's special price B2B (returns nothing if there is no offer
 * @param string $product_id
 * @return $product['specials_new_products_price'] the special price
 * @access public
 * TABLES: products B2B
 */


    Public function setSpecialPriceGroup() {
      global $OSCOM_PDO, $OSCOM_Customer, $products_id;

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        $Qproducts = $OSCOM_PDO->prepare('select distinct specials_new_products_price
                                           from :table_specials
                                           where products_id = :products_id
                                           and status = :status
                                           and customers_group_id = :customers_group_id
                                          ');
        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
        $Qproducts->bindValue(':status', '1');

      } else {
        $Qproducts = $OSCOM_PDO->prepare('select distinct specials_new_products_price
                                           from :table_specials
                                           where products_id = :products_id
                                           and status = :status
                                           and customers_group_id = :customers_group_id
                                          ');
        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindInt(':customers_group_id', '0' );
        $Qproducts->bindValue(':status', '1');
      }

      $Qproducts->execute();
      $product = $Qproducts->fetch();

      return $product['specials_new_products_price'];
    }



/**
 * Product group price
 *
 * @param string $product_id, id of the procduct
 * @return $products_price, the product price
 * @access public
 * osc_group_price($product_id)
 */

    Private function setGroupPrice() {
      global $price, $OSCOM_Customer, $OSCOM_PDO, $products_id;

// Call currencies class
      $currencies = new currencies;

      $Qproduct = $OSCOM_PDO->prepare('select p.products_id,
                                              p.products_price
                                       from :table_products p
                                       where p.products_status = :products_status
                                       and p.products_id = :products_id
                                       and p.products_archive = :products_archive
                                     ');
      $Qproduct->bindInt(':products_id', (int)$products_id );
      $Qproduct->bindInt(':products_status', '1');
      $Qproduct->bindInt(':products_archive', '0');

      $Qproduct->execute();

      $products = $Qproduct->fetch();

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {

        $customerGroupPrice = $OSCOM_PDO->prepare('select customers_group_price
                                                    from :table_products_groups
                                                    where products_id = :products_id
                                                    and customers_group_id =  :customers_group_id
                                    ');
        $customerGroupPrice->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
        $customerGroupPrice->bindInt(':products_id', (int)$products_id);
        $customerGroupPrice->execute();

       if ($customer_group_price = $customerGroupPrice->fetch() ) {
          if($customer_group_price['price_group_view'] == 1) {
            $products_price = $currencies->display_price($customer_group_price['customers_group_price'], osc_get_tax_rate($this->getProductsTaxClassId($products_id)  ));
          } else {
            $products_price = $currencies->display_price($products['products_price'], osc_get_tax_rate($this->getProductsTaxClassId($products_id) ));
          }
        } else {
          $products_price = $currencies->display_price($products['products_price'], osc_get_tax_rate($this->getProductsTaxClassId($products_id) ));
        }
      } else {
        $products_price = $currencies->display_price($products['products_price'], osc_get_tax_rate($this->getProductsTaxClassId($products_id) ));
      }

      return $products_price;
    }


/**
 * Calcul the different price in function the group
 *
 * @param string $products_price, the price of the product
 * @access private
 * osc_b2b_calcul_price($products_price)
 */

    Private function setCalculPrice() {
      global $OSCOM_PDO, $OSCOM_Customer, $normal_price, $products_id;

// Call currencies class
      $currencies = new currencies;

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        $Qproducts = $OSCOM_PDO->prepare('select g.price_group_view
                                          from :table_products p left join :table_products_groups g on p.products_id = g.products_id
                                          where p.products_status = :products_status
                                          and g.customers_group_id = :customers_group_id
                                          and g.products_group_view = :products_group_view
                                          and p.products_id = :products_id
                                          order by p.products_date_added DESC
                                         ');

        $Qproducts->bindInt(':products_id', (int)$products_id );
        $Qproducts->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
        $Qproducts->bindValue(':products_group_view', '1' );
        $Qproducts->bindValue(':products_status',   '1' );
        $Qproducts->execute();

        $products = $Qproducts->fetch();
        $price_group_view = $products['price_group_view'];
      }


      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        if ($new_price = $this->setSpecialPriceGroup($products_id) ) {
          if ($price_group_view == '1') {
              $products_price = '<span class="normalPrice"><del>' . $this->setGroupPrice($products_id) . '</del></span><span class="specialPrice extendSpecialPrice" itemprop="price">' . $currencies->display_price($new_price, osc_get_tax_rate($this->getProductsTaxClassId($products_id) )) . '</span>';
            } else {
             $products_price = '<span itemprop="price">' . $this->setGroupPrice($products_id) . '</span>';
            }
        } else {
           $products_price = '<span itemprop="price">' . $this->setGroupPrice($products_id) . '</span>';
        }
      } else {
        $normal_price = 1; // Arret du mode Grand Public pour refus d'afficher le prix groupe B2B
      }

      if (($OSCOM_Customer->getCustomersGroupID() == 0) || ($normal_price == 1)) {
        if ($new_price = $this->setSpecialPriceGroup($products_id) ) {
          $products_price = '<span class="normalPrice"><del>' . $this->setGroupPrice($products_id) . '</del></span><span class="specialPrice extendSpecialPrice" itemprop="price">' . $currencies->display_price($new_price, osc_get_tax_rate($this->getProductsTaxClassId($products_id) )) . '</span>';
        } else {
          $products_price = '<span itemprop="price">' .$this->setGroupPrice($products_id) . '</span>';
        }

        $products_price .= '<meta itemprop="priceCurrency" content="' . osc_output_string($_SESSION['currency']) . '" />';
      }

      $normal_price = 0; // Arret du mode Grand Public pour refus d'afficher le prix groupe B2B

      return $products_price;
    }

/**
 * Display the price in function the group
 *
 * @param string $products_price, the price of the product
 * @access private
 * osc_b2b_calcul_price($products_price)
 */
      Public function getCalculPrice()  {
        return $this->setCalculPrice();
      }

  } // end class
