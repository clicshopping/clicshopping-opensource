<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('EMAIL_TEXT_SUBJECT', 'Traitement de votre commande sur '. STORE_NAME);
define('EMAIL_TEXT_ORDER_NUMBER', 'Bonjour,' . "\n\n" . 'Veuillez trouver ci joint les éléments concernant la commande passée chez  '. STORE_NAME .' '."\n\n".'Nous vous remercions de votre confiance et nous faisons notre possible pour traiter au mieux l\'envoi de vos produits commandés rapidement.' . "\n\n" . 'Numero de votre commande :');
define('EMAIL_TEXT_INVOICE_URL', "\n" . 'Vous pouvez suivre l\'évolution de votre commande via cette adresse :');
define('EMAIL_TEXT_DATE_ORDERED', "\n" . 'Date de votre commande :');
define('EMAIL_TEXT_PRODUCTS', 'Articles');
define('EMAIL_TEXT_SUBTOTAL', 'Sous-Total :');
define('EMAIL_TEXT_TAX', 'TVA :        ');
define('EMAIL_TEXT_SHIPPING', 'Expédition : ');
define('EMAIL_TEXT_TOTAL', 'Total :    ');
define('EMAIL_TEXT_DELIVERY_ADDRESS', 'Adresse de livraison');
define('EMAIL_TEXT_BILLING_ADDRESS', 'Adresse de facturation');
define('EMAIL_TEXT_PAYMENT_METHOD', 'Méthode de paiement');

define('EMAIL_SEPARATOR', '');
define('TEXT_EMAIL_VIA', 'par l\'intermédiaire de');
define('EMAIL_TEXT_FOOTER', 'Nous venons de prendre en compte votre commande. <br /><br />Celle-ci sera validée dès réception de votre réglement. Veuillez imprimer ce mail certifiant que vous avez bien passé commande sur '.STORE_NAME.' ou pour toute correspondance ultérieure.' . "\n\n" . 'Recevez, cher client, nos sincères salutations.'. "\n\n" . '
Très cordialement,'. "\n\n" . '<strong>L\'équipe '.STORE_NAME.'</strong>'. "\n\n" . '
<u><font size="2">Avis de confidentialité :</u>' . "\n\n" . 'Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans être le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re�u ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n\n" . '
<p>Conformément à la Loi dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</font>
</div>');

define('EMAIL_TEXT_SUBJECT_STOCK','Alerte Stock sur ' . STORE_NAME);

define('EMAIL_REORDER_LEVEL_TEXT_STOCK', 'Bonjour,' . "\n\n" . 'Vous recevez une alerte email sur le stock d\'alerte (paramétrage générique)  d\'un de vos produits. Ce stock vient d\'etre atteint. '. "\n" . 'Voici les caractéristiques des produits : '. "\n" . ' ');
define('EMAIL_TEXT_STOCK', 'Bonjour,' . "\n\n" . 'Vous recevez une alerte email concernant l\'épuisement du stock d\'un de vos produits. Votre produit n\'est plus visble sur la boutique. Vous pouvez néanmoins le réactiver sans modifier votre stock. Dans ce cas, le bouton ajouté sera remplacé par le bouton épuisé.  '. "\n\n" . 'Voici les caractéristiques de produit épuisé : '. "\n\n" . ' ');

define('EMAIL_TEXT_SUBJECT_ALERT_STOCK','Alerte Stock fiche produit sur ' . STORE_NAME);
define('EMAIL_REORDER_LEVEL_TEXT_ALERT_STOCK', 'Bonjour,' . "\n\n" . 'Vous recevez une alerte email concernant le stock d\'alerte (défini dans la fiche produit) d\'un de vos produits. Ce stock vient d\'etre atteint. '. "\n" . 'Voici les caractéristiques des produits : '. "\n" . ' ');
define('EMAIL_TEXT_PRODUCT_ALERT_STOCK','Stock restant du produit : ');


define('EMAIL_TEXT_ID_PRODUCT','Id produit : ');
define('EMAIL_TEXT_MODEL','Ref : ');
define('EMAIL_TEXT_PRODUCTS_NAME','Nom produit : ');
define('EMAIL_TEXT_PRODUCT_URL','Url du produit : ');
define('EMAIL_TEXT_PRODUCT_STOCK','Nouveau stock du produit : ');
define('EMAIL_TEXT_DATE_ALERT','Date Alerte');
?>
