<?php
/*
 * export_data.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Gestion des exportations');
define('TEXT_TITLE_EXPORT','Formulaire de configuration des exportations');
define('COMPARATEUR_SELECT', 'Veuillez sélectionner votre format : ');
define('COMPARATEUR_LNG', 'Veuillez sélectionner votre langue : ');
define('COMPARATEUR_CODE', 'Veuillez inérer un code de sécurité pour l\'accès au fichier : ');
define('COMPARATEUR_CACHE', 'Souhaitez vous créer un fichier sur le serveur ? ');
define('COMPARATEUR_OUI', 'Oui');
define('COMPARATEUR_NON', 'Non');
define('COMPARATEUR_SECU', 'Souhaitez vous placer le fichier dans un répertoire sécurisé ? ');
define('COMPARATEUR_FICHIER', 'Veuillez indiquer le nom du fichier qui sera généré avec son extension (monfichier.xml ou .csv ou .txt) : ');
define('COMPARATEUR_OBLIG', '<font color="#FF0000">Champ obligatoire si vous souhaitez créer un fichier sur le serveur</font> : ');

define('COMPARATEUR_CHAMP', 'Champ libre permettant d\'améliorer la sécurité, exemple <strong><i>"?id=1"</i></strong>. Aucune vérification n\'est effectuée ');
define('COMPARATEUR_URL', 'Url &agrave recopier dans la bar d\'adresse de votre navigateur pour générer le fichier ou voir le script ou à donner à votre intégrateur : ');
define('COMPARATEUR_SELECT_EXPORT','Sélectionner votre format d\'exportation : ');

// Aide
define('TITLE_AIDE_EXPORT_IMAGE', 'Aide');
define('TITLE_AIDE_EXPORT', 'Informations les exportations');
define('TEXT_AIDE_EXPORT', 'Ce système va vous permettre de réaliser différents types d\'exportation de votre base de données .....<br /><br />
<u><strong>Nos conseils</strong></u><br />
<blockquote>
<li>les <font color="#FF0000">*</font> sont nos conseils à suivre pour une bonne utilisation de cet outil. Ils s\'appliquent à la plupart des configurations.</li><li>Nous vous conseillons de stocker le fichier sur le serveur et de bien mémoriser son nom si vous avez une quantité importante de  produit.
<li>Si vous avez une gamme de plus de 100 produits, il faudra générer un fichier. Certaines options seront alors supprimées dés ce quota atteint</li>
<blockquote>
- La création du fichier implique une monté en charge du serveur importante.<br />- La pratique idéale est d\'avoir le même nom que l\'exportation sélectionnée. <br />  (ex :  Comparateur de prix le guide.com xml ===> nom du fichier : le_guidecom.xml ou <br />  Comparateur de prix kelkoo xml ===> nom du fichier : kelkoo.xml).<br />Cette synthaxe vous permettra facilement de générer un nouveau fichier en écrasant l\'ancien et de tenir à jour vos données.<br />- Cette procédure sera à réaliser à chaque fois que vous modifierez vos données pour que votre fichier soit toujours actualisé<br />- L\'enregistrement du fichier sur le serveur est importante car elle permettra au serveur distant de récupérér plus facilement vos informations sans demander de ressources au serveur.<br />
- Les options et les champs supplémentaires ne sont pas pris en compte actuellement.<br />
</blockquote>
<li>Si vous ne créez pas de fichier et que vous générer directement le fichier (apparition du flux xml dans votre navigateur). L\'enregistrement du fichier doit être en xml. (ex : export_data.php en export_data.xml)<br />
<br />
<li> Comment accéder à votre fichier depuis une adresse URL et le télécharger</li>
<blockquote>- Adresse sécurisée accessible via le navigateur : '. HTTP_SERVER . DIR_WS_ADMIN .'ext/export/monfichier.xml<br />
<strong>- Cette adresse URL pour accéder aux informations de votre bases de données et n\'est pas à divulguer à n\'importe qui !</strong>
</blockquote>
');

?>