<?php
  /**
   * d_whos_online_dash.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class d_whos_online_dash {
    var $code = 'd_whos_online_dash';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_whos_online_dash() {
      $this->title = MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_STATUS == 'True');
      }
    }

  function getOutput() {
    global $OSCOM_PDO;

    $xx_mins_ago = (time() - 900);

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_whos_online
                                    where time_last_click < :time_last_click
                                  ');
    $Qdelete->bindValue(':time_last_click',  $xx_mins_ago);
    $Qdelete->execute();

    $QwhosOnline = $OSCOM_PDO->prepare('select customer_id,
                                              full_name,
                                              ip_address,
                                              user_agent
                                        from  :table_whos_online
                                        limit 5
                                    ');
    $QwhosOnline->execute();

    $output = '<div class="clearfix"></div>';
    $output .=  osc_draw_separator('pixel_trans.gif', '1', '10');
    $output .= '<table class="table table-hover table-responsive" style="padding-top:0px; padding-left:0px;">';

    $output .= '<thead>';
    $output .= '<tr class="dataTableHeadingRow">';
    $output .= '<th class="dataTableHeadingContent">' . TABLE_HEADING_ONLINE . '</th>';
    $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . TABLE_HEADING_FULL_NAME . '</th>';
    $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . TABLE_HEADING_IP_ADDRESS . '</th>';
    $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . TABLE_HEADING_USER_AGENT. '</th>';
    $output .= '</tr>';
    $output .= '</thead>';
    $output .= '<tbody>';

    while ($whos_online = $QwhosOnline->fetch() ) {

      $time_online = (time() - $whos_online['time_entry']);

      $output .= '<tr class="dataTableRow">';
      $output .= '<td class="dataTableContent">' . gmdate('H:i:s', $time_online) . '</td> ';

      if($whos_online['customer_id'] == 0){
        $output .= '<td class="dataTableContent" style="text-align:left;>' . $whos_online['full_name'] . '</td>';
      } else {
        $output .= '<td class="dataTableContent"><a href="'. osc_href_link('customer.php', '?cID=' . $whos_online['customer_id']) . '&action=edit" title="View Customer">' . $whos_online['full_name'] . '</a></td>';
      }

      $output .= '<td class="dataTableContent" style="text-align:center;"><a href="http://ip-lookup.net/index.php?ip='. urlencode($whos_online['ip_address']). '" title="Lookup" target="_blank">'. $whos_online['ip_address'] .'</a></td>';
      $output .= '<td class="dataTableContent">' . $whos_online['user_agent'].'</td>';
      $output .= '</tr>';
    } // end while


    $output .= '</tbody>';
    $output .= '</table>';

    $output .= '<div class="clearfix"></div>';
    $output .= '<div>';
    $output .= '<a href="' . osc_href_link('whos_online.php') . '>' . sprintf(TEXT_NUMBER_OF_CUSTOMERS, $QwhosOnline->rowCount() ).'</a>';
    $output .=  osc_draw_separator('pixel_trans.gif', '1', '10');
    $output .= '</div>';
    $output .= '<br /><br />';

    return $output;
  }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_STATUS');
    }

    function install()  {
      global $OSCOM_PDO;
      if ($_SESSION['languages_id'] == 1) {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_SORT_ORDER',
            'configuration_value' => '80',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr� en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      } else {
        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Do you want enable this Module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Do you want enable this Module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Sort Order',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SORT_ORDER',
            'configuration_value' => '80',
            'configuration_description' => 'Sort order of display. Lowest is displayed first.',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );
      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_STATUS',
                   'MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_SORT_ORDER'
                  );
    }
  }
