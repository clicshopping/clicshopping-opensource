<?php
/**
 * admin_http_authentification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_ADMIN_HTTP_AUTHENTICATION_ERROR', 'L\'authentication HTTP n\'a pas été configurée dans l\'administration - veuillez configuer votre serveur pour éviter l\accès à l\'administration.');
?>
