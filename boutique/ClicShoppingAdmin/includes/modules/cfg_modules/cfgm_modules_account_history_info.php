<?php
  /**
   * cfm_modules_history_info.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_account_history_info {
    var $code = 'modules_account_history_info';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_ACCOUNT_HISTORY_INFO_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_account_history_info() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_account_history_info/';
      $this->title = MODULE_CFG_MODULE_ACCOUNT_HISTORY_INFO_MODULES_TITLE;
    }
  }
?>