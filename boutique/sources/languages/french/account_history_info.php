<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Historique');
define('NAVBAR_TITLE_3', 'Commande #%s');

define('HEADING_TITLE', 'Informations commande / facture');

define('HEADING_ORDER_NUMBER', 'N  #%s');
define('HEADING_ORDER_DATE', 'Date de prise de la commande :');
define('HEADING_ORDER_TOTAL', 'Montant total :');

define('HEADING_DELIVERY_ADDRESS', 'Adresse de livraison');
define('HEADING_SHIPPING_METHOD', 'Méthode d\'expédition');

define('HEADING_PRODUCTS', 'Produits');
define('HEADING_TAX', 'Taxe');
define('HEADING_TOTAL', 'Total');

define('HEADING_BILLING_INFORMATION', 'Information facturation');
define('HEADING_BILLING_ADDRESS', 'Adresse de facturation');
define('HEADING_PAYMENT_METHOD', 'Méthode de paiement');

define('HEADING_ORDER_HISTORY', 'Etat de la commande / facture :');
define('HEADING_COMMENT', 'Commentaires');
define('TEXT_NO_COMMENTS_AVAILABLE', 'Aucun commentaires disponibles.');

define('HEADING_FOLLOW_TRACKING','Suivi de votre commande');
define('HEADING_FOLLOW_TRACKING_TEXT','Pour toute demande de rétractation, veuillez passer par le lien à votre disposition ci-dessous');

define('TABLE_HEADING_DOWNLOAD_DATE', 'Date d\'expiration : ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' Téléchargements restant.');
define('HEADING_DOWNLOAD', 'Liens du téléchargement');
define('IMAGE_BUTTON_PRINT_INVOICE','Télécharger la facture');
define('IMAGE_BUTTON_PRINT_ORDER_ORDER','Télécharger la commande');
define('INVOICE_DOWNLOAD','Télécharger la facture');
define('HEADING_MY_SUPPORT','Support client');
define('TEXT_MY_SUPPORT','Contacter le support ou service commercial concernant cette commande');
define('TEXT_TRACKING_SERVICES', 'Suivre votre expédition');
define('IMAGE_BUTTON_PRINT_ORDER_ORDER','Imprimer la commande');

define('TEXT_DOWNLOAD_FILES', 'Télécharger le fichier');
define('TEXT_INVOICE_DOWNLOAD', 'Facturation et Téléchargement');

define('HEADING_ORDER_CONDITIONS_OF_SALES','Contrat des conditions générales de vente');
define('TEXT_CONDITION_GENERAL_OF_SALES','Accéder à vos conditions générales de vente concernant cette vente');
?>