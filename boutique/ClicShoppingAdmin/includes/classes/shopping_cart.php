<?php
/*
 * shopping_cart.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  class shoppingCart {
    var $contents, $total, $weight;

    function shoppingCart() {
      $this->reset();
    }

    function restore_contents() {
      global $customer_id, $OSCOM_PDO;

      if (!$customer_id) return 0;

// insert current cart contents in database
      if ($this->contents) {
        foreach( array_keys($this->contents) as $products_id ) {
          $qty = $this->contents[$products_id]['qty'];

          $Qproduct = $OSCOM_PDO->prepare('select products_id
                                           from :table_customers_basket
                                           where customers_id = :customers_id
                                           and products_id = :products_id
                                         ');
          $Qproduct->bindInt(':customers_id', (int)$customer_id);
          $Qproduct->bindInt(':products_id', (int)$products_id);

          $Qproduct->execute();


          if ($Qproduct->fetch() === false) {

            osc_db_query("insert into customers_basket (customers_id,
                                                        products_id,
                                                        customers_basket_quantity,
                                                        customers_basket_date_added)
                         values ('" . (int)$customer_id . "', 
                                 '" . osc_db_input($products_id) . "', 
                                 '" . osc_db_input($qty) . "', 
                                 '" . date('Ymd') . "')
                        ");


            if ($this->contents[$products_id]['attributes']) {

              foreach ( $this->contents[$products_id]['attributes'] as $option => $value ) {

                osc_db_query("insert into customers_basket_attributes (customers_id,
                                                                                    products_id, 
                                                                                    products_options_id, 
                                                                                    products_options_value_id) 
                            values ('" . (int)$customer_id . "', 
                                    '" . osc_db_input($products_id) . "', 
                                    '" . (int)$option . "', 
                                    '" . (int)$value . "')
                            ");
              }
            }
          } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_customers_basket
                                              set customers_basket_quantity = :customers_basket_quantity
                                              where customers_id = :customers_id
                                              and products_id = :products_id
                                            ');
            $Qupdate->bindValue(':customers_basket_quantity', $qty);
            $Qupdate->bindInt(':customers_id', (int)$customer_id );
            $Qupdate->bindInt(':products_id', (int)$products_id);
            $Qupdate->execute();

          }
        }
      }

// reset per-session cart contents, but not the database contents
      $this->reset(FALSE);

      $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                             customers_basket_quantity
                                       from :table_customers_basket
                                       where customers_id = :customers_id
                                      ');
      $Qproducts->bindInt(':customers_id', (int)$customer_id);
      $Qproducts->execute();

      while ($products = $Qproducts->fetch() ) {
        $this->contents[$products['products_id']] = array('qty' => $products['customers_basket_quantity']);
// attributes

        $Qattributes = $OSCOM_PDO->prepare('select products_options_id,
                                                 products_options_value_id
                                          from :table_customers_basket_attributes
                                          where customers_id = :customers_id
                                          and products_id = :products_id
                                      ');
        $Qproducts->bindInt(':customers_id', (int)$customer_id);
        $Qproducts->bindInt(':products_id', (int)$products['products_id']);
        $Qattributes->execute();

        while ($attributes = $Qattributes->fetch() ) {
          $this->contents[$products['products_id']]['attributes'][$attributes['products_options_id']] = $attributes['products_options_value_id'];
        }
      }

      $this->cleanup();
    }

    function reset($reset_database = FALSE) {
      global $customer_id, $OSCOM_PDO;

      $this->contents = array();
      $this->total = 0;

      if ($customer_id && $reset_database) {

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_customers_basket
                                        where customers_id = :customers_id
                                        ');
        $Qdelete->bindInt(':customers_id', (int)$customer_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_customers_basket_attributes
                                        where customers_id = :customers_id
                                        ');
        $Qdelete->bindInt(':customers_id', (int)$customer_id);
        $Qdelete->execute();

      }
    }

    function add_cart($products_id, $qty = '', $attributes = '') {
      global $new_products_id_in_cart, $customer_id;

      $products_id = osc_get_uprid($products_id, $attributes);

      if ($this->in_cart($products_id)) {
        $this->update_quantity($products_id, $qty, $attributes);
      } else {
        if ($qty == '') $qty = '1'; // if no quantity is supplied, then add '1' to the customers basket

        $this->contents[] = array($products_id);
        $this->contents[$products_id] = array('qty' => $qty);
// insert into database
        if ($customer_id) osc_db_query("insert into customers_basket (customers_id,
                                                                      products_id,
                                                                      customers_basket_quantity,
                                                                      customers_basket_date_added)
                                       values ('" . (int)$customer_id . "', 
                                              '" . osc_db_input($products_id) . "', 
                                              '" . osc_db_input($qty) . "', 
                                              '" . date('Ymd') . "')
                                      ");

        if (is_array($attributes)) {
          foreach( $attributes as $option => $value ) {
            $this->contents[$products_id]['attributes'][$option] = $value;
// insert into database
            if ($customer_id) osc_db_query("insert into customers_basket_attributes (customers_id,
                                                                                      products_id,
                                                                                      products_options_id,
                                                                                      products_options_value_id)
                                            values ('" . (int)$customer_id . "', 
                                                    '" . osc_db_input($products_id) . "',
                                                    '" . (int)$option . "', 
                                                    '" . (int)$value . "')
                                         ");
          }
        }
        $new_products_id_in_cart = $products_id;
        osc_session_register('new_products_id_in_cart');
      }
      $this->cleanup();
    }

    function update_quantity($products_id, $quantity = '', $attributes = '') {
      global $customer_id, $OSCOM_PDO;

      if ($quantity == '') return true; // nothing needs to be updated if theres no quantity, so we return true..

      $this->contents[$products_id] = array('qty' => $quantity);
// update database

      if ($customer_id) {

        $Qupdate = $OSCOM_PDO->prepare('update :table_customers_basket
                                        set customers_basket_quantity = :customers_basket_quantity
                                        where customers_id = :customers_id
                                        and products_id = :products_id
                                      ');
        $Qupdate->bindValue(':customers_basket_quantity', $quantity);
        $Qupdate->bindInt(':customers_id', (int)$customer_id);
        $Qupdate->bindInt(':products_id', (int)$products_id);

        $Qupdate->execute();

      }

      if (is_array($attributes)) {
        foreach( $attributes as $option => $value ) {
          $this->contents[$products_id]['attributes'][$option] = $value;
// update database
          if ($customer_id) {

            $Qupdate = $OSCOM_PDO->prepare('update :table_customers_basket_attributes
                                            set products_options_value_id = :products_options_value_id
                                            where customers_id = :customers_id
                                            and products_id = :products_id
                                            and products_options_id = :products_options_id
                                          ');
            $Qupdate->bindInt(':products_options_value_id', (int)$value);
            $Qupdate->bindInt(':customers_id', (int)$customer_id);
            $Qupdate->bindInt(':products_id', (int)$products_id);
            $Qupdate->bindInt(':products_options_id', (int)$option);

            $Qupdate->execute();
          }
        }
      }
    }

    function cleanup() {
      global $customer_id, $OSCOM_PDO;

      foreach( array_keys($this->contents) as $key ) {
        if ($this->contents[$key]['qty'] < 1) {
          unset($this->contents[$key]);
// remove from database
          if ($customer_id) {

            $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_customers_basket
                                            where customers_id = :customers_id
                                            and products_id = :products_id
                                            ');
            $Qdelete->bindInt(':customers_id', (int)$customer_id );
            $Qdelete->bindInt(':products_id', $key);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_customers_basket_attributes
                                            where customers_id = :customers_id
                                            and products_id = :products_id
                                            ');
            $Qdelete->bindInt(':customers_id', (int)$customer_id );
            $Qdelete->bindInt(':products_id', $key);
            $Qdelete->execute();

          }
        }
      }
    }

    function count_contents() {  // get total number of items in cart 
        $total_items = 0;
        if (is_array($this->contents)) {
          foreach ( array_keys($this->contents) as $products_id ) {
                $total_items += $this->get_quantity($products_id);
            }
        }
        return $total_items;
    }

    function get_quantity($products_id) {
      if ($this->contents[$products_id]) {
        return $this->contents[$products_id]['qty'];
      } else {
        return 0;
      }
    }

    function in_cart($products_id) {
      if ($this->contents[$products_id]) {
        return true;
      } else {
        return false;
      }
    }

    function remove($products_id) {
      global $customer_id, $OSCOM_PDO;

      unset($this->contents[$products_id]);
// remove from database
      if ($customer_id) {

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_customers_basket
                                        where customers_id = :customers_id
                                        and products_id = :products_id
                                        ');
        $Qdelete->bindInt(':customers_id', (int)$customer_id );
        $Qdelete->bindInt(':products_id', $products_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_customers_basket_attributes
                                        where customers_id = :customers_id
                                        and products_id = :products_id
                                        ');
        $Qdelete->bindInt(':customers_id', (int)$customer_id );
        $Qdelete->bindInt(':products_id', $products_id);
        $Qdelete->execute();
      }
    }

    function remove_all() {
      $this->reset();
    }

    function get_product_id_list() {
      $product_id_list = '';
      if (is_array($this->contents)) {
        foreach ( array_keys($this->contents) as $products_id ) {
          $product_id_list .= ', ' . $products_id;
        }
      }
      return substr($product_id_list, 2);
    }

    function calculate() {
      global $OSCOM_PDO;
      $this->total = 0;
      $this->weight = 0;
      if (!is_array($this->contents)) return 0;

      foreach ( array_keys($this->contents) as $products_id ) {
        $qty = $this->contents[$products_id]['qty'];

// products price
        $Qproduct = $OSCOM_PDO->prepare('select products_id,
                                                products_price,
                                                products_tax_class_id,
                                                products_weight
                                        from :table_products
                                        where products_id = :products_id
                                        ');
        $Qproduct->bindInt(':products_id', (int)osc_get_prid($products_id) );
        $Qproduct->execute();

        if ($product = $Qproduct->fetch() ) {

          $prid = $product['products_id'];
          $products_tax = osc_get_tax_rate($product['products_tax_class_id']);
          $products_price = $product['products_price'];
          $products_weight = $product['products_weight'];

          $Qspecials = $OSCOM_PDO->prepare('select specials_new_products_price
                                            from :table_specials
                                            where products_id = :products_id
                                            and status = 1
                                          ');
          $Qspecials->bindInt(':products_id', (int)$prid );
          $Qspecials->execute();

          if ($Qspecials->fetch() !== false) {
            $specials = $Qspecials->fetch();
            $products_price = $specials['specials_new_products_price'];
          }

          $this->total += osc_add_tax($products_price, $products_tax) * $qty;
          $this->weight += ($qty * $products_weight);
        }

// attributes price
        if (isset($this->contents[$products_id]['attributes'])) {
          foreach ( $this->contents[$products_id]['attributes'] as $option => $value ) {

            $QattributePrice = $OSCOM_PDO->prepare('select options_values_price,
                                                          price_prefix
                                                  from :table_products_attributes
                                                  where products_id = :products_id
                                                  and options_id = :options_id
                                                  and options_values_id = :options_values_id
                                          ');
            $QattributePrice->bindInt(':products_id', (int)$prid );
            $QattributePrice->bindInt(':options_id', (int)$option );
            $QattributePrice->bindInt(':options_values_id', (int)$value );

            $QattributePrice->execute();

            $attribute_price = $QattributePrice->fetch();

            if ($attribute_price['price_prefix'] == '+') {
              $this->total += $qty * osc_add_tax($attribute_price['options_values_price'], $products_tax);
            } else {
              $this->total -= $qty * osc_add_tax($attribute_price['options_values_price'], $products_tax);
            }
          }
        }
      }
    }

    function attributes_price($products_id) {
      global $OSCOM_PDO;
      $attributes_price = 0;

      if (isset($this->contents[$products_id]['attributes'])) {
        foreach ( $this->contents[$products_id]['attributes'] as $option => $value ) {

          $QattributePrice = $OSCOM_PDO->prepare('select options_values_price,
                                                          price_prefix
                                                  from :table_products_attributes
                                                  where products_id = :products_id
                                                  and options_id = :options_id
                                                  and options_values_id = :options_values_id
                                                  ');
          $QattributePrice->bindInt(':products_id', (int)$products_id );
          $QattributePrice->bindInt(':options_id', (int)$option );
          $QattributePrice->bindInt(':options_values_id', (int)$value );

          $QattributePrice->execute();

          $attribute_price = $QattributePrice->fetch();

          if ($attribute_price['price_prefix'] == '+') {
            $attributes_price += $attribute_price['options_values_price'];
          } else {
            $attributes_price -= $attribute_price['options_values_price'];
          }
        }
      }

      return $attributes_price;
    }

    function get_products() {
      global $OSCOM_PDO;
      
      if (!is_array($this->contents)) return 0;
      $products_array = array();
      foreach ( $this->contents[$products_id]['attributes'] as $option => $value ) {

        $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                                pd.products_name,
                                                p.products_model,
                                                p.products_price,
                                                p.products_weight,
                                                p.products_tax_class_id
                                                from :table_products p,
                                                     :table_products_description pd
                                                where p.products_id = :products_id
                                                and pd.products_id = p.products_id
                                                and pd.language_id = :language_id
                                        ');
        $Qproducts->bindInt(':products_id', (int)osc_get_prid($products_id) );
        $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );

        $Qproducts->execute();

        if ($products = $Qproducts->fetch() ) {
          $prid = $products['products_id'];
          $products_price = $products['products_price'];

          $Qspecials = $OSCOM_PDO->prepare('select specials_new_products_price
                                            from :table_specials
                                            where products_id = :products_id
                                            and status = 1
                                           ');
          $Qspecials->bindInt(':products_id', (int)$prid );

          $Qspecials->execute();

          if ($Qspecials->fetch() !== false) {
            $specials = $Qspecials->fetch();
            $products_price = $specials['specials_new_products_price'];
          }

          $products_array[] = array('id' => $products_id,
                                    'name' => $products['products_name'],
                                    'model' => $products['products_model'],
                                    'price' => $products_price,
                                    'quantity' => $this->contents[$products_id]['qty'],
                                    'weight' => $products['products_weight'],
                                    'final_price' => ($products_price + $this->attributes_price($products_id)),
                                    'tax_class_id' => $products['products_tax_class_id'],
                                    'attributes' => (isset($this->contents[$products_id]['attributes']) ? $this->contents[$products_id]['attributes'] : '')
                                    );
        }
      }
      return $products_array;
    }

    function show_total() {
      $this->calculate();

      return $this->total;
    }

    function show_weight() {
      $this->calculate();

      return $this->weight;
    }

    function unserialize($broken) {
      for(reset($broken);$kv=each($broken);) {
        $key=$kv['key'];
        if (gettype($this->$key)!="user function")
        $this->$key=$kv['value'];
      }
    }
  }

