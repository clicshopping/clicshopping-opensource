
CREATE TABLE IF NOT EXISTS `action_recorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `success` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_action_recorder_module` (`module`),
  KEY `idx_action_recorder_user_id` (`user_id`),
  KEY `idx_action_recorder_identifier` (`identifier`),
  KEY `idx_action_recorder_date_added` (`date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `address_book` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_gender` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `entry_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_siret` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `entry_ape` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `entry_tva_intracom` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `entry_cf` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `entry_piva` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `entry_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  `entry_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `address_format` (
  `address_format_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_format` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address_summary` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_format_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(1, '$firstname $lastname$cr$streets$cr$city, $postcode$cr$statecomma$country', '$city / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(2, '$firstname $lastname$cr$streets$cr$city, $state    $postcode$cr$country', '$city, $state / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(3, '$firstname $lastname$cr$streets$cr$city$cr$postcode - $statecomma$country', '$state / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(4, '$firstname $lastname$cr$streets$cr$city ($postcode)$cr$country', '$postcode / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(5, '$firstname $lastname$cr$streets$cr$postcode $city$cr$country', '$city / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(6, '$firstname $lastname$cr$streets$cr$postcode $city$cr$statecomma$country', '$city / $country');

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `atos_informations` (
  `orders_id` int(11) NOT NULL,
  `entete` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `merchant_id` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `payment_means` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `origin_amount` float NOT NULL,
  `amount` float NOT NULL,
  `currency_code` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_time` time NOT NULL,
  `card_validity` int(11) NOT NULL,
  `card_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `response_code` int(11) NOT NULL,
  `cvv_response_code` int(11) NOT NULL,
  `complementary_code` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `certificate` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `authorization_id` int(11) NOT NULL,
  `capture_date` int(11) NOT NULL,
  `transaction_status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `return_context` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `autoresponse_status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `atos_order_id` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `atos_customer_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `customer_ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `account_serial` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_condition` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `cavv_ucaf` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `complementary_info` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `bank_response_code` int(11) NOT NULL,
  PRIMARY KEY (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `banners_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_target` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `banners_html_text` text COLLATE utf8_unicode_ci,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `banners_title_admin` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`banners_id`),
  KEY `idx_banners_group` (`banners_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL DEFAULT '0',
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`banners_history_id`),
  KEY `idx_banners_history_banners_id` (`banners_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `blog_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_categories_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_categories_id`),
  KEY `idx_blog_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_categories_description` (
  `blog_categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `blog_categories_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blog_categories_description` text COLLATE utf8_unicode_ci,
  `blog_categories_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_categories_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_categories_head_keywords_tag` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`blog_categories_id`,`language_id`),
  KEY `idx_blog_categories_name` (`blog_categories_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `blog_content` (
  `blog_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_content_date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `blog_content_last_modified` datetime DEFAULT NULL,
  `blog_content_date_available` datetime DEFAULT NULL,
  `blog_content_status` tinyint(1) NOT NULL DEFAULT '0',
  `blog_content_archive` tinyint(1) NOT NULL DEFAULT '0',
  `admin_user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blog_content_sort_order` int(11) NOT NULL DEFAULT '0',
  `blog_content_author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_content_id`),
  KEY `idx_blog_content_date_added` (`blog_content_date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_content_description` (
  `blog_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `blog_content_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blog_content_description` text COLLATE utf8_unicode_ci,
  `blog_content_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_viewed` int(5) DEFAULT '0',
  `blog_content_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_head_keywords_tag` text COLLATE utf8_unicode_ci,
  `blog_content_head_tag_product` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_head_tag_blog` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_description_summary` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`blog_content_id`,`language_id`),
  KEY `blog_content_name` (`blog_content_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_content_to_categories` (
  `blog_content_id` int(11) NOT NULL DEFAULT '0',
  `blog_categories_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_content_id`,`blog_categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `cache` (
  `cache_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cache_language_id` tinyint(1) NOT NULL DEFAULT '0',
  `cache_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cache_data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cache_global` tinyint(1) NOT NULL DEFAULT '1',
  `cache_gzip` tinyint(1) NOT NULL DEFAULT '1',
  `cache_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'RETURN',
  `cache_date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `cache_expires` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`cache_id`,`cache_language_id`),
  KEY `cache_id` (`cache_id`),
  KEY `cache_language_id` (`cache_language_id`),
  KEY `cache_global` (`cache_global`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `virtual_categories` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categories_id`),
  KEY `idx_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `categories_description` (
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `categories_description` text COLLATE utf8_unicode_ci,
  `categories_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categories_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categories_head_keywords_tag` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`categories_id`,`language_id`),
  KEY `idx_categories_name` (`categories_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `cmcic_reference` (
  `ref_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cmcic_response` (
  `resp_id` int(11) NOT NULL AUTO_INCREMENT,
  `MAC` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TPE` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `montant` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `texte_libre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_retour` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `retourPLUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`resp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `configuration` (
  `configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration_value` text COLLATE utf8_unicode_ci NOT NULL,
  `configuration_description` text COLLATE utf8_unicode_ci NOT NULL,
  `configuration_group_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(5) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `use_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `set_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=695 ;

INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(1, 'What is your the store Name', 'STORE_NAME', 'ClicShopping', 'Please specify the name of your store.<br /><br /><font color="#FF0000"><b>Note :</b> This name will appear in the content of e-mails</font><br>', 1, 1, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(2, 'Who is your the manager of the store ?', 'STORE_OWNER', '', 'Please specify the name of the manager of the shop.<br />', 1, 2, '2008-09-15 09:39:27', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(3, 'What is your e-mail address of the store ?', 'STORE_OWNER_EMAIL_ADDRESS', '', 'Please enter the email that will be used by the store when sending emails address.', 1, 3, '2007-06-02 15:40:42', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(4, '''From'' field of an email sent', 'EMAIL_FROM', '', 'Field used in the headers of email.<br><br><font color="#FF0000"><b>Note :</b>The e-mail address is built as follows mon_service<email> (between  <>, enter your email) </font>', 1, 4, '2007-05-07 12:31:36', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(5, 'Country', 'STORE_COUNTRY', '73', 'The country of residence of the store.<br /><br /><font color="#FF0000"><b>Note :</b> Do not forget also configure zones in the following option<br /></font>', 1, 6, '2008-12-05 19:10:24', '2006-04-09 16:13:47', 'osc_get_country_name', 'osc_cfg_pull_down_country_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(6, 'What is your zone ?', 'STORE_ZONE', '265', 'Zone on the location of the store.<br /><br /><font color="#FF0000"><b>Note :</b> To complete the menu, you can go to the menu "Location / Taxes"<br /></font>', 1, 7, '2008-12-05 19:10:33', '2006-04-09 16:13:47', 'osc_cfg_get_zone_name', 'osc_cfg_pull_down_zone_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(9, 'Automatic adjustment of currency', 'USE_DEFAULT_LANGUAGE_CURRENCY', 'false', 'Automatic change of monetary currency according to the language selected customer.<br /><br />', 1, 10, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(10, 'Send a copy of orders on other e-mails addresses', 'SEND_EXTRA_ORDER_EMAILS_TO', '', 'Send a copy of the order to the address specified.<br /><br /><font color="#FF0000"><b>Note :</b> You can add more with comma separation<br></font>', 1, 11, '2007-06-02 15:44:29', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(12, 'Do you want to display cart after adding a product', 'DISPLAY_CART', 'true', 'In value <b><i>true</i></b> the store display the contents of the basket after adding a product.<br><br>', 1, 14, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(13, 'Allow visitors the recommendation of a product', 'ALLOW_GUEST_TO_TELL_A_FRIEND', 'false', 'In value  <b><i>true</i></b> visitors will find a link on the product page to enable it to recommend this product to a friend by sending an email.<br /><br />', 1, 15, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(14, 'Default search function', 'ADVANCED_SEARCH_DEFAULT_OPERATOR', 'and', 'Please select the search operator that will be used by default.<br /><br /><i>(Value and = Word1 <b>and</b> the word2 - Value or = Word1 <b>or</b> the word2)</i><br />', 1, 17, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''and'', ''or''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(15, 'Specify Name, address, country and such of the store', 'STORE_NAME_ADDRESS', 'Adresse\r\nZip Code - City\r\ntelephone', 'Name, address, country, phone of the store to be used in printing documents and online display.<br />', 1, 18, '2008-09-15 09:39:38', '2006-04-09 16:13:47', NULL, 'osc_cfg_textarea(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(17, 'Specify Decimal tax', 'TAX_DECIMAL_PLACES', '2', 'Decimal slot the value of the amount of tax.<br />', 1, 20, '2006-05-31 01:17:46', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(18, 'Do you want to display Prices with taxes', 'DISPLAY_PRICE_WITH_TAX', 'true', 'in value <b><i>true</i></b> Price include all taxes is displayed in false, the price is excluding taxes.<br />', 1, 21, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(19, 'Minimum number of characters for first name', 'ENTRY_FIRST_NAME_MIN_LENGTH', '2', 'Minimum number of characters required for the firstname', 16, 1, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(20, 'Minimum number of characters for name', 'ENTRY_LAST_NAME_MIN_LENGTH', '2', 'Minimum number of characters required for the family name.', 16, 2, '2006-10-23 22:49:39', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(21, 'Minimum number of characters for date of birth', 'ENTRY_DOB_MIN_LENGTH', '10', 'Minimum number of characters required for the date of birth.', 16, 3, '2006-10-23 01:10:08', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(23, 'Minimum number of characters for address', 'ENTRY_STREET_ADDRESS_MIN_LENGTH', '4', 'Minimum number of characters required for the address.', 16, 5, '2006-10-23 01:10:20', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(24, 'Minimum number of characters for company', 'ENTRY_COMPANY_MIN_LENGTH', '0', 'Minimum number of characters required for the company name.', 16, 6, '2006-10-23 01:15:51', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(25, 'Minimum number of characters for zip code', 'ENTRY_POSTCODE_MIN_LENGTH', '4', 'Minimum number of characters required for zip code.', 16, 7, '2006-10-23 01:10:48', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(26, 'Minimum number of characters for city', 'ENTRY_CITY_MIN_LENGTH', '3', 'Minimum number of characters required for the city.', 16, 8, '2006-10-23 01:10:58', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(27, 'Minimum number of characters for department or state', 'ENTRY_STATE_MIN_LENGTH', '3', 'Minimum number of characters required for the department or state.', 16, 9, '2006-10-23 01:11:12', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(28, 'Minimum number of characters for telephone', 'ENTRY_TELEPHONE_MIN_LENGTH', '3', 'Minimum number of characters required for the phone.', 16, 10, '2006-10-23 22:49:04', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(29, 'Minimum number of characters for password', 'ENTRY_PASSWORD_MIN_LENGTH', '5', 'Minimum number of characters required for the user password.', 16, 11, '2006-10-23 00:37:27', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(30, 'Minimum number of characters for owner of the credit card', 'CC_OWNER_MIN_LENGTH', '3', 'Minimum number of characters required for the name of the owner of the credit card.', 2, 12, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(31, 'Minimum number of characters for number of credit cards', 'CC_NUMBER_MIN_LENGTH', '10', 'Minimum number of characters required for the number of the credit card.', 2, 13, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(32, 'Please specify the minimum number of words that the client must insert in the comments', 'REVIEW_TEXT_MIN_LENGTH', '50', 'Minimum number of characters required for the product reviewed.<br><i> - 10 comments for a minimum of 10 characters</i><br>', 32, 1, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(35, 'Maximum number of characters for number of entries in the address book', 'MAX_ADDRESS_BOOK_ENTRIES', '5', 'Maximum number of entries in the address book for a client.', 3, 1, '2006-10-14 15:45:07', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(36, 'Search Result', 'MAX_DISPLAY_SEARCH_RESULTS', '20', 'Quantity of items to display per page.', 3, 2, '2007-04-24 19:02:41', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(37, 'Links page', 'MAX_DISPLAY_PAGE_LINKS', '5', 'Number of links used.', 3, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(53, 'Page View Order History', 'MAX_DISPLAY_ORDER_HISTORY', '10', 'Maximum number of historic command to display the page.', 3, 18, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(54, 'Thumbnail or small image: please specify the width', 'SMALL_IMAGE_WIDTH', '130 ', 'Please specify the number of pixels for the width of the thumbnails or small images', 4, 1, '2007-05-19 16:34:40', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(55, 'Thumbnail or small image: Please specify height', 'SMALL_IMAGE_HEIGHT', '', 'Please specify the number of pixels for the height of the thumbnails or small images', 4, 2, '2007-05-19 16:34:35', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(56, 'Image header, Width', 'HEADING_IMAGE_WIDTH', '', 'The number of pixels for the width of the image header.', 4, 3, '2007-05-19 16:34:44', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(57, 'Image header, Height', 'HEADING_IMAGE_HEIGHT', '', 'Number of pixels for the height of the header image.', 4, 4, '2007-05-19 16:34:49', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(58, 'Image subcategory, Width', 'SUBCATEGORY_IMAGE_WIDTH', '', 'Number of pixels for the width of subcategory images.', 4, 5, '2007-05-19 16:34:53', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(59, 'Image subcategory, Hauteur', 'SUBCATEGORY_IMAGE_HEIGHT', '', 'Number of pixels for the height images subcategory.', 4, 6, '2007-05-19 16:34:58', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(60, 'Auto calculation of the size of the images', 'CONFIG_CALCULATE_IMAGE_SIZE', 'false', 'Automatically calculate the image size.<br />', 4, 7, '2007-05-18 05:21:07', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(61, 'broken image', 'IMAGE_REQUIRED', 'false', 'Enable display of broken images links (for development).<br />', 4, 8, '2007-05-19 03:31:22', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(62, 'civility <i>(Man & Woman)</i>', 'ACCOUNT_GENDER', 'true', 'Show two checkboxes on civility in the registration form customers.<br />', 5, 1, '2006-10-23 01:15:03', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(63, 'Date of birth', 'ACCOUNT_DOB', 'true', 'Display the date of birth in the registration form for customers.<br />', 5, 2, '2006-10-23 01:15:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(64, 'Company', 'ACCOUNT_COMPANY', 'true', 'View company field in the "My Account" and the registration form for customers.<br />', 5, 3, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(65, 'Additional address', 'ACCOUNT_SUBURB', 'true', 'Show address further in the registration form for customers.<br>', 5, 4, '2006-10-23 01:16:30', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(66, 'State and / or Department', 'ACCOUNT_STATE', 'true', 'Show the department field (state) in the registration form<br>', 5, 5, '2006-10-23 01:17:10', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(84, 'Default currency', 'DEFAULT_CURRENCY', 'EUR', 'Default currency.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(85, 'Default language', 'DEFAULT_LANGUAGE', 'fr', 'Default language.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(86, 'Default status for new order', 'DEFAULT_ORDERS_STATUS_ID', '1', 'When a new order is created, this order status will be assigned to him.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(98, 'Country code store', 'SHIPPING_ORIGIN_COUNTRY', '38', 'Insert the code "ISO 3166" Country to calculate shipping costs.', 7, 1, '2008-09-13 13:08:41', '2006-04-09 16:13:48', 'osc_get_country_name', 'osc_cfg_pull_down_country_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(99, 'Postcode store', 'SHIPPING_ORIGIN_ZIP', 'H2S2N1', 'Enter the zip code of the store to calculate shipping.', 7, 2, '2008-09-13 13:08:47', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(100, 'Maximum weight for the shipping (in Kg)', 'SHIPPING_MAX_WEIGHT', '50', 'Carriers have a maximum weight limit per package. This limitation is common to all.', 7, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(101, 'Tare packaging', 'SHIPPING_BOX_WEIGHT', '0', 'What is the average weight of packaging and packaging parcels small to medium size ?', 7, 4, '2008-09-13 14:57:27', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(102, 'Large packages - Percentage surcharge', 'SHIPPING_BOX_PADDING', '10', '10% surcharge, enter 10.', 7, 5, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(103, 'Do you want to display the product image? ?', 'PRODUCT_LIST_IMAGE', '1', 'In the list of items, do you want display the product image? ?<br />Please specify a sort order<br /><br /><i>- 0 for no display<br />- 1 for example the first position</i><br />', 0, 1, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(104, 'Do you want display the sort order for the brand ?', 'PRODUCT_LIST_MANUFACTURER', '0', 'In the products listing, do you want display the sort order for the brand ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br />- 1 to display the sort order</i><br>', 8, 2, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(105, 'Do you want display the sort order for product model ?', 'PRODUCT_LIST_MODEL', '0', 'In the products listing, do you want display the sort order for product model ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br />- 1 to display the sort order</i><br />', 8, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(106, 'Do you want display the sort order for the product name ?', 'PRODUCT_LIST_NAME', '2', 'In the products listing, do you want display the sort order for the product name ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br>- 1 to display the sort order</i><br />', 8, 4, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(107, 'Do you want display the sort order for the product price ?', 'PRODUCT_LIST_PRICE', '3', 'In the products listing, do you want display the sort order for the product price ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br />- 1 to display the sort order</i><br />', 8, 5, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(108, 'Do you want display the sort order for product stock ?', 'PRODUCT_LIST_QUANTITY', '0', 'In the products listing, do you want display the sort order for product stock ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br />- 1  to display the sort order</i><br />', 8, 6, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(109, 'Do you want display the sort order for the product weight ?', 'PRODUCT_LIST_WEIGHT', '0', 'In the products listing, do you want display the sort order for the product weight ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br />- 1  to display the sort order</i><br />', 8, 7, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(111, 'Do you want to display the  categories/brand filter', 'PRODUCT_LIST_FILTER', '1', 'In the list of items, do you want display the Categories/brands ?<br /><br /><i>- 0 pour No<br />- 1 pour yes</i><br />', 8, 9, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(112, 'Location of Prev/Next Navigation Bar (1-top, 2-bottom, 3-both)', 'PREV_NEXT_BAR_LOCATION', '2', 'Sets the location of the Prev/Next Navigation Bar (1-top, 2-bottom, 3-both)?<br /><br /><i>- 1 en haut<br>- 2 pour le bas<br>- 3 pour le haut et le bas</i><br>', 3, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(113, 'Stock Check', 'STOCK_CHECK', 'true', 'Check if the stock level is sufficient.<br />', 9, 1, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(114, 'Count of stock', 'STOCK_LIMITED', 'true', 'Count of stock items ordered.<br />', 9, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(115, 'Authorization purchase out of stock', 'STOCK_ALLOW_CHECKOUT', 'false', 'Allows the customer to order even if an item is not in stock.<br />', 9, 3, '2008-09-14 18:46:31', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(116, 'Showing out of stock the products', 'STOCK_MARK_PRODUCT_OUT_OF_STOCK', '***', 'Displays the following note if the product is no longer in stock.', 9, 4, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(117, 'Stock level warning', 'STOCK_REORDER_LEVEL', '5', 'Level alert stock replenishment.', 9, 5, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(118, 'Storage runtime', 'STORE_PAGE_PARSE_TIME', 'false', 'Stores the execution time of a page.<br />', 10, 1, '2007-05-20 01:00:47', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(119, 'File location for the execution stores', 'STORE_PAGE_PARSE_TIME_LOG', '/home/www/site/boutique/Clicpanel/cache/admin.log', 'Path and file name of the runtime.', 10, 2, '2008-09-15 10:07:36', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(120, 'Date format executions', 'STORE_PARSE_DATE_TIME_FORMAT', '%d/%m/%Y %H:%M:%S', 'Date format executions.', 10, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(121, 'Display runtime', 'DISPLAY_PAGE_PARSE_TIME', 'false', 'displays the execution time of a page (storage runtime must be enabled and the selected location in the file for the execution storage).<br />', 10, 4, '2007-06-03 16:58:14', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(122, 'Queries database', 'STORE_DB_TRANSACTIONS', 'false', 'Stores queries the database with the runtime.<br />', 10, 5, '2007-06-03 16:58:22', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(123, 'Use the cache', 'USE_CACHE', 'false', 'Use caching features.<br />', 11, 1, '2006-07-18 00:13:26', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(124, 'Cache directory', 'DIR_FS_CACHE', '/tmp/', 'Directory where the cache files are stored.', 11, 2, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(125, 'Method of transmission of email', 'EMAIL_TRANSPORT', 'sendmail', 'Specifies whether the server uses a local connection to sendmail or SMTP connection via TCP / IP. For Windows Servers and MacOS, you should select SMTP.', 12, 1, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''sendmail'', ''gmail'', ''smtp''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(126, 'Newline header emails', 'EMAIL_LINEFEED', 'LF', 'Set the characters used to separate headers emails.', 12, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''LF'', ''CRLF''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(127, 'Use MIME HTML for sending emails', 'EMAIL_USE_HTML', 'false', 'Send emails in html or plain text.<br />', 12, 3, '2008-09-15 22:57:14', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(128, 'Check the email address by the DNS', 'ENTRY_EMAIL_ADDRESS_CHECK', 'false', 'Check the email address through a DNS server.<br />', 12, 4, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(129, 'Activation emails', 'SEND_EMAILS', 'true', 'Allow sending emails.<br />', 12, 5, '2008-09-16 10:52:38', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(130, 'Allow downloading', 'DOWNLOAD_ENABLED', 'false', 'Validate the download function of the products.<br />', 13, 1, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(131, 'Download by redirect', 'DOWNLOAD_BY_REDIRECT', 'false', 'Use redirection for download. Turn off non-UNIX systems.<br />', 13, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(132, 'Timeouts downloads', 'DOWNLOAD_MAX_DAYS', '7', 'Number of days before expiration of download link.<br />(0 for no limit)', 13, 3, NULL, '2006-04-09 16:13:48', NULL, '');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(133, 'Maximum number of downloads', 'DOWNLOAD_MAX_COUNT', '5', 'Maximum number of downloads allowed.<br />(0 for none)', 13, 4, NULL, '2006-04-09 16:13:48', NULL, '');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(134, 'Enable GZip compression', 'GZIP_COMPRESSION', 'true', 'Enable HTTP Compression GZip.<br />', 14, 1, '2006-09-23 01:42:33', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(135, 'Level Compression', 'GZIP_LEVEL', '5', 'Determine the level of compression between 0-9.<br />(0 = minimum, 9 = maximum)<br>Recommended : 5.', 14, 2, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(136, 'Sessions directory', 'SESSION_WRITE_DIRECTORY', '/tmp', 'The path where the sessions will be stored.', 15, 1, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(137, 'Forced use of cookies', 'SESSION_FORCE_COOKIE_USE', 'False', 'Force the use of sessions when cookies are enabled.<br />', 15, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(138, 'Check the session ID', 'SESSION_CHECK_SSL_SESSION_ID', 'False', 'Confirm SSL_SESSION_ID on each page request secure HTTPS.<br />', 15, 3, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(139, 'Check the user', 'SESSION_CHECK_USER_AGENT', 'False', 'Confirm the client browser on each page request.<br />', 15, 4, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(140, 'Check the IP address', 'SESSION_CHECK_IP_ADDRESS', 'False', 'Confirm the IP address of the client on each page request.<br />', 15, 5, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(141, 'Prevent spider sessions', 'SESSION_BLOCK_SPIDERS', 'True', 'Prevent known spiders from starting a session.<br />', 15, 6, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(142, 'Recreate a session', 'SESSION_RECREATE', 'True', 'Recreate the session to generate a new session ID when the customer enters or creates an account (PHP> = 4.1 required).<br>', 15, 7, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(143, 'Please select the theme of the website', 'SITE_THEMA', 'bootstrap', 'Please select the number of graphic theme.<br><br><font color="#FF0000"><b>Note :</b> To complete your list of graphic themes, do not hesitate to contact our online support<br /></font>', 43, 1, '2013-12-16 18:12:37', '2006-04-09 18:20:19', NULL, 'osc_cfg_pull_down_all_template_directorylist(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(144, 'Hidde the occurrence price for visitors', 'PRICES_LOGGED_IN', 'false', 'Hidde the occurrence price unregistered visitors.<br />', 17, 3, '2006-04-26 19:36:44', '2001-11-17 11:22:55', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(145, 'Method on price (Discount or Surcharge)', 'B2B', 'false', 'Option <i><b>false</b></i> allows a discount on the price, <i><b>true</b></i> allows a price increase.', 17, 2, '2006-04-11 18:32:13', '2003-10-06 17:24:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(146, 'Approval of professional members', 'MEMBER', 'true', 'L\\''option sur <i><b>true</b></i> allows a control to approve the creation of new professionals accounts <br /><br /><font color="FF0000"><b>Note :</b> The approval takes place from the Clients menu -> Pending clients</font>', 17, 4, '2006-10-20 00:49:26', '2004-02-26 19:30:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(147, 'Get an e-mail to the registration of a new customer', 'EMAIL_CREAT_ACCOUNT_PRO', 'true', 'Get an email when registering a new customer Business.<br />', 17, 5, '2007-05-05 13:23:32', '2006-04-29 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(148, 'Display the civility <i>(man & woman)</i>', 'ACCOUNT_GENDER_PRO', 'true', 'Display two checkboxes on civility in the registration form.<br />', 18, 1, '2006-10-29 19:49:55', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(149, 'Display the Date of birth', 'ACCOUNT_DOB_PRO', 'true', 'Display the date of birth in the registration form.<br />', 18, 2, '2006-10-29 16:07:10', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(150, 'Display the Company', 'ACCOUNT_COMPANY_PRO', 'true', 'Display company field in the "My Account" and the registration form.<br />', 18, 3, '2006-10-29 19:49:34', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(151, 'Display an additional address', 'ACCOUNT_SUBURB_PRO', 'true', 'Display address further in the registration form.<br />', 18, 4, '2006-10-29 19:50:30', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(152, 'Display the state and / or Department', 'ACCOUNT_STATE_PRO', 'true', 'Display the department field (state) in the registration form<br />', 18, 5, '2006-10-29 19:50:57', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(153, 'Display the registration number of the company', 'ACCOUNT_SIRET_PRO', 'true', 'Display the registration number of the company in the registration form des clients <i>(eg for RCS France or Quebec NEQ)</i>.<br />', 18, 6, '2006-10-29 16:04:49', '2006-04-26 14:44:07', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(154, 'Display the nomenclature number of society', 'ACCOUNT_APE_PRO', 'true', 'Display the number of the nomenclature of the company in the registration form <i>(eg : 721Z for APE code in France)</i>.<br />', 18, 7, '2006-10-29 16:05:35', '2006-04-27 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(155, 'Display the VAT number (EU only)', 'ACCOUNT_TVA_INTRACOM_PRO', 'true', 'Display the VAT number in the registration form des clients.<br><br><font color="#FF0000"><b> Note :</b> Valid only for Europe</font>', 18, 8, '2006-10-29 16:05:56', '2006-04-26 14:43:56', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(156, 'Default country to display the dropdown menu', 'ACCOUNT_COUNTRY_PRO', '73', 'Please select the country default to display in the drop-down menu on the registration form.<br />', 18, 9, '2007-03-18 21:43:58', '2006-04-29 00:00:00', 'osc_get_country_name', 'osc_cfg_pull_down_country_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(157, 'Customer group to assign default to new business customers', 'ACCOUNT_GROUP_DEFAULT_PRO', '1', 'Please select customer group assigned by default to new customers.', 18, 10, '2008-08-30 11:55:02', '2006-10-16 00:00:00', 'osc_get_customers_group_name', 'osc_cfg_pull_down_customers_group_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(158, 'Allow editing on the information society', 'ACCOUNT_MODIFY_PRO', 'true', 'Allow default customers to change <b> <i> Name, Siret number, APE code and VAT number Intracom</i></b> of this company.<br />', 18, 11, '2006-10-15 22:50:18', '2006-04-29 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(159, 'Allow editing of primary billing address', 'ACCOUNT_MODIFY_ADRESS_DEFAULT_PRO', 'true', 'Allow default customers to change the address of its primary address book.<br />', 18, 12, '2006-10-15 22:43:28', '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(160, 'Allow adding in the address book', 'ACCOUNT_ADRESS_BOOK_PRO', 'true', 'Allow default customers add addresses in his book.<br />', 18, 13, '2006-10-15 22:50:21', '2006-05-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(161, 'Minimum number of characters for the first name', 'ENTRY_FIRST_NAME_PRO_MIN_LENGTH', '2', 'Minimum number of characters required for the first.', 19, 1, '2006-10-29 22:51:30', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(162, 'Minimum number of characters for the name', 'ENTRY_LAST_NAME_PRO_MIN_LENGTH', '2', 'Minimum number of characters required for the name.', 19, 2, '2006-10-29 22:30:18', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(163, 'Minimum number of characters for the date of birth', 'ENTRY_DOB_PRO_MIN_LENGTH', '10', 'Minimum number of characters for the date of birth.', 19, 3, NULL, '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(689, 'Do you want display the sort order by the product date ?', 'PRODUCT_LIST_DATE', '0', 'In the products listing, do you want display the sort order by product date ?<br />Please specify the sort order<br /><br /><i>- 0 to hide the sort order<br />- 1  to display the sort order</i><br />', 8, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(165, 'Minimum number of characters for the address', 'ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH', '3', 'Minimum number of characters for the address.', 19, 5, '2006-10-29 22:30:35', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(166, 'Minimum number of characters for the company', 'ENTRY_COMPANY_PRO_MIN_LENGTH', '4', 'Minimum number of characters for the company.', 19, 6, '2006-10-29 16:08:26', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(167, 'Minimum number of characters for the registration number of the company', 'ENTRY_SIRET_MIN_LENGTH', '14', 'Minimum number of characters required for the registration number of the company.<br><br><i>(eg for RCS France or Quebec NEQ)</i>', 19, 7, '2006-10-29 16:09:04', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(168, 'Minimum number of characters for the nomenclature number of the company', 'ENTRY_CODE_APE_MIN_LENGTH', '4', 'Minimum number of characters required for the nomenclature of the company.<br><br><i>(eg 721Z for APE code in France)</i>', 19, 8, '2006-10-29 16:09:50', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(169, 'Minimum number of characters for the VAT number (EU only)', 'ENTRY_TVA_INTRACOM_MIN_LENGTH', '14', 'Minimum number of characters required for the VAT number (excluding the ISO country code is automatically indicated).<br /><br /><font color="#FF0000"><b> Note :</b> Valid only for europe</font>', 19, 9, '2006-10-29 16:10:35', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(170, 'Minimum number of characters for the ZIP code', 'ENTRY_POSTCODE_PRO_MIN_LENGTH', '3', 'Minimum number of characters for the ZIP code.', 19, 10, '2006-10-29 22:31:16', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(171, 'Minimum number of characters for the city', 'ENTRY_CITY_PRO_MIN_LENGTH', '3', 'Minimum number of characters for the city.', 19, 11, '2006-10-29 22:31:31', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(172, 'Minimum number of characters for the department or state', 'ENTRY_STATE_PRO_MIN_LENGTH', '3', 'Minimum number of characters for the department or state.', 19, 12, '2006-10-29 22:31:47', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(173, 'Minimum number of characters for the phone', 'ENTRY_TELEPHONE_PRO_MIN_LENGTH', '3', 'Minimum number of characters for the phone.', 19, 13, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(174, 'Minimum number of characters for the password', 'ENTRY_PASSWORD_PRO_MIN_LENGTH', '5', 'Minimum number of characters for the password.', 19, 14, NULL, '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(175, 'Maximum number of characters for the  company name', 'ENTRY_COMPANY_PRO_MAX_LENGTH', '128', 'Maximum number of characters for the  company name.', 20, 1, '2006-10-29 15:21:20', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(176, 'Maximum number of characters for the registration of the company', 'ENTRY_SIRET_MAX_LENGTH', '14', 'Maximum number of characters for the registration of the company.<br><br><i>(eg for RCS France or Quebec NEQ)</i>', 20, 2, '2006-10-29 15:21:14', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(177, 'Maximum number of characters for the nomenclature of the company', 'ENTRY_CODE_APE_MAX_LENGTH', '4', 'Maximum number of characters for the nomenclature of the company.<br><br><i>(eg 721Z for APE code in France)</i>', 20, 3, '2006-10-29 15:21:06', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(178, 'Maximum number of characters for the EU VAT (Europe only)', 'ENTRY_TVA_INTRACOM_MAX_LENGTH', '14', 'Maximum number of characters for the EU VAT (excluding the ISO country code is automatically indicated).<br><br><font color="#FF0000"><b> Note :</b> Valide uniquement pour l''europe</font>', 20, 4, '2006-10-29 15:20:57', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(209, 'Installed Modules', '', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2006-08-21 23:28:24', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(234, 'Please indicate the maximum number of rows you want displayed in the list of administration', 'MAX_DISPLAY_SEARCH_RESULTS_ADMIN', '20', 'Affiche un nombre de ligne maximale dans afficher dans les listing de l''administration', 21, NULL, '2007-06-03 13:09:14', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(235, 'Receive emails Registered customers', 'EMAIL_INFORMA_ACCOUNT_ADMIN', 'true', 'Do you want to receive the registration email clients in the creation of his account ?<br />', 22, 1, '2007-05-05 13:33:01', '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(236, 'Last Database Restore', 'DB_LAST_RESTORE', 'ClicShopping_05_05_2007_v2.sql', 'Last database restore file', 6, 0, '1000-01-01 00:00:00', '2007-05-07 10:12:36', '', '');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(248, 'Display the product model.', 'DISPLAY_MAJR_MODEL', 'true', 'Do you want to view the model ?<br />', 24, 1, '2007-05-24 23:05:01', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(249, 'Change the product model.', 'MODIFY_MAJR_MODEL', 'true', 'Do you want to change the model ?<br />', 24, 2, '2007-05-24 23:05:15', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(250, 'Change the product name.', 'MODIFY_MAJR_NAME', 'true', 'Do you want to view and edit the product name ?<br />', 24, 3, '2007-05-24 23:05:26', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(251, 'Display and change the status of product.', 'DISPLAY_MAJR_STATUS', 'true', 'Do you want to view and edit the product status ?<br />', 24, 4, '2007-05-24 23:05:36', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(252, 'Display and change the weight of products.', 'DISPLAY_MAJR_WEIGHT', 'true', 'Do you want to view and modify the weight of products ?<br>', 24, 5, '2007-05-24 23:06:02', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(253, 'Display and change the stock.', 'DISPLAY_MAJR_QUANTITY', 'true', 'Do you want to view and edit stock  ?<br />', 24, 6, '2007-05-24 23:06:12', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(254, 'Display and change the product image.', 'DISPLAY_MAJR_IMAGE', 'false', 'Do you want to view and edit product image ?<br>', 24, 7, '2007-05-24 23:06:24', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(255, 'Display the product manufacturer.', 'DISPLAY_MAJR_MANUFACTURER', 'true', 'Do you want  to view the product manufacturer ?<br>', 24, 8, '2007-05-24 23:06:36', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(256, 'Change the product manufacturer.', 'MODIFY_MAJR_MANUFACTURER', 'true', 'Do you want  to modify the product manufacturer ?<br>', 24, 9, '2007-05-24 23:06:47', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(257, 'Display the prices B2B groups.', 'DISPLAY_MAJR_B2B', 'true', 'Do you want to view the prices of B2B groups ?<br>', 24, 10, '2007-05-24 23:06:59', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(258, 'Display the products tax.', 'DISPLAY_MAJR_TAX', 'true', 'Do you want view tax products ?<br />', 24, 11, '2007-05-24 23:07:11', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(259, 'Change the tax class products.', 'MODIFY_MAJR_TAX', 'true', 'Do you want iew and modify the tax class to which products are subjected ?<br />', 24, 12, '2007-05-24 23:07:26', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(260, 'Display the prices all taxes when mouseover.', 'DISPLAY_MAJR_TVA_OVER', 'true', 'Do you want to see the price all taxes products when you mouse over it ?<br />', 24, 13, '2007-05-24 23:07:42', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(261, 'Display the prices all taxes when typing price.', 'DISPLAY_MAJR_TVA_UP', 'true', 'Do you want that taxes products prices displayed dynamically as you enter the price ?<br />', 24, 14, '2007-05-24 00:00:00', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(262, 'Display a link to view product page.', 'DISPLAY_MAJR_PREVIEW', 'true', 'Do you want a link to view product sheet ?<br />', 24, 15, '2007-05-24 23:08:16', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(263, 'Display a link to edit a product.', 'DISPLAY_MAJR_EDIT', 'true', 'Do you want a link to edit a product ?<br />', 24, 16, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(318, 'store Administration to use (B2C, B2B or B2C/B2B)', 'MODE_MANAGEMENT_B2C_B2B', 'B2C_B2B', 'Please select below the setup mode to use for the registration of your clients and the operation of the store.', 17, 1, '2008-09-14 20:47:30', '2007-06-24 02:52:29', NULL, 'osc_cfg_select_option(array(''B2C'', ''B2B'', ''B2C_B2B''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(319, 'Are you subject to the legislation of Ecotax ? (nonfunctional)', 'DISPLAY_ECOTAX', 'false', 'Position <i>True</i> calculating the Ecotax will be considered billing, do not forget to check if the price are correct.<br /><br /><b><font color="#FF0000">Not working on this ClicShopping version</font></b>', 25, 1, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(320, 'Are you in a country managing a double tax sale ?', 'DISPLAY_DOUBLE_TAXE', 'false', 'Position <i>True</i> you can manage 2 taxes (eg Federalists States - Province / Federal) on your bill. <br />', 25, 2, '2008-09-15 18:41:51', '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(321, 'Do you want to display the "Accept the general conditions"', 'DISPLAY_CONDITIONS_ON_CHECKOUT', 'true', 'Would you like to view the terms and conditions of purchase during the checkout process ?<br />', 25, 3, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(322, 'Do you want ro display the terms of confidentiality to create an account', 'DISPLAY_PRIVACY_CONDITIONS', 'true', 'Do you want to see the conditions of privacy during the process of account creation ?<br />', 0, 4, NULL, '2007-07-15 00:00:00', 'NULL', 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(323, 'URL terms of sale', 'SHOP_CODE_URL_CONDITIONS_VENTE', 'page_manager.php?pages_id=4', 'Please indicate the URL terms of sale.', 0, 5, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(324, 'URL Privacy Policy;', 'SHOP_CODE_URL_CONFIDENTIALITY', 'page_manager.php?pages_id=5', 'Please indicate the URL of the Privacy Policy.', 0, 6, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(325, 'Capital of the company', 'SHOP_CODE_CAPITAL', 'SARL au capital de 4000 Ã©', 'Please indicate your company''s capital.', 25, 7, '2008-09-15 11:45:32', '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(326, 'Intracommunity VAT of the Company (Europe only)', 'TVA_SHOP_INTRACOM', 'TVA Intracommunautaire : FR14568557555', 'Please indicate your company''s VAT registration.<br/ ><br />- <i>Leave blank if it does not apply to you.</i>', 25, 8, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(327, 'Number of provincial tax', 'TVA_SHOP_PROVINCIAL', 'TPS/GST : R127066546', 'Please indicate the administrative number of the provincial tax (ex : TPS/GST : R127066546).<br /><br />- <i>Leave blank if it does not apply to you.</i>', 25, 9, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(328, 'Number of federal tax', 'TVA_SHOP_FEDERAL', 'TVQ/QST : 1006197104', 'Please specify the number of administrative federal tax(ex TVQ/QST : 1006197104).<br /><br />- <i>Leave blank if it does not apply to you.</i>', 25, 10, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(330, 'Registration number of the company', 'SHOP_CODE_RCS', 'RCS : 1234567', 'Please specify the registration number of your company (ex RCS : 1234567).<br /><br />- <i>Leave blank if it does not apply to you.</i>', 25, 11, '2008-09-15 09:46:19', '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(331, 'Nomenclature number (or otherwise) of the company', 'SHOP_CODE_APE', 'Code APE : 721Z', 'Please specify the number of the Nomenclature of your company (ex : Code APE : 721z).<br /><br />- <i>Leave blank if it does not apply to you.</i>', 25, 12, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(332, 'Legal information on invoices', 'SHOP_DIVERS', '', 'Other legal information to be included on invoices.<br /><br />- <i>Leave blank if it does not apply to you.</i>', 25, 13, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(333, 'Display near the price an indication of the type of tax', 'DISPLAY_PRODUCT_PRICE_VALUE_TAX', 'true', 'display after price an indication if the product price is taxed (eg VAT) or not taxed (eg VAT).<br />', 22, 2, NULL, '2008-04-14 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(334, 'Display near the price of a indiquation tax type (for customer groups)', 'DISPLAY_PRODUCT_PRICE_VALUE_TAX_PRO', 'true', 'Display near the  price if the product price is taxed (eg VAT) or untaxed(ex: HT) for customer groups.<br />', 17, 6, NULL, '2008-04-14 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(336, 'Do you want to remove the header of PDF invoices and delivery notes', 'DISPLAY_INVOICE_HEADER', 'false', 'If yes (True) then the header will be deleted invoices.<br />', 26, 2, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(337, 'Do you want to remove footer of PDF invoices and delivery notes', 'DISPLAY_INVOICE_FOOTER', 'false', 'If yes (True) then the footer of invoices will be deleted.<br />', 26, 3, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(338, 'Filename of the logo to appear on the invoice and delivery notes', 'INVOICE_LOGO', 'invoice_logo.jpg', 'Please specify the name of your logo.<br /><br /><font color="#FF0000"><b>Note :</b> our logo (gif, jpg, png) must be inserted in the directory image/logo/invoice.<br />Be careful its height<font>', 7, 6, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(339, 'Order status to display invoices', 'INVOICE_DISPLAY_ORDER_STATUT', '1', 'Please select below the default status that will allow to print invoices.<br><br><font color="#FF0000"><b>Note :</b> Other articles will print a purchase order.</font><br />', 26, 5, '2008-06-23 23:42:37', '2008-06-23 00:00:00', 'osc_get_orders_status_name', 'osc_cfg_pull_down_order_status_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(340, 'Display Method bills', 'INVOICE_DISPLAY_ORDER', 'true', 'On yes (true) you will have the ability to display purchase orders and invoices by status configured by default.<br />', 26, 4, '2008-06-23 23:15:21', '2008-06-23 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(341, 'ClicShopping Version', 'PROJECT_VERSION', 'ClicShopping est une marque d&eacute;pos&eacute;e (INPI : 3810384) V2.55 -  All right Reserved - ', 'Name and version ClicShopping', 0, NULL, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(342, 'Specify the maximum quantity that the customer can order in his basket', 'MAX_QTY_IN_CART', '99', 'Insert a maximum quantity that the customer can put in the basket for an order.<br><i> - 0 for no limit</i><br />', 3, 19, NULL, '2008-08-30 10:21:58', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(343, 'Sessions setup', 'MYSESSION_LIFETIME', '3600', 'Determine the time you want before That a session ends on a page without refreshing. Measured in seconds. <br /><br /><i>(Please select a value other than 0)</i>', 15, 8, '2005-01-19 03:43:31', '2005-01-19 03:30:25', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(356, 'Please enter your delivery time by defaults to indicate at your customers', 'DISPLAY_SHIPPING_DELAY', '4 days', 'Please indicate your delivery time products to your customer by default<br><br>.<b>Note :</b><br /><i>- For France this information is mandatory (laws Chatel)</i>', 25, 14, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(357, 'Default state for an edition of invoice / order PDF', 'DEFAULT_ORDERS_STATUS_INVOICE_ID', '1', 'When a new generation editing invoice / order PDF is created, his order status will be assigned to it.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(358, 'Do you want to make your store offline for Maintenance ?', 'STORE_OFFLINE', 'false', '<br>If true, your site will be taken offline and customers can no take an order<br />', 1, 23, NULL, '1000-01-01 00:00:00', 'NULL', 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(359, 'Administrator authorized to connect to the site when it is in maintenance', 'STORE_OFFLINE_ALLOW', '', '<br>Please specify your IP address or yours IP addresses. If you have multiple IP addresses, please follow the instructions in parentheses (Each IP must be separated by commas <br />ex: 127.0.0.1,222.0.0.5', 1, 24, '2007-03-07 20:36:44', '2007-03-07 20:04:07', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(376, 'Installed Modules', 'MODULE_PAYMENT_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, '2008-12-05 19:09:30', '2008-09-16 16:13:41', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(377, 'Specify the minimum quantity that the customer can insert in his basket', 'MAX_MIN_IN_CART', '1', 'Insert a minimum default quantity that the customer must put in the basket for an order.<br><br><i> - 1 : for a default quantity</i></br><br><i> - 0 : prevents all orders made ​​on the site by users (not recommended)</i>', 3, 19, NULL, '2008-08-30 10:21:58', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(399, 'Installed Modules', 'MODULE_SHIPPING_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, '2008-12-05 19:10:05', '2008-12-05 19:09:42', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(433, 'Installed Modules', 'MODULE_ORDER_TOTAL_INSTALLED', 'ot_subtotal.php;ot_shipping.php;ot_tax.php;ot_total.php', 'This is automatically updated. No need to edit.', 6, 0, '2008-12-05 19:10:58', '2008-12-05 19:10:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(434, 'Do you want display the shipping cost', 'MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'true', 'Do you want display the shipping cost in the order ?', 6, 1, NULL, '2008-12-05 19:10:50', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(435, 'Sort Order for display', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', '2', 'Sort order for display (The smallest number is shown first).', 6, 2, NULL, '2008-12-05 19:10:50', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(436, 'Allow free delivery', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'false', 'Would you accept free delivery depending on the amount ?', 6, 3, NULL, '2008-12-05 19:10:50', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(437, 'Livraison gratuite pour commande au dessus', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', '50', 'Permettre la livraison gratuite pour les commandes au dessus du montant suivant.', 6, 4, NULL, '2008-12-05 19:10:50', 'currencies->format', NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(438, 'Attacher livraison gratuite pour les destinations', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION', 'national', 'Livraison gratuite pour des commandes envoy&eacute;s &agrave; l''ensemble des destinations.<br>(both=tous les deux)', 6, 5, NULL, '2008-12-05 19:10:50', NULL, 'osc_cfg_select_option(array(''national'', ''international'', ''both''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(439, 'Affichage du sous-total', 'MODULE_ORDER_TOTAL_SUBTOTAL_STATUS', 'true', 'Voulez-vous montrer le sous-total de la commande ?', 6, 1, NULL, '2008-12-05 19:10:52', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(440, 'Ordre de tri', 'MODULE_ORDER_TOTAL_SUBTOTAL_SORT_ORDER', '1', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:52', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(441, 'Affichage de la taxe', 'MODULE_ORDER_TOTAL_TAX_STATUS', 'true', 'Voulez-vous montrer la taxe de la commande ?', 6, 1, NULL, '2008-12-05 19:10:55', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(442, 'Ordre de tri', 'MODULE_ORDER_TOTAL_TAX_SORT_ORDER', '3', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:55', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(443, 'Affichage du total', 'MODULE_ORDER_TOTAL_TOTAL_STATUS', 'true', 'Voulez-vous montrer le total de la commande ?', 6, 1, NULL, '2008-12-05 19:10:58', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(444, 'Ordre de tri', 'MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER', '4', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:58', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(445, 'Coupon number assigned to the customer during the account creation', 'COUPON_CUSTOMER', '', 'For any change in the coupon code, please refer to the Marketing / coupon section of your menu', 0, NULL, NULL, '1000-01-01 00:00:00', ' ', NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(446, 'Coupon number assigned to the customer during the account creation B2B', 'COUPON_CUSTOMER_B2B', '', 'For any change in the coupon code, please refer to the Marketing / coupon section of your menu', 0, NULL, NULL, '1000-01-01 00:00:00', ' ', NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(447, 'Display and change the minimum order quantity', 'DISPLAY_QTY_MIN_ORDER', 'true', 'Do you want to display and change the minimum order quantity a customer can achieve?<br />', 24, 7, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(456, 'Do you want to receive an email alert for the setting of stock Alert ?', 'STOCK_ALERT_PRODUCT_REORDER_LEVEL', 'false', 'An email alert will be sent if the stock reaches its alert level ?<br />', 9, 6, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(457, 'Do You want to receive an email alert for exhausted products  ?', 'STOCK_ALERT_PRODUCT_EXHAUSTED', 'false', 'An email alert will be sent if the stock is below 0 ?<br />', 9, 7, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(460, 'Do you want to insert a security code to export files xml, txt, csv generated ?', 'EXPORT_CODE', 'Gh87yDE', 'This code allows access to data import by people or robots that are allowed to perform this operation. We strongly recommend that you change the source code and create a complex (letters and numbers).', 36, 1, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(461, 'Display authorization for products intended for price comparison.', 'DISPLAY_MAJR_PRICE_COMPARISON', 'true', 'Do you want to know which products are allowed to be included in the export for price comparison ?<br />', 24, 18, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(462, 'Specify the number of days you want to see the little icon appear next to your new products ?', 'DAY_NEW_PRODUCTS_ARRIVAL', '30', 'Please indicate the number of days you want to display a small icon "new". <br> In terms of the number of days, the icon will not be displayed<br />', 3, 9, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(502, 'Do you want to display banners ?', 'STORE_DISPLAY_BANNERS', 'true', 'Do you want to display the banners on the site ?', 4, 10, '2010-07-21 19:20:37', '2010-07-21 19:20:37', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(503, 'Small image height for administration', 'SMALL_IMAGE_HEIGHT_ADMIN', '50', 'Number of pixels for the height of the small images articles.', 23, 8, '2007-05-19 16:34:35', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(504, 'Small Image Width administration', 'SMALL_IMAGE_WIDTH_ADMIN', '', 'The number of pixels for the width of the header image.', 23, 9, '2007-05-19 16:34:44', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(505, 'Modules install&eacute;s', 'MODULE_ACTION_RECORDER_INSTALLED', 'ar_admin_login.php;ar_contact_us.php;ar_tell_a_friend.php', 'liste des actions enregistr&eacute;es concernant les modules (s&eacute;par&eacute;s par des points virgules). Mise &agrave; jour automatique. Ce n''est pas utile de l''editer.', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(506, 'Quel d&eacute;lais minimum souhaitez-vous concernant l''envoi par emails par le module contactez-nous', 'MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES', '15', 'Veuillez indiquer un nombre minimum pour permettre l''envoi des emails.<br><font color="#FF0000"><br><b>Note :</b></font>15 pour l''envoi des emails tous les 15 minutes)', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(507, 'Quel d&eacute;lais minimum souhaitez vous concernant l''envoi par emails par le module envoyez &agrave; un ami ?', 'MODULE_ACTION_RECORDER_TELL_A_FRIEND_EMAIL_MINUTES', '15', 'Veuillez indiquer un nombre minimum pour permettre l''envoi des emails.<br><font color="#FF0000"><br><b>Note :</b></font>15 pour l''envoi des emails tous les 15 minutes)', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(508, 'Veuiller indiquer le temps d''attente pour une erreur de connexion dans la partie administration', 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES', '5', 'Veuillez indiquer le nombre de minutes d''attente concernant une nouvelle connexion dans l''administration', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(509, 'Veuiller indiquer le nombre de login permis pour se connecter dans la partie administration ', 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_ATTEMPTS', '3', 'Veuillez indiquer le nombre de login permis concernant une connexion dans l''administration', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(531, 'Installed Template Block Groups', 'TEMPLATE_BLOCK_GROUPS', ';header_tags;modules_account_customers;modules_advanced_search;modules_blog;modules_blog_content;modules_boxes;modules_checkout_success;modules_contact_us;modules_create_account;modules_create_account_pro;modules_footer_suffix;modules_footer;modules_front_page;modules_header;modules_index_categories;modules_login;modules_products_featured;modules_products_heart;modules_products_info;modules_products_listing;modules_products_new;modules_products_special;modules_shopping_cart;modules_sitemap', 'This is automatically updated. No need to edit.', 6, 0, '2014-02-07 20:02:26', '2010-11-12 21:28:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(534, 'Installed Modules', 'MODULE_BOXES_INSTALLED', '', 'List of box module filenames separated by a semi-colon. This is automatically updated. No need to edit.', 6, 0, NULL, '2011-01-17 19:45:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(536, 'Please indicate the type of default prefix for the model of the product', 'CONFIGURATION_PREFIX_MODEL', 'REF-', 'Please indicate the type of default prefix that you want on the product number.<br /><br /><i>ex : product model : <b>REF-</b>product number ', 7, 7, NULL, '2011-01-17 19:45:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(537, 'Modules install&eacute;es', 'MODULE_SOCIAL_BOOKMARKS_INSTALLED', '', 'Liste des modules des r&eacute;seaux sociaux s&eacute;par&eacute; par un point virgule. Elle est automatiquement mis a jour.', 6, 0, '2014-02-07 20:07:16', '2011-01-17 19:45:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(538, 'Do you want to show mobile phone', 'ACCOUNT_CELLULAR_PHONE', 'false', 'Display the the field cell phone number in the "My Account" and the registration form.<br />', 5, 6, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(539, 'Do you ou want to  display the fax ?', 'ACCOUNT_FAX', 'false', 'Display the the Fax field in the "My Account" and the registration form.<br />', 5, 7, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(540, 'Do you ou want to  display mobile phone ?', 'ACCOUNT_CELLULAR_PHONE_PRO', 'false', 'Display the the field cell phone number in the "My Account" and the registration form.<br />', 18, 161, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(541, 'Do you ou want to display the Fax ?', 'ACCOUNT_FAX_PRO', 'false', 'Display the the Fax field in the "My Account" and the registration form.<br />', 18, 162, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(542, 'Please indicate the color in RGB text editing invoices / orders', 'INVOICE_RGB', '158,11,14', 'Veuillez indiquer la couleur du texte au format RGB<br><br><font color="#FF0000"><b>Note :</b>Each number must be separated by a comma(ex : 0,0,0 for the black)</font><br>', 26, 5, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(544, 'Default status for orders by type of quantity', 'DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID', '1', 'When a new type of quantity is created, this status will be assigned.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(545, 'Please select the type of cache you want to use ?', 'USU5_CACHE_SYSTEM', 'memcache', 'Please choose one of the options on the strategy of cache setting.', 34, 5, '2011-01-09 21:12:26', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''mysql'', ''file'',''sqlite'',''memcache''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(546, 'Please specify the number of days stokage for the cache.', 'USU5_CACHE_DAYS', '7', 'Please specify the number of days of storage cache, the cache timeout will be automatically updated.', 34, 6, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(547, 'Please choose the type of display URL', 'USU5_URLS_TYPE', 'rewrite', '<b>Choose the URL type of display</b>', 34, 7, '2011-01-08 21:37:49', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''standard'',''path_standard'',''rewrite'',''path_rewrite'',), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(548, 'Please select the display mode on the text link of the product that will be built in the url', 'USU5_PRODUCTS_LINK_TEXT_ORDER', 'p', 'Please indicate the type of link that will be built in the url:<br /><br /><b>p</b> = product name<br /><b>c</b> = category name<br /><b>b</b> = model<br /><b>m</b> =brand<br /><br /><i>ex : <b>bp</b> (brand/product)</i>', 34, 8, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(549, 'Do you want to  update cache', 'USU5_RESET_CACHE', 'false', '<span style="color: red;">Note : </span>This can be useful to do so when you modify a product eg<br />', 34, 17, '2011-01-09 22:48:06', '2011-01-08 21:22:06', 'osc_reset_cache_data_usu5', 'osc_cfg_select_option(array(''reset'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(550, 'Do you want to  display the variables treated in the site ?', 'USU5_DEBUG_OUPUT_VARS', 'false', '<span style="color: red;"><strong>Do not use this tool if you are in a mode of production</strong></span><br /><br />This system allows you to view the contents of variables handled by the rewriting manager.<br />', 34, 15, '2011-01-08 21:39:51', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(551, 'Do you want to  see the system performance', 'USU5_OUPUT_PERFORMANCE', 'false', '<span style="color: red;"><strong>Do not use this tool if you are in a mode of production.</stong></span><br /><br />This system allows you to display the performance of queries that will be displayed at the bottom of the webpage.<br />', 34, 14, '2011-01-09 22:15:04', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(552, 'Do you want to  remove all non-alphanumeric characters ?', 'USU5_REMOVE_ALL_SPEC_CHARS', 'true', 'This setting removes all non-letters and not numbers. The result changes the writing url.<br /><br /><i>(ex :my-product-p-1.html become myproduct-p-1.html)<br />', 34, 11, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(553, 'Do you want to  add cPath in the URL of your products ?', 'USU5_ADD_CPATH_TO_PRODUCT_URLS', 'false', 'This parameter inserts the cPath to the end of the URL of the product<br /><br /><i>(ex. - my-product-p-1.html?cPath=xx).<br />', 34, 12, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(554, 'Do you want to  convert special characters ?', 'USU5_CHAR_CONVERT_SET', '', 'This parameter allows you to convert some characters.<br /><br />the format <b>DOIT</b> must be of the form : <b>char=>conv, char2=>conv2</b><br />', 34, 13, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(555, 'Do you want forcing the URL of the index page ?', 'USU5_HOME_PAGE_REDIRECT', 'false', 'Force redirection www.mysite.com/ when www.mysite.com/index.php displayed ?<br />', 34, 16, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(556, 'Do you want to Filter words during ?', 'USU5_FILTER_SHORT_WORDS', '2', 'This parameter allows you to filter words that have a lower number of characters in the value that you specify in the URL. <br /><br />1 = Remove the words of 1 letter <br />2 = Remove 2-letter words or less <br />3 = Remove words of 3 letters or less<br />', 34, 9, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''1'',''2'',''3'',), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(557, 'Do you want to add the parent category at the beginning of the URL ?', 'USU5_ADD_CAT_PARENT', 'true', 'This parameter inserts the name of the parent category of the product at the beginning of the URL. <br /><br /><i>(ex : category-parent-c-1.html)<br />', 34, 10, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(558, 'Do you want to  activate the system multi-language ?', 'USU5_MULTI_LANGUAGE_SEO_SUPPORT', 'false', 'If you manage multiple languages, please select this option.<br />', 34, 3, '2011-01-09 21:12:11', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(559, 'Do you want URL related to W3C standards ?', 'USU5_USE_W3C_VALID', 'true', 'This parameter (True) creates URLs conform to the standards established by the W3C group.<br />', 34, 4, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(560, 'Do you want to enable rewriting URLs?', 'USU5_ENABLED', 'false', 'Use modified URLs for all links in the store.<br />', 34, 1, '2011-01-09 22:47:43', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(561, 'Do you want  to enable the cache manager ?', 'USU5_CACHE_ON', 'false', 'Cache Manager lets you view pages faster. Please note that a waiting period is required before the page display (otherwise update the cache).<br />', 34, 2, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(562, 'Please indicate the maximum number of attributes to display in the file setting attributes', 'MAX_ROW_LISTS_OPTIONS', '17', 'Please indicate the maximum number of attributes to display', 21, 2, '1000-01-01 00:00:00', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(567, 'Website module installed', 'WEBSITE_MODULE_INSTALLED', '0', 'Verification si le site contient un module installe ou pas', 0, 0, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(569, 'Show only products available on the internet (not the store) ?', 'DISPLAY_MAJR_PRODUCTS_ONLY_ONLINE', 'false', 'Do you want to display only products available on the Internet (not the store) ?<br>', 24, 16, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(570, 'Edit product supplier.', 'MODIFY_MAJR_SUPPLIER', 'false', 'Do you want to change the supplier ?<br />', 24, 18, '2007-05-24 23:06:47', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(571, 'Display product supplier.', 'DISPLAY_MAJR_SUPPLIER', 'false', 'Do you want to view product supplier ?<br />', 24, 19, '2007-05-24 23:06:36', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(572, 'Display a small preview image of the product ?', 'DISPLAY_MAJR_PREVIEW_IMAGE', 'false', 'Do you want to display a small preview image of the product ?<br />', 24, 17, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(573, 'Please select the type of bar code', 'BAR_CODE_TYPE', 'EAN', 'Please indicate the type of bar code.', 9, 8, '2008-09-14 20:47:30', '2007-06-24 02:52:29', NULL, 'osc_cfg_select_option(array(''C128'', ''C128C'', ''C25'', ''C25I'', ''MSI'', ''EAN'', ''UPC'', ''C39'', ''C11'', ''CODABAR'', ''POSTNET'', ''CMC7'', ''KIX''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(574, 'Please indicate the size of the bar code', 'BAR_CODE_SIZE', '50', 'Please indicate the size of the image of the bar code.', 9, 9, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(575, 'Please indicate the color of the bar code', 'BAR_CODE_COLOR', '#254433', 'Please indicate the color of the bar code (in hezadécimal).', 9, 10, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(576, 'Please indicate extansion image that will be generated', 'BAR_CODE_EXTENSION', 'png', 'Please indicate extansion image that will be generated.', 9, 11, '2008-09-14 20:47:30', '2007-06-24 02:52:29', NULL, 'osc_cfg_select_option(array(''png'', ''gif'', ''jpg''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(577, 'Do you want to manage the manual setup of images in the product description page', 'MANUAL_IMAGE_PRODUCTS_DESCRIPTION', 'false', 'The display of the image in manual will allow you to have better manage your images in the visual part of the description of products and access to a gallery of images.<br />', 23, 3, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(578, 'Do you want to manage the manual setup of images in the product description page', 'MANUAL_IMAGE_PRODUCTS_DESCRIPTION', 'false', 'The display of the image in manual will allow you to have better manage your images in the visual part of product description and access to a gallery of images.<br>', 23, 3, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(579, 'Image Medium: please specify the width in the product description page', 'MEDIUM_IMAGE_WIDTH', '250', 'Please indicate the number of pixels for the width of the average images in the page description of the items', 4, 11, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(580, 'Image Medium: please specify the height in the product description page', 'MEDIUM_IMAGE_HEIGHT', '', 'Please indicate the number of pixels for the height of the average images in the page description of the items', 4, 12, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(581, 'Image Zoom: please specify the width in the product description page', 'BIG_IMAGE_WIDTH', '640', 'Please indicate the number of pixels for the width of the zoom images in the page description of the items', 4, 13, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(582, 'Image Zoom: please specify the height in the product description page', 'BIG_IMAGE_HEIGHT', '', 'Please specify the number of pixels for the height of the zoom images in the page description of the items', 4, 14, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(583, 'Image Medium: please specify the width in the product description page', 'MEDIUM_IMAGE_WIDTH', '250', 'Please specify the number of pixels for the width of the average images in the page description of the items', 4, 11, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(584, 'Image Medium: please specify the height in the product description page', 'MEDIUM_IMAGE_HEIGHT', '', 'Please specify the number of pixels for the height of the average images in the page description of the items', 4, 12, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(585, 'Image Zoom: please specify the width in the product description page', 'BIG_IMAGE_WIDTH', '640', 'Please specify the number of pixels for the width of the zoom images in the page description of the items', 4, 13, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(586, 'Image Zoom: please specify the height in the product description page', 'BIG_IMAGE_HEIGHT', '', 'Please indicate the number of pixels for the height of the zoom images in the page description of the items', 4, 14, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(587, 'Display a department of the company in the Contact Us form', 'CONTACT_DEPARTMENT_LIST', '', 'Please indicate which departments to contact which will be listed in the Contact Us form.<br /><br /> <font color="#FF0000"><strong>Notes :</strong></font> <br /><br />- the list of departments must be under the form <strong> department1<department1@mydomain.com>, Département2<department2@mydomain.com> separated by a comma</strong><br />- The default Receive email is your contact email Store</font><br />', 1, 5, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(588, 'Default status for tracking shipments', 'DEFAULT_ORDERS_STATUS_TRACKING_ID', '1', 'When a new generation editing invoice / order PDF is created,status tracking will be assigned to it.', 0, 6, '1000-01-01 00:00:00', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(589, 'Do you want allow pre-order products if the authorization off stock purchases is True (under test)', 'PRE_ORDER_AUTORISATION', 'false', 'Si vous autorisez la pr&eacute;-commande, the button exhausted product will not appear anymore and the customer will spend his pre-order.<br />', 9, 3, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(590, 'Receive an email if a customer makes a comment', 'REVIEW_COMMENT_SEND_EMAIL', 'false', 'If the client makes a comment on a product you will receive an email alert.<br />', 1, 14, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(591, 'Do you want change the purchase price when switching from a mobile phone or tablet?', 'PRICE_MOBILE_ACCEPT', 'false', 'You can increase or decrease the purchase price of the product if the visit from a mobile phone or tablet. <br />', 22, 3, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(592, 'Amount of increase / decrease in price for mobile phones and tablets', 'PRICE_MOBILE_POURCENTAGE', '0', '<br /><br /> <font color="#FF0000"><strong>Notes :</strong></font> <br /><br />- The increase applies only to prices B2C. <br /><br />- 10 = 10% increase (Please do not use the %)<br />- 0 for a normal price', 22, 4, '1000-01-01 00:00:00', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(593, 'Do you want display an automatic default currency by continent ?', 'CONFIGURATION_CURRENCIES_GEOLOCALISATION', 'false', 'Depending on the source of the client and the continent, the price of the product defaults to the currency of the continent client.<br /><br /><u><strong>Note :</strong></u><br />- The default currency implemented are: USD, EUR, CAD <br />- If the customer comes from another continent, it will be the default currency that is displayed.<br />- The automatic adjustment of currencies depending on the language does not work in this case.<br />', 1, 9, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(594, 'Size of site columns', 'GRID_CONTAINER_WITH', '12', 'Please enter a numeric value.', 43, 5, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(595, 'Column Size Site Content', 'GRID_CONTENT_WITH', '8', 'Please enter a numeric value.', 43, 6, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(687, 'What type of container should the page content (full-size or centered)', 'BOOTSTRAP_CONTAINER', 'container', 'Please, select your preference', 43, 1, NULL, '2015-02-19 21:39:51', NULL, 'osc_cfg_select_option(array(''container-fluid'', ''container''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(599, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_NEW_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2013-09-01 11:31:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(600, 'Installed Modules', 'MODULE_MODULES_BOXES_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2013-09-01 11:31:33', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(601, 'Do you want display the price if it is equal to 0', 'NOT_DISPLAY_PRICE_ZERO', 'true', 'Display or not the price of the product if it is equal to 0.<br />', 22, 2, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(602, 'Please indicate the maximum number of scores you want to display', 'MAX_DISPLAY_NEW_REVIEWS', '5', 'Please enter a numeric value.', 3, 8, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(603, 'Installed Modules', 'MODULE_ADMIN_DASHBOARD_INSTALLED', 'd_total_month.php;d_total_ca_by_year.php;d_total_revenue.php;d_total_customers.php;d_orders.php;d_twitter.php', 'This is automatically updated. No need to edit.', 6, 0, '2013-12-16 18:12:18', '2013-12-16 18:11:51', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(604, 'Souhaitez-vous afficher les derni&egrave;res commandes ?', 'MODULE_ADMIN_DASHBOARD_ORDERS_STATUS', 'True', 'Module qui affiche les derni&egrave;res commandes pass&eacute;es', 6, 1, NULL, '2013-12-16 18:11:57', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(605, 'Combien de commande souhaitez-vous afficher ?', 'MODULE_ADMIN_DASHBOARD_ORDERS_LIMIT', '10', 'Veuillez indiquer le nombre de commande &agrave; afficher.', 6, 2, NULL, '2013-12-16 18:11:57', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(606, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_ORDERS_SORT_ORDER', '60', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 3, NULL, '2013-12-16 18:11:57', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(607, 'Souhaitez-vous faire apparaitre le graphique sur votre tableau concernant votre chiffre d''affaires total par ann&eacute;e ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS', 'True', 'Le chiffre d''affaires comprend toutes les ventes effectu&eacute;es et liv&eacute;es dans lann&eacute;e calcul&eacute;es en fonction du sous-total ?', 6, 1, NULL, '2013-12-16 18:12:06', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(608, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_SORT_ORDER', '30', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 0, NULL, '2013-12-16 18:12:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(609, 'Souhaitez-vous connaitre l&eacute;volution de nombre de clients ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_STATUS', 'True', 'Nombre de Clients inscrit au cours des 30 derniers jours', 6, 1, NULL, '2013-12-16 18:12:10', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(610, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_SORT_ORDER', '50', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2013-12-16 18:12:10', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(611, 'Souhaitez-vous faire apparaitre le graphique sur votre tableau concernant l''&eacute;volution du chiffre d''affaires par mois ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS', 'True', 'Le chiffre d''affaires comprend les ventes qui ont &eacute;t&eacute; r&eacute;alis&eacute;es dans le mois ?', 6, 1, NULL, '2013-12-16 18:12:14', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(612, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_SORT_ORDER', '20', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 3, NULL, '2013-12-16 18:12:14', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(613, 'Souhaitez-vous faire apparaitre le graphique sur votre tableau concernant votre chiffre d''affaires journalier ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_STATUS', 'True', 'Le chiffre d''affaires comprend les ventes qui ont les statuts suivants, en attente, en cours, livr&eacute; ?', 6, 1, NULL, '2013-12-16 18:12:18', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(614, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_SORT_ORDER', '40', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 0, NULL, '2013-12-16 18:12:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(615, 'Souhaitez-vous utiliser le module twitter pour envoyer des Twits', 'MODULE_ADMIN_DASHBOARD_TWITTER_STATUS', 'True', 'Ce module n&eacute;cessite de cr&eacute;er un compte sur twitter.com pour pouvoir l''utiliser.<br /><br /><b><i>Note :</i></b>Vous devez aussi vous identifier sur cette page <a href="http://dev.twitter.com/apps/" target="_blank">http://dev.twitter.com/apps/</a> (connectez-vous avant sur votre compe) puis remplissez le formulaire et r&eacute;cup&eacute;rez les codes confidentiels.<br /><br /><font color="red"><b>le mode access level doit etre en read / write</b></font>', 6, 1, NULL, '2013-12-16 18:12:22', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(616, 'Veuillez ins&eacute;rer votre code consumerKey', 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_KEY', '', 'Veuillez indiquer le code consumerKey', 6, 2, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(617, 'Veuillez ins&eacute;rer votre code consumerSecret', 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_SECRET', '', 'Veuillez indiquer le code consumerSecret', 6, 3, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(618, 'Veuillez ins&eacute;rer votre code accessToken', 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN', '', 'Veuillez indiquer le code accessToken<br /><br /><b>Note : </b> cliquer sur le bouton My access token ', 6, 3, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(619, 'Veuillez ins&eacute;rer votre code accessTokenSecret', 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN_SECRET', '', 'Veuillez indiquer le code accessTokenSecret<br /><br /> <b>Note : </b> cliquer sur le bouton My access token', 6, 3, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(620, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TWITTER_SORT_ORDER', '70', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 4, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(623, 'Installed Modules', 'MODULE_MODULES_HEADER_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(624, 'Installed Modules', 'MODULE_MODULES_FOOTER_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:20', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(625, 'Installed Modules', 'MODULE_MODULES_ADVANCED_SEARCH_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:23', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(626, 'Installed Modules', 'MODULE_MODULES_SITEMAP_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:25', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(627, 'Installed Modules', 'MODULE_MODULES_CONTACT_US_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:28', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(628, 'Installed Modules', 'MODULE_MODULES_BLOG_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:31', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(629, 'Installed Modules', 'MODULE_MODULES_BLOG_CONTENT_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:34', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(630, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_INFO_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:42', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(631, 'Installed Modules', 'MODULE_MODULES_SHOPPING_CART_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:44', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(632, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_SHIPPING_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(633, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_PAYMENT_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:56', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(634, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_CONFIRMATION_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:03', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(635, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_SUCCESS_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:07', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(636, 'Installed Modules', 'MODULE_MODULES_FRONT_PAGE_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:11', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(637, 'Installed Modules', 'MODULE_MODULES_INDEX_CATEGORIES_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:13', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(638, 'Installed Modules', 'MODULE_MODULES_LOGIN_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:15', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(639, 'Installed Modules', 'MODULE_MODULES_ACCOUNT_CUSTOMERS_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(640, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_HEART_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:23', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(641, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_SPECIAL_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:26', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(642, 'Minimum number of characters for the Mobile Phone', 'ENTRY_CELLULAR_PHONE_MIN_LENGTH', '3', 'inimum number of characters for the Mobile Phone.', 16, 10, '2006-10-23 22:49:04', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(643, 'Minimum number of characters for the Mobile Phone', 'ENTRY_CELLULAR_PHONE_PRO_MIN_LENGTH', '3', 'Minimum number of characters for the Mobile Phone.', 19, 13, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(644, 'Minimum number of characters for the provincial tax', 'ENTRY_CODE_TAXE_PROVINCIALE_MIN_LENGTH', '3', 'Minimum number of characters for the provincial tax.', 19, 15, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(645, 'Minimum number of characters for the federal tax', 'ENTRY_CODE_TAXE_FEDERALE_MIN_LENGTH', '3', 'Minimum number of characters for the federal tax.', 19, 16, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(646, 'Installed Modules', 'MODULE_HEADER_TAGS_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:07:32', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(647, 'Do you want display the acceptance of the order by the consumer (Hamon law)', 'CONFIGURATION_LAW_HAMON', 'false', 'Hamon law (French) requires the consumer to accept its order. Do you want to display ?>', 25, 3, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(648, 'What type of menu do you want to appear in your administration ?', 'CONFIGURATION_MENU_NAVIGATION', 'computer', 'Display a horizontal menu or a vertical menu in the administration.<br /><br /><i>(Value True = Vertical Menu - Value False = Horizontal Menu)', 1, 400, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''computer'', ''tablet'', ''both''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(649, 'SMTP hosts', 'EMAIL_SMTP_HOSTS', '', 'Assign SMTP host senders', 12, 6, NULL, '2014-05-16 11:19:59', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(650, 'SMTP authentication', 'EMAIL_SMTP_AUTHENTICATION', 'true', 'Do you want authenticated SMTP server?', 12, 7, NULL, '2014-05-16 11:19:59', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(651, 'SMTP Password', 'EMAIL_SMTP_PASSWORD', '', 'Add SMTP Password for SMTP protocol', 12, 8, NULL, '2014-05-16 11:19:59', 'osc_cfg_password', 'osc_cfg_input_password(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(652, 'SMTP User Name', 'EMAIL_SMTP_USER', '', 'Add a user for the SMTP protocol', 12, 9, NULL, '2014-05-16 11:21:13', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(653, 'SMTP User', 'EMAIL_SMTP_USER', '', 'Add SMTP user for SMTP protocol', 12, 9, NULL, '2014-05-16 11:19:59', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(654, 'Do you want use the SMTP secured protocol', 'EMAIL_SMTP_SECURE', 'no', 'use a secured SMTP protocol', 12, 11, NULL, '2014-06-02 11:52:39', NULL, 'osc_cfg_select_option(array(''no'', ''ssl'', ''tls''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(655, 'SMTP Reply To', 'EMAIL_SMTP_REPLYTO', '', 'Add SMTP reply to address', 12, 10, NULL, '2014-05-16 11:19:59', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(656, 'Do you want to be in open sales ou private sales ?', 'MODE_VENTE_PRIVEE', 'false', 'If your choice is true, all the customer must create an account to access at catalog sur les mode B2B/B2C - B2C- B2C)', 22, 2, NULL, '2014-08-20 15:51:12', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(657, 'Do you want activate the Odoo Webservice ?', 'ODOO_ACTIVATE_WEB_SERVICE', 'false', 'Enable Webservice will allow you to use with Odoo<br /><br/><Strong>Note :</strong><br /><br/> - Please, be carefull with theses informations below.<br />- Please, install the odoo module, if you want to use ClicShopping and Odoo', 44, 1, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(658, 'Do you want enable the store in private mode ?', 'MODE_VENTE_PRIVEE', 'false', 'The customer must create a account or not to acces at the catalog with this settings B2B/B2C - B2C- B2C', 22, 2, NULL, '2014-08-20 15:51:50', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(659, '- Step 2 Setting - Please, insert the port number using by Odoo', 'ODOO_PORT_WEB_SERVICE', '', 'Please, insert the port number using by Odoo<br /><br />- Example : 8069', 44, 3, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(660, '- Step 3 Setting - Please, insert the Odoo User', 'ODOO_USER_WEB_SERVICE', '', 'Please insert the odoo User. This is a odoo connexion login, It could be an email for example.<br /><br /><strong><u>Note :</u><br /><br /> - To have a maximal security, please, do not insert the Admin odoo user', 44, 4, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(661, '- Step 4 Setting - Please, insert the odoo password of user', 'ODOO_PASSWORD_WEB_SERVICE', '', 'Please, insert the Odoo password user', 44, 5, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(662, '- Step 5 Setting - Please, insert the database name use by Odoo', 'ODOO_DATABASE_NAME_WEB_SERVICE', '', 'Please, insert the Odoo database name', 44, 6, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(663, 'Do you want to receive an email when there is a connexion error ClicShopping to Odoo ?', 'ODOO_EMAIL_WEB_SERVICE', 'false', 'An email will be sent if there is an error', 44, 1, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(664, 'Do you want activate the web service to save the customers creating an account by the catalog ?', 'ODOO_ACTIVATE_CUSTOMERS_CATALOG', 'false', 'Save all the customers creating an account by the catalog or change their information to Odoo', 44, 8, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(666, 'Do you want activate the web service ti create / modify the products in the administration ?', 'ODOO_ACTIVATE_PRODUCTS_ADMIN', 'false', 'Save all products modification or creation in Odoo administration', 44, 11, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(667, 'Do you want activate the web service to registred the administration order to Odoo ?', 'ODOO_WEB_SERVICE_ORDERS_ADMIN', 'false', 'Enregistre la mise &agrave; jour de la commande de l''administration vers Odoo<strong>Note :</strong><br /><br /> - - Do not forget to check the tax codes in the section ClicShopping Location and taxes / tax rate', 44, 12, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(668, '- Step 6 Setting - Please insert the company name created in odoo', 'ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE', '', 'Insert the company name created in Odoo<br />- Please respect,the name registred in Odoo', 44, 7, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(669, '- Step 1 Accounting; - Please insert the sell account number of Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_SELL', '', 'Odoo use an account to save the invoice.<br /><br /><strong>Note :</strong><br /><br />- In France, its the 707100 (french accounting)<br />- Please, respect the Odoo account number', 44, 14, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(670, '- Step 2 Accounting - Please insert the customer account number of Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_PURCHASE', '', 'Odoo use an account to save the invoice.<br /><br /><strong>Note :</strong><br /><br />- In France, it''s the 411100 (french accounting)<br />- Please, respect the Odoo account number', 44, 15, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(671, '- Step 3 Accounting - Please insert the shipping account number of Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_SHIPPING', '', 'Odoo use an account to save the invoice.<br /><br /><strong>Note :</strong><br /><br />- In France, it''s the 708500 (french accounting)<br />- Please, respect the Odoo account number', 44, 16, NULL, '2014-11-03 13:54:17', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(672, 'Do you want allow a modification on defaul in ClicShopping ?', 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG', 'false', 'This allows customer to change his default billing address.<br /><br /><strong>Note :</strong><br /><br />- He is strongly recommended to keep this feature option on <strong>false</strong> when you use invoice option (below). Accept address changes, will an <u>important repercussion in the whole management of your invoice Odoo</u> (bug Odoo)<br />.', 44, 10, NULL, '2014-11-05 14:57:21', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(673, '- Step 4 Accounting - Please insert the discount account number of Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_DISCOUNT', '', 'Odoo use an account to save the invoice inside the accounting.<br /><br /><strong>Note :</strong><br /><br />- In France, it''s the 709700 (french accounting)<br />- Please, respect the Odoo account number', 44, 16, NULL, '2014-12-04 14:23:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(674, 'Do you want enable the web service to the save the catalog order to Odoo ?', 'ODOO_ACTIVATE_ORDER_CATALOG', 'none', 'Billing type must be selected registered according to your process.<br /><br /><strong>Note :</strong><br /><br />- None : No registration will be processed in Odoo by ClicShopping.<br/><br />- Order : Save the ClicShopping order like order in Odoo. You will be treated and fill afterwards  the treatment in Odoo and ClicShopping.<br /><br />- Invoice : The order placed by the customer is directly saved as invoice Odoo.<br /><br />- Advise : Do not forget adjust your order status by default according to your choice.', 44, 10, NULL, '2014-12-04 14:23:47', NULL, 'osc_cfg_select_option(array(''none'', ''order'', ''invoice''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(675, '- Step 7 Export - Do you want export the products in Odoo Odoo ?', 'ODOO_EXPORT_PRODUCTS', 'false', 'Export all ClicShopping products inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of products, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1 - If you suppliers, please import the suppliers in first.</br >- 2 - To start the export process , please <strong>select the Export option</strong> validate and re-edit option. This will trigger the export process <br />- 3 - As soon as the export process is completed,  please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br />', 44, 87, '2014-12-04 14:24:07', '2014-12-04 14:24:07', 'osc_bulk_product', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(676, '- Step 3 Export - Do you want export the customers in odoo ?', 'ODOO_EXPORT_CUSTOMERS', 'false', 'Export all ClicShopping customers inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of customers, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong>, then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed, please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = customers exportation - Valeur False = No)</i>', 44, 83, '2014-12-04 14:24:22', '2014-12-04 14:24:22', 'osc_bulk_customer', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(677, 'Do you want activate the creation or modification for the suppliers to Odoo ?', 'ODOO_ACTIVATE_SUPPLIERS_ADMIN', 'false', 'Save all creations or modifications for the suppliers to Odoo ?', 44, 8, NULL, '2014-12-04 14:23:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(678, 'Do you want activate the web service to create / modify the customers in the administration ?', 'ODOO_ACTIVATE_CUSTOMERS_ADMIN', 'false', 'Save all customer modification or creation in the admin to Odoo', 44, 9, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(679, '- Step 4 Export - Do you want export the suppliers in Odoo ?', 'ODOO_EXPORT_SUPPLIERS', 'false', 'Export all ClicShopping suppliers inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of suppliers, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong>, then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed, please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = customers exportation - Valeur False = No)</i>', 44, 84, '2015-01-12 20:30:18', '2015-01-12 20:30:18', 'osc_bulk_suppliers', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(680, '- Step 1 Export - Do you want configure the Wharehouse  ?', 'ODOO_WHAREHOUSE_CONFIG', 'false', 'Create a ClicShopping warehouse.<br /><br /><strong>Note :</strong><br /><br />This configuration allow you to manage your stock in Odoo<br /><br /> - 1-To start the configuration process, please <strong>select the option : Configure</strong>, then validate and re-edit option. This will trigger the wharehouse configuration process<br />- 2 - As soon as the configuration process is completed,  please <strong>select the option on False</strong>. This will avoid triggering the configuration process again later.<br /><br />- In case of error or mishandling by you, we accept no liability;<br /><br /><i>(Valeur configure = Configuration wahrehouse - Valeur False = No)</i>', 44, 81, '2015-01-12 20:30:18', '2015-01-12 20:30:18', 'osc_config_create_wharehouse', 'osc_cfg_select_option(array(''configure'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(681, '- Step 6 Export - Do you want export the manufacturers/brands in Odoo Odoo ?', 'ODOO_EXPORT_MANUFACTURER', 'false', 'Export all ClicShopping product brand inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of suppliers, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong>, then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed, please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = brands exportation - Valeur False = No)</i>', 44, 86, '2015-01-12 20:32:30', '2015-01-12 20:32:30', 'osc_bulk_manufacturer', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(682, '- Step 5 Export - Do you want export the product categories in Odoo Odoo ?', 'ODOO_EXPORT_CATEGORIES', 'false', 'Export all ClicShopping categories inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of suppliers, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong>, then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed, please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = catgories exportation - Valeur False = No)</i>', 44, 85, '2015-01-12 20:32:41', '2015-01-12 20:32:41', 'osc_bulk_categories', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(683, '- Step 2 - Do you want export the customers groups in Odoo ?', 'ODOO_EXPORT_CUSTOMERS_GROUP', 'false', 'Export all ClicShopping customer groups in Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of suppliers, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong>, then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed, please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = customers exportation - Valeur False = No)</i>', 44, 82, '2015-01-30 20:07:48', '2015-01-30 20:07:48', 'osc_bulk_customer_group', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(686, 'Do you want enable the web service to save the customer groups in Odoo ?', 'ODOO_ACTIVATE_CUSTOMERS_GROUP', 'false', 'Save all customers groups in Odoo.', 44, 8, NULL, '2015-01-30 20:18:20', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(688, 'Do you want activate the  web service to save the brands in Odoo ?', 'ODOO_ACTIVATE_MANUFACTURER_ADMIN', 'false', 'Activate the  web service to save the brands in Odoo', 44, 9, NULL, '2015-01-30 19:58:59', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(690, 'Installed Modules', 'MODULE_MODULES_FOOTER_SUFFIX_INSTALLED', 'sf_footer_suffix_copyright.php', 'This is automatically updated. No need to edit.', 6, 0, '2015-04-05 11:16:32', '2015-04-05 11:16:27', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(691, 'Souhaitez-vous activer ce module ?', 'MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS', 'True', 'Souhaitez vous activer ce module &agrave; votre boutique ?', 6, 1, NULL, '2015-04-05 11:16:32', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(692, 'Ordre de tri d''affichage', 'MODULES_FOOTER_SUFFIX_COPYRIGHT_SORT_ORDER', '100', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 3, NULL, '2015-04-05 11:16:32', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(684, 'Do you want to save all customers support  request come form ClicShopping ?', 'ODOO_ACTIVATE_SUPPORT_CONTACT_US_ODOO', 'false', 'Save all customers support  request come form ClicShopping', 44, 8, NULL, '2015-02-19 21:46:19', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');



CREATE TABLE IF NOT EXISTS `configuration_group` (
  `configuration_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_group_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `configuration_group_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(5) DEFAULT NULL,
  `visible` int(1) DEFAULT '1',
  PRIMARY KEY (`configuration_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(1, 'Store Setup', 'General Information on the Store.', 1, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(2, 'Setup credit card', 'Minimum value: functions / data', 2, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(3, 'Setup maximum and minimum values', 'Values​​: functions / data', 3, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(4, 'Image Setup', 'Image setting', 4, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(5, 'Setup B2C customers', 'Customer account setting.', 5, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(6, 'Setup options Modules', 'Hidden setting.', 6, 0);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(7, 'Setup Shipping / Packing', 'Delivery options available in this store.', 7, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(8, 'Setup the Product List', 'Setting options lists of products.', 8, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(9, 'Stock setup', 'Setting options of stock.', 9, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(10, 'Logging setup', 'Setting Logging options.', 10, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(11, 'Setup Cache', 'Setting Cache  Options.', 11, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(12, 'Setup options mail', 'General setting for the mail client and emails in HTML format.', 12, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(13, 'Download Setup', 'Options downloadable products.', 13, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(14, 'Setup GZip compression', 'GZip compression options.', 14, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(15, 'Session Setup', 'Session options', 15, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(16, 'Setup minimum values ​​for B2C customers', 'Minimum value for the field of B2C customers', 16, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(17, 'B2B Setup', 'General Setting B2B', 17, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(18, 'Setup B2B customers', 'Setting inscriptions B2B group customers', 18, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(19, 'Setup minimum values ​​for B2B customers', 'Minimum value for the field of B2B customers', 19, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(20, 'Setup maximum values ​​for B2B customers', 'Maximum value for the field of B2B customers', 20, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(21, 'Setup maximum values', 'Maximum value for: functions / data', 21, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(22, 'B2C setup', 'General setting for the B2C', 22, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(23, 'Image setup', 'Image Setting', 23, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(24, 'Setup quick update', 'Please choose what you want to modify in the module Price Updates.', 24, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(25, 'Setup regulatory legislation', 'Regulatory options for setup your site', 25, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(26, 'Setup invoices and shipments', 'Setting invoices and shipments', 26, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(27, 'SEO Setup and statistics', 'Options for managing SEO and Statistics', 27, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(28, 'Setup product also bought', 'Display information in the product description sheet', 28, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(29, 'Setup display specials listing', 'Sheet Listing Description promotions', 29, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(30, 'Setup the Home page and categories', 'Values ​​concerning the setup of the home page and categories', 30, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(31, 'Setup boxes left and right', 'Value in the setup of boxes left and right', 31, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(32, 'Setup Comments', 'Value for comments', 32, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(33, 'Setup news listing', 'Value for the listing of new', 33, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(34, 'Setup site URL', 'Setting options URL', 34, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(35, 'Setup listing favorites', 'Values ​​for the listing of favorites', 35, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(36, 'Setup import / export', 'Setting import and export.', 36, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(37, 'Setup specials for home page', 'Setting specials for home page', 37, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(39, 'Setup Similar Products', 'Information on similar products', 2, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(40, 'Setup of cross products', 'Information concerning cross products', 2, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(43, 'General and miscellaneous Setup design', 'General and miscellaneous Setting design', 43, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(44, 'Web Service', 'Connect via webservice at new external application', NULL, 1);

CREATE TABLE IF NOT EXISTS `contact_customers` (
  `contact_customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_department` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_enquiry` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_date_added` datetime NOT NULL,
  `languages_id` int(11) NOT NULL,
  `contact_customers_archive` tinyint(1) NOT NULL,
  `contact_customers_status` tinyint(1) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `contact_telephone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`contact_customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `contact_customers_follow` (
  `id_contact_customers_follow` int(11) NOT NULL AUTO_INCREMENT,
  `contact_customers_id` int(11) NOT NULL,
  `administrator_user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_response` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contact_date_sending` datetime NOT NULL,
  PRIMARY KEY (`id_contact_customers_follow`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `counter` (
  `startdate` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `counter` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `counter_history` (
  `month` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `counter` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_2` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_3` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `address_format_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`countries_id`),
  KEY `IDX_COUNTRIES_NAME` (`countries_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=240 ;

INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(1, 'Afghanistan', 'AF', 'AFG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(2, 'Albania', 'AL', 'ALB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(3, 'Algeria', 'DZ', 'DZA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(4, 'American Samoa', 'AS', 'ASM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(5, 'Andorra', 'AD', 'AND', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(6, 'Angola', 'AO', 'AGO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(7, 'Anguilla', 'AI', 'AIA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(8, 'Antarctica', 'AQ', 'ATA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(9, 'Antigua and Barbuda', 'AG', 'ATG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(10, 'Argentina', 'AR', 'ARG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(11, 'Armenia', 'AM', 'ARM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(12, 'Aruba', 'AW', 'ABW', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(13, 'Australia', 'AU', 'AUS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(14, 'Austria', 'AT', 'AUT', 5, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(15, 'Azerbaijan', 'AZ', 'AZE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(16, 'Bahamas', 'BS', 'BHS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(17, 'Bahrain', 'BH', 'BHR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(18, 'Bangladesh', 'BD', 'BGD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(19, 'Barbados', 'BB', 'BRB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(20, 'Belarus', 'BY', 'BLR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(21, 'Belgium', 'BE', 'BEL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(22, 'Belize', 'BZ', 'BLZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(23, 'Benin', 'BJ', 'BEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(24, 'Bermuda', 'BM', 'BMU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(25, 'Bhutan', 'BT', 'BTN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(26, 'Bolivia', 'BO', 'BOL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(28, 'Botswana', 'BW', 'BWA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(29, 'Bouvet Island', 'BV', 'BVT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(30, 'Brazil', 'BR', 'BRA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(32, 'Brunei Darussalam', 'BN', 'BRN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(33, 'Bulgaria', 'BG', 'BGR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(34, 'Burkina Faso', 'BF', 'BFA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(35, 'Burundi', 'BI', 'BDI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(36, 'Cambodia', 'KH', 'KHM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(37, 'Cameroon', 'CM', 'CMR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(38, 'Canada', 'CA', 'CAN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(39, 'Cape Verde', 'CV', 'CPV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(40, 'Cayman Islands', 'KY', 'CYM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(41, 'Central African Republic', 'CF', 'CAF', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(42, 'Chad', 'TD', 'TCD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(43, 'Chile', 'CL', 'CHL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(44, 'China', 'CN', 'CHN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(45, 'Christmas Island', 'CX', 'CXR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(47, 'Colombia', 'CO', 'COL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(48, 'Comoros', 'KM', 'COM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(49, 'Congo', 'CG', 'COG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(50, 'Cook Islands', 'CK', 'COK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(51, 'Costa Rica', 'CR', 'CRI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(52, 'Cote D''Ivoire', 'CI', 'CIV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(53, 'Croatia', 'HR', 'HRV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(54, 'Cuba', 'CU', 'CUB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(55, 'Cyprus', 'CY', 'CYP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(56, 'Czech Republic', 'CZ', 'CZE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(57, 'Denmark', 'DK', 'DNK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(58, 'Djibouti', 'DJ', 'DJI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(59, 'Dominica', 'DM', 'DMA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(60, 'Dominican Republic', 'DO', 'DOM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(61, 'East Timor', 'TP', 'TMP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(62, 'Ecuador', 'EC', 'ECU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(63, 'Egypt', 'EG', 'EGY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(64, 'El Salvador', 'SV', 'SLV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(66, 'Eritrea', 'ER', 'ERI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(67, 'Estonia', 'EE', 'EST', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(68, 'Ethiopia', 'ET', 'ETH', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(70, 'Faroe Islands', 'FO', 'FRO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(71, 'Fiji', 'FJ', 'FJI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(72, 'Finland', 'FI', 'FIN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(73, 'France', 'FR', 'FRA', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(75, 'French Guiana', 'GF', 'GUF', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(76, 'French Polynesia', 'PF', 'PYF', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(77, 'French Southern Territories', 'TF', 'ATF', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(78, 'Gabon', 'GA', 'GAB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(79, 'Gambia', 'GM', 'GMB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(80, 'Georgia', 'GE', 'GEO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(81, 'Germany', 'DE', 'DEU', 5, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(82, 'Ghana', 'GH', 'GHA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(83, 'Gibraltar', 'GI', 'GIB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(84, 'Greece', 'GR', 'GRC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(85, 'Greenland', 'GL', 'GRL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(86, 'Grenada', 'GD', 'GRD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(87, 'Guadeloupe', 'GP', 'GLP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(88, 'Guam', 'GU', 'GUM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(89, 'Guatemala', 'GT', 'GTM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(90, 'Guinea', 'GN', 'GIN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(91, 'Guinea-bissau', 'GW', 'GNB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(92, 'Guyana', 'GY', 'GUY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(93, 'Haiti', 'HT', 'HTI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(95, 'Honduras', 'HN', 'HND', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(96, 'Hong Kong', 'HK', 'HKG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(97, 'Hungary', 'HU', 'HUN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(98, 'Iceland', 'IS', 'ISL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(99, 'India', 'IN', 'IND', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(100, 'Indonesia', 'ID', 'IDN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(102, 'Iraq', 'IQ', 'IRQ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(103, 'Ireland', 'IE', 'IRL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(104, 'Israel', 'IL', 'ISR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(105, 'Italy', 'IT', 'ITA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(106, 'Jamaica', 'JM', 'JAM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(107, 'Japan', 'JP', 'JPN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(108, 'Jordan', 'JO', 'JOR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(109, 'Kazakhstan', 'KZ', 'KAZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(110, 'Kenya', 'KE', 'KEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(111, 'Kiribati', 'KI', 'KIR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(112, 'Korea, Democratic People''s Republic of', 'KP', 'PRK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(113, 'Korea, Republic of', 'KR', 'KOR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(114, 'Kuwait', 'KW', 'KWT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(115, 'Kyrgyzstan', 'KG', 'KGZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(117, 'Latvia', 'LV', 'LVA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(118, 'Lebanon', 'LB', 'LBN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(119, 'Lesotho', 'LS', 'LSO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(120, 'Liberia', 'LR', 'LBR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(122, 'Liechtenstein', 'LI', 'LIE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(123, 'Lithuania', 'LT', 'LTU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(124, 'Luxembourg', 'LU', 'LUX', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(125, 'Macau', 'MO', 'MAC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(127, 'Madagascar', 'MG', 'MDG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(128, 'Malawi', 'MW', 'MWI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(129, 'Malaysia', 'MY', 'MYS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(130, 'Maldives', 'MV', 'MDV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(131, 'Mali', 'ML', 'MLI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(132, 'Malta', 'MT', 'MLT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(133, 'Marshall Islands', 'MH', 'MHL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(134, 'Martinique', 'MQ', 'MTQ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(135, 'Mauritania', 'MR', 'MRT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(136, 'Mauritius', 'MU', 'MUS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(137, 'Mayotte', 'YT', 'MYT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(138, 'Mexico', 'MX', 'MEX', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(139, 'Micronesia, Federated States of', 'FM', 'FSM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(140, 'Moldova, Republic of', 'MD', 'MDA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(141, 'Monaco', 'MC', 'MCO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(142, 'Mongolia', 'MN', 'MNG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(143, 'Montserrat', 'MS', 'MSR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(144, 'Morocco', 'MA', 'MAR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(145, 'Mozambique', 'MZ', 'MOZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(146, 'Myanmar', 'MM', 'MMR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(147, 'Namibia', 'NA', 'NAM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(148, 'Nauru', 'NR', 'NRU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(149, 'Nepal', 'NP', 'NPL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(150, 'Netherlands', 'NL', 'NLD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(151, 'Netherlands Antilles', 'AN', 'ANT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(152, 'New Caledonia', 'NC', 'NCL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(153, 'New Zealand', 'NZ', 'NZL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(154, 'Nicaragua', 'NI', 'NIC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(155, 'Niger', 'NE', 'NER', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(156, 'Nigeria', 'NG', 'NGA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(157, 'Niue', 'NU', 'NIU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(158, 'Norfolk Island', 'NF', 'NFK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(159, 'Northern Mariana Islands', 'MP', 'MNP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(160, 'Norway', 'NO', 'NOR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(161, 'Oman', 'OM', 'OMN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(162, 'Pakistan', 'PK', 'PAK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(163, 'Palau', 'PW', 'PLW', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(164, 'Panama', 'PA', 'PAN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(165, 'Papua New Guinea', 'PG', 'PNG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(166, 'Paraguay', 'PY', 'PRY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(167, 'Peru', 'PE', 'PER', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(168, 'Philippines', 'PH', 'PHL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(169, 'Pitcairn', 'PN', 'PCN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(170, 'Poland', 'PL', 'POL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(171, 'Portugal', 'PT', 'PRT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(172, 'Puerto Rico', 'PR', 'PRI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(173, 'Qatar', 'QA', 'QAT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(174, 'Reunion', 'RE', 'REU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(175, 'Romania', 'RO', 'ROM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(176, 'Russian Federation', 'RU', 'RUS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(177, 'Rwanda', 'RW', 'RWA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(179, 'Saint Lucia', 'LC', 'LCA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(181, 'Samoa', 'WS', 'WSM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(182, 'San Marino', 'SM', 'SMR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(183, 'Sao Tome and Principe', 'ST', 'STP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(184, 'Saudi Arabia', 'SA', 'SAU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(185, 'Senegal', 'SN', 'SEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(186, 'Seychelles', 'SC', 'SYC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(187, 'Sierra Leone', 'SL', 'SLE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(188, 'Singapore', 'SG', 'SGP', 4, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(190, 'Slovenia', 'SI', 'SVN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(191, 'Solomon Islands', 'SB', 'SLB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(192, 'Somalia', 'SO', 'SOM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(193, 'South Africa', 'ZA', 'ZAF', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(195, 'Spain', 'ES', 'ESP', 3, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(196, 'Sri Lanka', 'LK', 'LKA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(197, 'St. Helena', 'SH', 'SHN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(199, 'Sudan', 'SD', 'SDN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(200, 'Suriname', 'SR', 'SUR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(202, 'Swaziland', 'SZ', 'SWZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(203, 'Sweden', 'SE', 'SWE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(204, 'Switzerland', 'CH', 'CHE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(205, 'Syrian Arab Republic', 'SY', 'SYR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(206, 'Taiwan', 'TW', 'TWN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(207, 'Tajikistan', 'TJ', 'TJK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(209, 'Thailand', 'TH', 'THA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(210, 'Togo', 'TG', 'TGO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(211, 'Tokelau', 'TK', 'TKL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(212, 'Tonga', 'TO', 'TON', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(213, 'Trinidad and Tobago', 'TT', 'TTO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(214, 'Tunisia', 'TN', 'TUN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(215, 'Turkey', 'TR', 'TUR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(216, 'Turkmenistan', 'TM', 'TKM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(218, 'Tuvalu', 'TV', 'TUV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(219, 'Uganda', 'UG', 'UGA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(220, 'Ukraine', 'UA', 'UKR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(221, 'United Arab Emirates', 'AE', 'ARE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(222, 'United Kingdom', 'GB', 'GBR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(223, 'United States', 'US', 'USA', 2, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(225, 'Uruguay', 'UY', 'URY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(226, 'Uzbekistan', 'UZ', 'UZB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(227, 'Vanuatu', 'VU', 'VUT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(229, 'Venezuela', 'VE', 'VEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(230, 'Viet Nam', 'VN', 'VNM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(231, 'Virgin Islands (British)', 'VG', 'VGB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(234, 'Western Sahara', 'EH', 'ESH', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(235, 'Yemen', 'YE', 'YEM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(236, 'Yugoslavia', 'YU', 'YUG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(237, 'Zaire', 'ZR', 'ZAR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(238, 'Zambia', 'ZM', 'ZMB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(239, 'Zimbabwe', 'ZW', 'ZWE', 1, 1);

CREATE TABLE IF NOT EXISTS `currencies` (
  `currencies_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `symbol_left` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol_right` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thousands_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_places` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` float(13,8) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`currencies_id`),
  KEY `idx_currencies_code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES(1, 'Euro', 'EUR', '', 'EUR', '.', ',', '2', 1.00000000, '2008-09-13 18:02:35');
INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES(2, 'Dollard', 'USD', 'USD', '', '.', ',', '2', 1.40750003, '2008-09-13 18:02:36');
INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES(3, 'Canada', 'CAD', '', 'CAD', '.', '.', '2', 1.50580001, '2008-09-13 18:02:36');

CREATE TABLE IF NOT EXISTS `customers` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_siret` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_ape` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_tva_intracom` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_tva_intracom_code_iso` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `customers_gender` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `customers_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_dob` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `customers_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_default_address_id` int(11) DEFAULT NULL,
  `customers_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `customers_newsletter` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languages_id` int(11) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `member_level` int(5) NOT NULL DEFAULT '0',
  `customers_options_order_taxe` tinyint(1) NOT NULL DEFAULT '0',
  `customers_modify_company` tinyint(1) NOT NULL DEFAULT '1',
  `customers_modify_address_default` tinyint(1) NOT NULL DEFAULT '1',
  `customers_add_address` tinyint(1) NOT NULL DEFAULT '1',
  `customers_cellular_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_email_validation` int(1) NOT NULL DEFAULT '0',
  `customer_discount` decimal(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`customers_id`),
  KEY `idx_customers_email_address` (`customers_email_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customers_basket` (
  `customers_basket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `products_id` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `customers_basket_quantity` int(2) NOT NULL DEFAULT '0',
  `final_price` decimal(15,4) DEFAULT NULL,
  `customers_basket_date_added` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`customers_basket_id`),
  KEY `idx_customers_basket_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customers_basket_attributes` (
  `customers_basket_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `products_id` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `products_options_value_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_basket_attributes_id`),
  KEY `idx_customers_basket_att_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customers_groups` (
  `customers_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_group_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `customers_group_discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `color_bar` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `group_order_taxe` tinyint(1) NOT NULL DEFAULT '0',
  `group_payment_unallowed` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cc',
  `group_shipping_unallowed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_tax` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `customers_group_quantity_default` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

INSERT INTO `customers_groups` (`customers_group_id`, `customers_group_name`, `customers_group_discount`, `color_bar`, `group_order_taxe`, `group_payment_unallowed`, `group_shipping_unallowed`, `group_tax`, `customers_group_quantity_default`) VALUES(1, 'Tarifs 1', 5.00, '#FF0000', 0, 'moneyorder', 'table', 'true', 0);

CREATE TABLE IF NOT EXISTS `customers_info` (
  `customers_info_id` int(11) NOT NULL DEFAULT '0',
  `customers_info_date_of_last_logon` datetime DEFAULT NULL,
  `customers_info_number_of_logons` int(5) DEFAULT NULL,
  `customers_info_date_account_created` datetime DEFAULT NULL,
  `customers_info_date_account_last_modified` datetime DEFAULT NULL,
  `global_product_notifications` int(1) DEFAULT '0',
  `password_reset_key` char(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_date` datetime DEFAULT NULL,
  PRIMARY KEY (`customers_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `customers_notes` (
  `customers_notes_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `customers_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_notes_date` datetime DEFAULT '1000-01-01 00:00:00',
  `user_administrator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customers_notes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `desjardins_reference` (
  `ref_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `desjardins_response` (
  `resp_id` int(11) NOT NULL AUTO_INCREMENT,
  `MAC` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TPE` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `montant` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `texte_libre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_retour` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `retourPLUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`resp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `discount_coupons` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coupons_description` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coupons_discount_amount` decimal(15,12) NOT NULL DEFAULT '0.000000000000',
  `coupons_discount_type` enum('fixed','percent','shipping') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'percent',
  `coupons_date_start` datetime DEFAULT NULL,
  `coupons_date_end` datetime DEFAULT NULL,
  `coupons_max_use` int(3) NOT NULL DEFAULT '0',
  `coupons_min_order` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `coupons_min_order_type` enum('price','quantity') COLLATE utf8_unicode_ci DEFAULT 'price',
  `coupons_number_available` int(3) NOT NULL DEFAULT '0',
  `coupons_create_account_b2c` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_create_account_b2b` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_twitter` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_categories` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_customers` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_manufacturers` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `manufacturers_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`manufacturers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_orders` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `orders_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_products` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_zones` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`geo_zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `geo_zones` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `geo_zone_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(1, 'France', 'Etat français avec tous les départements', '2008-09-15 10:04:04', '2006-04-11 18:48:30');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(2, 'Etats membres de l''UE', 'Etats membres de l''union européenne', '2008-09-15 10:03:58', '2006-05-04 10:27:42');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(3, 'Etats Zones Européennes', 'Tous les états ainsi que les départements', '2015-02-09 16:33:22', '2006-05-04 17:10:19');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(7, 'Zone Fédérale 5% - Canada GST', 'Zone Fédérale 5% - GST', '2015-02-09 16:08:55', '2008-09-15 21:48:44');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(8, 'Zone fédérale 12% HST BC', 'Zone fédérale 12% HST BC', '2015-02-09 16:04:08', '2008-09-15 21:49:02');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(9, 'Zone Provinciale', 'Canada (québec)', NULL, '2008-09-15 21:51:15');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(15, 'Taxe hamonisée Québec', 'Taxe hamonisée Québec', NULL, '2015-02-09 16:59:28');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(11, 'Zone fédérale 13% - NB / NF /', 'Zone fédérale 13%', NULL, '2015-02-09 16:05:34');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(12, 'Zone fédérale 14%', 'Zone fédérale 14%', NULL, '2015-02-09 16:07:02');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(13, 'Zone fédérale 15% - NS HST', 'Zone fédérale 15% - HST', NULL, '2015-02-09 16:07:53');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(14, 'Zone fédérale 7%', 'Zone fédérale 7%', NULL, '2015-02-09 16:11:43');

CREATE TABLE IF NOT EXISTS `groups_to_categories` (
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`customers_group_id`,`categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `information_email_customers` (
  `information_email_customers_id` int(11) NOT NULL,
  `information_email_customers_delay_90` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_60` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_30` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_15` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_7` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`information_email_customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `information_email_customers` (`information_email_customers_id`, `information_email_customers_delay_90`, `information_email_customers_delay_60`, `information_email_customers_delay_30`, `information_email_customers_delay_15`, `information_email_customers_delay_7`) VALUES(1, 0, 0, 0, 0, 0);

CREATE TABLE IF NOT EXISTS `languages` (
  `languages_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `directory` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`languages_id`),
  KEY `IDX_LANGUAGES_NAME` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `languages` (`languages_id`, `name`, `code`, `image`, `directory`, `sort_order`, `status`) VALUES(1, 'Francais', 'fr', 'french.gif', 'french', 1, 1);
INSERT INTO `languages` (`languages_id`, `name`, `code`, `image`, `directory`, `sort_order`, `status`) VALUES(2, 'Anglais', 'en', 'english.gif', 'english', 2, 1);

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `manufacturers_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturers_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturers_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `manufacturers_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manufacturers_id`),
  KEY `IDX_MANUFACTURERS_NAME` (`manufacturers_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `manufacturers_info` (
  `manufacturers_id` int(11) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `manufacturers_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_clicked` int(5) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL,
  `manufacturer_description` text COLLATE utf8_unicode_ci,
  `manufacturer_seo_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_seo_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_seo_keyword` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`manufacturers_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `newsletters` (
  `newsletters_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `date_sent` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `locked` int(1) DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `newsletters_accept_file` int(1) NOT NULL DEFAULT '0',
  `newsletters_twitter` tinyint(1) NOT NULL DEFAULT '0',
  `newsletters_customer_no_account` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsletters_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `newsletter_customers_temp` (
  `customers_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `newsletter_no_account` (
  `customers_firstname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_lastname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_email_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `customers_newsletter` tinyint(1) NOT NULL DEFAULT '1',
  `customers_date_added` datetime DEFAULT NULL,
  `languages_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`customers_email_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_siret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_ape` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_tva_intracom` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_email_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_cf` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_piva` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_owner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_expires` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_status_invoice` int(5) NOT NULL DEFAULT '1',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `client_computer_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_name_client` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_cellular_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orders_archive` int(1) NOT NULL DEFAULT '0',
  `odoo_invoice` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_id`),
  KEY `idx_orders_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_pages_manager` (
  `orders_page_manager_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `page_manager_general_condition` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_page_manager_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_products` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` int(2) NOT NULL DEFAULT '0',
  `products_full_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_products_orders_id` (`orders_id`),
  KEY `idx_orders_products_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_products_attributes` (
  `orders_products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_id` int(11) NOT NULL DEFAULT '0',
  `products_options` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_values` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `options_values_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `products_attributes_reference` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_products_attributes_id`),
  KEY `idx_orders_products_att_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_products_download` (
  `orders_products_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `download_maxdays` int(2) NOT NULL DEFAULT '0',
  `download_count` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_products_download_id`),
  KEY `idx_orders_products_download_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_status` (
  `orders_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `public_flag` int(11) DEFAULT '1',
  `downloads_flag` int(11) DEFAULT '0',
  `support_orders_flag` int(1) DEFAULT '0',
  PRIMARY KEY (`orders_status_id`,`language_id`),
  KEY `idx_orders_status_name` (`orders_status_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(1, 1, 'En instance', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(1, 2, 'Pending', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(2, 1, 'Traitement en cours', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(2, 2, 'processing', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(3, 1, 'Livr&eacute;', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(3, 2, 'Delivered', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(4, 1, 'Annul&eacute;', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(4, 2, 'Cancelled', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(5, 1, 'Support Client', 0, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(5, 2, 'Customers Support', 0, 0, 0);

CREATE TABLE IF NOT EXISTS `orders_status_history` (
  `orders_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) DEFAULT '0',
  `orders_status_id` int(5) DEFAULT '0',
  `orders_status_invoice_id` int(5) NOT NULL DEFAULT '1',
  `admin_user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `customer_notified` int(1) DEFAULT '0',
  `comments` text COLLATE utf8_unicode_ci,
  `orders_status_tracking_id` int(5) NOT NULL DEFAULT '0',
  `orders_tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`orders_status_history_id`),
  KEY `idx_orders_status_history_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_status_invoice` (
  `orders_status_invoice_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_invoice_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_status_invoice_id`,`language_id`),
  KEY `idx_orders_status_invoice_name` (`orders_status_invoice_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(3, 1, 'Annuler');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(4, 1, 'Avoir');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(3, 2, 'Cancelled');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(1, 1, 'Commande');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(2, 1, 'Facture');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(4, 2, 'Have a bill');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(2, 2, 'Invoice');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(1, 2, 'Order');

CREATE TABLE IF NOT EXISTS `orders_status_tracking` (
  `orders_status_tracking_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_tracking_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `orders_status_tracking_link` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_status_tracking_id`,`language_id`),
  KEY `idx_orders_status_tracking_name` (`orders_status_tracking_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(1, 1, '-- Aucune --', '');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(1, 2, '-- Nothing --', '');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(2, 1, 'Colissimo', 'http://www.coliposte.fr/particulier/suivi_particulier.jsp?colispart=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(2, 2, 'Colissimo', 'http://www.coliposte.fr/particulier/suivi_particulier.jsp?colispart=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(3, 1, 'Chronospost', 'http://www.fr.chronopost.com/web/fr/tracking/suivi_inter.jsp?listeNumeros=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(3, 2, 'Chronospost', 'http://www.fr.chronopost.com/web/fr/tracking/suivi_inter.jsp?listeNumeros=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(4, 1, 'FEDEX', 'http://fedex.com/Tracking?ascend_header=1&clienttype=dotcomreg&cntry_code=fr&language=french&tracknumbers=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(4, 2, 'FEDEX', 'http://fedex.com/Tracking?ascend_header=1&clienttype=dotcomreg&cntry_code=fr&language=french&tracknumbers=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(5, 1, 'UPS', 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(5, 2, 'UPS', 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(6, 1, 'Post Canada', 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(6, 2, 'Post Canada', 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(7, 1, 'Coliposte Pro', 'https://www.coliposte.net/pro/services/main.jsp?m=12003010&colispro=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(7, 2, 'Coliposte Pro', 'https://www.coliposte.net/pro/services/main.jsp?m=12003010&colispro=');

CREATE TABLE IF NOT EXISTS `orders_total` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_orders_total_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `pages_manager` (
  `pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `links_target` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `page_type` int(1) NOT NULL DEFAULT '0',
  `page_box` int(1) NOT NULL DEFAULT '0',
  `page_time` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `page_date_start` datetime DEFAULT NULL,
  `page_date_closed` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `last_modified` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `page_general_condition` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(1, '', 0, 1, 1, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2008-09-08 15:39:50', '2008-09-03 20:38:09', 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(2, '', 1, 1, 2, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2008-09-02 08:39:21', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(3, '', 2, 1, 3, 0, '', NULL, NULL, '1000-01-01 00:00:00', NULL, NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(4, '_self', 3, 1, 4, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2014-02-07 20:14:00', NULL, 0, 1);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(5, '', 4, 1, 4, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2008-09-16 14:55:26', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(7, '', 5, 1, 4, 0, '', NULL, NULL, '2008-09-02 10:36:53', '2008-09-02 10:37:16', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(8, '', 6, 1, 4, 0, '', NULL, NULL, '2008-09-16 14:48:16', '2008-09-16 14:51:26', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(9, '_self', 7, 1, 4, 0, '', NULL, NULL, '2009-08-30 15:51:56', '2009-08-30 15:52:32', NULL, 0, 0);

CREATE TABLE IF NOT EXISTS `pages_manager_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pages_id` int(11) DEFAULT NULL,
  `pages_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pages_html_text` longtext COLLATE utf8_unicode_ci,
  `externallink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `page_manager_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_manager_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_manager_head_keywords_tag` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(1, 1, 'page intro', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(2, 1, 'Intro Page', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(3, 2, 'Page Accueil', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(4, 2, 'MainPage', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(5, 3, 'Nous Contacter', '<p><strong>(indiquer le nom de votre site ou votre soci&eacute;t&eacute;) </strong></p>', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(6, 3, 'Contact Us', '<p><strong>(Specify the name of your website or company)</strong></p>', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(7, 4, 'Conditions Générales', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(8, 4, 'General Conditions', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(9, 5, 'Politiques de Confidentialité', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(10, 5, 'Confidential politics', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(13, 7, 'RSS', '', 'rss.php', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(14, 7, 'RSS', '', 'rss.php', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(17, 8, 'Cartographie du site', '', 'sitemap.php', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(18, 8, 'Sitemap', '', 'sitemap.php', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(19, 9, 'newsletter subscription', '', 'newsletter_no_account.php', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(20, 9, 'Inscription newsletter', '', 'newsletter_no_account.php', 1, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_quantity` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_ean` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_sku` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_image_zoom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `products_price_kilo` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `products_status` tinyint(1) NOT NULL DEFAULT '0',
  `products_tax_class_id` int(11) NOT NULL DEFAULT '0',
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_percentage` tinyint(1) NOT NULL DEFAULT '1',
  `products_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `orders_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `suppliers_id` int(11) DEFAULT NULL,
  `products_archive` tinyint(1) NOT NULL DEFAULT '0',
  `products_min_qty_order` int(4) NOT NULL DEFAULT '0',
  `products_price_comparison` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `products_dimension_width` decimal(5,2) NOT NULL DEFAULT '0.00',
  `products_dimension_height` decimal(5,2) NOT NULL DEFAULT '0.00',
  `products_dimension_depth` decimal(5,2) NOT NULL DEFAULT '0.00',
  `products_dimension_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_volume` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `products_quantity_unit_id` int(11) NOT NULL DEFAULT '0',
  `products_only_online` int(1) NOT NULL DEFAULT '0',
  `products_image_medium` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_weight_pounds` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `products_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `products_handling` decimal(15,2) NOT NULL DEFAULT '0.00',
  `products_wharehouse_time_replenishment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_wharehouse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_wharehouse_row` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_wharehouse_level_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_packaging` int(1) NOT NULL DEFAULT '0',
  `Products_sort_order` int(11) NOT NULL DEFAULT '0',
  `products_quantity_alert` int(4) NOT NULL DEFAULT '0',
  `products_only_shop` int(1) NOT NULL DEFAULT '0',
  `products_download_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `products_download_public` int(1) NOT NULL DEFAULT '0',
  `products_type` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_id`),
  KEY `idx_products_date_added` (`products_date_added`),
  KEY `idx_products_model` (`products_model`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_attributes` (
  `products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `options_id` int(11) NOT NULL DEFAULT '0',
  `options_values_id` int(11) NOT NULL DEFAULT '0',
  `options_values_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_sort_order` int(3) NOT NULL DEFAULT '1',
  `products_attributes_reference` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_attributes_id`),
  KEY `idx_products_attributes_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_attributes_download` (
  `products_attributes_id` int(11) NOT NULL DEFAULT '0',
  `products_attributes_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `products_attributes_maxdays` int(2) DEFAULT '0',
  `products_attributes_maxcount` int(2) DEFAULT '0',
  PRIMARY KEY (`products_attributes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_description` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_description` text COLLATE utf8_unicode_ci,
  `products_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_viewed` int(5) DEFAULT '0',
  `products_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_head_keywords_tag` text COLLATE utf8_unicode_ci,
  `products_head_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_shipping_delay` text COLLATE utf8_unicode_ci NOT NULL,
  `products_description_summary` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_id`,`language_id`),
  KEY `products_name` (`products_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_extra_fields` (
  `products_extra_fields_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_extra_fields_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_extra_fields_order` int(3) NOT NULL DEFAULT '0',
  `products_extra_fields_status` tinyint(1) NOT NULL DEFAULT '1',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_id` tinyint(11) NOT NULL,
  `products_extra_fields_type` int(1) NOT NULL,
  PRIMARY KEY (`products_extra_fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_featured` (
  `products_featured_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_featured_date_added` datetime DEFAULT NULL,
  `products_featured_last_modified` datetime DEFAULT NULL,
  `scheduled_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_featured_id`),
  KEY `idx_featured_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_groups` (
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_group_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `products_group_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `orders_group_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `products_quantity_unit_id_group` int(5) NOT NULL DEFAULT '0',
  `products_model_group` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_quantity_fixed_group` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_heart` (
  `products_heart_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_heart_date_added` datetime DEFAULT NULL,
  `products_heart_last_modified` datetime DEFAULT NULL,
  `scheduled_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_heart_id`),
  KEY `idx_specials_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_images_prodid` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_notifications` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`products_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_options` (
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_sort_order` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`products_options_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_options_values` (
  `products_options_values_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_values_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_options_values_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_options_values_to_products_options` (
  `products_options_values_to_products_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `products_options_values_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_options_values_to_products_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_quantity_unit` (
  `products_quantity_unit_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `products_quantity_unit_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_quantity_unit_id`,`language_id`),
  KEY `idx_products_quantity_unit_title` (`products_quantity_unit_title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(4, 1, 'douzaine');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(4, 2, 'dozen');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(2, 1, 'kg');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(2, 2, 'kg');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(3, 2, 'liter');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(3, 1, 'litre');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(5, 2, 'pcs');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(5, 1, 'pi&egrave;ce');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(1, 2, 'unit');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(1, 1, 'unit&eacute;');

CREATE TABLE IF NOT EXISTS `products_related` (
  `products_related_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_related_id_master` int(11) NOT NULL DEFAULT '0',
  `products_related_id_slave` int(11) NOT NULL DEFAULT '0',
  `products_related_sort_order` smallint(6) NOT NULL DEFAULT '0',
  `products_cross_sell` tinyint(1) NOT NULL DEFAULT '0',
  `products_related` tinyint(1) NOT NULL DEFAULT '0',
  `products_mode_b2b` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_to_categories` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_id`,`categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_to_products_extra_fields` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_extra_fields_id` int(11) NOT NULL DEFAULT '0',
  `products_extra_fields_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`products_id`,`products_extra_fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `reviews` (
  `reviews_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reviews_rating` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reviews_read` int(5) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`reviews_id`),
  KEY `idx_reviews_products_id` (`products_id`),
  KEY `idx_reviews_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `reviews_description` (
  `reviews_id` int(11) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `reviews_text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`reviews_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sec_directory_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(1, 'ClicShoppingAdmin/backups');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(2, 'ClicShoppingAdmin/images');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(3, 'ClicShoppingAdmin/images/graphs');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(4, 'sources');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(5, 'sources/images');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(6, 'pub');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(7, 'includes/work');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(8, 'cache');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(9, 'ClicCpanel/cache');

CREATE TABLE IF NOT EXISTS `sessions` (
  `sesskey` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` int(11) unsigned NOT NULL DEFAULT '0',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sesskey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `specials` (
  `specials_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `specials_new_products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `specials_date_added` datetime DEFAULT NULL,
  `specials_last_modified` datetime DEFAULT NULL,
  `scheduled_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `flash_discount` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`specials_id`),
  KEY `idx_specials_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `submit_description` (
  `submit_id` int(11) NOT NULL DEFAULT '1',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `submit_defaut_language_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_defaut_language_keywords` text COLLATE utf8_unicode_ci,
  `submit_defaut_language_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_defaut_language_footer` text COLLATE utf8_unicode_ci,
  `submit_language_products_info_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_products_info_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_products_info_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_products_new_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_products_new_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_products_new_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_special_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_special_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_special_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_reviews_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_reviews_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_reviews_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`submit_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `submit_description` (`submit_id`, `language_id`, `submit_defaut_language_title`, `submit_defaut_language_keywords`, `submit_defaut_language_description`, `submit_defaut_language_footer`, `submit_language_products_info_title`, `submit_language_products_info_keywords`, `submit_language_products_info_description`, `submit_language_products_new_title`, `submit_language_products_new_keywords`, `submit_language_products_new_description`, `submit_language_special_title`, `submit_language_special_keywords`, `submit_language_special_description`, `submit_language_reviews_title`, `submit_language_reviews_keywords`, `submit_language_reviews_description`) VALUES(1, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `submit_description` (`submit_id`, `language_id`, `submit_defaut_language_title`, `submit_defaut_language_keywords`, `submit_defaut_language_description`, `submit_defaut_language_footer`, `submit_language_products_info_title`, `submit_language_products_info_keywords`, `submit_language_products_info_description`, `submit_language_products_new_title`, `submit_language_products_new_keywords`, `submit_language_products_new_description`, `submit_language_special_title`, `submit_language_special_keywords`, `submit_language_special_description`, `submit_language_reviews_title`, `submit_language_reviews_keywords`, `submit_language_reviews_description`) VALUES(1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

CREATE TABLE IF NOT EXISTS `suppliers` (
  `suppliers_id` int(11) NOT NULL AUTO_INCREMENT,
  `suppliers_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `suppliers_manager` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_email_address` varchar(96) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_fax` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_suburb` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_postcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_city` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_states` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_country_id` int(11) NOT NULL DEFAULT '0',
  `suppliers_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`suppliers_id`),
  KEY `IDX_SUPPLIERS_NAME` (`suppliers_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `suppliers_info` (
  `suppliers_id` int(11) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `suppliers_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_clicked` int(5) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL,
  PRIMARY KEY (`suppliers_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `support_admin` (
  `support_admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `support_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_support` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `message_support` text COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`support_admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tax_class_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(1, 'TVA 20', 'TVA France 20', '2015-02-09 16:13:40', '2006-04-09 16:13:48');
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(2, 'TVA 5.5', 'TVA France 5.5', '2008-09-03 13:33:35', '2006-04-16 00:30:06');
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(3, 'Biens taxables Canada', 'Biens taxables Canada', NULL, '2008-09-16 15:02:29');
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(4, 'Taxe hamonisée Québec', 'Taxe hamonisée Québec', NULL, '2015-01-25 01:23:07');

CREATE TABLE IF NOT EXISTS `tax_rates` (
  `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_zone_id` int(11) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL DEFAULT '0',
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `tax_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `code_tax_odoo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tax_rates_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(1, 1, 1, 1, 20.0000, 'TVA 20%', '2015-02-09 16:14:24', '2006-04-09 16:13:48', '20.0');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(2, 1, 2, 0, 5.5000, 'TVA 5.5%', '2015-02-09 17:01:13', '2006-04-16 00:30:21', '5.5');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(3, 9, 3, 1, 9.9750, 'Zone TVH 9.975', '2015-02-09 16:16:14', '2008-09-16 15:04:49', 'TVQ');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(5, 7, 3, 1, 5.0000, 'Zone TPS 5%', '2015-02-09 16:16:55', '2008-09-16 15:05:48', 'TPS_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(6, 15, 4, 1, 14.9750, 'Taxe hamonisée Québec', '2015-02-09 17:00:14', '2015-01-25 01:25:11', 'TPSTVQ_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(7, 11, 3, 1, 13.0000, 'Zone TVH 13%', '2015-02-09 16:18:27', '2015-02-09 16:15:01', 'TVH13_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(8, 12, 3, 1, 14.0000, 'Zone fédérale 14%', '2015-02-09 16:18:38', '2015-02-09 16:17:18', 'TVH14_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(9, 8, 3, 1, 12.0000, 'Zone Fédérale 12%', '2015-02-09 16:18:46', '2015-02-09 16:17:47', 'TVH12_SALE');

CREATE TABLE IF NOT EXISTS `template_email` (
  `template_email_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_email_variable` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `customers_group_id` int(2) NOT NULL DEFAULT '0',
  `template_email_type` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_email_id`),
  KEY `customer_group_id` (`customers_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(1, 'TEMPLATE_EMAIL_WELCOME', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(2, 'TEMPLATE_EMAIL_TEXT_FOOTER', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(3, 'TEMPLATE_EMAIL_WELCOME_ADMIN', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(4, 'TEMPLATE_EMAIL_TEXT_COUPON', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(5, 'TEMPLATE_EMAIL_SIGNATURE', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(6, 'TEMPLATE_EMAIL_INTRO_COMMAND', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(7, 'TEMPLATE_EMAIL_NEWSLETTER_TEXT_FOOTER', 0, 0);

CREATE TABLE IF NOT EXISTS `template_email_description` (
  `template_email_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_email_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `template_email_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_email_short_description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `template_email_description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`template_email_description_id`),
  KEY `template_id` (`language_id`),
  KEY `template_email_id` (`template_email_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(1, 1, 1, 'Message de bienvenue catalogue', 'Catalogue  -Message de bienvenue lors de l''inscription du client', '<div align="justify">\r\n &nbsp;Nous vous remercions pour la confiance que vous nous t&eacute;moigniez en vous enregistrant comme nouveau client sur le site %%HTTP_CATALOG%%.</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;Comme suite &agrave; votre demande d&#39;ouverture de compte sur %%STORE_NAME%% nous vous confirmons sa validation. Vous pouvez d&egrave;s a pr&eacute;sent vous connecter sur %%STORE_NAME%%.</div>\r\n<br />\r\n<div align="justify">\r\n Nous sommes ravis de vous compter parmi nos nouveaux clients et nous restons &agrave; votre service pour vous faciliter vos achats !</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;&#39;<strong>Vos liens utiles :</strong>&#39;</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;1) Pour acc&eacute;der &agrave; votre compte client, modifier vos coordonn&eacute;es, vos adresses de livraison et/ou facturation, %%HTTP_CATALOG%%boutique/account.php</a></div>\r\n<br />\r\n<br />\r\n<div align="justify">\r\n 2) Pour acc&eacute;der directement au suivi de vos commandes, %%HTTP_CATALOG%%boutique/account_history.php</a></div>\r\n<div align="justify">\r\n 3) Retrouver son mot de passe oubli&eacute; : %%HTTP_CATALOG%%boutique/password_forgotten</a><br />\r\n &nbsp;</div>\r\n<div align="justify">\r\n Pour tous les acc&egrave;s pr&eacute;cit&eacute;s, vous devrez renseigner au pr&eacute;alable vos identifiants.</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n Pour toute aide sur nos services, n&#39;h&eacute;sitez pas &agrave; contacter notre service client&egrave;le :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(2, 1, 2, 'Welcome message on catalogue', 'Catalog - welcome message for a new customer', '<div align="justify">\r\n &nbsp;Thank you for the trust you place in us by registering as a new customer on site %%HTTP_CATALOG%%.</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;Following your request to open an account on %%STORE_NAME%% we confirm its validity. You can now log on %%STORE_NAME%%.</div>\r\n<br />\r\n<div align="justify">\r\n We are delighted to count you among our new clients and we remain at your service to make shopping easy! !</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;&#39;<strong>Your links :</strong>&#39;</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;1) To access your account, change your address, your delivery address and / or billing,%%HTTP_CATALOG%%boutique/account.php</div>\r\n<br />\r\n<br />\r\n<div align="justify">\r\n 2) To go directly to track your orders, %%HTTP_CATALOG%%boutique/account_history.php</div>\r\n<div align="justify">\r\n 3) Recover lost password : %%HTTP_CATALOG%%/boutique/password_forgotten</a><br />\r\n &nbsp;</div>\r\n<div align="justify">\r\n To access all the above, you should check before your login..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n For assistance on our services, please contact our customer service: %%STORE_OWNER_EMAIL_ADDRESS%% :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(3, 2, 1, 'Avis de confidentialité', 'Législation : pied de page pour tous les emails envoyés sauf newsletter', '<p>\r\n &nbsp;<u>Avis de confidentialit&eacute; :</u><br />\r\n Ce message ainsi que les documents qui seraient joints en annexe sont adress&eacute;s exclusivement &agrave; leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la l&eacute;gislation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans &ecirc;tre le destinataire ou une personne autoris&eacute;e, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re&ccedil;u ce message, nous vous prions de bien vouloir nous le communiquer par courriel :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%% et de proc&eacute;der directement &agrave; sa destruction.</p>\r\n<p>\r\n Conform&eacute;ment &agrave; la Loi dans le pays de r&eacute;sidence de la soci&eacute;t&eacute; exploitant la boutique %%STORE_NAME%%, vous avez droit &agrave; la rectification de vos donn&eacute;es personnelles &agrave; tout moment ou sur simple demande par email. %%STORE_OWNER_EMAIL_ADDRESS%% </p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(4, 2, 2, 'Confidentialities', 'Regulation : footer for all email sending except newsletter', 'This e-mail and any files transmitted with it are confidential and intended solely for the use of the individual to whom it is addressed. If you have received this email in error please send it back to the person that sent it to you. Any views or opinions presented are solely those of its author and do not necessarily represent those of %%STORE_NAME%% or any of its subsidiary companies. Unauthorized publication, use, dissemination, forwarding, printing or copying of this email and its associated attachments is strictly prohibited.<br />\r\n<br />\r\n.Pursuant to the Act in the country of residence of the company operating the store %%STORE_NAME%%, you are entitled to the correction of your personal data at any time or upon request by email  at %%STORE_OWNER_EMAIL_ADDRESS%% .</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(5, 3, 1, 'Création du compte client', 'Admin : Message de bienvenue lors de la création du comte client', '<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n Nous venons de vous cr&eacute;er un compte suite &agrave; une commande de votre part (par t&eacute;l&eacute;phone ou autre) sur le site %%STORE_NAME%%.</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n En vous connectant sur %%STORE_NAME%% (apr&egrave;s avoir r&eacute;cup&eacute;r&eacute; votre mot de passe), nous pourrez b&eacute;n&eacute;ficier d&#39;un ensemble de services.</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n Nous sommes ravis de vous compter parmi nos nouveaux clients et nous restons &agrave; votre service pour vous faciliter vos achats !</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n <strong>Vos liens utiles :</strong></div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div>\r\n 1) Pour acc&eacute;der &agrave; votre compte client, modifier vos coordonn&eacute;es, vos adresses de livraison et/ou facturation : %%HTTP_CATALOG%%account.php<br />\r\n </a></div>\r\n<div>\r\n 2) Pour acc&eacute;der directement au suivi de vos commandes : %%HTTP_CATALOG%%account_history.php</div>\r\n<div>\r\n 3) Retrouver son mot de passe oubli&eacute; : %%HTTP_CATALOG%%password_forgotten.php<br />\r\n 4) Pour contacter notre service apr&egrave;s vente concernant une commande, veuillez &eacute;diter votre commande dans votre espace et cliquer sur support.</div>\r\n<br />\r\n<div align="justify">\r\n Pour tous les acc&egrave;s pr&eacute;cit&eacute;s, vous devrez renseigner au pr&eacute;alable vos identifiants.</div>\r\n<div align="justify">\r\n Pour toute aide sur nos services, n&#39;h&eacute;sitez pas &agrave; contacter notre service client&egrave;le :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(6, 3, 2, 'Customer create account', 'Admin : Welcome message - Customer create account', '<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n We just create an account following an order by you (by phone or otherwise) on site%%STORE_NAME%%..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n By logging on %%STORE_NAME%% (after retrieving your password), we can enjoy a range of services..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\nWe are delighted to count you among our new clients and we remain at your service to make shopping easy! !</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n <strong>Your links :</strong>&#39;</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;1) To access your account, change your address, your delivery address and / or billing,%%HTTP_CATALOG%%boutique/account.php</a></div>\r\n<br />\r\n<br />\r\n<div align="justify">\r\n 2) To go directly to track your orders, %%HTTP_CATALOG%%boutique/account_history.php</a></div>\r\n<div align="justify">\r\n 3) Recover lost password : %%HTTP_CATALOG%%boutique/password_forgotten</a><br />\r\n &nbsp;</div>\r\n<div align="justify">\r\n To access all the above, you should check before your login..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n For assistance on our services, please contact our customer service: %%STORE_OWNER_EMAIL_ADDRESS%% :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(7, 4, 1, 'Coupon Client', 'Offrir un coupon lors de la création d''un compte client', '<p>\r\n %%STORE_NAME%% se fait un plaisir de vous offrir un coupon remise sur votre prochaine commande que vous pourrez utiliser n&#39;importe quand sur la boutique. Pour connaitre les modalit&eacute;s d&#39;application du coupon, veuillez consulter notre aide en ligne.</p>\r\n<p>\r\n Le num&eacute;ro du coupon est :</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(8, 4, 2, 'Customer coupon', 'Give a coupon during the creation a customer account', '<p>\r\n %%STORE_NAME%% is pleased to offer you a discount on your next order coupon that you can use anytime Store. To know the rules for applying the coupon, please visit our online help. .</p>\r\n<p>\r\n The coupon number is: :</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(9, 5, 1, 'Signature', 'Qui sera associé au bas d''un mail envoyé : signature', '<p>\r\n ---------------------</p>\r\n<p>\r\n Cordialement,</p>\r\n<p>\r\n L&#39;&eacute;quipe %%STORE_NAME%%</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(10, 5, 2, 'Signature', 'Signature at the bottom of the message', '<p>\r\n ---------------------</p>\r\n<p>\r\n Regards,</p>\r\n<p>\r\n %%STORE_NAME%% team</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(11, 6, 1, 'Statut commande', 'Mail concernant le statut commande', 'Email envoy&eacute; suite &agrave; une MAJ d&#39;un statut<br />\r\n<p>\r\n &nbsp;Bonjour,<br />\r\n <br />\r\n &nbsp;Le statut de votre commande a &eacute;t&eacute; mis &agrave; jour.<br />\r\n <br />\r\n &nbsp;Pour toutes les demandes suivantes, veuillez vous connecter &agrave; votre espace d&#39;administration.<br />\r\n <br />\r\n &nbsp;- Consulter vos commandes ou votre historique de commande : %%HTTP_CATALOG%%account_history.php<br />\r\n &nbsp;- Nous envoyer un message concernant cette commande (&eacute;diter votre commande et cliquer contacter notre support)<br />\r\n &nbsp;- Imprimer, t&eacute;l&eacute;charger une commande, une facture (&eacute;diter votre commande et cliquer sur l&#39;icone PDF facture)<br />\r\n &nbsp;</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(12, 6, 2, 'Order status', 'Order status email', 'Email sent after an update status<br />\r\n<p>\r\n &nbsp;Hello,<br />\r\n <br />\r\n &nbsp;The status of your order has been updated..<br />\r\n <br />\r\n &nbsp;For all subsequent requests, please log into your administration area..<br />\r\n <br />\r\n &nbsp;- View your orders and your order history : %%HTTP_CATALOG%%account_history.php<br />\r\n &nbsp;- We send a message regarding this order (click edit your order and contact our support)<br />\r\n &nbsp;- Print, download an order, an invoice (edit your order and click on the PDF icon invoice)<br />\r\n &nbsp;</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(13, 7, 1, 'Législation newsletter', 'text bas de page législation newsletter', '<style="font-weight="6">Conform&eacute;ment aux <strong>Lois</strong> en vigueur dans le pays de la boutique %%STORE_NAME%%, vous avez droit &agrave; la rectification de vos donn&eacute;es personnelles &agrave; tout moment ou sur simple demande par email.<br />\r\nPour se d&eacute;sabonner de notre Newsletter, veuillez recopier&nbsp; sur le lien suivant : %%HTTP_CATALOG%%account_newsletters.php</font>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(14, 7, 2, 'Newsletter regulation', 'Regulation footer text', '<style="font-weight="6">In accordance with the <strong>Law of the </strong>%%STORE_NAME%%, you are entitled to correction of your personal data or on request by email.<br />\r\nTo unsubscribe from our newsletter, send us an email or just click on the following link: %%HTTP_CATALOG%%account_newsletters.php</font>');

CREATE TABLE IF NOT EXISTS `usu_cache` (
  `cache_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `cache_data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cache_date` datetime NOT NULL,
  PRIMARY KEY (`cache_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `whos_online` (
  `customer_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_entry` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `time_last_click` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `last_page_url` text COLLATE utf8_unicode_ci NOT NULL,
  `http_referer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL DEFAULT '0',
  `zone_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `idx_zones_to_geo_zones_country_id` (`zone_country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=642 ;

INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(1, 223, 'AL', 'Alabama');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(2, 223, 'AK', 'Alaska');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(3, 223, 'AS', 'American Samoa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(4, 223, 'AZ', 'Arizona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(5, 223, 'AR', 'Arkansas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(6, 223, 'AF', 'Armed Forces Africa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(7, 223, 'AA', 'Armed Forces Americas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(8, 223, 'AC', 'Armed Forces Canada');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(9, 223, 'AE', 'Armed Forces Europe');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(10, 223, 'AM', 'Armed Forces Middle East');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(11, 223, 'AP', 'Armed Forces Pacific');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(12, 223, 'CA', 'California');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(13, 223, 'CO', 'Colorado');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(14, 223, 'CT', 'Connecticut');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(15, 223, 'DE', 'Delaware');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(16, 223, 'DC', 'District of Columbia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(17, 223, 'FM', 'Federated States Of Micronesia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(18, 223, 'FL', 'Florida');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(19, 223, 'GA', 'Georgia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(20, 223, 'GU', 'Guam');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(21, 223, 'HI', 'Hawaii');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(22, 223, 'ID', 'Idaho');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(23, 223, 'IL', 'Illinois');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(24, 223, 'IN', 'Indiana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(25, 223, 'IA', 'Iowa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(26, 223, 'KS', 'Kansas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(27, 223, 'KY', 'Kentucky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(28, 223, 'LA', 'Louisiana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(29, 223, 'ME', 'Maine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(30, 223, 'MH', 'Marshall Islands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(31, 223, 'MD', 'Maryland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(32, 223, 'MA', 'Massachusetts');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(33, 223, 'MI', 'Michigan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(34, 223, 'MN', 'Minnesota');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(35, 223, 'MS', 'Mississippi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(36, 223, 'MO', 'Missouri');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(37, 223, 'MT', 'Montana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(38, 223, 'NE', 'Nebraska');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(39, 223, 'NV', 'Nevada');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(40, 223, 'NH', 'New Hampshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(41, 223, 'NJ', 'New Jersey');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(42, 223, 'NM', 'New Mexico');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(43, 223, 'NY', 'New York');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(44, 223, 'NC', 'North Carolina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(45, 223, 'ND', 'North Dakota');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(46, 223, 'MP', 'Northern Mariana Islands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(47, 223, 'OH', 'Ohio');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(48, 223, 'OK', 'Oklahoma');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(49, 223, 'OR', 'Oregon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(50, 223, 'PW', 'Palau');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(51, 223, 'PA', 'Pennsylvania');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(52, 223, 'PR', 'Puerto Rico');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(53, 223, 'RI', 'Rhode Island');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(54, 223, 'SC', 'South Carolina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(55, 223, 'SD', 'South Dakota');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(56, 223, 'TN', 'Tennessee');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(57, 223, 'TX', 'Texas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(58, 223, 'UT', 'Utah');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(59, 223, 'VT', 'Vermont');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(60, 223, 'VI', 'Virgin Islands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(61, 223, 'VA', 'Virginia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(62, 223, 'WA', 'Washington');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(63, 223, 'WV', 'West Virginia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(64, 223, 'WI', 'Wisconsin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(65, 223, 'WY', 'Wyoming');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(66, 38, 'AB', 'Alberta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(67, 38, 'BC', 'British Columbia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(68, 38, 'MB', 'Manitoba');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(69, 38, 'NF', 'Newfoundland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(70, 38, 'NB', 'New Brunswick');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(71, 38, 'NS', 'Nova Scotia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(72, 38, 'NT', 'Northwest Territories');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(73, 38, 'NU', 'Nunavut');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(74, 38, 'ON', 'Ontario');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(75, 38, 'PE', 'Prince Edward Island');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(76, 38, 'QC', 'Quebec');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(77, 38, 'SK', 'Saskatchewan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(78, 38, 'YT', 'Yukon Territory');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(79, 81, 'NDS', 'Niedersachsen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(80, 81, 'BAW', 'Baden-W&uuml;rttemberg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(81, 81, 'BAY', 'Bayern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(82, 81, 'BER', 'Berlin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(83, 81, 'BRG', 'Brandenburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(84, 81, 'BRE', 'Bremen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(85, 81, 'HAM', 'Hamburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(86, 81, 'HES', 'Hessen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(87, 81, 'MEC', 'Mecklenburg-Vorpommern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(88, 81, 'NRW', 'Nordrhein-Westfalen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(89, 81, 'RHE', 'Rheinland-Pfalz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(90, 81, 'SAR', 'Saarland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(91, 81, 'SAS', 'Sachsen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(92, 81, 'SAC', 'Sachsen-Anhalt');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(93, 81, 'SCN', 'Schleswig-Holstein');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(94, 81, 'THE', 'Th&uuml;ringen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(95, 14, 'WI', 'Wien');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(96, 14, 'NO', 'Nieder&ouml;sterreich');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(97, 14, 'OO', 'Ober&ouml;sterreich');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(98, 14, 'SB', 'Salzburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(99, 14, 'KN', 'K&auml;rnten');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(100, 14, 'ST', 'Steiermark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(101, 14, 'TI', 'Tirol');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(102, 14, 'BL', 'Burgenland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(103, 14, 'VB', 'Voralberg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(104, 204, 'AG', 'Aargau');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(105, 204, 'AI', 'Appenzell Innerrhoden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(106, 204, 'AR', 'Appenzell Ausserrhoden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(107, 204, 'BE', 'Bern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(108, 204, 'BL', 'Basel-Landschaft');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(109, 204, 'BS', 'Basel-Stadt');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(110, 204, 'FR', 'Freiburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(111, 204, 'GE', 'Genf');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(112, 204, 'GL', 'Glarus');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(113, 204, 'JU', 'Graub&uuml;nden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(114, 204, 'JU', 'Jura');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(115, 204, 'LU', 'Luzern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(116, 204, 'NE', 'Neuenburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(117, 204, 'NW', 'Nidwalden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(118, 204, 'OW', 'Obwalden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(119, 204, 'SG', 'St. Gallen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(120, 204, 'SH', 'Schaffhausen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(121, 204, 'SO', 'Solothurn');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(122, 204, 'SZ', 'Schwyz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(123, 204, 'TG', 'Thurgau');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(124, 204, 'TI', 'Tessin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(125, 204, 'UR', 'Uri');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(126, 204, 'VD', 'Waadt');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(127, 204, 'VS', 'Wallis');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(128, 204, 'ZG', 'Zug');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(129, 204, 'ZH', 'Z&uuml;rich');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(130, 195, 'A Coruña', 'A Coruña');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(131, 195, 'Alava', 'Alava');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(132, 195, 'Albacete', 'Albacete');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(133, 195, 'Alicante', 'Alicante');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(134, 195, 'Almeria', 'Almeria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(135, 195, 'Asturias', 'Asturias');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(136, 195, 'Avila', 'Avila');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(137, 195, 'Badajoz', 'Badajoz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(138, 195, 'Baleares', 'Baleares');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(139, 195, 'Barcelona', 'Barcelona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(140, 195, 'Burgos', 'Burgos');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(141, 195, 'Caceres', 'Caceres');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(142, 195, 'Cadiz', 'Cadiz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(143, 195, 'Cantabria', 'Cantabria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(144, 195, 'Castellon', 'Castellon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(145, 195, 'Ceuta', 'Ceuta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(146, 195, 'Ciudad Real', 'Ciudad Real');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(147, 195, 'Cordoba', 'Cordoba');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(148, 195, 'Cuenca', 'Cuenca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(149, 195, 'Girona', 'Girona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(150, 195, 'Granada', 'Granada');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(151, 195, 'Guadalajara', 'Guadalajara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(152, 195, 'Guipuzcoa', 'Guipuzcoa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(153, 195, 'Huelva', 'Huelva');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(154, 195, 'Huesca', 'Huesca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(155, 195, 'Jaen', 'Jaen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(156, 195, 'La Rioja', 'La Rioja');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(157, 195, 'Las Palmas', 'Las Palmas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(158, 195, 'Leon', 'Leon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(159, 195, 'Lleida', 'Lleida');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(160, 195, 'Lugo', 'Lugo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(161, 195, 'Madrid', 'Madrid');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(162, 195, 'Malaga', 'Malaga');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(163, 195, 'Melilla', 'Melilla');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(164, 195, 'Murcia', 'Murcia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(165, 195, 'Navarra', 'Navarra');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(166, 195, 'Ourense', 'Ourense');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(167, 195, 'Palencia', 'Palencia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(168, 195, 'Pontevedra', 'Pontevedra');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(169, 195, 'Salamanca', 'Salamanca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(170, 195, 'Santa Cruz de Tenerife', 'Santa Cruz de Tenerife');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(171, 195, 'Segovia', 'Segovia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(172, 195, 'Sevilla', 'Sevilla');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(173, 195, 'Soria', 'Soria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(174, 195, 'Tarragona', 'Tarragona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(175, 195, 'Teruel', 'Teruel');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(176, 195, 'Toledo', 'Toledo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(177, 195, 'Valencia', 'Valencia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(178, 195, 'Valladolid', 'Valladolid');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(179, 195, 'Vizcaya', 'Vizcaya');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(180, 195, 'Zamora', 'Zamora');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(181, 73, 'Et', 'Etranger');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(182, 73, '01', 'Ain');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(183, 73, '02', 'Aisne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(184, 73, '03', 'Allier');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(185, 73, '04', 'Alpes de Haute Provence');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(186, 73, '05', 'Hautes-Alpes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(187, 73, '06', 'Alpes Maritimes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(188, 73, '07', 'Ard&egrave;che');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(189, 73, '08', 'Ardennes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(190, 73, '09', 'Ari&egrave;ge');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(191, 73, '10', 'Aube');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(192, 73, '11', 'Aude');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(193, 73, '12', 'Aveyron');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(194, 73, '13', 'Bouches du Rh&ocirc;ne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(195, 73, '14', 'Calvados');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(196, 73, '15', 'Cantal');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(197, 73, '16', 'Charente');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(198, 73, '17', 'Charente Maritime');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(199, 73, '18', 'Cher');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(200, 73, '19', 'Corr&egrave;ze');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(201, 73, '2A', 'Corse du Sud');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(202, 73, '2B', 'Haute Corse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(203, 73, '21', 'C&ocirc;te d''or');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(204, 73, '22', 'C&ocirc;tes d''Armor');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(205, 73, '23', 'Creuse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(206, 73, '24', 'Dordogne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(207, 73, '25', 'Doubs');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(208, 73, '26', 'Dr&ocirc;me');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(209, 73, '27', 'Eure');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(210, 73, '28', 'Eure et Loir');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(211, 73, '29', 'Finist&egrave;re');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(212, 73, '30', 'Gard');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(213, 73, '31', 'Haute Garonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(214, 73, '32', 'Gers');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(215, 73, '33', 'Gironde');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(216, 73, '34', 'H&eacute;rault');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(217, 73, '35', 'Ille et Vilaine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(218, 73, '36', 'Indre');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(219, 73, '37', 'Indre et Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(220, 73, '38', 'Is&egrave;re');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(221, 73, '39', 'Jura');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(222, 73, '40', 'Landes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(223, 73, '41', 'Loir et Cher');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(224, 73, '42', 'Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(225, 73, '43', 'Haute Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(226, 73, '44', 'Loire Atlantique');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(227, 73, '45', 'Loiret');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(228, 73, '46', 'Lot');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(229, 73, '47', 'Lot et Garonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(230, 73, '48', 'Loz&egrave;re');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(231, 73, '49', 'Maine et Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(232, 73, '50', 'Manche');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(233, 73, '51', 'Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(234, 73, '52', 'Haute Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(235, 73, '53', 'Mayenne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(236, 73, '54', 'Meurthe et Moselle');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(237, 73, '55', 'Meuse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(238, 73, '56', 'Morbihan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(239, 73, '57', 'Moselle');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(240, 73, '58', 'Ni&egrave;vre');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(241, 73, '59', 'Nord');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(242, 73, '60', 'Oise');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(243, 73, '61', 'Orne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(244, 73, '62', 'Pas de Calais');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(245, 73, '63', 'Puy de D&ocirc;me');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(246, 73, '64', 'Pyr&eacute;n&eacute;es Atlantiques');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(247, 73, '65', 'Hautes Pyr&eacute;n&eacute;es');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(248, 73, '66', 'Pyr&eacute;n&eacute;es Orientales');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(249, 73, '67', 'Bas Rhin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(250, 73, '68', 'Haut Rhin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(251, 73, '69', 'Rhone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(252, 73, '70', 'Haute Sa&ocirc;ne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(253, 73, '71', 'Sa&ocirc;ne et Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(254, 73, '72', 'Sarthe');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(255, 73, '73', 'Savoie');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(256, 73, '74', 'Haute Savoie');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(257, 73, '75', 'Paris');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(258, 73, '76', 'Seine Maritime');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(259, 73, '77', 'Seine et Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(260, 73, '78', 'Yvelines');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(261, 73, '79', 'Deux S&egrave;vres');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(262, 73, '80', 'Somme');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(263, 73, '81', 'Tarn');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(264, 73, '82', 'Tarn et Garonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(265, 73, '83', 'Var');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(266, 73, '84', 'Vaucluse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(267, 73, '85', 'Vend&eacute;e');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(268, 73, '86', 'Vienne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(269, 73, '87', 'Haute Vienne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(270, 73, '88', 'Vosges');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(271, 73, '89', 'Yonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(272, 73, '90', 'Territoire de Belfort');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(273, 73, '91', 'Essonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(274, 73, '92', 'Hauts de Seine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(275, 73, '93', 'Seine St-Denis');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(276, 73, '94', 'Val de Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(277, 73, '95', 'Val d''Oise');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(278, 73, '971 (DOM)', 'Guadeloupe');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(279, 73, '972 (DOM)', 'Martinique');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(280, 73, '973 (DOM)', 'Guyane');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(281, 73, '974 (DOM)', 'Saint Denis');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(282, 73, '975 (DOM)', 'St-Pierre de Miquelon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(283, 73, '976 (TOM)', 'Mayotte');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(284, 73, '984 (TOM)', 'Terres australes et Antartiques ');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(285, 73, '985 (TOM)', 'Nouvelle Cal&eacute;donie');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(286, 73, '986 (TOM)', 'Wallis et Futuna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(287, 73, '987 (TOM)', 'Polyn&eacute;sie fran&ccedil;aise');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(288, 21, 'Anvers', 'Anvers');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(289, 21, 'Limbourg', 'Limbourg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(290, 21, 'Hainaut', 'Hainaut');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(291, 21, 'Brabant Flamand', 'Brabant Flamand');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(292, 21, 'Brabant Wallon', 'Brabant Wallon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(293, 21, 'Liege', 'Liege');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(294, 21, 'Namur', 'Namur');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(295, 21, 'Luxembourg', 'Luxembourg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(296, 21, 'Flandre Occidental', 'Namen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(297, 21, 'Flandre Orientale', 'Flandre Orientale');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(298, 105, 'AG', 'Agrigento');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(299, 105, 'AL', 'Alessandria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(300, 105, 'AN', 'Ancona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(301, 105, 'AO', 'Aosta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(302, 105, 'AR', 'Arezzo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(303, 105, 'AP', 'Ascoli Piceno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(304, 105, 'AT', 'Asti');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(305, 105, 'AV', 'Avellino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(306, 105, 'BA', 'Bari');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(307, 105, 'BL', 'Belluno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(308, 105, 'BN', 'Benevento');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(309, 105, 'BG', 'Bergamo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(310, 105, 'BI', 'Biella');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(311, 105, 'BO', 'Bologna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(312, 105, 'BZ', 'Bolzano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(313, 105, 'BS', 'Brescia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(314, 105, 'BR', 'Brindisi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(315, 105, 'CA', 'Cagliari');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(316, 105, 'CL', 'Caltanissetta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(317, 105, 'CB', 'Campobasso');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(318, 105, 'CE', 'Caserta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(319, 105, 'CT', 'Catania');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(320, 105, 'CZ', 'Catanzaro');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(321, 105, 'CH', 'Chieti');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(322, 105, 'CO', 'Como');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(323, 105, 'CS', 'Cosenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(324, 105, 'CR', 'Cremona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(325, 105, 'KR', 'Crotone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(326, 105, 'CN', 'Cuneo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(327, 105, 'EN', 'Enna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(328, 105, 'FE', 'Ferrara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(329, 105, 'FI', 'Firenze');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(330, 105, 'FG', 'Foggia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(331, 105, 'FO', 'Forl�');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(332, 105, 'FR', 'Frosinone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(333, 105, 'GE', 'Genova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(334, 105, 'GO', 'Gorizia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(335, 105, 'GR', 'Grosseto');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(336, 105, 'IM', 'Imperia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(337, 105, 'IS', 'Isernia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(338, 105, 'AQ', 'Aquila');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(339, 105, 'SP', 'La Spezia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(340, 105, 'LT', 'Latina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(341, 105, 'LE', 'Lecce');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(342, 105, 'LC', 'Lecco');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(343, 105, 'LI', 'Livorno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(344, 105, 'LO', 'Lodi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(345, 105, 'LU', 'Lucca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(346, 105, 'MC', 'Macerata');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(347, 105, 'MN', 'Mantova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(348, 105, 'MS', 'Massa-Carrara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(349, 105, 'MT', 'Matera');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(350, 105, 'ME', 'Messina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(351, 105, 'MI', 'Milano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(352, 105, 'MO', 'Modena');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(353, 105, 'NA', 'Napoli');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(354, 105, 'NO', 'Novara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(355, 105, 'NU', 'Nuoro');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(356, 105, 'OR', 'Oristano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(357, 105, 'PD', 'Padova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(358, 105, 'PA', 'Palermo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(359, 105, 'PR', 'Parma');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(360, 105, 'PG', 'Perugia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(361, 105, 'PV', 'Pavia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(362, 105, 'PS', 'Pesaro e Urbino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(363, 105, 'PE', 'Pescara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(364, 105, 'PC', 'Piacenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(365, 105, 'PI', 'Pisa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(366, 105, 'PT', 'Pistoia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(367, 105, 'PN', 'Pordenone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(368, 105, 'PZ', 'Potenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(369, 105, 'PO', 'Prato');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(370, 105, 'RG', 'Ragusa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(371, 105, 'RA', 'Ravenna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(372, 105, 'RC', 'Reggio di Calabria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(373, 105, 'RE', 'Reggio Emilia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(374, 105, 'RI', 'Rieti');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(375, 105, 'RN', 'Rimini');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(376, 105, 'RM', 'Roma');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(377, 105, 'RO', 'Rovigo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(378, 105, 'SA', 'Salerno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(379, 105, 'SS', 'Sassari');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(380, 105, 'SV', 'Savona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(381, 105, 'SI', 'Siena');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(382, 105, 'SR', 'Siracusa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(383, 105, 'SO', 'Sondrio');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(384, 105, 'TA', 'Taranto');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(385, 105, 'TE', 'Teramo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(386, 105, 'TR', 'Terni');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(387, 105, 'TO', 'Torino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(388, 105, 'TP', 'Trapani');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(389, 105, 'TN', 'Trento');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(390, 105, 'TV', 'Treviso');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(391, 105, 'TS', 'Trieste');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(392, 105, 'UD', 'Udine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(393, 105, 'VA', 'Varese');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(394, 105, 'VE', 'Venezia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(395, 105, 'VB', 'Verbania');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(396, 105, 'VC', 'Vercelli');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(397, 105, 'VR', 'Verona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(398, 105, 'VV', 'Vibo Valentia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(399, 105, 'VI', 'Vicenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(400, 105, 'VT', 'Viterbo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(401, 182, 'RSM', 'Acquaviva');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(402, 182, 'RSM', 'Borgo Maggiore');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(403, 182, 'RSM', 'Ca''Rigo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(404, 182, 'RSM', 'Cailungo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(405, 182, 'RSM', 'Casole');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(406, 182, 'RSM', 'Cerbaiola');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(407, 182, 'RSM', 'Chiesanuova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(408, 182, 'RSM', 'Dogana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(409, 182, 'RSM', 'Domagnano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(410, 182, 'RSM', 'Faetano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(411, 182, 'RSM', 'Falciano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(412, 182, 'RSM', 'Fiorentino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(413, 182, 'RSM', 'Fiorina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(414, 182, 'RSM', 'Galazzano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(415, 182, 'RSM', 'Gualdicciolo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(416, 182, 'RSM', 'Montegiardino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(417, 182, 'RSM', 'Murata');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(418, 182, 'RSM', 'Rovereta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(419, 182, 'RSM', 'S.Giovanni');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(420, 182, 'RSM', 'S.ta Mustiola');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(421, 182, 'RSM', 'SanMarino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(422, 182, 'RSM', 'Serravalle');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(423, 182, 'RSM', 'Teglio');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(424, 182, 'RSM', 'Torraccia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(425, 182, 'RSM', 'Valdragone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(426, 182, 'RSM', 'Ventoso');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(427, 97, 'BUD', 'Budapest');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(428, 97, 'BAR', 'Baranya');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(429, 97, 'BKK', 'Bacs-Kiskun');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(430, 97, 'BÉK', 'B&eacute;k&eacute;s');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(431, 97, 'BAZ', 'Borsod-Abaúj-Zempl&eacute;n');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(432, 97, 'CSO', 'Csongrad');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(433, 97, 'FEJ', 'Fej&eacute;r');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(434, 97, 'GYS', 'Gyõr-Sopron');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(435, 97, 'HAB', 'Hajdú-Bihar');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(436, 97, 'HEV', 'Heves');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(437, 97, 'JNS', 'Jasz-Nagykun-Szolnok');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(438, 97, 'KOE', 'Komarom-Esztergom');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(439, 97, 'NOG', 'Nógrad');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(440, 97, 'PES', 'Pest');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(441, 97, 'SOM', 'Somogy');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(442, 97, 'SSB', 'Szabolcs-Szatmar-Bereg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(443, 97, 'TOL', 'Tolna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(444, 97, 'VAS', 'Vas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(445, 97, 'VES', 'Veszpr&eacute;m');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(446, 97, 'ZAL', 'Zala');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(447, 103, 'CAR', 'Carlow');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(448, 103, 'CAV', 'Cavan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(449, 103, 'CLA', 'Clare');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(450, 103, 'COR', 'Cork');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(451, 103, 'DON', 'Donegal');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(452, 103, 'DUB', 'Dublin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(453, 103, 'GAL', 'Galway');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(454, 103, 'KER', 'Kerry');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(455, 103, 'KID', 'Kildare');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(456, 103, 'KIK', 'Kilkenny');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(457, 103, 'LET', 'Leitrim');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(458, 103, 'LEX', 'Leix');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(459, 103, 'LIM', 'Limerick');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(460, 103, 'LOG', 'Longford');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(461, 103, 'LOU', 'Louth');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(462, 103, 'MAY', 'Mayo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(463, 103, 'MEA', 'Meath');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(464, 103, 'MOG', 'Monaghan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(465, 103, 'OFF', 'Offaly');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(466, 103, 'ROS', 'Roscommon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(467, 103, 'SLI', 'Sligo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(468, 103, 'TIP', 'Tipperary');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(469, 103, 'WAT', 'Waterford');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(470, 103, 'WEM', 'Westmeath');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(471, 103, 'WEX', 'Wexford');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(472, 103, 'WIC', 'Wicklow');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(473, 56, 'B', 'Jihomoravsky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(474, 56, 'C', 'Jiho&egrave;esky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(475, 56, 'J', 'Vyso&egrave;ina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(476, 56, 'K', 'Karlovarsky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(477, 56, 'H', 'Kralov&eacute;hradecky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(478, 56, 'L', 'Liberecky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(479, 56, 'M', 'Olomoucky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(480, 56, 'P', 'Plzeosky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(481, 56, 'A', 'Hlavní mìsto Praha');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(482, 56, 'S', 'Støedo&egrave;esky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(483, 56, 'U', 'Ustecky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(484, 56, 'Z', 'Zlínsky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(485, 56, 'T', 'Moravskoslezsky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(486, 56, 'E', 'Pardubicky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(487, 160, 'OSL', 'Oslo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(488, 160, 'AKE', 'Akershus');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(489, 160, 'AUA', 'Aust-Agder');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(490, 160, 'BUS', 'Buskerud');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(491, 160, 'FIN', 'Finnmark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(492, 160, 'HED', 'Hedmark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(493, 160, 'HOR', 'Hordaland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(494, 160, 'MOR', 'Møre og Romsdal');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(495, 160, 'NOR', 'Nordland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(496, 160, 'NTR', 'Nord-Trøndelag');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(497, 160, 'OPP', 'Oppland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(498, 160, 'ROG', 'Rogaland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(499, 160, 'SOF', 'Sogn og Fjordane');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(500, 160, 'STR', 'Sør-Trøndelag');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(501, 160, 'TEL', 'Telemark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(502, 160, 'TRO', 'Troms');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(503, 160, 'VEA', 'Vest-Agder');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(504, 160, 'OST', 'Ostfold');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(505, 160, 'SVA', 'Svalbard');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(506, 203, 'AB', 'Stockholm');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(507, 203, 'C', 'Uppsala');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(508, 203, 'D', 'S&ouml;dermanland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(509, 203, 'E', '&Ouml;sterg&ouml;tland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(510, 203, 'F', 'J&ouml;nk&ouml;ping');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(511, 203, 'G', 'Kronoberg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(512, 203, 'H', 'Kalmar');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(513, 203, 'I', 'Gotland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(514, 203, 'K', 'Blekinge');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(515, 203, 'M', 'Skane');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(516, 203, 'N', 'Halland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(517, 203, 'O', 'V&auml;stra G&ouml;taland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(518, 203, 'S', 'V&auml;rmland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(519, 203, 'T', '&Ouml;rebro');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(520, 203, 'U', 'V&auml;stmanland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(521, 203, 'W', 'Dalarna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(522, 203, 'X', 'G&auml;vleborg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(523, 203, 'Y', 'V&auml;sternorrland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(524, 203, 'Z', 'J&auml;mtland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(525, 203, 'AC', 'V&auml;sterbotten');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(526, 203, 'BD', 'Norrbotten');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(527, 222, 'Avon', 'England - Avon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(528, 222, 'Bath and Northeast Somerset', 'England - Bath and Northeast Som');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(529, 222, 'Bedfordshire', 'England - Bedfordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(530, 222, 'Bristol', 'England - Bristol');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(531, 222, 'Buckinghamshire', 'England - Buckinghamshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(532, 222, 'Cambridgeshire', 'England - Cambridgeshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(533, 222, 'Cheshire', 'England - Cheshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(534, 222, 'Cleveland', 'England - Cleveland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(535, 222, 'Cornwall', 'England - Cornwall');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(536, 222, 'Cumbria', 'England - Cumbria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(537, 222, 'Derbyshire', 'England - Derbyshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(538, 222, 'Devon', 'England - Devon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(539, 222, 'Dorset', 'England - Dorset');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(540, 222, 'Durham', 'England - Durham');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(541, 222, 'E. Riding', 'England - East Riding of Yorkshi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(542, 222, 'E. Sussex', 'England - East Sussex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(543, 222, 'Essex', 'England - Essex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(544, 222, 'Gloucestershire', 'England - Gloucestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(545, 222, 'Gr. Manchester', 'England - Greater Manchester');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(546, 222, 'Hampshire', 'England - Hampshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(547, 222, 'Herefordshire', 'England - Herefordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(548, 222, 'Hertfordshire', 'England - Hertfordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(549, 222, 'Humberside', 'England - Humberside');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(550, 222, 'Isle of Wight', 'England - Isle of Wight');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(551, 222, 'Isles of Scilly', 'England - Isles of Scilly');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(552, 222, 'Kent', 'England - Kent');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(553, 222, 'Lancashire', 'England - Lancashire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(554, 222, 'Leicestershire', 'England - Leicestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(555, 222, 'Lincolnshire', 'England - Lincolnshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(556, 222, 'London', 'England - London');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(557, 222, 'Merseyside', 'England - Merseyside');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(558, 222, 'Middlesex', 'England - Middlesex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(559, 222, 'Norfolk', 'England - Norfolk');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(560, 222, 'N. Yorkshire', 'England - North Yorkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(561, 222, 'Northamptonshire', 'England - Northamptonshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(562, 222, 'Northumberland', 'England - Northumberland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(563, 222, 'Nottinghamshire', 'England - Nottinghamshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(564, 222, 'Oxfordshire', 'England - Oxfordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(565, 222, 'Rutland', 'England - Rutland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(566, 222, 'Shropshire', 'England - Shropshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(567, 222, 'Somerset', 'England - Somerset');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(568, 222, 'S. Gloucestershire', 'England - South Gloucestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(569, 222, 'S. Yorkshire', 'England - South Yorkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(570, 222, 'Staffordshire', 'England - Staffordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(571, 222, 'Suffolk', 'England - Suffolk');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(572, 222, 'Surrey', 'England - Surrey');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(573, 222, 'Tyne and Wear', 'England - Tyne and Wear');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(574, 222, 'Warwickshire', 'England - Warwickshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(575, 222, 'W. Midlands', 'England - West Midlands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(576, 222, 'W. Sussex', 'England - West Sussex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(577, 222, 'W. Yorkshire', 'England - West Yorkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(578, 222, 'Wiltshire', 'England - Wiltshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(579, 222, 'Worcestershire', 'England - Worcestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(580, 222, 'Antrim', 'N. Ireland - Antrim');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(581, 222, 'Armagh', 'N. Ireland - Armagh');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(582, 222, 'Down', 'N. Ireland - Down');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(583, 222, 'Fermanagh', 'N. Ireland - Fermanagh');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(584, 222, 'Londonderry', 'N. Ireland - Londonderry');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(585, 222, 'Tyrone', 'N. Ireland - Tyrone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(586, 222, 'Aberdeen City', 'Scotland - Aberdeen City');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(587, 222, 'Aberdeenshire', 'Scotland - Aberdeenshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(588, 222, 'Angus', 'Scotland - Angus');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(589, 222, 'Argyll and Bute', 'Scotland - Argyll and Bute');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(590, 222, 'Borders', 'Scotland - Borders');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(591, 222, 'Clackmannan', 'Scotland - Clackmannan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(592, 222, 'Dumfries and Galloway', 'Scotland - Dumfries and Galloway');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(593, 222, 'E. Ayrshire', 'Scotland - East Ayrshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(594, 222, 'E. Dunbartonshire', 'Scotland - East Dunbartonshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(595, 222, 'E. Lothian', 'Scotland - East Lothian');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(596, 222, 'E. Renfrewshire', 'Scotland - East Renfrewshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(597, 222, 'Edinburgh City', 'Scotland - Edinburgh City');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(598, 222, 'Falkirk', 'Scotland - Falkirk');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(599, 222, 'Fife', 'Scotland - Fife');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(600, 222, 'Glasgow', 'Scotland - Glasgow');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(601, 222, 'Highland', 'Scotland - Highland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(602, 222, 'Inverclyde', 'Scotland - Inverclyde');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(603, 222, 'Midlothian', 'Scotland - Midlothian');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(604, 222, 'Moray', 'Scotland - Moray');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(605, 222, 'North Ayrshire', 'Scotland - North Ayrshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(606, 222, 'North Lanarkshire', 'Scotland - North Lanarkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(607, 222, 'Orkney', 'Scotland - Orkney');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(608, 222, 'Perthshire and Kinross', 'Scotland - Perthshire and Kinros');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(609, 222, 'Renfrewshire', 'Scotland - Renfrewshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(610, 222, 'Shetland', 'Scotland - Shetland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(611, 222, 'South Ayrshire', 'Scotland - South Ayrshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(612, 222, 'South Lanarkshire', 'Scotland - South Lanarkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(613, 222, 'Stirling', 'Scotland - Stirling');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(614, 222, 'West Dunbartonshire', 'Scotland - West Dunbartonshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(615, 222, 'West Lothian', 'Scotland - West Lothian');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(616, 222, 'Western Isles', 'Scotland - Western Isles');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(617, 222, 'Blaenau Gwent', 'UA Wales - Blaenau Gwent');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(618, 222, 'Bridgend', 'UA Wales - Bridgend');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(619, 222, 'Caerphilly', 'UA Wales - Caerphilly');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(620, 222, 'Cardiff', 'UA Wales - Cardiff');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(621, 222, 'Carmarthenshire', 'UA Wales - Carmarthenshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(622, 222, 'Ceredigion', 'UA Wales - Ceredigion');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(623, 222, 'Conwy', 'UA Wales - Conwy');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(624, 222, 'Denbighshire', 'UA Wales - Denbighshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(625, 222, 'Flintshire', 'UA Wales - Flintshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(626, 222, 'Gwynedd', 'UA Wales - Gwynedd');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(627, 222, 'Isle of Anglesey', 'UA Wales - Isle of Anglesey');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(628, 222, 'Merthyr Tydfil', 'UA Wales - Merthyr Tydfil');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(629, 222, 'Monmouthshire', 'UA Wales - Monmouthshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(630, 222, 'Neath Port Talbot', 'UA Wales - Neath Port Talbot');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(631, 222, 'Newport', 'UA Wales - Newport');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(632, 222, 'Pembrokeshire', 'UA Wales - Pembrokeshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(633, 222, 'Powys', 'UA Wales - Powys');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(634, 222, 'Rhondda Cynon Taff', 'UA Wales - Rhondda Cynon Taff');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(635, 222, 'Swansea', 'UA Wales - Swansea');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(636, 222, 'Torfaen', 'UA Wales - Torfaen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(637, 222, 'The Vale of Glamorgan', 'UA Wales - The Vale of Glamorgan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(638, 222, 'Wrexham', 'UA Wales - Wrexham');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(639, 222, 'Channel Islands', 'UK Offshore Dependencies - Chann');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(640, 222, 'Isle of Man', 'UK Offshore Dependencies - Isle ');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(641, 195, 'Zaragoza', 'Zaragoza');

CREATE TABLE IF NOT EXISTS `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`association_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=84 ;

INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(83, 38, 76, 9, NULL, '2015-02-09 18:53:27');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(2, 73, 0, 2, NULL, '2006-04-11 18:48:48');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(4, 81, 0, 3, NULL, '2006-05-04 10:29:41');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(5, 14, 0, 3, NULL, '2006-05-04 10:31:27');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(6, 21, 0, 3, NULL, '2006-05-04 10:31:49');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(7, 55, 0, 3, NULL, '2006-05-04 10:32:32');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(8, 57, 0, 3, NULL, '2006-05-04 10:32:47');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(9, 67, 0, 3, NULL, '2006-05-04 10:33:17');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(10, 72, 0, 3, NULL, '2006-05-04 10:33:31');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(12, 84, 0, 3, NULL, '2006-05-04 10:33:56');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(14, 103, 0, 3, NULL, '2006-05-04 10:35:42');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(15, 105, 0, 3, NULL, '2006-05-04 10:35:57');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(16, 117, 0, 3, NULL, '2006-05-04 10:36:50');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(17, 123, 0, 3, NULL, '2006-05-04 10:37:26');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(18, 124, 0, 3, NULL, '2006-05-04 10:37:53');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(19, 132, 0, 3, NULL, '2006-05-04 10:38:21');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(20, 150, 0, 3, NULL, '2006-05-04 10:39:27');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(21, 170, 0, 3, NULL, '2006-05-04 10:39:43');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(22, 171, 0, 3, NULL, '2006-05-04 10:40:22');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(23, 56, 0, 3, NULL, '2006-05-04 10:41:04');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(24, 222, 0, 3, NULL, '2006-05-04 10:41:39');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(25, 189, 0, 3, NULL, '2006-05-04 10:42:32');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(26, 190, 0, 3, NULL, '2006-05-04 10:43:00');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(27, 203, 0, 3, NULL, '2006-05-04 10:43:41');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(28, 195, 0, 3, NULL, '2006-05-04 10:45:45');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(30, 0, 0, 3, NULL, '2006-07-08 17:51:18');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(73, 38, 69, 11, NULL, '2015-02-09 16:06:12');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(72, 38, 70, 11, NULL, '2015-02-09 16:05:57');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(71, 38, 67, 8, NULL, '2015-02-09 16:04:56');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(60, 38, 66, 7, NULL, '2008-09-15 21:52:33');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(82, 38, 77, 14, NULL, '2015-02-09 16:12:14');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(63, 38, 72, 7, NULL, '2008-09-15 21:53:08');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(81, 38, 68, 14, NULL, '2015-02-09 16:12:03');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(77, 38, 76, 7, NULL, '2015-02-09 16:10:12');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(80, 38, 77, 7, NULL, '2015-02-09 16:11:01');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(74, 38, 74, 11, NULL, '2015-02-09 16:06:26');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(79, 38, 68, 7, NULL, '2015-02-09 16:10:46');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(78, 38, 78, 7, NULL, '2015-02-09 16:10:29');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(75, 38, 75, 12, NULL, '2015-02-09 16:07:25');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(76, 38, 71, 13, NULL, '2015-02-09 16:08:19');
