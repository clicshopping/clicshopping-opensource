<?php
/**
 * newlsetter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: newlsetter_no_account.php
*/

define('NAVBAR_TITLE', 'Abonnement newsletter');

define('HEADING_TITLE', 'Abonnement newsletter');

define('TEXT_ORIGIN_LOGIN', 'S\'abonner à la newsletter sur '.STORE_NAME.' se fait très simplement !<br /><br />Toutes les informations que vous nous donnerez resteront bien évidemment strictement privées et réservées. A aucun moment vos informations ne seront divulguées à d\'autres parties ou pour aucune action ou propos commercial autre que celles de '.STORE_NAME.' et que vous avez autorisé . Toutes ces informations ne servent à '.STORE_NAME.' que pour mieux vous suivre et vous servir.<br /><br /> Vous pourrez à tout moment mettre à jour ou supprimer les informations que vous nous aurez données sur simple demande par email. Vous gardez ainsi à tout moment le plein contr&ocirc;le sur les informations que vous nous avez transmis.</font><br /><br />'.STORE_NAME.'  respecte les chartes Informatiques et Libertés existantes.');


define('EMAIL_SUBJECT', 'Abonnement à la newsletter de ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_WELCOME', '<div align="justify">Nous vous remercions pour la confiance que vous nous témoigniez en vous enregistrant à la newsletter  sur le site ' . HTTP_COOKIE_DOMAIN . "\n\n" .'Comme suite à votre demande  sur '.STORE_NAME.', nous vous confirmons sa validation. '. "\n\n" . 'Pour toute aide sur nos services, n\'hésitez pas à contacter notre service clientèle : ' . STORE_OWNER_EMAIL_ADDRESS);
define('EMAIL_WARNING', ''. "\n\n" . ' Très cordialement,'. "\n\n" . '<strong>L\'équipe '.STORE_NAME.'</strong>'. "\n\n" . '
<u><font size="2">Avis de confidentialité :</u>' . "\n\n" . 'Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans être le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez recu ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n\n" . '
<p>Conformément à la Loi dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</font>
</div>');

define('TEXT_PRIVACY_CONDITIONS_DESCRIPTION', 'Notre politique de confidentialité est consultable à tout moment sur '.STORE_NAME.' en cliquant <a target="_blank" href="' . SHOP_CODE_URL_CONFIDENTIALITY . '"><u>ici</u></a>.');

define('TEXT_SUCCESS','Félicitations ! Vous pourrez recevoir prochainement l\'ensemble des informations concernant '.STORE_NAME.' pendant une période de 1 an.
<p>N\'hesitez pas vous enregistrer sur '.STORE_NAME.' pour recevoir encore plus d\'informations sur nos actualités, nos exceptionnels promotions destinées à nos clients fidèles.<br /><br />Pour toutes questions relative au fonctionnement du site et aux commandes en ligne, vous pouvez soit consulter la rubrique  "INFOS CLIENTS" ou contacter notre service commercial via un e-mail par le formulaire mis à votre disposition : <a href="' . osc_href_link('contact_us.php') . '">Nous contacter</a>.<br /><br />Cordialement, l\'équipe commeciale '.STORE_NAME.'');

define('ENTRY_NUMBER_EMAIL_CONFIRMATION','Combien font : ');

define('ERROR_ACTION_RECORDER', 'Erreur: Une demande a été déjà réalisée. Veuillez recommencer dans %s minutes.');
?>
