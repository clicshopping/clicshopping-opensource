<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'History');

define('HEADING_TITLE', 'My Order / Invoice History');

define('HEADING_DESCRIPTION', 'Follow the development of your order and see your historic.');
define('TEXT_ORDER_NUMBER', 'Order / Invoice Number:');
define('TEXT_ORDER_STATUS', 'Status : ');
define('TEXT_ORDER_DATE', 'Date of the order :');
define('TEXT_ORDER_SHIPPED_TO', 'Shipped To:');
define('TEXT_ORDER_BILLED_TO', 'Billed To:');
define('TEXT_ORDER_PRODUCTS', 'Products:');
define('TEXT_ORDER_COST', 'Total amount:');
define('TEXT_VIEW_ORDER', 'View Order');

define('TEXT_NO_PURCHASES', 'You have not yet made any purchases or your order is being processed.');
?>
