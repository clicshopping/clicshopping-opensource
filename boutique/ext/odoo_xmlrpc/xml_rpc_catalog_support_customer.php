<?php
/*
 * xml_rpc_catalog_support_customer.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_id', '=', $OSCOM_Customer->getId(), 'res.partner');

// **********************************
// read id customer odoo
// **********************************
  $field_list = array('id',
                      'email',
                      'phone'
                    );
  $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
  $id_odoo_customer = $id_odoo_customer_array[0][id];
  $id_odoo_customer_email = $id_odoo_customer_array[0][email];
  $id_odoo_customer_phone = $id_odoo_customer_array[0][phone];


  if  (!empty($id_odoo_customer)) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
    $values = array(
                    "name" => new xmlrpcval($email_subject, "string"),
                    "partner_id" => new xmlrpcval($id_odoo_customer, "int"),
                    "partner_phone" => new xmlrpcval($id_odoo_customer_phone, "string"),
                    "email_from" => new xmlrpcval($id_odoo_customer_email, "string"),
                    "description" => new xmlrpcval($enquiry, "string"),
                  );

    $OSCOM_ODOO->createOdoo($values, "crm.claim");
  }
?>