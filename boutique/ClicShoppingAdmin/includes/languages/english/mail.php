<?php
/**
 * mail.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Send Email To Customers');

define('TEXT_CUSTOMER', 'Customer:');
define('TEXT_SUBJECT', 'Subject:');
define('TEXT_FROM', 'From:');
define('TEXT_MESSAGE', 'Message:');
define('TEXT_SELECT_CUSTOMER', 'Select Customer');
define('TEXT_ALL_CUSTOMERS', 'All Customers');
define('TEXT_NEWSLETTER_CUSTOMERS', 'To All Newsletter Subscribers');

define('NOTICE_EMAIL_SENT_TO', 'Email sent to: %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Error: No customer has been selected.');

define('TITLE_HELP_DESCRIPTION', 'Note on the use of the wysiwyg');
define('HELP_DESCRIPTION', '<li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock more)</li>');
define('TEXT_FOOTER','<U><font size="2">Confidentiality Note :</font></U><font size="2">'. "\n" . '
This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '
<p><font size="2">In accordance with the <strong>Law</strong> on �Data-processing Law and Freedom�, you are entitled to correction of your personal data or on request by email</font>
');
?>
