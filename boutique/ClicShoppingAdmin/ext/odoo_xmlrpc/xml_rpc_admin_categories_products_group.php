<?php
/*
 * xml_rpc_admin_catgories_products_group.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// **********************************
// Search odoo groups id
// **********************************


  $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('clicshopping_customers_group_id', '=', $customers_group['customers_group_id'], 'clicshopping.products.group', 'int',
                                              'clicshopping_products_id', '=', $products_id, 'int');

// **********************************
// read id Categories odoo
// **********************************

  $field_list = array('id');

  $QodooCustomersGroupId = $OSCOM_ODOO->readOdoo($ids, $field_list, "clicshopping.products.group");
  $odoo_customers_group_id = $QodooCustomersGroupId[0][id];

  if  (!empty($odoo_customers_group_id)) {

      $values = array("clicshopping_customers_group_id" => new xmlrpcval($customers_group['customers_group_id'], "int"),
                      "clicshopping_products_id" => new xmlrpcval($products_id, "int"),
                      "clicshopping_products_group_view" => new xmlrpcval($products_group_view, "int"),
                      "clicshopping_orders_group_view" => new xmlrpcval($orders_group_view, "int"),
                      "clicshopping_price_group_view" => new xmlrpcval($price_group_view, "double"),
                      "clicshopping_products_model_group" => new xmlrpcval($products_model_group, "string"),
                      "clicshopping_products_quantity_unit_id_group" => new xmlrpcval($products_quantity_unit_id_group, "int"),
                      "clicshopping_products_quantity_fixed_group" => new xmlrpcval($products_quantity_fixed_group, "int"),
                    );

      $OSCOM_ODOO->updateOdoo($odoo_customers_group_id, $values, "clicshopping.products.group");
  }