<?php
/**
 * security_checks.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  function osc_sort_secmodules($a, $b) {
    return strcmp($a['title'], $b['title']);
  }

  $types = array('info', 'warning', 'error');

  $modules = array();

  if ($secdir = @dir(DIR_FS_ADMIN . 'includes/modules/security_check/')) {
    while ($file = $secdir->read()) {
      if (!is_dir(DIR_FS_ADMIN . 'includes/modules/security_check/' . $file)) {
        if (substr($file, strrpos($file, '.')) == '.php') {
          $class = 'securityCheck_' . substr($file, 0, strrpos($file, '.'));

          include(DIR_FS_ADMIN . 'includes/modules/security_check/' . $file);
          $$class = new $class();

          $modules[] = array('title' => substr($file, 0, strrpos($file, '.')),
                             'class' => $class);
        }
      }
    }
    $secdir->close();
  }

  if ($extdir = @dir(DIR_FS_ADMIN . 'includes/modules/security_check/extended/')) {
    while ($file = $extdir->read()) {
      if (!is_dir(DIR_FS_ADMIN . 'includes/modules/security_check/extended/' . $file)) {
        if (substr($file, strrpos($file, '.')) == '.php') {
          $class = 'securityCheckExtended_' . substr($file, 0, strrpos($file, '.'));

          include(DIR_FS_ADMIN . 'includes/modules/security_check/extended/' . $file);
          $$class = new $class();

          $modules[] = array('title' => substr($file, 0, strrpos($file, '.')),
                             'class' => $class);
        }
      }
    }
    $extdir->close();
  }

  usort($modules, 'osc_sort_secmodules');

  require('includes/header.php');
?>


<!-- body //-->
 <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/modules_action_recorder.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-7 pageHeading"><?php echo HEADING_TITLE; ?></span>
    <span class="pull-right"><?php  echo '<a href="' . osc_href_link('security_checks.php') . '">' . osc_image_button('button_reset.gif', IMAGE_UPDATE) . '</a>'; ?></span>
    </tr>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" width="20">&nbsp;</td>
                <td class="dataTableHeadingContent" width="200"><?php echo TABLE_HEADING_MODULES; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_INFO; ?></td>
<!--                <td class="dataTableHeadingContent" width="20" align="right">&nbsp;</td>-->
              </tr>

<?php
  foreach ($modules as $module) {
    $secCheck = $$module['class'];

    if ( !in_array($secCheck->type, $types) ) {
      $secCheck->type = 'info';
    }

    $output = '';

    if ( $secCheck->pass() ) {
      $secCheck->type = 'success';
    } else {
      $output = $secCheck->getMessage();
    }

    echo '  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n" .
         '    <td class="dataTableContent" align="center" valign="top">' . osc_image(DIR_WS_IMAGES . 'ms_' . $secCheck->type . '.png', '', 16, 16) . '</td>' . "\n" .
         '    <td class="dataTableContent" valign="top">' . osc_output_string_protected($module['title']) . '</td>' . "\n" .
         '    <td class="dataTableContent" valign="top">' . $output . '</td>' . "\n" .
//         '    <td class="dataTableContent" align="center" valign="top"><a href="http://library.oscommerce.com/Wiki&en&oscom_2_3&security&security_checks&">' . osc_image(DIR_WS_IMAGES . 'icons/preview.gif') . '</a></td>' . "\n" .
         '  </tr>' . "\n";
  }
?>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<?php
  require('includes/footer.php');
  require('includes/application_bottom.php');
?>