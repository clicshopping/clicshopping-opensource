<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'History');
define('NAVBAR_TITLE_3', 'Order #%s');

define('HEADING_TITLE', 'Order / Invoice Informations');

define('HEADING_ORDER_NUMBER', 'No #%s');
define('HEADING_ORDER_DATE', 'Date of the order :');
define('HEADING_ORDER_TOTAL', 'Amount Total :');

define('HEADING_DELIVERY_ADDRESS', 'Delivery Address');
define('HEADING_SHIPPING_METHOD', 'Shipping Method');

define('HEADING_PRODUCTS', 'Products');
define('HEADING_TAX', 'Tax');
define('HEADING_TOTAL', 'Total');

define('HEADING_BILLING_INFORMATION', 'Billing Information');
define('HEADING_BILLING_ADDRESS', 'Billing Address');
define('HEADING_PAYMENT_METHOD', 'Payment Method');

define('HEADING_ORDER_HISTORY', 'Order / Invoice History');
define('HEADING_COMMENT', 'Comments');
define('TEXT_NO_COMMENTS_AVAILABLE', 'No comments available.');

define('HEADING_FOLLOW_TRACKING','Follow your order');
define('HEADING_FOLLOW_TRACKING_TEXT','To request a withdrawal, please use the link to available below');

define('TABLE_HEADING_DOWNLOAD_DATE', 'Link expires: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' downloads remaining');
define('HEADING_DOWNLOAD', 'Download links');
define('IMAGE_BUTTON_PRINT_INVOICE','Download the invoice');
define('IMAGE_BUTTON_PRINT_ORDER','Download the order');
define('INVOICE_DOWNLOAD','Download invoice');
define('HEADING_MY_SUPPORT','Customer support');
define('TEXT_MY_SUPPORT','Contact support or sales department regarding this order');
define('TEXT_TRACKING_SERVICES', 'Follow your shipping');
define('IMAGE_BUTTON_PRINT_ORDER_ORDER','Print order');

define('TEXT_DOWNLOAD_FILES', 'Download the files');
define('TEXT_INVOICE_DOWNLOAD', 'Invoice et Download');

define('HEADING_ORDER_CONDITIONS_OF_SALES','Contract of conditions of Sales');
define('TEXT_CONDITION_GENERAL_OF_SALES','See your generals conditions of sales concerning this order');
?>
