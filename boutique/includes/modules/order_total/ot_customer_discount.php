<?php
/**
 * ot_customer_discount.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class ot_customer_discount {
    var $title, $output;

    function ot_customer_discount() {
      $this->code = 'ot_customer_discount';
      $this->title = MODULE_CUSTOMER_DISCOUNT_TITLE;
      $this->description = MODULE_CUSTOMER_DISCOUNT_DESCRIPTION;
      $this->enabled = MODULE_CUSTOMER_DISCOUNT_STATUS;
      $this->sort_order = MODULE_CUSTOMER_DISCOUNT_SORT_ORDER;
      $this->include_shipping = MODULE_CUSTOMER_DISCOUNT_INC_SHIPPING;
      $this->include_tax = MODULE_CUSTOMER_DISCOUNT_INC_TAX;
      $this->calculate_tax = MODULE_CUSTOMER_DISCOUNT_CALC_TAX;
      $this->output = array();
    }

    function process(){
      global $order, $currencies;

      $od_amount = $this->calculate_discount($this->get_order_total());
      if ($od_amount>0){
        $this->deduction = $od_amount;
        $this->output[] = array('title' => $this->title . ':',
                                'text' =>  $currencies->format($od_amount),
                                'value' => $od_amount);
        $order->info['total'] = $order->info['total'] - $od_amount;
        $order->info['subtotal'] -= $od_amount;
      }
    }

  function calculate_discount($amount) {
    global $order, $OSCOM_Customer, $OSCOM_PDO;
    $od_amount = 0;

    $QCustomerDiscount = $OSCOM_PDO->prepare("select customer_discount
                                                from :table_customers
                                                where customers_id = :customers_id
                                      ");
    $QCustomerDiscount->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    $QCustomerDiscount->execute();

    $customer_discount = $QCustomerDiscount->fetch();
    $customer_discount = $customer_discount['customer_discount'];

    if ($customer_discount > 0) {
// Calculate tax reduction if necessary
    if ($this->calculate_tax == 'true') {

// Calculate main tax reduction
      $tod_amount = round($order->info['tax']*($customer_discount/100),2);

      $order->info['tax'] = $order->info['tax'] - $tod_amount;
// Calculate tax group deductions
      reset($order->info['tax_groups']);
      while (list($key, $value) = each($order->info['tax_groups'])) {
        $od_amount = round($value*($customer_discount/100),2);
        $order->info['tax_groups'][$key] = $order->info['tax_groups'][$key] - $od_amount;
      }
    }

    $od_amount = round($amount*($customer_discount/100),2);
    $order->info['total'] -= $tod_amount;

    }
  return $od_amount;
  }


  function get_order_total() {
    global  $order;
    $order_total = $order->info['total'];
    if ($this->include_tax == 'false') $order_total = $order_total-$order->info['tax'];
    if ($this->include_shipping == 'false') $order_total = $order_total-$order->info['shipping_cost'];
    return $order_total;
  }   
    
    function check() {
      if (!isset($this->check)) {
        $check_query = osc_db_query("select configuration_value from configuration where configuration_key = 'MODULE_CUSTOMER_DISCOUNT_STATUS'");
        $this->check = osc_db_num_rows($check_query);
      }

      return $this->check;
    }

    function keys() {
      return array('MODULE_CUSTOMER_DISCOUNT_STATUS', 
                  'MODULE_CUSTOMER_DISCOUNT_INC_SHIPPING', 
                  'MODULE_CUSTOMER_DISCOUNT_INC_TAX', 
                  'MODULE_CUSTOMER_DISCOUNT_CALC_TAX',
                  'MODULE_CUSTOMER_DISCOUNT_SORT_ORDER'
                  );
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez vous Afficher la remise ?', 'MODULE_CUSTOMER_DISCOUNT_STATUS', 'true', 'Souhaitez-vous installer le module de remise client ?<br /><br /><strong>Note : </strong><br /><br />- Le module doit toujours etre install&eacute; avant le montant total de commande.<br />- Pour les double taxe, veuillez afficher le module avant l\'affichage du sous-total pour un bon fonctionnement.', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez-vous inclure les frais d\'exp&eacute;dition dans le calcul de la remise ?', 'MODULE_CUSTOMER_DISCOUNT_INC_SHIPPING', 'false', 'Souhaitez-vous inclure les frais d\'exp&eacute;dition dans le calcul de la remise ?', '6', '2', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez-vous inclure la taxe dans le calcul de la remise ?', 'MODULE_CUSTOMER_DISCOUNT_INC_TAX', 'false', 'Souhaitez-vous inclure la taxe dans le calcul de la remise ?', '6', '3', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez-vous  un re-calcul de la taxe avec le montant de la remise ?', 'MODULE_CUSTOMER_DISCOUNT_CALC_TAX', 'false', 'Souhaitez-vous  un re-calcul de la taxe avec le montant de la remise.', '6', '4', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_CUSTOMER_DISCOUNT_SORT_ORDER', '3', 'Sort order of display.', '6', '5', now())");
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>