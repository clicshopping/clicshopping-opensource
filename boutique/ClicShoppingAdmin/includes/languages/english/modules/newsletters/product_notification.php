<?php
/**
 * products_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('TEXT_COUNT_CUSTOMERS', 'Customers receiving newsletter: %s');
define('TEXT_PRODUCTS', 'Products');
define('TEXT_SELECTED_PRODUCTS', 'Selected Products');
define('TEXT_UNSUBSCRIBE', '<font size="2">In accordance with the <strong>Law</strong>, you are entitled to correction of your personal data or on request by email.<br />To unsubscribe, go to your account and product notifcation section:</font><br />');

define('JS_PLEASE_SELECT_PRODUCTS', 'Please select some products.');

define('BUTTON_GLOBAL', 'Global');
define('BUTTON_SELECT', '>>>');
define('BUTTON_UNSELECT', '<<<');
define('BUTTON_SUBMIT', 'Submit');
define('BUTTON_CANCEL', 'Cancel');
?>