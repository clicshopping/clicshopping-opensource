<?php
/**
 * odoo.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */


include_once("ext/api/xml_rpc/xmlrpc.inc.php");
include_once("ext/api/xml_rpc/xmlrpcs.inc.php");
$GLOBALS['xmlrpc_internalencoding']='UTF-8';

  class odoo {

    private $user, $password, $database, $server_url, $response, $id;

    function __construct() {

      $this->user = ODOO_USER_WEB_SERVICE;
      $this->password = ODOO_PASSWORD_WEB_SERVICE;
      $this->database = ODOO_DATABASE_NAME_WEB_SERVICE;
      $this->server_url = ODOO_WEBSERVER_WEB_SERVICE .':'.ODOO_PORT_WEB_SERVICE;

      $connexion = new xmlrpc_client($this->server_url . "/xmlrpc/common");
      $connexion->setSSLVerifyPeer(0);

      $c_msg = new xmlrpcmsg('login');
      $c_msg->addParam(new xmlrpcval($this->database, "string"));
      $c_msg->addParam(new xmlrpcval($this->user, "string"));
      $c_msg->addParam(new xmlrpcval($this->password, "string"));
      $this->response = $connexion->send($c_msg);

      if ($this->response->errno == 0 ){
        $this->id = $this->response->value()->scalarval();
      }  else {
        if (ODOO_EMAIL_WEB_SERVICE == 'true') {
          $email_subject = 'Odoo webservice Error on customers.php';
          $body_message = 'There is an error with odoo webservice on ' . HTTP_SERVER . ' at ' . date('Y-m-d H:i:s') . ' - message webservice odoo - error : ' . $this->res->faultString();
          osc_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $body_message .'<br /><br /> '.$this->response, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, '');
        }
//        echo '<strong>Action saved on the website but : </strong>Unable to login to Odoo, Please verify your webservice parameters. You will received an email with details. <br /><br />' .$this->response->faultString();
        return -1;
      }
    }


    function traverse_structure($ids) {
      $return_ids = array();
      $iterator = new RecursiveArrayIterator($ids);

      while ( $iterator -> valid() ) {
        if ( $iterator -> hasChildren() ) {
          $return_ids = array_merge( $return_ids, $this->traverse_structure($iterator -> getChildren()) );
        }
        else {
          if ($iterator -> key() == 'int') {
            $return_ids = array_merge( $return_ids, array( $iterator -> current() ) );
          }
        }
        $iterator -> next();
      }
      return $return_ids;
    }



/*
@string : $attribute, fields of odoo databasee
@string : $operator, search type
@string : $keys, valueto search
@string : $relation, odoo database
]return : $ids , id and name of the odoo country
*/
    Public function odooSearch($attribute, $operator, $keys, $relation, $string = 'string') {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $domain_filter = array (
                                new xmlrpcval(
                                                array(
                                                  new xmlrpcval($attribute , "string"),
                                                  new xmlrpcval($operator,"string"),
                                                  new xmlrpcval($keys, $string),
                                                ),"array"
                                              ),
                              );

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int")); /* user_id */
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval("search", "string"));
      $msg->addParam(new xmlrpcval($domain_filter, "array"));

      $response = $client->send($msg);

      $val = $response->value();
      $ids = $val->scalarval();

      return $this->traverse_structure($ids);
    }


// search by 2 criterias with and OPERATOR
    Public function odooSearchByTwoCriteria($attribute, $operator, $keys, $relation, $string = 'string', $attribute1, $operator1, $keys1, $string1 = 'string') {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $domain_filter = array (
                              new xmlrpcval(
                                              array(
                                                new xmlrpcval($attribute , "string"),
                                                new xmlrpcval($operator,"string"),
                                                new xmlrpcval($keys, $string),
                                              ),"array"
                                            ),
                              new xmlrpcval(
                                              array(
                                                new xmlrpcval($attribute1 , "string"),
                                                new xmlrpcval($operator1,"string"),
                                                new xmlrpcval($keys1, $string1),
                                              ),"array"
                                            ),
                              );

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval("search", "string"));
      $msg->addParam(new xmlrpcval($domain_filter, "array"));

      $response = $client->send($msg);

      $val = $response->value();
      $ids = $val->scalarval();

      return $this->traverse_structure($ids);
    }

    public function readOdoo($ids, $fields, $relation) {
      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);


      $client->return_type = 'phpvals';
      $id_val = array();
      $count = 0;

      foreach ($ids as $id) {
        $id_val[$count++] = new xmlrpcval($id, "int");
      }

      $fields_val = array();

      $count = 0;

      foreach ($fields as $field) {
        $fields_val[$count++] = new xmlrpcval($field, "string");
      }

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval("read", "string"));
      $msg->addParam(new xmlrpcval($id_val, "array"));
      $msg->addParam(new xmlrpcval($fields_val, "array"));

      $response = $client->send($msg);

       if ($response->faultCode()) {
        return -1;  /* if the record is not writable or not existing the ids or not having permissions  */
       } else {
        return $response->value();
      }
    }


    public function createOdoo($values, $relation) {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));  //* database name */
      $msg->addParam(new xmlrpcval($this->id, "int")); /* useid */
      $msg->addParam(new xmlrpcval($this->password, "string"));/** password */
      $msg->addParam(new xmlrpcval($relation, "string"));/** model name where operation will held * */
      $msg->addParam(new xmlrpcval("create", "string"));/** method which u like to execute */
      $msg->addParam(new xmlrpcval($values, "struct"));/** parameters of the methods with values....  */

      $response = $client->send($msg);

      if ($response->faultCode()) {
        return -1;  /* if the record is not writable or not existing the ids or not having permissions  */
      } else {
        return $response->value();
      }
    }

    public function updateOdoo($ids, $values, $relation) {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $id_list = array();
      $id_list[]= new xmlrpcval($ids, 'int');

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval("write", "string"));
      $msg->addParam(new xmlrpcval($id_list, "array"));
      $msg->addParam(new xmlrpcval($values, "struct"));
      $response = $client->send($msg);


      if ($response->faultCode()) {
        return -1;  /* if the record is not writable or not existing the ids or not having permissions  */
      } else {
        return $response->value();
      }
    }



    public function getFieldsOdoo($relation){
      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $client->return_type = 'phpvals';

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval("fields_get", "string"));

      $response = $client->send($msg);


      if ($response->faultCode()) {
        return -1;
      } else {
        return $response->value();
      }
    }



    public function getDefaultValuesOdoo($relation){

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $values = $this->getFieldsOdoo($relation);

      $columns = array_keys($values);
      $array_temp = array();

      foreach($columns as $column){
        array_push($array_temp, new xmlrpcval($column,"string"));
      }

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval("default_get", "string"));
      $msg->addParam(new xmlrpcval($array_temp, "array"));

      $response = $client->send($msg);
      print_r($response);

      if ($response->faultCode()) {
        return -1;
      } else {
        return $response->value();
      }
    }


/**
 * ButtonClick function in Odoo
 *
 * @param string $relation  : database in odoo
 * @param string $method : method in Odoo
 * @param string $record_id : id of record in Odoo
 * @return string $response, response of request
 * @access public
 */
    public function buttonClickOdoo($relation, $method, $record_id) {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $client->return_type = 'phpvals';

      $nval = array();

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval($method, "string"));
      $msg->addParam(new xmlrpcval($record_id, "int"));


      $response = $client->send($msg);

      if ($response->faultCode()) {
        return -1;
      } else {
        return $response->value();
      }
    }

/*
  $OSCOM_ODOO->workflowOdoo('sale.order', 'order_confirm',  $invoice_id);
  $OSCOM_ODOO->workflowOdoo('sale.order', 'manual_invoice',  $invoice_id);
*/

    public function workflowOdoo($relation, $method, $record_id) {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $client->return_type = 'phpvals';

      $msg = new xmlrpcmsg('exec_workflow');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval($method, "string"));
      $msg->addParam(new xmlrpcval($record_id, "int"));

      $response = $client->send($msg);
      if ($response->faultCode())
        return -1; /* if the record is not created  */
      else
        return $response->value();  /* return new generated id of record */
    }


    public function callFunctionOdoo($relation, $method, $ids, $params) {

      $client = new xmlrpc_client($this->server_url . "/xmlrpc/object");
      $client->setSSLVerifyPeer(0);

      $id_val = array();
      $count = 0;

      foreach ($ids as $id) {
        $id_val[$count++] = new xmlrpcval($id, "int");
      }

      $msg = new xmlrpcmsg('execute');
      $msg->addParam(new xmlrpcval($this->database, "string"));
      $msg->addParam(new xmlrpcval($this->id, "int"));
      $msg->addParam(new xmlrpcval($this->password, "string"));
      $msg->addParam(new xmlrpcval($relation, "string"));
      $msg->addParam(new xmlrpcval($method, "string"));
      $msg->addParam(new xmlrpcval($id_val, "array"));

// Send parameter to function
      foreach ($params as $param){
        $param_value = $param[0];
        $param_type = $param[1];
        $msg->addParam(new xmlrpcval($param_value, $param_type));
      }
      // Functions return values

      $response = $client->send($msg);

      if ($response->faultCode()) {
        return -1;
      } else {
        return $response->value();
      }
    }
  }