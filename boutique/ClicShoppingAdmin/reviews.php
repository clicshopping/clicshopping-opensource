<?php
/**
 * reviews.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
     case 'setflag':
        osc_set_reviews_status($_GET['id'], $_GET['flag']);
        osc_redirect(osc_href_link('reviews.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'rID=' . $_GET['id']));
        break;

     case 'update':
        $reviews_id = osc_db_prepare_input($_GET['rID']);
        $reviews_rating = osc_db_prepare_input($_POST['reviews_rating']);
        $reviews_text = osc_db_prepare_input($_POST['reviews_text']);
        $reviews_status = osc_db_prepare_input($_POST['status']);

        $Qupdate = $OSCOM_PDO->prepare('update :table_reviews 
                                        set reviews_rating = :reviews_rating,
                                            status = :status,
                                            last_modified = now()
                                        where reviews_id = :reviews_id
                                      ');
        $Qupdate->bindValue(':reviews_rating', $reviews_rating);
        $Qupdate->bindInt(':status', $reviews_status);
        $Qupdate->bindInt(':reviews_id', (int)$reviews_id );

        $Qupdate->execute();

        $Qupdate = $OSCOM_PDO->prepare('update :table_reviews_description
                                        set reviews_text = :reviews_text
                                        where reviews_id = :reviews_id
                                      ');
        $Qupdate->bindValue(':reviews_text',  $reviews_text);
        $Qupdate->bindInt(':reviews_id', (int)$reviews_id );

        $Qupdate->execute();

        osc_redirect(osc_href_link('reviews.php', 'page=' . $_GET['page'] . '&rID=' . $reviews_id));
      break;

      case 'delete_all': 
 
       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $reviews['reviews_id'] ) { 

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_reviews
                                          where reviews_id = :reviews_id 
                                        ');
          $Qdelete->bindInt(':reviews_id',   (int)$reviews['reviews_id'] );
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_reviews_description
                                          where reviews_id = :reviews_id 
                                        ');
          $Qdelete->bindInt(':reviews_id',   (int)$reviews['reviews_id'] );
          $Qdelete->execute();

         }
       }

       osc_redirect(osc_href_link('reviews.php'));
      break;
    }
  }

  if (empty($action)) {
    $reviews_query_raw = "select r.reviews_id,
                                 r.products_id, 
                                 r.date_added, 
                                 r.last_modified, 
                                 r.reviews_rating, 
                                 r.status,
                                 p.products_image                                                          
                          from reviews r,
                               products p
                          where p.products_id = r.products_id 
                      order by r.date_added DESC
                         ";
    $reviews_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $reviews_query_raw, $reviews_query_numrows);
    $reviews_query = osc_db_query($reviews_query_raw);
  }
 
  require('includes/header.php');
?>

  <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
<!-- body_text //-->
      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <div class="adminTitle">
        <span class="col-md-1 pull-left"><?php echo osc_image(DIR_WS_IMAGES . 'categories/reviews.gif', HEADING_TITLE, '40', '40'); ?></span>
        <span class="pageHeading col-md-3 pull-left"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>

        <span class="col-md-4 smallText" valign="middle" align="center">
<?php echo $reviews_split->display_count($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></br />
<?php echo $reviews_split->display_links($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
        </span>
        <span class="pull-right">
          <form name="delete_all" <?php echo 'action="' . osc_href_link('reviews.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
            <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
        </span>
<?php
  } elseif ($action == 'edit') {
    if (osc_not_null($_POST)) {
      $rInfo = new objectInfo($_POST);
    } else {
      $rID = osc_db_prepare_input($_GET['rID']);

      $Qreviews = $OSCOM_PDO->prepare("select r.reviews_id,
                                              r.products_id,
                                              r.customers_name,
                                              r.date_added,
                                              r.last_modified,
                                              r.reviews_read,
                                              r.status,
                                              rd.reviews_text,
                                              r.reviews_rating
                                       from :table_reviews r,
                                            :table_reviews_description rd
                                       where r.reviews_id = :reviews_id
                                       and r.reviews_id = rd.reviews_id
                                      ");
      $Qreviews->bindValue(':reviews_id',(int)$rID);
      $Qreviews->execute();

      $reviews = $Qreviews->fetch();

      $Qproducts = $OSCOM_PDO->prepare("select products_image
                                       from :table_products
                                       where products_id = :products_id
                                      ");
      $Qproducts->bindValue(':products_id',(int)$reviews['products_id']);
      $Qproducts->execute();

      $products = $Qproducts->fetch();


      $QproductsName = $OSCOM_PDO->prepare("select products_name
                                           from :table_products_description
                                           where products_id = :products_id
                                           and language_id = :language_id
                                      ");
      $QproductsName->bindValue(':products_id',(int)$reviews['products_id']) ;
      $QproductsName->bindValue(':language_id',(int)$_SESSION['languages_id']);
      $QproductsName->execute();

      $products_name = $QproductsName->fetch();

      $rInfo_array = array_merge($reviews, $products, $products_name);
      $rInfo = new objectInfo($rInfo_array);
    }

    echo osc_draw_form('update', 'reviews.php', 'page=' . $_GET['page'] . '&rID=' . $_GET['rID'] . '&action=update', 'post', 'enctype="multipart/form-data"');

    foreach ( $_POST as $key => $value ) echo osc_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
?>
        <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
        <span class="pull-right"><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?>&nbsp;</span>
        <span class="pull-right"><?php echo '<a href="' . osc_href_link('reviews.php', 'page=' . $_GET['page'] . '&rID=' . $rInfo->reviews_id) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
<?php
  }
?>
      </div>
    </tr>
  </table>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<!-- //################################################################################################################ -->
<!-- //                                               EDITION AVIS CLIENTS                                              -->
<!-- //################################################################################################################ -->
<?php
  if ($action == 'edit') {
    $rID = osc_db_prepare_input($_GET['rID']);

    $Qreviews = $OSCOM_PDO->prepare("select r.reviews_id,
                                            r.products_id,
                                            r.customers_name,
                                            r.date_added,
                                            r.last_modified,
                                            r.reviews_read,
                                            r.status,
                                            rd.reviews_text,
                                            r.reviews_rating
                                     from :table_reviews r,
                                          :table_reviews_description rd
                                     where r.reviews_id = :reviews_id
                                     and r.reviews_id = rd.reviews_id
                                    ");
    $Qreviews->bindValue(':reviews_id',(int)$rID);
    $Qreviews->execute();

    $reviews = $Qreviews->fetch();


    $Qproducts = $OSCOM_PDO->prepare("select products_image
                                     from :table_products
                                     where products_id = :products_id
                                    ");
    $Qproducts->bindValue(':products_id',(int)$reviews['products_id']);
    $Qproducts->execute();

    $products = $Qproducts->fetch();



    $QproductsName = $OSCOM_PDO->prepare("select products_name
                                         from :table_products_description
                                         where products_id = :products_id
                                         and language_id = :language_id
                                         ");
    $QproductsName->bindValue(':products_id',(int)$reviews['products_id']) ;
    $QproductsName->bindValue(':language_id',(int)$_SESSION['languages_id']);
    $QproductsName->execute();

    $products_name = $QproductsName->fetch();

    $rInfo_array = array_merge((array)$reviews, (array)$products, (array)$products_name);
    $rInfo = new objectInfo($rInfo_array);

//creation du tableau pour le  dropdown des status des commentaires
    $status_array = array(array('id' => '1', 'text' => ENTRY_STATUS_YES),
                    array('id' => '0', 'text' => ENTRY_STATUS_NO));
 ?>

      <div>
        <ul class="nav nav-tabs" role="tablist"  id="myTab">
          <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
        </ul>
        <div class="tabsClicShopping">
          <div class="tab-content">
<!-- //################################################################################################################ -->
<!--          ONGLET Information General sur l'avis client          //-->
<!-- //################################################################################################################ -->
            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TITLE_REVIEWS_GENERAL; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12 col-md-10">
                  <div class="col-md-12">
                    <span class="col-md-2"><?php echo ENTRY_PRODUCT; ?></span>
                    <span class="col-md-4"><?php echo '<strong>' . $rInfo->products_name . '</strong>'; ?></span>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2"><?php echo ENTRY_FROM; ?></span>
                      <span class="col-md-4"><?php echo  '<strong>' . $rInfo->customers_name . '</strong>'; ?></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2"><?php echo ENTRY_DATE; ?></span>
                      <span class="col-md-4"><?php echo '<strong>' . $OSCOM_Date->getShort($rInfo->date_added) . '</strong>'; ?></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2"><?php echo ENTRY_STATUS; ?></span>
                      <span class="col-md-4"><?php echo osc_draw_pull_down_menu('status', $status_array, (($rInfo->status == '1') ? '1' : '0')); ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-md-2 pull-right">
                  <div class="adminformAide">
                    <?php echo osc_image(HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $rInfo->products_image, $rInfo->products_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'hspace="5" vspace="5"'); ?>
                  </div>
                </div>
              </div>
            </div>
<!-- //################################################################################################################ -->
<!--          avis client          //-->
<!-- //################################################################################################################ -->
            <div><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></div>
            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TITLE_REVIEWS_ENTRY; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2"><?php echo ENTRY_REVIEW; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_textarea_field('reviews_text', 'soft', '60', '15', $rInfo->reviews_text); ?></span>
                </div>
              </div>
            </div>
<!-- //################################################################################################################ -->
<!--         Evaluation client          //-->
<!-- //################################################################################################################ -->
            <div><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></div>
            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TITLE_REVIEWS_RATING; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2"><?php echo ENTRY_RATING; ?></span>
                  <span class="col-md-1"><?php echo TEXT_BAD; ?></span>
                  <span class="col-md-1"><?php  for ($i=1; $i<=5; $i++) echo osc_draw_radio_field('reviews_rating', $i, '', $rInfo->reviews_rating); ?></span>
                  <span class="col-md-1"><?php echo '&nbsp;' . TEXT_GOOD;; ?></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php echo osc_draw_hidden_field('reviews_id', $rInfo->reviews_id) . osc_draw_hidden_field('products_id', $rInfo->products_id) . osc_draw_hidden_field('customers_name', $rInfo->customers_name) . osc_draw_hidden_field('products_name', $rInfo->products_name) . osc_draw_hidden_field('products_image', $rInfo->products_image) . osc_draw_hidden_field('date_added', $rInfo->date_added); ?></td>
   </form>
<?php
  } else {
?>
    <!-- //################################################################################################################ -->
    <!-- //                                            LISTING DES AVIS CLIENTS                                             -->
    <!-- //################################################################################################################ -->
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top">
          <table border="0" width="100%" cellspacing="0" cellpadding="5">
            <tr class="dataTableHeadingRow">
              <td width="1" style="text-align: center;"><input type="checkbox"
                                                               onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"/>
              </td>
              <td class="dataTableHeadingContent">&nbsp;</td>
              <td class="dataTableHeadingContent">&nbsp;</td>
              <td class="dataTableHeadingContent">&nbsp;</td>
              <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
              <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_RATING; ?></td>
              <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_REVIEW_AUTHOR; ?></td>
              <td class="dataTableHeadingContent"
                  align="center"><?php echo TABLE_HEADING_PRODUCTS_AVERAGE_RATING; ?></td>
              <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_REVIEW_READ; ?></td>
              <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAST_MODIFIED; ?></td>
              <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_APPROVED; ?></td>
              <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
            </tr>
<?php
    $reviews_query_raw = "select r.reviews_id,
                       r.products_id,
                       r.date_added,
                       r.last_modified,
                       r.reviews_rating,
                       r.status,
                       p.products_image
                from reviews r,
                     products p
                where p.products_id = r.products_id
                order by r.date_added DESC
               ";
    $reviews_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $reviews_query_raw, $reviews_query_numrows);
    $reviews_query = osc_db_query($reviews_query_raw);

    while($reviews = osc_db_fetch_array($reviews_query)) {

      if ((!isset($_GET['rID']) || (isset($_GET['rID']) && ($_GET['rID'] == $reviews['reviews_id']))) && !isset($rInfo)) {

        $QreviewsText = $OSCOM_PDO->prepare("select r.reviews_read,
                                                     r.customers_name,
                                                     length(rd.reviews_text) as reviews_text_size
                                              from :table_reviews r,
                                                   :table_reviews_description rd
                                              where r.reviews_id = :reviews_id
                                              and r.reviews_id = rd.reviews_id
                                              ");
        $QreviewsText->bindValue(':reviews_id', (int)$reviews['reviews_id']);
        $QreviewsText->execute();

        $reviews_text = $QreviewsText->fetch();

        $Qproducts_image = $OSCOM_PDO->prepare("select products_image
                                                 from :table_products
                                                 where products_id = :products_id
                                                ");
        $Qproducts_image->bindValue(':products_id', (int)$reviews['products_id']);
        $Qproducts_image->execute();

        $products_image = $Qproducts_image->fetch();


        $Qproducts = $OSCOM_PDO->prepare("select products_name
                                           from :table_products_description
                                           where products_id = :products_id
                                           and language_id = :language_id
                                         ");
        $Qproducts->bindValue(':products_id', (int)$reviews['products_id']);
        $Qproducts->bindValue(':language_id', (int)$_SESSION['languages_id']);
        $Qproducts->execute();

        $products = $Qproducts->fetch();

        $QreviewsText = $OSCOM_PDO->prepare("select r.reviews_read,
                                                   r.customers_name,
                                                   length(rd.reviews_text) as reviews_text_size
                                            from :table_reviews r,
                                                 :table_reviews_description rd
                                            where r.reviews_id = :reviews_id
                                            and r.reviews_id = rd.reviews_id
                                            ");
        $QreviewsText->bindValue(':reviews_id', (int)$reviews['reviews_id']);
        $QreviewsText->execute();

        $reviews_text = $QreviewsText->fetch();
        $QreviewsAverage = $OSCOM_PDO->prepare("select (avg(reviews_rating) / 5 * 100) as average_rating
                                                from :table_reviews
                                                where products_id = '" . (int)$reviews['products_id'] . "'
                                              ");
        $QreviewsAverage->bindValue(':products_id', (int)$reviews['reviews_id']);
        $QreviewsAverage->execute();

        $reviews_average = $QreviewsAverage->fetch();

        $review_info = array_merge((array)$reviews_text, (array)$reviews_average, (array)$products);
        $rInfo_array = array_merge((array)$reviews, (array)$review_info, (array)$products_image);
        $rInfo = new objectInfo($rInfo_array);
      }

      if (isset($rInfo) && is_object($rInfo) && ($reviews['reviews_id'] == $rInfo->reviews_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
          <td>
<?php
      if ($reviews['selected']) {
?>
            <input type="checkbox" name="selected[]" value="<?php echo $reviews['reviews_id']; ?>"  checked="checked"/>
<?php
      } else {
?>
            <input type="checkbox" name="selected[]" value="<?php echo $reviews['reviews_id']; ?>"/>
<?php
      }
?>
          </td>
          <td></td>
          <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $reviews['products_id'] . '&origin=' . 'reviews.php' . '?page=' . $_GET['page']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) . '</a>'; ?></td>
          <td class="dataTableContent"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . $reviews['products_image'], $reviews['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td>
          <td class="dataTableContent"><?php echo osc_get_products_name($reviews['products_id']); ?></td>
          <td class="dataTableContent" align="left"><?php echo  '<i>' .  osc_draw_stars($reviews['reviews_rating']) . '</i>'; ?></td>
          <td class="dataTableContent" align="center"><?php echo $rInfo->customers_name; ?></td>
          <td class="dataTableContent" align="center"><?php echo number_format($rInfo->average_rating, 2) . '%'; ?></td>
          <td class="dataTableContent" align="center"><?php echo number_format($rInfo->reviews_read); ?></td>
          <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($reviews['last_modified']); ?></td>
          <td class="dataTableContent" align="center">
<?php
      if ($reviews['status'] == '1') {
        echo '<a href="' . osc_href_link('reviews.php', 'action=setflag&flag=0&id=' . $reviews['reviews_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('reviews.php', 'action=setflag&flag=1&id=' . $reviews['reviews_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
          </td>
          <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('reviews.php', 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      if (isset($rInfo->reviews_id) && ($reviews['reviews_id'] == $rInfo->reviews_id)) {
        echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif');
      } else {
        echo '<a href="' . osc_href_link('reviews.php', 'page=' . $_GET['page'] . '&rID=' . $reviews['reviews_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>';
      }
?>
          </td>
        </tr>
<?php
    } //end while
?>
            </form><!-- end form delete all -->
            <tr>
              <td colspan="12" class="smallText" valign="top"><?php echo $reviews_split->display_count($reviews_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
<?php
  }
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>