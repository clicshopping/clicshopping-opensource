<?php
/**
 * index.php
 * @copyright Copyright 2008 - ClicShopping http://www.e-imaginis.com
 * @license GNU Public License V2.0
 * @version $Id:  
*/

// Chemin absolue du répertoire absolue
  $cwd=getcwd();

// Chemin relatif à la boutique
  chdir(''.$cwd.'/boutique/');

// Appel aux fonctions de OPC
  include('includes/application_top.php');

 if ($PHP_SELF == 'index.php') {

/**
 * Recuperation des donnees de la BD de gestion desc
 * meta tag 
**/  
 
    $Qsubmit = $OSCOM_PDO->prepare('select submit_id,
                                           language_id,
                                           submit_defaut_language_title,
                                           submit_defaut_language_keywords,
                                           submit_defaut_language_description
                                   from :table_submit_description
                                   where submit_id = :submit_id
                                   and language_id = :language_id
                                  ');
    $Qsubmit->bindInt(':submit_id',  '1');
    $Qsubmit->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
    $Qsubmit->execute();

    $submit = $Qsubmit->fetch();


// Definition de la variable de gestion des colonnes
    $tags_array = array();

//----------------------------------------------------------------
//         fichier index.php du catalog                         //
//---------------------------------------------------------------

     if (empty($submit['submit_defaut_language_title'])) {
       $tags_array['title']= TITLE; 
     } else {
       $tags_array['title']= clean_html_comments($submit['submit_defaut_language_title']); 
     }

     if (empty($submit['submit_defaut_language_description'])) {
       $tags_array['desc']= TITLE; 
     } else {
       $tags_array['desc']= clean_html_comments($submit['submit_defaut_language_description']); 
     }

     if (empty($submit['submit_defaut_language_keywords'])) {
       $tags_array['keywords']= TITLE; 
     } else {
       $tags_array['keywords']= clean_html_comments($submit['submit_defaut_language_keywords']); 
     }

     $OSCOM_Template->setTitle($tags_array['title'] . ', ' . $OSCOM_Template->getTitle());
     $OSCOM_Template->setDescription($tags_array['desc'] . ', ' . $OSCOM_Template->getDescription());
     $OSCOM_Template->setKeywords($tags_array['keywords'] . ', ' . $OSCOM_Template->getKeywords());
  }

// Récupération de la page d'introduction
  $page_manager->osc_display_page_manager('introduction');

  if ($page_manager->pages_introduction['pages_html_text'] != '') {

// Démmarrage de la bufferisation de sortie
    ob_start();
?>
<!DOCTYPE html>
<html <?php echo HTML_PARAMS; ?>>
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET;?>">
     <base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG;?>">
     <title><?php echo osc_output_string_protected($OSCOM_Template->getTitle());?></title>
     <meta name="Description" content="<?php echo osc_output_string_protected($OSCOM_Template->getDescription());?>" />
     <meta name="Keywords" content="<?php echo osc_output_string_protected($OSCOM_Template->getKeywords());?>" />
     <meta name="news_keywords" content="<?php echo osc_output_string_protected($OSCOM_Template->getNewsKeywords());?>" />
     <meta name="no-email-collection" content="<?php echo HTTP_SERVER; ?>" />
     <meta name="generator" content="ClicShopping" />
     <meta name="author" content="e-Imaginis & Innov Concept" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<![endif]-->

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo HTTP_SERVER . DIR_WS_HTTP_CATALOG . DIR_WS_SOURCES . 'javascript/bootstrap/less/bootstrap.less';?>" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" media="screen, print, projection" href="<?php echo $OSCOM_Template->getTemplategraphism();?>" />

    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

     <?php echo $OSCOM_Template->getBlocks('header_tags'); ?>

<?php
    if (!empty($page_manager->pages_introduction['page_time'])) {
?>
<meta http-equiv="refresh" content="<?php echo $page_manager->pages_introduction['page_time']; ?> ;url=<?php echo HTTP_SERVER . DIR_WS_HTTP_CATALOG; ?>">
<?php
    }
// http://ryanfait.com/sticky-footer/   
?>
<style>
* {margin: 0;}
html, body {height: 100%;}
</style>
</head>

    <div class="wrapperIntroduction">
      <div class="Pageintroduction"><?php echo $page_manager->pages_introduction['pages_html_text']; ?></div>
      <div class="push"></div>
    </div>
    <div class="PageintroductionAccessCatalog footerIntroduction"><a href="<?php echo HTTP_SERVER . DIR_WS_HTTP_CATALOG; ?>"><?php echo ACCESS_CATALOG; ?></a></div>

<?php
  require(DIR_WS_INCLUDES . 'application_bottom.php');
// Afficher le contenu du buffer
    ob_end_flush();
  } else { 
// Redirection vers la page d'acceuil si aucune page d'introduction existe
    osc_redirect(osc_href_link('index.php', '', 'NONSSL'));
  }
?>
  </html>
</body>