<?php
/**
 * general_addon.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */


//
// utilise par le flux rss.php
//

  function osc_replace_problem_characters($text) {
    $in[] = '@&(amp|#038);@i'; $out[] = '&';
    $in[] = '@&(#036);@i'; $out[] = '$';
    $in[] = '@&(quot);@i'; $out[] = '"';
    $in[] = '@&(#039);@i'; $out[] = '\'';
    $in[] = '@&(nbsp|#160);@i'; $out[] = ' ';
    $in[] = '@&(hellip|#8230);@i'; $out[] = '...';
    $in[] = '@&(copy|#169);@i'; $out[] = '(c)';
    $in[] = '@&(trade|#129);@i'; $out[] = '(tm)';
    $in[] = '@&(lt|#60);@i'; $out[] = '<';
    $in[] = '@&(gt|#62);@i'; $out[] = '>';
//    $in[] = '@&(laquo);@i'; $out[] = '«';
//    $in[] = '@&(raquo);@i'; $out[] = '»';
    $in[] = '@&(laquo);@i'; $out[] = '';
    $in[] = '@&(raquo);@i'; $out[] = '';
//    $in[] = '@&(deg);@i'; $out[] = '°';
    $in[] = '@&(mdash);@i'; $out[] = '—';
    $in[] = '@&(reg);@i'; $out[] = '®';
    $in[] = '@&(–);@i'; $out[] = '-';
    $text = preg_replace($in, $out, $text);
  return $text;
  }



// $document should contain an HTML document.
// This will remove HTML tags, javascript sections
// and white space. It will also convert some
// common HTML entities to their text equivalent. 
 
  function osc_strip_html_tags($str) {

    $search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                     "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
                     //"'([rn])[s]+'",                // Strip out white space
                     "'&(quot|#34);'i",                // Replace HTML entities
                     "'&(amp|#38);'i",
                     "'&(lt|#60);'i",
                     "'&(gt|#62);'i",
                     "'&(nbsp|#160);'i",
                     "'&(iexcl|#161);'i",
                     "'&(cent|#162);'i",
                     "'&(pound|#163);'i",
                     "'&(copy|#169);'i",
                     "'&#(d+);'e");                    // evaluate as php
    
    $replace = array ("",
                       "",
                       //"\1",
                       "\"",
                       "&",
                       "<",
                       ">",
                       " ",
                       chr(161),
                       chr(162),
                       chr(163),
                       chr(169),
                       "chr(\1)");
              
    return preg_replace($search, $replace, $str);
 }

/**
 * Display a product description for a product
 *
 * @param string $products_description, the  description of the product
 * @access public
 */
///

/**
 * Display a short description for a product description
 *
 * @param string $short_description, the short description of the product
 * @access public
 */
////
  function osc_display_short_description() {
    global  $delete_word, $OSCOM_Products;

    if (isset ($_GET['products_id'])) {
      $description = $OSCOM_Products->getProductsDescription();
      $products_short_description = MODULE_PRODUCTS_INFO_SHORT_DESCRIPTION_SHORT_DESCRIPTION;

      if ($products_short_description > 0) {
        $short_description = substr($description, $delete_word, $products_short_description);
        $short_description = osc_break_string(osc_output_string_protected($short_description), $products_short_description, '-<br />') . ((strlen($description) >= $products_short_description-1) ? ' ...' : '');
      }
    }

    return $short_description;
  }

/**
 * Display a short description for a listing product
 *
 * @param string $short_description, the short description of the product
 * @access public
 */
////
  function osc_display_short_description_module() {
    global  $description, $products_short_description, $delete_word;

    if ($products_short_description > 0) {
      $short_description = substr($description, $delete_word, $products_short_description);
      $short_description = osc_break_string(osc_output_string_protected($short_description), $products_short_description, '-<br />') . ((strlen($description) >= $products_short_description-1) ? ' ...' : '');
    }
    return $short_description;
  }


 /**
 * Return an image concerning the stock 
 * @param string $products_id
 * @return the $display_stock_values, the image value of stock
 * @access public
 */
////
  function osc_get_display_products_stock($products_id) {
    global $products_id;

    $display_products_stock  = osc_get_products_stock($products_id);
  
    if ($display_products_stock > STOCK_REORDER_LEVEL) {  
        $display_stock_values = osc_image_button('button_display_stock_good.png', IMAGE_BUTTON_DISPLAY_STOCK_GOOD, null,null,null, true);
    } elseif ($display_products_stock <= STOCK_REORDER_LEVEL && $display_products_stock > 0) {
        $display_stock_values = osc_image_button('button_display_stock_alert.png', IMAGE_BUTTON_DISPLAY_STOCK_ALERT, null,null,null, true);
    } else {
        $display_stock_values = osc_image_button('button_display_stock_out.png', IMAGE_BUTTON_DISPLAY_STOCK_OUT, null,null,null, true);
    } 
    return $display_stock_values;
  }

/**
 * Return a products button exhausted (ui or a gif button)
 * @param string $product_button_exhausted
 * @return $product_button_exhausted,the ui or gif button
 * @access public
 */
   function osc_product_button_exhausted() {
     if (PRE_ORDER_AUTORISATION == 'false') {
         $product_button_exhausted = osc_draw_button(IMAGE_BUTTON_STOCK_MARK_PRODUCT_EXHAUSTED, null, null, 'warning', null,'xs');
     }
    return $product_button_exhausted;
   }

/**
 * Display a description of manufacturer
 *
 * @param int $_GET['manufacturers_id']) the id of manufacturer
 * @param string $manufacturers['manufacturer_description'], The description of manufacturer
 * @access public
 */
 function osc_get_manufacturers_description($manufacturers_id) {

    global $OSCOM_PDO;

    if (empty($language)) $language_id = $_SESSION['languages_id'];

    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturer_description
                                            from :table_manufacturers_info
                                            where manufacturers_id = :manufacturers_id
                                            and languages_id = :languages_id
                                           ');
    $Qmanufacturers->bindInt(':manufacturers_id', (int)$_GET['manufacturers_id'] );
    $Qmanufacturers->bindInt(':languages_id', (int)$language_id  );

    $Qmanufacturers->execute();
    $manufacturers = $Qmanufacturers->fetch();

    return $manufacturers['manufacturer_description'];
  }

/**
 * Get the provider name of the client
 * $isp_provider_client the provider name
 * @access public
 */
  function osc_get_provider_name_client() {
    
    global $_SERVER;

    if (!empty($_SERVER["REMOTE_ADDR"]))  { //check ip from share internet
      $provider_client_ip = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
      $str = preg_split("/\./", $provider_client_ip);
      $i = count($str);
      $x = $i - 1;
      $n = $i - 2;
      $isp_provider_client = $str[$n] . "." . $str[$x];
    }
    return $isp_provider_client;
  }

/**
 * Controle autorisation au client de modifier son adresse par defaut 
 */
  
  function  osc_count_customers_modify_address_default($id = '', $check_session = true) {
    
    global $OSCOM_Customer, $OSCOM_PDO;
    
    if (is_numeric($id) == false) {
      if ($OSCOM_Customer->isLoggedOn()) {
        $id = $OSCOM_Customer->getID();
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( !$OSCOM_Customer->isLoggedOn() || ($id != $OSCOM_Customer->getID()) ) {
        return 0;
      }
    }

    if (ACCOUNT_MODIFY_ADRESS_DEFAULT_PRO == 'true' || $OSCOM_Customer->getCustomersGroupID() == '0' ) {

      $QcustomersModifyAddressDefault = $OSCOM_PDO->prepare('select customers_modify_address_default 
                                                             from :table_customers
                                                             where customers_id = :customers_id
                                                            ');
      $QcustomersModifyAddressDefault->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );

      $QcustomersModifyAddressDefault->execute();
      $customers_modify_address_default = $QcustomersModifyAddressDefault->fetch();

    }

    return $customers_modify_address_default['customers_modify_address_default'];
  }

/**
 * Get getallheaders
 *
 * @param string $headers
 * @return $headers
 * @access public
 */
// RSS function
  if(!function_exists('getallheaders')) {
  function getallheaders() {
    settype($headers,'array');
     foreach($_SERVER as $h => $v) {
      if (preg_match('#HTTP_(.+)#',$h,$hp)) {
       $headers[$hp[1]] = $v;
      }
    }
   return $headers;
    }
   } 
   
/**
 * PGet the tracking number
 *
 * @param string $tracking, $_SESSION['languages_id']
 * @return string tracking_url, the url of the tracking
 * @access public
 */
 function osc_tracking_link($tracking_url) {

   global $tracking, $OSCOM_PDO;

    $QordersTracking = $OSCOM_PDO->prepare('select orders_status_tracking_id,
                                               orders_status_tracking_link
                                            from :table_orders_status_tracking
                                            where orders_status_tracking_id = :orders_status_tracking_id
                                            and language_id = :language_id
                                          ');
    $QordersTracking->bindInt(':orders_status_tracking_id', (int)$tracking['orders_status_tracking_id'] );
    $QordersTracking->bindInt(':language_id', $_SESSION['languages_id'] );

    $QordersTracking->execute();
    $orders_tracking = $QordersTracking->fetch();


   if (empty($orders_tracking['orders_status_tracking_link']) || (empty($tracking['orders_tracking_number']) )) {
     $tracking_url = ' (<a href="http://www.track-trace.com/" target="_blank">http://www.track-trace.com/</a>)';
   } else {
     $tracking_url = ' (<a href="'.$orders_tracking['orders_status_tracking_link'] .  $tracking['orders_tracking_number'] .'" target="_blank">'.$orders_tracking['orders_status_tracking_link'] .  $tracking['orders_tracking_number'].'</a>)';;
   }
   return $tracking_url; 
 } 



/**
 * Get the general condition to include in the order
 *
 * @param string $customer_group, $_SESSION['languages_id']
 * @return string page_manager_general_condition, the text of the general condition of sales
 * @access public
*/
  function osc_page_manager_general_condition() {
    global $OSCOM_PDO, $OSCOM_Customer;

    $QpageManagerGeneralGroup = $OSCOM_PDO->prepare('select pages_id,
                                                              customers_group_id
                                                       from :table_pages_manager
                                                       where customers_group_id = 99
                                                       and page_type = 4
                                                       and page_general_condition = 1
                                                       and status = 1
                                                     ');
    $QpageManagerGeneralGroup->bindInt(':customers_group_id', $OSCOM_Customer->getCustomersGroupID() );
    $QpageManagerGeneralGroup->execute();

    if ($QpageManagerGeneralGroup->valueInt('customers_group_id') == 99) {
      $QpageManagerGeneralCondition = $OSCOM_PDO->prepare('select pmd.pages_html_text
                                                           from :table_pages_manager pm,
                                                                :table_pages_manager_description pmd
                                                           where  pm.customers_group_id = 99
                                                           and pm.page_general_condition = 1
                                                           and pm.page_type = 4
                                                           and pm.pages_id = :pages_id
                                                           and pmd.language_id = :language_id
                                                           and pmd.pages_id = pm.pages_id
                                                           and pm.status = 1
                                                           limit 1
                                                          ');
      $QpageManagerGeneralCondition->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QpageManagerGeneralCondition->bindInt(':pages_id', $QpageManagerGeneralGroup->valueInt('pages_id')  );

      $QpageManagerGeneralCondition->execute();

    } else {

      $QpageManagerGeneralCondition = $OSCOM_PDO->prepare('select pmd.pages_html_text
                                                             from :table_pages_manager pm,
                                                                  :table_pages_manager_description pmd
                                                             where  pm.customers_group_id = :customers_group_id
                                                             and pm.page_general_condition = 1
                                                             and pm.page_type = 4
                                                             and pm.pages_id = :pages_id
                                                             and pmd.language_id = :language_id
                                                             and pmd.pages_id = pm.pages_id
                                                             and pm.status = 1
                                                             limit 1
                                                            ');
      $QpageManagerGeneralCondition->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QpageManagerGeneralCondition->bindInt(':customers_group_id', $OSCOM_Customer->getCustomersGroupID() );

      $QpageManagerGeneralCondition->execute();
    }

    return $QpageManagerGeneralCondition->value('pages_html_text');
  }

/**
 * Get minimise url of twiter
 *
 * @param string $url
 * @return string tinyurl, the minimise url 
 * @access public
 */
  function getTinyUrl($url) {
    $tinyurl = @file_get_contents("http://tinyurl.com/api-create.php?url=".$url);
    if (!$tinyurl) {
      return $url;
   }
    return $tinyurl;
  }

/**
 * Controle autorisation d'ajouter une adresse selon la fiche client 
 */
   
  function  osc_count_customers_add_address($id = '', $check_session = true) {
    global $OSCOM_Customer, $OSCOM_PDO;

    if (is_numeric($id) == false) {
      if ($OSCOM_Customer->isLoggedOn()) {
        $id = $OSCOM_Customer->getID();
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( !$OSCOM_Customer->isLoggedOn() || ($id != $OSCOM_Customer->getID()) ) {
        return 0;
      }
    }

    if ( $OSCOM_Customer->getCustomersGroupID() == 0 || ACCOUNT_ADRESS_BOOK_PRO == 'true') {
      $Qaddresses = $OSCOM_PDO->prepare('select customers_add_address 
                                          from :table_customers
                                          where customers_id = :customers_id
                                        ');
      $Qaddresses->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );

      $Qaddresses->execute();
      $customers_add_address = $Qaddresses->fetch();
    }

    return $customers_add_address['customers_add_address'];
  }


/** Controle autorisation au client B2B de modifier ses informations sur la societe 
 *
 */
  
  function  osc_count_customers_modify_company($id = '', $check_session = true) {
    global $OSCOM_Customer, $OSCOM_PDO;

    if (is_numeric($id) == false) {
      if ($OSCOM_Customer->isLoggedOn()) {
        $id = $OSCOM_Customer->getID();
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( !$OSCOM_Customer->isLoggedOn() || ($id != $OSCOM_Customer->getID()) ) {
        return 0;
      }
    }

    $QcustomersModifyCompany = $OSCOM_PDO->prepare('select customers_modify_company 
                                                     from :table_customers
                                                      where customers_id = :customers_id
                                                    ');
    $QcustomersModifyCompany->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );

    $QcustomersModifyCompany->execute();
    $customers_modify_company = $QcustomersModifyCompany->fetch();

    return $customers_modify_company['customers_modify_company'];
  }


/**
 * Count the number of attributes on product
 *
 * @param string 
 * @return string $products_attributes['total'], total of attributes
 * @access public
 */
  function osc_count_products_attributes() { 
    global $OSCOM_PDO;

   if (empty($language)) $language_id = $_SESSION['languages_id'];

    $QproductsAttributes = $OSCOM_PDO->prepare('select  count(*) as total
                                                from :table_products_options popt, 
                                                     :table_products_attributes patrib 
                                                where patrib.products_id = :products_id
                                                and patrib.options_id = popt.products_options_id 
                                                and popt.language_id = :language_id
                                               ');
    $QproductsAttributes->bindInt(':products_id', (int)$_GET['products_id'] );
    $QproductsAttributes->bindInt(':language_id', (int)$language_id );
    
    $QproductsAttributes->execute();

    $products_attributes = $QproductsAttributes->fetch();

    return $products_attributes['total'];
  }

/**
 * Display the language flag
 *
 * @param string
 * @return string $languages_string, flag language
 * @access public
 */
  function osc_language_flag() {
    global  $PHP_SELF, $lng, $request_type;

    // language
    if (substr($PHP_SELF, 0, 8) != 'checkout') {
      if (!isset($lng) || (isset($lng) && !is_object($lng))) {
        include(DIR_WS_CLASSES . 'language.php');
        $lng = new language;
      }

      if (count($lng->catalog_languages) > 1) {
        $languages_string = '';
        reset($lng->catalog_languages);
        foreach( $lng->catalog_languages as $key => $value ) {
          if ( $value['directory'] == $_SESSION['language'] ) {
            $current_lang_key = $key;
            break;
          }
        }
        reset($lng->catalog_languages);
        $home_page_redirect = defined( 'USU5_HOME_PAGE_REDIRECT' ) && ( USU5_HOME_PAGE_REDIRECT == 'true' ) ? true : false;
        while (list($key, $value) = each($lng->catalog_languages)) {
          if ( defined( 'USU5_MULTI_LANGUAGE_SEO_SUPPORT' ) && ( USU5_MULTI_LANGUAGE_SEO_SUPPORT == 'true' ) && defined( 'USU5_ENABLED' ) && ( USU5_ENABLED == 'true' ) ) {
            if( false === ( $_SESSION['language'] == $value['directory'] ) ) { // we don't want to show a link to the currently loaded language
              if ( false !== $home_page_redirect ) { // If we are using the root site redirect
                $link = str_replace( array( 'index.php', '/' . $current_lang_key ), '', osc_href_link( 'index.php' ) );
              } else {
                $link = str_replace('/' . $current_lang_key, '', osc_href_link( 'index.php' ) );
              }
              if ( $key !== DEFAULT_LANGUAGE ) { // if it is not the default language we are dealing with
                if ( false === strpos( $link, '.php' ) && ( false !== $home_page_redirect ) ) {
                  $link_array = explode( '?', $link );
                  $qs = array_key_exists( 1, $link_array ) ? ( '?' . $link_array[1] ) : '';
                  $link = $link_array[0] . 'index.php' . '/' . $key . $qs;
                } else {
                  $link_array = explode( '?', $link );
                  $qs = array_key_exists( 1, $link_array ) ? ( '?' . $link_array[1] ) : '';
                  $link = str_replace( '.php', '.php/' . $key . $qs, $link );
                }
              }
              // USU5  shows the language link and image
              $languages_string .= '<a href="' . $link . '">' . osc_image(DIR_WS_ICONS . 'languages/'  . $value['image'], $value['name']) . '</a>&nbsp;';
            }
          } else { // Just do the standard osCommerce links
            // Standard osCommerce shows the language link and image
            $languages_string .= '<a href="' . osc_href_link($PHP_SELF, osc_get_all_get_params(array('language', 'currency')) . 'language=' . $key, $request_type) . '">' . osc_image(DIR_WS_ICONS . 'languages/'  . $value['image'], $value['name']) . '</a>&nbsp;';
          }
        }
      }
    } // end language

    return $languages_string;
  }


/**
 * Delete cached files by their key ID for module and configuration.php
 *
 * @param string $key The key ID of the cached files to delete
 * @access public
 */

  function osc_cache_clear($key) {

    $directory_cache = DIR_FS_CACHE2;
    if ( is_writable($directory_cache) ) {
      $key_length = strlen($key);

      $d = dir($directory_cache);

      while ( ($entry = $d->read()) !== false ) {
        if ( (strlen($entry) >= $key_length) && (substr($entry, 0, $key_length) == $key) ) {
          @unlink($directory_cache . $entry);
        }
      }

      $d->close();
    }
  }


/**
 * Reset cached files by their key  for module and configuration.php
 *
 * @param string $key The key ID of the cached files to delete
 * @access public
 */

  function osc_cache_reset($key) {

    $languages = osc_get_languages();
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      $language_id = $languages[$i]['id'];
      osc_cache_clear($key . $language_id  .'.cache');
    }
  }
  

/**
 * Extract email to send more one email
 * bug with SEND_EXTRA_ORDER_EMAILS_TO
 *
 * @param string : email
 * @return string $emails, email
 * @access public
 */
  function extract_email_address($string) {
    foreach(preg_split('/\s/', $string) as $token) {
      $email = filter_var(filter_var($token, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
      if ($email !== false) {
        $emails[] = $email;
      }
    }
    return $emails;
  }
