<?php
/**
 * discount_coupons.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Coupons / réduction');
define('TITLE_DISCOUNT_COUPONS','Code du coupon');
define('TITLE_DISCOUNT_AMOUNT','Remise sur le coupon');
define('TITLE_DISCOUNT_DATE','Date début et date de fin');
define('TITLE_DISCOUNT_ORDER','Type et montant de la commande');
define('TITLE_DISCOUNT_USE','Nombre d\'utilisation du coupon');


define('TEXT_HEADING_DISCOUNT_COUPONS_ID', 'Code du coupon');
define('TEXT_HEADING_DISCOUNT_CREATE_ACCOUNT','Compte B2C'); 	
define('TEXT_HEADING_DISCOUNT_AMOUNT', 'Remise');
define('TEXT_HEADING_DISCOUNT_TYPE', 'Type remise');
define('TEXT_HEADING_DISCOUNT_DATE_START', 'Début');
define('TEXT_HEADING_DISCOUNT_DATE_END', 'Fin');
define('TEXT_HEADING_DISCOUNT_MAX_USE', 'Nbr utilisation');
define('TEXT_HEADING_DISCOUNT_MIN_ORDER', 'Commande minimale');
define('TEXT_HEADING_DISCOUNT_NUMBER_AVAILABLE', 'Disponibilité');
define('TEXT_HEADING_DISCOUNT_CUSTOMER_GROUP','Compte B2B');
define('TABLE_HEADING_ACTION','Actions');

define('TITLE_HELP_DISCOUNT_PRICE', 'Notes sur la gestion des coupons');
define('TEXT_HELP_DISCOUNT_PRICE','<li>Le code du coupon peut être généré aléatoirement si vous laissez le champs vide.</li>
<li>Pour utiliser indéfiniment certaines fonctions du coupon, veuillez insérer 0 pour le montant minimal de commande, nombre d\'utilisation du coupon et nombre disponible</li>.
<li>Pour les modes de calcul, veuillez vous reporter à la section du menu Configuration/Modules/Total Commandes.</li>
<li>Avant de créer un nouveau coupon envoyé à tous les utilisateurs créant un nouveau compte, veuillez supprimer l\'ancien coupon. A défaut, celui-ci ne sera pas enregistré, et le coupon conservera sa valeur initiale.</li>
');

define('TEXT_DISCOUNT_COUPONS_ID', 'Code du coupon : ');
define('TEXT_DISCOUNT_COUPONS_DESCRIPTION', 'Description : ');
define('TEXT_DISCOUNT_COUPONS_AMOUNT', 'Montant de la remise : ');
define('TEXT_DISCOUNT_COUPONS_TYPE', 'Type de remise : ');
define('TEXT_DISCOUNT_COUPONS_DATE_START', 'Date de départ : ');
define('TEXT_DISCOUNT_COUPONS_DATE_END', 'Date de fin : ');
define('TEXT_DISCOUNT_COUPONS_MAX_USE', 'Nombre d\'utilisation par client: ');
define('TEXT_DISCOUNT_COUPONS_MIN_ORDER', 'Montant minimal de la commande  : ');
define('TEXT_DISCOUNT_COUPONS_MIN_ORDER_TYPE', 'Type sur le montant minimal de la commande : ');
define('TEXT_DISCOUNT_COUPONS_NUMBER_AVAILABLE', 'Nombre total disponible aux clients: ');

define('TEXT_DISCOUNT_TWITTER','Souhaitez-vous publier ce coupon sur twitter ?');
define('TEXT_NEW_DISCOUNT_TWITTER','Bénéficier d\'un coupon rabais sur nos produits code : ');
define('TEXT_DISCOUNT_DATE_END', ' jusqu\'au ');
define('TEXT_TWITTER_AMOUNT', ' : Montant / Remise : ');

define('TEXT_DISCOUNT_COUPONS_CREATE_ACCOUNT','Souhaitez vous offrir ce coupon quand un client crée un compte ?');
define('TEXT_DISCOUNT_COUPONS_WARNING','<font color="#FF0000"><strong>Attention !</stong></font> Vous avez déjà défini un coupon offert lors de la création d\'un compte client. Si vous sélectionnez la case à cocher, ce nouveau coupon ne remplacera pas l\'ancien. Veuillez actualiser ou supprimer
l\'ancien coupon avant de procéder à l\'insertion d\'un nouveau coupon lors de la création du compte par le  client');
define('TEXT_DISCOUNT_CUSTOMERS_GROUP','Souhaitez vous offrir ce coupon quand un client crée un compte (mode b2b) ? ');

define('TEXT_DISPLAY_NUMBER_OF_DISCOUNT_COUPONS', 'Produits');
define('TEXT_DISPLAY_UNLIMITED', 'illimitée' );
define('TEXT_DISPLAY_SHIPPING_DISCOUNT', 'off shipping');

define('TEXT_INFO_DISCOUNT_AMOUNT_HINT', '<font color="#FF0000"><strong>   Note :</stong></font> Pour un pourcentage de remise sur la commande, veuillez insérer une décimale.  Ex : 0.10 pour 10%');
define('TEXT_INFO_DISCOUNT_AMOUNT', 'Remise : ');
define('TEXT_INFO_DISCOUNT_TYPE', 'Type remise : ');
define('TEXT_INFO_DATE_START', 'Début : ');
define('TEXT_INFO_DATE_END', 'Fin : ');
define('TEXT_INFO_MAX_USE', 'Utilisation max : ');
define('TEXT_INFO_MIN_ORDER', 'Commande minimale : ');
define('TEXT_INFO_MIN_ORDER_TYPE', 'Min Order Type : ');
define('TEXT_INFO_NUMBER_AVAILABLE', 'Disponibilité : ');

define('TEXT_INFO_HEADING_DELETE_DISCOUNT_COUPONS', 'Souhaitez vous supprimer ce coupon ?');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de supprimer ce coupon ?');

define('ERROR_DISCOUNT_COUPONS_NO_AMOUNT', 'Veuillez insérer un montant pour le coupon.' );

//exclusions
define('IMAGE_PRODUCT_EXCLUSIONS', 'Exclure des produits');
define('IMAGE_MANUFACTURER_EXCLUSIONS', 'Exclure des marques');
define('IMAGE_CATEGORY_EXCLUSIONS', 'Exclure des catégories');
define('IMAGE_CUSTOMER_EXCLUSIONS', 'Exclure des clients');
define('IMAGE_SHIPPING_ZONE_EXCLUSIONS', 'Exclure des zones d\'expédition');
//end exclusions

define('ACTIVATE_MODULE_DISCOUNT_COUPON','<br /><br /><font color="#FF0000"><strong>Attention ! Veuillez activer le module Coupon de réduction dans Configuration / Modules / Total Commande avant d\'insérer un coupon</strong></font>');

define('IMAGE_NEW_COUPON', 'Nouveau coupon');
?>
