<?php
/*
 * account_password.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  echo osc_draw_form('account_password', osc_href_link('account_password.php', '', 'SSL'), 'post', '', true) . osc_draw_hidden_field('action', 'process');

  if ( $OSCOM_MessageStack->exists('account_password') ) {
    echo $OSCOM_MessageStack->get('account_password');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Password   -----  
// ---------------------- 
?>
<div class="contentContainer">
  <div>
    <legend><h4><span class="text-warning pull-right"><?php echo FORM_REQUIRED_INFORMATION; ?></span></h4></legend>
  </div>
  <div class="contentText">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="password_current"><?php echo ENTRY_PASSWORD_CURRENT; ?></label>
        <div class="controls col-sm-6">
          <?php echo osc_draw_input_field('password_current', null, 'id="password_current" autofocus="autofocus" required aria-required="true" placeholder="' . ENTRY_PASSWORD_CURRENT . '"'); ?>
          <?php if (osc_not_null(ENTRY_PASSWORD_CURRENT_TEXT)) echo '<span class="help-block">' . ENTRY_PASSWORD_CURRENT_TEXT . '</span>'; ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="password_new"><?php echo ENTRY_PASSWORD_NEW; ?></label>
        <div class="controls col-sm-6">
          <?php echo osc_draw_input_field('password_new', null, 'id="password_new" required aria-required="true" placeholder="' . ENTRY_PASSWORD_NEW . '"'); ?>
          <?php if (osc_not_null(ENTRY_PASSWORD_NEW_TEXT)) echo '<span class="help-block">' . ENTRY_PASSWORD_NEW_TEXT . '</span>'; ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="password_confirmation"><?php echo ENTRY_PASSWORD_CONFIRMATION; ?></label>
        <div class="controls col-sm-6">
          <?php echo osc_draw_input_field('password_confirmation', null, 'id="password_confirmation" required aria-required="true" placeholder="' . ENTRY_PASSWORD_CONFIRMATION . '"'); ?>
          <?php if (osc_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT)) echo '<span class="help-block">' . ENTRY_PASSWORD_CONFIRMATION_TEXT . '</span>'; ?>
        </div>
      </div>
    </div>
  </div>
<?php
// ---------------------- 
// --- button   -----  
// ---------------------- 
?>
  <div class="buttonSet row">
    <div class="col-xs-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('account.php', '', 'SSL'), 'primary');  ?></div>
    <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success');  ?></div>

  </div>
</div>

</form>
<?php   require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>