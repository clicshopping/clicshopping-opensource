<?php
/*
 * google_sitemap_products.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $page_manager_array = array();

    $QpageManager = $OSCOM_PDO->prepare('select pages_id,
                                         coalesce(NULLIF(last_modified, :last_modified),
                                                         date_added) as last_modified
                                          from :table_pages_manager
                                          where status = :status
                                          and customers_group_id = :customers_group_id
                                          and pages_id <> :pages_id
                                          and pages_id <> :pages_id1
                                          and pages_id <> :pages_id2
                                          and pages_id <> :pages_id3
                                          order by last_modified DESC
                                         ');

    $QpageManager->bindValue(':last_modified', '');
    $QpageManager->bindValue(':status', '1');
    $QpageManager->bindValue(':customers_group_id', '0');
    $QpageManager->bindInt(':pages_id', '5');
    $QpageManager->bindInt(':pages_id1', '4');
    $QpageManager->bindInt(':pages_id2', '3');
    $QpageManager->bindInt(':pages_id3', '2');
    $QpageManager->execute();

    while ($page_manager = $QpageManager->fetch() ) {
      $page_manager_array[$page_manager['pages_id']]['loc'] = osc_href_link('page_manager.php', 'pages_id=' . (int)$page_manager['pages_id'], 'NONSSL', false);
      $page_manager_array[$page_manager['pages_id']]['lastmod'] = $page_manager['last_modified'];
      $page_manager_array[$page_manager['pages_id']]['changefreq'] = 'weekly';
      $page_manager_array[$page_manager['pages_id']]['priority'] = '0.5';
    }

    foreach ($page_manager_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
