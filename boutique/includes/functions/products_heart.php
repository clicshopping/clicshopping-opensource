<?php
/**
 * products_heart.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: products_heart.php 
*/
////
// Sets the status of a product_heart product
  function osc_set_products_heart_status($products_heart_id, $status) {
     global $OSCOM_PDO;
      if ($status == '1') {

        return $OSCOM_PDO->save('products_heart', ['status' => 1,
                                                  'date_status_change' => 'now()',
                                                  'scheduled_date' => 'null'
                                                  ],
                                                  ['products_heart_id' => (int)$products_heart_id]
                              );

      } elseif ($status == '0') {

        return $OSCOM_PDO->save('products_heart', ['status' => 0,
                                                  'date_status_change' => 'now()',
                                                  'scheduled_date' => 'null'
                                                  ],
                                                  ['products_heart_id' => (int)$products_heart_id]
                               );
      } else {
        return -1;
      }
    }

// Auto activate scheduled products on favorites
    function osc_scheduled_products_heart() {
      global $OSCOM_PDO;

      $QFavorites = $OSCOM_PDO->query('select products_heart_id
                                      from :table_products_heart
                                      where scheduled_date is not null
                                      and scheduled_date <= now()
                                      and status != 1
                                     ');

      $QFavorites->execute;
      $QFavorites->execute;

      if ($QFavorites->fetch() !== false) {
        do {
          static::setFavoritesStatus($QFavorites->valueInt('products_heart_id'), 1);
        } while($QFavorites->fetch());
      }
    }

////
// Auto expire products on products heart
   function osc_expire_products_heart() {
      global $OSCOM_PDO;

      $QFavorites = $OSCOM_PDO->query('select products_heart_id
                                      from :table_products_heart
                                      where status = 1
                                      and expires_date is not null
                                      and now() >= expires_date
                                    ');

      $QFavorites->execute;

      if ($QFavorites->fetch() !== false) {
        do {
          static::setFavoritesStatus($QFavorites->valueInt('products_heart_id'), 0);
        } while ($QFavorites->fetch());
      }
    }
 
