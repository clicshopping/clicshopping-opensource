<?php
/*
 * address_book_process.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('address_book_process'));

  if (isset($_GET['action']) && ($_GET['action'] == 'deleteconfirm') && isset($_GET['delete']) && is_numeric($_GET['delete']) && isset($_GET['formid']) && ($_GET['formid'] == md5($_SESSION['sessiontoken']))) {

    if ((int)$_GET['delete'] == $OSCOM_Customer->get('default_address_id') ) {
      $OSCOM_MessageStack->addError('addressbook', WARNING_PRIMARY_ADDRESS_DELETION);
    } else {

      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_address_book
                                      where address_book_id = :address_book_id
                                      and customers_id = :customers_id
                                    ');
      $Qdelete->bindInt(':address_book_id', (int)$_GET['delete']);
      $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qdelete->execute();

      $OSCOM_MessageStack->addSuccess('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_DELETED);
    }

    osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
  }

// error checking when updating or adding an entry
  $process = false;

  if (isset($_POST['action']) && (($_POST['action'] == 'process') || ($_POST['action'] == 'update')) && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $process = true;
    $error = false;

    if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
     $gender = osc_db_prepare_input($_POST['gender']);
    }
    
    if (((ACCOUNT_COMPANY == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_COMPANY_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
      $company = osc_db_prepare_input($_POST['company']);
    }

    $firstname = osc_db_prepare_input($_POST['firstname']);
    $lastname = osc_db_prepare_input($_POST['lastname']);
    $telephone = osc_db_prepare_input($_POST['telephone']);
    $cellular_phone = osc_db_prepare_input($_POST['cellular_phone']);
    $fax = osc_db_prepare_input($_POST['fax']);

    $street_address = osc_db_prepare_input($_POST['street_address']);

    if (((ACCOUNT_SUBURB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_SUBURB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
      $suburb = osc_db_prepare_input($_POST['suburb']);
    }

    $postcode = osc_db_prepare_input($_POST['postcode']);
    $city = osc_db_prepare_input($_POST['city']);
    $country = osc_db_prepare_input($_POST['country']);

    if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_TELEPHONE_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_TELEPHONE_PRO_MIN_LENGTH > 0))) {
      $telephone = osc_db_prepare_input($_POST['telephone']);
    }

    if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ACCOUNT_CELLULAR_PHONE =='true')) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ACCOUNT_CELLULAR_PHONE_PRO =='true'))) {
      $cellular_phone = osc_db_prepare_input($_POST['cellular_phone']);   
    }

    if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ACCOUNT_FAX =='true')) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ACCOUNT_FAX_PRO =='true'))) {
      $fax = osc_db_prepare_input($_POST['fax']);
    }

    if (((ACCOUNT_STATE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_STATE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
      if (isset($_POST['zone_id'])) {
        $zone_id = osc_db_prepare_input($_POST['zone_id']);
      } else {
        $zone_id = false;
      }
      $state = osc_db_prepare_input($_POST['state']);
    }

// Clients B2C et B2B : Controle selection de la civilite
    if ((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      if ( ($gender != 'm') && ($gender != 'f') ) {
        $error = true;

        $OSCOM_MessageStack->addError('addressbook', ENTRY_GENDER_ERROR);
      }
    } else if ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      if ( ($gender != 'm') && ($gender != 'f') ) {
        $error = true;

        $OSCOM_MessageStack->addError('addressbook', ENTRY_GENDER_ERROR_PRO);
      }
    }


// Clients B2C et B2B : Controle entree du prenom
    if ((strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_FIRST_NAME_ERROR);
    } else if ((strlen($firstname) < ENTRY_FIRST_NAME_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_FIRST_NAME_ERROR_PRO);
    }

// Clients B2C et B2B : Controle entree du nom de famille
    if ((strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      $error = true;

      $OSCOM_MessageStack->add('addressbook', ENTRY_LAST_NAME_ERROR);
    } else if ((strlen($lastname) < ENTRY_LAST_NAME_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_LAST_NAME_ERROR_PRO);
    }

// Clients B2C et B2B : Controle entree adresse
    if ((strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      $error = true;

      $OSCOM_MessageStack->add('addressbook', ENTRY_STREET_ADDRESS_ERROR);
    } else if ((strlen($street_address) < ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_STREET_ADDRESS_ERROR_PRO);
    }

// Clients B2C et B2B : Controle entree code postal
    if ((strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      $error = true;

      $OSCOM_MessageStack->add('addressbook', ENTRY_POST_CODE_ERROR);
    } else if ((strlen($postcode) < ENTRY_POSTCODE_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_POST_CODE_ERROR_PRO);
    }

// Clients B2C et B2B : Controle entree de la ville
    if ((strlen($city) < ENTRY_CITY_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      $error = true;

      $OSCOM_MessageStack->add('addressbook', ENTRY_CITY_ERROR);
    } else if ((strlen($city) < ENTRY_CITY_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_CITY_ERROR_PRO);
    }

// Clients B2C et B2B : Controle de la selection du pays
    if ((!is_numeric($country)) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
      $error = true;

      $OSCOM_MessageStack->add('addressbook', ENTRY_COUNTRY_ERROR);
    } else if ((!is_numeric($country)) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
      $error = true;

      $OSCOM_MessageStack->addError('addressbook', ENTRY_COUNTRY_ERROR_PRO);
    }

// Clients B2C et B2B : Controle entree du departement
    if (((ACCOUNT_STATE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_STATE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
      $zone_id = 0;

      $Qcheck = $OSCOM_PDO->prepare('select zone_country_id
                                     from :table_zones
                                     where zone_country_id = :zone_country_id
                                     limit 1');
      $Qcheck->bindInt(':zone_country_id', (int)$country);
      $Qcheck->execute();

      $entry_state_has_zones = ($Qcheck->fetch() !== false);

      if ($entry_state_has_zones == true) {

       $Qzone = $OSCOM_PDO->prepare('select distinct zone_id 
                                     from :table_zones
                                     where zone_country_id = :zone_country_id
                                     and (zone_name = :zone_name or zone_code = :zone_code)
                                     ');
        $Qzone->bindInt(':zone_country_id', $country);
        $Qzone->bindValue(':zone_name', $state);
        $Qzone->bindValue(':zone_code', $state);
        $Qzone->execute();

        $result = $Qzone->fetchAll();

        if ( count($result) === 1 ) {
          $zone_id = (int)$result[0]['zone_id'];
        } else {
          $error = true;
          if ($OSCOM_Customer->getCustomersGroupID() == 0) {
            $OSCOM_MessageStack->addError('addressbook', ENTRY_STATE_ERROR_SELECT);
          } else if ($OSCOM_Customer->getCustomersGroupID() != 0) {
            $OSCOM_MessageStack->addError('addressbook', ENTRY_STATE_ERROR_SELECT_PRO);
          }
        } // end else
      } // end $entry_state_has_zones
    } else {
      if ((strlen($state) < ENTRY_STATE_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
        $error = true;

        $OSCOM_MessageStack->addError('addressbook', ENTRY_STATE_ERROR);
      } else if ((strlen($state) < ENTRY_STATE_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        $error = true;

        $OSCOM_MessageStack->addError('addressbook', ENTRY_STATE_ERROR_PRO);
      }
    } // end else


    if ($error == false) {

      $sql_data_array = array('entry_firstname' => $firstname,
                              'entry_lastname' => $lastname,
                              'entry_street_address' => $street_address,
                              'entry_postcode' => $postcode,
                              'entry_city' => $city,
                              'entry_country_id' => (int)$country
                              );

      if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
        $sql_data_array['entry_gender'] = $gender;
      }
      
      if (((ACCOUNT_COMPANY == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_COMPANY_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
        $sql_data_array['entry_company'] = $company;
      }

      if (((ACCOUNT_SUBURB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_SUBURB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
        $sql_data_array['entry_suburb'] = $suburb;
      }

      if (((ACCOUNT_STATE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_STATE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
        if ($zone_id > 0) {
          $sql_data_array['entry_zone_id'] = (int)$zone_id;
          $sql_data_array['entry_state'] = '';
        } else {
          $sql_data_array['entry_zone_id'] = '0';
          $sql_data_array['entry_state'] = $state;
        }
      }
// update address
      if ($_POST['action'] == 'update') {


        $Qcheck = $OSCOM_PDO->prepare('select address_book_id 
                                      from :table_address_book 
                                      where address_book_id = :address_book_id 
                                      and customers_id = :customers_id');
        $Qcheck->bindInt(':address_book_id', $_GET['edit']);
        $Qcheck->bindInt(':customers_id', $OSCOM_Customer->getID());
        $Qcheck->execute();

        if ( $Qcheck->fetch() !== false ) {
          $OSCOM_PDO->save('address_book', $sql_data_array, array('address_book_id' => $_GET['edit'],
                                                                  'customers_id' => $OSCOM_Customer->getID())
                           );

// register session variables
          if ( (isset($_POST['primary']) && ($_POST['primary'] == 'on')) || ($_GET['edit'] == $OSCOM_Customer->getDefaultAddressID()) ) {
            $OSCOM_Customer->setCountryID($country);
            $OSCOM_Customer->setZoneID(($zone_id > 0) ? (int)$zone_id : '0');

            $OSCOM_Customer->setDefaultAddressID($_GET['edit']);
          }

          if ($_POST['shopping'] != 1) {
            $sql_data_array = array('customers_firstname' => $firstname,
                                    'customers_lastname' => $lastname);

            if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
              $sql_data_array['customers_gender'] = $gender;
            }

          } else {
            $sql_data_array = array('customers_firstname' => $firstname,
                                    'customers_lastname' => $lastname,
                                    'customers_telephone' => $telephone
                                   );

            if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
              $sql_data_array['customers_gender'] = $gender;
            }

            if (((ACCOUNT_CELLULAR_PHONE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_CELLULAR_PHONE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
              $sql_data_array['customers_cellular_phone'] = $cellular_phone;
            }

            if (((ACCOUNT_FAX == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_FAX_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
              $sql_data_array['customers_fax'] = $fax;
            }
          }

          $OSCOM_PDO->save('customers', $sql_data_array, array('customers_id' => $OSCOM_Customer->getID()));

//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_catalog_address_book_process.php');
          }
//***************************************
// End odoo web service
//***************************************
        } // end $Qcheck->fetch

      } else {
// create address
        $sql_data_array['customers_id'] = (int)$OSCOM_Customer->getID();

        $OSCOM_PDO->save('address_book', $sql_data_array);

        $new_address_book_id = $OSCOM_PDO->lastInsertId();
        $odoo_create = '1';

// register session variables
        if ( (isset($_POST['primary']) && ($_POST['primary'] == 'on')) || ($_GET['edit'] == $OSCOM_Customer->getDefaultAddressID()) ) {
          $OSCOM_Customer->setCountryID($country);
          $OSCOM_Customer->setZoneID(($zone_id > 0) ? (int)$zone_id : '0');
          $OSCOM_Customer->setDefaultAddressID($_GET['id']);
          $odoo_create = '';

          if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) $OSCOM_Customer->getDefaultAddressID = $new_address_book_id;

          $sql_data_array = array('customers_firstname' => $firstname,
                                  'customers_lastname' => $lastname);

          if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
            $sql_data_array['customers_gender'] = $gender;
          }

          if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) $sql_data_array['customers_default_address_id'] = $new_address_book_id;

          $OSCOM_PDO->save('customers', $sql_data_array, array('customers_id' => $OSCOM_Customer->getID()) );

//***************************************
// odoo web service
//***************************************
            if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
              require ('ext/odoo_xmlrpc/xml_rpc_catalog_address_book_process.php');
            }
//***************************************
// End odoo web service
//***************************************
          $OSCOM_MessageStack->addSuccess('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED);
        }  else {
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_catalog_address_book_process.php');
          }
//***************************************
// End odoo web service
//***************************************
        }// end isset($_POST['primary']
      } // end else

      if ($_POST['shopping'] == 1) {
        osc_redirect(osc_href_link('shopping_cart.php', '', 'NONSSL'));
      } else {
        osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
      }
    } // end $error
  } // end isset($_POST['action']

  if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {

    if ($_GET['newcustomer'] == 1) {
      if ($OSCOM_Customer->getDefaultAddressID() != '')  {
        $_GET['edit'] = $OSCOM_Customer->getDefaultAddressID();
      }
    }

    $exists = false;

    $Qab = $OSCOM_PDO->prepare('select entry_gender, 
                                      entry_company, 
                                      entry_firstname,
                                      entry_lastname, 
                                      entry_street_address, 
                                      entry_suburb, 
                                      entry_postcode, 
                                      entry_city, 
                                      entry_state, 
                                      entry_zone_id, 
                                      entry_country_id,
                                      address_book_id
                                from :table_address_book 
                                where address_book_id = :address_book_id 
                                and customers_id = :customers_id');
    $Qab->bindInt(':address_book_id', (int)$_GET['edit']);
    $Qab->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    $Qab->execute();

    $entry = $Qab->fetch();

    if ( $entry !== false ) {
      $exists = true;
    }

    if ( $exists === false ) {
      $OSCOM_MessageStack->addError('addressbook', ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);

      osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
    }

  } elseif (isset($_GET['delete']) && is_numeric($_GET['delete'])) {
    if ($_GET['delete'] == $OSCOM_Customer->getDefaultAddressID()) {
      $OSCOM_MessageStack->addError('addressbook', WARNING_PRIMARY_ADDRESS_DELETION);

      osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
    } else {

      $exists = false;

      $QCheck = $OSCOM_PDO->prepare('select count(*) as total
                                     from  :table_address_book
                                     where address_book_id = :address_book_id
                                     and customers_id = :customers_id
                                  ');
      $QCheck->bindInt(':address_book_id', (int)$_GET['delete'] );          
      $QCheck->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );   

      $QCheck->execute();


      if ( $QCheck->fetch() !== false ) {
        $exists = true;
      }

      if ( $exists === false ) {
        $OSCOM_MessageStack->addError('addressbook', ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);

        osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
      }
    } // end else
  } else {
    $entry = array();
  }

// Controle autorisation du client a ajouter des adresse dans son carnet selon la quantite ou sa fiche client
  if (!isset($_GET['delete']) && !isset($_GET['edit'])) {
    if (osc_count_customer_address_book_entries() >= MAX_ADDRESS_BOOK_ENTRIES) {
      $OSCOM_MessageStack->addError('addressbook', ERROR_ADDRESS_BOOK_FULL);

      osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
    } else if (osc_count_customers_add_address() == 0) { 
      $OSCOM_MessageStack->addError('addressbook', ERROR_ADDRESS_BOOK_NO_ADD);

      osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
    }
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('address_book.php', '', 'SSL'));

  if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {

// Controle autorisation au client de modifier son adresse par defaut
    if ((osc_count_customers_modify_address_default() == 0) && ($OSCOM_Customer->getDefaultAddressID() == $_GET['edit'])) { 
      $OSCOM_MessageStack->addError('addressbook', ERROR_ADDRESS_BOOK_NO_MODIFY_DEFAULT);

      osc_redirect(osc_href_link('address_book.php', '', 'SSL'));
    }

    $OSCOM_Breadcrumb->add(NAVBAR_TITLE_MODIFY_ENTRY, osc_href_link('address_book_process.php', 'edit=' . $_GET['edit'], 'SSL'));
  } elseif (isset($_GET['delete']) && is_numeric($_GET['delete'])) {
    $OSCOM_Breadcrumb->add(NAVBAR_TITLE_DELETE_ENTRY, osc_href_link('address_book_process.php', 'delete=' . $_GET['delete'], 'SSL'));
  } else {
    $OSCOM_Breadcrumb->add(NAVBAR_TITLE_ADD_ENTRY, osc_href_link('address_book_process.php', '', 'SSL'));
  }

  require($OSCOM_Template->getTemplateFiles('address_book_process'));
