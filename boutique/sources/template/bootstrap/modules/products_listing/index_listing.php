 <?php 
/*
 Fichier Source : index_listing.php
 Fichier module : index_listing.php
 Fichier impacte :  index.php
 Autres fichiers possible : non
*/

// optional Product List Filter
    if (PRODUCT_LIST_FILTER > 0) {
// ########## B2B ##########
      if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B
        if (isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id'])) {
// Affichage en mode B2B du menu deroulant des categories sur une selection d'une marque depuis la boxe manufacturer
          $filterlist_sql = "select distinct c.categories_id as id, 
                                             cd.categories_name as name 
                             from products p left join products_groups g on p.products_id = g.products_id,
                                  products_to_categories p2c,
                                  categories c,
                                  categories_description cd
                             where p.products_status = '1' 
                             and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "' 
                             and g.products_group_view = '1' 
                             and p.products_id = p2c.products_id 
                             and p2c.categories_id = c.categories_id 
                             and p2c.categories_id = cd.categories_id 
                             and cd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and p.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "' 
                             and p.products_archive = '0'
                             order by cd.categories_name
                            ";
        } else {
          // Affichage en mode B2B du menu deroulant des Marques sur la liste des produits d'une categorie
          $filterlist_sql= "select distinct m.manufacturers_id as id, 
                                            m.manufacturers_name as name 
                            from products p left join products_groups g on p.products_id = g.products_id,
                                 products_to_categories p2c,
                                 manufacturers  m
                            where p.products_status = '1' 
                            and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "' 
                            and g.products_group_view = '1' 
                            and p.manufacturers_id = m.manufacturers_id 
                            and p.products_id = p2c.products_id 
                            and p.products_archive = '0'
                            and p2c.categories_id = '" . (int)$current_category_id . "' 
                            and m.manufacturers_status = '0'
                            order by m.manufacturers_name
                           ";
        }
// Clients Grand Public
      } else {
        if (isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id'])) {
// Affichage du menu deroulant des categories sur une selection d'une marque depuis la boxe manufacturer
          $filterlist_sql = "select distinct c.categories_id as id, 
                                             cd.categories_name as name 
                             from products p,
                                  products_to_categories p2c,
                                  categories c,
                                  categories_description cd
                             where p.products_status = '1' 
                             and p.products_view = '1' 
                             and p.products_id = p2c.products_id 
                             and p2c.categories_id = c.categories_id 
                             and p2c.categories_id = cd.categories_id 
                             and p.products_archive = '0'
                             and cd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and p.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "' 
                             order by cd.categories_name
                            ";
        } else {
// Affichage du menu deroulant des Marques sur la liste des produits d'une categorie
          $filterlist_sql= "select distinct m.manufacturers_id as id, 
                                            m.manufacturers_name as name 
                            from products p,
                                 products_to_categories p2c,
                                 manufacturers m
                            where p.products_status = '1'
                            and p.products_view = '1' 
                            and p.manufacturers_id = m.manufacturers_id 
                            and p.products_id = p2c.products_id 
                            and p.products_archive = '0'
                            and p2c.categories_id = '" . (int)$current_category_id . "' 
                            and m.manufacturers_status = '0'
                            order by m.manufacturers_name
                           ";
        }
    }
// ####### END - B2B #######

      $filterlist_query = osc_db_query($filterlist_sql);

      if (osc_db_num_rows($filterlist_query) > 1) {
        echo '<div>' . osc_draw_form('filter', osc_href_link('index.php'), 'get') . '<p align="center"> '. TEXT_SHOW . '&nbsp;';
        if (isset($_GET['manufacturers_id'])  && !empty($_GET['manufacturers_id'])) {
          echo osc_draw_hidden_field('manufacturers_id', $_GET['manufacturers_id']);
          $options = array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES));
        } else {
          echo osc_draw_hidden_field('cPath', $cPath);
          $options = array(array('id' => '', 'text' => TEXT_ALL_MANUFACTURERS));
        }
        echo osc_draw_hidden_field('sort', $_GET['sort']);
        while ($filterlist = osc_db_fetch_array($filterlist_query)) {
          $options[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
        }
        echo osc_draw_pull_down_menu('filter_id', $options, (isset($_GET['filter_id']) ? $_GET['filter_id'] : ''), 'onchange="this.form.submit()"');
        echo osc_hide_session_id() . '</p></form></div>' . "\n";

      }
    }

// Get the right image for the top-right
    $image = DIR_WS_IMAGES . '';
    $heading_title = HEADING_TITLE;
    if (isset($_GET['manufacturers_id'])) {
      $image = osc_db_query("select manufacturers_image 
                             from manufacturers
                             where manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'
                             and manufacturers_status = '0'
                           ");
      $image = osc_db_fetch_array($image);
      $heading_title = $image['manufacturers_name'];
      $image = $image['manufacturers_image'];

    } elseif ($current_category_id) {

      $Qcategories = $OSCOM_PDO->prepare('select  cd.categories_description,
                                                  c.categories_image
                                             from :table_categories c,
                                                  :table_categories_description cd
                                             where c.categories_id = :categories_id
                                             and c.categories_id = :categories_id
                                             and cd.language_id = :language_id
                                         ');
      $Qcategories->bindInt(':categories_id', (int)$current_category_id );
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qcategories->execute();

      $categories = $Qcategories->fetch();

      $image = $categories['categories_image'];

    }

?>