<?php
/**
 * modules_upload.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require ('includes/application_top.php');
  require ('includes/classes/pclzip.lib.php');
  require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . 'modules_upload.php');
  
  $action = (isset ($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'choice' :
        $choice = osc_db_prepare_input($_POST['module']);

        if ($choice == '1') {
          $action = 'template';
          osc_redirect(osc_href_link('modules_upload.php','action='. $action .'&module='.$choice));
        } elseif ($choice == '2') {
          $action= 'insert'; 	
          osc_redirect(osc_href_link('modules_upload.php','action='. $action));
        } 
      break;

    case 'template_choice' :
        $template_choice = osc_db_prepare_input($_POST['configuration_value']);

      if ($template_choice != '') {
        $action = 'insert_module';
        osc_redirect(osc_href_link('modules_upload.php','action='. insert .'&module='.$template_choice));
      } else {
        osc_redirect(osc_href_link('modules_upload.php'));
      } 
    break;

    case 'insert_module' :

      $title_template = osc_db_prepare_input($_POST['new_template']);

// upload file in the directory	        
      if ($module_filename = new upload('filename', DIR_FS_CATALOG . 'plugin_download')) {
        if (isset ($module_filename->filename)) {
        }
      }
// file name
      $file =	$module_filename ->filename; 

/*
Note :
la decompression doit prendre en compte les cas suivants
Si c'est un module fixe (non customizable), la decompression doit prendre en compte le repertoire de langue par defaut et le repertoire module
Si c'est un module template (e customize) la decompression doit prendre en compte le theme du template et le repertoire par defaut de la langue
Si c'est un template le décompresser dans le répertoire template
**/

// installe a module in template directory
      if ($title_template !='') {
        $directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . $title_template; 

/*
le module doit comporter les éléments suivants
rep langue/lalangue/module/nomdumodule/fichier.php
rep module/nommodule.fichier.php
pb : les repertoires du haut comme javascript ne sont pas pris en compte, et doivent etre insere dans le répertoire du template en tant que tel.
*/

//decompression du fichier + restriction acces disque
        $archive = new PclZip(DIR_FS_CATALOG . 'plugin_download/'.$file);
        $extract = $archive->extract(PCLZIP_OPT_EXTRACT_DIR_RESTRICTION, DIR_FS_CATALOG,
                                     PCLZIP_OPT_PATH, $directory);
        if ($extract == 0) {
           echo "ERROR : ".$archive->errorInfo(true);
        } 
      } else {
/*
le module doit comporter les éléments suivants
rep sources/replangue/lalangue/module/nomdumodule/fichier.php
rep includes/repmodule/nommodule.fichier.php
*/

// decompression du fichier + restriction acces disque
        $archive = new PclZip(DIR_FS_CATALOG . 'plugin_download/' . $file);
        $extract = $archive->extract(PCLZIP_OPT_EXTRACT_DIR_RESTRICTION, DIR_FS_CATALOG,
                                     PCLZIP_OPT_PATH, DIR_FS_CATALOG);
        if ($extract == 0) {
           echo "ERROR : ".$archive->errorInfo(true);
        }
      } // end $title_template

       osc_redirect(osc_href_link('modules_upload.php','action=success'));

      break;

     case 'success':
     break;

    }
  }

  require ('includes/header.php');
?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<?php
//-----------------------
// Success
//----------------------
if ($action == 'success') {
?>

  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/desktop_enhancements.png', HEADING_TITLE_STEP4, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_STEP4; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div style=" padding-top:50px;"><?php echo WELCOME_MESSAGE_INTRO_STEP4; ?></div>
  <div style=" padding-top:10px;"><?php echo WELCOME_MESSAGE_STEP4; ?></div>

<?php
//-----------------------
// File upload
//----------------------
} elseif ($action == 'insert') {
  $form_action = 'insert_module';
  $template_install = $_GET['module'];
?>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/desktop_enhancements.png', HEADING_TITLE_STEP3, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_STEP3; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div style="padding-top:50px;"><?php echo WELCOME_MESSAGE_STEP3; ?></div>
  <form name="module" <?php echo 'action="' . osc_href_link('modules_upload.php', 'action=' . $form_action) . '"'; ?> method="post" enctype="multipart/form-data">
    <div style="text-align:center;"><?php echo 'installation dans le template' . $title_template; ?></div>
    <div style="text-align: center; padding-top:50px;">
      <span class="col-md-4 pull-left;"></span>
      <span class="col-md-4 pull-left;">
       <input type="hidden" name="new_template" value="<?php echo $template_install ?>" />
<?php
    echo osc_draw_file_field('filename') .'<br /><br />';
?>
      </span>
    </div>
    <div class="clearfix"></div>
    <div style="text-align:center; padding-top:30px;">
<?php
      echo osc_image_submit('button_insert.gif', IMAGE_INSERT);
?>
    </div>
  </form>


<?php
//-----------------------
// template choice (option)
//----------------------
  } elseif ($action == 'template') {
    $form_action = 'template_choice';
?>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/desktop_enhancements.png', HEADING_TITLE_STEP2, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_STEP2; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div style=" padding-top:50px;"><?php echo WELCOME_MESSAGE_STEP2; ?></div>
  <form name="module" <?php echo 'action="' . osc_href_link('modules_upload.php', 'action=' . $form_action) . '"'; ?> method="post">
    <div style="text-align: center; padding-top:50px;"><?php echo osc_cfg_pull_down_all_template_directorylist($value); ?></div>
    <div style="text-align: center; padding-top:50px;"><?php echo osc_image_submit('button_insert.gif',  IMAGE_INSERT) . '</a>'; ?></div>
  </form>
<?php
//-----------------------
// Install Module Option
//----------------------
  } elseif ($action == 'step1') {
    $form_action = 'choice';
?>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/desktop_enhancements.png', HEADING_TITLE_STEP1, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_STEP1; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div style=" padding-top:50px;"><?php echo WELCOME_MESSAGE_STEP1; ?></div>
  <form name="module" <?php echo 'action="' . osc_href_link('modules_upload.php', 'action=' . $form_action) . '"'; ?> method="post">

    <div style="text-align: center; padding-bottom: 50px;">
<?php
    $modules_array = array ( array ('id' => '0',
                                    'text' => TEXT_NONE),  // aucun
                            array ('id' => '1',
                                   'text' => TEXT_MODULE_TEMPLATE), // module dans un template
                            array ('id' => '2',
                                   'text' => TEXT_MODULE_FIXE) // module fixe
    );

  echo osc_draw_pull_down_menu('module', $modules_array);
?>
    </div>
    <div style="text-align: center; padding-top:50px;"><?php echo osc_image_submit('button_insert.gif',  IMAGE_INSERT) . '</a>'; ?></div>
  </form>
<?php
  } else {
//-----------------------
// Introduction
//-----------------------
    $action = 'step1';
?>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/desktop_enhancements.png', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div style=" padding-top:50px;"><?php echo WELCOME_HEADING_TITLE; ?></div>
  <div style="text-align: center; padding-top:50px;"><?php echo '<a href="' . osc_href_link('modules_upload.php', 'action=' . $action) . '">' . osc_image_button('button_insert.gif',  IMAGE_INSERT) . '</a>'; ?></div>
<?php
  }
  require ('includes/footer.php');
  require ('includes/application_bottom.php');
?>