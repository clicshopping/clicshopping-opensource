<?php
/*
 * xml_rpc_admin_order_invoice.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/


  require_once(DIR_WS_CLASSES. 'order.php');
  $order = new order($oID);


// **********************************
//  search customer_id in ClicShopping
// **********************************

  $Qcustomers = $OSCOM_PDO->prepare('select customers_id,
                                             orders_status,
                                             date_purchased,
                                             orders_status_invoice,
                                             orders_date_finished,
                                             odoo_invoice
                                       from :table_orders
                                       where orders_id = :orders_id
                                     ');
  $Qcustomers->bindValue(':orders_id', (int)$oID);
  $Qcustomers->execute();

  $customers = $Qcustomers->fetch();

  $date_invoice = $customers['date_purchased'];
  $ref_webstore = $OSCOM_Date->getDateReferenceShort($date_invoice) . 'S'; // ref invoice  clicshopping
  $odoo_invoice = $customers['odoo_invoice']; // status of Odoo in clicshopping

  if ($odoo_invoice > 0) {

// search id of order in Odoo
    $ids = $OSCOM_ODOO->odooSearch('origin', '=', 'WebStore - order : ' . $ref_webstore . $oID, 'sale.order');

    $field_list = array('id',
                        'name'
                      );

    $odoo_order_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'sale.order');
    $odoo_order_id = $odoo_order_read[0][id];
    $odoo_order_name = $odoo_order_read[0][name];

// workflow to update the Odoo invoice
// transfert order to invoice before treatment
    if ($status == 2) {
      $OSCOM_ODOO->workflowOdoo('sale.order', 'manual_invoice',  $odoo_order_id);
    }

// research id of invoice in  Odoo  if it exist
    if ($odoo_order_name != '') {
      $ids = $OSCOM_ODOO->odooSearch('origin', '=', $odoo_order_name, 'account.invoice');
    } else {
      $ids = $OSCOM_ODOO->odooSearch('origin', '=', 'WebStore - order : ' . $ref_webstore . $oID, 'account.invoice');
    }

    $field_list = array('id',
                       'origin');

    $qInvoiceId = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.invoice');
    $invoice_id = $qInvoiceId[0][id];
    $invoice_origin = $qInvoiceId[0][origin];

    if ($invoice_id != '' ) {

      $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();

// **********************************
// Update invoice inside Odoo
// **********************************


//*******************************************
// shipping line
//*******************************************


      $QshippingTitle = $OSCOM_PDO->prepare ('select title
                                             from :table_orders_total
                                             where orders_id = :orders_id
                                             and class = :class
                                           ');
      $QshippingTitle->bindInt (':orders_id', (int)$oID);
      $QshippingTitle->bindValue (':class', 'ot_shipping');
      $QshippingTitle->execute ();
      $shipping_title = $QshippingTitle->fetch ();
      $shipping_title = html_entity_decode ($shipping_title['title']);

// Search odoo id shipping number account.account
      $ids = $OSCOM_ODOO->odooSearch('code', '=', ODOO_WEB_SERVICE_ACCOUNT_SHIPPING, 'account.account');

// read id shipping odoo
      $field_list = array('id',
                          'name',
                          'code',
                        );

      $QshippingAccountId = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
      $shipping_account_id = $QshippingAccountId[0][id]; // 864

//search shipping in odoo
      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $shipping_title, 'account.invoice.line', 'string',
                                                  'invoice_id', '=', $invoice_id, 'int');

      $field_list = array('id',
                          'name',
                          'invoice_id',
                          'account_id',
                        );

      $qInvoiceShippingLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');
      $invoice_shipping_line_id = $qInvoiceShippingLine[0][id]; //15
      $invoice_shipping_line_name = $qInvoiceShippingLine[0][name];

// shipping line and update
      $values = array(
                        "invoice_id" => new xmlrpcval($invoice_id, "int"),
                        "company_id" => new xmlrpcval($company_id, "int"),
                        "name" => new xmlrpcval($invoice_shipping_line_name, "string"),
                        "account_id" => new xmlrpcval($shipping_account_id, "int"),
                      );

      $OSCOM_ODOO->updateOdoo($invoice_shipping_line_id, $values, 'account.invoice.line');

// **********************************
// Search odoo id discount account.account
// **********************************
      $QCustomerDiscountTitle = $OSCOM_PDO->prepare('select title
                                                     from :table_orders_total
                                                     where orders_id = :orders_id
                                                     and class = :class
                                                   ');
      $QCustomerDiscountTitle->bindInt(':orders_id', (int)$oID);
      $QCustomerDiscountTitle->bindValue(':class', 'ot_customer_discount');
      $QCustomerDiscountTitle->execute();
      $customer_discount_title = $QCustomerDiscountTitle->fetch();
      $customer_discount_title = html_entity_decode($customer_discount_title['title']);

//search discount coupon in odoo
      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $customer_discount_title, 'account.invoice.line', 'string',
                                                 'invoice_id', '=', $invoice_id, 'int');

      $field_list = array('id',
                          'name',
                        );

      $QinvoiceDiscountLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');
      $invoice_discount_line_id = $QinvoiceDiscountLine[0][id]; //26
      $invoice_discount_line_name = $QinvoiceDiscountLine[0][name];

// **********************************
// Search odoo id discount account.account
// **********************************
// Apply also to discount customerand coupon
      $ids = $OSCOM_ODOO->odooSearch('code', '=', ODOO_WEB_SERVICE_ACCOUNT_DISCOUNT, 'account.account');

      $field_list = array('id',
                          'name',
                          'code',
                        );

      $QdiscountId = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.account');
      $discount_number_id = $QdiscountId[0][id]; // 874


//******************************************************
// coupon line
//*****************************************************

//search coupon inside ClicShipping
      $QDiscountCouponTitle = $OSCOM_PDO->prepare ('select title
                                                   from :table_orders_total
                                                   where orders_id = :orders_id
                                                   and class = :class
                                                 ');
      $QDiscountCouponTitle->bindInt (':orders_id', (int)$oID);
      $QDiscountCouponTitle->bindValue (':class', 'ot_discount_coupon');
      $QDiscountCouponTitle->execute ();
      $discount_coupon_title = $QDiscountCouponTitle->fetch ();
      $discount_coupon_title = html_entity_decode($discount_coupon_title['title']);

//search discount coupon in odoo
      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $discount_coupon_title, 'account.invoice.line', 'string',
                                                  'invoice_id', '=', $invoice_id, 'int');

      $field_list = array('id',
                          'name',
                          );

      $invoice_coupon_line = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.invoice.line');
      $invoice_coupon_line_id = $invoice_coupon_line[0][id];
      $invoice_coupon_line_name = $invoice_coupon_line[0][name];

      $values = array(
                      "id" => new xmlrpcval($invoice_coupon_line_id, "int"),
                      "invoice_id" => new xmlrpcval($invoice_id, "int"), // 13
                      "company_id" => new xmlrpcval($company_id, "int"),
                      "name" => new xmlrpcval($invoice_coupon_line_name, "string"),
                      "account_id" => new xmlrpcval($discount_number_id, "int"),
                    );

      $OSCOM_ODOO->updateOdoo($invoice_coupon_line_id, $values, 'account.invoice.line');


//******************************************************
// Discount line
//*****************************************************

//search discount inside ClicShipping
      $QCustomerDiscountTitle = $OSCOM_PDO->prepare('select title
                                                     from :table_orders_total
                                                     where orders_id = :orders_id
                                                     and class = :class
                                                    ');
      $QCustomerDiscountTitle->bindInt (':orders_id', (int)$oID);
      $QCustomerDiscountTitle->bindValue (':class', 'ot_customer_discount');
      $QCustomerDiscountTitle->execute ();
      $customer_discount_title = $QCustomerDiscountTitle->fetch ();
      $customer_discount_title = html_entity_decode($customer_discount_title['title']);

//search discount coupon in odoo
      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('name','=', $customer_discount_title, 'account.invoice.line', 'string', 'invoice_id', '=', $invoice_id, 'int');

      $field_list = array('id',
                          'name',
                         );

      $QinvoiceDiscountLine = $OSCOM_ODOO->readOdoo ($ids, $field_list, 'account.invoice.line');
      $invoice_discount_line_id = $QinvoiceDiscountLine[0][id]; //26
      $invoice_discount_line_name = $QinvoiceDiscountLine[0][name];

      $values = array(
                      "id" => new xmlrpcval($invoice_discount_line_id, "int"), // 26
                      "invoice_id" => new xmlrpcval($invoice_id, "int"), // 13
                      "company_id" => new xmlrpcval($company_id, "int"),
                      "name" => new xmlrpcval($invoice_discount_line_name, "string"),
                      "account_id" => new xmlrpcval($discount_number_id, "int"),
                    );

      $OSCOM_ODOO->updateOdoo($invoice_discount_line_id, $values, 'account.invoice.line');

// invoice in treatment

      if ($status == '2'  && $odoo_invoice == '1' )  {
        $odoo_invoice = 2;
      } elseif ($status == '3' && $odoo_invoice == '2')  {
        $OSCOM_ODOO->workflowOdoo('account.invoice', 'invoice_open',  $invoice_id);
        $odoo_invoice = 3;
      } elseif ($status == '4' &&  $odoo_invoice = '2') {
        $OSCOM_ODOO->workflowOdoo ('account.invoice', 'invoice_cancel', $invoice_id);
        $odoo_invoice = 4;
      }
    }  // $invoice_id


//******************************************************
// Stock Management
//*****************************************************
// Stock.move, create the good real stock and not the virtual
    if ( ($odoo_invoice == '3')  || ($odoo_invoice == '4') ) {
// search id of order in Odoo for stock move
      $ids = $OSCOM_ODOO->odooSearchAll('origin', '=', $odoo_order_name, 'stock.move');

      $field_list = array('id');

      $Qodoo_order_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
      $odoo_picking_id = $odoo_order_read[0][id];

      if ($odoo_invoice = 3) {
        foreach ($Qodoo_order_read as $key) {
          $odoo_picking_id = $key[id];
          $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);
        }
      } else {
        foreach ($Qodoo_order_read as $key) {
          $odoo_picking_id = $key[id];
          $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_cancel', $odoo_picking_id);
        }
      }
    }


// Update ClicShopping
    $Qupdate = $OSCOM_PDO->prepare('update :table_orders
                                    set odoo_invoice = :odoo_invoice
                                    where orders_id = :orders_id
                                  ');
    $Qupdate->bindInt(':orders_id', (int)$oID);
    $Qupdate->bindValue(':odoo_invoice', $odoo_invoice);
    $Qupdate->execute();
  }
?>