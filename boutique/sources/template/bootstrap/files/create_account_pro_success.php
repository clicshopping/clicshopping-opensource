<?php
/*
 * create_account_pro_success.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Account success  -----  
// ---------------------- 
?>
<div class="contentContainer">
  <div class="contentText">
    <?php echo TEXT_ACCOUNT_CREATED; ?>
  </div>
  <div class="buttonSet" align="right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, $origin_href, 'success'); ?></div>
</div>

<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
