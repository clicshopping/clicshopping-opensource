<?php
/**
 * products_listing.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */


  class ProductsReviews {

    public function __construct($products_id)  {
      $this->id = $products_id;
    }

    // prepare query for all product review
    public function getReviewsRaw()  {
      global $OSCOM_Customer, $OSCOM_PDO;

      if ($OSCOM_Customer->getCustomersGroupID() != 0) {

        $Qreviews = $OSCOM_PDO->prepare("select  r.reviews_id,
                                                 left(rd.reviews_text, 100) as reviews_text,
                                                 r.reviews_rating,
                                                 r.date_added,
                                                 p.products_id,
                                                 pd.products_name,
                                                 p.products_image,
                                                 r.customers_name
                                         from  :table_reviews r,
                                               :table_reviews_description rd,
                                               :table_products p left join :table_products_groups g on p.products_id = g.products_id
                                               :table_products_description pd
                                         where p.products_status = :products_status
                                         and r.status = '1'
                                         and g.customers_group_id = :customers_group_id
                                          and g.products_group_view = :products_group_view
                                         and p.products_id = r.products_id
                                         and r.reviews_id = rd.reviews_id
                                         and p.products_id = pd.products_id
                                         and pd.language_id = :languages_id
                                         and rd.languages_id = :languages_id
                                         and r.status = :status
                                         order by r.reviews_id DESC limit :page_set_offset, :page_set_max_results
                                         ");
        $Qreviews->bindInt(':products_status',  1);
        $Qreviews->bindInt(':customers_group_id',  (int)$OSCOM_Customer->getCustomersGroupID());
        $Qreviews->bindInt(':products_group_view',  1);
        $Qreviews->bindInt(':languages_id',  (int)$_SESSION['languages_id']);
        $Qreviews->bindInt(':status',  1);
        $Qreviews->setPageSet(MAX_DISPLAY_NEW_REVIEWS);
       
        $Qreviews->execute();
        $reviews = $Qreviews->fetch();

      } else {
        $Qreviews = $OSCOM_PDO->prepare("select  r.reviews_id,
                                                 left(rd.reviews_text, 100) as reviews_text,
                                                 r.reviews_rating,
                                                 r.date_added,
                                                 p.products_id,
                                                 pd.products_name,
                                                 p.products_image,
                                                 r.customers_name
                                         from  :table_reviews r,
                                               :table_reviews_description rd,
                                               :table_products p,
                                               :table_products_description pd
                                         where p.products_status = :products_status
                                         and p.products_view = :products_view
                                         and p.products_id = r.products_id
                                         and r.reviews_id = rd.reviews_id
                                         and p.products_id = pd.products_id
                                         and pd.language_id = :languages_id
                                         and rd.languages_id = :languages_id
                                         and status = :status
                                         order by r.reviews_id DESC limit :page_set_offset, :page_set_max_results
                                         ");

        $Qreviews->bindInt(':products_status',  1);
        $Qreviews->bindInt(':products_view',  1);
        $Qreviews->bindInt(':languages_id',  (int)$_SESSION['languages_id']);
        $Qreviews->bindInt(':status',  1);
        $Qreviews->setPageSet(MAX_DISPLAY_NEW_REVIEWS);
        
        $Qreviews->execute();
        $reviews = $Qreviews->fetch();
      }

      return $reviews;
    }

  // prepare query for single product review
    public function getReviews() {
      global $OSCOM_PDO;
      
      $Qreviews = $OSCOM_PDO->prepare("select  r.reviews_id,
                                               left(rd.reviews_text, 100) as reviews_text,
                                               r.reviews_rating,
                                               r.date_added,
                                               r.customers_name
                                         from  :table_reviews r,
                                               :table_reviews_description rd,
                                         where r.products_id = :products_id
                                         and r.reviews_id = rd.reviews_id
                                         and rd.languages_id = :languages_id
                                         and r.status = 1
                                         order by r.reviews_id desc
                                         ");

      $Qreviews->bindInt(':products_id',  (int)$this->id);
      $Qreviews->bindInt(':languages_id',  (int)$_SESSION['languages_id']);
      $Qreviews->execute();
      $reviews = $Qreviews->fetch();

      return $reviews;
    }


  // returns a new splitpageresults object that can be used to split the all reviews query or the single product reviews query
    public function getReviewsSplit($query) {
      return new splitPageResults($query, MAX_DISPLAY_NEW_REVIEWS);
    }

  // returns the average reviews rating as array
    public function getReviewsAvg() {
      global $OSCOM_Products, $OSCOM_PDO;

      $Qaverage = $OSCOM_PDO->prepare('select AVG(r.reviews_rating) as average,
                                            count(r.reviews_rating) as count
                                       from :table_reviews r
                                       where r.products_id = :products_id
                                       and r.status = 1
                                      ');
      $Qaverage->bindInt(':products_id',  (int)$OSCOM_Products->getId());
      $Qaverage->bindInt(':status',  1);
      $Qaverage->execute();

      $average = $Qaverage->fetch();

      return $average;
    }

  // returns number of reviews for current product
    public function getReviewsCount() {
      global $OSCOM_PDO, $OSCOM_Products;

      $Qreviewcheck = $OSCOM_PDO->prepare('select count(*) as total
                                         from :table_reviews r,
                                              :table_reviews_description rd
                                          where r.reviews_id = :reviews_id
                                          and r.products_id = :products_id
                                          and r.reviews_id = rd.reviews_id
                                          and rd.languages_id = :languages_id
                                        ');
      $Qreviewcheck->bindInt(':reviews_id', (int)$_GET['reviews_id']);
      $Qreviewcheck->bindInt(':products_id', (int)$OSCOM_Products->getId() );
      $Qreviewcheck->bindInt(':languages_id', (int)$_SESSION['languages_id']);
      $Qreviewcheck->execute();

      $review_check = $Qreviewcheck->fetch();

      $review_check = $review_check['total'];

      return $review_check;
    }
  }
