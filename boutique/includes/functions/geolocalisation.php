<?php
/*
 * geolocalisation.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

/*
 * indicate different informations on the customer
 * @param $localisation_array return an array on the localisation
 * @access public
*/
  function  osc_locate_customer() {
    global $spider_flag;

    if ( $spider_flag === false ) {
      if (CONFIGURATION_CURRENCIES_GEOLOCALISATION == 'true') {

      require_once(DIR_WS_EXT . 'modules/ipcountry/ip2country.class.php');
        $region = new ip2country();

        $localisation_array = array('continent' => $region->getRegion(),
                                    'country_code2' => $region->getCountrycode()
                                  );
      }
    }
    return $localisation_array;
  }

/*
 * Currency in function the localisation
 * @param $new_currency return the currency in function the localisation
 * @access public
*/
  function osc_get_currencies_location() {

    $localisation = osc_locate_customer();
    $continent_code = $localisation['continent'];
    $country_code2 = $localisation['country_code2'];

    if ($continent_code == 'NA') {
      if ($country_code2 == 'CA') {
        if (DEFAULT_CURRENCY != 'CAD') {
          $new_currency = 'CAD';
        } else {
          $new_currency = DEFAULT_CURRENCY;
        }
      } elseif ($country_code2 == 'US') {
        if (DEFAULT_CURRENCY != 'USD') {
          $new_currency = 'USD';
        } else {
          $new_currency = DEFAULT_CURRENCY;
        }
      } else {
        $new_currency = DEFAULT_CURRENCY;
      }

    } elseif ($continent_code == 'EU') {
      if (DEFAULT_CURRENCY != 'EUR') {
        $new_currency = 'EUR';
      } else {
        $new_currency = DEFAULT_CURRENCY;
      }
    } else {
      $new_currency = DEFAULT_CURRENCY;
    }
    return $new_currency;
  }
