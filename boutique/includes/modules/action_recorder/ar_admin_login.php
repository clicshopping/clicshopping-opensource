<?php
/*
 * ar_admin_login.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  class ar_admin_login {
    var $code = 'ar_admin_login';
    var $title;
    var $description;
    var $sort_order = 0;
    var $minutes = 5;
    var $attempts = 3;
    var $identifier;

    function ar_admin_login() {
      $this->title = MODULE_ACTION_RECORDER_ADMIN_LOGIN_TITLE;
      $this->description = MODULE_ACTION_RECORDER_ADMIN_LOGIN_DESCRIPTION;

      if ($this->check()) {
        $this->minutes = (int)MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES;
        $this->attempts = (int)MODULE_ACTION_RECORDER_ADMIN_LOGIN_ATTEMPTS;
      }
    }

    function setIdentifier() {
      $this->identifier = osc_get_ip_address();
    }

    function canPerform($user_id, $user_name) {
      global $OSCOM_PDO;
      
      $sql_query = 'select id 
                    from :table_action_recorder 
                    where module = :module
                   ';

      if (!empty($user_name)) {
        $sql_query .= ' and (user_name = :user_name or identifier = :identifier)';
      } else {
        $sql_query .= ' and identifier = :identifier';
      }

      $sql_query .= ' and date_added >= date_sub(now(), 
                      interval :limit_minutes minute) 
                      and success = 0 
                      limit :limit_attempts
                    ';

      $Qcheck = $OSCOM_PDO->prepare($sql_query);
      $Qcheck->bindValue(':module', $this->code);

      if (!empty($user_name)) {
        $Qcheck->bindValue(':user_name', $user_name);
      }

      $Qcheck->bindValue(':identifier', $this->identifier);
      $Qcheck->bindInt(':limit_minutes', $this->minutes);
      $Qcheck->bindInt(':limit_attempts', $this->attempts);
      $Qcheck->execute();

      if (count($Qcheck->fetchAll()) == $this->attempts) {
        return false;
      }

      return true;
    }

    function expireEntries() {
      global $OSCOM_PDO, $db_link;

      $Qdel = $OSCOM_PDO->prepare('delete
                                  from :table_action_recorder 
                                  where module = :module 
                                  and date_added < date_sub(now(), 
                                  interval :limit_minutes minute)
                                ');
      $Qdel->bindValue(':module', $this->code);
      $Qdel->bindInt(':limit_minutes', $this->minutes);
      $Qdel->execute();

      return $Qdel->rowCount();
    }

    function check() {
      return defined('MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES');
    }

    function install() {
      global $OSCOM_PDO;
      if ($_SESSION['languages_id'] =='1') {
        $OSCOM_PDO->save('configuration', [
                                          'configuration_title' => 'Veuiller indiquer le temps d\'attente pour une erreur de connexion dans la partie administration',
                                          'configuration_key' => 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES',
                                          'configuration_value' => '5',
                                          'configuration_description' => 'Veuillez indiquer le nombre de minutes d\'attente concernant une nouvelle connexion dans l\'administration.',
                                          'configuration_group_id' => '6',
                                          'sort_order' => '0',
                                          'date_added' => 'now()'
                                        ]
                      );

        $OSCOM_PDO->save('configuration', [
                                          'configuration_title' => 'Veuiller indiquer le nombre de login permis pour se connecter dans la partie administration',
                                          'configuration_key' => 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_ATTEMPTS',
                                          'configuration_value' => '3',
                                          'configuration_description' => 'Veuillez indiquer le nombre de login permis concernant une connexion dans l\'administration.',
                                          'configuration_group_id' => '6',
                                          'sort_order' => '0',
                                          'date_added' => 'now()'
                                        ]
                       );
      } else {

        $OSCOM_PDO->save('configuration', [
                                          'configuration_title' => 'Allowed Minutes',
                                          'configuration_key' => 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES',
                                          'configuration_value' => '5',
                                          'configuration_description' => 'Number of minutes to allow login attempts to occur.',
                                          'configuration_group_id' => '6',
                                          'sort_order' => '0',
                                          'date_added' => 'now()'
                                        ]
                      );

      $OSCOM_PDO->save('configuration', [
                                        'configuration_title' => 'Allowed Attempts',
                                        'configuration_key' => 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_ATTEMPTS',
                                        'configuration_value' => '3',
                                        'configuration_description' => 'Number of login attempts to allow within the specified period.',
                                        'configuration_group_id' => '6',
                                        'sort_order' => '0',
                                        'date_added' => 'now()'
                                      ]
                     );
      }
    }

    function remove() {
     global $OSCOM_PDO;
    
     $Qdel = $OSCOM_PDO -> prepare('delete
                                    from :table_configuration
                                    where configuration_key in  :configuration_key
                                  ');
      $Qdel->bindValue(':configuration_key', implode("', '", $this->keys() ));

      $Qdel->execute();
    }

    function keys() {
      return array('MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES', 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_ATTEMPTS');
    }
  }
