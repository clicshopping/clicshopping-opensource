<?php
/**
 * manufacturers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Marques');

define('TABLE_HEADING_MANUFACTURERS', 'Marques');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_HEADING_NEW_MANUFACTURER', 'Nouvelle marque');
define('TEXT_HEADING_EDIT_MANUFACTURER', 'Editer marque');
define('TEXT_HEADING_DELETE_MANUFACTURER', 'Supprimer marque');
define('TABLE_HEADING_STATUS','Statut');

define('TITLE_MANUFACTURER_GENERAL','Informations générale');
define('TEXT_MANUFACTURERS_DESCRIPTION','Description');
define('TITLE_MANUFACTURER_IMAGE','Image');

define('TEXT_MANUFACTURERS', 'Marques :');
define('TEXT_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_PRODUCTS', 'Produits :');
define('TEXT_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');
define('TEXT_MANUFACTURERS_DESCRIPTION ','Description');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');
define('TEXT_PRODUCTS_IMAGE_VISUEL','Prévisualition de l\'Image');

define('TAB_DESCRIPTION','Description');
define('TAB_VISUEL','Image');
define('TAB_SEO','Métadonnées');


define('TEXT_NEW_INTRO', 'Merci de compléter les informations sur la nouvelle marque');
define('TEXT_EDIT_INTRO', 'Merci de faire les changements nécessaires');

define('TEXT_MANUFACTURERS_NAME', 'Nom de la marque :');
define('TEXT_MANUFACTURERS_NEW_IMAGE', 'Nouvelle image de la marque :');
define('TEXT_MANUFACTURERS_IMAGE', 'Image actuelle de la marque :');
define('TEXT_MANUFACTURERS_DELETE_IMAGE', 'Ne plus afficher d\'image de la marque');
define('HELP_IMAGE_MANUFACTURERS', '* Veuillez à ne pas mettre plus d\'une image dans la section "Nouvelle image".<br /><br />* Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus).');

define('TEXT_MANUFACTURERS_IMAGE_DELETE','Suppprimer l\'image');
define('TEXT_MANUFACTURERS_URL', 'URL de la marque :');

define('TEXT_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer cette marque ?');
define('TEXT_DELETE_IMAGE', 'Suppprimer l\'image de la marque ?');
define('TEXT_DELETE_PRODUCTS', 'Souhaitez vous supprimer tous les produits de cette marque ? (en incluant les critiques, produits en promotion, produits à venir)');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>ATTENTION :</strong> Il reste %s produit(s) liées à cette marque !');


define('TITLE_AIDE_IMAGE', 'Notes sur l\'utilisation des images');
define('HELP_IMAGE_MANUFACTURERS', '<li>Veuillez à ne pas mettre plus d\'une image dans la sections "Image vignette de la catégorie".</li><li>Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

define('TITLE_HELP_DESCRIPTION', 'Notes sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

define('TITLE_MANUFACTURER_SEO','Métadonnées pour le marques du produit');
define('TEXT_MANUFACTURER_SEO_TITLE','Titre');
define('TEXT_MANUFACTURER_SEO_DESCRIPTION','Description');
define('TEXT_MANUFACTURER_SEO_KEYWORDS','Mots clés');

define('TITLE_HELP_SUBMIT', 'Notes sur la gestion des métadonnées');
define('HELP_SUBMIT', '<li>Le système gère automatiquement la gestion des métadonnées.<br />
<blockquote>Il inscrit automatiquement les éléments dans l\'ordre suivant si les champs ne sont pas renseignées:<br />
Titre : le nom de votre produit (ex blouson cuir)<br />
Description : le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie,)<br />
Keywords : le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
categorie : la catégorie du produit<br />
</blockquote></li>
<li>Si vous renseignez les champs, les balises apparaitront de la facon suivante : <br />
<blockquote>
Titre : le titre du champs, le nom de votre produit (ex blouson cuir)<br />
Description : le champs description, le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
Mots clefs : le champs mots clefs, le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
</blockquote></li>
<li><strong>Veuillez à ne pas mettre trop de mots clefs (maximum 10)</strong> et établissez une cohérence entre chaque champs afin de retourver les mots les plus importants en premier</li>
');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Erreur : Impossible d\'écrire dans le répertoire. Merci de modifier les droits d\'accès sur : %s');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire n\'existe pas : %s');
?>