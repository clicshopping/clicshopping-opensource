<?php
  /**
   * cfm_modules_payment.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_payment {
    var $code = 'payment';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_PAYMENT_INSTALLED';
    var $title;
    var $template_integration = false;

    function cfgm_payment() {
      $this->directory = DIR_FS_CATALOG_MODULES . 'payment/';
      $this->title = MODULE_CFG_MODULE_PAYMENT_TITLE;
    }
  }
?>
