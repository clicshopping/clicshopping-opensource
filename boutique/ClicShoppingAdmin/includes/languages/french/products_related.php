<?php
/**
 * products_related.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Gestion des produits croisés et des produits similaires');
define('HEADING_TITLE_SEARCH','Rechercher');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_PRODUCT_FROM','Produits références (De)');
define('TABLE_HEADING_PRODUCT_TO', 'Produits associés (A)');
define('TABLE_HEADING_STATUS','Statut');
define('TABLE_HEADING_CUSTOMERS_GROUP','Mode B2B');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_ORDER','Ordre d\'affichage');
define('TABLE_HEADING_RELATED_PRODUCTS','Produits similaires');
define('TABLE_HEADING_CROSS_SELL_PRODUCTS','Produits croisés');
define('TABLE_HEADING_MODEL','Réf');


define('TEXT_CONFIRM_DELETE_ATTRIBUTE','Veuillez confirmer de la suppression');

define('SHOW_ALL_PRODUCTS','Tous les produits');
define('EDIT','Editer');
define('DELETE','Supprimer');
define('INHERIT','Héritage');
define('RECIPROCICATE','Réciprocité');
define('INSERT','Insérer');
define('TITLE_AIDE_RELATED_PRODUCTS','Notes sur les produits croisés et similaires');
define('TEXT_AIDE_RELATED_PRODUCTS_CONTENT', '<ul><li>Nous vous conseillons de choisir pour un produit soit l\'option produit croisé ou soit l\'option produit similaire.</li>
<li><u>Le mode B2B</u> (si vous avez l\'option) permet d\'afficher les produits uniquement pour les groupes clients B2B. A défaut, le client faisant partie d\'un groupe particulier ne verra pas ce produit si la case n\'est pas cochée. Cette règle s\'applique à tous les groupes clients.
<li><u>le mode Insérer</u> permet de mettre des produits qui seront liés entre eux. Dans ce cas, vous créez qu\'une seule relation entre les produits (l\'esclave s\affichera dans maitre).
<li><u>Le mode Réprocité</u> permet de créer une liaison entre les 2 produits (maitre et esclave).</li>
<li><u>Le mode Héritage</u> permet de créer plusieurs enregistrements avec le même produit maitre et différents produits esclaves. Pour utiliser cette fonction, vous devez avoir plusieurs produits créés. L\'affichage se fait uniquement que dans un sens maitre - esclave (l\'esclave s\'affichera dans maitre).</li>
<li>Cliquez sur actualiser pour revenir sur la page générale pour visualiser l\'ensemble des produits enregistrés.</li>
');	   
define('TITLE_PRODUCTS_RELATED_IMAGE','Aide sur les produits croisés et similaires');
?>
