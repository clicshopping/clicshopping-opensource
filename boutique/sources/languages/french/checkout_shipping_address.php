<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Commande');
define('NAVBAR_TITLE_2', 'Changer l\'adresse d\'expédition');

define('HEADING_TITLE', 'Information expédition');

define('TABLE_HEADING_SHIPPING_ADDRESS', 'Adresse d\'expédition');
define('TEXT_SELECTED_SHIPPING_DESTINATION', 'Veuillez choisir dans votre carnet d\'adresses l\'adresse o&ucirc; vous voudriez que les articles soient livrés.');
define('TITLE_SHIPPING_ADDRESS', 'Adresse d\'expédition :');

define('TABLE_HEADING_ADDRESS_BOOK_ENTRIES', 'Carnet d\'adresses');
define('TEXT_SELECT_OTHER_SHIPPING_DESTINATION', 'Veuillez choisir l\'adresse d\'expédition  si les articles de cette commande doivent être livrés ailleurs.');
define('TITLE_PLEASE_SELECT', 'Veuillez choisir');

define('TABLE_HEADING_NEW_SHIPPING_ADDRESS', 'Nouvelle adresse d\'expédition');
define('TEXT_CREATE_NEW_SHIPPING_ADDRESS', 'Veuillez utiliser ce formulaire pour créer une nouvelle adresse d\'expédition pour cette commande.');

define('TABLE_HEADING_NEW_SHIPPING_ADDRESS_PROBLEM', 'Erreur sur la nouvelle adresse de livraison');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continuer la commande');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'pour choisir la méthode préférée d\'expédition');
?>