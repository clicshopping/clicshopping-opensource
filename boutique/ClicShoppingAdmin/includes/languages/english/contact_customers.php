<?php
/**
 * contact_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Contact customers services');

define('TABLE_HEADING_REF', 'id contact');
define('TABLE_HEADING_DEPARTMENT', 'Department');
define('TABLE_HEADING_DATE_ADDED', 'Date Added');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_CUSTOMERS_NAME','Customer name');						
define('TABLE_HEADING_CUSTOMERS_EMAIL','Email');
define('TABLE_HEADING_CUSTOMERS_LANGUAGE','Language');
define('TABLE_HEADING_STATUS','Ask Status');
define('TABLE_HEADING_ARCHIVE','Message archived');
define('TABLE_HEADING_USER_NAME','Realised by');
define('TABLE_HEADING_CUSTOMERS_RESPONSE','Response');
define('TABLE_HEADING_DATE_SENDING','Date sending');
define('TABLE_HEADING_CUSTOMERS_ID','Customers reference');

define('TITLE_REVIEWS_GENERAL', 'General information general');
define('TITLE_REVIEWS_ENTRY', 'Response to customer');

define('SUBJECT_EMAIL', STORE_NAME .' : Resquest response');

define('TEXT_INFO_MESSAGE','<br />We remain at your disposal for further information. Please do not reply to this email and go through the contact form on the site for all information requests');

define('ENTRY_ID_CUSTOMERS', 'Id contact :');	
define('ENTRY_CUSTOMER_ID','Customer reference :');
define('ENTRY_DEPARTMENT', 'Department :');	
define('ENTRY_CUSTOMERS_NAME','Customer name :');	
define('ENTRY_EMAIL_SUBJECT','Subjet :');	
define('ENTRY_DATE_ADDED','Date receiving :');	
define('ENTRY_CUSTOMERS_MESSAGE','Customer message :');	
define('ENTRY_ARCHIVE','Archived message :');
define('ENTRY_REVIEW', 'Answer to customer :');
define('ENTRY_STATUS_MESSAGE','Message status');
define('ENTRY_STATUS_MESSAGE_NOT_REALISED', 'Message processed');
define('ENTRY_STATUS_MESSAGE_REALISED', 'Message unprocessed');
define('ENTRY_ARCHIVE_YES', 'Yes');
define('ENTRY_ARCHIVE_NO', 'No');
define('ENTRY_CUSTOMERS_TELEPHONE','T�l�phon� :');


define('TAB_HISTORY','History');

define('TABLE_HEADING_LAST_MODIFIED','Modified date');
define('ICON_PREVIEW_COMMENT','Response preview');
define('TEXT_INFO_DELETE_REVIEW_INTRO', 'Are you sure you want to delete this  request and this historic ?');
define('TEXT_INFO_DATE_ADDED', 'Date Added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last Modified:');

define('TEXT_INFO_HEADING_DELETE_REVIEW', 'Delete request');

?>