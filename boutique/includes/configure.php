<?php
/**
 * configure.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: configure.php 
*/

  define('HTTP_SERVER', '');
  define('HTTPS_SERVER', '');
  define('ENABLE_SSL', false);
  define('HTTP_COOKIE_DOMAIN', '');
  define('HTTPS_COOKIE_DOMAIN', '');
  define('HTTP_COOKIE_PATH', '');
  define('HTTPS_COOKIE_PATH', '');
  define('DIR_WS_HTTP_CATALOG', '');
  define('DIR_WS_HTTPS_CATALOG', '');

  define('DIR_WS_SOURCES','sources/');  
  define('DIR_WS_TEMPLATE', DIR_WS_SOURCES . 'template/');

  define('DIR_WS_IMAGES', DIR_WS_SOURCES . 'image/');
  define('DIR_WS_CATALOG_PRODUCTS_IMAGES',  DIR_WS_SOURCES);

  define('DIR_WS_ICONS', DIR_WS_IMAGES . 'icons/');
  define('DIR_WS_DEFAULT_IMAGES', DIR_WS_IMAGES . 'default/');

  define('DIR_WS_INCLUDES', 'includes/');
  define('DIR_WS_BOXES', '/boxes/');
  define('DIR_WS_TEMPLATE_MODULES', '/modules/');
  define('DIR_WS_TEMPLATE_GRAPHISM', '/graphism/');
  define('DIR_WS_TEMPLATE_FILES', '/files/');

  define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
  define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
  define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');
  define('DIR_WS_LANGUAGES', DIR_WS_SOURCES . 'languages/');

  define('DIR_WS_EXT','ext/');
  define('DIR_WS_DOWNLOAD_PUBLIC', 'pub/');
  define('DIR_FS_CATALOG', dirname($_SERVER['SCRIPT_FILENAME']) . '/');

  define('DIR_FS_DOWNLOAD', DIR_FS_CATALOG . DIR_WS_SOURCES . 'download/');
  define('DIR_FS_DOWNLOAD_PUBLIC', DIR_FS_CATALOG . 'pub/');

// define our database connection
  define('DB_DRIVER', 'mysql_standard');
  define('DB_DATABASE_TYPE', 'mysql');
  define('DB_SERVER', '');
  define('DB_SERVER_USERNAME', '');
  define('DB_SERVER_PASSWORD', '');
  define('DB_DATABASE', '');
  define('DB_TABLE_PREFIX', '');
  define('USE_PCONNECT', 'false'); // use persistent connections?
  define('STORE_SESSIONS', 'mysql');

//  define('DIR_FS_CACHE', DIR_FS_CATALOG . 'cache/');
  define('DIR_FS_CACHE_PUBLIC', DIR_FS_CATALOG . 'cache/public/');
  define('DIR_FS_CACHE2', DIR_FS_CATALOG. 'cache/work/');

  define('CFG_TIME_ZONE', 'America/New_York');
?>
