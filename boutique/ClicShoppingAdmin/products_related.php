<?php
/**
 * products_related.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  $languages = osc_get_languages();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $products_id_view = $_GET['products_id_view'];
  $products_related_id = $_GET['products_related_id'];
  $products_related_id_master = osc_db_prepare_input($_GET['products_related_id_master']);
  $products_related_id_slave = osc_db_prepare_input($_GET['products_related_id_slave']);
  $products_related_sort_order = osc_db_prepare_input($_GET['products_related_sort_order']);
  $products_cross_sell = osc_db_prepare_input($_GET['products_cross_sell']);
  $products_related = osc_db_prepare_input($_GET['products_related']);
  $products_mode_b2b = osc_db_prepare_input($_GET['products_mode_b2b']);

  if ($products_related_id_master) { 
      $products_id_view = $products_related_id_master; 
  }

  if (osc_not_null($action)) {
    $page_info = '';
    if (isset($_GET['attribute_page'])) $page_info .= 'attribute_page=' . $_GET['attribute_page'] . '&';
    if (osc_not_null($page_info)) {
      $page_info = substr($page_info, 0, -1);
    }

  switch ($action) {
      case 'setflag_product_cross_sell_status':
        osc_set_products_cross_sell_status($_GET['products_related_id_master'],$_GET['flag_cross'], $_GET['products_related_id']);
        osc_redirect(osc_href_link('products_related.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'products_related_master_id='.$_GET['products_related_id_master'].'&products_related_id='.  $_GET['products_related_id'] . '&attribute_page=' . $_GET['attribute_page']));
      break;

      case 'setflag_products_related_status':
        osc_set_products_related_status($_GET['products_related_id_master'],$_GET['flag_related'], $_GET['products_related_id']);
        osc_redirect(osc_href_link('products_related.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'products_related_master_id='.$_GET['products_related_id_master'].'&products_related_id='.  $_GET['products_related_id'] . '&attribute_page=' . $_GET['attribute_page']));
      break;

      case 'setflag_products_b2b_status':
        osc_set_products_mode_b2b_status($_GET['products_related_id_master'],$_GET['flag_b2b'], $_GET['products_related_id']);
        osc_redirect(osc_href_link('products_related.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'products_related_master_id='.$_GET['products_related_id_master'].'&products_related_id='.  $_GET['products_related_id'] . '&attribute_page=' . $_GET['attribute_page']));
      break;	

      case 'Insert':
        if ($products_related_id_master != $products_related_id_slave) {

          $Qcheck = $OSCOM_PDO->prepare('select products_related_id
                                         from :table_products_related
                                         where products_related_id_master = :products_related_id_master
                                         and products_related_id_slave = :products_related_id_slave
                                        ');

          $Qcheck->bindInt(':products_related_id_master', (int)$products_related_id_master);
          $Qcheck->bindInt(':products_related_id_slave', (int)$products_related_id_slave);
          $Qcheck->execute();

          if ($Qcheck->fetch() === false) {

            osc_db_query("insert into products_related
                          values ('', 
                                  '" . (int)$products_related_id_master . "', 
                                  '" . (int)$products_related_id_slave . "', 
                                  '" . (int)$products_related_sort_order."',
                                  '" . (int)$products_cross_sell . "',
                                  '" . (int)$products_related . "',
                                  '" . (int)$products_mode_b2b . "'
                                 )
                         ");
          }
        }
        osc_redirect(osc_href_link('products_related.php', $page_info.'&products_related_id_master='.$products_related_id_master.'&products_related_id_slave='.$products_related_id_slave.'&products_id_view='.$products_id_view));
        break;

      case 'Reciprocate':
        if ($products_related_id_master != $products_related_id_slave) {

          $Qcheck = $OSCOM_PDO->prepare('select products_related_id
                                         from :table_products_related
                                         where products_related_id_master = :products_related_id_master
                                         and products_related_id_slave = :products_related_id_slave
                                        ');

          $Qcheck->bindInt(':products_related_id_master', (int)$products_related_id_master);
          $Qcheck->bindInt(':products_related_id_slave', (int)$products_related_id_slave);
          $Qcheck->execute();

          if ($Qcheck->fetch() === false) {
            osc_db_query("insert into products_related
                          values ('', 
                                  '" . (int)$products_related_id_master . "', 
                                  '" . (int)$products_related_id_slave . "', 
                                  '" . (int)$products_related_sort_order."',
                                  '" . (int)$products_cross_sell . "',
                                  '" . (int)$products_related . "',
                                  '" . (int)$products_mode_b2b . "')                                 
                          ");
          }

          $Qcheck = $OSCOM_PDO->prepare('select products_related_id
                                         from :table_products_related 
                                         where products_related_id_master = :products_related_id_master
                                         and products_related_id_slave = :products_related_id_slave
                                      ');

          $Qcheck->bindInt(':products_related_id_master', (int)$products_related_id_slave);
          $Qcheck->bindInt(':products_related_id_slave', (int)$products_related_id_master);
          $Qcheck->execute();

          if ($Qcheck->fetch() === false) {

            osc_db_query("insert into products_related
                          values ('', 
                                  '" . (int)$products_related_id_slave . "', 
                                  '" . (int)$products_related_id_master  . "', 
                                  '" . (int)$products_related_sort_order."',
                                  '" . (int)$products_cross_sell . "',
                                  '" . (int)$products_related . "',
                                  '" . (int)$products_mode_b2b . "')
                          ");
          }
        }
        osc_redirect(osc_href_link('products_related.php', $page_info.'&products_related_id_master='.$products_related_id_master.'&products_related_id_slave='.$products_related_id_slave.'&products_id_view='.$products_id_view));
        break;

      case 'Inherit':

        if ($products_related_id_master != $products_related_id_slave) {

          $Qcheck = $OSCOM_PDO->prepare('select products_related_id
                                         from :table_products_related 
                                         where products_related_id_master = :products_related_id_master
                                         and products_related_id_slave = :products_related_id_slave
                                      ');

          $Qcheck->bindInt(':products_related_id_master', (int)$products_related_id_master);
          $Qcheck->bindInt(':products_related_id_slave', (int)$products_related_id_master);
          $Qcheck->execute();

          if ($Qcheck->fetch() === false) {

             osc_db_query("insert into products_related
                           values ('',
                                   '" . (int)$products_related_id_master . "',
                                   '" . (int)$products_related_id_slave . "',
                                   '" . (int)$products_related_sort_order."',
                                   '" . (int)$products_cross_sell . "',
                                   '" . (int)$products_related . "',
                                   '" . (int)$products_mode_b2b . "'
                                   )
                          ");
            }


          $Qcheck = $OSCOM_PDO->prepare('select products_related_id
                                         from :table_products_related
                                         where products_related_id_master = :products_related_id_master
                                         order by products_related_id
                                      ');

          $Qcheck->bindInt(':products_related_id_master', (int)$products_related_id_master);
          $Qcheck->bindInt(':products_related_id_slave', (int)$products_related_id_master);
          $Qcheck->execute();


          while ($products_values = $Qcheck->fetch() ) {
            $products_related_id_slave2 = $products_values['products_related_id_slave'];
            if ($products_related_id_master != $products_related_id_slave2) {

              $Qcheck = $OSCOM_PDO->prepare('select products_related_id
                                             from :table_products_related
                                             where products_related_id_master = :products_related_id_master
                                             and products_related_id_slave = :products_related_id_slave
                                          ');

              $Qcheck->bindInt(':products_related_id_master', (int)$products_related_id_master);
              $Qcheck->bindInt(':products_related_id_slave', (int)$products_related_id_slave2);
              $Qcheck->execute();

              if ($Qcheck->fetch() === false) {

                osc_db_query("insert into products_related
                             values ('',
                                     '" . (int)$products_related_id_master . "',
                                     '" . (int)$products_related_id_slave . "',
                                     '" . (int)$products_related_sort_order."',
                                     '" . (int)$products_cross_sell . "',
                                     '" . (int)$products_related . "',
                                     '" . (int)$products_mode_b2b . "'
                                    )
                           ");
              }
            }
          }
        }
        osc_redirect(osc_href_link('products_related.php', $page_info.'&products_related_id_master='.$products_related_id_master.'&products_related_id_slave='.$products_related_id_slave.'&products_id_view='.$products_id_view));
        break;

      case 'update_product_attribute':

        $products_related_id = osc_db_prepare_input($_GET['products_related_id']);

        $Qupdate = $OSCOM_PDO->prepare('update :table_products_related
                                        set products_related_id_master = :products_related_id_master, 
                                            products_related_id_slave = :products_related_id_slave, 
                                            products_related_sort_order = :products_related_sort_order,
                                            products_cross_sell = :products_cross_sell,
                                            products_related = :products_related,
                                            products_mode_b2b = :products_mode_b2b
                                        where products_related_id = :products_related_id
                                      ');
        $Qupdate->bindInt(':products_related_id_master', (int)$products_related_id_master );
        $Qupdate->bindInt(':products_related_id_slave', (int)$products_related_id_slave );
        $Qupdate->bindInt(':products_related_sort_order', (int)$products_related_sort_order );
        $Qupdate->bindInt(':products_cross_sell', (int)$products_cross_sell );
        $Qupdate->bindInt(':products_related', (int)$products_related );
        $Qupdate->bindInt(':products_mode_b2b',  (int)$products_mode_b2b );
        $Qupdate->bindInt(':products_related_id', (int)$products_related_id  );

        $Qupdate->execute();
/*
        if (USE_CACHE == 'true') {
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
        }
*/
        osc_redirect(osc_href_link('products_related.php', $page_info.'&products_id_view='.$products_id_view));
        break;

      case 'delete_attribute':
        $products_related_id = osc_db_prepare_input($_GET['products_related_id']);

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_products_related
                                        where products_related_id = :products_related_id 
                                      ');
        $Qdelete->bindInt(':products_related_id',   (int)$products_related_id );
        $Qdelete->execute();
/*
        if (USE_CACHE == 'true') {
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
        }
*/
        osc_redirect(osc_href_link('products_related.php', $page_info.'&products_id_view='.$products_id_view));
        break;
    }
  }

  require('includes/header.php');

?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<!-- body_text //-->
      <div class="adminTitle">
         <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_related.gif', HEADING_TITLE, '40', '40'); ?></span>
         <span class="col-md-5 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
         <span class="col-md-2">
            <div class="form-group">
              <div class="controls">
<?php
  echo osc_draw_form('search', 'products_related.php', '', 'get');
  echo osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');
?>
              </div>
            </div>
         </span>

<?php
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
?>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('products_related.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></span>
<?php
    }
    echo osc_hide_session_id(); 
?>
           </form>
<?php
    if ($products_related_id !='' || $_GET['products_related_id_master'] != '') {
?>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('products_related.php') . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
<?php
    }
    echo osc_hide_session_id(); 
?>
            <span>
              <form name="formview"><select name="products_id_view" onchange="return formview.submit();">
<?php
    echo '<option name="show_all_products" value="">'.SHOW_ALL_PRODUCTS .'</option>';

    $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                             p.products_model,
                                             pd.products_name
                                      from :table_products p,
                                           :table_products_description pd
                                      where pd.products_id = p.products_id
                                      and pd.language_id = :language_id
                                      and p.products_archive = :products_archive
                                      order by  p.products_model,
                                                pd.products_name
                                    ');

    $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qproducts->bindInt(':products_archive', 0);
    $Qproducts->execute();

    while ($products_values = $Qproducts->fetch() ) {

        $model = $products_values['products_model'].' - ';
        $name = $products_values['products_name'];

        if ($products_id_view == $products_values['products_id']) {
          echo '<option name="' . $name . '" value="' . $products_values['products_id'] . '" SELECTED>' . $model . $name . '</option>';
        } else {
          echo '<option name="' . $name . '" value="' . $products_values['products_id'] . '">' . $model . $name . '</option>';
        }
    }
?>
            </select>
            <?php echo osc_hide_session_id(); ?>
          </form>
        </span>
      </div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<!-- //################################################################################################################ -->
<!-- //                                      Update Attributes                                       -->
<!-- //################################################################################################################ -->
<?php

/*
//debugage
foreach ($_GET as $key => $value) {
   echo "Key: $key; Value: $value<br />\n";
}
*/
  if ($action == 'update_attribute') {
    $form_action = 'update_product_attribute';
  } else {
    $form_action = 'add_product_attributes';
  }

  if (!isset($attribute_page)) {
    $attribute_page = 1;
  }

  
  $form_params = 'action=' . $form_action . '&option_page=' . $option_page . '&value_page=' . $value_page . '&attribute_page=' . $attribute_page;
?>




<?php
// Recherche
    $search = '';
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "where  (pd.products_name like '" . $keywords . "' 
                         or p.products_model like '%" . $keywords . "%'
                         ) 
                ";
    }

// definition requete sql
    $attributes = "select distinct pa.products_related_id, 
                                   pa.products_related_id_master,
                                   pa.products_related_id_slave,
                                   pa.products_related_sort_order,
                                   pa.products_related,
                                   pa.products_cross_sell,
                                   pa.products_mode_b2b
                   from products_related pa left join products_description pd ON pa.products_related_id_master = pd.products_id
                                            left join products p on pa.products_related_id_master = p.products_id
                        " . $search . "  
                   and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
         ";

    if ($products_id_view) { 
     $attributes .= " where pd.products_id = '$products_id_view'";
  }

    $attributes .= "order by pd.products_name, 
                     pa.products_related_sort_order,
                     pa.products_related_id
                   ";

    $attribute_query = osc_db_query($attributes);

    $per_page = MAX_DISPLAY_SEARCH_RESULTS_ADMIN;


    $attribute_page_start = ($per_page * $attribute_page) - $per_page;
    $num_rows = osc_db_num_rows($attribute_query);

    if ($num_rows <= $per_page) {
      $num_pages = 1;
    } else if (($num_rows % $per_page) == 0) {
      $num_pages = ($num_rows / $per_page);
    } else {
      $num_pages = ($num_rows / $per_page) + 1;
    }
      $num_pages = (int) $num_pages;

     $attributes = $attributes . " limit $attribute_page_start, $per_page";

     $view_id = '';
  
     if ($products_id_view) {
       $products_id_view = $products_related_id_master?$products_related_id_master:$products_id_view;
       $view_id = '&products_id_view=' . $products_id_view;
     }
?>
  <form name="attributes" action="<?php echo osc_href_link('products_related.php', $form_params); ?>" method="get">

      <div class="adminformTitle" style="padding-top: 0px; padding-left: 0px;">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr class="dataTableHeadingRow">
            <td align="center" class="dataTableHeadingContent"><?php echo TABLE_HEADING_ID; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCT_FROM; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCT_TO; ?></td>
            <td align="center" class="dataTableHeadingContent"><?php echo TABLE_HEADING_CROSS_SELL_PRODUCTS; ?></td>
            <td align="center" class="dataTableHeadingContent"><?php echo TABLE_HEADING_RELATED_PRODUCTS; ?></td>
            <td align="center" class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER; ?></td>
<?php 
   if (MODE_B2B_B2C =='true') {
?>            
            <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_CUSTOMERS_GROUP; ?></td>
<?php
   }
?>
            <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_ACTION; ?></td>
          </tr>
<?php

    $next_id = 1;
  
  
// requete sql avec moteur de recherche ligne 310 
  $attributes_query = osc_db_query($attributes);

  while ($attributes_values = osc_db_fetch_array($attributes_query)) {
    $products_name_master = osc_get_products_name($attributes_values['products_related_id_master']);
    $products_name_slave = osc_get_products_name($attributes_values['products_related_id_slave']);

    $mModel = osc_get_products_model($attributes_values['products_related_id_master']) . ' - ';
    $sModel = osc_get_products_model($attributes_values['products_related_id_slave']). ' - ';
//      $mModel = $sModel = '';

    $products_related_sort_order = $attributes_values['products_related_sort_order'];
    $products_related = $attributes_values['products_related'];
    $products_cross_sell = $attributes_values['products_cross_sell'];
    $products_mode_b2b = $attributes_values['products_mode_b2b'];

    $rows++;
?>
<!-- //################################################################################################################ -->
<!-- //                                      Update Attributes                                                          -->
<!-- //################################################################################################################ -->
          <tr  class="dataTableRow">
<?php
    if (($action == 'update_attribute') && ($_GET['products_related_id'] == $attributes_values['products_related_id'])) {
?>
            <td  class="dataTableContent">&nbsp;<?php echo $attributes_values['products_related_id']; ?><?php echo osc_draw_hidden_field('products_related_id',  $attributes_values['products_related_id']); ?>&nbsp;</td>
            <td  class="dataTableContent">&nbsp;<select name="products_related_id_master">
<?php
      $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                                 p.products_model,
                                                 pd.products_name
                                          from :table_products p,
                                               :table_products_description pd
                                          where pd.products_id = p.products_id
                                          and pd.language_id = :language_id
                                          and p.products_archive = :products_archive
                                          order by  p.products_model,
                                                    pd.products_name
                                        ');

      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qproducts->bindInt(':products_archive', 0);
      $Qproducts->execute();

      while($products_values = $Qproducts->fetch() ) {
        $model = $products_values['products_model'].' - ';
        $name = $products_values['products_name'];
        $product_name = $name;

        if ($attributes_values['products_related_id_master'] == $products_values['products_id']) {
          echo "\n" . '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '" SELECTED>' . $model . $product_name . '</option>';
        } else {
          echo "\n" . '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">' . $model . $product_name . '</option>';
        }
      }
?>
            </select>&nbsp;</td>
            <td  class="dataTableContent">&nbsp;<select name="products_related_id_slave">
<?php
      $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                               p.products_model,
                                               pd.products_name
                                        from :table_products p,
                                             :table_products_description pd
                                        where pd.products_id = p.products_id
                                        and pd.language_id = :language_id
                                        and p.products_archive = :products_archive
                                        order by  p.products_model,
                                                  pd.products_name
                                       ');

      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qproducts->bindInt(':products_archive', 0);
      $Qproducts->execute();

      while($products_values_slave = $Qproducts->fetch() ) {
        $model = $products_values_slave['products_model'] .' - ';
        $name = $products_values_slave['products_name'];
        $product_name = $name;

        if ($attributes_values['products_related_id_slave'] == $products_values_slave['products_id']) {
          echo "\n" . '<option name="' . $products_values_slave['products_name'] . '" value="' . $products_values_slave['products_id'] . '" SELECTED>' . $model . $product_name . '</option>';
        } else {
          echo "\n" . '<option name="' . $products_values_slave['products_name'] . '" value="' . $products_values_slave['products_id'] . '">' . $model . $product_name . '</option>';
        }

      }
?>
            </select>&nbsp;
          </td>

<?php 
  if ($products_related =='1') {
    $checkbox ='true';
  } else {
    $checkbox ='false';
  }
    
  if ($attributes_values['products_cross_sell'] =='1') {
    $checkbox1 ='true';
  } else {
    $checkbox1 ='false';
  }
   
   if ($products_mode_b2b =='1') {
    $checkbox2 ='true';
  } else {
    $checkbox2 ='false';
  }    
?>
           <td align="center"  class="dataTableContent"><?php echo osc_draw_checkbox_field('products_cross_sell',$attributes_values['products_cross_sell'], $checkbox1); ?></td>
           <td align="center"  class="dataTableContent"><?php echo osc_draw_checkbox_field('products_related', $attributes_values['products_related'], $checkbox); ?></td>
           <td align="center"  class="dataTableContent"><?php echo osc_draw_input_field('products_related_sort_order',  $attributes_values['products_related_sort_order'], 'size="3"'); ?></td>
<?php 
   if (MODE_B2B_B2C =='true') {
?>            
             <td align="center"  class="dataTableContent"><?php echo osc_draw_checkbox_field('products_mode_b2b', $attributes_values['products_mode_b2b'], $checkbox2); ?></td>            
<?php 
  }    
?>
             <td align="center"  class="dataTableContent" width="190"><?php echo osc_draw_hidden_field('action', 'update_product_attribute'); ?><?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE); ?>&nbsp;&nbsp;<?php echo '<a href="' . osc_href_link('products_related.php', '&attribute_page=' . $attribute_page . '&products_id_view='.$products_id_view) . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL); ?></a></td>
<?php
    } else {
?>
<!-- //################################################################################################################ -->
<!-- //                                     list des liaisons                                                 -->
<!-- //################################################################################################################ -->

            <td class="dataTableContent">&nbsp;<?php echo $attributes_values['products_related_id']; ?>&nbsp;</td>
            <td class="dataTableContent">&nbsp;<?php echo $mModel ?><?php echo $products_name_master; ?>&nbsp;</td>
            <td class="dataTableContent">&nbsp;<?php echo $sModel ?><?php echo $products_name_slave; ?>&nbsp;</td>
            <td class="dataTableContent" align="center">
<?php
      if ($products_cross_sell == '1') {
        echo '<a href="' . osc_href_link('products_related.php', 'action=setflag_product_cross_sell_status&flag_cross=0&products_related_id_master=' .$attributes_values['products_related_id_master'] .'&products_related_id=' .$attributes_values['products_related_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('products_related.php', 'action=setflag_product_cross_sell_status&flag_cross=1&products_related_id_master=' .$attributes_values['products_related_id_master'] .'&products_related_id=' .$attributes_values['products_related_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
            <td class="dataTableContent" align="center"> 
<?php
      if ($products_related == '1') {
        echo '<a href="' . osc_href_link('products_related.php', 'action=setflag_products_related_status&flag_related=0&products_related_id_master=' .$attributes_values['products_related_id_master'] .'&products_related_id=' .$attributes_values['products_related_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('products_related.php', 'action=setflag_products_related_status&flag_related=1&products_related_id_master=' .$attributes_values['products_related_id_master'] .'&products_related_id=' .$attributes_values['products_related_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }

?>
            </td>
            <td align="center"  class="dataTableContent"><?php echo $products_related_sort_order; ?></td>
<?php 
      if (MODE_B2B_B2C =='true') {
?>
            <td class="dataTableContent" align="center"> 
<?php
        if ($products_mode_b2b != '0') {
          echo '<a href="' . osc_href_link('products_related.php', 'action=setflag_products_b2b_status&flag_b2b=0&products_related_id_master=' .$attributes_values['products_related_id_master'] .'&products_related_id=' .$attributes_values['products_related_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
        } else {
          echo '<a href="' . osc_href_link('products_related.php', 'action=setflag_products_b2b_status&flag_b2b=1&products_related_id_master=' .$attributes_values['products_related_id_master'] .'&products_related_id=' .$attributes_values['products_related_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
        }
?>
            </td>
<?php
    }
?>
            <td class="dataTableContent"  align="center">
<?php
   $params = 'action=update_attribute&products_related_id='  . $attributes_values['products_related_id'] . '&attribute_page=' . $attribute_page . '&products_id_view=' . $products_id_view;
    echo '<a href="' . osc_href_link('products_related.php', $params) . '">'; ?><?php echo  osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT); ?></a>&nbsp;
<?php
   $params = 'action=delete_attribute&products_related_id='  . $attributes_values['products_related_id'] . '&attribute_page=' . $attribute_page . '&products_id_view=' . $products_id_view;
?>
              <a href="<?php echo osc_href_link('products_related.php', $params); ?>" onclick="return confirm('<?php echo sprintf(TEXT_CONFIRM_DELETE_ATTRIBUTE, addslashes($products_name_slave), addslashes($products_name_master)); ?>');"><?php echo osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE); ?></a>
             </td>
<?php
    }

    $QmaxAttributesId = $OSCOM_PDO->prepare('select max(products_related_id) + 1 as next_id
                                               from :table_products_related
                                  ');
    $QmaxAttributesId->execute();

    $max_attributes_id_values = $QmaxAttributesId->fetch();

    $next_id = $max_attributes_id_values['next_id'];
?>
          </tr>
<!-- //################################################################################################################ -->
<!-- //                                    Insert data                                                    -->
<!-- //################################################################################################################ -->

<?php
  }
  if ($action != 'update_attribute') {
?>
          <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'dataTableRowSelected' : 'dataTableRow'); ?>">
<?php          
/*
      if (isset($sInfo) && is_object($sInfo) && ($products_heart['products_heart_id'] == $sInfo->products_heart_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('products_heart.php', 'page=' . $_GET['page'] . '&sID=' . $sInfo->products_heart_id . '&action=edit') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('products_heart.php', 'page=' . $_GET['page'] . '&sID=' . $products_heart['products_heart_id']) . '\'">' . "\n";
      }
*/
?>

            <td class="smallText">&nbsp;<?php echo $next_id; ?>&nbsp;</td>
            <td class="smallText"><strong>A:</strong>&nbsp;
              <select name="products_related_id_master">
<?php
  $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                               p.products_model,
                                               pd.products_name
                                        from :table_products p,
                                             :table_products_description pd
                                        where pd.products_id = p.products_id
                                        and pd.language_id = :language_id
                                        and p.products_archive = :products_archive
                                        order by  p.products_model,
                                                  pd.products_name
                                       ');

  $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $Qproducts->bindInt(':products_archive', 0);
  $Qproducts->execute();

    $products_related_id_master = $_GET['products_related_id_master'];

    if (!$products_related_id_master)  {
      $products_related_id_master = $products_id_view;
    }
    
    while ($products_values = $Qproducts->fetch() ) {
      $model = $products_values['products_model'] . ' - ';
      $name = $products_values['products_name'];
      $product_name = $name;

    if ($products_related_id_master == $products_values['products_id']) {
        echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '" SELECTED>' . $model . $product_name . '</option>';
      } else {
       echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">' . $model . $product_name . '</option>';
      }
    }
?>
             </select>&nbsp;
            </td>
            <td class="smallText"><strong>B:</strong>&nbsp;
              <select name="products_related_id_slave">
<?php
    $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                                 p.products_model,
                                                 pd.products_name
                                          from :table_products p,
                                               :table_products_description pd
                                          where pd.products_id = p.products_id
                                          and pd.language_id = :language_id
                                          and p.products_archive = :products_archive
                                          order by  p.products_model,
                                                    pd.products_name
                                         ');

    $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qproducts->bindInt(':products_archive', 0);
    $Qproducts->execute();

     while ($products_values = $Qproducts->fetch() ) {
       $model = $products_values['products_model'] .' - ';
       $name = $products_values['products_name'];
       $product_name = $name;
       if ($_GET['products_related_id_slave'] == $products_values['products_id']) {
         echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '" SELECTED>' . $model . $product_name . '</option>';
       } else {
         echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">' . $model . $product_name . '</option>';
       }
     }
?>
              </select>&nbsp;
            </td>
            
             <td align="center"  class="dataTableContent"><?php echo osc_draw_checkbox_field('products_cross_sell', '1', false); ?></td>
             <td align="center"  class="dataTableContent"><?php echo osc_draw_checkbox_field('products_related', '1', false); ?></td>            
             <td align="center"  class="dataTableContent"><?php echo osc_draw_input_field('products_related_sort_order', '', 'size="3"'); ?></td>
<?php 
      if (MODE_B2B_B2C =='true') {
?>
             <td align="center"  class="dataTableContent"><?php echo osc_draw_checkbox_field('products_mode_b2b', '1', false); ?></td>
<?php
      }
?>
              <td  align="center" class="smallText">
                <input type="submit" name="action" value="Insert">
                <input type="submit" name="action" value="Reciprocate">
                <input type="submit" name="action" value="Inherit">
              </td>
          </tr>
<?php
  }

  echo osc_draw_hidden_field('products_id_view', $products_id_view);
  echo osc_hide_session_id() . '</form>';
?>

           </td>
         </tr>
       </table>
    </div>
  </form>

  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminformAide">
    <div class="row">
      <span class="col-md-12">
        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_PRODUCTS_RELATED_IMAGE); ?>
        <strong><?php echo '&nbsp;' . TITLE_PRODUCTS_RELATED_IMAGE; ?></strong>
      </span>
    </div>
    <div class="spaceRow"></div>
    <div class="row">
      <span class="col-md-12">
        <?php echo TEXT_AIDE_RELATED_PRODUCTS_CONTENT; ?>
      </span>
    </div>
  </div>
  <?php require('includes/footer.php'); ?>
</body>
</html>
<?php
  require('includes/application_bottom.php');