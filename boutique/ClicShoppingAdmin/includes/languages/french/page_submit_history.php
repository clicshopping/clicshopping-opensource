<?php
/**
 * page_submit_history.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE_SUBMIT_HISTORY','Historique d\'Analyse des métadonnées');

define('TAB_KEYWORDS_GOOGLE','Historique Google');
define('TAB_SUBMIT_CATEGORIES','Catégories');
define('TAB_SUBMIT_PRODUCTS_INFO','Description produits');
define('TAB_SUBMIT_PRODUCTS_NEW','Nouveautés');
define('TAB_SUBMIT_SPECIAL','Promotions');
define('TAB_SUBMIT_REVIEWS','Commentaires');
define('TAB_SUBMIT_SEO','Positionnement');
define('TAB_SUBMIT_RANKING','Page Rank & Densité');


define('TEXT_DELETE','Supprimer l\'historique de recherche');
define('TEXT_DATE','Date de la recherche');
define('TEXT_URL','Url du site');
define('TEXT_RANK','Rang');
define('TEXT_WORD','Mots clefs');

?>
