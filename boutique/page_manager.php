<?php
/*
 * page_manager.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
  
*/

  require('includes/application_top.php');

// Recuperation de la valeur id de order.php
  if (isset($_GET['pages_id'])) {
    $id = (int)$_GET['pages_id'];
  } else {
    osc_redirect('index.php');
  }

// Recuperation de la page d'information
  $page_manager->osc_display_page_manager('informations', $id);

  if ($page_manager->pages_informations['pages_id'] != '0') {

  require($OSCOM_Template->getTemplateFiles('page_manager'));

// Duplication application_bottom
    require('includes/application_bottom.php');
  } else {
    $url = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'index.php';
    header('Location: ' . $url);
  }

