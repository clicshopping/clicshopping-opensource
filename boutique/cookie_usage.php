<?php
/*
 * cookie_usage.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('cookie_usage'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('cookie_usage.php'));

  require($OSCOM_Template->getTemplateFiles('cookie_usage'));
