<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Nous contacter');
define('NAVBAR_TITLE', 'Nous contacter');
define('TEXT_SUCCESS', 'Votre demande a été envoyée avec succès au service client '.STORE_NAME.'.');

define('TEXT_ADMINISTRATOR_DEPARTMENT','Aucun département défini');

define('ENTRY_CUSTOMERS_ID','Votre numéro Client : ');
define('ENTRY_ORDER','Votre numéro de commande : ');
define('ENTRY_DATE','Date : ');
define('ENTRY_CUSTOMERS_PHONE','Téléphone : ');

define('ENTRY_NEED_CUSTOMERS',' Veuillez indiquer le service à contacter ?');
define('ENTRY_CUSTOMER', 'Question du client : ');

define('ENTRY_NOTE_REGISTERED','Veuillez nous indiquer la référence du produit ou toute information pertinente sur le problème rencontré concernant la commande que vous avez effectuée<br />');
define('ENTRY_NOTE_NO_REGISTERED','Si votre demande concerne une commande déjà effectuée, veuillez cliquez sur : <a href="' . osc_href_link('account.php', '', 'SSL') . '"><u>Accèder à mon compte</u></a> puis choisir votre numéro de commande');

define('ENTRY_INFORMATION_ADMIN','Une question relative à une commande d\'un client de '.STORE_NAME.' vous est demandée. '."\n\n".' Veuillez vous connecter à votre panel d\'administration (section commande) pour répondre à cette demande. '."\n\n".' En attendant le traitement de cette question, les statuts ont ètè modifiès et remis à leur ètat d\'origine'."\n\n".'Veuillez trouver ci-joint l\'information concernant le numéro de commande du client : ');
define('ENTRY_ADDITIONAL_INFORMATION','Pour toute correspondance avec nos services, veuillez passer par le formulaire de contact.');

define('EMAIL_TEXT_LAW', '<p style="font-size: 7px; text-decoration: underline;">Avis de confidentialité :</p>' . "\n\n" . '<p style="font-size:7px;">Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans être le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez reçu ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n\n" . '
Conformément à la Loi dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</p>');

define('ENTRY_NAME', 'Nom et Prénom :');
define('ENTRY_EMAIL', 'Adresse E-mail :');
define('ENTRY_ENQUIRY', 'Demande de renseignement :');
define('ENTRY_ENQUIRY_CUSTOMER', 'Votre message : ');
define('ENTRY_ENQUIRY_CUSTOMER_INFORMATION', 'Message du client: ');
define('ENTRY_EMAIL_OBJECT_CUSTOMER','Copie de votre demande ');
define('ENTRY_CUSTOMERS_SUBJECT','Sujet de votre demande : ');

define('ENTRY_NUMBER_EMAIL_CONFIRMATION','Combien font : ');
define('SEND_DEPARTMENT_COMPANY','Dèpartement à contacter :  ');

define('ERROR_ACTION_RECORDER', 'Erreur : Votre demande a déjà été envoyée. Veuillez réessayer dans %s minutes.');
?>