<?php
/**
 * products_attributes.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Product Options');

define('HEADING_TITLE_OPT', 'Step 1 : Product options name');
define('HEADING_TITLE_VAL', 'Step 2 : Option values for the product');
define('HEADING_TITLE_ATRIB', 'Step 3 : Define the products Attributes');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_REF_ATTRIBUTES','Products attribut REF');
define('TABLE_HEADING_PRODUCT', 'Product Name');
define('TABLE_HEADING_OPT_NAME', 'Option Name');
define('TABLE_HEADING_OPT_VALUE', 'Option Value');
define('TABLE_HEADING_OPT_PRICE', 'Value Price');
define('TABLE_HEADING_OPT_PRICE_PREFIX', 'Prefix');
define('TABLE_HEADING_OPT_ORDER','Catalog display order');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_DOWNLOAD', 'Downloadable products:');
define('TABLE_TEXT_FILENAME', 'Filename:');
define('TABLE_TEXT_MAX_DAYS', 'Expiry days:');
define('TABLE_TEXT_MAX_COUNT', 'Maximum download count:');
define('HEADING_TITLE_CLONE_PRODUCTS_ATTRIBUTES','Step 4 :Clone / Copy attributs');
define('CLONE_PRODUCTS_FROM','from ');
define('CLONE_PRODUCTS_TO','to ');

define('TITLE_AIDE_ATTRIBUTS', 'Note on the Attributs');

$upload_max_filesize =  @ini_get('upload_max_filesize');
define('TEXT_AIDE_ATTRIBUTS', '<ul><strong><u>Attributs Prefix </u></strong> :<br /><br /><li> - The syntax "+" allow to multiply the attribut by the quantity and add to the price</li> <li> - The syntax "-" allow to multiply  the attribut by the quantity and add the price</li><br /><li> - Don\'t forget to configure the order status concerning the upload files.</li><li> - The upload file can\'t exceed  : ' . $upload_max_filesize .'</li>');

define('TITLE_AIDE_CLONE', 'Note on the Clone');
define('TEXT_AIDE_CLONE', '<ul><strong><u>Copy/ Clone attributs</u></strong> : <br /><br /><li>You can copy one or several products attributs on one or several others products</li><li>To delete one or several attributs, you must choose a product and select one or several products.<br />Click on delete and the attributs will be deleted</li></ul>');

define('TEXT_WARNING_OF_DELETE', 'This option has products and values linked to it - it is not safe to delete it.');
define('TEXT_OK_TO_DELETE', 'This option has no products and values linked to it - it is safe to delete it.');
define('TEXT_OPTION_ID', 'Option ID');
define('TEXT_OPTION_NAME', 'Option Name');

define('ONGLET_STEP1','Step 1');
define('ONGLET_STEP2','Step 2');
define('ONGLET_STEP3','Step 3');
define('ONGLET_STEP4','Step 4');
?>