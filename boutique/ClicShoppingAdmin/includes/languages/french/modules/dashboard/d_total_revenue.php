<?php
/**
 * d_total_revenue.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_TITLE', 'Total du CA sur les 30 derniers jours <br />(statut en attente, en cours et livré)');
define('MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_DESCRIPTION', 'Graphique permettant de voir l\'évolution du chiffre d\'affaires sur 30 jours (statut en attente, en cours et livré) basé en fonction du sous-total');
define('MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_CHART_LINK', 'Total CA journalier');


