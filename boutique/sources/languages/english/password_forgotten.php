<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Login');
define('NAVBAR_TITLE_2', 'Password Forgotten');

define('HEADING_TITLE', 'I\'ve Forgotten My Password!');

define('TEXT_MAIN', 'If you\'ve forgotten your password, enter your e-mail address below and we\'ll send you instructions on how to securely change your password.');

define('TEXT_PASSWORD_RESET_INITIATED', 'Please check your e-mail for instructions on how to change your password. The instructions contain a link that is valid only for 24 hours or until your password has been updated.');

define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Error: The E-Mail Address was not found in our records, please try again.');
define('TEXT_NO_CUSTOMER_LEVEL', 'Your account was not validated by our sales department, which does not enable us to send your new parameters of connection to you. Do not hesitate to contact our support for all complementary request for information.');

define('EMAIL_PASSWORD_RESET_SUBJECT', STORE_NAME . ' - New Password');
define('EMAIL_PASSWORD_RESET_BODY', 'A new password has been requested for your account at ' . STORE_NAME . '.' . "\n\n" . 'Please follow this personal link to securely change your password:' . "\n\n" . '%s' . "\n\n" . 'This link will be automatically discarded after 24 hours or after your password has been changed.' . "\n\n" . 'For help with any of our online services, please email the store-owner: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");

define('EMAIL_PASSWORD_REMINDER_SUBJECT', ' '. STORE_NAME . ' - Sending of your new password of access');

define('EMAIL_PASSWORD_REMINDER_BODY', 'Hello, ' . "\n\n" . 'Please find below your new password to connect you on ' . STORE_NAME . "\n\n" . 'Your username : ' . $email_address . "\n\n" . 'Your new password : %s' . "\n". '
Regards,'. "\n" . '<strong>'.STORE_NAME.' Team</strong>'. "\n" . '
'.STORE_NAME_ADDRESS.''. "\n" . '
<p style="font-size:10px;"><u>Confidentiality Note :</u>'. "\n" . '
This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '
In accordance with the <strong>Law</strong> on "Data-processing Law and Freedom", you are entitled to correction of your personal data or on request by email</p>');

define('SUCCESS_PASSWORD_SENT', 'An email containing your password will be sent to you in the next minutes...');
define('ERROR_ACTION_RECORDER', 'Error: A password reset link has already been sent. Please try again in %s minutes.');
?>
