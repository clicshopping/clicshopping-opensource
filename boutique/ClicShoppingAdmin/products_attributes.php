<?php
/**
 * products_attributes.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  $languages = osc_get_languages();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $option_page = (isset($_GET['option_page']) && is_numeric($_GET['option_page'])) ? $_GET['option_page'] : 1;
  $value_page = (isset($_GET['value_page']) && is_numeric($_GET['value_page'])) ? $_GET['value_page'] : 1;
  $attribute_page = (isset($_GET['attribute_page']) && is_numeric($_GET['attribute_page'])) ? $_GET['attribute_page'] : 1;
  $page_info = 'option_page=' . $option_page . '&value_page=' . $value_page . '&attribute_page=' . $attribute_page;

  if (osc_not_null($action)) {
    switch ($action) {

     case 'clone_attributes':
      $multi_clone_products_id_to = $_POST['clone_products_id_to'];

      for ($i=0;$i<sizeof($multi_clone_products_id_to);$i++) {
        $clone_product_id_from = $_POST['clone_products_id_from'];
        $clone_product_id_to = $multi_clone_products_id_to[$i];

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_attributes
                                        where products_id = :products_id
                                        ');
        $Qdelete->bindInt(':products_id', (int)$clone_product_id_to );

        $Qdelete->execute();

        $Qattributes = $OSCOM_PDO->prepare("select products_id,
                                           options_id,
                                           options_values_id,
                                           options_values_price,
                                           price_prefix,
                                           products_options_sort_order
                                     from products_attributes
                                     where products_id = :products_id
                                ");
        $Qattributes->bindInt(':products_id', (int)$clone_product_id_from);
        $Qattributes->execute();

        while($attributes_values =  $Qattributes->fetch() ) {

          $OSCOM_PDO->save('products_attributes', [
                                                    'products_id' =>  (int)$clone_product_id_to,
                                                    'options_id' => (int)$attributes_values['options_id'],
                                                    'options_values_id' => (int)$attributes_values['options_values_id'],
                                                    'options_values_price' => $attributes_values['options_values_price'],
                                                    'price_prefix' => $attributes_values['price_prefix'],
                                                    'products_options_sort_order' => (int)$attributes_values['products_options_sort_order'],
                                                    'products_attributes_reference' => ''
                                                  ]
                          );
        }
      }
    break;
    case 'add_product_options':
        $products_options_id = osc_db_prepare_input($_POST['products_options_id']);
        $option_name_array = $_POST['option_name'];
        $option_sort_order = $_POST['option_sort_order'];

        for ($i=0, $n=sizeof($languages); $i<$n; $i ++) {
          $option_name = osc_db_prepare_input($option_name_array[$languages[$i]['id']]);

          $OSCOM_PDO->save('products_options', [
                                                'products_options_id' =>  (int)$products_options_id,
                                                'products_options_name' => $option_name,
                                                'language_id' => (int)$languages[$i]['id'],
                                                'products_options_sort_order' => (int)$option_sort_order
                                                ]
                          );
        }
        osc_redirect(osc_href_link('products_attributes.php', $page_info));
        break;

      case 'add_product_option_values':
        $value_name_array = $_POST['value_name'];
        $value_id = osc_db_prepare_input($_POST['value_id']);
        $option_id = osc_db_prepare_input($_POST['option_id']);

        for ($i=0, $n=sizeof($languages); $i<$n; $i ++) {
          $value_name = osc_db_prepare_input($value_name_array[$languages[$i]['id']]);

          $OSCOM_PDO->save('products_options_values', [
                                                        'products_options_values_id' =>  (int)$value_id,
                                                        'language_id' => (int)$languages[$i]['id'],
                                                        'products_options_values_name' => $value_name
                                                      ]
                          );

        }

        $OSCOM_PDO->save('products_options_values_to_products_options', [
                                                                          'products_options_id' =>  (int)$option_id,
                                                                          'products_options_values_id' => (int)$value_id
                                                                        ]
                        );

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
        break;

      case 'add_product_attributes':
        $products_id = osc_db_prepare_input($_POST['products_id']);
        $options_id = osc_db_prepare_input($_POST['options_id']);
        $values_id = osc_db_prepare_input($_POST['values_id']);
        $value_price = osc_db_prepare_input($_POST['value_price']);
        $price_prefix = osc_db_prepare_input($_POST['price_prefix']);
        $value_sort_order = osc_db_prepare_input($_POST['value_sort_order']);
        $products_attributes_reference  = osc_db_prepare_input($_POST['products_attributes_reference']);   

        osc_db_query("insert into products_attributes
                      values (null, 
                              '" . (int)$products_id . "', 
                              '" . (int)$options_id . "', 
                              '" . (int)$values_id . "', 
                              '" . (float)osc_db_input($value_price) . "', 
                              '" . osc_db_input($price_prefix) . "',
                              '" . (int)$value_sort_order . "',
                              '" . osc_db_input($products_attributes_reference) . "'
                             )
                    ");

        if (DOWNLOAD_ENABLED == 'true') {
          $products_attributes_id = osc_db_insert_id();

          $products_attributes_filename = osc_db_prepare_input($_POST['products_attributes_filename']);
          $products_attributes_maxdays = osc_db_prepare_input($_POST['products_attributes_maxdays']);
          $products_attributes_maxcount = osc_db_prepare_input($_POST['products_attributes_maxcount']);

// ########## Bouton parcourir pour les fichiers Download ##########

          if ($new_products_attributes_filename = new upload('new_products_attributes_filename', DIR_FS_DOWNLOAD)) {
            if (isset($new_products_attributes_filename->filename)) {
//new_products_attributes_filename = 
              osc_db_query("insert into products_attributes_download
                            values (" . (int)$products_attributes_id . ", 
                                    '" . osc_db_input($new_products_attributes_filename->filename) . "', 
                                    '" . osc_db_input($products_attributes_maxdays) . "', 
                                    '" . osc_db_input($products_attributes_maxcount) . "'
                                    )
                           ");
            } else {


              if (osc_not_null($products_attributes_filename)) {
               osc_db_query("insert into products_attributes_download
                             values (" . (int)$products_attributes_id . ", 
                                     '" . osc_db_input($products_attributes_filename) . "', 
                                     '" . osc_db_input($products_attributes_maxdays) . "', 
                                     '" . osc_db_input($products_attributes_maxcount) . "'
                                    )                         
                          ");
              }
            }
// ####### END - Bouton parcourir pour les fichiers Download #######

          }
        }

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
        break;

      case 'update_option_name':
        $option_name_array = $_POST['option_name'];
        $option_sort_order = $_POST['option_sort_order'];
        $option_id = osc_db_prepare_input($_POST['option_id']);

        for ($i=0, $n=sizeof($languages); $i<$n; $i ++) {
          $option_name = osc_db_prepare_input($option_name_array[$languages[$i]['id']]);

          $Qupdate = $OSCOM_PDO->prepare('update products_options
                                          set products_options_name = :products_options_name,
                                              products_options_sort_order = :products_options_sort_order
                                          where products_options_id = :products_options_id
                                          and language_id = :language_id
                                      ');
          $Qupdate->bindValue(':products_options_name', $option_name);
          $Qupdate->bindInt(':products_options_sort_order', $value_id);
          $Qupdate->bindInt(':products_options_id', (int)$option_id );
          $Qupdate->bindInt(':language_id', (int)$languages[$i]['id']);
          $Qupdate->execute();

        }

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
        break;
      case 'update_value':
        $value_name_array = $_POST['value_name'];
        $value_id = osc_db_prepare_input($_POST['value_id']);
        $option_id = osc_db_prepare_input($_POST['option_id']);

        for ($i=0, $n=sizeof($languages); $i<$n; $i ++) {
          $value_name = osc_db_prepare_input($value_name_array[$languages[$i]['id']]);

          $Qupdate = $OSCOM_PDO->prepare('update :table_products_options_values
                                          set products_options_values_name = :products_options_values_name
                                          where products_options_values_id = :products_options_values_id
                                          and language_id = :language_id
                                      ');
          $Qupdate->bindValue(':products_options_values_name', $value_name);
          $Qupdate->bindInt(':products_options_values_id', $value_id);
          $Qupdate->bindInt(':language_id', (int)$languages[$i]['id']);
          $Qupdate->execute();

        }

        $Qupdate = $OSCOM_PDO->prepare('update :table_products_options_values_to_products_options
                                        set products_options_id = :products_options_id
                                        where products_options_values_id = :products_options_values_id
                                        ');

        $Qupdate->bindInt(':products_options_id', (int)$option_id );
        $Qupdate->bindInt(':products_options_values_id', (int)$value_id);
        $Qupdate->execute();

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
      break;

      case 'update_product_attribute':
        $products_id = osc_db_prepare_input($_POST['products_id']);
        $options_id = osc_db_prepare_input($_POST['options_id']);
        $values_id = osc_db_prepare_input($_POST['values_id']);
        $value_price = osc_db_prepare_input($_POST['value_price']);
        $price_prefix = osc_db_prepare_input($_POST['price_prefix']);
        $value_sort_order = osc_db_prepare_input($_POST['value_sort_order']);
        $attribute_id = osc_db_prepare_input($_POST['attribute_id']);
        $products_attributes_reference = osc_db_prepare_input($_POST['products_attributes_reference']);


        $Qupdate = $OSCOM_PDO->prepare('update :table_products_attributes
                                        set products_id = :products_id,
                                            options_id = :options_id,
                                            options_values_id = :options_values_id,
                                            options_values_price = :options_values_price,
                                            price_prefix = :price_prefix,
                                            products_options_sort_order = :products_options_sort_order,
                                            products_attributes_reference = :products_attributes_reference
                                        where products_attributes_id =:products_attributes_id
                                      ');

        $Qupdate->bindInt(':products_id', (int)$products_id);
        $Qupdate->bindInt(':options_id', (int)$options_id);
        $Qupdate->bindInt(':options_values_id', (int)$values_id);
        $Qupdate->bindValue(':options_values_price', (float)$value_price);
        $Qupdate->bindValue(':price_prefix', $price_prefix);
        $Qupdate->bindInt(':products_options_sort_order', $value_sort_order);
        $Qupdate->bindValue(':products_attributes_reference', $products_attributes_reference);
        $Qupdate->bindInt(':products_attributes_id', (int)$attribute_id);
        $Qupdate->execute();


        if (DOWNLOAD_ENABLED == 'true') {
          $products_attributes_filename = osc_db_prepare_input($_POST['products_attributes_filename']);
          $products_attributes_maxdays = osc_db_prepare_input($_POST['products_attributes_maxdays']);
          $products_attributes_maxcount = osc_db_prepare_input($_POST['products_attributes_maxcount']);

          if ($new_products_attributes_filename = new upload('new_products_attributes_filename', DIR_FS_DOWNLOAD)) {
            if (isset($new_products_attributes_filename->filename)) {

              osc_db_query("replace into products_attributes_download
                            set products_attributes_id = '" . (int)$attribute_id . "', 
                                products_attributes_filename = '" . $new_products_attributes_filename->filename . "', 
                                products_attributes_maxdays = '" . osc_db_input($products_attributes_maxdays) . "', 
                                products_attributes_maxcount = '" . osc_db_input($products_attributes_maxcount) . "'
                          ");
            } else {

              $QproductsFilename = $OSCOM_PDO->prepare('select products_attributes_id,
                                                              products_attributes_filename
                                                       from :table_products_attributes_download
                                                       where products_attributes_id = :products_attributes_id
                                                      ');
              $QproductsFilename->bindInt(':products_attributes_id', (int)$attribute_id );
              $QproductsFilename->execute();

              $products_filename = $QproductsFilename->fetch();

              osc_db_query("replace into products_attributes_download
                            set products_attributes_id = '" . (int)$attribute_id . "', 
                                products_attributes_filename = '" . $products_filename['products_attributes_filename'] . "', 
                                products_attributes_maxdays = '" . osc_db_input($products_attributes_maxdays) . "', 
                                products_attributes_maxcount = '" . osc_db_input($products_attributes_maxcount) . "'
                          ");

             }
           }

          if (osc_not_null($products_attributes_filename)) {

            osc_db_query("replace into products_attributes_download
                          set products_attributes_id = '" . (int)$attribute_id . "', 
                          products_attributes_filename = '" . osc_db_input($products_attributes_filename) . "',
                          products_attributes_maxdays = '" . osc_db_input($products_attributes_maxdays) . "',
                          products_attributes_maxcount = '" . osc_db_input($products_attributes_maxcount) . "'
                         ");
          }
        }

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
      break;

      case 'delete_option':
        $option_id = osc_db_prepare_input($_GET['option_id']);

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_options
                                        where products_options_id = :products_options_id
                                        ');
        $Qdelete->bindInt(':products_options_id', (int)$option_id );
        $Qdelete->execute();

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
      break;

      case 'delete_value':
        $value_id = osc_db_prepare_input($_GET['value_id']);

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_options_values
                                        where products_options_values_id = :products_options_values_id
                                        ');
        $Qdelete->bindInt(':products_options_values_id', (int)$value_id );
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_options_values_to_products_options
                                        where products_options_values_id = :products_options_values_id
                                        ');
        $Qdelete->bindInt(':products_options_values_id', (int)$value_id );
        $Qdelete->execute();

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
      break;
      case 'delete_attribute':
        $attribute_id = osc_db_prepare_input($_GET['attribute_id']);

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_attributes
                                        where products_attributes_id = :products_attributes_id
                                        ');
        $Qdelete->bindInt(':products_attributes_id', (int)$attribute_id );
        $Qdelete->execute();

// added for DOWNLOAD_ENABLED. Always try to remove attributes, even if downloads are no longer enabled
        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_pproducts_attributes_download
                                        where products_attributes_id = :products_attributes_id
                                        ');
        $Qdelete->bindInt(':products_attributes_id', (int)$attribute_id );
        $Qdelete->execute();

        osc_redirect(osc_href_link('products_attributes.php', $page_info));
      break;
    }
  }

  require('includes/header.php');
?>

<script language="javascript"><!--
function go_option() {
  if (document.option_order_by.selected.options[document.option_order_by.selected.selectedIndex].value != "none") {
    location = "<?php echo osc_href_link('products_attributes.php', 'option_page=' . ($_GET['option_page'] ? $_GET['option_page'] : 1)); ?>&option_order_by="+document.option_order_by.selected.options[document.option_order_by.selected.selectedIndex].value;
  }
}
//--></script>
<!-- body //-->
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div>
  <div class="adminTitle">
    <span><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_attributes.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
</div>

  <div style="padding-left: 10px;">
    <ul class="nav nav-tabs" role="tablist"  id="myTab">
      <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo ONGLET_STEP1; ?></a></li>
      <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo ONGLET_STEP2; ?></a></li>
      <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo ONGLET_STEP3; ?></a></li>
      <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo ONGLET_STEP4; ?></a></li>
    </ul>
    <div class="tabsClicShopping">
        <div class="tab-content">
<!-- //########################################################################################## -->
<!-- //                  Option des produit : etape 1                                            -->
<!-- //######################################################################################### -->
        <div class="tab-pane active" id="tab1">
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
<!-- options //-->
<?php
  if ($action == 'delete_product_option') { // delete product option

    $Qoptions = $OSCOM_PDO->prepare('select products_options_id,
                                            products_options_name
                                     from :table_products_options
                                     where products_options_id = :products_options_id
                                     and language_id = :language_id
                                    ');
    $Qoptions->bindInt(':products_options_id',(int)$_GET['option_id'] );
    $Qoptions->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
    $Qoptions->execute();

    $options_values = $Qoptions->fetch();
?>
           <tr>
             <td class="pageHeading">&nbsp;</td>
           </tr>
           <tr>
             <td class="pageHeading">&nbsp;<?php echo $options_values['products_options_name']; ?>&nbsp;</td>
           </tr>
           <tr>
              <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                <tr>
                  <td colspan="3"><?php echo osc_black_line(); ?></td>
                </tr>
<?php
    $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                           p.products_model,
                                           pd.products_name,
                                           pov.products_options_values_name
                                    from :table_products p,
                                         :table_products_options_values pov,
                                         :table_products_attributes pa,
                                         :table_products_description pd
                                    where pd.products_id = p.products_id
                                    and pov.language_id = :language_id
                                    and pd.language_id = :language_id
                                    and pa.products_id = p.products_id
                                    and pa.options_id = :options_id
                                    and pov.products_options_values_id = pa.options_values_id
                                    order by pd.products_name
                                   ');
    $Qproducts->bindInt(':language_id',  (int)$_SESSION['languages_id']);
    $Qproducts->bindInt(':options_id',  (int)$_GET['option_id']);

    $Qproducts->execute();


    if ($Qproducts->fetch() !== false) {
?>
                <tr class="dataTableHeadingRow">
                  <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_ID; ?>&nbsp;</td>
                  <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_PRODUCT; ?>&nbsp;</td>
                  <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_OPT_VALUE; ?>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3"><?php echo osc_black_line(); ?></td>
                </tr>
<?php
      $rows = 0;
      while ($products_values = $Qproducts->fetch() ) {
        $rows++;
?>
                <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
                  <td align="center" class="smallText">&nbsp;<?php echo $products_values['products_id']; ?>&nbsp;</td>
                  <td class="smallText">&nbsp;<?php echo $products_values['products_name']; ?>&nbsp;</td>
                  <td class="smallText">&nbsp;<?php echo $products_values['products_options_values_name']; ?>&nbsp;</td>
                </tr>
<?php
      }
?>
                <tr>
                  <td colspan="3"><?php echo osc_black_line(); ?></td>
                </tr>
                <tr>
                 <td colspan="3" class="main"><br /><?php echo TEXT_WARNING_OF_DELETE; ?></td>
                </tr>
                <tr>
                  <td align="right" colspan="3" class="main"><br /><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info) . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', ' cancel '); ?></a>&nbsp;</td>
               </tr>
<?php
    } else {
?>
               <tr>
                 <td class="main" colspan="3"><br /><?php echo TEXT_OK_TO_DELETE; ?></td>
               </tr>
               <tr>
                 <td class="main" align="right" colspan="3"><br /><?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=delete_option&option_id=' . $_GET['option_id']) . '">'; ?><?php echo osc_image_button('button_delete.gif', ' delete '); ?></a>&nbsp;&nbsp;&nbsp;<?php echo '<a href="' . osc_href_link('products_attributes.php', (isset($_GET['order_by']) ? 'order_by=' . $_GET['order_by'] . '&' : '') . (isset($_GET['page']) ? 'page=' . $_GET['page'] : '')) . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', ' cancel '); ?></a>&nbsp;</td>
               </tr>
<?php
    }
?>
              </table></td>
           <tr>
<?php
  } else {
?>
           <tr>
             <td colspan="7"><table width="100%" cellpadding="0" cellspacing="2">
               <tr>
                 <td><table width="100%" cellpadding="0" cellspacing="0">
                   <tr>
                     <td class="pageHeading" width="80%">&nbsp;<?php echo HEADING_TITLE_OPT; ?>&nbsp;
                     <td class="smallText" width="20%" align="right">
<?php
    $options = "select * from products_options
                where language_id = '" . (int)$_SESSION['languages_id'] . "' 
                order by products_options_id
              ";
    $options_split = new splitPageResults($option_page, MAX_ROW_LISTS_OPTIONS, $options, $options_query_numrows);
    echo $options_split->display_links($options_query_numrows, MAX_ROW_LISTS_OPTIONS, MAX_DISPLAY_PAGE_LINKS, $option_page, 'value_page=' . $value_page . '&attribute_page=' . $attribute_page .'#tab1', 'option_page');
?>
                     </td>
                   </tr>
                 </table></td>
               </tr>
             </table></td>
           </tr>
           <tr>
             <td colspan="7"><?php echo osc_black_line(); ?></td>
           </tr>
           <tr class="dataTableHeadingRow">
             <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_ID; ?>&nbsp;</td>
             <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_OPT_NAME; ?>&nbsp;</td>
             <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_OPT_ORDER; ?>&nbsp;</td>
             <td class="dataTableHeadingContent" align="center" colspan="2">&nbsp;<?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>

           </tr>
           <tr>
             <td colspan="7"><?php echo osc_black_line(); ?></td>
           </tr>
<?php
    $next_id = 1;
    $rows = 0;
    $options = osc_db_query($options);
    while ($options_values = osc_db_fetch_array($options)) {
      $rows++;
?>
           <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
<?php
      if (($action == 'update_option') && ($_GET['option_id'] == $options_values['products_options_id'])) {

        echo '<form name="option" action="' . osc_href_link('products_attributes.php', 'action=update_option_name&' . $page_info . '&DHTMLSuite_active_tab=0') . '" method="post">'. osc_draw_hidden_field('DHTMLSuite_active_tab', '0');
 
        $inputs = '';

        for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {

          $QoptionsName = $OSCOM_PDO->prepare('select products_options_name
                                               from :table_products_options
                                               where products_options_id = :products_options_id
                                               and language_id = :language_id
                                              ');
          $QoptionsName->bindInt(':products_options_id', $options_values['products_options_id'] );
          $QoptionsName->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
          $QoptionsName->execute();

          $option_name = $QoptionsName->fetch();

          $inputs .= $languages[$i]['code'] . ':&nbsp;<input type="text" name="option_name[' . $languages[$i]['id'] . ']" size="20" value="' . $option_name['products_options_name'] . '">&nbsp;<br />';
        }
?>
             <td align="center" class="smallText">&nbsp;<?php echo $options_values['products_options_id']. osc_draw_hidden_field('option_id', $options_values['products_options_id']); ?>&nbsp;</td>
             <td class="smallText"><?php echo $inputs; ?></td>
             <td class="smallText"><input type="text" name="option_sort_order" size="3" value="<?php echo $options_values['products_options_sort_order']; ?>"></td>
             <td align="right">&nbsp;<?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE); ?>&nbsp;</td>
             <td><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info) . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL); ?></a>&nbsp;</td>
<?php
        echo '</form>' . "\n";
      } else {
?>
             <td align="center" class="smallText">&nbsp;<?php echo $options_values['products_options_id']; ?>&nbsp;</td>
             <td class="smallText">&nbsp;<?php echo $options_values['products_options_name']; ?>&nbsp;</td>
             <td class="smallText"><?php echo $options_values['products_options_sort_order']; ?></td>
             <td align="right">&nbsp;<?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=update_option&option_id=' . $options_values['products_options_id'] . '&' . $page_info) . '">'; ?><?php echo osc_image_button('button_edit.gif', IMAGE_UPDATE); ?></a>&nbsp;</td>
             <td><?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=delete_product_option&option_id=' . $options_values['products_options_id'] . '&' . $page_info) , '">'; ?><?php echo osc_image_button('button_delete.gif', IMAGE_DELETE); ?></a>&nbsp;</td>
<?php
      }
?>
           </tr>
<?php
      $QmaxOptionsId = $OSCOM_PDO->prepare('select max(products_options_id) + 1 as next_id
                                            from :table_products_options
                                          ');
      $QmaxOptionsId->execute();

      $max_options_id_values = $QmaxOptionsId->fetch();

      $next_id = $max_options_id_values['next_id'];
    }
?>
           <tr>
             <td colspan="7"><?php echo osc_black_line(); ?></td>
           </tr>
<?php
    if ($action != 'update_option') {
?>
           <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
<?php
      echo '<form name="options" action="' . osc_href_link('products_attributes.php', 'action=add_product_options&' . $page_info) . '" method="post">'. osc_draw_hidden_field('products_options_id', $next_id);
      $inputs = '';
      for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {
        $inputs .= $languages[$i]['code'] . ':&nbsp;<input type="text" name="option_name[' . $languages[$i]['id'] . ']" size="20">&nbsp;<br />';
      }
?>
             <td align="center" class="smallText">&nbsp;<?php echo $next_id; ?>&nbsp;</td>
             <td class="smallText"><?php echo $inputs; ?></td>
             <td class="smallText"><input type="text" name="option_sort_order" size="3"></td>
             <td align="center" class="smallText">&nbsp;<?php echo osc_image_submit('button_insert.gif', IMAGE_INSERT); ?>&nbsp;</td>
<?php
      echo '</form>';
?>
           </tr>
           <tr>
             <td colspan="7"><?php echo osc_black_line(); ?></td>
           </tr>
<?php
    }
  }
?>
           </table></td>
         </tr>
       </table></td>
     </div>

<!-- //########################################################################################## -->
<!-- //                  Valeur des Options des produits : etape 2                                -->
<!-- //######################################################################################### -->
     <div class="tab-pane" id="tab2">
       <table width="100%" border="0" width="100%"  cellspacing="0" cellpadding="2">
         <tr>
           <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
<?php
  if ($action == 'delete_option_value') { // delete product option value

    $Qvalues = $OSCOM_PDO->prepare('select products_options_values_id,
                                           products_options_values_name
                                     from :table_products_options_values
                                     where products_options_values_id = :products_options_values_id
                                     and language_id = :language_id
                                          ');

    $Qvalues->bindInt(':language_id',  (int)$_SESSION['languages_id']);
    $Qvalues->bindInt(':products_options_values_id',  (int)$_GET['value_id']);
    $Qvalues->execute();

    $values_values = $Qvalues->fetch();
?>
             <tr>
                <td class="pageHeading">&nbsp;</td>
             </tr>
             <tr>
               <td colspan="4" class="pageHeading">&nbsp;<?php echo $values_values['products_options_values_name']; ?>&nbsp;</td>
               <td>&nbsp;<?php echo osc_image(DIR_WS_IMAGES . 'pixel_trans.gif', '', '1', '53'); ?>&nbsp;</td>
             </tr>
             <tr>
               <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                 <tr>
                   <td colspan="5"><?php echo osc_black_line(); ?></td>
                 </tr>
<?php
    $products = $OSCOM_PDO->prepare('select p.products_id,
                                             pd.products_name,
                                             po.products_options_name
                                      from :table_products p,
                                           :table_products_attributes pa,
                                           :table_products_options po,
                                           :table_products_description pd
                                     where pd.products_id = p.products_id
                                     and pd.language_id = :language_id
                                     and po.language_id = :language_id
                                     and pa.products_id = p.products_id
                                     and pa.options_values_id = :options_values_id
                                     and po.products_options_id = pa.options_id
                                     order by pd.products_name
                                    ');
    $products->bindInt(':language_id',  (int)$_SESSION['languages_id']);
    $products->bindInt(':options_values_id',  (int)$_GET['value_id']);
    $products->execute();

    if ($products->fetch() !== false) {
?>
                 <tr class="dataTableHeadingRow">
                   <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_ID; ?>&nbsp;</td>
                   <td class="dataTableHeadingContent" align="center">&nbsp;<?php  echo TABLE_HEADING_PRODUCT; ?>&nbsp;</td>
                   <td class="dataTableHeadingContent">&nbsp;<?php  echo TABLE_HEADING_OPT_ORDER; ?>&nbsp;</td>
                 </tr>
                 <tr>
                   <td colspan="3"><?php echo osc_black_line(); ?></td>
                 </tr>
<?php
      while ($products_values = $products->fetch() ) {
        $rows++;
?>
                 <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
                   <td align="center" class="smallText">&nbsp;<?php echo $products_values['products_id']; ?>&nbsp;</td>
                   <td class="smallText">&nbsp;<?php echo $products_values['products_name']; ?>&nbsp;</td>
                   <td class="smallText">&nbsp;<?php echo $products_values['products_options_name']; ?>&nbsp;</td>
                 </tr>
<?php
      }
?>
                 <tr>
                   <td colspan="7"><?php echo osc_black_line(); ?></td>
                 </tr>
                 <tr>
                   <td class="main" colspan="4"><br /><?php echo TEXT_WARNING_OF_DELETE; ?></td>
                 </tr>
                 <tr>
                   <td class="main" align="right" colspan="4"><br /><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info . '#tab2'). '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', ' cancel '); ?></a>&nbsp;</td>
                 </tr>
<?php
    } else {
?>
                 <tr>
                   <td class="main" colspan="4"><br /><?php echo TEXT_OK_TO_DELETE; ?></td>
                 </tr>
                 <tr>
                   <td class="main" align="right" colspan="4"><?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=delete_value&value_id=' . $_GET['value_id'] . '&' . $page_info . '#tab2').'">'; ?><?php echo osc_image_button('button_delete.gif', ' delete '); ?></a>&nbsp;</td>
                   <td><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info) . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', ' cancel '); ?></a>&nbsp;</td>
                 </tr>
<?php
    }
?>
               </table></td>
             </tr>
<?php
  } else {
?>
              <tr>
                <td colspan="5"><table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td><table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="pageHeading" width="80%">&nbsp;<?php echo HEADING_TITLE_VAL; ?>&nbsp;
                        <td class="smallText" width="20%" align="right">
<?php
    $values = "select pov.products_options_values_id, 
                      pov.products_options_values_name, 
                      pov2po.products_options_id 
               from products_options_values pov left join products_options_values_to_products_options pov2po on pov.products_options_values_id = pov2po.products_options_values_id
               where pov.language_id = '" . (int)$_SESSION['languages_id'] . "'
               order by pov.products_options_values_id";
    $values_split = new splitPageResults($value_page, MAX_ROW_LISTS_OPTIONS, $values, $values_query_numrows);

    echo $values_split->display_links($values_query_numrows, MAX_ROW_LISTS_OPTIONS, MAX_DISPLAY_PAGE_LINKS, $value_page, 'option_page=' . $option_page . '&attribute_page=' . $attribute_page . '#tab2', 'value_page');
?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td colspan="5"><?php echo osc_black_line(); ?></td>
              </tr>
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_ID; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_OPT_NAME; ?>&nbsp;</td>
                <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_OPT_VALUE; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center" colspan="2">&nbsp;<?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
              <tr>
                <td colspan="5"><?php echo osc_black_line(); ?></td>
              </tr>
<?php
    $next_id = 1;
    $rows = 0;
    $values = osc_db_query($values);

    while ($values_values = osc_db_fetch_array($values)) {

      $options_name = osc_options_name($values_values['products_options_id']);
      $values_name = $values_values['products_options_values_name'];
      $rows++;
?>
              <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
<?php
      if (($action == 'update_option_value') && ($_GET['value_id'] == $values_values['products_options_values_id'])) {
        echo '<form name="values" action="' . osc_href_link('products_attributes.php', 'action=update_value&' . $page_info.'#tab2') . '" method="post">';
        $inputs = '';

        for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {

          $QvaluesName = $OSCOM_PDO->prepare('select products_options_values_name
                                              from :table_products_options_values
                                              where products_options_values_id = :products_options_values_id
                                              and language_id = :language_id
                                             ');

          $QvaluesName->bindInt(':language_id',  (int)$_SESSION['languages_id']);
          $QvaluesName->bindInt(':products_options_values_id',  (int)$values_values['products_options_values_id']);
          $QvaluesName->execute();

          $value_name = $QvaluesName->fetch();

          $inputs .= $languages[$i]['code'] . ':&nbsp;<input type="text" name="value_name[' . $languages[$i]['id'] . ']" size="15" value="' . $value_name['products_options_values_name'] . '">&nbsp;<br />';
        }
?>
                <td align="center" class="smallText">&nbsp;<?php echo $values_values['products_options_values_id'] . osc_draw_hidden_field('value_id', $values_values['products_options_values_id']); ?>&nbsp;</td>
                <td align="center" class="smallText">&nbsp;<?php echo "\n"; ?><select name="option_id">
<?php

        $Qoptions = $OSCOM_PDO->prepare('select products_options_id,
                                                products_options_name
                                         from :table_products_options
                                         where language_id = :language_id
                                         order by products_options_name
                                        ');

        $Qoptions->bindInt(':language_id',  (int)$_SESSION['languages_id']);

        $Qoptions->execute();

        while ($options_values =  $Qoptions->fetch() ) {
          echo "\n" . '<option name="' . $options_values['products_options_name'] . '" value="' . $options_values['products_options_id'] . '"';
          if ($values_values['products_options_id'] == $options_values['products_options_id']) { 
            echo ' selected';
          }
          echo '>' . $options_values['products_options_name'] . '</option>';
        } 
?>
                </select>&nbsp;</td>
                <td class="smallText"><?php echo $inputs; ?></td>
                <td align="right">&nbsp;<?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE); ?>&nbsp;</td>
                <td><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info.'#tab2') . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL); ?></a>&nbsp;</td>
<?php
        echo '</form>';
      } else {
?>
                <td align="center" class="smallText">&nbsp;<?php echo $values_values["products_options_values_id"]; ?>&nbsp;</td>
                <td align="center" class="smallText">&nbsp;<?php echo $options_name; ?>&nbsp;</td>
                <td class="smallText">&nbsp;<?php echo $values_name; ?>&nbsp;</td>
                <td align="right">&nbsp;<?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=update_option_value&value_id=' . $values_values['products_options_values_id'].'#tab2') . '">'; ?><?php echo osc_image_button('button_edit.gif', IMAGE_UPDATE); ?></a>&nbsp;</td>
                <td><?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=delete_option_value&value_id=' . $values_values['products_options_values_id'] . '&' . $page_info.'#tab2') , '">'; ?><?php echo osc_image_button('button_delete.gif', IMAGE_DELETE); ?></a>&nbsp;</td>
<?php
      }

      $QmaxValuesId = $OSCOM_PDO->prepare('select max(products_options_values_id) + 1 as next_id
                                           from :table_products_options_values
                                         ');

      $QmaxValuesId->execute();

      $max_values_id_values = $QmaxValuesId->fetch();

      $next_id = $max_values_id_values['next_id'];
    }
?>
              </tr>
              <tr>
                <td colspan="5"><?php echo osc_black_line(); ?></td>
              </tr>
<?php
    if ($action != 'update_option_value') {
?>
              <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
<?php
      echo '<form name="values" action="' . osc_href_link('products_attributes.php', 'action=add_product_option_values&' . $page_info.'#tab2') . '" method="post">';
?>
                <td align="center" class="smallText">&nbsp;<?php echo $next_id; ?>&nbsp;</td>
                <td align="center" class="smallText">
                    <select name="option_id">
<?php
      $Qoptions = $OSCOM_PDO->prepare('select products_options_id,
                                                products_options_name
                                         from :table_products_options
                                         where language_id = :language_id
                                         order by products_options_name
                                         ');
      $Qoptions->bindInt(':language_id',(int)$_SESSION['languages_id'] );
      $Qoptions->execute();

      while ($options_values = $Qoptions->fetch() ) {
        echo '<option name="' . $options_values['products_options_name'] . '" value="' . $options_values['products_options_id'] . '">' . $options_values['products_options_name'] . '</option>';
      }

      $inputs = '';
      for ($i = 0, $n = sizeof($languages); $i < $n; $i ++) {
        $inputs .= $languages[$i]['code'] . ':&nbsp;<input type="text" name="value_name[' . $languages[$i]['id'] . ']" size="15">&nbsp;<br />';
      }
?>
                        </select>&nbsp;
                </td>
                <td class="smallText"><?php echo osc_draw_hidden_field('value_id', $next_id) . $inputs; ?></td>
                <td align="center">&nbsp;<?php echo osc_image_submit('button_insert.gif', IMAGE_INSERT); ?>&nbsp;</td>
<?php
      echo '</form>';
?>
              </tr>
              <tr>
                <td colspan="5"><?php echo osc_black_line(); ?></td>
              </tr>
<?php
    }
  }
?>
           </table></td>
         </tr>
       </table>
     </div>
<!-- //########################################################################################## -->
<!-- //                Definition des attributs produits : etape 3                                -->
<!-- //######################################################################################### -->
     <div class="tab-pane" id="tab3">
       <table width="100%" border="0" width="100%"  cellspacing="0" cellpadding="2">
<?php
  if ($action == 'update_attribute') {
    $form_action = 'update_product_attribute';
  } else {
    $form_action = 'add_product_attributes';
  }
?>
         <tr>
           <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
             <tr>
               <td class="pageHeading">&nbsp;<?php echo HEADING_TITLE_ATRIB; ?>&nbsp;</td>
               <td class="smallText" align="right">
<?php
  $attributes = "select pa.*
                 from products_attributes pa
                 left join products_description pd on pa.products_id = pd.products_id
                and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                order by pd.products_name";

  $attributes_split = new splitPageResults($attribute_page, MAX_ROW_LISTS_OPTIONS, $attributes, $attributes_query_numrows);

// pb ici suivi page option

  echo $attributes_split->display_links($attributes_query_numrows, MAX_ROW_LISTS_OPTIONS, MAX_DISPLAY_PAGE_LINKS, $attribute_page, 'option_page=' . $option_page . '&value_page=' . $value_page .'&tab3', 'attribute_page');
?>
               </td>
             </tr>
           </table></td>
         </tr>
       </table>
       <form name="attributes" action="<?php echo osc_href_link('products_attributes.php', 'action=' . $form_action . '&' . $page_info. '#tab3'); ?>" method="post" enctype="multipart/form-data">

       <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td colspan="10"><?php echo osc_black_line(); ?></td>
          </tr>
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_ID; ?>&nbsp;</td>
            <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_REF_ATTRIBUTES; ?>&nbsp;</td>
            <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_PRODUCT; ?>&nbsp;</td>
            <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_OPT_NAME; ?>&nbsp;</td>
<?php 
      if (DOWNLOAD_ENABLED == 'true') {
?>
            <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_DOWNLOAD; ?>&nbsp;</td>
<?php
      }
?>
            <td class="dataTableHeadingContent">&nbsp;<?php echo TABLE_HEADING_OPT_VALUE; ?>&nbsp;</td>
            <td class="dataTableHeadingContent" align="right">&nbsp;<?php echo TABLE_HEADING_OPT_PRICE; ?>&nbsp;</td>
            <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_OPT_PRICE_PREFIX; ?>&nbsp;</td>
            <td class="dataTableHeadingContent" align="center">&nbsp;<?php echo TABLE_HEADING_OPT_ORDER; ?>&nbsp;</td>
            <td class="dataTableHeadingContent" align="center" colspan="2">&nbsp;<?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="10"><?php echo osc_black_line(); ?></td>
          </tr>
<?php
  $next_id = 1;
  $attributes = osc_db_query($attributes);
  while ($attributes_values = osc_db_fetch_array($attributes)) {
    $products_name_only = osc_get_products_name($attributes_values['products_id']);
    $options_name = osc_options_name($attributes_values['options_id']);
    $values_name = osc_values_name($attributes_values['options_values_id']);
    $rows++;
?>
          <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
<?php
    if (($action == 'update_attribute') && ($_GET['attribute_id'] == $attributes_values['products_attributes_id'])) {
?>
            <td class="smallText">&nbsp;<?php echo $attributes_values['products_attributes_id'] . osc_draw_hidden_field('attribute_id', $attributes_values['products_attributes_id']); ?>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<input type="text" name="products_attributes_reference" value="<?php echo $attributes_values['products_attributes_reference']; ?>"  size="10">&nbsp;</td>
            <td class="smallText">&nbsp;<select name="products_id">
<?php
      $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                               pd.products_name,
                                               p.products_model
                                         from :table_products p,
                                              :table_products_description pd
                                         where pd.products_id = p.products_id
                                         and pd.language_id = :language_id
                                         order by pd.products_name
                                        ');
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->execute();

      while($products_values = $Qproducts->fetch() ) {
        if ($attributes_values['products_id'] == $products_values['products_id']) {
          echo "\n" . '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '" SELECTED>' . $products_values['products_model']  . ' - ' . $products_values['products_name'] . '</option>';
        } else {
          echo "\n" . '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">' . $products_values['products_model']  . ' - ' . $products_values['products_name'] . '</option>';
        }
      } 
?>
            </select>&nbsp;</td>
            <td class="smallText">&nbsp;<select name="options_id">
<?php
      $Qoptions = $OSCOM_PDO->prepare('select *
                                       from :table_products_options
                                       where language_id = :language_id
                                       order by products_options_name
                                      ');
      $Qoptions->bindInt(':language_id',(int)$_SESSION['languages_id'] );
      $Qoptions->execute();

      while($options_values = $Qoptions->fetch() ) {
        if ($attributes_values['options_id'] == $options_values['products_options_id']) {
          echo "\n" . '<option name="' . $options_values['products_options_name'] . '" value="' . $options_values['products_options_id'] . '" SELECTED>' . $options_values['products_options_name'] . '</option>';
        } else {
         echo "\n" . '<option name="' . $options_values['products_options_name'] . '" value="' . $options_values['products_options_id'] . '">' . $options_values['products_options_name'] . '</option>';
        }
      } 
?>
            </select>&nbsp;</td>
<?php 
      if (DOWNLOAD_ENABLED == 'true') {

?>
            <td class="smallText"></td>
<?php
      }
?>
            <td class="smallText">&nbsp;<select name="values_id">
<?php
      $Qvalues = $OSCOM_PDO->prepare('select *
                                      from :table_products_options_values
                                      where language_id = :language_id
                                      order by products_options_values_name
                                     ');
      $Qvalues->bindInt(':language_id',(int)$_SESSION['languages_id'] );
      $Qvalues->execute();

      while($values_values = $Qvalues->fetch() ) {
        if ($attributes_values['options_values_id'] == $values_values['products_options_values_id']) {
          echo "\n" . '<option name="' . $values_values['products_options_values_name'] . '" value="' . $values_values['products_options_values_id'] . '" SELECTED>' . $values_values['products_options_values_name'] . '</option>';
        } else {
          echo "\n" . '<option name="' . $values_values['products_options_values_name'] . '" value="' . $values_values['products_options_values_id'] . '">' . $values_values['products_options_values_name'] . '</option>';
        }
      } 
?>        
            </select>&nbsp;</td>
            <td align="right" class="smallText">&nbsp;<input type="text" name="value_price" value="<?php echo $attributes_values['options_values_price']; ?>" size="6">&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<input type="text" name="price_prefix" value="<?php echo $attributes_values['price_prefix']; ?>" size="2">&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<input type="text" name="value_sort_order" value="<?php echo $attributes_values['products_options_sort_order']; ?>" size="2">&nbsp;</td>
            <td align="right">&nbsp;<?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE); ?>&nbsp;</td>
            <td><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info. '#tab3') . '">'; ?><?php echo osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL); ?></a>&nbsp;</td>
<?php
      if (DOWNLOAD_ENABLED == 'true') {

        $Qdownload = $OSCOM_PDO->prepare('select products_attributes_filename,
                                                 products_attributes_maxdays,
                                                 products_attributes_maxcount
                                          from :table_products_attributes_download
                                          where products_attributes_id = :products_attributes_id
                                      ');
        $Qdownload->bindInt(':products_attributes_id', (int)$attributes_values['products_attributes_id']);
        $Qdownload->execute();

        if ($Qdownload->rowCount() > 0) {
          $download = $Qdownload->fetch();
          $products_attributes_filename = $download['products_attributes_filename'];
          $products_attributes_maxdays  = $download['products_attributes_maxdays'];
          $products_attributes_maxcount = $download['products_attributes_maxcount'];
        }
?>
          <tr class="<?php echo (!($rows % 2)? 'attributes-even' : 'attributes-odd');?>">
            <td>&nbsp;</td>
            <td colspan="7">
              <table>
                <tr class="<?php echo (!($rows % 2)? 'attributes-even' : 'attributes-odd');?>">
                  <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DOWNLOAD; ?>&nbsp;</td>
                  <td class="smallText"><?php echo TABLE_TEXT_FILENAME; ?></td>
                  <td class="smallText" valign="bottom"><?php echo osc_draw_file_field('new_products_attributes_filename'); ?>&nbsp;</td>
                  <td class="smallText"><?php echo '&nbsp;' . $products_attributes_filename; ?>&nbsp;</td>
                  <td class="smallText"><?php echo TABLE_TEXT_MAX_DAYS; ?></td>
                  <td class="smallText"><?php echo osc_draw_input_field('products_attributes_maxdays', $products_attributes_maxdays, 'size="5"'); ?>&nbsp;</td>
                  <td class="smallText"><?php echo TABLE_TEXT_MAX_COUNT; ?></td>
                  <td class="smallText"><?php echo osc_draw_input_field('products_attributes_maxcount', $products_attributes_maxcount, 'size="5"'); ?>&nbsp;</td>
                </tr>
              </table>
            </td>
            <td>&nbsp;</td>
          </tr>
<?php
      }
?>
<?php
    } elseif (($action == 'delete_product_attribute') && ($_GET['attribute_id'] == $attributes_values['products_attributes_id'])) {
?>
            <td class="smallText">&nbsp;<strong><?php echo $attributes_values["products_attributes_id"]; ?></strong>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<?php echo $attributes_values['products_attributes_reference']; ?>&nbsp;</td>
            <td class="smallText">&nbsp;<strong><?php echo $products_name_only; ?></strong>&nbsp;</td>
            <td class="smallText">&nbsp;<strong><?php echo $options_name; ?></strong>&nbsp;</td>
<?php 
      if (DOWNLOAD_ENABLED == 'true') {

        $Qfilename = $OSCOM_PDO->prepare('select products_attributes_filename
                                           from :table_products_attributes_download
                                           where products_attributes_id =  :products_attributes_id
                                           limit 1
                                         ');
        $Qfilename->bindInt(':products_attributes_id',(int)$attributes_values['products_attributes_id'] );
        $Qfilename->execute();

        $filename_query = $Qfilename->fetch();

?>
            <td class="smallText"><?php echo $filename_query['products_attributes_filename']; ?></td>
<?php
      }
?>
            <td class="smallText">&nbsp;<strong><?php echo $values_name; ?></strong>&nbsp;</td>
            <td align="right" class="smallText">&nbsp;<strong><?php echo $attributes_values["options_values_price"]; ?></strong>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<strong><?php echo $attributes_values["price_prefix"]; ?></strong>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<strong><?php echo $attributes_values["products_options_sort_order"]; ?></strong>&nbsp;</td>





            <td align="right"><?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=delete_attribute&attribute_id=' . $_GET['attribute_id'] . '&' . $page_info . '#tab3') . '">'; ?><?php echo osc_image_button('button_mini_update.gif', IMAGE_CONFIRM); ?></a>&nbsp;</td>
            <td><?php echo '<a href="' . osc_href_link('products_attributes.php', $page_info . '#tab3') . '">'; ?><?php  echo osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL); ?></a>&nbsp;</strong></td>
<?php
    } else {
?>
            <td class="smallText">&nbsp;<?php echo $attributes_values["products_attributes_id"]; ?>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<?php echo $attributes_values['products_attributes_reference']; ?>&nbsp;</td>
            <td class="smallText">&nbsp;<?php echo $products_name_only; ?>&nbsp;</td>
            <td class="smallText">&nbsp;<?php echo $options_name; ?>&nbsp;</td>
<?php 
      if (DOWNLOAD_ENABLED == 'true') {

        $Qfilename = $OSCOM_PDO->prepare('select products_attributes_filename
                                           from :table_products_attributes_download
                                           where products_attributes_id =  :products_attributes_id
                                           limit 1
                                         ');
        $Qfilename->bindInt(':products_attributes_id',(int)$attributes_values['products_attributes_id'] );
        $Qfilename->execute();

        $filename_query = $Qfilename->fetch();

?>
            <td class="smallText"><?php echo $filename_query['products_attributes_filename']; ?></td>
<?php
      }
?>
            <td class="smallText">&nbsp;<?php echo $values_name; ?>&nbsp;</td>
            <td align="right" class="smallText">&nbsp;<?php echo $attributes_values["options_values_price"]; ?>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<?php echo $attributes_values["price_prefix"]; ?>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<?php echo $attributes_values["products_options_sort_order"]; ?>&nbsp;</td>
            <td align="right">&nbsp;<?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=update_attribute&attribute_id=' . $attributes_values['products_attributes_id']  . '&' . $page_info . '#tab3') . '">'; ?><?php echo osc_image_button('button_edit.gif', IMAGE_UPDATE); ?></a></td>
            <td><?php echo '<a href="' . osc_href_link('products_attributes.php', 'action=delete_product_attribute&attribute_id=' . $attributes_values['products_attributes_id'] . '&' . $page_info . '#tab3') , '">'; ?><?php echo osc_image_button('button_delete.gif', IMAGE_DELETE); ?></a></td>
<?php
    }

    $QmaxAttributes = $OSCOM_PDO->prepare('select max(products_attributes_id) + 1 as next_id
                                           from :table_products_attributes
                                         ');

    $QmaxAttributes->execute();

    $max_attributes_id_values = $QmaxAttributes->fetch();

    $next_id = $max_attributes_id_values['next_id'];
?>
          </tr>
<?php
  }
  if ($action != 'update_attribute') {
?>
          <tr>
            <td colspan="10"><?php echo osc_black_line(); ?></td>
          </tr>
          <tr class="<?php echo (floor($rows/2) == ($rows/2) ? 'attributes-even' : 'attributes-odd'); ?>">
            <td class="smallText">&nbsp;<?php echo $next_id; ?>&nbsp;</td>
            <td align="center" class="smallText">&nbsp;<input type="text" name="products_attributes_reference" value="<?php echo $attributes_values['products_attributes_reference']; ?>" size="10">&nbsp;</td>
            <td class="smallText">&nbsp;<select name="products_id">

<?php
    $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                               pd.products_name,
                                               p.products_model
                                        from :table_products p,
                                             :table_products_description pd
                                        where pd.products_id = p.products_id
                                        and pd.language_id = :language_id
                                              and p.products_archive = 0
                                        order by pd.products_name
                                      ');
    $Qproducts->bindInt(':language_id',(int)$_SESSION['languages_id'] );
    $Qproducts->execute();

    while ($products_values =  $Qproducts->fetch() ) {
      echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">'. $products_values['products_model']  . ' - ' . $products_values['products_name'] . '</option>';
    } 
?>
            </select>&nbsp;</td>
            <td class="smallText">&nbsp;<select name="options_id">
<?php
    $Qoptions = $OSCOM_PDO->prepare('select *
                                       from :table_products_options
                                       where language_id = :language_id
                                       order by products_options_name
                                     ');
    $Qoptions->bindInt(':language_id',(int)$_SESSION['languages_id'] );
    $Qoptions->execute();


    while ($options_values = $Qoptions->fetch() ) {
      echo '<option name="' . $options_values['products_options_name'] . '" value="' . $options_values['products_options_id'] . '">' . $options_values['products_options_name'] . '</option>';
    } 
?>
            </select>&nbsp;</td>
<?php 
      if (DOWNLOAD_ENABLED == 'true') {
?>
            <td class="smallText">&nbsp;&nbsp;</td>

<?php
      }
?>
            <td class="smallText">&nbsp;<select name="values_id">
<?php
    $Qvalues = $OSCOM_PDO->prepare('select *
                                    from :table_products_options_values
                                    where language_id = :language_id
                                    order by products_options_values_name
                                   ');
    $Qvalues->bindInt(':language_id',(int)$_SESSION['languages_id'] );
    $Qvalues->execute();

    while ($values_values = $Qvalues->fetch() ) {
      echo '<option name="' . $values_values['products_options_values_name'] . '" value="' . $values_values['products_options_values_id'] . '">' . $values_values['products_options_values_name'] . '</option>';
    } 
?>
            </select>&nbsp;</td>
            <td align="right" class="smallText">&nbsp;<input type="text" name="value_price" size="6">&nbsp;</td>
            <td align="right" class="smallText">&nbsp;<input type="text" name="price_prefix" size="2" value="+">&nbsp;</td>
            <td align="right" class="smallText">&nbsp;<input type="text" name="value_sort_order" size="2" value="1">&nbsp;</td>
            <td></td>
            <td align="center" class="smallText">&nbsp;<?php echo osc_image_submit('button_insert.gif', IMAGE_INSERT); ?>&nbsp;</td>
          </tr>
<?php
      if (DOWNLOAD_ENABLED == 'true') {
        $products_attributes_maxdays  = DOWNLOAD_MAX_DAYS;
        $products_attributes_maxcount = DOWNLOAD_MAX_COUNT;
        $products_attributes_filename = '';
?>
          <tr class="<?php echo (!($rows % 2)? 'attributes-even' : 'attributes-odd');?>">
            <td>&nbsp;</td>
            <td colspan="7">
              <table>
                <tr class="<?php echo (!($rows % 2)? 'attributes-even' : 'attributes-odd');?>">
                  <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DOWNLOAD; ?>&nbsp;</td>
                  <td class="smallText"><?php echo TABLE_TEXT_FILENAME; ?></td>
                  <td class="smallText"><?php echo osc_draw_file_field('products_attributes_filename'); ?>&nbsp;</td>
                  <td class="smallText"><?php echo TABLE_TEXT_MAX_DAYS; ?></td>
                  <td class="smallText"><?php echo osc_draw_input_field('products_attributes_maxdays', $products_attributes_maxdays, 'size="5"'); ?>&nbsp;</td>
                  <td class="smallText"><?php echo TABLE_TEXT_MAX_COUNT; ?></td>
                  <td class="smallText"><?php echo osc_draw_input_field('products_attributes_maxcount', $products_attributes_maxcount, 'size="5"'); ?>&nbsp;</td>
                </tr>
              </table>
            </td>
            <td>&nbsp;</td>
          </tr>
<?php
      }
?>
<?php
  }
?>
              <tr>
                <td colspan="10"><?php echo osc_black_line(); ?></td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>

       <tr>
         <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
       </tr>
       <tr>
         <td width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide" align="center">
           <tr>
             <td><table border="0" cellpadding="2" cellspacing="2">
               <tr>
                 <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_CLONE); ?></td>
                 <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_ATTRIBUTS; ?></strong></td>
               </tr>
               <tr>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                 <td class="main"><?php echo TEXT_AIDE_ATTRIBUTS; ?></td>
               </tr>
             </table></td>
           </tr>
        </table></td>
      </tr>
    </div>

<!-- //########################################################################################## -->
<!-- //                Clonage des produits : etape 4                                -->
<!-- //######################################################################################### -->

    <div class="tab-pane" id="tab4">

      <Table width="100%" cellpadding="2" cellspacing="0">
         <tr>
<?php
        echo '<form name="option" action="' . osc_href_link('products_attributes.php', 'action=clone_attributes#tab3') . '" method="post">';
?>
           <td><Table width="100%" cellpadding="0" cellspacing="0">
             <tr>
               <td class="pageHeading"><?php echo HEADING_TITLE_CLONE_PRODUCTS_ATTRIBUTES; ?></td>
             </tr>
           </table></td>
         </tr>
        <tr>
          <td><?php echo osc_black_line(); ?></td>
        </tr>
         <tr>
          <td><Table width="100%" cellpadding="0" cellspacing="0" align="center">
            <tr valign="middle">
               <td class="smallText" align="center" width="20%"><?php echo CLONE_PRODUCTS_FROM; ?>
                  <select name="clone_products_id_from">
<?php
  $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                           pd.products_name,
                                           p.products_model
                                      from :table_products p,
                                           :table_products_description pd
                                      where pd.products_id = p.products_id
                                      and pd.language_id = :language_id
                                      and p.products_archive = 0
                                      order by pd.products_name
                                   ');
  $Qproducts->bindInt(':language_id',(int)$_SESSION['languages_id'] );
  $Qproducts->execute();

  while ($products_values = $Qproducts->fetch() ) {
    echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">'. $products_values['products_model']  . ' - '  . $products_values['products_name'] . '</option>';
  }
?>
                  </select>
                </td>
                <td class="smallText" colspan="2" valign="middle" width="5%"><?php echo CLONE_PRODUCTS_TO; ?></td>
                <td width="20%">
                  <select name="clone_products_id_to[]" multiple size="10">
<?php
  $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                           pd.products_name,
                                           p.products_model
                                    from :table_products p,
                                         :table_products_description pd
                                    where pd.products_id = p.products_id
                                    and pd.language_id = :language_id
                                    and p.products_archive = 0
                                    order by pd.products_name
                                   ');
  $Qproducts->bindInt(':language_id',(int)$_SESSION['languages_id'] );
  $Qproducts->execute();

  while ($products_values = $Qproducts->fetch() ) {
    echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">'. $products_values['products_model']  . ' - '  . $products_values['products_name'] . '</option>';
  }
?>
                  </select>
                </td>
                <td width="55%" align="left"><?php echo osc_image_submit('button_copy.gif', IMAGE_COPY); ?><br /><br /><?php echo osc_image_submit('button_delete.gif', IMAGE_DELETE); ?></td>
              </table></td>
             </tr>
             <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
             </tr>
             <tr>
                <td width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide" align="center">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_CLONE); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_CLONE; ?></strong></td>
                      </tr>
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo TEXT_AIDE_CLONE; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
             </tr>
           </table></td>
         </tr>
       </div>
     </div>
   </div>
 </div>
<!-- products_attributes_eof //-->
<!-- body_text_eof //-->
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
