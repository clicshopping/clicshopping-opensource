<?php
/**
 * categories.php 
 * @copyright Copyright 2008 - ClicShopping http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: categories_description.php 
*/

/**
 * the category name
 * 
 * @param string  $category_id, $language_id
 * @return string $category['blog_categories_name'],  name of the blog categorie
 * @access public
 */
  function osc_get_category_heading_title($category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qcategory = $OSCOM_PDO->prepare('select categories_heading_title
                                      from :table_categories_description
                                      where categories_id = :categories_id
                                      and language_id = :language_id
                                    ');
    $Qcategory->bindInt(':categories_id', (int)$category_id);
    $Qcategory->bindInt(':language_id', (int)$language_id);

    $Qcategory->execute();

    return $Qcategory->value('categories_heading_title');
  }


/**
 * the category description
 * 
 * @param string  $category_id, $language_id
 * @return string $category['blog_categories_name'],  description of the blog categorie
 * @access public
 */
  function osc_get_category_description($category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qcategory = $OSCOM_PDO->prepare('select categories_description
                                      from :table_categories_description
                                      where categories_id = :categories_id
                                      and language_id = :language_id
                                    ');
    $Qcategory->bindInt(':categories_id', (int)$category_id);
    $Qcategory->bindInt(':language_id', (int)$language_id);

    $Qcategory->execute();

    return $Qcategory->value('categories_description');
  }

  function osc_get_categories_head_title_tag($category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qcategory = $OSCOM_PDO->prepare('select categories_head_title_tag
                                      from :table_categories_description
                                      where categories_id = :categories_id
                                      and language_id = :language_id
                                    ');
    $Qcategory->bindInt(':categories_id', (int)$category_id);
    $Qcategory->bindInt(':language_id', (int)$language_id);

    $Qcategory->execute();

    return $Qcategory->value('categories_head_title_tag');
  }

  function osc_get_categories_head_desc_tag($category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qcategory = $OSCOM_PDO->prepare('select categories_head_desc_tag
                                      from :table_categories_description
                                      where categories_id = :categories_id
                                      and language_id = :language_id
                                    ');
    $Qcategory->bindInt(':categories_id', (int)$category_id);
    $Qcategory->bindInt(':language_id', (int)$language_id);

    $Qcategory->execute();

    return $category['categories_head_desc_tag'];
  }


  function osc_get_categories_head_keywords_tag($category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qcategory = $OSCOM_PDO->prepare('select categories_head_keywords_tag
                                      from :table_categories_description
                                      where categories_id = :categories_id
                                      and language_id = :language_id
                                    ');
    $Qcategory->bindInt(':categories_id', (int)$category_id);
    $Qcategory->bindInt(':language_id', (int)$language_id);

    $Qcategory->execute();

    return $Qcategory->value('categories_head_keywords_tag');
  }


  function osc_get_product_clone_to_category () {
    global $OSCOM_PDO, $clone_products_id, $multi_clone_categories_id_to, $dup_products_id, $user_administrator, $products_id;

    $Qproduct = $OSCOM_PDO->prepare('select *
                                      from :table_products
                                      where products_id = :products_id
                                    ');
    $Qproduct->bindInt(':products_id', (int)$clone_products_id);

    $Qproduct->execute();

    $product = $Qproduct->fetch();


    for ($i=0;$i<sizeof($multi_clone_categories_id_to);$i++) {

  // clonage dans la categorie
      $clone_categories_id_to = $multi_clone_categories_id_to[$i];

  // copy du produit
      $OSCOM_PDO->save('products', [
                                    'products_quantity' => $product['products_quantity'],
                                    'products_model' => $product['products_model'],
                                    'products_ean' => $product['products_ean'],
                                    'products_sku' => $product['products_sku'],
                                    'products_image' => $product['products_image'],
                                    'products_image_zoom' => $product['products_image_zoom'],
                                    'products_price' => $product['products_price'],
                                    'products_date_added' => 'now()',
                                    'products_date_available' => (empty($product['products_date_available']) ? "null" : "'" . osc_db_input($product['products_date_available']) . "'"),
                                    'products_weight' => $product['products_weight'],
                                    'products_price_kilo' => $product['products_price_kilo'],
                                    'products_status' => 0,
                                    'products_tax_class_id' => (int)$product['products_tax_class_id'],
                                    'manufacturers_id' => (int)$product['manufacturers_id'],
                                    'products_view' => $product['products_view'],
                                    'orders_view' => $product['orders_view'],
                                    'suppliers_id' => (int)$product['suppliers_id'],
                                    'products_min_qty_order' => $product['products_min_qty_order'],
                                    'products_price_comparison' => $product['products_price_comparison'],
                                    'products_dimension_width' => $product['products_dimension_width'],
                                    'products_dimension_height' => $product['products_dimension_height'],
                                    'products_dimension_depth' => $product['products_dimension_depth'],
                                    'products_dimension_type' => $product['products_dimension_type'],
                                    'admin_user_name' =>  osc_user_admin($user_administrator) ,
                                    'products_volume' => $product['products_volume'],
                                    'products_quantity_unit_id' => $product['products_quantity_unit_id'],
                                    'products_only_online' => $product['products_only_online'],
                                    'products_image_medium' => $product['products_image_medium'],
                                    'products_weight_pounds' => $product['products_weight_pounds'],
                                    'products_cost' => $product['products_cost'],
                                    'products_handling' => $product['products_handling'],
                                    'products_wharehouse_time_replenishment' => $product['products_wharehouse_time_replenishment'],
                                    'products_wharehouse' => $product['products_wharehouse'],
                                    'products_wharehouse_row' => $product['products_wharehouse_row'],
                                    'products_wharehouse_level_location' => $product['products_wharehouse_level_location'],
                                    'products_packaging' => $product['products_packaging'],
                                    'products_sort_order' => $product['products_sort_order'],
                                    'products_quantity_alert' => $product['products_quantity_alert'],
                                    'products_only_shop' => $product['products_only_shop']
                                  ]
                      );

      $dup_products_id = $OSCOM_PDO->lastInsertId();

  // ---------------------
  // gallery
  // ----------------------
      $QproductImage = $OSCOM_PDO->prepare('select image,
                                                   htmlcontent,
                                                   sort_order
                                            from :table_products_images
                                            where products_id = :products_id
                                          ');
      $QproductImage->bindInt(':products_id', (int)$clone_products_id);

      $QproductImage->execute();

      while ($product_images = $QproductImage->fetch() ) {

        $OSCOM_PDO->save('products_images', [
                                              'products_id' =>  (int)$dup_products_id,
                                              'image' => $product_images['image'],
                                              'htmlcontent' => $product_images['htmlcontent'],
                                              'sort_order' => $product_images['sort_order']
                                            ]
                        );
      }

  // ---------------------
  // referencement clonage
  // ----------------------
      $Qdescription = $OSCOM_PDO->prepare('select language_id,
                                                  products_name,
                                                  products_description,
                                                  products_head_title_tag,
                                                  products_head_desc_tag,
                                                  products_head_keywords_tag,
                                                  products_url,
                                                  products_head_tag,
                                                  products_shipping_delay
                                           from :table_products_description
                                           where products_id = :products_id
                                          ');
      $Qdescription->bindInt(':products_id', (int)$clone_products_id);

      $Qdescription->execute();

      while ($description = $Qdescription->fetch() ) {

        $OSCOM_PDO->save('products_description', [
                                                  'products_id' => (int)$dup_products_id,
                                                  'language_id' =>  (int)$description['language_id'],
                                                  'products_name' => $description['products_name'],
                                                  'products_description' => $description['products_description'],
                                                  'products_head_title_tag' => $description['products_head_title_tag'],
                                                  'products_head_desc_tag' => $description['products_head_desc_tag'],
                                                  'products_head_keywords_tag' => $description['products_head_keywords_tag'],
                                                  'products_url' => $description['products_url'],
                                                  'products_viewed' => 0,
                                                  'products_head_tag' => $description['products_head_tag'],
                                                  'products_shipping_delay' => $description['products_shipping_delay']
                                                ]
                        );

      }

  // ---------------------
  // insertion table 
  // ----------------------

      $OSCOM_PDO->save('products_to_categories', [
                                                  'products_id' => (int)$dup_products_id,
                                                  'categories_id' =>  (int)$clone_categories_id_to
                                                 ]
                      );

      $clone_products_id = $dup_products_id;

  // ---------------------
  // groupe client clonage
  // ----------------------
      $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                           customers_group_name,
                                                           customers_group_discount
                                           from :table_customers_groups
                                           where customers_group_id >  0
                                           order by customers_group_id
                                          ');
      $QcustomersGroup->execute();

 // Gets all of the customers groups
      while ($customers_group = $QcustomersGroup->fetch() ) {

        $Qattributes = $OSCOM_PDO->prepare('select g.customers_group_id,
                                                   g.customers_group_price,
                                                   p.products_price
                                            from :table_products_groups g,
                                                 :table_products p
                                            where p.products_id = :products_id
                                            and p.products_id =g.products_id
                                            and g.customers_group_id = :customers_group_id
                                            order by g.customers_group_id
                                          ');
        $Qattributes->bindInt(':products_id', (int)$clone_products_id);
        $Qattributes->bindInt(':customers_group_id', (int)$customers_group['customers_group_id']);

        $Qattributes->execute();

        $attributes = $Qattributes->fetch();

        if ($Qattributes->rowCount() > 0) {
// Definir la position 0 ou 1 pour --> Affichage Prix Public + Affichage Produit + Autorisation Commande 
// L'Affichage des produits, autorisation de commander et affichage des prix mis par defaut en valeur 1 dans la cas de la B2B desactive.
          if (MODE_B2B_B2C == 'true') {
            if (osc_db_prepare_input($_POST['price_group_view' . $customers_group['customers_group_id']]) == '1') {
              $price_group_view = '1';
            } else {
              $price_group_view = '0';
            }

            if (osc_db_prepare_input($_POST['products_group_view' . $customers_group['customers_group_id']]) == '1') {
              $products_group_view = '1';
            } else {
              $products_group_view = '0';
            }

            if (osc_db_prepare_input($_POST['orders_group_view' . $customers_group['customers_group_id']]) == '1') {
              $orders_group_view = '1';
            } else {
              $orders_group_view = '0';
            }

            $products_quantity_unit_id_group = $_POST['products_quantity_unit_id_group' . $customers_group['customers_group_id']];
            $products_model_group  = $_POST['products_model_group' . $customers_group['customers_group_id']];
            $products_quantity_fixed_group  = $_POST['products_quantity_fixed_group' . $customers_group['customers_group_id']];

          } else {
            $price_group_view = '1';
            $products_group_view = '1';
            $orders_group_view = '1';
            $products_quantity_unit_id_group = '0';
            $products_model_group = '';
            $products_quantity_fixed_group = 1;

          } //end MODE_B2B_B2C


          $Qupdate = $OSCOM_PDO->prepare('update :table_products_groups
                                          set price_group_view = :price_group_view,
                                              products_group_view = :products_group_view,
                                              orders_group_view = :orders_group_view,
                                              products_quantity_unit_id_group = :products_quantity_unit_id_group,
                                              products_model_group = :products_model_group,
                                              products_quantity_fixed_group = :products_quantity_fixed_group
                                          where customers_group_id = :customers_group_id
                                          and products_id = :products_id
                                          ');
          $Qupdate->bindInt(':price_group_view', $price_group_view);
          $Qupdate->bindInt(':products_group_view', $products_group_view);
          $Qupdate->bindInt(':orders_group_view', $orders_group_view);
          $Qupdate->bindInt(':products_quantity_unit_id_group', $products_quantity_unit_id_group);
          $Qupdate->bindValue(':products_model_group', $products_model_group);
          $Qupdate->bindValue(':products_quantity_fixed_group', $products_quantity_fixed_group);
          $Qupdate->bindInt(':customers_group_id',  (int)$attributes['customers_group_id'] );
          $Qupdate->bindInt(':products_id',  (int)$clone_products_id);

          $Qupdate->execute();


// Prix TTC B2B ----------
            if ( ($_POST['price' . $customers_group['customers_group_id']] <> $attributes['customers_group_price']) && ($attributes['customers_group_id'] == $customers_group['customers_group_id']) ) {

              $Qupdate = $OSCOM_PDO->prepare('update :table_products_groups
                                              set customers_group_price = :customers_group_price,
                                                  products_price = :products_price
                                              where customers_group_id = :customers_group_id
                                              and products_id = :products_id
                                            ');
              $Qupdate->bindInt(':customers_group_price', $_POST['price' . $customers_group['customers_group_id']]);
              $Qupdate->bindInt(':products_price', $_POST['products_price']);
              $Qupdate->bindInt(':customers_group_id', (int)$attributes['customers_group_id'] );
              $Qupdate->bindInt(':products_id',  (int)$clone_products_id);

              $Qupdate->execute();


            } elseif (($_POST['price' . $customers_group['customers_group_id']] == $attributes['customers_group_price'])) {
              $attributes = $Qattributes->fetch();
            }
// Prix + Afficher Prix Public + Afficher Produit + Autoriser Commande
          } elseif ($_POST['price' . $customers_group['customers_group_id']] != '') {
                                            
          $OSCOM_PDO->save('products_groups', [
                                              'products_id' => (int)$clone_products_id,
                                              'products_price' => $_POST['products_price'],
                                              'customers_group_id' => (int)$customers_group['customers_group_id'],
                                              'customers_group_price' => $_POST['price' . $customers_group['customers_group_id']],
                                              'price_group_view' => $_POST['price_group_view' . $customers_group['customers_group_id']],
                                              'products_group_view' => $_POST['products_group_view' . $customers_group['customers_group_id']],
                                              'orders_group_view' => $_POST['orders_group_view' . $customers_group['customers_group_id']],
                                              'products_quantity_unit_id_group' => $_POST['products_quantity_unit_id_group' . $customers_group['customers_group_id']],
                                              'products_model_group' =>  $_POST['products_model_group' . $customers_group['customers_group_id']],
                                              'products_quantity_fixed_grou' => $_POST['products_quantity_fixed_group' . $customers_group['customers_group_id']],
                                            ]
                           );

            $attributes = $Qattributes->fetch();
          } // edn osc_db_num_rows
        } // end while
      } //End for
    return;
  }


