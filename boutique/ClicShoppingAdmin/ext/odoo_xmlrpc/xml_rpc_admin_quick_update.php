<?php
/*
 * xml_rpc_admin_customers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

    $Qproduct = $OSCOM_PDO->prepare('select distinct pd.products_name,
                                                      p.products_model,
                                                      p.products_price,
                                                      p.products_weight,
                                                      p.products_status,
                                                      p.products_quantity,
                                                      p.products_tax_class_id,
                                                      p.suppliers_id,
                                                      p.manufacturers_id,
                                                      p.products_price_comparison,
                                                      p.products_min_qty_order,
                                                      p.products_only_online,
                                                      p.products_id
                                               from :table_products p,
                                                    :table_products_description pd
                                               where p.products_id = :products_id
                                               and pd.products_id = p.products_id
                                               and pd.language_id = :language_id
                                              ');

    $Qproduct->bindInt(':products_id',  (int)$id);
    $Qproduct->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
    $Qproduct->execute();

    $product = $Qproduct->fetch();

    $products_id_clicshopping  = $product['products_id'];

    $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping, 'product.template', 'int');
    $field_list = array('id');

    $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
    $products_id_odoo = $Qproducts_id[0][id];

// **************************************************************************
// Search odoo products tax
// **************************************************************************

      $QProductsCodeTax = $OSCOM_PDO->prepare('select distinct tax_class_id,
                                                                code_tax_odoo
                                                 from :table_tax_rates
                                                 where tax_class_id = :tax_class_id
                                               ');

      $QProductsCodeTax->bindValue(':tax_class_id',$product['products_tax_class_id']);

      $QProductsCodeTax->execute();

      $Qproducts_code_tax = $QProductsCodeTax->fetch();
      $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];

//******************************************
// research id tax by description
//***************************************

      if (!empty($products_code_tax)) {
        $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
        $odoo_products_tax_id = $ids;
        $odoo_products_tax_id = $odoo_products_tax_id[0];

        if (!empty($odoo_products_tax_id)) {

          $type_tax_string = 'array';

          $tax = array(new xmlrpcval(
                                      array(
                                            new xmlrpcval(6, "int"),// 6 : id link
                                            new xmlrpcval(0, "int"),
                                            new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                      ), "array"
                                  )
                      );
        } else  {
          $tax = 0;
          $type_tax_string = 'int';
        }

      } else {
        $tax = 0;
        $type_tax_string = 'int';
      }

// **************************************************************************************************
// Manufacturer
// **************************************************************************************************


  if (!empty($product['manufacturers_id'])) {
// **************************
// Search odoo manufacturers id in clicshopping.manufacturer
// ***************************
    $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $product['manufacturers_id'], 'clicshopping.manufacturer', 'int');
    $field_list = array('id');
    $Qmanufacturers_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
    $manufacturer_id_odoo = $Qmanufacturers_id_odoo[0][id];
  }



  if  (!empty($products_id_odoo)) {

// **********************************
// update products if exist
// **********************************

      $id_list = array();
      $id_list[]= new xmlrpcval($products_id_odoo, 'int');

      $values = array ( "default_code" => new xmlrpcval($product['products_model'], "string"),
                        "name" => new xmlrpcval($product['products_name'],"string"),
                        "description_sale" => new xmlrpcval($product['products_name'],"string"),
                        "list_price" => new xmlrpcval($product['products_price'],"double"),
                        "weight" => new xmlrpcval($product['products_weight'],"double"),
                        "clicshopping_products_price_comparison" => new xmlrpcval($product['products_price_comparison'],"double"),
                        "clicshopping_products_only_online" => new xmlrpcval($product['products_only_online'],"double"),
                        "clicshopping_products_stock_status" => new xmlrpcval($product['products_status'],"double"),
                        "clicshopping_products_min_qty_order" => new xmlrpcval($product['products_min_qty_order'],"double"),
                        "taxes_id" =>  new xmlrpcval($tax, $type_tax_string),
                        "clicshopping_product_manufacturer_id" => new xmlrpcval($manufacturer_id_odoo,"int"),

                      );

      $OSCOM_ODOO->updateOdoo($products_id_odoo, $values, 'product.template');
    }


// **************************************************************************************************
// Supplier
// **************************************************************************************************


    if (!empty($product['suppliers_id'])) {

// **************************
// Search odoo suppliers id in res.partner
// ***************************

      $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore Suppliers - ' . $product['suppliers_id'] , 'res.partner');

// **********************************
// read id suppliers_id odoo
// **********************************

      $field_list = array('id',
                          'name',
                        );

      $Qsuppliers_id_name_res_partner_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $suppliers_id_name_res_partner_odoo = $Qsuppliers_id_name_res_partner_odoo[0][id];

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping, 'product.template', 'int');
      $field_list = array('id');

      $Qproducts_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
      $products_id_odoo = $Qproducts_id_odoo[0][id];

      if  (!empty($suppliers_id_name_res_partner_odoo)) {

// **********************************
// Search odoo suppliers Info id
// **********************************

       $ids =  $OSCOM_ODOO->odooSearchByTwoCriteria('product_code', '=', $product['products_model'], 'product.supplierinfo', 'string',
                                                    'product_tmpl_id', '=', $products_id_odoo, 'int');

// **********************************
// read id suppliers Info  Id odoo
// **********************************

        $field_list = array('id',
                            'name',
                           );

        $Qsuppliers_id_name_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.supplierinfo');
        $suppliers_id_name_odoo = $Qsuppliers_id_name_odoo[0][id];

        if  (empty($suppliers_id_name_odoo) ) {

          $values = array (  "name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new xmlrpcval($product['products_name'], "string"),
                            "product_code" => new xmlrpcval($product['products_model'], "string"),
                            "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                          );

          $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

        } else {

          $id_list = array();
          $id_list[] = new xmlrpcval($suppliers_id_name_odoo, 'int');



          $values = array ( "name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new xmlrpcval($product['products_name'], "string"),
                            "product_code" => new xmlrpcval($product['products_model'],"string"),
                            "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                          );

          $OSCOM_ODOO->updateOdoo($suppliers_id_name_odoo, $values, 'product.supplierinfo');
        }

      } else {

        $values = array("name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                        "product_name" => new xmlrpcval($product['products_name'], "string"),
                        "product_code" => new xmlrpcval($product['products_model'], "string"),
                        "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                      );

        $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

      }
    }

// **************************************************************************************************
// Stock
// **************************************************************************************************

    $date = date("Y-m-d H:i:s");

    $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();
    $products_name_odoo = '[' . $product['products_model'] . '] ' . $product['products_name'];


// stock warehouse search id and code concerning ClicShopping Wharehouse
    $ids = $OSCOM_ODOO->odooSearch('name', '=', 'ClicShopping', 'stock.warehouse');

    $field_list = array('id',
                        'code',
                        'name',
                      );

    $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');
    $stock_wharehouse_id = $Qstock_wharehouse[0][id];
    $stock_wharehouse_code = $Qstock_wharehouse[0][code];


// search location name and id in stock location
    $ids = $OSCOM_ODOO->odooSearch('name', '=', $stock_wharehouse_code, 'stock.location');

    $field_list = array('id',
                        'location_id',
                        'name',
                      );

    $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');
    $stock_location_id = $Qstock_location[0][id];
    $stock_location_name = $Qstock_location[0][name];

  if (!empty($product['products_quantity'])) {

// **********************************
// Search odoo products id & stock available
// **********************************
      $products_id_clicshopping  = $product['products_id'];

      $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping,  'product.template', 'int');
      $field_list = array('id');

      $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
      $products_id_odoo = $Qproducts_id[0][id];

// **********************************
// read id products odoo
// **********************************

    $field_list = array('qty_available');

    $Qproducts_stock_available = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
    $products_stock_available = $Qproducts_stock_available[0][qty_available];


    if (!empty($stock_location_id)) {

      if ($product['products_quantity'] != $products_stock_available) {


// search qty in stock.quant
        $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_name_odoo, 'stock.quant', 'string',
                                                       'location_id', 'like', $stock_location_name . '%', 'string');

        $field_list = array('qty');

        $QSearchStock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.quant');

        $stock_odoo_qty_total = '0';

// Stock calcul inside wharehouse
        foreach ($QSearchStock as $key) {
          $products_stock_qty = $key[qty];
          $stock_odoo_qty_total = $stock_odoo_qty_total + $products_stock_qty;
        }

// difference between ClicShopping and Odoo
        $new_stock = $product['products_quantity'] - $stock_odoo_qty_total;

// hack, don't find the good value in location.stock. Add +1 on id because it's create just after
        $stock_location_id = $stock_location_id + 1;

// move stock

        if ($new_stock > 0) {
          $values = array(
                          "product_id" => new xmlrpcval($products_id_odoo, "int"),
                          "product_uom_qty" => new xmlrpcval($new_stock, "double"), // qty of my product
                          "name" => new xmlrpcval($products_name_odoo, "string"),
                          "invoice_state" => new xmlrpcval('none', "string"),
                          "date" => new xmlrpcval($date, "string"),
                          "date_expected" => new xmlrpcval($date, "string"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "procure_method" => new xmlrpcval('make_to_stock', "string"),
                          "location_dest_id" => new xmlrpcval($stock_location_id, "string"),
                          "location_id" => new xmlrpcval(5, "int"), // Virtual Locations/Inventory loss
                          "product_uom" => new xmlrpcval(1, "int"),
                          "origin" => new xmlrpcval("ClicShopping Webstore", "string"),
                        );
        } else {

// Adapted for odoo on the calcul
          $new_stock = $new_stock * (-1);

          $values = array(
                          "product_id" => new xmlrpcval($products_id_odoo, "int"),
                          "product_uom_qty" => new xmlrpcval($new_stock, "double"), // qty of my product
                          "name" => new xmlrpcval($products_name_odoo, "string"),
                          "invoice_state" => new xmlrpcval('none', "string"),
                          "date" => new xmlrpcval($date, "string"),
                          "date_expected" => new xmlrpcval($date, "string"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "procure_method" => new xmlrpcval('make_to_stock', "string"),
                          "location_dest_id" => new xmlrpcval(5, "string"),
                          "location_id" => new xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                          "product_uom" => new xmlrpcval(1, "int"),
                          "origin" => new xmlrpcval("ClicShopping Webstore", "string"),
                        );
        }

        $OSCOM_ODOO->createOdoo($values, "stock.move");


// search id for stock.move concerning the product
        $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_id_odoo, 'stock.move', 'int',
                                                       'date', '=', $date, 'string');

        $field_list = array('id');

        $Qodoo_move_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
        $odoo_picking_id = $Qodoo_move_read[0][id];

        $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);

      }
    } // end !empty($stock_location_id)
  } //!empty($product['products_quantity'])
?>