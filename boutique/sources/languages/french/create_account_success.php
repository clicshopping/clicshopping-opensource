<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE_1', 'Création du comtpe');
define('NAVBAR_TITLE_2', 'Succès');
define('HEADING_TITLE', 'Votre compte a été créé !');
define('TEXT_ACCOUNT_CREATED', 'Félicitations ! Votre nouveau compte vient d\'être crée ! Vous pouvez maintenant personnaliser votre compte et mettre à jour vos préférences depuis votre ESPACE CLIENT.
<p>Vous pouvez également commander directement nos produits.
Pour toute question relative au fonctionnement du site et aux commandes en ligne, vous pouvez soit consulter la rubrique  "INFOS CLIENTS" ou contacter notre Hotline via un E-Mail par le formulaire mis à votre disposition : <a href="' . osc_href_link('contact_us.php') . '">Nous contacter</a>.<br /><br />');
define('TABLE_HEADING_NEW_PRODUCTS','Nouveautés et Collections');
define('MIN_QTY_ORDER_PRODUCT','Qté min : ');
?>