<?php
/*
 * login.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  $login_request = true;

  require('includes/application_top.php');
  require('includes/functions/password_funcs.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

// prepare to logout an active administrator if the login page is accessed again
  if (isset($_SESSION['admin'])) {
    $action = 'logoff';
  }

  if (osc_not_null($action)) {
    switch ($action) {
      case 'process':
        if (isset($_SESSION['redirect_origin']) && isset($redirect_origin['auth_user']) && !isset($_POST['username'])) {

          $username = osc_db_prepare_input($_SESSION['redirect_origin']['auth_user']);
          $password = osc_db_prepare_input($_SESSION['redirect_origin']['auth_pw']);
        } else {
          $username = osc_db_prepare_input($_POST['username']);
          $password = osc_db_prepare_input($_POST['password']);
        }

        $actionRecorder = new actionRecorderAdmin('ar_admin_login', null, $username);

        if ($actionRecorder->canPerform()) {

          $Qcheck = $OSCOM_PDO->prepare('select id,
                                                user_name,
                                                user_password,
                                                name,
                                                first_name
                                         from :table_administrators
                                         where user_name = :user_name
                                        ');

          $Qcheck->bindValue(':user_name', $username);
          $Qcheck->execute();

          if ($Qcheck->rowCount() == 1) {

            $check = $Qcheck->fetch();

            if (osc_validate_password($password, $check['user_password'])) {
// migrate old hashed password to new phpass password
              if (osc_password_type($check['user_password']) != 'phpass') {

                $Qupdate = $OSCOM_PDO->prepare('update :table_administrators
                                                 set user_password = :user_password
                                                 where id = :id
                                              ');
                $Qupdate->bindValue(':user_password', osc_encrypt_password($password));
                $Qupdate->bindInt(':id', (int)$check['id']);

                $Qupdate->execute();
              }

              $_SESSION['admin'] = array('id' => $check['id'],
                                         'username' => $check['user_name']);

              $actionRecorder->_user_id = $_SESSION['admin']['id'];
              $actionRecorder->record();

              if (isset($_SESSION['redirect_origin'])) {
                $page = $_SESSION['redirect_origin']['page'];
                $get_string = '';

                if (function_exists('http_build_query')) {
                  $get_string = http_build_query($_SESSION['redirect_origin']['get']);
                }

                unset($_SESSION['redirect_origin']);

                osc_redirect(osc_href_link($page, $get_string));
              } else {
                osc_redirect(osc_href_link('index.php'));
              }
            }
          }

          if (isset($_POST['username'])) {
            $OSCOM_MessageStack->add(ERROR_INVALID_ADMINISTRATOR, 'error');

// send an email if someone try to connect on admin panel without authorization
// get ip and infos

            $ip = $_SERVER['REMOTE_ADDR'];
            $host = @gethostbyaddr($ip);
            $referer = $_SERVER['HTTP_REFERER'];

// build report
            $report = date("D M j G:i:s Y") . "\n\n"  . REPORT_ACCESS_LOGIN;
            $report .= "\n\n" . REPORT_SENDER_IP_ADRESS . $ip;
            $report .= "\n" . REPORT_SENDER_HOST_NAME . $host;
            $report .= "\n" . REPORT_SENDER_USERNAME . $username;

// mail report
            osc_mail(STORE_OWNER_EMAIL_ADDRESS, STORE_OWNER_EMAIL_ADDRESS,  REPORT_SUBJECT_EMAIL, $report, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

          }
        } else {
          $OSCOM_MessageStack->add(sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES') ? (int)MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES : 5)));
        }

        $actionRecorder->record(false);

        break;

      case 'logoff':
        unset($_SESSION['admin']);

        if (isset($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']) && !empty($_SERVER['PHP_AUTH_PW'])) {
          $_SESSION['auth_ignore'] = true;
        }

        osc_redirect(osc_href_link('index.php'));

        break;

      case 'create':
        $check_query = osc_db_query("select id 
                                     from administrators
                                     limit 1");

        if (osc_db_num_rows($check_query) == 0) {
          $username = osc_db_prepare_input($_POST['username']);
          $password = osc_db_prepare_input($_POST['password']);
          $name = osc_db_prepare_input($_POST['name']);
          $first_name = osc_db_prepare_input($_POST['first_name']);

          if (!empty($username) ) {
            osc_db_query("insert into administrators (user_name,
                                                                    user_password, 
                                                                    name, 
                                                                    first_name) 
                         values ('" . osc_db_input($username) . "', 
                                 '" . osc_db_input(osc_encrypt_password($password)) . "',
                                 '" . osc_db_input($name) . "',   
                                 '" . osc_db_input($first_name) . "'   
                                 )
                       ");
          }
        } // end osc_db_num_rows
        
        osc_redirect(osc_href_link('login.php'));

        break;
        
      case 'send_password':
        $username = osc_db_prepare_input($_POST['username']);

        $Qcheck = $OSCOM_PDO->prepare('select id
                                      from :table_administrators
                                       where user_name = :user_name
                                       limit 1
                                      ');
        $Qcheck->bindValue(':user_name', $username);
        $Qcheck->execute();

        if ($Qcheck->rowCount() == 1 && osc_validate_email($username)) {
          $new_password = osc_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);
          $crypted_password = osc_encrypt_password($new_password);

          $Qupdate = $OSCOM_PDO->prepare('update :table_administrators
                                           set user_password = :user_password
                                           where user_name = :user_name
                                           limit 1
                                        ');
          $Qupdate->bindValue(':user_password', osc_encrypt_password($new_password));
          $Qupdate->bindValue(':user_name', osc_db_input($username));

          $Qupdate->execute();

          osc_mail('', $username, EMAIL_PASSWORD_REMINDER_SUBJECT, sprintf(EMAIL_PASSWORD_REMINDER_BODY, $new_password), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
          $OSCOM_MessageStack->add(SUCCESS_PASSWORD_SENT, 'success');
        } else {
          $OSCOM_MessageStack->add(TEXT_NO_EMAIL_ADDRESS_FOUND, 'error');
        }
        break;        
    }
  }

  $languages = osc_get_languages();
  $languages_array = array();
  $languages_selected = DEFAULT_LANGUAGE;
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    $languages_array[] = array('id' => $languages[$i]['code'],
                               'text' => $languages[$i]['name']);
    if ($languages[$i]['directory'] == $_SESSION['language']) {
      $languages_selected = $languages[$i]['code'];
    }
  }

  $Qcheck = $OSCOM_PDO->prepare('select id
                                 from :table_administrators
                                 limit 1
                                ');
  $Qcheck->execute();


  if ($Qcheck->rowCount() < 1) {
    $OSCOM_MessageStack->add(TEXT_CREATE_FIRST_ADMINISTRATOR, 'warning');
  }

  require('includes/header.php');

  if ($Qcheck->rowCount() > 0) {
    $form_action = 'process';
    $button_text = BUTTON_LOGIN;
  } else {
    $form_action = 'create';
    $button_text = BUTTON_CREATE_ADMINISTRATOR;
  }

  if ($action != 'password') {
?>
  <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="form-group">
<?php
  if ( count($languages_array) > 1 ) {
    echo osc_draw_form('adminlanguage', 'index.php', '', 'get') .
    osc_draw_pull_down_menu('language', $languages_array, $languages_selected, 'onchange="this.form.submit();" class="pull-right"') . osc_hide_session_id() .
    '</form>';
  }
?>
          </div>
          <h1 class="text-center"><?php  echo HEADING_TITLE; ?></h1>
        </div>
        <?php echo osc_draw_form('login', 'login.php', 'action=' . $form_action); ?>
          <div class="modal-body">
            <div class="col-md-12 center-block">
              <div class="form-group">
                <?php echo osc_draw_input_field('username', '', 'class="form-control input-lg" placeholder="' . TEXT_USERNAME . '"'); ?>
              </div>
              <div class="form-group">
                <?php echo osc_draw_password_field('password', '','class="form-control input-lg" placeholder="' . TEXT_PASSWORD . '"'); ?>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-lg btn-block" type="submit"><?php echo $button_text; ?></button>
              </div>
            </div>
          </div>
        </form>
        <div class="modal-footer">
          <div class="col-md-6">
            <a href="../index.php"><button class="btn  pull-left" data-dismiss="modal" aria-hidden="true"><?php echo HEADER_TITLE_ONLINE_CATALOG; ?></button></a>
          </div>
          <div class="col-md-6">
           <a href="<?php echo osc_href_link('login.php', 'action=password'); ?>"> <button class="btn pull-right" data-dismiss="modal" aria-hidden="true"><?php echo TEXT_NEW_TEXT_PASSWORD; ?></button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
  } else {
?>
  <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <?php echo osc_draw_form('send_password', 'login.php', 'action=send_password'); ?>
          <div class="modal-header">
            <legend>
              <div><?php echo  HEADING_TITLE_SENT_PASSWORD; ?><div>
            </legend>
            <div class="modal-body">
              <div class="col-md-12 center-block">
                <div class="text-warning" style="font-size:12px; padding-bottom:10px;"><?php echo TEXT_SENT_PASSWORD; ?></div>
                  <div class="form-group">
                    <?php echo osc_draw_input_field('username', '', 'class="form-control input-lg" size="150" placeholder="' . TEXT_EMAIL_LOST_PASSWORD . '"'); ?>
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="col-md-6">
              <a href="<?php echo osc_href_link('login.php' ); ?>"><button class="btn btn-secondary pull-left" type="submit"><?php echo HEADER_TITLE_ADMINISTRATION; ?></button></a>
            </div>
            <div class="col-md-6">
              <button class="btn btn-primary btn-block pull-right" type="submit"><?php echo BUTTON_SUBMIT; ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php
  }
?>
	<div class="clearfix"></div>

<!-- body_eof //-->

<!-- footer //-->
<?php 

 require('includes/footer.php');
 require('includes/application_bottom.php');
