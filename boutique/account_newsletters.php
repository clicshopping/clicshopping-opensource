<?php
/*
 * account_newletter.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_newsletters'));

  $Qnewsletter = $OSCOM_PDO->prepare('select customers_newsletter 
                                       from :table_customers 
                                       where customers_id = :customers_id
                                      ');
  $Qnewsletter->bindInt(':customers_id', $OSCOM_Customer->getID());
  $Qnewsletter->execute();

  $newsletter = $Qnewsletter->fetch();

  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    if (isset($_POST['newsletter_general']) && is_numeric($_POST['newsletter_general'])) {
      $newsletter_general = osc_db_prepare_input($_POST['newsletter_general']);
    } else {
      $newsletter_general = '0';
    }

    if ($newsletter_general != $newsletter['customers_newsletter']) {
      $newsletter_general = (($newsletter['customers_newsletter'] == '1') ? '0' : '1');

      $Qupdate = $OSCOM_PDO->prepare('update :table_customers 
                                     set customers_newsletter = :customers_newsletter 
                                     where customers_id = :customers_id
                                    ');
      $Qupdate->bindValue(':customers_newsletter', (int)$newsletter_general);
      $Qupdate->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
      $Qupdate->execute();
    }

    $OSCOM_MessageStack->addSuccess('account', SUCCESS_NEWSLETTER_UPDATED);

    osc_redirect(osc_href_link('account.php', '', 'SSL'));
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_newsletters.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('account_newsletters'));
