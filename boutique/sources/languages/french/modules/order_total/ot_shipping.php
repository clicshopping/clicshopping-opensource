<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ORDER_TOTAL_SHIPPING_TITLE', 'Expédition');
  define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION', 'Co&ugrave;t d\'Expédition<br /><br /><font color="#FF0000"><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut</font> ');

  define('FREE_SHIPPING_TITLE', 'Expedition gratuite');
  define('FREE_SHIPPING_DESCRIPTION', 'Expédition gratuite pour les commandes supérieures à %s ');
?>