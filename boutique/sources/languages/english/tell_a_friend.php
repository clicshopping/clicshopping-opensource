<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Tell A Friend');
define('HEADING_TITLE', 'Tell A Friend About \'%s\'');
define('HEADING_TITLE_ERROR', 'Error tell a friend about');

define('COPY_ANTISPAM', 'Copy the AntiSpam Code');

define('FORM_TITLE_CUSTOMER_DETAILS', 'Your Details');
define('FORM_TITLE_FRIEND_DETAILS', 'Your Friends Details');
define('FORM_TITLE_FRIEND_MESSAGE', 'Your Message');

define('FORM_FIELD_CUSTOMER_NAME', 'Your Name:');
define('FORM_FIELD_CUSTOMER_EMAIL', 'Your E-Mail Address:');
define('FORM_FIELD_FRIEND_NAME', 'Your Friends Name:');
define('FORM_FIELD_FRIEND_EMAIL', 'Your Friends E-Mail Address:');

define('TEXT_EMAIL_SUCCESSFUL_SENT', 'Your email about <strong>%s</strong> has been successfully sent to <strong>%s</strong>.');

define('TEXT_EMAIL_SUBJECT', 'Your friend %s has recommended this great product from %s');
define('TEXT_EMAIL_INTRO', 'Hi %s!' . "\n\n" . 'Your friend, %s, thought that you would be interested in %s from %s.');
define('TEXT_EMAIL_LINK', 'To view the product click on the link below or copy and paste the link into your web browser:' . "\n\n" . '%s');
define('TEXT_EMAIL_SIGNATURE', '
Regards,'. "\n" . '<strong>'.STORE_NAME.' Team</strong>'. "\n" . '
<style font-size:2;"><u>Confidentiality Note :</u>'. "\n\n" . '
This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '
<p><font size="2">In accordance with the <strong>Law</strong> in the '.STORE_NAME.' country, you can rectify your personal data or you can send us a request by email</style>
</div>');

define('ERROR_TO_NAME', 'Error: Your friends name must not be empty.');
define('ERROR_TO_ADDRESS', 'Error: Your friends e-mail address must be a valid e-mail address.');
define('ERROR_FROM_NAME', 'Error: Your name must not be empty.');
define('ERROR_FROM_ADDRESS', 'Error: Your e-mail address must be a valid e-mail address.');
define('ERROR_TO_SPAM', 'Error : The AntiSpam Code must not be empty');
define('ERROR_INVALID_PRODUCT', 'This products not available. Try again.');
define('ENTRY_NUMBER_EMAIL_CONFIRMATION', 'What is : ');
?>
