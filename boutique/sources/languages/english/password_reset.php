<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Login');
define('NAVBAR_TITLE_2', 'Password Reset');

define('HEADING_TITLE', 'Password Reset');

define('TEXT_MAIN', 'Please fill all these elements.');

define('TEXT_NO_RESET_LINK_FOUND', ' Error: The password reset link was not found in our records, please try again by generating a new link.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', ' Error: The E-Mail Address was not found in our records, please try again.');

define('SUCCESS_PASSWORD_RESET', 'Your password has been successfully updated. Please login with your new password.');

define('TEXT_EMAIL_SUBJECT','Passwor reset');
define('TEXT_EMAIL_BODY','Hello,' . "\n\n" . ' Your password has been reseted with success. You can connect you now on '.STORE_NAME);

?>