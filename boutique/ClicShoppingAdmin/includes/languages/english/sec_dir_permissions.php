<?php
/**
 * sec_dir_permissions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Security Directory Permissions');

define('TABLE_HEADING_DIRECTORIES', 'Directories');
define('TABLE_HEADING_WRITABLE', 'Writable');
define('TABLE_HEADING_RECOMMENDED', 'Recommended');

define('TEXT_DIRECTORY', 'Directory:');
?>
