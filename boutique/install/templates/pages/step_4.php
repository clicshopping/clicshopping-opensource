<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  $www_location = 'http://' . $_SERVER['HTTP_HOST'];

  if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI'])) {
    $www_location .= $_SERVER['REQUEST_URI'];
  } else {
    $www_location .= $_SERVER['SCRIPT_FILENAME'];
  }

  $www_location = substr($www_location, 0, strpos($www_location, 'install'));

  $dir_fs_www_root = osc_realpath(__DIR__ . '/../../../') . '/';
?>


  <div class="contentBlock">
    <div class="contentPane">
      <h1><?php echo TEXT_NEW_INSTALLATION; ?></h1>
      <?php echo TEXT_STEP_INTRO_STEP4; ?>
    </div>
  </div>

  <form name="install" id="installForm" action="index.php?step=5" method="post" class="form-horizontal" role="form">

  <div class="contentBlock">
    <div class="contentPane">

      <h2><?php echo TEXT_STEP_INTRO_3; ?></h2>
      <div class="form-group has-feedback">
        <label for="wwwAddress" class="control-label col-xs-3"<?php echo TEXT_STEP_INTRO_4; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('HTTP_WWW_ADDRESS', $www_location, 'required aria-required="true" id="wwwAddress" placeholder="http://"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STEP_HELP_4; ?></span>
        </div>
      </div>
    
      <div class="form-group has-feedback">
        <label for="webRoot" class="control-label col-xs-3"><?php echo TEXT_STEP_INTRO_5; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('DIR_FS_DOCUMENT_ROOT', $dir_fs_www_root, 'required aria-required="true" id="webRoot"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STEP_HELP_5; ?></span>
        </div>
      </div>
    </div>
  </div>
  <div class="pull-right" style="padding-bottom: 10px;"><?php echo osc_draw_button('Continue', null, null, 'success', null, null); ?></div>

<?php
      foreach ( $_POST as $key => $value ) {
        if (($key != 'x') && ($key != 'y')) {
          echo osc_draw_hidden_field($key, $value);
        }
      }
?>

  </form>
