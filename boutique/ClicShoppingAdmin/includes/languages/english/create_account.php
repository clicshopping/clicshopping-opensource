<?php
/*
 * create_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Customer account creation');

define('EMAIL_SUBJECT', 'Customer account creation on ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Hello Mr ' . $_POST['customers_firstname'] . ' ' . $_POST['customers_lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Hello Ms ' . $_POST['customers_firstname'] . ' ' . $_POST['customers_lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Hello ' . $_POST['customers_firstname'] . ' ' . $_POST['customers_lastname'] . ',' . "\n\n");

define('ENTRY_CUSTOMERS_COUPONS','Do you wish offer a coupon at your customer ?');
define('ENTRY_CUSTOMERS_EMAIL',' Do you wish send at your customer an email for this account creation ?');

// Autorisation de modification pour les clients
define('ENTRY_CUSTOMERS_MODIFY_COMPANY', 'To authorize the customer to modify this information');
define('ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT', 'To authorize the customer to modify the address by default');
define('ENTRY_CUSTOMERS_ADD_ADDRESS', 'To authorize the customer to add other addresses');


// TVA Intracom
define('TVA_INTRACOM_VERIFY','Verify VAT');
define('TITLE_AIDE_CUSTOMERS_TVA','NNote on VAT Intracom');
define('TITLE_AIDE_TVA_CUSTOMERS','Please, valid the document before to verify the society VAT');


// Social Network
define('EMAIL_NETWORK','Follow us in real time on  :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');
?>
