<?php
/**
 * page_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

////
// Status des pages d'accueil et d'informations 
  function osc_set_page_manager_status($pages_id, $status) {
      global $OSCOM_PDO;

      if ($status == '1') {


        $Qupdate = $OSCOM_PDO->prepare('update :table_pages_manager
                                        set status = :status,
                                            date_status_change =  now(),
                                            page_date_start = null
                                        where pages_id = :pages_id
                                      ');
        $Qupdate->bindInt(':status', 1);
        $Qupdate->bindInt(':pages_id', (int)$pages_id);

        return $Qupdate->execute();

      } elseif ($status == '0') {

        $Qupdate = $OSCOM_PDO->prepare('update :table_pages_manager
                                        set status = :status,
                                            date_status_change = now(),
                                            page_date_closed = null
                                        where pages_id = :pages_id
                                      ');

        $Qupdate->bindInt(':status', 0);
        $Qupdate->bindInt(':pages_id', (int)$pages_id);
        $Qupdate->execute();
        return $Qupdate->execute();

      } else {
        return -1;
      }
    }


// Auto activation des pages d'accueil et d'informations 
  function osc_activate_page_manager() {
    global $OSCOM_PDO;

      $QPages = $OSCOM_PDO->query('select pages_id
                                  from :table_pages_manager
                                  where page_date_start is not null
                                  and page_date_start <= now()
                                  and status != 1
                                 ');

      $QPages->execute;

      if ($QPages->fetch() !== false) {
        do {
          static::SetPageManagerStatus($QPages->valueInt('pages_id'), 1);
        } while($QPages->fetch());
      }
    }


// Auto expiration des pages d'accueil et d'informations
  function osc_expire_page_manager() {
    global $OSCOM_PDO;

    $QPages = $OSCOM_PDO->query('select pages_id
                                  from :table_pages_manager
                                  where status = 1
                                  and page_date_closed is not null
                                  and now() >= page_date_closed
                                ');

    $QPages->execute;

    if ($QPages->fetch() !== false) {
      do {
        static::SetPageManagerStatus($QPages->valueInt('pages_id'), '0');
      } while($QPages->fetch());
    }
  }

