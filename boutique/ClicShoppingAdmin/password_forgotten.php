<?php
/*
 * password_forgotten.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  ////
// This function makes a new password from a plaintext password.
  function osc_encrypt_password($plain) {
    $password = '';

    for ($i=0; $i<10; $i++) {
      $password .= osc_rand();
    }
    $salt = substr(md5($password), 0, 2);
    $password = md5($salt . $plain) . ':' . $salt;

    return $password;
  }
  
  
   function osc_create_random_value($length, $type = 'mixed') {
    if ( ($type != 'mixed') && ($type != 'chars') && ($type != 'digits')) return false;

    $rand_value = '';
    while (strlen($rand_value)<$length) {
      if ($type == 'digits') {
        $char = osc_rand(0,9);
      } else {
        $char = chr(osc_rand(0,255));
      }
      if ($type == 'mixed') {
        if (preg_match('#^[a-z0-9]$#i', $char)) $rand_value .= $char;
      } elseif ($type == 'chars') {
        if (preg_match('#^[a-z]$#i', $char)) $rand_value .= $char;
      } elseif ($type == 'digits') {
        if (preg_match('#^[0-9]$#', $char)) $rand_value .= $char;
      }
    }

    return $rand_value;
  }

  $QcheckCustomer = $OSCOM_PDO->prepare("select customers_firstname, 
                                               customers_lastname, 
                                               customers_password, 
                                               customers_id,
                                               customers_email_address 
                                         from :table_customers
                                         where customers_id = :customers_id
                                        ");
  $QcheckCustomer->bindInt(':customers_id', (int)$_GET['cID'] );
  $QcheckCustomer->execute();

  $check_customer = $QcheckCustomer->fetch();

// Crypted password mods - create a new password, update the database and mail it to them
  $newpass = osc_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);
  $crypted_password = osc_encrypt_password($newpass);

  $Qupdate = $OSCOM_PDO->prepare('update :table_customers 
                                  set customers_password = :customers_password 
                                  where customers_id = :customers_id
                                ');
  $Qupdate->bindValue(':customers_password', $crypted_password);
  $Qupdate->bindInt(':customers_id', (int)$check_customer['customers_id'] );
  $Qupdate->execute();  

  $text_password_body = EMAIL_PASSWORD_REMINDER_BODY;
  $text_password_body = html_entity_decode($text_password_body);

  osc_mail($check_customer['customers_firstname'] . " " . $check_customer['customers_lastname'], $check_customer['customers_email_address'], EMAIL_PASSWORD_REMINDER_SUBJECT, nl2br(sprintf($text_password_body, $check_customer['customers_email_address'],$newpass )), STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);
  $OSCOM_MessageStack->add_session(TEXT_NEW_PASSWORD . '&nbsp;' . ($check_customer['customers_firstname'] . " " . $check_customer['customers_lastname']), 'success');
  osc_redirect(osc_href_link('customers.php'));

  require('includes/application_bottom.php');
?>