<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Contenu du panier');
define('HEADING_TITLE', 'Contenu de votre panier...');
define('TABLE_HEADING_QUANTITY', 'Qté.');
define('TABLE_HEADING_PRODUCTS', 'Article(s)');
define('TABLE_HEADING_TOTAL', 'Total');
define('TEXT_CART_EMPTY', 'Votre panier est vide ');
define('SUB_TITLE_SUB_TOTAL', 'Sous-Total :');
define('SUB_TITLE_TOTAL', 'Total :');
define('TEXT_ALTERNATIVE_CHECKOUT_METHODS','Méthode de paiement alternatif');
define('IMAGE_BUTTON_REMOVE_PRODUCT', 'Supprimer ce produit du panier');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'Les articles marqués ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '  ne sont pas en stock dans la quantité désirée.<br />Merci de corriger la quantité des articles marqués (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ').');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Les articles marqués ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' ne sont pas en stock dans la quantité désirée.<br />Vous pouvez néanmoins les acheter ils vous seront délivrés dés disponibilités.');

define('SHOPPING_CART_INFORMATION_SAVE','Le contenu de votre panier est stocké plus de 30 jours aprés enregistrement, vous pouvez revenir quand vous le souhaitez.');
?>