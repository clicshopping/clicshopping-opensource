<?php
/**
 * html_output.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  /**
  * ULTIMATE Seo Urls 5 PRO by FWR Media
  * Replacement for osCommerce href link wrapper function
 * @param string $page The page to link to
 * @param string $parameters The parameters to pass to the page (in the GET scope)
 * @param string $connection The connection type for the page (NONSSL, SSL, AUTO)
 * @param boolean $add_session_id Conditionally add the session ID to the URL
 * @param boolean $search_engine_friendly Convert the URL to be search engine friendly
 * @param boolean $use_full_address Add the full server address to the URL
 * @access public
  */
  require_once (DIR_WS_MODULES . 'ultimate_seo_urls5/main/usu5.php');
  
  function osc_href_link( $page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true ) {
    return Usu_Main::i()->hrefLink( $page, $parameters, $connection, $add_session_id, $search_engine_safe );
  }

////
// The HTML image wrapper function
/**
 * Outputs an image
 *
 * @param string $image The image filename to display
 * @param string $src The path of the image
 * @param string $title The title of the image button
 * @param int $width The width of the image
 * @param int $height The height of the image
 * @param string $parameters Additional parameters for the image
 * @access public
 */

// templte modification
  function osc_image($src, $alt = '', $width = '', $height = '', $parameters = '', $responsive = true, $bootstrap_css = '') {

    if ((empty($src) || ($src == DIR_WS_IMAGES)  || ($src == 'NULL')) || 
        (empty($src) || ($src == DIR_WS_DEFAULT_IMAGES)  || ($src == 'NULL')) || 
        (!file_exists(DIR_FS_CATALOG . $src))  && (IMAGE_REQUIRED == 'false')) {

      $src = (DIR_WS_DEFAULT_IMAGES . 'nophoto.png');

      if (!file_exists(DIR_FS_CATALOG . $src)) {
        $src = ('images/nophoto.png');
    }

    if (IMAGE_REQUIRED == 'true') {
      return false;
    }
  }

// alt is added to the img tag even if it is null to prevent browsers from outputting
// the image filename as default
    $image = '<img src="' . osc_output_string($src) . '" alt="' . osc_output_string($alt) . '"';

    if (osc_not_null($alt)) {
      $image .= ' title="' . osc_output_string($alt) . '"';
    }

    global $binary_gateway;
    if ($binary_gateway == '') {
      if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true') && (empty($width) || empty($height)) ) {
          if (file_exists($src)) {
            if ($image_size = @getimagesize($src)) {
              if (empty($width) && osc_not_null($height)) {
                $ratio = $height / $image_size[1];
                $width = (int)($image_size[0] * $ratio);
              } elseif (osc_not_null($width) && empty($height)) {
                $ratio = $width / $image_size[0];
                $height = (int)($image_size[1] * $ratio);
              } elseif (empty($width) && empty($height)) {
                $width = $image_size[0];
                $height = $image_size[1];
              }
            }
        } elseif (IMAGE_REQUIRED == 'false') {
          return false;
        }
      }
    }
    if (osc_not_null($width) && osc_not_null($height)) {
      $image .= ' width="' . osc_output_string($width) . '" height="' . osc_output_string($height) . '"';
    }


    $image .= ' class="';

    if (osc_not_null($responsive) && ($responsive === true)) {
      $image .= 'img-responsive';
    }

    if (osc_not_null($bootstrap_css)) $image .= ' ' . $bootstrap_css;

    $image .= '"';

    if (osc_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= ' />';

    return $image;
  }

/**
 * Outputs an image submit button
 *
 * @param string $image The image filename to display
 * @param string $title The title of the image button
 * @param string $parameters Additional parameters for the image submit button
 * @access public
 */

////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
// Modification pour la Thema

  function osc_image_submit($image, $alt = '', $parameters = '', $responsive = false) {
    global $binary_gateway;

    if ($binary_gateway == '') {
      $image_submit = '<input type="image" src="' . osc_output_string(DIR_WS_IMAGES . 'template/' . SITE_THEMA . '/' . $_SESSION['language'] . '/buttons/' . $image) . '"  alt="' . osc_output_string($alt) . '"';
    } else {
      $image_submit = '<input type="image" src="' . osc_output_string($binary_gateway . HTTP_SERVER . DIR_WS_CATALOG .  DIR_WS_IMAGES .'template/' . SITE_THEMA  . '/' . $_SESSION['language'] . '/buttons/' . $image) . '" alt="' . osc_output_string($alt) . '"';
    }

    if (osc_not_null($alt)) $image_submit .= ' title=" ' . osc_output_string($alt) . ' "';

    if (osc_not_null($parameters)) $image_submit .= ' ' . $parameters;

    if (osc_not_null($responsive) && ($responsive === false)) {
      $image_submit .= ' class="img-responsive"';
    }

    $image_submit .= ' />';

    return $image_submit;
  }

/**
 * Outputs an image button
 *
 * @param string $image The image filename to display
 * @param string $title The title of the image button
 * @param string $parameters Additional parameters for the image button
 * @access public
 */
////
// Output a function button in the selected language
// Modification pour la Thema
  function osc_image_button($image, $alt = '', $parameters = '', $responsive = false) {
    global $binary_gateway;

    if (osc_not_null($responsive) && ($responsive === false)) {
      $image_responsive = ' class="img-responsive"';
    }

    if ($binary_gateway == '') {
      return osc_image(DIR_WS_IMAGES . 'template/' . SITE_THEMA . '/' . $_SESSION['language'] . '/' . $image, $alt, '', '', $parameters, $image_responsive);
    } else {
      return osc_image($binary_gateway . HTTP_SERVER . DIR_WS_CATALOG . DIR_WS_IMAGES .'template/' . SITE_THEMA  . '/' . $_SESSION['language'] . '/' . $image, $alt, '', '', $parameters, $image_responsive);
    }
  }

////
// Output a separator either through whitespace, or with an image
  function osc_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return osc_image(DIR_WS_DEFAULT_IMAGES . $image, '', $width, $height);
  }

////
// Output a form
  function osc_draw_form($name, $action, $method = 'post', $parameters = '', $tokenize = false) {

    $form = '<form name="' . osc_output_string($name) . '" action="' . osc_output_string($action) . '" method="' . osc_output_string($method) . '"';

    if (osc_not_null($parameters)) $form .= ' ' . $parameters;

    $form .= '>';

    if ( ($tokenize == true) && isset($_SESSION['sessiontoken']) ) {
      $form .= '<input type="hidden" name="formid" value="' . osc_output_string($_SESSION['sessiontoken']) . '">';
    }

    return $form;
  }

/**
 * Outputs a form password field
 *
 * @param string $name The name and ID of the password field
 * @param string $parameters Additional parameters for the password field
 * @access public
 */
////
// Output a form input field
  function osc_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true, $class = 'form-control') {

    $field = '<input type="' . osc_output_string($type) . '" name="' . osc_output_string($name) . '"';

    if (strpos($parameters, 'id=') === false) {
       $field .= ' id="' . osc_output_string($name) . '"';
    }

    if ( ($reinsert_value == true) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $value = $_GET[$name];
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $value = $_POST[$name];
      }
    }

    if (osc_not_null($value)) {
      $field .= ' value="' . osc_output_string($value) . '"';
    }

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    if (osc_not_null($class)) $field .= ' class="' . $class . '"';

    $field .= ' />';

    return $field;
  }

////
// Output a form password field
  function osc_draw_password_field($name, $value = '', $parameters = 'maxlength="40"') {
    return osc_draw_input_field($name, $value, $parameters, 'password', false);
  }

/**
 * Outputs a form selection field (checkbox/radio)
 *
 * @param string $name The name and indexed ID of the selection field
 * @param string $type The type of the selection field (checkbox/radio)
 * @param mixed $values The value of, or an array of values for, the selection field
 * @param string $default The default value for the selection field
 * @param string $parameters Additional parameters for the selection field
 * @param string $separator The separator to use between multiple options for the selection field
 * @access public
 */
////
// Output a selection field - alias function for osc_draw_checkbox_field() and osc_draw_radio_field()
  function osc_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {

    $selection = '<input type="' . osc_output_string($type) . '" name="' . osc_output_string($name) . '"';

    if (osc_not_null($value)) $selection .= ' value="' . osc_output_string($value) . '"';

    if ( ($checked == true) || (isset($_GET[$name]) && is_string($_GET[$name]) && (($_GET[$name] == 'on') || ($_GET[$name] == $value))) || (isset($_POST[$name]) && is_string($_POST[$name]) && (($_POST[$name] == 'on') || ($_POST[$name] == $value))) ) {
      $selection .= ' checked="checked"';
    }

    if (osc_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= ' />';

    return $selection;
  }

/**
 * Outputs a form checkbox field
 *
 * @param string $name The name and indexed ID of the checkbox field
 * @param mixed $values The value of, or an array of values for, the checkbox field
 * @param string $default The default value for the checkbox field
 * @param string $parameters Additional parameters for the checkbox field
 * @param string $separator The separator to use between multiple options for the checkbox field
 * @access public
 */
////
// Output a form checkbox field
  function osc_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return osc_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

////
// Bouton radio
// Output a form radio field
  function osc_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return osc_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }

/**
 * Outputs a form textarea field
 *
 * @param string $name The name and ID of the textarea field
 * @param string $value The default value for the textarea field
 * @param int $width The width of the textarea field
 * @param int $height The height of the textarea field
 * @param string $parameters Additional parameters for the textarea field
 * @param boolean $override Override the default value with the value found in the GET or POST scope
 * @access public
 */
////
// Output a form textarea field
  function osc_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true, $class = 'form-control') {

    $field = '<textarea class="form-control" name="' . osc_output_string($name) . '" wrap="' . osc_output_string($wrap) . '" cols="' . osc_output_string($width) . '" rows="' . osc_output_string($height) . '"';

    if (strpos($parameters, 'id=') === false) {
      $field .= ' id="' . osc_output_string($name) . '"';
    }

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    if (osc_not_null($class)) $field .= ' class="' . $class . '"';

    $field .= '>';

    if ( ($reinsert_value == true) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $field .= osc_output_string_protected(stripslashes($_GET[$name]));
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $field .= osc_output_string_protected(stripslashes($_POST[$name]));
      }
    } elseif (osc_not_null($text)) {
      $field .= osc_output_string_protected($text);
    }

    $field .= '</textarea>';

    return $field;
  }

/**
 * Outputs a form hidden field
 *
 * @param string $name The name of the hidden field
 * @param string $value The value for the hidden field
 * @param string $parameters Additional parameters for the hidden field
 * @access public
 */
  function osc_draw_hidden_field($name, $value = '', $parameters = '') {

    $field = '<input type="hidden" name="' . osc_output_string($name) . '"';

    if (osc_not_null($value)) {
      $field .= ' value="' . osc_output_string($value) . '"';
    } elseif ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) {
      if ( (isset($_GET[$name]) && is_string($_GET[$name])) ) {
        $field .= ' value="' . osc_output_string(stripslashes($_GET[$name])) . '"';
      } elseif ( (isset($_POST[$name]) && is_string($_POST[$name])) ) {
        $field .= ' value="' . osc_output_string(stripslashes($_POST[$name])) . '"';
      }
    }

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= ' />';

    return $field;
  }

////
// Hide form elements
  function osc_hide_session_id() {
    global $session_started, $SID;

    if (($session_started == true) && osc_not_null($SID)) {
      return osc_draw_hidden_field(session_name(), session_id());
    }
  }

/**
 * Outputs a form pull down menu for a date selection
 *
 * @param string $name The base name of the date pull down menu fields
 * @param array $value An array containing the year, month, and date values for the default date (year, month, date)
 * @param boolean $default_today Default to todays date if no default value is used
 * @param boolean $show_days Show the days in a pull down menu
 * @param boolean $use_month_names Show the month names in the month pull down menu
 * @param int $year_range_start The start of the years range to use for the year pull down menu
 * @param int $year_range_end The end of the years range to use for the year pull down menu
 * @access public
 */
////
// Output a form pull down menu
// Menu deroulant
  function osc_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false, $class = 'form-control') {
    $field = '<select name="' . osc_output_string($name) . '"';
 
    if (strpos($parameters, 'id=') === false) {
      $field .= ' id="' . osc_output_string($name) . '"';
    }
 

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;

    if (osc_not_null($class)) $field .= ' class="' . $class . '"';

    $field .= '>';

    if (empty($default) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $default = $_GET[$name];
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $default = $_POST[$name];
      }
    }

    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . osc_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' selected="selected"';
      }

      $field .= '>' . osc_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

/**
 * Creates a pull-down list of countries
 *
 * @param string $name The name of the hidden field
 * @param string $selected The value for the hidden field
 * @param string $parameters Additional parameters for the hidden field
 * @access public
 */
////
// Creates a pull-down list of countries

  function osc_get_country_list($name, $selected = '', $parameters = '') {
    $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
    $countries = osc_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name'], 'iso' => $countries[$i]['countries_iso_code_2']);
    }

    return osc_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }

// Creates a pull-down list of countries for B2B
  function osc_get_iso_list($name, $selected = '', $parameters = '') {
    $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
    $countries = osc_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_iso_code_2'], 'text' => $countries[$i]['countries_name']);
    }

    return osc_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }


////

/**
 * Creates a aui jquery button
 * Output a jQuery UI Button
 * @param string $title , title of the button
 * @param string $$icon, ui icon of the button
 * @param string $link, link
 * @param string $priotiy; primary or secondary
 * @param string $param primary or secondaty
 * @param string $id, stylesheet of the button via an a id
 * @param string $target, _blank, self, _top,_new,_parent   of the link
 * @param return $button, the button
 * osc_draw_button('HOME', 'triangle-1-n', 'http://www.website.com/home.html', null, null, null, 'parent')
 * @access public
 */
////
//-------------------------
// Output a Bootstrap Button
// ------------------------
// 
    function osc_draw_button($title = null, $icon = null, $link = null, $style = null, $params = null,  $size = null, $target = null) {
      $types = array('submit', 'button', 'reset');
      $styles = array('primary', 'info', 'success', 'warning', 'danger', 'inverse', 'link', 'new', 'secondary');
      $size_button = array('lg', 'sm', 'xs');

      if ( !isset($params['type']) ) {
        $params['type'] = 'submit';
      }

      if ( !in_array($params['type'], $types) ) {
        $params['type'] = 'submit';
      }

      if ( ($params['type'] == 'submit') && isset($link) ) {
        $params['type'] = 'button';
      }

      if ( isset($style) && !in_array($style, $styles) ) {
        unset($style);
      }

      if ( isset($size) && !in_array($size, $size_button) ) {
        unset($size);
      }

      $button = '';

      if ( ($params['type'] == 'button') && isset($link) ) {
        $button .= '<a href="' . $link . '"';

       if ( isset($params['newwindow']) && !isset($target) ) {
          $button .= ' target="_blank"';
  // link target
        if (isset($target)) {
          if ($target == 'parent' ){
            $button .= ' target="_parent"';
          }
          if ($target == 'blank' ){
            $button .= ' target="_blank"';
          }
          if ($target == 'top' ){
            $button .= ' target="_top"';
          }
          if ($target == 'self' ){
            $button .= ' target="_self"';
          }
          if ($target == 'new' ){
           $button .= ' target="_new"';
          }
        }
      }


      } else {
        $button .= '<button type="' . osc_output_string($params['type']) . '"';
      }

      if ( isset($params['params']) ) {
        $button .= ' ' . $params['params'];
      }

      $button .= ' class="btn';

      if ( isset($style) ) {
        $button .= ' btn-' . $style;
      }


      if ( isset($size) ) {
        $button .= ' btn-' . $size;
      }

      $button .= '">';

      if ( isset($icon) ) {
        if ( !isset($params['iconpos']) ) {
          $params['iconpos'] = 'left';
        }

        if ( $params['iconpos'] == 'left' ) {
          $button .= '<span class="' . $icon;

          if ( isset($style) ) {
            $button .= '';
          }

          $button .= '"></span> ';
        }
      }

      $button .= $title;

      if ( isset($icon) && ($params['iconpos'] == 'right') ) {
        $button .= ' <span class="'.$icon;

        if ( isset($style) ) {
          $button .= '';
        }

        $button .= '"></span>';
      }

      if ( ($params['type'] == 'button') && isset($link) ) {
        $button .= '</a>';
      } else {
        $button .= '</button>';
      }

      return $button;
    }

// review stars
  function osc_draw_stars($rating = 0, $empty = true) {
    $stars = str_repeat('<span class="glyphicon glyphicon-star"></span>', (int)$rating);
    if ($empty === true) $stars .= str_repeat('<span class="glyphicon glyphicon-star-empty"></span>', 5-(int)$rating);

    return $stars;
  }

?>