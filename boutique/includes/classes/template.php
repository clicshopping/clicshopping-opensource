<?php
/**
 * template.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class template {
    protected $_template = 'template';
    var $_title;
    var $_description;
    var $_keywords;
    var $_Newskeywords;

    var $_blocks = array();
    var $_content = array();
    var $_grid_container_width = GRID_CONTAINER_WITH;
    var $_grid_content_width = GRID_CONTENT_WITH;
    var $_grid_column_width = 0; // deprecated
    var $_data = array();

    public function __construct() {
      $this->_title = STORE_NAME;
      $this->_description = '';
      $this->_keywords = '';
      $this->_Newskeywords = '';
    }
  
    public function getCode() {
      return $this->_template;
    }

    function setGridContainerWidth($width) {
      $this->_grid_container_width = $width;
    }

    function getGridContainerWidth() {
      return $this->_grid_container_width;
    }

    function setGridContentWidth($width) {
      $this->_grid_content_width = $width;
    }

    function getGridContentWidth() {
      return $this->_grid_content_width;
    }

    function setGridColumnWidth($width) {
      $this->_grid_column_width = $width;
    }

    function getGridColumnWidth() {
      return (12 -GRID_CONTENT_WITH) / 2;
    }

    function setTitle($title) {
      $this->_title = $title;
    }

    function getTitle() {
      return $this->_title;
    }

    function setDescription($description) {
      $this->_description = $description;
    }

    function getDescription() {
      return $this->_description;
    }

    function setKeywords($keywords) {
      $this->_keywords = $keywords;
    }

    function getKeywords() {
      return $this->_keywords;
    }

    function setNewsKeywords($Newskeywords) {
      $this->_Newskeywords = $Newskeywords;
    }

    function getNewsKeywords() {
      return $this->_Newskeywords;
    }

    function addBlock($block, $group) {
      $this->_blocks[$group][] = $block;
    }

    function hasBlocks($group) {
      return (isset($this->_blocks[$group]) && !empty($this->_blocks[$group]));
    }

    function getBlocks($group) {
      if ($this->hasBlocks($group)) {
        return implode("\n", $this->_blocks[$group]);
      }
    }

    function buildBlocks() {
      global $PHP_SELF, $OSCOM_Customer;

      if ( defined('TEMPLATE_BLOCK_GROUPS') && osc_not_null(TEMPLATE_BLOCK_GROUPS) ) {
        $tbgroups_array = explode(';', TEMPLATE_BLOCK_GROUPS);

        foreach ($tbgroups_array as $group) {
          $module_key = 'MODULE_' . strtoupper($group) . '_INSTALLED';

          if ( defined($module_key) && osc_not_null(constant($module_key)) ) {
            $modules_array = explode(';', constant($module_key));

            foreach ( $modules_array as $module ) {
              $class = basename($module, '.php');

              if ( !class_exists($class) ) {

               if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . DIRECTORY_SEPARATOR .'languages' .DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' .DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $module)) {
                 if (file_exists(DIR_WS_LANGUAGES . $_SESSION['language'] . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $module)) {
                     require(DIR_WS_TEMPLATE . SITE_THEMA . DIRECTORY_SEPARATOR . 'languages'. DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $module);
                     require(DIR_WS_LANGUAGES . $_SESSION['language'] . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $module);
                 } else {
                     require(DIR_WS_TEMPLATE . SITE_THEMA . DIRECTORY_SEPARATOR . 'languages'. DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $module);
                 }
               } else {
// module par defaut
                 include(DIR_WS_LANGUAGES . $_SESSION['language'] . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . DIRECTORY_SEPARATOR . $module);
               }

//mode privee ou ouvert - affichage des boxes gauche ou droite
               if ( MODE_VENTE_PRIVEE == 'true' && $OSCOM_Customer->isLoggedOn() ) {
                  $modules_boxes = 'modules_boxes';
               } elseif (MODE_VENTE_PRIVEE == 'true' && !$OSCOM_Customer->isLoggedOn ) {
                  $modules_boxes = '';
               } else {
                  $modules_boxes = 'modules_boxes';
               }

               if ($group == $modules_boxes ||
                   $group == 'modules_account_customers' ||
                   $group == 'modules_account_history_info' ||
                   $group == 'modules_advanced_search' ||
                   $group == 'modules_blog' ||
                   $group == 'modules_blog_content' ||
                   $group == 'modules_create_account' ||
                   $group == 'modules_create_account_pro' ||
                   $group == 'modules_contact_us' ||
                   $group == 'modules_checkout_shipping' || 
                   $group == 'modules_checkout_payment' ||
                   $group == 'modules_checkout_confirmation' ||
                   $group == 'modules_checkout_success' ||
                   $group == 'modules_front_page' || 
                   $group == 'modules_footer' ||
                   $group == 'modules_footer_suffix' ||
                   $group == 'modules_header' ||
                   $group == 'modules_index_categories' ||
                   $group == 'modules_login'||
                   $group == 'modules_products_info' ||
                   $group == 'modules_products_listing' ||
                   $group == 'modules_products_new' ||
                   $group == 'modules_products_heart' || 
                   $group == 'modules_products_special' ||
                   $group == 'modules_shopping_cart' ||
                   $group == 'modules_sitemap' ||
                   $group == 'modules_products_featured'
               ) {
// verifie si le module existe dans le template sinon il prend le module par defaut

                 if (file_exists(DIR_WS_TEMPLATE . SITE_THEMA . DIR_WS_TEMPLATE_MODULES . $group . DIRECTORY_SEPARATOR . $class . '.php')) {
                   include(DIR_WS_TEMPLATE . SITE_THEMA . DIR_WS_TEMPLATE_MODULES . $group . DIRECTORY_SEPARATOR . $class . '.php');
                 } else {
                   if ( file_exists(DIR_WS_MODULES . $group . DIRECTORY_SEPARATOR . $class . '.php') ) {
                     include(DIR_WS_MODULES . $group . DIRECTORY_SEPARATOR . $class . '.php');
                   }
                 }
               } else {
// module par defaut
                  if ( file_exists(DIR_WS_MODULES . $group . DIRECTORY_SEPARATOR . $class . '.php') ) {
                    include(DIR_WS_MODULES . $group . DIRECTORY_SEPARATOR . $class . '.php');
                  }
               }
             }

              if ( class_exists($class) ) {
              $mb = new $class();

// Template dynamique
                if(!isset($mb->pages) && ($mb->isEnabled())){
                  $this->pages = 'all';
                  $mb->execute();
                } else {
                  if(($mb->isEnabled()) && (($mb->pages === 'all') || (in_array($PHP_SELF , explode(';' , $mb->pages))))) {
//                  if(($mb->isEnabled()) && (($mb->pages === 'all') && ($category_depth != 'top')) || (in_array($PHP_SELF , explode(';' , $mb->pages)))){
                    $mb->execute();
                  }
                }
              // eof Dynamic Template System
              }
            }
          }
        }
      }
    }

    function addContent($content, $group) {
      $this->_content[$group][] = $content;
    }

    function hasContent($group) {
      return (isset($this->_content[$group]) && !empty($this->_content[$group]));
    }

    function getContent($group) {
/*
      if ( !class_exists('tp_' . $group) && file_exists(DIR_WS_MODULES . 'pages/tp_' . $group . '.php') ) {
        include(DIR_WS_MODULES . 'pages/tp_' . $group . '.php');
      }

      if ( class_exists('tp_' . $group) ) {
        $template_page_class = 'tp_' . $group;
        $template_page = new $template_page_class();
        $template_page->prepare();
      }
*/

      foreach ( $this->getContentModules($group) as $module ) {

        if ( !class_exists($module) ) {

// file
          if (file_exists(DIR_WS_TEMPLATE . SITE_THEMA . DIR_WS_TEMPLATE_MODULES . $group . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . $module . '.php')) {
            include(DIR_WS_TEMPLATE . SITE_THEMA . DIR_WS_TEMPLATE_MODULES . $group . DIRECTORY_SEPARATOR . 'content' . DIRECTORY_SEPARATOR . $module . '.php');
          } else {
            include(DIR_WS_TEMPLATE . SITE_THEMA . DIR_WS_TEMPLATE_MODULES . $group . DIRECTORY_SEPARATOR . 'content' . $module . '.php');
          }

//language
          if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . DIRECTORY_SEPARATOR . 'languages'. DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group .  $module . '.php')) {
            if (file_exists(DIR_WS_LANGUAGES . $_SESSION['language'] . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . $module . 'php')) {
              require(DIR_WS_TEMPLATE . SITE_THEMA . DIRECTORY_SEPARATOR . 'languages'. DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . $module . '.php');
              require(DIR_WS_LANGUAGES . $_SESSION['language'] . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . $module . '.php');
            } else {
              require(DIR_WS_TEMPLATE . SITE_THEMA . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group . $module . '.php');
            }
          } else {
// module par defaut
            include(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . DIRECTORY_SEPARATOR . 'languages'. DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $group .  $module . '.php');
          }
        }

        if ( class_exists($module) ) {
          $mb = new $module();

          if ( $mb->isEnabled() ) {
            $mb->execute();
          }
        }
      }

/*
      if ( class_exists('tp_' . $group) ) {
        $template_page->build();
      }
*/
      if ($this->hasContent($group)) {
        return implode("\n", $this->_content[$group]);
      }
    }

    function getContentModules($group) {
      $result = array();

      foreach ( explode(';', MODULE_CONTENT_INSTALLED) as $m ) {
        $module = explode('/', $m, 2);

        if ( $module[0] == $group ) {
          $result[] = $module[1];
        }
      }

      return $result;
    }


/**
  * Select the default template and verify if it exist
  *
  * @param string $thema
  * @access private
*/

    private function setTemplateThema() {
      if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . DIR_WS_TEMPLATE_FILES . 'index.php') ) {
        $thema = DIR_WS_TEMPLATE . SITE_THEMA;
      } else {
        osc_redirect(HTTP_SERVER . DIR_WS_HTTP_CATALOG  . 'thema_template.php');
        clearstatcache();
      }
      return $thema;
    }

/**
   * Select the header or footer of the template
   *
   * @param string $name, header or footer of the template
   * @access public
 */
    public function getTemplateHeaderFooter($name) {
      return $this->setTemplateThema() . DIRECTORY_SEPARATOR . $name.'.php';
    }

/**
   * Select the css in directory of the template by language
   *
   * @param string $themaGraphism, css directory in the template
   * @access public
 */
    public function getTemplategraphism() {
      $themaGraphism = $this->setTemplateThema(). DIR_WS_TEMPLATE_GRAPHISM . $_SESSION['language'] . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR .'compressed_css.php';
      return $themaGraphism;
    }


/**
  * Select the the file in this directory Files
  *
  * @param string $themaGraphism, file in this directory Files
  * @access public
*/
    public function getTemplateFiles($name) {
      $themaFiles = $this->setTemplateThema()  .  DIR_WS_TEMPLATE_FILES . $name.'.php';
      return $themaFiles;
    }

/**
   * Select the the file in this directory module
   *
   * @param string $themaGraphism, file in this directory module
   * @access public
 */
    public function getTemplateModules($name) {
      $themaFiles = $this->setTemplateThema()  .  DIR_WS_TEMPLATE_MODULES . $name.'.php';
      return $themaFiles;
    }

/**
  * Select the the filename in this directory Modules
  *
  * @param string $themaFilename, filename in this module
  * @access public
*/
    public function getTemplateModulesFilename($name) {
      $themaFilename = $this->setTemplateThema()  .  DIR_WS_TEMPLATE_MODULES .  $name;
      return $themaFilename;
    }

/**
 * Select the file language in function the the file for the template
 *
 * @param string $languagefiles, file language in function the the file for the template
 * @access public
 */

    public function GeTemplatetLanguageFiles($name) {
      if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR . $_SESSION['language']  . DIRECTORY_SEPARATOR . $name . '.php') ) {
        $languagefiles = DIR_WS_TEMPLATE . SITE_THEMA . DIRECTORY_SEPARATOR . 'languages' . DIRECTORY_SEPARATOR .  $_SESSION['language'] . DIRECTORY_SEPARATOR . $name . '.php';
        if (file_exists(DIR_WS_LANGUAGES . $_SESSION['language']  . DIRECTORY_SEPARATOR . $name . '.php')) {
          $languagefiles = DIR_WS_LANGUAGES . $_SESSION['language'] . DIRECTORY_SEPARATOR . $name . '.php';
        }
      } else {
        $languagefiles = DIR_WS_LANGUAGES .$_SESSION['language'] . DIRECTORY_SEPARATOR . $name . '.php';
      }
      return $languagefiles;
    }

    public function getTemplateFilename($name) {
      return DIR_WS_SOURCES . $this->_template . SITE_THEMA. DIRECTORY_SEPARATOR . 'header.php';
    }
  }
