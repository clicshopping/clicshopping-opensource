<?php
/**
 * manufacturers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {

      case 'setflag':
        osc_set_manufacturers_status($_GET['id'], $_GET['flag']);
        osc_redirect(osc_href_link('manufacturers.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'mID=' . $_GET['id']));
        break;

      case 'insert':

           if (isset($_GET['mID'])) $manufacturers_id = osc_db_prepare_input($_GET['mID']);

           $manufacturers_name = osc_db_prepare_input($_POST['manufacturers_name']);
           $language_id = osc_db_prepare_input($_POST['languages_id']);
           $manufacturers_image = osc_db_prepare_input($_POST['manufacturers_image']);

// Insertion images des fabricants via l'editeur FCKeditor (fonctionne sur les nouvelles et editions des fabricants)
        if (isset($_POST['manufacturers_image']) && osc_not_null($_POST['manufacturers_image']) && ($_POST['manufacturers_image'] != 'none') && ($_POST['delete_image'] != 'yes')) {
          $manufacturers_image = htmlspecialchars($manufacturers_image);
          $manufacturers_image = strstr($manufacturers_image, DIR_WS_CATALOG_IMAGES);
          $manufacturers_image = str_replace(DIR_WS_CATALOG_IMAGES, '', $manufacturers_image);
          $manufacturers_image_end = strstr($manufacturers_image, '&quot;');
          $manufacturers_image = str_replace($manufacturers_image_end, '', $manufacturers_image);
          $manufacturers_image = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $manufacturers_image);
        } else {
          $manufacturers_image = 'null';
        }

        $sql_data_array = array('manufacturers_name' => $manufacturers_name);

          if ($manufacturers_image != 'null') {
            $insert_image_sql_data = array('manufacturers_image' => $manufacturers_image);
            $sql_data_array = array_merge($sql_data_array, $insert_image_sql_data);
         }

          $insert_sql_data = array('date_added' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          $OSCOM_PDO->save('manufacturers', $sql_data_array );

          $manufacturers_id = $OSCOM_PDO->lastInsertId();

          $languages = osc_get_languages();

          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {

            $manufacturers_url_array = $_POST['manufacturers_url'];
            $manufacturer_description_array =  $_POST['manufacturer_description'];
            $manufacturer_seo_title_array =  $_POST['manufacturer_seo_title'];
            $manufacturer_seo_description_array =  $_POST['manufacturer_seo_description'];
            $manufacturer_seo_keyword_array =  $_POST['manufacturer_seo_keyword'];
            $language_id = $languages[$i]['id'];

            $sql_data_array = array('manufacturers_id' => $manufacturers_id);

             if ($action == 'insert') {
               $insert_sql_data = array('manufacturers_url' => osc_db_prepare_input($manufacturers_url_array[$language_id]),
                                        'languages_id' => $language_id,
                                        'manufacturer_description' => osc_db_prepare_input($manufacturer_description_array[$language_id]),
                                        'manufacturer_seo_title' => osc_db_prepare_input($manufacturer_seo_title_array[$language_id]),     
                                        'manufacturer_seo_description' => osc_db_prepare_input($manufacturer_seo_description_array[$language_id]),
                                        'manufacturer_seo_keyword' => osc_db_prepare_input($manufacturer_seo_keyword_array[$language_id])
                                       );

               $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

               osc_db_perform('manufacturers_info', $sql_data_array);
             }
          }

          if (USE_CACHE == 'true') {
            osc_reset_cache_block('manufacturers');
          }


//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_MANUFACTURER_ADMIN == 'true') {
          require('ext/odoo_xmlrpc/xml_rpc_admin_manufacturer.php');
        }
//***************************************
// End odoo web service
//***************************************

        osc_redirect(osc_href_link('manufacturers.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'mID=' . $manufacturers_id));
        break;

      case 'update':

          if (isset($_GET['mID'])) $manufacturers_id = osc_db_prepare_input($_GET['mID']);
          $manufacturers_image = osc_db_prepare_input($_POST['manufacturers_image']);
          $manufacturers_name = osc_db_prepare_input($_POST['manufacturers_name']);


          $Qupdate = $OSCOM_PDO->prepare('update :table_manufacturers
                                          set last_modified = now(),
                                              manufacturers_name = :manufacturers_name
                                           where manufacturers_id = :manufacturers_id
                                        ');

          $Qupdate->bindValue(':manufacturers_name', $manufacturers_name );
          $Qupdate->bindInt(':manufacturers_id', (int)$manufacturers_id);
          $Qupdate->execute();

// Insertion images des fabricants via l'editeur FCKeditor (fonctionne sur les nouvelles et editions des fabricants)
        if (isset($_POST['manufacturers_image']) && osc_not_null($_POST['manufacturers_image']) && ($_POST['manufacturers_image'] != 'none') && ($_POST['delete_image'] != 'yes')) {

          $manufacturers_image = htmlspecialchars($manufacturers_image);
          $manufacturers_image = strstr($manufacturers_image, DIR_WS_CATALOG_IMAGES);
          $manufacturers_image = str_replace(DIR_WS_CATALOG_IMAGES, '', $manufacturers_image);
          $manufacturers_image_end = strstr($manufacturers_image, '&quot;');
          $manufacturers_image = str_replace($manufacturers_image_end, '', $manufacturers_image);

          $manufacturers_image = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $manufacturers_image);

          $Qupdate = $OSCOM_PDO->prepare('update :table_manufacturers
                                          set manufacturers_image = :manufacturers_image
                                          where manufacturers_id = :manufacturers_id
                                        ');

          $Qupdate->bindValue(':manufacturers_image', $manufacturers_image);
          $Qupdate->bindInt(':manufacturers_id', (int)$manufacturers_id);

          $Qupdate->execute();
        }

// Suppression de l'image
        if ($_POST['delete_image'] == 'yes') {

          $Qupdate = $OSCOM_PDO->prepare('update :table_manufacturers
                                          set manufacturers_image = :manufacturers_image
                                          where manufacturers_id = :manufacturers_id
                                        ');

          $Qupdate->bindValue(':manufacturers_image','' );
          $Qupdate->bindInt(':manufacturers_id', (int)$manufacturers_id);

          $Qupdate->execute();
        }

        $languages = osc_get_languages();

        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $manufacturers_url_array = $_POST['manufacturers_url'];
          $manufacturer_description_array = $_POST['manufacturer_description'];
          $manufacturer_seo_title_array = $_POST['manufacturer_seo_title'];
          $manufacturer_seo_description_array = $_POST['manufacturer_seo_description'];
          $manufacturer_seo_keyword_array = $_POST['manufacturer_seo_keyword'];
          $language_id = $languages[$i]['id'];

          $sql_data_array = array('manufacturers_url' => osc_db_prepare_input($manufacturers_url_array[$language_id]),
                                  'manufacturer_description' => osc_db_prepare_input($manufacturer_description_array[$language_id]),
                                  'manufacturer_seo_title' => osc_db_prepare_input($manufacturer_seo_title_array[$language_id]),
                                  'manufacturer_seo_description' => osc_db_prepare_input($manufacturer_seo_description_array[$language_id]),
                                  'manufacturer_seo_keyword' => osc_db_prepare_input($manufacturer_seo_keyword_array[$language_id])
         );

          $OSCOM_PDO->save('manufacturers_info', $sql_data_array, ['manufacturers_id' => (int)$manufacturers_id,
                                                                   'language_id' => (int)$language_id
                                                                  ]
                          );
       }

        if (USE_CACHE == 'true') {
          osc_reset_cache_block('manufacturers');
        }
// seo
        if (SEO_URLS_ENABLED =='true') {
         osc_reset_cache_data_usu5('reset');
        }

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_MANUFACTURER_ADMIN == 'true') {
          require('ext/odoo_xmlrpc/xml_rpc_admin_manufacturer.php');
        }
//***************************************
// End odoo web service
//***************************************

        osc_redirect(osc_href_link('manufacturers.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'mID=' . $manufacturers_id));

     break;

// the images are not deleted in this case
      case 'delete_all':

/*
        if (isset($_POST['delete_products']) && ($_POST['delete_products'] == 'on')) {
          $products_query = osc_db_query("select products_id from products where manufacturers_id = '" . (int)$manufacturers_id . "'");


          while ($products = osc_db_fetch_array($products_query)) {
            osc_remove_product($products['products_id']);
          }
        } else {
          osc_db_query("update products set manufacturers_id = '' where manufacturers_id = '" . (int)$manufacturers_id . "'");
        }
*/
       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $manufacturers['manufacturers_id'] ) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_manufacturers
                                          where manufacturers_id = :manufacturers_id
                                        ');
          $Qdelete->bindInt(':manufacturers_id', (int)$manufacturers['manufacturers_id']);
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_manufacturers_info
                                          where manufacturers_id = :manufacturers_id
                                        ');
          $Qdelete->bindInt(':manufacturers_id', (int)$manufacturers['manufacturers_id']);
          $Qdelete->execute();

          $products_query = osc_db_query("select products_id
                                          from products
                                          where manufacturers_id = '" . (int)$manufacturers_id . "'
                                         ");

          $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                          set products_status = :products_status,
                                              manufacturers_id = :manufacturers_id
                                           where manufacturers_id = :manufacturers_id
                                        ');

          $Qupdate->bindInt(':products_status', '0' );
          $Qupdate->bindValue(':manufacturers_id','' );
          $Qupdate->bindInt(':manufacturers_id', (int)$manufacturers_id);

          $Qupdate->execute();

         }
/*
          if (isset($_POST['delete_image']) && ($_POST['delete_image'] == 'on')) {
            $manufacturers_image_query = osc_db_query("select manufacturers_image 
                                                       from manufacturers
                                                       where manufacturers_id = '" . (int)$manufacturers_id . "'
                                                      ");
            $manufacturers_image = osc_db_fetch_array($manufacturers_image_query);

            $image_location = DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . DIR_WS_PRODUCTS . $manufacturers_image['manufacturers_image'];

            if (file_exists($image_location)) @unlink($image_location);
         }
*/
       }

        if (USE_CACHE == 'true') {
          osc_reset_cache_block('manufacturers');
        }

       osc_redirect(osc_href_link('manufacturers.php'));
      break;
    }
  }

  if (empty($action)) {

    $manufacturers_query_raw = "select manufacturers_id, 
                                       manufacturers_name, 
                                       manufacturers_image, 
                                       date_added, 
                                       last_modified,
                                       manufacturers_status
                                from manufacturers
                                order by manufacturers_name
                               ";
    $manufacturers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $manufacturers_query_raw, $manufacturers_query_numrows);
    $manufacturers_query = osc_db_query($manufacturers_query_raw);
  }

  require('includes/header.php');
?>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        <div class="adminTitle">
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/manufacturers.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
            <span class="col-md-4 smallText" style="text-align: center;">
<?php echo $manufacturers_split->display_count($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS); ?><br />
<?php echo $manufacturers_split->display_links($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
            </span>
            <span class="pull-right">
            <?php echo '<a href="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&mID=' . $mInfo->manufacturers_id . '&action=new') . '">' . osc_image_button('button_insert_manufacturers.gif', IMAGE_INSERT) . '</a>'; ?></span>
            </span>
            <span class="pull-right">
              <form name="delete_all" <?php echo 'action="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post"><a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
            </span>

<?php
  } else if ( ($action == 'new') || ($action == 'edit') ) {
    $form_action = 'insert';
    if ( ($action == 'edit') && isset($_GET['mID']) ) {
      $form_action = 'update';
    }
?>
          <form name="manufacturers" <?php echo 'action="' . osc_href_link('manufacturers.php', osc_get_all_get_params(array('action', 'info', 'mID')) . 'action=' . $form_action . '&mID=' . $_GET['mID']) . '"'; ?> method="post"><?php if ($form_action == 'update') echo osc_draw_hidden_field('manufacturers_id', $_GET['mID']); ?>
            <span></span>
            <span class="pull-right">&nbsp;<?php echo (($form_action == 'insert') ? osc_image_submit('button_insert_specials.gif', IMAGE_INSERT) : osc_image_submit('button_update.gif', IMAGE_UPDATE)); ?></span>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&mID=' . $_GET['mID']).'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
            <?php
            }
            ?>
        </div>
      </div>
      <div class="clearfix"></div>

<?php // Gestion des erreurs et de succes des validations
    if ($OSCOM_MessageStack->size > 0) {
      $ClassMessageStack = "messageStackSuccess";
      if ($ClassMessageStackError == 1) {
        $ClassMessageStack = "messageStackError";
      }
    }
?>
<!-- //################################################################################################################ -->
<!-- //                                      EDITION ET INSERTION D'UNE MARQUE                                       -->
<!-- //################################################################################################################ -->
<?php
  if ( ($action == 'new') || ($action == 'edit') )  {
    $form_action = 'insert';

    if ( ($action == 'edit') && isset($_GET['mID']) ) {
     $form_action = 'update';

      $Qmanufacturers = $OSCOM_PDO->prepare('select m.manufacturers_id, 
                                                     m.manufacturers_name, 
                                                     m.manufacturers_image, 
                                                     m.date_added, 
                                                     m.last_modified,
                                                     md.manufacturer_description,
                                                     md.manufacturers_url,
                                                     md.manufacturer_seo_title,
                                                     md.manufacturer_seo_description,
                                                     md.manufacturer_seo_keyword
                                               from :table_manufacturers  m,
                                                    :table_manufacturers_info md 
                                              where m.manufacturers_id = md.manufacturers_id 
                                              and md.languages_id = :languages_id
                                              and m.manufacturers_id = :manufacturers_id
                                            ');
      $Qmanufacturers->bindValue(':languages_id', (int)$_SESSION['languages_id'] );
      $Qmanufacturers->bindValue(':manufacturers_id', (int)$_GET['mID']);
      $Qmanufacturers->execute();

      $manufacturers = $Qmanufacturers->fetch();

      $mInfo = new objectInfo($manufacturers);

    } else {
      $mInfo = new objectInfo(array());
   }
  
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="5" cellpadding="0" class="<?php echo $ClassMessageStack; ?>">
          <tr>
            <td class="<?php echo $ClassMessageStack; ?>">
              <table width="100%">
                <?php echo $OSCOM_MessageStack->output(); ?>
            </table>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
              <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_DESCRIPTION; ?></a></li>
              <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_VISUEL; ?></a></li>
              <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_SEO; ?></a></li>
            </ul>

            <div class="tabsClicShopping">
              <div class="tab-content">
<!-- ------------------------------------------------------------ //-->
<!--           ONGLET Information General de du fabricant                  //-->
<!-- ------------------------------------------------------------ //-->
                <div class="tab-pane active" id="tab1">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_MANUFACTURER_GENERAL; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_MANUFACTURERS_NAME; ?></span>
                        <span class="col-md-2"><?php echo osc_draw_input_field('manufacturers_name', $mInfo->manufacturers_name, 'required aria-required="true" id="manufacturers_name"'); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                         <span class="col-md-2"><?php echo TEXT_MANUFACTURERS_URL; ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
<?php
    $languages = osc_get_languages();
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <div  class="col-md-12">
                        <span class="col-md-1 centerInputFields"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                        <span class="col-md-2 pull-left" style="height:50px;"><?php echo osc_draw_input_field('manufacturers_url[' . $languages[$i]['id'] . ']', osc_get_manufacturer_url($mInfo->manufacturers_id, $languages[$i]['id'])); ?></span>
                      </div>
                      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
    }
?>
                    </div>
                  </div>
                </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Information description       //-->
<!-- //################################################################################################################ -->
                <div class="tab-pane" id="tab2">
                  <div class="col-md-12 mainTitle">
                    <span><?php echo TEXT_MANUFACTURERS_DESCRIPTION; ?></span>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div>
                      <script type='text/javascript' src='ext/ckeditor/ckeditor.js'></script>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                        <span class="col-md-2"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                        <span class="col-md-10">
                          <div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('manufacturer_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300', (isset($manufacturer_description[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($manufacturer_description[$languages[$i]['id']])) : osc_get_manufacturer_description($mInfo->manufacturers_id, $languages[$i]['id']))); ?></div>
                        </span>
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
    }
?>
                    </div>
                  </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="adminformAide">
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', HELP_DESCRIPTION); ?>
                        <strong><?php echo '&nbsp;' . HELP_DESCRIPTION; ?></strong>
                      </span>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <span class="col-md-12">
                        <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                        <div class="modal fade" id="myModalWysiwyg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                              </div>
                              <div class="modal-body" style="text-align:center;">
                                <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </span>
                    </div>
                  </div>
                </div>

<!-- //################################################################################################################ -->
<!--          ONGLET Information visuelle          //-->
<!-- //################################################################################################################ -->
                <div class="tab-pane" id="tab3">
                  <div class="mainTitle">
                    <div class="pull-left"><?php echo TITLE_MANUFACTURER_IMAGE; ?></div>
                  </div>

                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                         <tr>
                           <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td class="main" valign="top" width="200"><?php  echo TEXT_MANUFACTURERS_NEW_IMAGE; ?>&nbsp;</td>
                              <td class="man" align="center" valign="top">
                                <table  width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                   <td width="20"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_PRODUCTS_IMAGE_VIGNETTE, '40', '40'); ?></td>
                                 <td class="main"><?php echo TEXT_PRODUCTS_IMAGE_VIGNETTE; ?></td>
                                  </tr>
                                </table>
                                <?php echo  osc_draw_file_field_image_ckeditor('manufacturers_image', '212', '212', '') ; ?>
                              </td>
                              <td align="center" valign="top" width="100%">
                                <table  width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product_images.gif', TEXT_PRODUCTS_IMAGE_VISUEL, '40', '40'); ?></td>
                                    <td class="main" align="left"><?php echo TEXT_PRODUCTS_IMAGE_VISUEL; ?></td>
                                  </tr>
                                </table>
                                <table  width="100%" border="0" class="adminformAide">
                                  <tr>
                                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                  </tr>
                                  <tr>
                                    <td align="center"><?php echo osc_info_image($mInfo->manufacturers_image, TEXT_PRODUCTS_IMAGE_VIGNETTE); ?></td>
                                  </tr>
                                  <tr>
                                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                  </tr>
                                  <tr>
                                    <td class="main" align="right" colspan="2"><?php echo TEXT_MANUFACTURERS_IMAGE_DELETE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                             <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                           </tr>
                         </table></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                       <tr>
                         <td><table border="0" cellpadding="2" cellspacing="2">
                           <tr>
                             <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_IMAGE); ?></td>
                             <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_IMAGE; ?></strong></td>
                           </tr>
                         </table></td>
                       </tr>
                       <tr>
                         <td><table border="0" cellpadding="2" cellspacing="2">
                           <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                           <td class="main"><?php echo HELP_IMAGE_MANUFACTURERS; ?></td>
                         </table></td>
                        </tr>
                      </table>
                    </div>

<!-- //################################################################################################################ -->
<!--          ONGLET SEO          //-->
<!-- //################################################################################################################ -->
<!-- decompte caracteres -->    
<script type="text/javascript" src="ext/javascript/jquery/charcount/charCount.js'; ?>" ></script>
<script type="text/javascript">
  $(document).ready(function(){ 
<?php
       for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>  
    //default title
    $("#default_title_<?php echo $i?>").charCount({
      allowed: 70,
      warning: 20,
      counterText: ' Max : '  
    });
    
    //default_description
    $("#default_description_<?php echo $i?>").charCount({
      allowed: 150,   
      warning: 20,
      counterText: 'Max : ' 
    });   

    //default tag
    $("#default_tag_<?php echo $i?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '  
    });

<?php
       }
?>  
  });
</script>

                    <div class="tab-pane" id="tab4">
                      <div class="col-md-12 mainTitle">
                        <div class="pull-left"><?php echo TITLE_MANUFACTURER_SEO; ?></div>
                      </div>
                      <div class="adminformTitle">
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-3"></span>
                            <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                            <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                          </div>
                        </div>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                          <div class="row">
                            <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                          </div>


                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo  TEXT_MANUFACTURER_SEO_TITLE; ?></span>
                              <span  class="col-md-6"><?php echo osc_draw_input_field('manufacturer_seo_title[' . $languages[$i]['id'] . ']', (($manufacturer_seo_title[$languages[$i]['id']]) ? $manufacturer_seo_title[$languages[$i]['id']] : osc_get_manufacturer_seo_title($mInfo->manufacturers_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?></span>
                            </div>
                          </div>
                          <div class="spaceRow"></div>
                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo  TEXT_MANUFACTURER_SEO_DESCRIPTION; ?></span>
                              <span  class="col-md-6"><?php echo osc_draw_textarea_field('manufacturer_seo_description[' . $languages[$i]['id'] . ']', 'soft', '75', '2', (isset($manufacturer_seo_description[$languages[$i]['id']]) ? $manufacturer_seo_description[$languages[$i]['id']] : osc_get_manufacturer_seo_description($mInfo->manufacturers_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></span>
                            </div>
                          </div>
                          <div class="spaceRow"></div>
                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo TEXT_MANUFACTURER_SEO_KEYWORDS; ?></span>
                              <span  class="col-md-6"><?php echo osc_draw_textarea_field('manufacturer_seo_keyword[' . $languages[$i]['id'] . ']', 'soft', '75', '5', (isset($manufacturer_seo_keyword[$languages[$i]['id']]) ? $manufacturer_seo_keyword[$languages[$i]['id']] : osc_get_manufacturer_seo_keyword($mInfo->manufacturers_id, $languages[$i]['id']))); ?></span>
                            </div>
                          </div>
<?php
    }
?>
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                        <div class="adminformAide">
                          <div class="row">
                              <span class="col-md-12">
                                <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                                <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                              </span>
                          </div>
                          <div class="spaceRow"></div>
                          <div class="row">
                            <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                 </div>
              </div>
           </div>

       </td>
    </tr>
  </form>
<?php
  } else {
?>
<!-- //################################################################################################################ -->
<!-- //                                             LISTING DES MODELES                                        -->
<!-- //################################################################################################################ -->
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MANUFACTURERS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    while ($manufacturers = osc_db_fetch_array($manufacturers_query)) {
      if ((!isset($_GET['mID']) || (isset($_GET['mID']) && ($_GET['mID'] == $manufacturers['manufacturers_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {

        $QmanufacturerProducts = $OSCOM_PDO->prepare("select count(*) as products_count
                                                      from products
                                                      where manufacturers_id = :manufacturers_id
                                                      ");
        $QmanufacturerProducts->bindInt(':manufacturers_id', (int)$manufacturers['manufacturers_id']);
        $QmanufacturerProducts->execute();

        $manufacturer_products = $QmanufacturerProducts->fetch();

        $mInfo_array = array_merge($manufacturers, $manufacturer_products);
        $mInfo = new objectInfo($mInfo_array);
      }

      if (isset($mInfo) && is_object($mInfo) && ($manufacturers['manufacturers_id'] == $mInfo->manufacturers_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                <td>
<?php 
      if ($manufacturers['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $manufacturers['manufacturers_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $manufacturers['manufacturers_id']; ?>" />
<?php 
      } 
?>
                </td>
                <td class="dataTableContent"><?php echo $manufacturers['manufacturers_name']; ?></td>
                <td  class="dataTableContent" align="center">
<?php
      if ($manufacturers['manufacturers_status'] == '0') {
        echo '<a href="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&action=setflag&flag=1&id=' . $manufacturers['manufacturers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&action=setflag&flag=0&id=' . $manufacturers['manufacturers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&mID=' . $manufacturers['manufacturers_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($mInfo) && is_object($mInfo) && ($manufacturers['manufacturers_id'] == $mInfo->manufacturers_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . osc_href_link('manufacturers.php', 'page=' . $_GET['page'] . '&mID=' . $manufacturers['manufacturers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    } //end while
?>
              </form><!-- end form delete all -->
              <tr>
                <td colspan="12" class="smallText" valign="top"><?php echo $manufacturers_split->display_count($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS); ?></td>
              </tr>
            </table></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
