<?php
  /**
   * d_orders.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class d_orders {
    var $code = 'd_orders';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_orders() {
      $this->title = MODULE_ADMIN_DASHBOARD_ORDERS_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_ORDERS_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_ORDERS_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_ORDERS_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_ORDERS_STATUS == 'True');
      }
    }

    function getOutput() {
      global $OSCOM_Date, $OSCOM_PDO;

      $Qorder = $OSCOM_PDO->prepare('select o.orders_id,
                                               o.customers_group_id,
                                               o.customers_id,
                                               o.customers_name,
                                               greatest(o.date_purchased,
                                               ifnull(o.last_modified, 0)) as date_last_modified,
                                               s.orders_status_name,
                                               o.odoo_invoice,
                                               ot.text as order_total
                                        from :table_orders o,
                                             :table_orders_total ot,
                                             :table_orders_status s
                                        where o.orders_id = ot.orders_id
                                        and ot.class = :class
                                        and o.orders_status = s.orders_status_id
                                        and(o.orders_status <> 3 and o.orders_status <> 4)
                                        and  s.language_id = :language_id
                                        order by date_last_modified desc
                                        limit :limit
                                       ');
      $Qorder->bindValue(':class', 'ot_total');
      $Qorder->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qorder->bindInt(':limit', MODULE_ADMIN_DASHBOARD_ORDERS_LIMIT );
      $Qorder->execute();


      $output = '<div class="clearfix"></div>';
      $output .=  osc_draw_separator('pixel_trans.gif', '1', '10');
      $output .= '<table class="table table-hover table-responsive" style="padding-top:0px; padding-left:0px;">';
      $output .= '<thead>';
      $output .= '<tr class="dataTableHeadingRow">';
      $output .= '<th class="dataTableHeadingContent">' . MODULE_ADMIN_DASHBOARD_ORDERS_DATE . '</th>';
      $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . MODULE_ADMIN_DASHBOARD_ORDERS_TITLE . '</th>';
      $output .=' <th class="dataTableHeadingContent" style="text-align:center;">' . MODULE_ADMIN_DASHBOARD_ORDERS_TOTAL . '</th>';
      $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . MODULE_ADMIN_DASHBOARD_ORDERS_ODOO_STATUS . '</th>';
      $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . MODULE_ADMIN_DASHBOARD_ORDERS_ORDER_STATUS . '</th>';
      $output .= '<th class="dataTableHeadingContent" style="text-align:center;">' . MODULE_ADMIN_DASHBOARD_ORDERS_ORDER_ACTION . '</th>';
      $output .= '</tr>';
      $output .= '</thead>';

      while ($orders = $Qorder->fetch() ) {
        $output .= '<tbody>';
        $output .= '  <tr class="dataTableRow" align="right">' .
         '    <td class="dataTableContent"  align="left">' . $OSCOM_Date->getShortTime($orders['date_last_modified']) . '</td>' .
         '    <td class="dataTableContent"  align="left"><a href="' . osc_href_link(('customers.php') . '?cID=' . (int)$orders['customers_id']) . '&action=edit">' . osc_output_string_protected($orders['customers_name']) . '</a></td>' .
         '    <td class="dataTableContent">' . strip_tags($orders['order_total']) . '</td>';

        if ($orders['odoo_invoice'] == '1') {
          $output .=          ' <td class="dataTableContent" style="text-align:center;">' . osc_image(DIR_WS_ICONS . 'odoo_order.png', IMAGE_ORDERS_ODOO) . '</td>';
        } elseif ($orders['odoo_invoice'] == '2') {
          $output .=          ' <td class="dataTableContent" style="text-align:center;">' . osc_image(DIR_WS_ICONS . 'odoo_invoice.png', IMAGE_ORDERS_INVOICE_MANUAL_ODOO) . '</td>';
        } else {
          $output .=          ' <td class="dataTableContent" style="text-align:center;"></td>';
        }
        $output .=
         '    <td class="dataTableContent">' . osc_output_string_protected($orders['orders_status_name']) . '</td>' .
         '    <td class="dataTableContent">
         <a href="' . osc_href_link('orders.php', 'oID=' . (int)$orders['orders_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_ORDER) . '</a>' .
         osc_draw_separator('pixel_trans.gif', '3', '3') . '
         <a href="' . osc_href_link(('customers.php') . '?cID=' . (int)$orders['customers_id']) . '&action=edit">' . osc_image(DIR_WS_ICONS . 'client_b2b.gif', ICON_EDIT_CUSTOMER) . '</a>' .
         osc_draw_separator('pixel_trans.gif', '3', '3') . '
         <a href="' . osc_href_link('orders.php', 'cID=' . $orders['customers_id']) . '">' . osc_image(DIR_WS_ICONS . 'order.gif', ICON_VIEW_CUSTOMERS_ALL_ORDERS) . '</a>' .
         osc_draw_separator('pixel_trans.gif', '3', '3');
        $output .= '</td>';
        $output .= '</tr>';
        $output .= osc_draw_separator('pixel_trans.gif', '1', '10');
        $output .= '</td>';
        $output .= '</tr> ';
        $output .= '</tbody>';
      }

      $output .= '</table>';
      $output .= '<div class="clearfix"></div>';

      return $output;
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_ORDERS_STATUS');
    }

    function install() {
     global $OSCOM_PDO;
     
      if ($_SESSION['languages_id'] =='1') {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_ORDERS_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Combien de commande souhaitez-vous afficher ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_ORDERS_LIMIT',
            'configuration_value' => '10',
            'configuration_description' => 'Veuillez indiquer le nombre de commande &agrave; afficher',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_ORDERS_SORT_ORDER',
            'configuration_value' => '45',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montré en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '60',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );
      } else {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Do you want display the latest orders', 'MODULE_ADMIN_DASHBOARD_ORDERS_STATUS', 'True', 'Do you want to show the latest orders on the dashboard ?', '6', '1', 'osc_cfg_select_option(array(\'True\', \'False\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('How much order do you want to display ?', 'MODULE_ADMIN_DASHBOARD_ORDERS_LIMIT', '10', 'Please specify the number of orders to display.', '6', '2', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_ADMIN_DASHBOARD_ORDERS_SORT_ORDER', '60', 'Sort order of display. Lowest is displayed first.', '6', '3', now())");
      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_ORDERS_STATUS',
                   'MODULE_ADMIN_DASHBOARD_ORDERS_LIMIT',
                   'MODULE_ADMIN_DASHBOARD_ORDERS_SORT_ORDER'
                  );
    }
  }
