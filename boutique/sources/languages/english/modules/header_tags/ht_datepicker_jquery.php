<?php
/**
 * ht_date_picker_jquery
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_TITLE', 'Do you want  use the internationals dates ?');
  define('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_DESCRIPTION', 'Add a script to display the internationals dates');
?>
