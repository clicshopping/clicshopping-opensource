<?php
/**
 * import.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Importation de base de données universelle');
define('HEADING_TITLE_STEP1', 'Choix du type d\'installation');
define('HEADING_TITLE_STEP2', 'Choix du Template');
define('HEADING_TITLE_STEP3', 'Téléchargement du module');
define('HEADING_TITLE_STEP4', 'Instructions complémentaires');
define('HEADING_TITLE', 'Informations complémentaires');

define('DATABASE_COLUMNS','Champs de la base de données');
define('DATABASE_TYPE','Type de champs');
define('DATABASE_DEFAULT', 'Valeurs par défaut');
define('IMPORT_FIELD_FILES','Champs du fichier importé');

define('TEXT_COLUMN_NUMBER','Nbr de colonnes pouvant etre traitées : ');
define('TEXT_LINE_IMPORT','<br />Nbr de lignes du fichier à importer : ');
define('TEXT_SELECTED','-- Sélectionner --');
define('TEXT_CATEGORIES', 'Importer des catégories');
define('TEXT_CATEGORIES_DESCRIPTION','Importer la description d\'une catégorie');
define('TEXT_PRODUCTS','Importer des produits');
define('TEXT_PRODUCTS_DESCRIPTION','Importer la description des produits');
define('TEXT_PRODUCT_IN_CATEGORIES', 'Importer des produits dans une catégorie spécifique');
define('TEXT_MANUFACTURERS','Importer des marques ');
define('TEXT_MANUFACTURERS_INFO','Importer les informations sur mes marques');
define('TEXT_SUPPLIERS','Importer des fournisseurs');
define('TEXT_SUPPLIERS_INFO','Importer les informations sur les forunisseurs');

define('TEXT_CHOOSE_FILE', 'Choisissez le fichier à télécharger *');
define('TEXT_FIELDS_DELIMITED_BY','Pour les fichiers CSV, le champs délimités par : *');
define('TEXT_ENTER_DELIMITED_BY', 'Pour les fichiers CSV, l\'entrées délimitée par :');
define('TEXT_TEST_IMPORT','Souhaitez-vous effectuer un test avant l\'importation définitive ? (très recommandé) * ');
define('TEXT_REFERENCE_ID','Veuillez indiquer le champs de référence id *');
define('TEXT_EXAMPLE','Exemple de données insérées dans la base de données');

define('TEXT_VIRTUAL_CATEGORIES_IMPORT','Import virtuel (non vu dans le catalogue)');

define('TEXT_IMPORT_NO_FILE','Aucun Fichier');
define('TEXT_IMPORT_FILE_EMPTY','le fichier vide : il ne contient pas de donné agrave; priori.');
define('TEXT_IMPORT_TEST_DATA','Import des 6 premières ligne du fichier');
define('TEXT_IMPORT_SUCCESS','L\'importation est terminée');
define('TEXT_IMPORT_SELECT_TYPE','Veuillez sélectionner le type d\'importation *');
define('TEXT_IMPORT_OFF','Souhaitez-vous mettre hors ligne cette importation ? *');

define('TEXT_YES','Oui');
define('TEXT_NO','Non');

define('TITLE_HELP_GENERAL','Informations complémentaires');
define('HELP_GENERAL','<li>Veuillez bien lire cette section avant de commencer la procédure d\'importation.</li>
<blockquote>
<li><strong>Une procédure d\'importation comporte des risques et peut gravement endommager votre base de données.</strong> Vous assumez par conséquent le résultat de cette importation et les impacts dans la base de données.</li>
<li>En fonction des options que vous allez choisir, les produits peuvent etre importés dans une catégorie virtuelle spécialement créée lors de l\'importation. Si vous procédez à une autre importation, une nouvelle catégorie virtuelle sera alors créée.<br>
la catégorie virtuelle n\'est pas visible dans le catalogue, vous devez retraiter vos produits et les déplacer dans les catégories adéquates</li>
</li>
<li>Veuillez faire attention a ce que votre fichier ne soit pas trop important, les d&eacutelais de traitement peuvent etre long et prendre beaucoup de ressources et pourraient causer un crash si le délai de traitement dépassent les 60 sec.</li>
<li>Vos fichiers .csv doivent avoir une entete et vous devez indiquer dans le formulaire un id de cette entete.</li>
<li>Veuillez faire attention quand vous importez des fichiers utilisant le multilangue. Veuillez vérifier que le id langue de votre csv correspond bien à l\'id langue de ClicShopping. Veuillez noter que 2 langues sont enregistrées par défaut dans ClicShopping</li>
<li>N\'oubliez pas d\'utiliser habillement la mise à jour rapide pour vous faire gagner encore du temps !</li>
');

// step1
define('WELCOME_HEADING_TITLE','<br /><br /><br /><br />
        <ul>Procédure d\'installation</ul>
        <blockquote>
         <li>Etape 1 - Sélectionner des critères d\'importation et le fichier à importer</li>
         <li>Etape 2 - Importation et mapping des données</li>
         <li>Etape 3 - test d\'importation ou enregistrement des données</li>
        </blockquote>
           <br /><br />
');

// step1
define('WELCOME_MESSAGE_STEP1','
<ul>Procédure de mapping</ul>
        <blockquote>
        <li>Etape 2 - Importation et mapping des données</li>
         <li>Etape 3 - test d\'importation et validation définitive</li>

        </blockquote>
           <br /><br />
          <p align="center">Veuillez choisir parmi les options suivantes</p>
');
?>