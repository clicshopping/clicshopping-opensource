<?php
/**
 * install_directory.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Le répertoire d\'installation n\'a pas été supprimée. il se trouve : ' . DIR_FS_CATALOG . 'install. Veuillez supprimer ce répertoire install pour des raisons de sécurité.');
?>
