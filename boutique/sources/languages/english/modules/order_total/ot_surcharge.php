<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_PAYMENT_SURCHAGE_TEXT_TITLE', 'Flat');
  define('MODULE_PAYMENT_SURCHAGE_DESCRIPTION', 'Order Flat<br /><br /><font color="#FF0000"> <strong> Warning! </strong> Disabling this module will restore the default values</font');
?>