
--
-- Mise a jour version / Update
--

-- Update information user Odoo Web Service
-- update twitter class
-- bug on checkout_address
-- cosmetic
-- update quick update and include odoo update
====================================================================================================================================================

update `configuration` SET `configuration_value` = 'ClicShopping est une marque d&eacute;pos&eacute;e (INPI : 3810384) V2.46 -  All right Reserved - ' where configuration_key ='PROJECT_VERSION' LIMIT 1 ;


===================================================================================================================================================
French
===================================================================================================================================================


UPDATE `configuration` SET `configuration_description` = 'Activation permettant d''utiliser le webservice Odoo<br /><br/><Strong>Note :</strong> - Odoo a un s&egrave;v&egrave;re bug au niveau de sa relation entre les adresses de facturation et les adresses de facture. Veuillez ne pas activer l\'option de permettre le changement de facturation par d&eacute;faut (ci-dessous).' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';

UPDATE `configuration` SET `configuration_title` = 'Veuillez indiquer le num&eacutero comptable de compte de vente de marchandises de Odoo' WHERE `configuration`.`configuration_key` = 'ODOO_WEB_SERVICE_ACCOUNT_SELL';
UPDATE `configuration` SET `configuration_title` = 'Veuillez indiquer le num&eacutero comptable de compte client d''achat de marchandises de Odoo' WHERE `configuration`.`configuration_key` = 'ODOO_WEB_SERVICE_ACCOUNT_PURCHASE';
UPDATE `configuration` SET `configuration_title` = 'Veuillez indiquer le num&eacutero comptable des frais d''exp&eacute;dition de marchandise de Odoo' WHERE `configuration`.`configuration_id` = 'ODOO_WEB_SERVICE_ACCOUNT_SHIPPING';

INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Souhaitez-vous permettre le changement de facturation par d&eacute;faut dans la boutique ?', 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG', 'false', 'Cela permet au client de changer son adresse de facturation par d&eacute;faut.<br /><br /><strong>Note :</strong><br /><br />- Nous vous recommandons fortement de laisser cette option sur <strong>false</strong> quand vous utilisez le service web Odoo. Accepter les changements d\'adresse, aura une <u>r&eacute;percution importante dans toute la gestion de vos facture dans Odoo</u> (bug Odoo)<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', '44', '10', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now());

UPDATE `configuration` SET `configuration_description` = 'Souhaitez-vous utiliser le webservice Odoo<br /><br/><Strong>Note :</strong> - Odoo a un s&grave;v&egrave;re bug au niveau de sa relation entre les adresses de facturation et les adresses de facture. Pour de plus amples informations, veuillez voir le forum.<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';
UPDATE `configuration` SET `configuration_description` = 'Enregistre toutes les commandes effectu&eacute;es via le catalogue vers l\'administration vers Odoo<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_ORDER_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'Une information vous sera envoy&eacute;e en cas de d&eacute;tection d\'une erreur.<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_key` = 'ODOO_EMAIL_WEB_SERVICE';
UPDATE `configuration` SET `configuration_description` = 'Enregistre tous les clients cr&eacute;ant un compte ou modifiant son compte<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_CUSTOMERS_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'Enregistre toutes les modifications ou cr&eacute;ations de client dans l\'administration vers Odoo<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_id` = 'ODOO_ACTIVATE_CUSTOMERS_ADMIN';
UPDATE `configuration` SET `configuration_description` = 'Enregistre toutes les modifications ou cr&eacute;ations de produit dans l\'administration vers Odoo<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_id` = 'ODOO_ACTIVATE_PRODUCTS_ADMIN';
UPDATE `configuration` SET `configuration_description` = 'Enregistre la commande de l\'administration vers Odoo<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>' WHERE `configuration`.`configuration_id` = 'ODOO_WEB_SERVICE_ORDERS_ADMIN';






===================================================================================================================================================
English
===================================================================================================================================================


UPDATE `configuration` SET `configuration_description` = 'Activation allowing to use the Odoo webservice<br /><br/><Strong>Note :</strong> - Odoo has an important bug concerning the relation between payment address and invoice address. This option allow to customer to change this default billing address. Please, do not activate the default billing address (below).' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Do you want allow  the default change of billing in the store ?', 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG', 'false', 'This allows the customer to change his default billing address.<br /><br /><strong>Note :</strong><br /><br />- e strongly recommend to keep this feature option on <strong>false</strong> when you use the Odoo web service. Accept address changes, will an <u>important repercussion in the whole management of your invoice Odoo</u> (bug Odoo)<br />.', '44', '10', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now());
