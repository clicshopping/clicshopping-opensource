<?php
/*
 * google_sitemap_products.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $product_array = array();

    $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                     coalesce(NULLIF(products_last_modified, :products_last_modified),
                                                     products_date_added) as last_modified
                                      from :table_products
                                      where products_status = :products_status
                                      and products_view = :products_view
                                      order by last_modified DESC
                                      ');

    $Qproducts->bindValue(':products_last_modified', '');
    $Qproducts->bindValue(':products_status', '1');
    $Qproducts->bindValue(':products_view', '1');
    $Qproducts->execute();


    while ($products = $Qproducts->fetch() ) {
      $product_array[$products['products_id']]['loc'] = osc_href_link('product_info.php', 'products_id=' . (int)$products['products_id'], 'NONSSL', false);
      $product_array[$products['products_id']]['lastmod'] = $products['last_modified'];
      $product_array[$products['products_id']]['changefreq'] = 'weekly';
      $product_array[$products['products_id']]['priority'] = '0.5';
    }

    foreach ($product_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
