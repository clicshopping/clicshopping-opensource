<?php
/**
 * manufacturers_popup.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Marques');

define('TITLE_MANUFACTURER_GENERAL','Informations générale');
define('TEXT_MANUFACTURERS_DESCRIPTION','Description');
define('TITLE_MANUFACTURER_IMAGE','Image');

define('TEXT_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');

define('TAB_DESCRIPTION','Description');
define('TAB_VISUEL','Image');
define('TAB_SEO','Métadonnées');

define('TEXT_MANUFACTURERS_NAME', 'Nom de la marque :');
define('TEXT_MANUFACTURERS_NEW_IMAGE', 'Nouvelle image de la marque :');
define('TEXT_MANUFACTURERS_IMAGE', 'Image actuelle de la marque :');
define('TEXT_MANUFACTURERS_DELETE_IMAGE', 'Ne plus afficher d\'image de la marque');
define('TEXT_MANUFACTURERS_URL', 'URL de la marque :');

define('TITLE_MANUFACTURER_SEO','Métadonnées pour le marques du produit');
define('TEXT_MANUFACTURER_SEO_TITLE','Titre');
define('TEXT_MANUFACTURER_SEO_DESCRIPTION','Description');
define('TEXT_MANUFACTURER_SEO_KEYWORDS','Mots clés');

define('TEXT_DELETE_IMAGE', 'Suppprimer l\'image de la marque ?');
?>