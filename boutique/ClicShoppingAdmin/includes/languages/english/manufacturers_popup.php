<?php
/**
 * manufacturers_popup.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Brands');

define('TITLE_MANUFACTURER_GENERAL','General informations');
define('TEXT_MANUFACTURERS_DESCRIPTION','Description');
define('TITLE_MANUFACTURER_IMAGE','Image');

define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');
define('EXT_PRODUCTS_IMAGE_VIGNETTE','Image');

define('TAB_DESCRIPTION','Description');
define('TAB_VISUEL','Image');
define('TAB_SEO','Metadatas');

define('TEXT_MANUFACTURERS_NAME', 'Brands name:');
define('TEXT_MANUFACTURERS_NEW_IMAGE', 'New brands Image:');
define('TEXT_MANUFACTURERS_IMAGE', 'Brands Image:');
define('TEXT_MANUFACTURERS_DELETE_IMAGE', 'Not display image brand');
define('TEXT_MANUFACTURERS_URL', 'Brands URL:');

define('TITLE_MANUFACTURER_SEO','Metadatas for the brand');
define('TEXT_MANUFACTURER_SEO_TITLE','Title');
define('TEXT_MANUFACTURER_SEO_DESCRIPTION','Description');
define('TEXT_MANUFACTURER_SEO_KEYWORDS','keywords');

define('TEXT_DELETE_IMAGE', 'Delete brand image?');
?>