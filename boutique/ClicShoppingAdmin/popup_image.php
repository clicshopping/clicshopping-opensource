<?php
/*
 * popup_image.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  reset($_GET);
  while (list($key, ) = each($_GET)) {
    switch ($key) {
      case 'banner':
        $banners_id = osc_db_prepare_input($_GET['banner']);

        $Qbanner = $OSCOM_PDO->prepare("select banners_title, 
                                                     banners_image, 
                                                     banners_html_text 
                                       from :table_banners
                                       where banners_id = :banners_id
                                      ");
        $Qbanner->bindInt(':banners_id', (int)$banners_id );
        $Qbanner->execute();

        $banner = $Qbanner->fetch();


        $page_title = $banner['banners_title'];

        if ($banner['banners_html_text']) {
          $image_source = $banner['banners_html_text'];
        } elseif ($banner['banners_image']) {
          $image_source = osc_image(HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES  . $banner['banners_image'], $page_title);
        }
        break;
      case 'products_image_zoom':
        $products_id = osc_db_prepare_input($_GET['products_image_zoom']);

        $Qproductsr = $OSCOM_PDO->prepare("select products_image_zoom
                                           from :table_products
                                           where products_id = :products_id
                                          ");
        $Qproductsr->bindInt(':products_id', (int)$products_id );
        $Qproductsr->execute();

        $products = $Qproductsr->fetch();

        $image_source = osc_info_image($products['products_image_zoom'], '');
        break;
      case 'product_preview_image_zoom':
        $image_source = osc_info_image($_GET['product_preview_image_zoom'], '');
        break;
    }
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<title><?php echo $page_title; ?></title>
<script type="text/javascript"><!--
var i=0;

function resize() {
  if (window.navigator.userAgent.indexOf('MSIE 6.0') != -1 && window.navigator.userAgent.indexOf('SV1') != -1) { 
    i=23; //IE 6.x on Windows XP SP2
  } else if (window.navigator.userAgent.indexOf('MSIE 6.0') != -1) { 
    i=50; //IE 6.x somewhere else
  } else if (window.navigator.userAgent.indexOf('MSIE 7.0') != -1) { 
    i=0;  //IE 7.x 
  } else if (window.navigator.userAgent.indexOf('Firefox') != -1 && window.navigator.userAgent.indexOf("Windows") != -1) { 
    i=38; //Firefox on Windows
  } else if (window.navigator.userAgent.indexOf('Mozilla') != -1 && window.navigator.userAgent.indexOf("Windows") != -1 && window.navigator.userAgent.indexOf("MSIE") == -1) { 
    i=45; //Mozilla on Windows, but not IE7		
  } else if (window.opera && document.childNodes) {
    i=50; //Opera 7+
  } else if (navigator.vendor == 'KDE' && window.navigator.userAgent.indexOf("Konqueror") != -1) {
    i=-4; //Konqueror- this works ok with small images but not so great with large ones
  } else { //if you tweak it make sure i remains negative
    i=70; //All other browsers
  }
          
  if (document.images[0]) {
    imgHeight = document.images[0].height+150-i;
    imgWidth = document.images[0].width+30;
    var height = screen.height;
    var width = screen.width;
    var leftpos = width / 2 - imgWidth / 2;
    var toppos = height / 2 - imgHeight / 2;
    window.moveTo(leftpos, toppos);
    window.resizeTo(imgWidth, imgHeight);
  }

  self.focus();
}
//--></script>
</head>
<body onload="resize();">
<?php echo $image_source; ?>
</body>
</html>
<?php
  require('includes/application_bottom.php');
