<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Address Book');

define('HEADING_TITLE', 'My Personal Address Book');

define('HEADING_DESCRIPTION', 'Create antoher address : your main residence, your word address, etc...');

define('PRIMARY_ADDRESS_TITLE', 'Primary Address');
define('PRIMARY_ADDRESS_DESCRIPTION', 'This address is used as the pre-selected shipping and billing address for orders placed on this ' . STORE_NAME . '<br /><br />This address is also used as the base for product and service tax calculations.');

define('ADDRESS_BOOK_TITLE', 'Address Book Entries');

define('PRIMARY_ADDRESS', '(primary address)');

define('TEXT_MAXIMUM_ENTRIES', '<p style="color="#ff0000;"><strong>NOTE:</strong></font> A maximum of %s address book entries allowed.</p>');
?>
