<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'My Action');
define('TEXT_SHARE', 'Share with my network');
define('TEXT_EVALUATE_CUSTOMERS', 'Average customer evaluation');
define('TEXT_INSERT_EVALUATION', 'This product is not evaluated.');
define('TEXT_INSERT_NEWSLETTER', 'Newsletter subscribe');
define('TEXT_SEND_A_FRIEND', 'Send this product at a friend');
define('TEXT_NOTIFICATIONS_NOTIFY', 'Send me a mail if the price change');
?>