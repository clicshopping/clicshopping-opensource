<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Log Off');
define('NAVBAR_TITLE', 'Log Off');
define('TEXT_MAIN', 'You have been logged off your account. It is now safe to leave the computer.<br /><br />Your shopping cart has been saved, the items inside it will be restored whenever you log back into your account.');
?>
