<?php
/**
 * newlsetter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: newlsetter_no_account.php
*/

define('NAVBAR_TITLE', 'Newsletter subscription');

define('HEADING_TITLE', 'Newsletter subscription');

define('TEXT_ORIGIN_LOGIN', 'Subscribe to the newsletter '. STORE_NAME. ' is very simple! <br /> All information you provide will clearly remain strictly private and reserved. At no time will your information disclosed to other parties or for any actions or statements other than trading of '. STORE_NAME. ' and you have authorized. All this information is used to '. STORE_NAME. ' for better follow you and serve you. <br /> You may at any time to update or delete information you have given on request by email. You keep well at all times have full control over the information you have provided.<br /><br /> '. STORE_NAME. ' respects the law computer and Freedoms charters available.');


define('EMAIL_SUBJECT', 'Newsletter subscription at ' . STORE_NAME);

define('EMAIL_GREET_MR', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_WELCOME', '<div align="justify">Thank you for your confidence by the subscription  at our newsletter on the site '. HTTP_COOKIE_DOMAIN. ' ". \n\n". "Pursuant to your request to '. STORE_NAME.', we confirm its validation. '. "\ n \ n". 'For any assistance on our services, please contact our customer service: ' . STORE_OWNER_EMAIL_ADDRESS);
define('EMAIL_WARNING',  ''. "\n\n" . ' Regards,'. "\n" . '<strong>'.STORE_NAME.' Team</strong>'. "\n" . '<p>
<U><font size="2">Confidentiality Note :</font></U><font size="2">'. "\n" . '
This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '
<p><font size="2">In accordance with the <strong>Law</strong> on "Data-processing Law and Freedom", you are entitled to correction of your personal data or on request by email</font>
</div>');

define('TEXT_PRIVACY_CONDITIONS_DESCRIPTION', 'The privacy statement are available always on '.STORE_NAME.' and can be read <a href="' . SHOP_CODE_URL_CONFIDENTIALITY . '" target="_blank"><u>here</u></a>.');

define('TEXT_SUCCESS ','Congratulations! You will soon receive all the '. STORE_NAME.' informations. Do not hesitate to create an account on '. STORE_NAME. ' for receive more information about our news, our exceptional promotions to our loyal customers. <br /> For all questions relating to the  site and  order, you can either consult the customer informationq on the site or contact our sales department via the form at your disposal  : <a href="' . osc_href_link('contact_us.php') . '">Contact us</a>.<br /><br />Regards, '.STORE_NAME.' Customer services');

define('ENTRY_NUMBER_EMAIL_CONFIRMATION','How many : ');

define('ERROR_ACTION_RECORDER', 'Error: An enquiry has already been sent. Please try again in %s minutes.');
?>
