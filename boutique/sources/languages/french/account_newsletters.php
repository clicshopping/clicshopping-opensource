<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Bulletin d\'information');

define('HEADING_TITLE', 'Bulletin d\'information');

define('MY_NEWSLETTERS_TITLE', 'Mon abonnement');
define('MY_NEWSLETTERS_GENERAL_NEWSLETTER', 'Bulletin d\'informations général');
define('MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION', 'Ceci inclut : les nouveautés, les promotions et toutes les annonces.');

define('MY_NEWSLETTERS_RSS','Abonnement Flux RSS');
define('MY_NEWSLETTERS_RSS_DESCRIPTION','En plus de l\'abonnement à la newsletter, nous vous proposons de suivre l\'actualité des ventes en temps réel Grâce à la technologie RSS. Il suffit de cliquer sur le lien ci-dessous :<br /><p align="center">
<img src ="' . HTTP_SERVER . DIR_WS_HTTP_CATALOG . DIR_WS_ICONS . 'icon_feed.gif"> <a href=' . HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'rss.php' .'>' . HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'rss.php' .'</a></p>');
define('MY_NEWSLETTERS_RSS_EXPLANATION','<strong>Note : </strong><br />Ce système est utilisé pour diffuser les mises à jour de sites dont le contenu change fréquemment. <br />
Grâce à ce format, vous n\'avez plus besoin de vous rendre sur le site pour être informé des nouveautés.<br />
Il suffit d\'insérer, en un seul clic, le fil d\'informations RSS dans un logiciel compatible pour avoir toutes vos sources d\'informations sur une seule page : vos emails, la météo et le trafic de votre ville, les blogs que vous suivez, les derniers articles des sites de sports ou d&rsquo;infos, les ventes en cours et à venir de '.STORE_NAME.' etc<br />
');

define('SUCCESS_NEWSLETTER_UPDATED', 'Votre abonnement à été mise à jour.');
define('TABLE_HEADING_NEW_PRODUCTS','Nouveautés et Collections');
define('MIN_QTY_ORDER_PRODUCT','Qté min : ');
?>