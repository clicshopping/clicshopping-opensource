<?php
/**
 * suppliers_popup.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  $supplier_inputs_string = '';
  $languages = osc_get_languages();
?>
<form name="ajaxform" id="ajaxform" <?php echo 'action="' . osc_href_link('suppliers_popup_ajax.php') . '"'; ?> method="post">

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
             <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/suppliers.gif', HEADING_TITLE, '40', '40'); ?></td>
             <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
             <td align="right" class="pageHeading"><table border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td>
                   <div id="simple-msg" style="float:left;"></div>
                   <div style="float:right;">&nbsp;<?php echo osc_image_submit('button_insert_specials.gif', IMAGE_INSERT, 'id="simple-post"'); ?></div>
                 </td>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
               </tr>
             </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"id="myTab">
              <li class="active"><a href="#tab31" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
              <li><a href="#tab32" role="tab" data-toggle="tab"><?php echo TAB_SUPPLIERS_NOTE; ?></a></li>
              <li><a href="#tab33" role="tab" data-toggle="tab"><?php echo TAB_VISUEL; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
<?php
// -- ------------------------------------------------------------ //
// --          ONGLET Information General du fournisseur          //
// -- ------------------------------------------------------------ //
?>
                <div class="tab-pane active" id="tab31">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_SUPPLIERS_GENERAL; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_NAME; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_input_field('suppliers_name',  $mInfo->suppliers_name, 'required aria-required="true" id="supliers_name"'); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_MANAGER; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_manager', $mInfo->suppliers_manager); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_PHONE; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_phone', $mInfo->suppliers_phone); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_FAX; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_fax', $mInfo->suppliers_fax); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_EMAIL_ADDRESS; ?></span>
                        <span class="col-md-4"><?php osc_draw_input_field('suppliers_email_address', $mInfo->suppliers_email_address); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_ADDRESS; ?></span>
                        <span class="col-md-4"><?php osc_draw_input_field('suppliers_address', $mInfo->suppliers_address); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_SUBURB; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_suburb', $mInfo->suppliers_suburb); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_POSTCODE; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_input_field('suppliers_postcode', $mInfo->suppliers_postcode); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_CITY; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_city', $mInfo->suppliers_city); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2"><?php echo TEXT_SUPPLIERS_COUNTRY; ?></span>
                        <span class="col-md-4">
<?php
if ($error == true) {
  if ($entry_country_error == true) {
    echo osc_draw_pull_down_menu('suppliers_country_id', osc_get_countries(), $mInfo->suppliers_country_id) . '&nbsp;' . ENTRY_COUNTRY_ERROR;
  } else {
    echo osc_get_country_name($mInfo->suppliers_country_id) . osc_draw_hidden_field('suppliers_country_id');
  }
} else {
  echo osc_draw_pull_down_menu('suppliers_country_id', osc_get_countries(), $mInfo->suppliers_country_id);
}
?>
                        </span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_STATES; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_states', $mInfo->suppliers_states); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_URL; ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                        <div class="col-md-12">
                          <span class="col-md-1"></span>
                          <span class="col-md-1 centerInputFields"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                          <span class="col-md-3 pull-left" style="height:50px;"><?php echo osc_draw_input_field('suppliers_url[' . $languages[$i]['id'] . ']', osc_get_supplier_url($mInfo->suppliers_id, $languages[$i]['id'])) ?></span>
                        </div>
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
    }
?>
                    </div>
                  </div>
                </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information note complementaire          //-->
<!-- ------------------------------------------------------------ //-->
                <div class="tab-pane" id="tab32">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_SUPPLIERS_GENERAL; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2"><?php echo TEXT_SUPPLIERS_NOTES; ?></span>
                        <span class="col-md-6"><?php echo  osc_draw_textarea_field('suppliers_notes', false, 70,10, $mInfo->suppliers_notes); ?></span>
                      </div>
                    </div>
                  </div>
                </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information visuelle          //-->
<!-- ------------------------------------------------------------ //-->
                <div class="tab-pane" id="tab33">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_SUPPLIERS_IMAGE; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-3"><?php echo  TEXT_SUPPLIERS_NEW_IMAGE; ?></span>
                            <span  class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_PRODUCTS_IMAGE_VIGNETTE, '40', '40'); ?></span>
                            <span  class="col-md-4"><?php echo TEXT_PRODUCTS_IMAGE_VIGNETTE . '<br /><br />' . osc_draw_file_field_image_ckeditor('suppliers_image', '212', '212','') ; ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
    </table></td>
  </tr>
</table>
</form>

  <script type="text/javascript" src="ext/javascript/bootstrap/js/bootstrap_ajax_form.js" /></script>
<?php
 require('includes/application_bottom.php');
