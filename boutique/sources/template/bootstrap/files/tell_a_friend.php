<?php
/*
 * tell_a_friend.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

 require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('friend') ) {
    echo $OSCOM_MessageStack->get('friend');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
  echo osc_draw_form('email_friend', osc_href_link('tell_a_friend.php', 'action=process&products_id=' . (int)$OSCOM_Products->getId(), $request_type ), 'post', '', true);

// ----------------------
// --- Customer details   -----  
// ---------------------- 
?>

  <div class="contentContainer">
    <div>
      <span class="inputRequirement text-right pull-right"><?php echo FORM_REQUIRED_INFORMATION; ?></span>
    </div>
    <div class="page-header">
      <h3><?php echo FORM_TITLE_CUSTOMER_DETAILS; ?></h3>
    </div>
    <div class="contentText">
      <div class="form-group has-feedback">
        <label for="inputFromName" class="control-label col-sm-4"><?php echo FORM_FIELD_CUSTOMER_NAME; ?></label>
        <div class="col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('from_name', NULL, 'required aria-required="true" id="inputFromName" placeholder="' . FORM_FIELD_CUSTOMER_NAME . '" minlength="'. ENTRY_FIRST_NAME_MIN_LENGTH .'"'); ?></span>
          <span class="text-warning pull-left">&nbsp; <?php echo ENTRY_FIRST_NAME_TEXT; ?></span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="form-group has-feedback" style="padding-top:10px;">
        <label for="inputFromEmail" class="control-label col-sm-4"><?php echo FORM_FIELD_CUSTOMER_EMAIL; ?></label>
        <div class="col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('from_email_address', NULL, 'required aria-required="true" id="inputFromEmail" placeholder="' . FORM_FIELD_CUSTOMER_EMAIL . '"', 'email'); ?></span>
          <span class="text-warning pull-left">&nbsp;<?php echo ENTRY_EMAIL_ADDRESS_TEXT; ?></span>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
<?php
// ---------------------- 
// --- Friend Detail   -----  
// ---------------------- 
?>
    <div class="page-header">
     <h3><?php echo FORM_TITLE_FRIEND_DETAILS; ?></h3>
    </div>
    <div class="contentText">
      <div class="form-group has-feedback">
        <label for="inputToName" class="control-label col-sm-4"><?php echo FORM_FIELD_CUSTOMER_NAME; ?></label>
        <div class="col-xs-8">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('to_name', NULL, 'required aria-required="true" id="inputToName" placeholder="' . FORM_FIELD_CUSTOMER_NAME . '" minlength="'. ENTRY_FIRST_NAME_MIN_LENGTH .'"'); ?></span>
          <span class="text-warning pull-left">&nbsp; <?php echo ENTRY_FIRST_NAME_TEXT; ?></span>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="form-group has-feedback" style="padding-top:10px;">
        <label for="inputFromEmail" class="control-label col-sm-4"><?php echo FORM_FIELD_CUSTOMER_EMAIL; ?></label>
        <div class="col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('to_email_address', NULL, 'required aria-required="true" id="inputFromEmail" placeholder="' . FORM_FIELD_CUSTOMER_EMAIL . '"', 'email'); ?></span>
          <span class="text-warning pull-left">&nbsp;<?php echo ENTRY_EMAIL_ADDRESS_TEXT; ?></span>
        </div>
      </div>
    </div>
<?php
// ---------------------- 
// --- Message   -----  
// ---------------------- 
?>
    <div class="clearfix"></div>
    <div class="page-header">
      <h3><?php echo FORM_TITLE_FRIEND_MESSAGE; ?></h3>
    </div>
    <div class="contentText">
        <div class="col-xs-11">
          <?php echo osc_draw_textarea_field('message', 'soft', 40, 8, NULL, 'required aria-required="true" id="inputMessage" placeholder="' . FORM_TITLE_FRIEND_MESSAGE . '"'); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <div class="contentText">
      <div class="form-group has-feedback">
        <label for="inputConfirmation" class="control-label col-sm-4"><?php echo ENTRY_NUMBER_EMAIL_CONFIRMATION; ?><span class="text-warning"><?php echo $number_confirmation; ?></span></label>
        <div class="col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('number_email_confirmation', NULL, 'required aria-required="true" id="inputFromEmail" placeholder="' . ENTRY_NUMBER_EMAIL_CONFIRMATION . '"', 'confirmation'); ?></span>
          <span class="text-warning pull-left">&nbsp; <?php echo ENTRY_SPAM_TEXT; ?></span>
        </div>
      </div>
  </div>
<?php
// ---------------------- 
// --- Button   -----  
// ---------------------- 
?>
    <div class="clearfix"></div>
    <div class="buttonSet row">
        <div class="col-xs-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('product_info.php', 'products_id=' . (int)$OSCOM_Products->getId() ),'primary'); ?></div>
        <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success'); ?></div>
    </div>
  </div>

</form>

<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
