<?php
/**
 * contact_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Service Contact Clients');

define('TABLE_HEADING_REF', 'Id contact');
define('TABLE_HEADING_DEPARTMENT', 'département');
define('TABLE_HEADING_DATE_ADDED', 'Date d\'ajout');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_CUSTOMERS_NAME','Nom du client');	
define('TABLE_HEADING_CUSTOMERS_EMAIL','Email');
define('TABLE_HEADING_CUSTOMERS_LANGUAGE','Langue');
define('TABLE_HEADING_STATUS','Statut de la demande');
define('TABLE_HEADING_ARCHIVE','Traitement Archivé');
define('TABLE_HEADING_USER_NAME','Traité par');
define('TABLE_HEADING_CUSTOMERS_RESPONSE','Réponse');
define('TABLE_HEADING_DATE_SENDING','Date envoyée');
define('TABLE_HEADING_CUSTOMERS_ID','Référence Client');

define('TITLE_REVIEWS_GENERAL', 'Informations générales');
define('TITLE_REVIEWS_ENTRY', 'Réponse au client');

define('SUBJECT_EMAIL', STORE_NAME .' : Reponse a votre requete');

define('TEXT_INFO_MESSAGE','<br />Nous restons à votre disposition pour toute information complémentaire. Veuillez ne pas répondre à cet email et passer par le formulaire de contact du site pour toute demande d\'information.');

define('ENTRY_ID_CUSTOMERS', 'Id contact :');	
define('ENTRY_CUSTOMER_ID','Référence client :');
define('ENTRY_DEPARTMENT', 'Département :');
define('ENTRY_CUSTOMERS_NAME','Nom du client :');	
define('ENTRY_EMAIL_SUBJECT','Sujet :');	
define('ENTRY_DATE_ADDED','Date d\'envoi :');	
define('ENTRY_CUSTOMERS_MESSAGE','Message du client :');	
define('ENTRY_ARCHIVE','Message archivé :');
define('ENTRY_REVIEW', 'Réponse au client :');
define('ENTRY_STATUS_MESSAGE','Statut du message');
define('ENTRY_STATUS_MESSAGE_NOT_REALISED', 'Message traité');
define('ENTRY_STATUS_MESSAGE_REALISED', 'Message non traité');
define('ENTRY_ARCHIVE_YES', 'Oui');
define('ENTRY_ARCHIVE_NO', 'non');
define('ENTRY_CUSTOMERS_TELEPHONE','Téléphoné :');

define('TAB_HISTORY','Historique');

define('TABLE_HEADING_LAST_MODIFIED','Modifié');
define('ICON_PREVIEW_COMMENT','Prévisualiser la réponse');
define('TEXT_INFO_DELETE_REVIEW_INTRO', 'Voulez vous vraiment effacer cette demande ?');
define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification :');

define('TEXT_INFO_HEADING_DELETE_REVIEW', 'Effacer les demandes');

?>