<?php
/*
 * stats_margin_report.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  require('includes/classes/currencies.php');
  $currencies = new currencies();
  
  // get list of order status
    $orders_statuses = array();
    $orders_status_array = array();
    
    $QordersStatus = $OSCOM_PDO->prepare('select orders_status_id,
                                                 orders_status_name
                                          from :table_orders_status
                                          where language_id = :language_id
                                          order by orders_status_id
                                         ');

    $QordersStatus->bindInt(':language_id', (int)$_SESSION['languages_id'] );
    $QordersStatus->execute();

    while ($orders_status = $QordersStatus->fetch() ) {
      $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                                 'text' => $orders_status['orders_status_name']);
      $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
    }
  
    $date = osc_db_query("SELECT curdate() as time");
    $d2 = osc_db_fetch_array($date, MYSQL_ASSOC);

  switch($_GET['report_id']) {
  case 'all':
    $header = TEXT_REPORT_HEADER;
    break;
  case 'daily':

    $date = osc_db_query("SELECT curdate() as time");
    $d = osc_db_fetch_array($date, MYSQL_ASSOC);

    $header = TEXT_REPORT_HEADER_FROM_DAY;
    break;
  case 'yesterday':

    $date = osc_db_query("SELECT DATE_SUB(curdate(), INTERVAL 1 DAY) as time");
    $d = osc_db_fetch_array($date, MYSQL_ASSOC);

    $l = 1;
    $header = TEXT_REPORT_HEADER_FROM_YESTERDAY;
    break;
  case 'weekly':
  case 'lastweek':
    if ($_GET['report_id'] == "lastweek") {
// last week
      $adjust = 7;
    } else {
      $adjust = 0;
    }
    $l = 7;  // seven day window length

    $weekday_query = osc_db_query("SELECT weekday(now()) as weekday");
    $weekday = osc_db_fetch_array($weekday_query);

    $day = 6+($weekday['weekday'] - 6);

//echo $day;
    switch($day) {
      case '0':
        $date = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+1)." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+1-7)." DAY as time");
      break;
      case '1':
        $date = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+2)." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+2-7)." DAY as time");
      break;
      case '2':
        $date = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+3)." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+3-7)." DAY as time");
      break;
      case '3':
        $date = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+4)." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+4-7)." DAY as time");
      break;
      case '4':
        $date = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+5)." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+5-7)." DAY as time");
      break;
      case '5':
        $date = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+6)." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjust+6-7)." DAY as time");
      break;
      case '6':
        $date = osc_db_query( "SELECT curdate() - INTERVAL ".$adjust." DAY as time");
        $date2 = osc_db_query("SELECT curdate() - INTERVAL ".($adjustd-7)." DAY as time");
      break;
    }

    $d = osc_db_fetch_array($date, MYSQL_ASSOC);
    $d2 = osc_db_fetch_array($date2, MYSQL_ASSOC);

    $header = TEXT_REPORT_HEADER_FROM_WEEK;
    break;
  case 'monthly':

    $date = osc_db_query("SELECT FROM_UNIXTIME(" . strtotime(date("F 1, Y")) . ") as time");
    $d = osc_db_fetch_array($date, MYSQL_ASSOC);

    $date = osc_db_query("SELECT curdate() as time");
    $d2 = osc_db_fetch_array($date, MYSQL_ASSOC);

    $header = TEXT_REPORT_HEADER_FROM_MONTH;
  break;
  case 'lastmonth':

    $date = osc_db_query("SELECT FROM_UNIXTIME(" . strtotime(date("F 1, Y")) . ") - INTERVAL 1 MONTH as time");
    $d = osc_db_fetch_array($date, MYSQL_ASSOC);

    $date = osc_db_query("SELECT FROM_UNIXTIME(" . strtotime(date("F 1, Y")) . ") - INTERVAL 0 MONTH as time");
    $d2 = osc_db_fetch_array($date, MYSQL_ASSOC);

    $header = TEXT_REPORT_HEADER_FROM_LASTMONTH;
    break;
  case 'quarterly':
    $quarter_query = osc_db_query("SELECT QUARTER(now()) as quarter, year(now()) as year");
    $quarter = osc_db_fetch_array($quarter_query, MYSQL_ASSOC);

    switch($quarter['quarter']) {
      case '1':
       $d['time'] = $quarter['year'] . '-01-01';
      break;
      case '2':
       $d['time'] = $quarter['year'] . '-04-01';
      break;
      case '3':
       $d['time'] = $quarter['year'] . '-07-01';
      break;
      case '4':
       $d['time'] = $quarter['year'] . '-10-01';
      break;
    }

    $header = TEXT_REPORT_HEADER_FROM_QUARTER;
    break;
    case 'semiannually':
      $year_query = osc_db_query("SELECT year(now()) as year, month(now()) as month");
      $year = osc_db_fetch_array($year_query, MYSQL_ASSOC);

      if ($year['month'] >= '7') {
        $d['time'] = $year['year'] . '-07-01';
      } else {
        $d['time'] = $year['year'] . '-01-01';
      }

      $header = TEXT_REPORT_HEADER_FROM_SEMIYEAR;
    break;
    case 'annually':
      $year_query = osc_db_query("SELECT year(now()) as year");
      $year = osc_db_fetch_array($year_query, MYSQL_ASSOC);
      $d['time'] = $year['year'] . '-01-01';
      $header = TEXT_REPORT_HEADER_FROM_YEAR;
    break;
  }
  // show orders with selected status
    if (isset($_GET['status']) && is_numeric($_GET['status']) && ($_GET['status'] > 0)){
      $status = osc_db_prepare_input($_GET['status']);	

    if (isset($_GET['date']) && ($_GET['date'] == 'yes')) {

      $header = TEXT_REPORT_BETWEEN_DAYS . $_GET['sdate'] . TEXT_AND . $_GET['edate'] . ': ';

      $date_debut = explode("/", $_GET['sdate']);
      $dd1 = $date_debut[2] . '-' . $date_debut[1] . '-' . $date_debut[0];

      $date_fin = explode("/", $_GET['edate']);
      $df1 = $date_fin[2] . '-' . $date_fin[1] . '-' . $date_fin[0];

      $order_query = osc_db_query("select orders_id from orders where date_purchased > '" . $dd1 . "' 
                                    and date_purchased < '" . $df1 . "' 
                                    and orders_status = '" . (int)$status . "' 
                                    order by orders_id asc
                                   ");
    } else {
      if ($_GET['report_id'] != '1') {

        $header_date_query = osc_db_query("select date_format('" . $d['time'] . "', '%d/%m/%Y') as date");
        $header_date = osc_db_fetch_array($header_date_query, MYSQL_ASSOC);
        $header .= $header_date['date'];

        if (isset($d2)) {
// have date range, use it
          $order_query = osc_db_query("select orders_id 
                                      from orders 
                                      where date_purchased > '" . $d['time'] . "' 
                                      and date_purchased < '" . $d2['time'] . "' 
                                      and orders_status = '" . (int)$status . "' 
                                      order by orders_id asc
                                    ");
         } else {
// don't have a d2, business as usual
            $order_query = osc_db_query("select orders_id 
                                              from orders 
                                              where date_purchased > '" . $d['time'] . "' 
                                              and date_purchased < now() 
                                              and orders_status = '" . (int)$status . "' 
                                              order by orders_id asc
                                             ");

        }
      }
    }
// show all orders
  } elseif (isset($_GET['date']) && ($_GET['date'] == 'yes')) {
    $header = TEXT_REPORT_BETWEEN_DAYS . $_GET['sdate'] . TEXT_AND . $_GET['edate'] . ': ';
    $date_debut = explode("/", $_GET['sdate']);
    $dd1 = $date_debut[2] . '-' . $date_debut[1] . '-' . $date_debut[0];
    $date_fin = explode("/", $_GET['edate']);
    $df1 = $date_fin[2] . '-' . $date_fin[1] . '-' . $date_fin[0];
    
    $order_query = osc_db_query("select orders_id 
                                  from orders 
                                  where date_purchased > '" . $dd1 . "' 
                                  and date_purchased < '" . $df1 . "' order by orders_id asc
                                ");
  } else {
    if ($_GET['report_id'] != '1') {
      $header_date_query = osc_db_query("SELECT DATE_FORMAT('" . $d2['time'] . "', '%d/%m/%Y') as date");
      $header_date = osc_db_fetch_array($header_date_query, MYSQL_ASSOC);
      $header .= $header_date['date'];

      if (isset($d2)) {
// have date range, use it
        $order_query = osc_db_query("select orders_id 
                                          from orders 
                                          where date_purchased > '" . $d['time'] . "' 
                                          and date_purchased < '" . $d2['time'] . "' 
                                          order by orders_id asc
                                        ");
      } else {
        
// don't have a d2, business as usual
        $order_query = osc_db_query("select orders_id 
                                          from orders 
                                          where date_purchased > '" . $d['time'] . "' 
                                          and date_purchased < now() 
                                          order by orders_id asc
                                        ");
         }
      } else {
        
        $order_query = osc_db_query("select orders_id from orders order by orders_id asc");
      }
    }

    $o = array();

    while ($order = osc_db_fetch_array($order_query, MYSQL_ASSOC)) {
      $o[] = $order['orders_id'];
    }

    $p = array();
    $total_price = 0;
    $total_cost = 0;
    $total_items_sold = 0;
    $t = '0';

  require('includes/header.php');
?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

  <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
      <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
        <tr valign="middle">
        <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/margin_report.png', HEADING_TITLE, '40', '40'); ?></td>
        <td class="pageHeading" width="60%"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
        <td align="right">
<?php 
    echo osc_draw_form('report', 'stats_margin_report.php', '', 'get') . TEXT_SHOW;

      $options = array();
      $options[] = array('id' => 'all', 'text' => TEXT_SELECT_REPORT);
      $options[] = array('id' => 'daily', 'text' => TEXT_SELECT_REPORT_DAILY);
      $options[] = array('id' => 'yesterday', 'text' => TEXT_SELECT_REPORT_YESTERDAY);
      $options[] = array('id' => 'weekly', 'text' => TEXT_SELECT_REPORT_WEEKLY);
      $options[] = array('id' => 'lastweek', 'text' => TEXT_SELECT_REPORT_LASTWEEK);
      $options[] = array('id' => 'monthly', 'text' => TEXT_SELECT_REPORT_MONTHLY);
      $options[] = array('id' => 'lastmonth', 'text' => TEXT_SELECT_REPORT_LASTMONTH);
      $options[] = array('id' => 'quarterly', 'text' => TEXT_SELECT_REPORT_QUARTERLY);
      $options[] = array('id' => 'semiannually', 'text' => TEXT_SELECT_REPORT_SEMIANNUALLY);
      $options[] = array('id' => 'annually', 'text' => TEXT_SELECT_REPORT_ANNUALLY);

    echo osc_draw_pull_down_menu('report_id', $options, (isset($_GET['report_id']) ? $_GET['report_id'] : '1'), 'onchange="this.form.submit()"');
// orders status
?>  
        <td align="right"><?php echo TITLE_STATUS . ' ' . osc_draw_pull_down_menu('status', array_merge(array(array('id' => '', 'text' => TEXT_ALL_ORDERS)), $orders_statuses), '', 'onChange="this.form.submit();"'); ?></td>
        </form></td>
        </tr>
      </table></td>
      </tr>
    </table></td>
    </tr>
  </table>
      <table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" width="100"><?php echo TEXT_ORDER_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo  TEXT_ITEMS_SOLD; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_SALES_AMOUNT; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_REDUC; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_SALES_NET; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_COST; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_HANDLING; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_GROSS_PROFIT; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_PROFIT_NET; ?></td>
                <td class="dataTableHeadingContent"><?php echo TEXT_ACTION; ?></td>
              </tr>

<?php

  for( $i=0; $i< sizeof($o); $i++) {
      $price = 0;
      $cost = 0;
      $handling = 0;
      $items_sold = 0;

      $prods_query = osc_db_query("select op.products_id, 
                                         op.products_price,
                                         op.final_price, 
                                         op.products_quantity,  
                                         p.products_cost,
                                         p.products_handling
                                  from orders_products op,
                                       products p
                                  where op.orders_id = '" . $o[$i] . "' 
                                  and op.products_id = p.products_id
                                 ");

    while ($prods = osc_db_fetch_array($prods_query, MYSQL_ASSOC)) {
      $p[] = array($prods['products_id'], $prods['products_price'], $prods['products_cost'], $prods['products_handling'], $prods['products_quantity']);

      $promo_discount_coupon_query = osc_db_query("select value 
                                                   from orders_total
                                                   where orders_id = '" . $o[$i] . "' 
                                                   and class='ot_discount_coupon'
                                                  ");
      $promo = osc_db_fetch_array($promo_discount_coupon_query);

/*
    $promo_customers_discount_query = osc_db_query("select value 
                                                     from orders_total
                                                     where orders_id = '" . $o[$i] . "' 
                                                     and class='ot_customers_discount'
                                                    ");
    $promo_customers_discount_ = osc_db_fetch_array($promo_customers_discount_query);
*/


    $items_sold += $prods['products_quantity'];
    $price += $prods['products_price'] * $prods['products_quantity'];
    $cost +=  $prods['products_cost'] * $prods['products_quantity'];
    $handling += $prods['products_handling'] * $prods['products_quantity']; 
    $reduc += $promo['value'] * $prods['products_quantity'];

    $total_items_sold += $prods['products_quantity'];
    $total_price += $prods['products_price'] * $prods['products_quantity']; 
    $total_reduc += $promo['value'] * $prods['products_quantity'];	
    $total_handling += $prods['products_handling'] * $prods['products_quantity'];
    $total_cost += ($prods['products_cost'] * $prods['products_quantity']); 

// the following two lines will give us per order margin as well as the total margin
    if ($prods['products_price']  !='0') {
      $margin = osc_round(((($prods['products_price']-$prods['products_cost']) / $prods['products_price'] )*100), 0);
      $margin_net = osc_round((((($prods['products_price'] - $promo['value'] - $prods['products_cost'] - $prods['products_handling'])) / $prods['products_price'])*100), 0);

      $total_margin = osc_round(((($total_price - $total_cost) / $total_price) *100), 2);
      $total_margin_net = osc_round(((($total_price - $total_reduc - $total_cost - $total_handling) / $total_price)*100), 2);
    }
   } // end while
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)"">
                <td  class="dataTableContent"><?php echo $o[$i]; ?></td>
                <td  class="dataTableContent"><?php echo $items_sold; ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format($price); ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format($reduc); ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format(($price - $reduc)); ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format($cost); ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format($handling); ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format(($price - $cost)) . ' (' . $margin . '%)'; ?></td>
                <td  class="dataTableContent"><?php echo $currencies->format($price - $reduc - $cost - $handling) . ' (' . $margin_net . '%)'; ?></td>
                <td  class="dataTableContent" align="right"><?php echo '<a href=' . osc_href_link('orders.php', '&oID=' . $o[$i] . '&action=edit') . '>' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT); ?></a></td>
              </tr>
<?php
  } // end for
?>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_TOTAL_ITEMS_SOLD; ?></strong></td>
                <td class="main" colspan="2" bgcolor="#E0E0E0" >&nbsp;<?php echo $total_items_sold; ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_ITEMS_SOLD; ?></strong></td>
                <td class="main" colspan="2" bgcolor="#E0E0E0">&nbsp;<?php echo $currencies->format($total_price); ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_TOTAL_REDUC; ?></strong></td>
                <td class="main"  colspan="2" bgcolor="#E0E0E0">&nbsp;<?php echo $currencies->format($total_reduc); ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_SALES_NET; ?></strong></td>
                <td class="main" colspan="2" bgcolor="#E0E0E0">&nbsp;<?php echo $currencies->format(($total_price - $total_reduc)); ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_TOTAL_COST; ?></strong></td>
                <td class="main" colspan="2" bgcolor="#E0E0E0">&nbsp;<?php echo $currencies->format($total_cost) ; ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_TOTAL_GROSS_PROFIT; ?></strong></td>
                <td class="main" colspan="2" bgcolor="#E0E0E0">&nbsp;<?php echo $currencies->format(($total_price - $total_cost)) . ' (' . $total_margin . '%)'; ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2" bgcolor="#C8FCCA"><strong><?php echo TEXT_TOTAL_MARGIN_NET; ?></strong></td>
                <td class="main" colspan="2" bgcolor="#E0E0E0">&nbsp;<?php echo $currencies->format(($total_price - $total_cost - $total_reduc - $total_handling)) . ' (' . $total_margin_net . '%)'; ?></td>
              </tr>
            </table></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
         <tr>
          <td><table border="0" cellpadding="2" cellspacing="2">
            <tr>
             <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_MARGIN_IMAGE); ?></td>
             <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_MARGIN; ?></strong></td>
            </tr>
            <tr>
             <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
             <td class="main"><?php echo TITLE_AIDE_MARGIN_DESCRIPTION; ?></td>
            </tr>
           </table></td>
         </tr>
        </table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
