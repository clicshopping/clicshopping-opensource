<?php
/**
 * suppliers_ajax.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Gestion des fournisseurs');

define('TITLE_SUPPLIERS_GENERAL','Informations généralres');
define('TITLE_SUPPLIERS_IMAGE','Image du fournisseur');
define('TAB_VISUEL','Image');
define('TAB_SUPPLIERS_NOTE','Notes');

define('TEXT_SUPPLIERS', 'Fournisseurs :');
define('TEXT_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');

define('TEXT_SUPPLIERS_NAME', 'Nom du fournisseur :');
define('TEXT_SUPPLIERS_NEW_IMAGE', 'Nouvelle image du fournisseur :');
define('TEXT_SUPPLIERS_IMAGE', 'Image actuel du fournisseur :');
define('TEXT_SUPPLIERS_DELETE_IMAGE', 'Ne plus afficher d\'image fournisseur');
define('TEXT_SUPPLIERS_URL', 'URL du fournisseur :');
define('TEXT_SUPPLIERS_MANAGER', 'Nom responsable :');
define('TEXT_SUPPLIERS_PHONE', 'Télephone :');
define('TEXT_SUPPLIERS_FAX', 'Fax :');
define('TEXT_SUPPLIERS_EMAIL_ADDRESS','Adresse email :');
define('TEXT_SUPPLIERS_ADDRESS', 'Adresse :');
define('TEXT_SUPPLIERS_SUBURB', ' Addresse complémentaire :');
define('TEXT_SUPPLIERS_POSTCODE', 'Code Postale : ');
define('TEXT_SUPPLIERS_CITY', 'Ville :');
define('TEXT_SUPPLIERS_STATES', 'Etat / Département :');
define('TEXT_SUPPLIERS_COUNTRY', 'Pays :');
define('TEXT_SUPPLIERS_NOTES','Notes complémentaires');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');
define('TEXT_DELETE_IMAGE', 'Suppprimer l\'image du fournisseur ?');
?>