<?php
/*
 * stats_suppliers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Récapitulatif des commandes fournisseurs');

define('TABLE_HEADING_SUPPLIERS_ID','Id fournisseur');
define('TABLE_HEADING_SUPPLIERS', 'Fournisseur');
define('TABLE_HEADING_MANAGER', 'Responsable');
define('TABLE_HEADING_EMAIL', 'email');
define('TABLE_HEADING_PHONE','Téléphone');
define('TABLE_HEADING_ORDER','Nombre de commandes');
define('TABLE_HEADING_ACTION','Actions');

define('HEADING_TITLE_PRODUCTS_BY_SUPPLIERS','Détail de la commande');
define('TABLE_HEADING_MODEL','Modèle du produit');
define('TABLE_HEADING_SUPPLIERS_NAME','Nom fournisseur');
define('TABLE_HEADING_PRODUCTS_NAME','Nom produit');	
define('TABLE_HEADING_PRODUCTS_OPTIONS','Option du produit');
define('TABLE_HEADING_PRODUCTS_OPTIONS_VALUES','Valeur Option');
define('TABLE_HEADING_QUANTITY','Quantité');

define('HEADING_TITLE_PRODUCTS_BY_CUSTOMERS','Détail des clients pour un fournisseur');
define('TABLE_HEADING_CUSTOMERS_id','Id Client');
define('TABLE_HEADING_CUSTOMERS_MANE','Nom Client');

define('ENTRY_STATUS','Statut de la commande traitée');


define('PERIOD','période du ');
define('ICON_STATS_SUPPLIERS_EDIT_CUSTOMERS','Voir les commandes par client');
define('IMAGE_STATS_SUPPLIERS_PDF','Editer le rapport PDF');
define('IMAGE_STATS_SUPPLIERS_EDIT','Editer les commandes concernant ce fournisseur');
define('IMAGE_ICON_INFO','Toutes les commandes du client');
define('ICON_EDIT_ORDERS','Voir toutes les commandes du client');
define('ICON_EDIT_PRODUCTS','Plus d\'informations sur ce produit');

define('TABLE_HEADING_PRODUCTS', 'Produits');

define('TEXT_ALL_STATUS','Tous les status');
define('ENTRY_START_DATE','Date début analyse');
define('ENTRY_TO_DATE','Date fin analyse');
define('ENTRY_SUBMIT','Go');
define('ENTRY_STATUS','Type Statut');

?>

