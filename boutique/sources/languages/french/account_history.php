<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Historique');

define('HEADING_TITLE', 'Historique de mes commandes / factures');

define('HEADING_DESCRIPTION', 'Suivez l\'avancement de vos commandes et consultez votre historique.');
define('TEXT_ORDER_NUMBER', 'Commande / facture No.');
define('TEXT_ORDER_STATUS', 'Statut : ');
define('TEXT_ORDER_DATE', 'Date de prise de la commande :');
define('TEXT_ORDER_SHIPPED_TO', 'Envoyée à :');
define('TEXT_ORDER_BILLED_TO', 'Facturée à:');
define('TEXT_ORDER_PRODUCTS', 'Articles :');
define('TEXT_ORDER_COST', 'Montant total :');
define('TEXT_VIEW_ORDER', 'Voir la commande');

define('TEXT_NO_PURCHASES', '<p style="color:#ff0000;">Vous n\'avez pas encore fait d\'achat ou votre commande est en cours de traitement.</p>');
?>
