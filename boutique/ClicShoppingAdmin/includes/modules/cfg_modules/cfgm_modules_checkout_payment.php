<?php
/**
 * cfm_modules_checkout_payment.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class cfgm_modules_checkout_payment {
    var $code = 'modules_checkout_payment';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_CHECKOUT_PAYMENT_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_checkout_payment() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_checkout_payment/';
      $this->title = MODULE_CFG_MODULE_CHECKOUT_PAYMENT_TITLE;
    }
  }
?>
