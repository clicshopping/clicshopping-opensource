<?php
/*
 * account_history.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_history'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_history.php', '', 'SSL'));

  $orders_total = osc_count_customer_orders();

  if ($orders_total > 0) {

    $history_query_raw = "select o.orders_id, 
                                 o.date_purchased, 
                                 o.delivery_name, 
                                 o.billing_name, 
                                 ot.text as order_total, 
                                 s.orders_status_name 
                         from orders o,
                              orders_total ot,
                              orders_status s
                         where o.customers_id = '" . (int)$OSCOM_Customer->getID() . "' 
                         and o.orders_id = ot.orders_id 
                         and ot.class = 'ot_total' 
                         and o.orders_status = s.orders_status_id 
                         and s.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                         and s.public_flag = '1' 
                         order by orders_id desc
                        ";

    $history_split = new splitPageResults($history_query_raw, MAX_DISPLAY_ORDER_HISTORY);
    $history_query = osc_db_query($history_split->sql_query);
  }

  require($OSCOM_Template->getTemplateFiles('account_history'));
