<?php
/*
 * order.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  class order {
    var $info, $totals, $products, $customer, $delivery;

    function order($order_id) {
      $this->info = array();
      $this->totals = array();
      $this->products = array();
      $this->customer = array();
      $this->delivery = array();

      $this->query($order_id);
    }

    function query($order_id) {
      global $OSCOM_PDO;
        
      $Qorders = $OSCOM_PDO->prepare('select *
                                      from :table_orders
                                      where orders_id = :orders_id
                                      ');
      $Qorders->bindInt(':orders_id', (int)$order_id);

      $Qorders->execute();

      $order = $Qorders->fetch();

      $QtotalOrders = $OSCOM_PDO->prepare('select title,
                                                   text
                                            from :table_orders_total
                                            where orders_id = :orders_id
                                            order by sort_order
                                          ');
      $QtotalOrders->bindInt(':orders_id', (int)$order_id);

      $QtotalOrders->execute();

      while ($totals = $QtotalOrders->fetch() ) {
        $this->totals[] = array('title' => $totals['title'],
                                'text' => $totals['text'],
                                'class' => $totals['class']);
      }

       $this->info = array('total' => null,
                           'currency' => $order['currency'],
                          'currency_value' => $order['currency_value'],
                          'payment_method' => $order['payment_method'],
                          'cc_type' => $order['cc_type'],
                          'cc_owner' => $order['cc_owner'],
                          'cc_number' => $order['cc_number'],
                          'cc_expires' => $order['cc_expires'],
                          'date_purchased' => $order['date_purchased'],
                          'orders_status' => $order['orders_status'],
                          'orders_status_invoice' => $order['orders_status_invoice'],
                          'last_modified' => $order['last_modified'],
                          'odoo_invoice' => $order['odoo_invoice'],
                        );

      foreach ( $this->totals as $t ) {
        if ($t['class'] == 'ot_total') {
          $this->info['total'] = $t['text'];
          break;
        }
      }
      
      $this->customer = array('name' => $order['customers_name'],
                              'company' => $order['customers_company'],
                              'siret' => $order['customers_siret'],
                              'ape' => $order['customers_ape'],
                              'tva_intracom' => $order['customers_tva_intracom'],
                              'street_address' => $order['customers_street_address'],
                              'suburb' => $order['customers_suburb'],
                              'city' => $order['customers_city'],
                              'postcode' => $order['customers_postcode'],
                              'state' => $order['customers_state'],
                              'country' => $order['customers_country'],
                              'format_id' => $order['customers_address_format_id'],
                              'telephone' => $order['customers_telephone'],
                              'cellular_phone' => $order['customers_cellular_phone'],
                              'email_address' => $order['customers_email_address'],
                              'client_computer_ip' => $order['client_computer_ip'],
                              'provider_name_client' => $order['provider_name_client']); 


      $this->delivery = array('name' => $order['delivery_name'],
                              'company' => $order['delivery_company'],
                              'street_address' => $order['delivery_street_address'],
                              'suburb' => $order['delivery_suburb'],
                              'city' => $order['delivery_city'],
                              'postcode' => $order['delivery_postcode'],
                              'state' => $order['delivery_state'],
                              'country' => $order['delivery_country'],
                              'format_id' => $order['delivery_address_format_id']);

      $this->billing = array('name' => $order['billing_name'],
                             'company' => $order['billing_company'],
                             'street_address' => $order['billing_street_address'],
                             'suburb' => $order['billing_suburb'],
                             'city' => $order['billing_city'],
                             'postcode' => $order['billing_postcode'],
                             'state' => $order['billing_state'],
                             'country' => $order['billing_country'],
                             'format_id' => $order['billing_address_format_id']);

      $index = 0;

      $QordersProducts = $OSCOM_PDO->prepare('select orders_products_id,
                                                      products_id,
                                                      products_name,
                                                      products_model,
                                                      products_price,
                                                      products_tax,
                                                      products_quantity,
                                                      final_price,
                                                      products_full_id
                                               from :table_orders_products
                                               where orders_id = :orders_id
                                              ');
      $QordersProducts->bindInt(':orders_id', (int)$order_id);

      $QordersProducts->execute();

      while ($orders_products = $QordersProducts->fetch() ) {
        $this->products[$index] = array('qty' => $orders_products['products_quantity'],
                                        'products_id' => $orders_products['products_id'],
                                        'name' => $orders_products['products_name'],
                                        'model' => $orders_products['products_model'],
                                        'tax' => $orders_products['products_tax'],
                                        'price' => $orders_products['products_price'],
                                        'final_price' => $orders_products['final_price'],
                                        'products_full_id' => $orders_products['products_id']
                                      );

        $subindex = 0;

        $Qattributes = $OSCOM_PDO->prepare('select products_options,
                                                   products_options_values,
                                                   options_values_price,
                                                   price_prefix,
                                                   products_attributes_reference
                                            from :table_orders_products_attributes
                                            where orders_id = :orders_id
                                            and orders_products_id = :orders_products_id
                                          ');
        $Qattributes->bindInt(':orders_id', (int)$order_id);
        $Qattributes->bindInt(':orders_products_id', (int)$orders_products['orders_products_id']);

        $Qattributes->execute();

        if ($Qattributes->rowCount()) {

          while ($attributes = $Qattributes->fetch() ) {
            $this->products[$index]['attributes'][$subindex] = array('option' => $attributes['products_options'],
                                                                     'value' => $attributes['products_options_values'],
                                                                     'prefix' => $attributes['price_prefix'],
                                                                     'price' => $attributes['options_values_price'],
                                                                     'reference' => $attributes['products_attributes_reference']
                                                                     );
            $subindex++;
          }
        }
        $index++;
      }
    }
  }

