<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_CUSTOMER_DISCOUNT_TITLE', 'Specific discount for a client');
  define('MODULE_CUSTOMER_DISCOUNT_DESCRIPTION', 'A discount on order has been realised');
?>
