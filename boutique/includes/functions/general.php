<?php
/**
 * general.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

/**
 * Stop from parsing any further PHP code
 *
 * @param string 
 * @access public
 */
  function osc_exit() {
   session_write_close();
   exit;
  }

/**
 * Redirect to a URL address
 *
 * @param string $url The URL address to redirect to anotherpage
 * @access public
  * ULTIMATE Seo Urls 5 PRO by FWR Media
 */
////
  function osc_redirect($url) {
    global $request_type;

    if ( (strstr($url, "\n") != false) || (strstr($url, "\r") != false) ) { 
      osc_redirect(osc_href_link('index.php', '', 'NONSSL', false));
    }

    if ( (ENABLE_SSL == true) && ($request_type == 'SSL') ) { // We are loading an SSL page
      if (substr($url, 0, strlen(HTTP_SERVER . DIR_WS_HTTP_CATALOG)) == HTTP_SERVER . DIR_WS_HTTP_CATALOG) { // NONSSL url
        $url = HTTPS_SERVER . DIR_WS_HTTPS_CATALOG . substr($url, strlen(HTTP_SERVER . DIR_WS_HTTP_CATALOG)); // Change it to SSL
      }
    }

    if ( strpos($url, '&amp;') !== false ) {
      $url = str_replace('&amp;', '&', $url);
    }

    header('Location: ' . $url);

    osc_exit();
  }

/**
 * Parse and output a user submited value
 *
 * @param string $string The string to parse and output
 * @param array $translate An array containing the characters to parse
 * @access public
 */
  function osc_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return strtr(trim($string), array('"' => '&quot;'));
      } else {
        return strtr(trim($string), $translate);
      }
    }
  }

/**
 * Strictly parse and output a user submited value
 *
 * @param string $string The string to strictly parse and output
 * @access public
 */

  function osc_output_string_protected($string) {
    return osc_output_string($string, false, true);
  }

/**
 * Sanitize a user submited value
 *
 * @param string $string The string to sanitize
 * @access public
 */

  function osc_sanitize_string($string) {
    $patterns = array ('/ +/','/[<>]/', '/&lt;/', '/&gt;/', '/%3c/', '/%2f/');
    $replace = array (' ', '_', '_', '_', '_', '_');

    return preg_replace($patterns, $replace, trim($string));
  }

/**
 * Return a random row from a database query
 *
 * @param string $random_product, a random
 * @access public
 */
  function osc_random_select($query) {
    $random_product = '';
    $random_query = osc_db_query($query);
    $num_rows = osc_db_num_rows($random_query);
    if ($num_rows > 0) {
      $random_row = osc_rand(0, ($num_rows - 1));
      osc_db_data_seek($random_query, $random_row);
      $random_product = osc_db_fetch_array($random_query);
    }

    return $random_product;
  }


/**
 * Display a product name for a product
 *
 * @param string $product['name'], the name of the product
 * @access public
 */
///
  function osc_get_products_name($product_id, $language = '') {
    global $OSCOM_PDO;

    if (!isset($language_id)) $language_id = $_SESSION['languages_id'];

    $Qproduct = $OSCOM_PDO->prepare('select products_name
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id );
    $Qproduct->execute();

    return $Qproduct->value('products_name');
  }

////
// Return a product's special price (returns nothing if there is no offer)
// TABLES: products
  function osc_get_products_special_price($product_id) {
    global $OSCOM_PDO;

// Ajout de customers_group_id = '0' afin de selectionner seulement les produits des clients grand public

      $Qproduct = $OSCOM_PDO->prepare('select distinct customers_group_id, 
                                                        specials_new_products_price
                                       from :table_specials
                                       where products_id = :products_id 
                                       and status = :status 
                                       and customers_group_id = :customers_group_id
                                     ');
      $Qproduct->bindInt(':products_id', (int)$product_id);
      $Qproduct->bindInt(':status', '1' );
      $Qproduct->bindInt(':customers_group_id', '0' );
      $Qproduct->execute();

    if ($Qproduct->fetch() !== false) {
      $result = $Qproduct->valueDecimal('specials_new_products_price');
    }

    return $result;
  }

/**
 * Return a product's stock
 * @param string $products_id
 * @return the $stock_values['products_quantity']
 * @access public
 * TABLES: products
 */
////
  function osc_get_products_stock($products_id) {
    global $OSCOM_PDO;

    $products_id = osc_get_prid($products_id);

    $Qproduct = $OSCOM_PDO->prepare('select products_quantity 
                                     from :table_products
                                     where products_id = :products_id
                                    ');
    $Qproduct->bindInt(':products_id', (int)$products_id);

    $Qproduct->execute();

    return $Qproduct->valueInt('products_quantity');
  }

/**
 * Return a out of stock on the products
 * Check if the required stock is available
 * If insufficent stock is available return an out of stock message
 * @param string $products_id
 * @param string $products_quantity
 * @return $out_of_stock
 * @access public
 * TABLES: products
 */
////

  function osc_check_stock($products_id, $products_quantity) {

    $stock_left = osc_get_products_stock($products_id) - $products_quantity;
    $out_of_stock = '';

    if ($stock_left < 0) {
      $out_of_stock = '<span class="markProductOutOfStock">' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '</span>';
    }

    return $out_of_stock;
  }




/**
 * Break a word in a string if it is longer than a specified length ($len)
 *
 * @param string $string
 * @param string $len
 * @param string $break_char
 * @access public
 */

  function osc_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }

/**
 * Get all parameters in the GET scope - Return all GET variables, except those passed as a parameter
 *
 * @param array $exclude A list of parameters to exclude
 * @access public
 */
  function osc_get_all_get_params($exclude_array = '') {

    if (!is_array($exclude_array)) $exclude_array = array();

    $exclude_array[] = session_name();
    $exclude_array[] = 'error';
    $exclude_array[] = 'x';
    $exclude_array[] = 'y';

    $get_url = '';

    foreach ( $_GET as $k => $v ) {
      if ( !in_array($k, $exclude_array) ) {
        $get_url .= $k;

        if ( !empty($v) ) {
          $get_url .= '=' . rawurlencode($v);
        }

        $get_url .= '&';
      }
    }
    
    return $get_url;
  }

/**
 *  Returns an array with the default countries
 *  TABLES: countries
 * @param string $default_countries
 * @param string $default_coutries
 * @access public
 */
////

  function osc_get_default_country($default_country) {

  $country_default_query = osc_db_query("select countries_iso_code_2 
                                         from countries
                                         where countries_id = '" . (int)STORE_COUNTRY . "'
                                        ");
  $country_default = osc_db_fetch_array($country_default_query);
  $default_country = $country_default['countries_iso_code_2'];

  return $default_country;
  }

/**
 *  Returns an array with countries
 *  TABLES: countries
 * @param string $countries_id
 * @param string $with_iso_codes
 * @access public
 */
////

  function osc_get_countries($countries_id = '', $with_iso_codes = false) {
    global $OSCOM_PDO;

    $countries_array = array();

    if (osc_not_null($countries_id)) {
      if ($with_iso_codes == true) {

        $Qcountries = $OSCOM_PDO->prepare('select countries_name,
                                                  countries_iso_code_2,
                                                  countries_iso_code_3
                                           from :table_countries
                                           where countries_id = :countries_id
                                           and status = :status
                                           order by countries_name
                                          ');
        $Qcountries->bindInt(':countries_id', (int)$countries_id);
        $Qcountries->bindValue(':status', '1');
        $Qcountries->execute();

        $countries_array = $Qcountries->toArray();
      } else {

        $Qcountries = $OSCOM_PDO->prepare('select countries_name
                                           from :table_countries
                                           where countries_id = :countries_id
                                           and status = :status
                                          ');
        $Qcountries->bindInt(':countries_id', (int)$countries_id);
        $Qcountries->bindValue(':status', '1');
        $Qcountries->execute();

        $countries_array = $Qcountries->toArray();
      }
    } else {

      $countries_array = $OSCOM_PDO->query('select countries_id,
                                                  countries_name,
                                                  countries_iso_code_2 
                                           from :table_countries
                                            order by countries_name
                                          ')->fetchAll();
    }

    return $countries_array;
  }


/**
 *  Alias function to osc_get_countries, which also returns the countries iso codes
 * @param string $countries_id
 * @access public
 */
////

  function osc_get_countries_with_iso_codes($countries_id) {
    return osc_get_countries($countries_id, true);
  }

/**
 * Generate a path to categories
 * @param $current_category_id
 * @access public
 */

  function osc_get_path($current_category_id = '') {
    global $OSCOM_PDO, $cPath_array;

    if (osc_not_null($current_category_id)) {
      $cp_size = sizeof($cPath_array);
      if ($cp_size == 0) {
        $cPath_new = $current_category_id;
      } else {
        $cPath_new = '';

        $QlastCategory = $OSCOM_PDO->prepare('select parent_id
                                              from :table_categories
                                              where categories_id = :categories_id
                                            ');
        $QlastCategory->bindInt(':categories_id',  (int)$cPath_array[($cp_size-1)]);
        $QlastCategory->execute();

        $QcurrentCategory = $OSCOM_PDO->prepare('select parent_id
                                                 from :table_categories
                                                 where categories_id = :categories_id
                                                ');
        $QcurrentCategory->bindValue(':categories_id',  (int)$current_category_id);
        $QcurrentCategory->execute();

        if ($QlastCategory->valueInt('parent_id') == $QcurrentCategory->valueInt('parent_id')) {
          for ($i=0; $i<($cp_size-1); $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        } else {
          for ($i=0; $i<$cp_size; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }
        $cPath_new .= '_' . $current_category_id;

        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
      }
    } else {
      $cPath_new = implode('_', $cPath_array);
    }

    return 'cPath=' . $cPath_new;
  }

/**
 * Returns the clients browser
 * @param $component
 * @access public
 */

  function osc_browser_detect($component) {

    return stristr($_SERVER['HTTP_USER_AGENT'], $component);
  }

/**
 * Alias function to osc_get_countries()
 * @param $country_id
 * @access public
 */

  function osc_get_country_name($country_id) {
    $country_array = osc_get_countries($country_id);

    return $country_array['countries_name'];
  }

/**
 *  Returns the zone (State/Province) name
 *  TABLES: zones
 * @param string $countries_id
 * @param string $zone_id
 * @param string $default_zone
 * @access public
 */
////

  function osc_get_zone_name($country_id, $zone_id, $default_zone) {
    global $OSCOM_PDO;

    $Qzone = $OSCOM_PDO->prepare('select zone_name 
                                 from :table_zones
                                 where zone_country_id = :zone_country_id
                                 and zone_id = :zone_id
                               ');
    $Qzone->bindInt(':zone_country_id', (int)$country_id);
    $Qzone->bindInt(':zone_id', (int)$zone_id );

    $Qzone->execute();

    if ($Qzone->fetch() !== false) {
      return $Qzone->value('zone_name');
    } else {
      return $default_zone;
    }
  }


/**
 *  Returns the zone code (State/Province) 
 *  TABLES: zones
 * @param string $countries_id
 * @param string $zone_id
 * @param string $default_zone
 * @access public
 */
////

  function osc_get_zone_code($country_id, $zone_id, $default_zone) {
    global $OSCOM_PDO;

    $Qzone = $OSCOM_PDO->prepare('select zone_code 
                                 from :table_zones
                                 where zone_country_id = :zone_country_id
                                 and zone_id = :zone_id
                               ');
    $Qzone->bindInt(':zone_country_id', (int)$country_id);
    $Qzone->bindInt(':zone_id', (int)$zone_id );

    $Qzone->execute();

    if ($Qzone->fetch() !== false) {
      return $Qzone->value('zone_code');
    } else {
      return $default_zone;
    }
  }

/**
 * Round a number with the wanted precision
 *
 * @param float $number The number to round
 * @param int $precision The precision to use for the rounding
 * @access public
 */
////
// Wrapper function for round()
  function osc_round($number, $precision) {
    if (strpos($number, '.') && (strlen(substr($number, strpos($number, '.')+1)) > $precision)) {
      $number = substr($number, 0, strpos($number, '.') + 1 + $precision + 1);

      if (substr($number, -1) >= 5) {
        if ($precision > 1) {
          $number = substr($number, 0, -1) + ('0.' . str_repeat(0, $precision-1) . '1');
        } elseif ($precision == 1) {
          $number = substr($number, 0, -1) + 0.1;
        } else {
          $number = substr($number, 0, -1) + 1;
        }
      } else {
        $number = substr($number, 0, -1);
      }
    }

    return $number;
  }

////
// Returns the tax rate for a zone / class
// TABLES: tax_rates, zones_to_geo_zones
  function osc_get_tax_rate($class_id, $country_id = -1, $zone_id = -1) {
    global $OSCOM_Customer, $OSCOM_PDO;

    static $tax_rates = array();

    if ( ($country_id == -1) && ($zone_id == -1) ) {
      if ( !$OSCOM_Customer->isLoggedOn() || !$OSCOM_Customer->hasDefaultAddress() ) {
        $country_id = STORE_COUNTRY;
        $zone_id = STORE_ZONE;
      } else {
        $country_id = $OSCOM_Customer->getCountryID();
        $zone_id = $OSCOM_Customer->getZoneID();
      }
    }

    if (!isset($tax_rates[$class_id][$country_id][$zone_id]['rate'])) {

      $Qtax = $OSCOM_PDO->prepare('select sum(tr.tax_rate) as tax_rate
                                  from :table_tax_rates tr left join :table_zones_to_geo_zones za on (tr.tax_zone_id = za.geo_zone_id) 
                                                           left join :table_geo_zones tz on (tz.geo_zone_id = tr.tax_zone_id) 
                                  where (za.zone_country_id is null or 
                                         za.zone_country_id = 0 or 
                                         za.zone_country_id = :zone_country_id) 
                                         and (za.zone_id is null or 
                                             za.zone_id = 0 or 
                                             za.zone_id = :zone_id) 
                                         and tr.tax_class_id = :tax_class_id 
                                         group by tr.tax_priority
                                         ');
      $Qtax->bindInt(':zone_country_id', $country_id);
      $Qtax->bindInt(':zone_id', $zone_id);
      $Qtax->bindInt(':tax_class_id', $class_id);
      $Qtax->execute();

      if ($Qtax->fetch() !== false) {
        $tax_multiplier = 1.0;

        do {
          $tax_multiplier *= 1.0 + ($Qtax->valueDecimal('tax_rate') / 100);
        } while ($Qtax->fetch());

        $tax_rates[$class_id][$country_id][$zone_id]['rate'] = ($tax_multiplier - 1.0) * 100;
      } else {
        $tax_rates[$class_id][$country_id][$zone_id]['rate'] = 0;
      }
    }

    return $tax_rates[$class_id][$country_id][$zone_id]['rate'];
  }

////
// Return the tax description for a zone / class
// TABLES: tax_rates;
  function osc_get_tax_description($class_id, $country_id, $zone_id) {
   global $OSCOM_PDO;
    static $tax_rates = array();

    if (!isset($tax_rates[$class_id][$country_id][$zone_id]['description'])) {

      $Qtax = $OSCOM_PDO->prepare('select tr.tax_description
                                  from :table_tax_rates tr left join :table_zones_to_geo_zones za on (tr.tax_zone_id = za.geo_zone_id)
                                                           left join :table_geo_zones tz on (tz.geo_zone_id = tr.tax_zone_id)
                                   where (za.zone_country_id is null or
                                          za.zone_country_id = 0 or
                                          za.zone_country_id = :zone_country_id
                                          )
                                   and (za.zone_id is null or
                                        za.zone_id = 0 or
                                        za.zone_id = :zone_id
                                        )
                                   and tr.tax_class_id = :tax_class_id
                                   order by tr.tax_priority
                                   ');
      $Qtax->bindInt(':zone_country_id', $country_id);
      $Qtax->bindInt(':zone_id', $zone_id);
      $Qtax->bindInt(':tax_class_id', $class_id);
      $Qtax->execute();

      if ($Qtax->fetch() !== false) {
        $tax_description = '';

        do {
          $tax_description .= $Qtax->value('tax_description') . ' x ';
        } while ($Qtax->fetch());

        $tax_description = substr($tax_description, 0, -3);

        $tax_rates[$class_id][$country_id][$zone_id]['description'] = $tax_description;
      } else {
        $tax_rates[$class_id][$country_id][$zone_id]['description'] = TEXT_UNKNOWN_TAX_RATE;
      }
    }

    return $tax_rates[$class_id][$country_id][$zone_id]['description'];
  }

////
// Add tax to a products price
  function osc_add_tax($price, $tax) {
    global $currencies, $tag, $OSCOM_Customer, $OSCOM_PDO;

// derniere mise a jour MS2 RC2A Mis en commentaire
/*	
    if ( (DISPLAY_PRICE_WITH_TAX == 'true') && ($tax > 0) ) {
      return $price + osc_calculate_tax($price, $tax);
    } else {
      return $price;
    }
  }	
*/	
// ########## B2B ##########

    $QgroupTax = $OSCOM_PDO->prepare('select group_tax 
                                     from :table_customers_groups
                                     where customers_group_id = :customers_group_id
                                     ');
    $QgroupTax->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());

    $QgroupTax->execute();

    $group_tax = $QgroupTax->fetch();

// Code modifie par rapport a l'original afin d'avoir un meilleur controle sur l'affichage de la TVA selon les comptes clients
    if (($OSCOM_Customer->isLoggedOn()) && ($OSCOM_Customer->getCustomersGroupID() != '0') && ($group_tax['group_tax'] == 'true') && ($tax > 0)) {
      $group_taxed = 'true';
    } else if (($OSCOM_Customer->isLoggedOn()) && ($OSCOM_Customer->getCustomersGroupID() != '0') && ($group_tax['group_tax'] != 'true')) {
      $group_taxed = 'false';
    } else if (($OSCOM_Customer->isLoggedOn()) && (DISPLAY_PRICE_WITH_TAX == 'true') && ($tax > 0)) {
      $group_taxed = 'true';
    } else if ((!$OSCOM_Customer->isLoggedOn()) && (DISPLAY_PRICE_WITH_TAX == 'true') && ($tax > 0)) {
      $group_taxed = 'true';
    } else {
      $group_taxed = 'false';
    }

    switch ($group_taxed) {
      case 'true':
        $tag = TAX_INCLUDED ;
        return osc_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + osc_calculate_tax($price, $tax) + $tag;
      break;
      case 'false':
        $tag = TAX_EXCLUDED ;
        return osc_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places'])  + $tag;
       break;
      default:
        $tag = TAX_EXCLUDED ;
        return osc_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places'])  + $tag;
      break;
    }
  }

// Calculates Tax rounding the result
  function osc_calculate_tax($price, $tax) {
    return $price * $tax / 100;
  }


////
// Return the number of products in a category
// TABLES: products, products_to_categories, categories
  function osc_count_products_in_category($category_id, $include_inactive = false) {
    global $OSCOM_PDO;

    $products_count = 0;

    $products_query = 'select count(*) as total
                       from :table_products p, 
                            :table_products_to_categories p2c 
                       where p.products_id = p2c.products_id 
                       and p2c.categories_id = :categories_id
                     ';

    if ($include_inactive == false) {
      $products_query .= ' and p.products_status = 1';
    }

    $Qproducts = $OSCOM_PDO->prepare($products_query);
    $Qproducts->bindInt(':categories_id', $category_id);
    $Qproducts->execute();

    if ($Qproducts->fetch() !== false) {
      $products_count += $Qproducts->valueInt('total');
    }

    $Qcategories = $OSCOM_PDO->prepare('select categories_id 
                                        from :table_categories 
                                        where parent_id = :parent_id
                                        and virtual_vategories = 0
                                      ');
    $Qcategories->bindInt(':parent_id', $category_id);
    $Qcategories->execute();

    if ($Qcategories->fetch() !== false) {
      do {
        $products_count += tep_count_products_in_category($Qcategories->valueInt('categories_id'), $include_inactive);
      } while ($Qcategories->fetch());
    }

    return $products_count;
  }

// Return true if the category has subcategories
// TABLES: categories
  function osc_has_category_subcategories($category_id) {
    global $OSCOM_PDO;

    $Qcheck = $OSCOM_PDO->prepare('select categories_id 
                                  from :table_categories
                                  where parent_id = :parent_id
                                  limit 1
                                 ');
    $Qcheck->bindInt(':parent_id', $category_id);
    $Qcheck->execute();

    return ($Qcheck->fetch() !== false);
  }

////
// Returns the address_format_id for the given country
// TABLES: countries;
  function osc_get_address_format_id($country_id) {
    global $OSCOM_PDO;
    $format_id = 1;

    $Qformat = $OSCOM_PDO->prepare('select address_format_id
                                    from :table_countries
                                    where countries_id = :countries_id
                                   ');
    $Qformat->bindInt(':countries_id', (int)$country_id);

    $Qformat->execute();

    if ($Qformat->fetch() !== false) {
      $format_id = $Qformat->valueInt('address_format_id');
    }

    return $format_id;
  }

////
// Return a formatted address
// TABLES: address_format
  function osc_address_format($address_format_id, $address, $html, $boln, $eoln) {
    global $OSCOM_Customer,$OSCOM_PDO;

    $customer_group_id = $OSCOM_Customer->getCustomersGroupID();

    $QaddressFormat = $OSCOM_PDO->prepare('select address_format as format 
                                           from :table_address_format
                                           where address_format_id = :address_format_id
                                         ');
    $QaddressFormat->bindInt(':address_format_id', (int)$address_format_id);

    $QaddressFormat->execute();

    $address_format = $QaddressFormat->fetch();

    $company = osc_output_string_protected($address['company']);
    if (isset($address['firstname']) && osc_not_null($address['firstname'])) {
      $firstname = osc_output_string_protected($address['firstname']);
      $lastname = osc_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && osc_not_null($address['name'])) {
      $firstname = osc_output_string_protected($address['name']);
      $lastname = '';
    } else {
      $firstname = '';
      $lastname = '';
    }
    $street = osc_output_string_protected($address['street_address']);
    $suburb = osc_output_string_protected($address['suburb']);
    $city = osc_output_string_protected($address['city']);
    $state = osc_output_string_protected($address['state']);
    if (isset($address['country_id']) && osc_not_null($address['country_id'])) {
      $country = osc_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && osc_not_null($address['zone_id'])) {
        $state = osc_get_zone_name($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && osc_not_null($address['country'])) {
      if ( is_string($address['country']) ) {
        $country = osc_output_string_protected($address['country']);
      } else {
        $country = osc_output_string_protected($address['country']['title']);
      }
    } else {
      $country = '';
    }
    $postcode = osc_output_string_protected($address['postcode']);
    $zip = $postcode;

    if ($html) {
// HTML Mode
      $HR = '<hr />';
      $hr = '<hr />';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br />';
        $cr = '<br />';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    $streets = $street;
    if ($suburb != '') $streets = $street . $cr . $suburb;
    if ($state != '') $statecomma = $state . ', ';

    $fmt = $address_format['format'];
    eval("\$address = \"$fmt\";");

    if ((($customer_group_id == 0) && (ACCOUNT_COMPANY == 'true') && (osc_not_null($company))) || (($customer_group_id != 0) && (ACCOUNT_COMPANY_PRO == 'true') && (osc_not_null($company)))) {
      $address = $company . $cr . $address;
    }

    return $address;
  }

////
// Return a formatted address
// TABLES: customers, address_book
  function osc_address_label($customers_id, $address_id = 1, $html = false, $boln = '', $eoln = "\n") {
    global $OSCOM_Customer, $OSCOM_PDO;
    if (is_array($address_id) && !empty($address_id)) {
      return osc_address_format($address_id['address_format_id'], $address_id, $html, $boln, $eoln);
    }

    $Qaddress = $OSCOM_PDO->prepare('select entry_firstname as firstname, 
                                            entry_lastname as lastname, 
                                            entry_company as company, 
                                            entry_street_address as street_address, 
                                            entry_suburb as suburb, entry_city as city, 
                                            entry_postcode as postcode, 
                                            entry_state as state, 
                                            entry_zone_id as zone_id, 
                                            entry_country_id as country_id
                                     from :table_address_book
                                     where customers_id = :customers_id 
                                     and address_book_id = :address_book_id
                                   ');

    $Qaddress->bindInt(':address_book_id', (int)$address_id);
    $Qaddress->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    
    $Qaddress->execute();

    $format_id = osc_get_address_format_id($Qaddress->valueInt('country_id'));

    return osc_address_format($format_id, $Qaddress->toArray(), $html, $boln, $eoln);
  }

//
//
//
  function osc_row_number_format($number) {
    if ( ($number < 10) && (substr($number, 0, 1) != '0') ) $number = '0' . $number;

    return $number;
  }

//
//
//
  function osc_get_categories($categories_array = '', $parent_id = '0', $indent = '') {
    global $OSCOM_PDO;

    if (!is_array($categories_array)) $categories_array = array();

    $Qcategories = $OSCOM_PDO->prepare('select c.categories_id,
                                             cd.categories_name
                                        from :table_categories c,
                                             :table_categories_description cd
                                        where parent_id = :parent_id
                                        and c.categories_id = cd.categories_id
                                        and cd.language_id = :language_id
                                        and virtual_categories = :virtual_categories
                                        order by sort_order, cd.categories_name
                                       ');
    $Qcategories->bindInt(':parent_id', (int)$parent_id);
    $Qcategories->bindInt(':language_id',  (int)$_SESSION['languages_id']);
    $Qcategories->bindValue(':virtual_categories', '0');
    $Qcategories->execute();

    while ($Qcategories->fetch()) {
      $categories_array[] = array('id' => $Qcategories->valueInt('categories_id'),
                                  'text' => $indent . $Qcategories->value('categories_name'));

      if ($Qcategories->valueInt('categories_id') != $parent_id) {
        $categories_array = osc_get_categories($categories_array, $Qcategories->valueInt('categories_id'), $indent . '&nbsp;&nbsp;');
      }
    }

    return $categories_array;
  }

/**
 * Display a  manufacturers under an array
 *
 * @param string $manufacturers_array, an array of manufacturer
 * @access public
 */

  function osc_get_manufacturers($manufacturers_array = '') {
    global $OSCOM_PDO;

    if (!is_array($manufacturers_array)) $manufacturers_array = array();

    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturers_id,
                                                  manufacturers_name
                                           from :table_manufacturers
                                           where manufacturers_status = :manufacturers_status
                                           order by manufacturers_name
                                         ');

    $Qmanufacturers->bindValue(':manufacturers_status', '0');
    $Qmanufacturers->execute();

    while ($Qmanufacturers->fetch()) {
      $manufacturers_array[] = array('id' => $Qmanufacturers->valueInt('manufacturers_id'), 
                                     'text' => $Qmanufacturers->value('manufacturers_name'));
    }

    return $manufacturers_array;
  }

////
// Return all subcategory IDs
// TABLES: categories
  function osc_get_subcategories(&$subcategories_array, $parent_id = 0) {
    global $OSCOM_PDO;
  
    $Qsub = $OSCOM_PDO->prepare('select categories_id 
                                from :table_categories 
                                where parent_id = :parent_id
                                ');
    $Qsub->bindInt(':parent_id', $parent_id);
    $Qsub->execute();

    while ($Qsub->fetch()) {
      $subcategories_array[sizeof($subcategories_array)] = $Qsub->valueInt('categories_id');

      if ($Qsub->valueInt('categories_id') != $parent_id) {
        osc_get_subcategories($subcategories_array, $Qsub->valueInt('categories_id'));
      }
    }
  }

////
// Parse search string into indivual objects
  function osc_parse_search_string($search_str = '', &$objects) {
    $search_str = trim(strtolower($search_str));

// Break up $search_str on whitespace; quoted string will be reconstructed later
    $pieces = preg_split('/[[:space:]]+/', $search_str);
    $objects = array();
    $tmpstring = '';
    $flag = '';

    for ($k=0; $k<count($pieces); $k++) {
      while (substr($pieces[$k], 0, 1) == '(') {
        $objects[] = '(';
        if (strlen($pieces[$k]) > 1) {
          $pieces[$k] = substr($pieces[$k], 1);
        } else {
          $pieces[$k] = '';
        }
      }

      $post_objects = array();

      while (substr($pieces[$k], -1) == ')')  {
        $post_objects[] = ')';
        if (strlen($pieces[$k]) > 1) {
          $pieces[$k] = substr($pieces[$k], 0, -1);
        } else {
          $pieces[$k] = '';
        }
      }

// Check individual words

      if ( (substr($pieces[$k], -1) != '"') && (substr($pieces[$k], 0, 1) != '"') ) {
//        $objects[] = trim($pieces[$k]);
         $objects[] = trim(preg_replace('/"/', ' ', $pieces[$k]));

        for ($j=0; $j<count($post_objects); $j++) {
          $objects[] = $post_objects[$j];
        }
      } else {
/* 
   This means that the $piece is either the beginning or the end of a string.
   So, we'll slurp up the $pieces and stick them together until we get to the
   end of the string or run out of pieces.
*/

// Add this word to the $tmpstring, starting the $tmpstring
        $tmpstring = trim(preg_replace('/"/', ' ', $pieces[$k]));

// Check for one possible exception to the rule. That there is a single quoted word.
        if (substr($pieces[$k], -1 ) == '"') {
// Turn the flag off for future iterations
          $flag = 'off';

          $objects[] = trim(preg_replace('/"/', ' ', $pieces[$k]));

          for ($j=0; $j<count($post_objects); $j++) {
            $objects[] = $post_objects[$j];
          }

          unset($tmpstring);

// Stop looking for the end of the string and move onto the next word.
          continue;
        }

// Otherwise, turn on the flag to indicate no quotes have been found attached to this word in the string.
        $flag = 'on';

// Move on to the next word
        $k++;

// Keep reading until the end of the string as long as the $flag is on

        while ( ($flag == 'on') && ($k < count($pieces)) ) {
          while (substr($pieces[$k], -1) == ')') {
            $post_objects[] = ')';
            if (strlen($pieces[$k]) > 1) {
              $pieces[$k] = substr($pieces[$k], 0, -1);
            } else {
              $pieces[$k] = '';
            }
          }

// If the word doesn't end in double quotes, append it to the $tmpstring.
          if (substr($pieces[$k], -1) != '"') {
// Tack this word onto the current string entity
            $tmpstring .= ' ' . $pieces[$k];

// Move on to the next word
            $k++;
            continue;
          } else {
/* If the $piece ends in double quotes, strip the double quotes, tack the
   $piece onto the tail of the string, push the $tmpstring onto the $haves,
   kill the $tmpstring, turn the $flag "off", and return.
*/
            $tmpstring .= ' ' . trim(preg_replace('/"/', ' ', $pieces[$k]));

// Push the $tmpstring onto the array of stuff to search for
            $objects[] = trim($tmpstring);

            for ($j=0; $j<count($post_objects); $j++) {
              $objects[] = $post_objects[$j];
            }

            unset($tmpstring);

// Turn off the flag to exit the loop
            $flag = 'off';
          }
        }
      }
    }

// add default logical operators if needed
    $temp = array();
    for($i=0; $i<(count($objects)-1); $i++) {
      $temp[] = $objects[$i];
      if ( ($objects[$i] != 'and') &&
           ($objects[$i] != 'or') &&
           ($objects[$i] != '(') &&
           ($objects[$i+1] != 'and') &&
           ($objects[$i+1] != 'or') &&
           ($objects[$i+1] != ')') ) {
        $temp[] = ADVANCED_SEARCH_DEFAULT_OPERATOR;
      }
    }
    $temp[] = $objects[$i];
    $objects = $temp;

    $keyword_count = 0;
    $operator_count = 0;
    $balance = 0;
    for($i=0; $i<count($objects); $i++) {
      if ($objects[$i] == '(') $balance --;
      if ($objects[$i] == ')') $balance ++;
      if ( ($objects[$i] == 'and') || ($objects[$i] == 'or') ) {
        $operator_count ++;
      } elseif ( ($objects[$i]) && ($objects[$i] != '(') && ($objects[$i] != ')') ) {
        $keyword_count ++;
      }
    }

    if ( ($operator_count < $keyword_count) && ($balance == 0) ) {
      return true;
    } else {
      return false;
    }
  }

////
// Return table heading with sorting capabilities
  function osc_create_sort_heading($sortby, $colnum, $heading) {
    global $PHP_SELF;

    $sort_prefix = '';
    $sort_suffix = '';

    if ($sortby) {
      $sort_prefix = '<a href="' . osc_href_link($PHP_SELF, osc_get_all_get_params(array('page', 'info', 'sort')) . 'page=1&sort=' . $colnum . ($sortby == $colnum . 'a' ? 'd' : 'a')) . '" title="' . osc_output_string(TEXT_SORT_PRODUCTS . ($sortby == $colnum . 'd' || substr($sortby, 0, 1) != $colnum ? TEXT_ASCENDINGLY : TEXT_DESCENDINGLY) . TEXT_BY . $heading) . '" class="productListing-heading">' ;
      $sort_suffix = (substr($sortby, 0, 1) == $colnum ? (substr($sortby, 1, 1) == 'a' ? '+' : '-') : '') . '</a>';
    }

    return $sort_prefix . $heading . $sort_suffix;
  }

////
// Recursively go through the categories and retreive all parent categories IDs
// TABLES: categories
  function osc_get_parent_categories(&$categories, $categories_id) {
    global $OSCOM_PDO;

    $Qparent = $OSCOM_PDO->prepare('select parent_id
                                    from :table_categories
                                    where categories_id = :categories_id
                                  ');

    $Qparent->bindInt(':categories_id', (int)$categories_id);
    $Qparent->execute();

    while ($Qparent->fetch()) {
      if ($Qparent->valueInt('parent_id') == 0) return true;

      $categories[sizeof($categories)] = $Qparent->valueInt('parent_id');

      if ($Qparent->valueInt('parent_id') != $categories_id) {
        osc_get_parent_categories($categories, $Qparent->valueInt('parent_id'));
      }
    }
  }

////
// Construct a category path to the product
// TABLES: products_to_categories
  function osc_get_product_path($products_id) {
    global $OSCOM_PDO;

    $cPath = '';

    $Qcategory = $OSCOM_PDO->prepare('select p2c.categories_id 
                                      from :table_products p, 
                                          :table_products_to_categories p2c 
                                      where p.products_id = :products_id 
                                      and p.products_status = :products_status
                                      and p.products_id = p2c.products_id 
                                      and p.products_archive = :products_archive
                                      limit 1
                                    ');
    $Qcategory->bindInt(':products_id', (int)$products_id);
    $Qcategory->bindInt(':products_status', '1');
    $Qcategory->bindInt(':products_archive', '0');

    $Qcategory->execute();

    if ( $Qcategory->fetch() !== false ) {

      $categories = array();
      osc_get_parent_categories($categories, $Qcategory->valueInt('categories_id'));

      $categories = array_reverse($categories);

      $cPath = implode('_', $categories);

      if (osc_not_null($cPath)) $cPath .= '_';
      $cPath .= $Qcategory->valueInt('categories_id');
    }

    return $cPath;
  }

////
// Return a product ID with attributes
  function osc_get_uprid($prid, $params) {
    if (is_numeric($prid)) {
      $uprid = (int)$prid;

      if (is_array($params) && (!empty($params))) {
        $attributes_check = true;
        $attributes_ids = '';

        foreach ($params as $option => $value) {
          if (is_numeric($option) && is_numeric($value)) {
            $attributes_ids .= '{' . (int)$option . '}' . (int)$value;
          } else {
            $attributes_check = false;
            break;
          }
        }

        if ($attributes_check == true) {
          $uprid .= $attributes_ids;
        }
      }
    } else {
      $uprid = osc_get_prid($prid);

      if (is_numeric($uprid)) {
        if (strpos($prid, '{') !== false) {
          $attributes_check = true;
          $attributes_ids = '';

// strpos()+1 to remove up to and including the first { which would create an empty array element in explode()
          $attributes = explode('{', substr($prid, strpos($prid, '{')+1));

          for ($i=0, $n=sizeof($attributes); $i<$n; $i++) {
            $pair = explode('}', $attributes[$i]);

            if (is_numeric($pair[0]) && is_numeric($pair[1])) {
              $attributes_ids .= '{' . (int)$pair[0] . '}' . (int)$pair[1];
            } else {
              $attributes_check = false;
              break;
            }
          }

          if ($attributes_check == true) {
            $uprid .= $attributes_ids;
          }
        }
      } else {
        return false;
      }
    }

    return $uprid;
  }

////
// Return a product ID from a product ID with attributes
  function osc_get_prid($uprid) {
    $pieces = explode('{', $uprid);

    if (is_numeric($pieces[0])) {
      return (int)$pieces[0];
    } else {
      return false;
    }
  }

//
// Return a customer greeting
//
  function osc_customer_greeting() {
    global $OSCOM_Customer;

    if ($OSCOM_Customer->isLoggedOn()) {
      $greeting_string = sprintf(TEXT_GREETING_PERSONAL, osc_output_string_protected($OSCOM_Customer->getFirstName()), osc_href_link('products_new.php'), osc_href_link('logoff.php'), osc_output_string_protected($OSCOM_Customer->getFirstName()));
    } else {
       if (MODE_MANAGEMENT_B2C_B2B == 'B2C_B2B' || MODE_MANAGEMENT_B2C_B2B =='B2B') {
          $greeting_string = sprintf(TEXT_GREETING_GUEST, osc_href_link('login.php', '', 'SSL'), osc_href_link('create_account.php', '', 'SSL'), osc_href_link('create_account_pro.php', '', 'SSL'));
       } else {
           $greeting_string = sprintf(TEXT_GREETING_GUEST, osc_href_link('login.php', '', 'SSL'), osc_href_link('products_new.php'));
      }
    }

    return $greeting_string;
  }

/**
 * Send email (text/html) using MIME
 * This is the central mail function. The SMTP Server should be configured
 * @param string $to_name The name of the recipient
 * @param string $to_email_address The email address of the recipient
 * @param string $subject The subject of the email
 * @param string $body The body text of the email
 * @param string $from_name The name of the sender
 * @param string $from_email_address The email address of the sender
 * @access public
 */


  function osc_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
    if (SEND_EMAILS != 'true') return false;

// Instantiate a new mail object
    $message = new email(array('X-Mailer: ClicShopping Mailer'));

    // Build the text version
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

////
// Check if product has attributes
  function osc_has_product_attributes($products_id) {
    global $OSCOM_PDO;

    $Qattributes = $OSCOM_PDO->prepare('select products_id 
                                      from :table_products_attributes 
                                      where products_id = :products_id 
                                      limit 1
                                      ');
    $Qattributes->bindInt(':products_id', $products_id);

    $Qattributes->execute();

    return $Qattributes->fetch() !== false;
  }


//
//
//
  function osc_count_modules($modules = '') {
    $count = 0;

    if (empty($modules)) return $count;

    $modules_array = explode(';', $modules);

    for ($i=0, $n=sizeof($modules_array); $i<$n; $i++) {
      $class = substr($modules_array[$i], 0, strrpos($modules_array[$i], '.'));

      if (isset($GLOBALS[$class]) && is_object($GLOBALS[$class])) {
        if ($GLOBALS[$class]->enabled) {
          $count++;
        }
      }
    }

    return $count;
  }

//
//
//
  function osc_count_payment_modules() {
    return osc_count_modules(MODULE_PAYMENT_INSTALLED);
  }

//
//
//
//
  function osc_count_shipping_modules() {
    return osc_count_modules(MODULE_SHIPPING_INSTALLED);
  }

//
//
//
  function osc_create_random_value($length, $type = 'mixed') {
    if ( ($type != 'mixed') && ($type != 'chars') && ($type != 'digits')) $type = 'mixed';

    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $digits = '0123456789';

    $base = '';

    if ( ($type == 'mixed') || ($type == 'chars') ) {
      $base .= $chars;
    }

    if ( ($type == 'mixed') || ($type == 'digits') ) {
      $base .= $digits;
    }

    $value = '';

    if (!class_exists('PasswordHash')) {
      include(DIR_WS_CLASSES . 'passwordhash.php');
    }

    $hasher = new PasswordHash(10, true);

    do {
      $random = base64_encode($hasher->get_random_bytes($length));

      for ($i = 0, $n = strlen($random); $i < $n; $i++) {
        $char = substr($random, $i, 1);

        if ( strpos($base, $char) !== false ) {
          $value .= $char;
        }
      }
    } while ( strlen($value) < $length );

    if ( strlen($value) > $length ) {
      $value = substr($value, 0, $length);
    }

    return $value;
  }

//
//
//
  function osc_array_to_string($array, $exclude = '', $equals = '=', $separator = '&') {
    if (!is_array($exclude)) $exclude = array();

    $get_string = '';
    if (!empty($array)) {
      foreach ($array as $key => $value) {
        if ( (!in_array($key, $exclude)) && ($key != 'x') && ($key != 'y') ) {
          $get_string .= $key . $equals . $value . $separator;
        }
      }
      $remove_chars = strlen($separator);
      $get_string = substr($get_string, 0, -$remove_chars);
    }

    return $get_string;
  }

  function osc_not_null($value) {
    if (is_array($value)) {
      if (!empty($value)) {
        return true;
      } else {
        return false;
      }
    } elseif(is_object($value)) {
      if (count(get_object_vars($value)) === 0) {
        return false;
      } else {
        return true;
      }
    } else {
      if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }

////
// Output the tax percentage with optional padded decimals
  function osc_display_tax_value($value, $padding = TAX_DECIMAL_PLACES) {
    if (strpos($value, '.')) {
      $loop = true;
      while ($loop) {
        if (substr($value, -1) == '0') {
          $value = substr($value, 0, -1);
        } else {
          $loop = false;
          if (substr($value, -1) == '.') {
            $value = substr($value, 0, -1);
          }
        }
      }
    }

    if ($padding > 0) {
      if ($decimal_pos = strpos($value, '.')) {
        $decimals = strlen(substr($value, ($decimal_pos+1)));
        for ($i=$decimals; $i<$padding; $i++) {
          $value .= '0';
        }
      } else {
        $value .= '.';
        for ($i=0; $i<$padding; $i++) {
          $value .= '0';
        }
      }
    }

    return $value;
  }

////
// Checks to see if the currency code exists as a currency
// TABLES: currencies
  function osc_currency_exists($code) {
    global $OSCOM_PDO;

    $code = osc_db_prepare_input($code);

    $Qcurrency = $OSCOM_PDO->prepare('select code
                                     from :table_currencies 
                                     where code = :code
                                     limit 1
                                    ');
    $Qcurrency->bindValue(':code', (int)$code);

    $Qcurrency->execute();

    if ($Qcurrency->fetch() !== false) {
      return $Qcurrency->value('code');
    }

    return false;
  }

////
// Parse and secure the cPath parameter values
  function osc_parse_category_path($cPath) {

// make sure the category IDs are integers
    $cPath_array = array_map(function ($string) {
      return (int)$string;
    }, explode('_', $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i=0; $i<$n; $i++) {
      if (!in_array($cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $cPath_array[$i];
      }
    }

    return $tmp_array;
  }

/**
 * Generate a random number
 *
 * @param int $min The minimum number to return
 * @param int $max The maxmimum number to return
 * @access public
 */

  function osc_rand($min = null, $max = null) {

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

/**
 * Set a cookie
 *
 * @param string $name The name of the cookie
 * @param string $value The value of the cookie
 * @param int $expire Unix timestamp of when the cookie should expire
 * @param string $path The path on the server for which the cookie will be available on
 * @param string $domain The The domain that the cookie is available on
 * @param boolean $secure Indicates whether the cookie should only be sent over a secure HTTPS connection
 * @param boolean $httpOnly Indicates whether the cookie should only accessible over the HTTP protocol
 * @access public
 */

  function osc_setcookie($name, $value = '', $expire = 0, $path = null, $domain = null, $secure = 0) {
    global $cookie_path, $cookie_domain;
    setcookie($name, $value, $expire, (isset($path)) ? $path : $cookie_path, (isset($domain)) ? $domain : $cookie_domain, $secure);
  }

/**
 * Validate the IP address of the client
 * @param string $ip_address, th ip of the client
 * @access public
 */
  function osc_validate_ip_address($ip_address) {
    return filter_var($ip_address, FILTER_VALIDATE_IP, array('flags' => FILTER_FLAG_IPV4));
  }

/**
 * Get the IP address of the client
 * @param string $ip, th ip of the client
 * @access public
 */

  function osc_get_ip_address() {
    static $_ip_address;

    if ( !isset($_ip_address) ) {
      $_ip_address = null;
      $ip_addresses = array();

      if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
        foreach ( array_reverse(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])) as $x_ip ) {
          $x_ip = trim($x_ip);

          if (osc_validate_ip_address($x_ip)) {
            $ip_addresses[] = $x_ip;
          }
        }
      }

      if ( isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']) ) {
        $ip_addresses[] = $_SERVER['HTTP_CLIENT_IP'];
      }

      if ( isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && !empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) ) {
        $ip_addresses[] = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
      }

      if ( isset($_SERVER['HTTP_PROXY_USER']) && !empty($_SERVER['HTTP_PROXY_USER']) ) {
        $ip_addresses[] = $_SERVER['HTTP_PROXY_USER'];
      }

      if ( isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']) ) {
        $ip_addresses[] = $_SERVER['REMOTE_ADDR'];
      }

      foreach ( $ip_addresses as $ip ) {
        if (!empty($ip) && osc_validate_ip_address($ip)) {
          $ip_address = $ip;
          break;
        }
      }
    }
    return $ip_address;
  }

/**
 * Get the IP address of the client
 * @param string $id The id of the order
 * @param string $check_session of the session customer
 * @access public
 */

  function osc_count_customer_orders($id = '', $check_session = true) {
    global $OSCOM_Customer, $OSCOM_PDO;
    
    if (is_numeric($id) == false) {
      if ($OSCOM_Customer->isLoggedOn()) {
        $id = $OSCOM_Customer->getID();
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( !$OSCOM_Customer->isLoggedOn() || ($id != $OSCOM_Customer->getID()) ) {
        return 0;
      }
    }

    $Qorders = $OSCOM_PDO->prepare('select count(*) as total 
                                        from :table_orders o, 
                                             :table_orders_status s 
                                        where o.customers_id = :customers_id 
                                        and o.orders_status = s.orders_status_id 
                                        and s.language_id = :language_id
                                        and s.public_flag = :public_flag
                                    ');
    $Qorders->bindInt(':customers_id', (int)$id);
    $Qorders->bindInt(':language_id',  (int)$_SESSION['languages_id']);
    $Qorders->bindValue(':public_flag', '1');

    $Qorders->execute();

    if ($Qorders->fetch() !== false) {
      return $Qorders->valueInt('total');
    }

    return 0;
  }


/**
 * count customer address book
 * @param string $id, $check_session
 * @param string $addresses['total'], number of the address
 * @access public
 */
  function osc_count_customer_address_book_entries($id = '', $check_session = true) {
    global $OSCOM_Customer, $OSCOM_PDO;
    
    if (is_numeric($id) == false) {
      if ($OSCOM_Customer->isLoggedOn()) {
        $id = $OSCOM_Customer->getID();
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( !$OSCOM_Customer->isLoggedOn() || ($id != $OSCOM_Customer->getID()) ) {
        return 0;
      }
    }

    $Qaddresses = $OSCOM_PDO->prepare('select count(*) as total 
                                       from :table_address_book
                                       where customers_id = :customers_id
                                       limit 1
                                      ');
    $Qaddresses->bindInt(':customers_id', (int)$id);

    $Qaddresses->execute();

    if ($Qaddresses->fetch() !== false) {
      return $Qaddresses->valueInt('total');
    }

    return 0;
  }

/**
 * nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
 */
   
  function osc_convert_linefeeds($from, $to, $string) {
    return str_replace($from, $to, $string);
  }
