<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Specials');
define('HEADING_TITLE', 'Special Offers - Get Them While They\'re Hot!');
define('MIN_QTY_ORDER_PRODUCT','Min qty : ');
define('TEXT_PRODUCT_OPTIONS','options suggested:');
define('TEXT_NO_PRODUCTS','No specials in this moment');
?>
