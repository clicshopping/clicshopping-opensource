<?php
/*
 * stats_manuacturers_sales_functions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  
  function getUSLanguageDateFormat($strDate) {
    // transform if necessary dd/mm/yyyy to yyyy-mm-dd 
    if ((strlen($strDate) <> 10) || (DEFAULT_LANGUAGE != "fr")) {
      return $strDate;
    } else {
      // replacing / by -
      $strDate = str_replace("/", "-", $strDate);
      
      if ($strDate{2} == "-") {
//        dd-mm-yyyy
//        0123456789
        return substr($strDate, 6, 4)."-".substr($strDate, 3, 2)."-".substr($strDate, 0, 2); 
      } else {
//        yyyy-mm-dd
//        0123456789
        return $strDate;
      }
    }
  }
  function getDefaultLanguageDateFormat($strDate) {
    // transform if necessary yyyy-mm-dd to dd/mm/yyyy 
    if ((strlen($strDate) <> 10) || (DEFAULT_LANGUAGE != "fr")) {
      return $strDate;
    } else {
      // replacing / by -
      $strDate = str_replace("/", "-", $strDate);
      
      if ($strDate{2} == "-") {
//        dd-mm-yyyy
//        0123456789
        return str_replace("-", "/", $strDate);
      } else {
//        yyyy-mm-dd
//        0123456789
        return substr($strDate, 8, 2)."/".substr($strDate, 5, 2)."/".substr($strDate, 0, 4); 
      }
    }
  }
  
  function getMySQLraw($whichSQL) {
    global $start_date, $end_date, $manufacturer_id, $customer_id;
    
    $select = "sum(op.products_quantity) as sum_pq, 
               sum(op.final_price*op.products_quantity) as sum_fp";
    $join = "";
    $max_date = "max(o.date_purchased) as dp, ";
    
    switch ($whichSQL) {
    case "m":
        $by = "m.manufacturers_name";
        $select = "m.manufacturers_id, " . $by . ", " . $select;
        $and = "AND m.manufacturers_id IS NOT NULL AND m.manufacturers_id <> ''"; 
        break;
    case "c":
        $by = "o.customers_name";
        $select = "o.customers_id, " . $by . ", " . $max_date . $select; 
        $and = "AND m.manufacturers_id = " . $manufacturer_id;
        break;
    case "p":
        $by = "op.products_name, opa.products_options, opa.products_options_values";
        $select = "op.products_id, " . $by . ", " . $max_date . $select; 
        $join = "LEFT JOIN orders_products_attributes AS opa ON op.orders_products_id = opa.orders_products_id";
        $and = "AND p.manufacturers_id = " . $manufacturer_id;
        if ($customer_id != "all") {
          $and .= " AND o.customers_id = " . $customer_id;
        }
        break;
    }
    $sql = "SELECT DISTINCT " . $select . "
                FROM orders_products AS op
                LEFT JOIN products AS p ON op.products_id = p.products_id
                LEFT JOIN manufacturers AS m on p.manufacturers_id = m.manufacturers_id
                LEFT JOIN orders AS o ON op.orders_id = o.orders_id " . $join . "
                WHERE o.date_purchased BETWEEN '" . getUSLanguageDateFormat($start_date) . " 00:00:00' AND '" . getUSLanguageDateFormat($end_date) . " 23:59:59' 
        " .$and . "
                GROUP BY " . $by . "
                ORDER BY " . $by;
//    echo $sql;
    return $sql;
  }
  function querystring_remove($remove_vars) {
  /*
    * Usage:
    * Suppose $_SERVER['QUERY_STRING'] looks like this: ?a=1&b=2&c=3:
    *
    * echo querystring_remove("a"); -> ?b=2&c=3 (a is removed)
  */
    if ( !is_array($remove_vars) ) {
      $remove_vars = array($remove_vars);
    }
  
    $sep = ini_get('arg_separator.output');
    $qs = "";
    foreach ( $_GET as $k => $v ) {
      if ( !in_array($k, $remove_vars) ) {
        $qs .= "$k=" . urlencode($v) . $sep;
      }
    }
  
    return substr($qs, 0, -1); /* trim off trailing $sep */
  }
  
  function getCustomerProductsBlock() {
    global $print, $total_quantity, $total_sales, $customer_id, $currencies;
    
    $total_quantity = 0;
    $total_sales = 0;

    $table = '<hr>
      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td class="main" colspan="1" cellpadding="40">';
          
    //recherche du client dans la base
    if (($customer_id != "all_by_once") && ($customer_id != "all")) {

      $Qorders = $OSCOM_PDO->prepare('select *
                                      from :table_orders
                                      where customers_id = :customer_id
                                      order by orders_id DESC
                                     ');

      $Qorders->bindInt(':customer_id', (int)$customer_id );
      $Qorders->execute();

      if ($orders = $Qorders->fetch() ) {
// customer found!
        $order = new order($orders['orders_id']);
        
        $table .= "<span class='pageHeadingSmall'>".TABLE_CUSTOMER_NAME."</span> : ";
        if ( (!empty($order->delivery['name'])) && (!empty($order->delivery['street_address'])) ) {
          $table .= osc_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />');
        }
        else if ($order->billing) {
          $table .= osc_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />');
        }
        $table .=  "<br /><br />";
      }
      else {
        $table .=  CUSTOMER_NOT_FOUND; 
      }		
    }
    else {
      $table .= '<h3>' . ALL_CUSTOMERS . '</h3>';
    }
    $table .= '
          </td>
          <td class="smallText" align="right" colspan="3">';
    if (!$print) {
      $table .= '<a href="'.$_SERVER['PHP_SELF'].'?'.querystring_remove('cID').'&print=yes&customer_only=yes&cID='.$customer_id.'" target="print">'; 
      $table .= TEXT_BUTTON_REPORT_PRINT_THIS_CUSTOMER."</a>";
    }
    $table .= '
          </td>
        </tr>
        <tr class="dataTableHeadingRow">
          <td class="dataTableHeadingContent" nowrap width="100%">'.TABLE_PRODUCT_NAME.'</td>
          <td class="dataTableHeadingContent" align="center" nowrap>'.TABLE_ORDER_PURCHASED.'</td>
          <td class="dataTableHeadingContent" align="right" nowrap>'.TABLE_PRODUCT_QUANTITY.'</td>
          <td class="dataTableHeadingContent" align="right" nowrap>'.TABLE_PRODUCT_REVENUE.'</td>
        </tr>';
    // =======================================================================================================================================================================    
    // list of all products bought for manufucter_id and customer_id
    // =======================================================================================================================================================================    
    $man_cust_products_query = osc_db_query(getMySQLraw("p"));

    while ($man_cust_products = osc_db_fetch_array($man_cust_products_query)) {

      $products_model = $man_cust_products['products_model'];
      $products_name = $man_cust_products['products_name'];

      if ($man_cust_products['products_options_values'] != "") {$products_name .= " (" . $man_cust_products['products_options'] . " : " . $man_cust_products['products_options_values'] . ")"; }
              $products_quantity = $man_cust_products['sum_pq'];
              $final_price = $man_cust_products['sum_fp'];
              $table .= '
                <tr class="dataTableRow">
                  <td class="dataTableContent" width="100%">';
            
      if (!$print) {
        $table .= '<a href="' . osc_href_link('categories.php', 'action=new_product_preview&read=only&pID=' . $man_cust_products['products_id'] . '&origin=stats_manufacturers?page=' . $_GET['page']) . '">' . $products_name . '</a>';
      }
      else {
        $table .= $products_name;
      }
      $table .= '
            </td>
            <td class="dataTableContent" align="center">'.strftime(DATE_FORMAT_SHORT, strtotime($man_cust_products['dp'])).'</td>
            <td class="dataTableContent" align="right">'.$products_quantity.'</td>
            <td class="dataTableContent" align="right">'.$currencies->format($final_price).'</td>
          </tr>';
      $total_quantity = $total_quantity + $products_quantity;
      $total_sales = ($total_sales + $final_price);
    }
    $table .= '
          <tr>
            <td class="dataTableTotalRow" colspan="2" align="right">'.ENTRY_TOTAL.'</td>
            <td class="dataTableTotalRow" align="right">'.$total_quantity.'</td>
            <td class="dataTableTotalRow" align="right">'.$currencies->format($total_sales).'</td>
          </tr>
      </table>';
    return $table;
  }
  
  function getShortDate($strDate) {
    return strftime(DATE_FORMAT_SHORT, strtotime($strDate));
  }

