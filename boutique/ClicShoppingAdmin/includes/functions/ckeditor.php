<?php
/*
 * ckeditor.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

/**
 * Outputs a form textarea field with ckeditor
 *
 * @param string $name The name and ID of the textarea field
 * @param string $value The default value for the textarea field
 * @param int $width The width of the textarea field
 * @param int $height The height of the textarea field
 * @param string $parameters Additional parameters for the textarea field
 * @param boolean $override Override the default value with the value found in the GET or POST scope
 * @access public
 */
////

function osc_draw_textarea_ckeditor($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {

    global $_GET, $_POST;

    $field = '<textarea name="' . osc_output_string($name) . '"';
    $height ='750';

    if (osc_not_null($parameters)) $field .= ' ' . $parameters;
    $field .= ' />';
    if ( ($reinsert_value == true) && ( (isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])) ) ) {
      if (isset($_GET[$name]) && is_string($_GET[$name])) {
        $field .= osc_output_string_protected($_GET[$name]);
      } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
        $field .= osc_output_string_protected($_POST[$name]);
      }
    } elseif (osc_not_null($text)) {
      $field .= osc_output_string_protected($text);
    }
    $field .= '</textarea>';
    $field .= '<script type="text/javascript">
        CKEDITOR.replace(\''.osc_output_string($name).'\',
    {
        width : '. $height .',
        toolbar : "Full"
    });
            </script>';

    return $field;

}

/**
 * Create form textarea field with ckeditor for image icon and source only
 *
 * @param string $name The name and ID of the textarea field
 * @access public
 */

  function osc_draw_file_field_image_ckeditor($name, $text) {

    $height ='250';
    $field = '<textarea name="' . osc_output_string($name) . '" /></textarea>';
    $field .= '<script type="text/javascript">
        CKEDITOR.replace(\''.osc_output_string($name).'\',
    {
        width : '. $height .'
    });
            </script>';
    return $field;
  }
