<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Edit Account');
define('HEADING_TITLE', 'My Account Information');

define('MY_ACCOUNT_TITLE', 'My Account');

define('SUCCESS_ACCOUNT_UPDATED', 'Your account has been successfully updated.');

define('ERROR_ADDRESS_MODIFY_COMPANY', 'You are not authorized to modify information of your company. Do not hesitate to contact us to receive more information over this impossibility.');
?>