<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'RSS');
define('HEADING_TITLE', 'RSS');
define('BOX_INFORMATION_RSS','Les dernieres actualites sur '.STORE_NAME.' ');
define('IMAGE_BUTTON_MORE_INFO','En savoir plus');
//define('BOX_INFORMATION_RSS','Les dernières actualités sur '.STORE_NAME.' ');
?>
