<?php
/**
 * session_storage.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'The sessions directory does not exist: ' . session_save_path() . '. Sessions will not work until this directory is created.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'I am not able to write to the sessions directory: ' . session_save_path() . '. Sessions will not work until the right user permissions are set.');
?>
