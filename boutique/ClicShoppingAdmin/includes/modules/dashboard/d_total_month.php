<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class d_total_month {
    var $code = 'd_total_month';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_total_month() {
      $this->title = MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS == 'True');
      }
    }

    function getOutput() {
       global $OSCOM_PDO;
      $days = array();
/*      
      for($i = 0; $i < 30; $i++) {
        $days[date('Y', strtotime('-'. $i .' days'))] = 0;
      }
*/

//year(o.date_purchased) = year(now())
//date_sub(curdate(), interval 360 day)

      $Qorder = $OSCOM_PDO->prepare('select date_format(o.date_purchased, "%Y-%m-%d") as dateday,
                                           sum(ot.value) as total
                                    from :table_orders o,
                                         :table_orders_total ot
                                    where date_sub(now(), interval 365 day) <= o.date_purchased
                                    and o.orders_status = 3
                                    and o.orders_id = ot.orders_id
                                    and ot.class = :class
                                    group by dateday
                                   ');
      $Qorder->bindValue(':class', 'ot_subtotal');
      $Qorder->execute();


      while ($orders = $Qorder->fetch() ) {
        $days[$orders['dateday']] = $total + $orders['total'];
        $total = $days[$orders['dateday']];
      }

      $days = array_reverse($days, true);

      $js_array = '';
      foreach ($days as $date => $total) {
        $js_array .= '[' . (mktime(0, 0, 0, substr($date, 5, 2), substr($date, 8, 2), substr($date, 0, 4))*1000) . ', ' . $total . '],';
      }

      if (!empty($js_array)) {
        $js_array = substr($js_array, 0, -1);
      }

      $chart_label = osc_output_string(MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_CHART_LINK);
      $chart_label_link = osc_href_link('orders.php');
      $chart_title = osc_output_string(MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_TITLE);
      $output = <<<EOD
<div style="float:left; width:50%;">
<div style="text-align: center;">$chart_title</div>
<div id="d_total_month" style="width: 100%; height: 200px;"></div>
<script type="text/javascript">
$(function () {
  var plot30 = [$js_array];
  $.plot($('#d_total_month'), [ {
    label: '$chart_label',
    data: plot30,
    lines: { show: true, fill: true },
    points: { show: true },
    color: '#f7d16e'
  }], {
    xaxis: {
      ticks: 6,
      mode: 'time'
    },
    yaxis: {
      ticks: 5,
      min: 0
    },
    grid: {
      backgroundColor: { colors:  ['#FAFAFA', '#FAFAFA'] }, //gradient ['#d3d3d3', '#fff']
      hoverable: true,
      borderWidth: 1
    },
    legend: {
      labelFormatter: function(label, series) {
        return '<a href="$chart_label_link">' + label + '</a>';
      }
    }
  });
});

function showTooltip(x, y, contents) {
  $('<div id="tooltip">' + contents + '</div>').css( {
    position: 'absolute',
    display: 'none',
    top: y + 5,
    left: x + 5,
    border: '1px solid #fdd',
    padding: '2px',
    backgroundColor: '#fee',
    opacity: 0.80
  }).appendTo('body').fadeIn(200);
}

var monthNames = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];

var previousPoint = null;
$('#d_total_month').bind('plothover', function (event, pos, item) {
  if (item) {
    if (previousPoint != item.datapoint) {
      previousPoint = item.datapoint;

      $('#tooltip').remove();
      var x = item.datapoint[0],
          y = item.datapoint[1],
          xdate = new Date(x);

      showTooltip(item.pageX, item.pageY, y + ' for ' + monthNames[xdate.getMonth()] + '-' + xdate.getDate());
    }
  } else {
    $('#tooltip').remove();
    previousPoint = null;
  }
});
</script>
</div>
EOD;

      return $output;
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS');
    }

    function install() {
       global $OSCOM_PDO;
       
      if ($_SESSION['languages_id'] =='1') {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_SORT_ORDER',
            'configuration_value' => '20',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr� en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      } else {
        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Do you want enable this Module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Do you want enable this Module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Sort Order',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_SORT_ORDER',
            'configuration_value' => '20',
            'configuration_description' => 'Sort order of display. Lowest is displayed first.',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS', 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_SORT_ORDER');
    }
  }
