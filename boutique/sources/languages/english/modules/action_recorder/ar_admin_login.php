<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ACTION_RECORDER_ADMIN_LOGIN_TITLE', 'Administration Tool Login');
  define('MODULE_ACTION_RECORDER_ADMIN_LOGIN_DESCRIPTION', 'Record usage of Administration Tool logins.');
?>
