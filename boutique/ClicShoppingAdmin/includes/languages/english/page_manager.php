<?php
/**
 * page_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version 
*/

// Titre principal
define('HEADING_TITLE', 'Page Manager');
define('HEADING_TITLE_EDITION', 'Edit page');
define('HEADING_TITLE_NEW', 'New page');
define('TABLE_HEADING_PAGE_TYPE','Box type');

// Texte tableau de la liste des pages
define('TABLE_HEADING_PAGES', 'Pages');
define('TABLE_HEADING_TYPE_PAGE', 'Page Type');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_CUSTOMERS_GROUP','Customers group');
define('TABLE_HEADING_SORT_ORDER', 'Sort Order');
define('TABLE_HEADING_ACTION', 'Actions');
define('TEXT_DISPLAY_NUMBER_OF_PAGES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> Pages)');
define('TABLE_HEADING_LINKS_TARGET','Links type');
define('TABLE_HEADING_PAGE_TYPE','Box type');

// Menu des onglets
define('TAB_GENERAL', 'General');
define('TAB_PAGE_LINK', 'Page links');
define('TAB_PAGE_DESCRIPTION', 'Description');
define('TAB_PAGE_META_TAG', 'Meta Tag');


// Nouvelle page
define('TITLE_PAGES_CHOOSE', 'Page type');
define('TEXT_PAGES_CHOOSE', 'Please select the type of page that you wish to insert:');

// Nom de la page
define('TITLE_NAME_PAGE', 'Page title');

// Date
define('TITLE_PAGES_DATE', 'Date');
define('TEXT_PAGES_DATE_START', 'Start date :');
define('TEXT_PAGES_DATE_CLOSED', 'Expires date :');
define('TEXT_DATE_ADDED','Date added :');
define('TEXT_LAST_MODIFIED',  'Last modified :');
define('TEXT_STATUS_CHANGE', 'Status change :');
define('TEXT_PAGE_MANAGER_CUSTOMERS_GROUP','Display customers groups : ');
define('TEXT_START_DATE','Start date : ');
define('TEXT_CLOSE_DATE','Expire Date : ');
define('TEXT_ALL_CUSTOMERS','Normal customers');
define('NORMAL_CUSTOMER','Normal customers');

// Type de la page
define('TITLE_PAGES_TYPE', 'Page type' );
define('TEXT_PAGES_BOX','Box choice to display : ');
define('TEXT_PAGES_TYPE', 'Page type :');
define('PAGE_MANAGER_INTRODUCTION_PAGE', 'Introduction page');
define('PAGE_MANAGER_MAIN_PAGE', 'Main page');
define('PAGE_MANAGER_CONTACT_US', 'Contact Us');
define('PAGE_MANAGER_INFORMATIONS', 'Informations');
define('PAGE_MANAGER_MAIN_BOX','Main Box');
define('PAGE_MANAGER_SECONDARY_BOX','Secondary box');
define('PAGE_MANAGER_NO_APPEAR','Not display inside the box');

// Conditions generales
define('TEXT_PAGES_GENERAL_CONDITIONS','Is it a Page Conditions of Sales ?');
define('PAGE_MANAGER_TEXT_YES','Yes');
define('PAGE_MANAGER_TEXT_NO','No');

// Divers
define('TITLE_DIVERS', 'Others');
define('TEXT_PAGES_TIME', 'Time display for introduction page :');
define('TEXT_PAGES_TIME_SECONDE', '<i>Seconds</i>');
define('TEXT_PAGES_SORT_ORDER', 'Sort Order :');

// Lien sur la page
define('TITLE_LINK', 'Type of link on the page');
define('TEXT_PAGES_INTEXT', 'Link Type :');
define('TEXT_LINKS_SAME_WINDOWS','Same window');
define('TEXT_LINKS_NEW_WINDOWS','New window');
define('TEXT_TARGET_INTERNAL', 'Internal');
define('TEXT_TARGET_EXTERNAL', 'External');
define('TEXT_PAGES_EXTERNAL_LINK', 'External URL of the page:');
define('TEXT_TARGET', 'Link Target:');
define('TEXT_TARGET_SAMEWINDOW', 'Same Window');
define('TEXT_TARGET_NEWWINDOW', 'New Window');

// Description
define('TEXT_PAGES_INFORMATION_DESCRIPTION', 'Page description');

// Boxe information
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this page?');

// seo
define('KEYWORDS_GOOGLE_TREND','Keywords Google trend');
define('ANALYSIS_GOOGLE_TOOL','Analysis Google Tool');
define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Product Meta Data');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Title');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_KEYWORDS', 'Keywords');
define('TEXT_PRODUCTS_TAG_PRODUCT','Products tag');
define('TEXT_PRODUCTS_TAG_BLOG','Tags blog');


define('TITLE_HELP_SUBMIT', 'Note on the search engine optimization management');
define('HELP_SUBMIT', '<li>The system automatically manages the search engine optimization.<br />
<blockquote>It automatically included elements in the following order if the fields are not filled:<br />
Title : the name of your product (ex leather jacket)<br />
Description: The product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Category: Category Product<br />
</blockquote></li>
<li>If you fill in the fields, the tags appear in the following way:<br />
<blockquote>
Title: The title of fields, the name of your product (ex leather jacket)<br />
Description: The field description, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the fields keywords, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
</blockquote></li>
<li><strong>Please do not put too keywords (maximum 10)</strong> and establish consistency between each field to find the words the most important first</li>
');

// Aide
define('TITLE_AIDE_IMAGE', 'Help');
define('TITLE_AIDE_PAGE_MANAGER', 'Note on creation introduction pages');
define('TEXT_PAGES_TYPE_INFORMATION', '<li>Concerning the introduction page, we suggest you to let display <u>10 secondes at least</u>.If you wish an illimited time, let the field empty');
define('TITLE_HELP_DESCRIPTION', 'Note on the use of the wysiwyg');
define('HELP_DESCRIPTION', '<li>If you inserted plugin in Firefox, it can be incompatibilities (ex: adblock more)</li>');


// Message flash
define('SUCCESS_PAGE_INSERTED', 'Success: The page has been inserted.');
define('SUCCESS_PAGE_UPDATED', 'Success: The page has been updated.');
define('SUCCESS_PAGE_REMOVED', 'Success: The page has been removed.');
define('SUCCESS_PAGE_MANAGER_STATUS_UPDATED', 'Success: The status of the page has been updated.');
define('ERROR_PAGE_TITLE_REQUIRED', 'Error: Page title required.');
define('ERROR_PAGE_TITLE_REQUIRED_CURSEUR', '<span class="errorText">Please inform the title of the page.</span>');

// TAB des boutons
define('IMAGE_NEW_PAGE', 'New Page');
define('ICON_EDIT', 'Edit la page');
?>