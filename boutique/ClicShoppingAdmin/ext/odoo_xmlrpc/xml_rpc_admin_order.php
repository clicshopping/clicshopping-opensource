<?php
/*
 * xml_rpc_admin_order.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/


// **********************************
//  search customer_id
// **********************************

  $Qcustomers = $OSCOM_PDO->prepare('select customers_id,
                                            orders_id,
                                            orders_status,
                                            date_purchased,
                                            orders_status_invoice,
                                            orders_date_finished,
                                            odoo_invoice,
                                            delivery_postcode,
                                            billing_postcode,
                                            provider_name_client,
                                            client_computer_ip
                                       from :table_orders
                                       where orders_id = :orders_id
                                     ');
  $Qcustomers->bindValue(':orders_id', (int)$oID);
  $Qcustomers->execute();

  $customers = $Qcustomers->fetch();

  $customers_id = $customers['customers_id'];
  $order_id = $customers['orders_id'];
  $provider_name_client = $customers['provider_name_client'];
  $client_computer_ip = $customers['client_computer_ip'];
  $date_invoice = $customers['date_purchased'];
  $orders_status_invoice = $customers['orders_status_invoice'];
  $ref_webstore = $OSCOM_Date->getDateReferenceShort($date_invoice) . 'S';
  $odoo_invoice = $customers['odoo_invoice'];
  $orders_status = $customers['orders_status'];


  if ($odoo_invoice == 0) {

// **********************************
// Search odoo customer id / partner_id
// **********************************

    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $customers_id, 'res.partner');

// **********************************
// read id order odoo
// **********************************

    $field_list = array('ref',
                        'id'
                      );

    $partner_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
    $partner_id = $partner_id[0][id];

// ********************************************
//    partner_invoice_id
// ********************************************

    $QcountryIdBillingOrder = $OSCOM_PDO->prepare("select address_book_id
                                                   from :table_address_book
                                                   where customers_id = :customers_id
                                                   and  entry_postcode = :entry_postcode
                                                  ");
    $QcountryIdBillingOrder->bindInt(':customers_id', (int)$customers_id );
    $QcountryIdBillingOrder->bindValue('entry_postcode', $billing_postcode );
    $QcountryIdBillingOrder->execute();

    $country_id_billing_order = $QcountryIdBillingOrder->fetch();
    $country_id_billing_order = $country_id_billing_order['address_book_id'];

// **********************************
// Search odoo customer id / partner_invoice_id
// **********************************

    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $country_id_billing_order, 'res.partner');

// **********************************
// read id order odoo partner_invoice_id
// **********************************

    $field_list = array('ref',
                        'id'
                       );

    $partner_invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
    $partner_invoice_id = $partner_invoice_id[0][id];

    if ($partner_invoice_id == '') $partner_invoice_id = $partner_id;

// ********************************************
//    partner_shipping_id
// ********************************************

    $QcountryIdShippingOrder = $OSCOM_PDO->prepare("select address_book_id
                                                     from :table_address_book
                                                     where customers_id = :customers_id
                                                     and  entry_postcode = :entry_postcode
                                                    ");
    $QcountryIdShippingOrder->bindInt(':customers_id', (int)$customers_id );
    $QcountryIdShippingOrder->bindValue(':entry_postcode', $delivery_postcode );
    $QcountryIdShippingOrder->execute();

    $country_id_shipping_order = $QcountryIdShippingOrder->fetch();
    $country_id_shipping_order_address_book_id = $country_id_shipping_order['address_book_id'];

// **********************************
// Search odoo customer id / partner_shipping_id
// **********************************

    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $country_id_shipping_order_address_book_id, 'res.partner');

// **********************************
// read id order odoo partner_invoice_id
// **********************************

    $field_list = array('ref',
                        'id'
                        );

    $partner_shipping_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
    $partner_shipping_id = $partner_shipping_id[0][id];

    if ($partner_shipping_id == '') $partner_shipping_id = $partner_id;


    $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();


// ********************************
// wharehouse
//********************************
//// stock warehouse search id and code concerning ClicShopping Wharehouse
    $ids = $OSCOM_ODOO->odooSearch('name', '=', 'ClicShopping', 'stock.warehouse');
    $field_list = array('id');

    $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');
    $stock_wharehouse_id = $Qstock_wharehouse[0][id];

// **********************************
// Create top order
// **********************************

    if ($orders_status == '1') {
      $clicshopping_order_status = 'instance';
    } elseif ($orders_status == '2') {
      $clicshopping_order_status = 'processing';
    } elseif ($orders_status == '3') {
      $clicshopping_order_status = 'delivered';
    } elseif ($orders_status == '4') {
      $clicshopping_order_status = 'cancelled';
    } else {
      $clicshopping_order_status = 'instance';
    }


// top of invoice
     $values = array ("partner_id" => new xmlrpcval($partner_id, "int"),
                     "partner_invoice_id" => new xmlrpcval($partner_invoice_id, "int"),
                     "partner_shipping_id" => new xmlrpcval($partner_shipping_id, "int"),
                     "company_id" => new xmlrpcval($company_id, "int"),
                     "origin" => new xmlrpcval("WebStore - order : " . $ref_webstore . $oID, "string"),
                     "client_order_ref" => new xmlrpcval("WebStore - order : " . $ref_webstore . $oID, "string"),
                     "warehouse_id" => new xmlrpcval($stock_wharehouse_id, "int"),
                     "clicshopping_order_id" => new xmlrpcval($order_id, "int"),
                     "clicshopping_order_reference" => new xmlrpcval($ref_webstore . $oID, "string"),
                     "clicshopping_order_customer_id" => new xmlrpcval($customers_id, "string"),
                     "clicshopping_order_provider_name_client" => new xmlrpcval($provider_name_client, "string"),
                     "clicshopping_order_client_computer_ip" => new xmlrpcval($client_computer_ip, "string"),
                     "clicshopping_order_customer_comments" => new xmlrpcval($order->info['comments'], "string"),
                     "clicshopping_order_status" => new xmlrpcval($clicshopping_order_status, "string"),
                    );
     $OSCOM_ODOO->createOdoo($values, "sale.order");

//******************************************
// research order_id by origin order
//***************************************

    $ids = $OSCOM_ODOO->odooSearch('origin', '=', "WebStore - order : " . $ref_webstore . $oID, 'sale.order');

// **********************************
// read id order odoo
// **********************************

    $field_list = array('id',
                        'name',
                        );

    $invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'sale.order');
    $invoice_id = $invoice_id[0][id];


// **********************************
// write the lines concerning the products
// **********************************

// search the amount of invoice of $order_id
      $QshippingAmount = $OSCOM_PDO->prepare('select title,
                                                     value,
                                                     class
                                               from :table_orders_total
                                               where orders_id = :orders_id
                                               and class = :class
                                             ');
      $QshippingAmount->bindInt(':orders_id', (int)$oID);
      $QshippingAmount->bindValue(':class', 'ot_shipping');
      $QshippingAmount->execute();

      $Qshipping_amount = $QshippingAmount->fetch();

      $shipping_title = html_entity_decode($Qshipping_amount['title']);
      $shipping_amount = $Qshipping_amount['value'];

//******************************************************
// coupon line
//*****************************************************

//search discount coupon
      $QDiscountCouponAmount = $OSCOM_PDO->prepare('select title,
                                                           value,
                                                           class
                                                     from :table_orders_total
                                                     where orders_id = :orders_id
                                                     and class = :class
                                                   ');
      $QDiscountCouponAmount->bindInt(':orders_id', (int)$oID);
      $QDiscountCouponAmount->bindValue(':class', 'ot_discount_coupon');
      $QDiscountCouponAmount->execute();

      $Qdiscount_coupon_amount = $QDiscountCouponAmount->fetch();

      $discount_coupon_title = html_entity_decode($Qdiscount_coupon_amount['title']);

// apply if the option - is selectied in discount_coupon
      if ($Qdiscount_coupon_amount['value'] < 0) {
        $discount_coupon_amount = $Qdiscount_coupon_amount['value'];
      } else {
        $discount_coupon_amount = $Qdiscount_coupon_amount['value'] * (-1);
      }

// **********************************
// Search odoo id discount account.account
// **********************************
      $QCustomerDiscountAmount = $OSCOM_PDO->prepare('select title,
                                                             value,
                                                             class
                                                       from :table_orders_total
                                                       where orders_id = :orders_id
                                                       and class = :class
                                                     ');
      $QCustomerDiscountAmount->bindInt(':orders_id', (int)$oID);
      $QCustomerDiscountAmount->bindValue(':class', 'ot_customer_discount');
      $QCustomerDiscountAmount->execute();

      $Qcustomer_discount_amount = $QCustomerDiscountAmount->fetch();

      $customer_discount_title = html_entity_decode($Qcustomer_discount_amount['title']);
      $customer_discount_amount = $Qcustomer_discount_amount['value'] * (-1);


      require_once('includes/classes/order.php');
      $order = new order($oID);
// count number of product
      $count_products = sizeof($order->products);

      for ($o=0, $n=$count_products; $o<$n; $o++) {

//******************************************
// research invoice_id by origin invoice
//***************************************
// ====> mettre en relation avec la B2B
        $ids = $OSCOM_ODOO->odooSearch('default_code', '=', $order->products[$o]['model'], 'product.template');

// **********************************
// read id products odoo
// **********************************

        $field_list = array('default_code',
                            'id'
                            );

        $Qodoo_products_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
        $odoo_products_id = $Qodoo_products_id[0][id];

// **********************************
// name of products and options description
// doesn't take more than one options
// text limited  by odoo
// **********************************

        $products_name = $order->products[$o]['name'];

        if (isset($order->products[$o]['attributes']) && (sizeof($order->products[$o]['attributes']) > 0)) {
          for ($j = 0, $k = sizeof($order->products[$o]['attributes']); $j < $k; $j++) {
// attributes reference
            if ($order->products[$o]['attributes'][$j]['reference'] != '' or $order->products[$o]['attributes'][$j]['reference'] !='null') {
              $attributes_reference = $order->products[$o]['attributes'][$j]['reference'] . ' - ';
            }
            $products_attributes = ' ' . $attributes_reference  . $order->products[$o]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
            $products_attributes = osc_strip_html_tags($products_attributes);
          }
        }

        if ($products_attributes != '') {
          $products_attributes = ' / ' . $products_attributes;
        }

        $products_name_odoo = $products_name . $products_attributes;

// **********************************
// tax
// **********************************

// select the good tax in function the products
        $QProductsTax = $OSCOM_PDO->prepare('select products_tax
                                             from :table_orders_products
                                             where orders_id = :orders_id
                                             and products_id = :products_id
                                           ');
        $QProductsTax->bindInt(':orders_id', (int)$oID);
        $QProductsTax->bindValue(':products_id', $order->products[$o]['products_id']);
        $QProductsTax->execute();

        $Qproducts_tax = $QProductsTax->fetch();


// select the tax code_tax_odoo in function the products tax
        $QProductsCodeTax = $OSCOM_PDO->prepare('select code_tax_odoo
                                                 from :table_tax_rates
                                                 where tax_rate = :tax_rate
                                               ');

        $QProductsCodeTax->bindValue(':tax_rate', $Qproducts_tax['products_tax']);
        $QProductsCodeTax->execute();

        $Qproducts_code_tax = $QProductsCodeTax->fetch();
        $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];

//******************************************
// research id tax by description
//***************************************

        if($products_code_tax != null) {
          $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
          $odoo_products_tax_id = $ids;
          $odoo_products_tax_id = $odoo_products_tax_id[0];

          if (!empty($odoo_products_tax_id)) {

            $type_tax_string = 'array';

            $tax = array(new xmlrpcval(
                                        array(
                                                new xmlrpcval(6, "int"),// 6 : id link
                                                new xmlrpcval(0, "int"),
                                                new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                              ), "array"
                                      )
                        );
          } else  {
            $tax = 0;
            $type_tax_string = 'int';
          }
        } else {
          $tax = 0;
          $type_tax_string = 'int';
        }

// **********************************
// Write a new line concerning the invoice
// **********************************

        $values = array (
                          "order_id" => new xmlrpcval($invoice_id, "int"),
                          "product_id" => new xmlrpcval($odoo_products_id, "int"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "name" => new xmlrpcval($products_name_odoo, "string"),
                          "price_unit" => new xmlrpcval($order->products[$o]['final_price'],"double"),
                          "product_uom_qty" => new xmlrpcval($order->products[$o]['qty'], 'double'),
                          "tax_id" =>  new xmlrpcval($tax, $type_tax_string),
                        );

        $OSCOM_ODOO->createOdoo($values, "sale.order.line");

      } //end for


// **********************************
// Write a new line concerning the shipping by the service line available in odoo
// **********************************

      if ($shipping_amount != 0 ) {

        if (DISPLAY_DOUBLE_TAXE =='true') {

// select the good tax in function the products
            $QProductsTax = $OSCOM_PDO->prepare('select products_tax
                                                   from :table_orders_products
                                                   where orders_id = :orders_id
                                                   and products_id = :products_id
                                                 ');
            $QProductsTax->bindInt(':orders_id', (int)$oID);
            $QProductsTax->bindValue(':products_id', $order->products[$o]['products_id']);
            $QProductsTax->execute();

            $Qproducts_tax = $QProductsTax->fetch();

            if  ( $Qproducts_tax['products_tax'] != '' || $Qproducts_tax['products_tax'] != 0) {
  // select the tax code_tax_odoo in function the products tax
              $QProductsCodeTax = $OSCOM_PDO->prepare('select code_tax_odoo
                                                         from :table_tax_rates
                                                         where tax_rate = :tax_rate
                                                       ');

              $QProductsCodeTax->bindValue(':tax_rate', $Qproducts_tax['products_tax']);

              $QProductsCodeTax->execute();

              $Qproducts_code_tax = $QProductsCodeTax->fetch();
              $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];


              if($products_code_tax != null) {

                $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
                $odoo_products_tax_id = $ids;
                $odoo_products_tax_id = $odoo_products_tax_id[0];

                if (!empty($odoo_products_tax_id)) {

                  $type_tax_string = 'array';

                  $tax = array(new xmlrpcval(
                                              array(
                                                      new xmlrpcval(6, "int"),// 6 : id link
                                                      new xmlrpcval(0, "int"),
                                                      new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                    ), "array"
                                            )
                                );
                } else  {
                  $tax = 0;
                  $type_tax_string = 'int';
                }
              } else {
                $tax = 0;
                $type_tax_string = 'int';
              }
            }
        } else {
          $tax = 0;
          $type_tax_string = 'int';
        }

            $values = array (
                            "order_id" => new xmlrpcval($invoice_id, "int"),
                            "company_id" => new xmlrpcval($company_id, "int"),
                            "product_id" => new xmlrpcval(0, "int"),
                            "name" => new xmlrpcval($shipping_title, "string"),
                            "price_unit" => new xmlrpcval($shipping_amount,"double"),
                            "product_uom_qty" => new xmlrpcval(1, 'int'),
                            "tax_id" =>  new xmlrpcval($tax, $type_tax_string)
                          );

            $OSCOM_ODOO->createOdoo($values, "sale.order.line");

      }

// **********************************
// Write a new line concerning the customer discount  by the service line available in odoo
// **********************************

      if ($customer_discount_amount != 0 ) {
        $values = array (
                          "order_id" => new xmlrpcval($invoice_id, "int"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "product_id" => new xmlrpcval(0, "int"),
                          "name" => new xmlrpcval($customer_discount_title, "string"),
                          "price_unit" => new xmlrpcval($customer_discount_amount,"double"),
                          "product_uom_qty" => new xmlrpcval(1, 'int'),
                          "tax_id" =>  new xmlrpcval(0, 'int'),
                        );
        $OSCOM_ODOO->createOdoo($values, "sale.order.line");
      }


// **********************************
// Write a new line concerning the discount coupon by the service line available in odoo
// **********************************

      if ($discount_coupon_amount != 0 ) {
        $values = array (
                          "order_id" => new xmlrpcval($invoice_id, "int"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "product_id" => new xmlrpcval(0, "int"),
                          "name" => new xmlrpcval($discount_coupon_title, "string"),
                          "price_unit" => new xmlrpcval($discount_coupon_amount,"double"),
                          "product_uom_qty" => new xmlrpcval(1, 'int'),
                          "tax_id" =>  new xmlrpcval(0, 'int'),
                        );

        $OSCOM_ODOO->createOdoo($values, "sale.order.line");
      }


    if ($invoice_id != '') {
// Update ClicShopping
      $Qupdate = $OSCOM_PDO->prepare('update :table_orders
                                      set odoo_invoice = :odoo_invoice
                                      where orders_id = :orders_id
                                    ');
      $Qupdate->bindInt(':orders_id', (int)$oID);
      $Qupdate->bindValue(':odoo_invoice', '1');
      $Qupdate->execute();
    }

      $OSCOM_ODOO->workflowOdoo('sale.order', 'order_confirm',  $invoice_id);
  } // end $odoo_invoice
?>