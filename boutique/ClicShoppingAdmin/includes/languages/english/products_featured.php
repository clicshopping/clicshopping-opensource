<?php
/**
 * products_featured.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Featured products');

define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Products price');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_PRODUCTS_GROUP','Customer group');
define('TABLE_HEADING_EXPIRES_DATE','Expiry date');
define('TABLE_HEADING_ARCHIVE','Archives products');
define('TABLE_HEADING_SCHEDULED_DATE','Start date');
define('TABLE_HEADING_MODEL','Models');

define('TEXT_PRODUCTS_FEATURED_PRODUCT', 'Product:');
define('TEXT_PRODUCTS_FEATURED_SPECIAL_PRICE', 'Favorites price TAX exc.:');
define('TEXT_PRODUCTS_FEATURED_EXPIRES_DATE', 'Expiry gate:');
define('TEXT_PRODUCTS_FEATURED_GROUPS', 'Customer group:');
define('TEXT_PRODUCTS_FEATURED_SCHEDULED_DATE','Start date');

define('TEXT_INFO_DATE_ADDED', 'Date added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last modified:');
define('TEXT_INFO_NEW_PRICE', 'New price:');
define('TEXT_INFO_ORIGINAL_PRICE', 'Original price:');
define('TEXT_INFO_PERCENTAGE', 'Percentage:');
define('TEXT_INFO_EXPIRES_DATE', 'Expires at:');
define('TEXT_INFO_STATUS_CHANGE', 'Status change:');

define('TEXT_NEW_PRODUCTS_FEATURED_TWITTER','Our featured products on  '. STORE_NAME  .' ! ' );
define('TEXT_PRODUCTS_FEATURED_TWITTER','Do you wish publish this special on Twitter ? ');
define('TEXT_YES','yes');
define('TEXT_NO','No');
define('TEXT_STATUS','Product status');

define('TEXT_INFO_HEADING_DELETE_PRODUCTS_FEATURED', 'Delete featured products');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete the featured products products price?');

define('TITLE_PRODUCTS_FEATURED_GENERAL', 'Information on the product');
define('TITLE_PRODUCTS_FEATURED_GROUPE', 'Price and customer Group');
define('TITLE_PRODUCTS_FEATURED_DATE', 'Display date');
define('TITLE_AIDE_PRODUCTS_FEATURED_PRICE', 'Favorites Notes');

define('TEXT_AIDE_PRODUCTS_FEATURED_PRICE', '<ul><li>Leave the expiry date empty for no expiration.</li>
<li>The related products are automatically updated (the store catalog manages this element), it is unnecessary to change the status of the related products after registration.</li>
<li>Leave the expiry date empty for no expiration.</li><li>The start date has more rights that the expire date.</li>
<li>If you wish initialize the dates, please change the status on the main page.</li>
<li>The twitter publication information is only allow on the product creation (with a status on it and a normal customer group). If your store name + URL of your product is more than 140 characters, the promotion will not be published (strong constraint)</li>
<li>The featured products are automatically updated (the store catalog manages this element), it is unnecessary to change the status of the featured products after registration.</li>
</ul>');

