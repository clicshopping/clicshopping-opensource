<?php
/*
 * object_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  class objectInfo {

// class constructor
    function objectInfo($object_array) {
      foreach ($object_array as $key => $value) {
        $this->$key = osc_db_prepare_input($value);
      }
    }
  }

