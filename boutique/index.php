<?php
/*
 * index.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

// the following cPath references come from application_top.php
  $category_depth = 'top';

  if (isset($cPath) && osc_not_null($cPath)) {

     $Qcheck = $OSCOM_PDO->prepare('select products_id
                                    from :table_products_to_categories
                                    where categories_id = :categories_id
                                    limit 1
                                   ');
    $Qcheck->bindInt(':categories_id', (int)$current_category_id);
    $Qcheck->execute();

    if ($Qcheck->fetch() !== false) {

      $category_depth = 'products'; // display products

    } else {

      $Qcheck = $OSCOM_PDO->prepare('select categories_id
                                    from :table_categories
                                    where parent_id = :parent_id
                                   ');
      $Qcheck->bindInt(':parent_id', (int)$current_category_id);
      $Qcheck->execute();

      if ($Qcheck->fetch() !== false) {
        $category_depth = 'nested'; // navigate through the categories
      } else {
        $category_depth = 'products'; // category has no products, but display the 'no products' message
      }
    }
  }

// definie la langues et les variables particulieres a lire
  require($OSCOM_Template->GeTemplatetLanguageFiles('index'));

//###########################################################################################################
//                                        PAGE CATEGORIES  1er niveau                                       
//###########################################################################################################

  if ($category_depth == 'nested') {

    require($OSCOM_Template->getTemplateFiles('index_categories'));

  } elseif ($category_depth == 'products' || (isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id']))) {

//###########################################################################################################
//                                        PAGE CATEGORIES  2EME niveau                                       
//###########################################################################################################

// create column list
    $define_list = array('PRODUCT_LIST_MODEL' => PRODUCT_LIST_MODEL,
                         'PRODUCT_LIST_NAME' => PRODUCT_LIST_NAME,
                         'PRODUCT_LIST_MANUFACTURER' => PRODUCT_LIST_MANUFACTURER,
                         'PRODUCT_LIST_PRICE' => PRODUCT_LIST_PRICE,
                         'PRODUCT_LIST_QUANTITY' => PRODUCT_LIST_QUANTITY,
                         'PRODUCT_LIST_WEIGHT' => PRODUCT_LIST_WEIGHT,
                         'PRODUCT_LIST_IMAGE' => PRODUCT_LIST_IMAGE,
                         'PRODUCT_LIST_DATE' => PRODUCT_LIST_DATE,
                         );

    asort($define_list);

    $column_list = array();
    foreach($define_list as $key => $value) {
      if ($value > 0) $column_list[] = $key;
    }

    $select_column_list = '';

    for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
      switch ($column_list[$i]) {
        case 'PRODUCT_LIST_MODEL':
          $select_column_list .= 'p.products_model, ';
          break;
        case 'PRODUCT_LIST_NAME':
          $select_column_list .= 'pd.products_name, pd.products_description as description, ';
          break;
        case 'PRODUCT_LIST_MANUFACTURER':
          $select_column_list .= 'm.manufacturers_name, ';
          break;
        case 'PRODUCT_LIST_QUANTITY':
          $select_column_list .= 'p.products_quantity, ';
          break;
        case 'PRODUCT_LIST_IMAGE':
          $select_column_list .= 'p.products_image_zoom, p.products_image, ';
          break;
        case 'PRODUCT_LIST_WEIGHT':
          $select_column_list .= 'p.products_weight, ';
          break;
        case 'PRODUCT_LIST_DATE':
          $select_column_list = 'p.products_date_added,';
          break;
      }
    }

// ########## Adedd B2B ##########
    if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B
      if (isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id'])) {
        if (isset($_GET['filter_id']) && osc_not_null($_GET['filter_id'])) {
          // Affichage des produits en mode B2B sur la selection d'une marque depuis la boxe manufacturer avec filtrage de la categorie
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                         from products p left join specials s on p.products_id = s.products_id left join products_groups g on p.products_id = g.products_id,
                              products_description pd,
                              manufacturers  m,
                              products_to_categories p2c
                         where p.products_status = '1'
                         and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "'
                         and g.products_group_view = '1'
                         and p.manufacturers_id = m.manufacturers_id
                         and m.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'
                         and p.products_id = p2c.products_id
                         and pd.products_id = p2c.products_id
                         and p.products_archive = '0'
                         and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                         and p2c.categories_id = '" . (int)$_GET['filter_id'] . "'
                         and m.manufacturers_status = 0
                        ";
        } else {
          // Affichage des produits en mode B2B sur la selection d'une marque depuis la boxe manufacturer
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                           from products p left join specials s on p.products_id = s.products_id left join products_groups g on p.products_id = g.products_id,
                                products_description pd,
                                manufacturers  m
                           where p.products_status = '1'
                           and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "'
                           and g.products_group_view = '1'
                           and pd.products_id = p.products_id
                           and p.products_archive = '0'
                           and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                           and p.manufacturers_id = m.manufacturers_id
                           and m.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'
                           and m.manufacturers_status = 0
                        ";
        }
      } else {
        if (isset($_GET['filter_id']) && osc_not_null($_GET['filter_id'])) {
          // Affichage general en mode B2B de la liste des produits d'une categorie avec un filtrage des Marques
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                          from products p left join specials s on p.products_id = s.products_id left join products_groups g on p.products_id = g.products_id,
                               products_description pd,
                               manufacturers  m,
                               products_to_categories p2c
                          where p.products_status = '1'
                          and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "'
                          and g.products_group_view = '1'
                          and p.manufacturers_id = m.manufacturers_id
                          and m.manufacturers_id = '" . (int)$_GET['filter_id'] . "'
                          and p.products_id = p2c.products_id
                          and pd.products_id = p2c.products_id
                          and p.products_archive = '0'
                          and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                          and p2c.categories_id = '" . (int)$current_category_id . "'
                          and m.manufacturers_status = 0
                         ";
        } else {
// Affichage general en mode B2B de la liste des produits d'une categorie avec toutes les Marques
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                          from products_description pd,
                               products p left join manufacturers  m on p.manufacturers_id = m.manufacturers_id left join specials s on p.products_id = s.products_id left join products_groups g on p.products_id = g.products_id,
                               products_to_categories p2c
                          where p.products_status = '1'
                          and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "'
                          and g.products_group_view = '1'
                          and p.products_id = p2c.products_id
                          and pd.products_id = p2c.products_id
                          and p.products_archive = '0'
                          and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                          and p2c.categories_id = '" . (int)$current_category_id . "'
                         ";
        }
      }

// ***************************
// Clients Grand Public
// ***************************
    } else {
      if (isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id'])) {
        if (isset($_GET['filter_id']) && osc_not_null($_GET['filter_id'])) {

          // Affichage des produits sur la selection d'une marque depuis la boxe manufacturer avec filtrage de la categorie
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                          from products p left join specials s on p.products_id = s.products_id,
                               products_description pd,
                               manufacturers  m,
                               products_to_categories p2c
                          where p.products_status = '1'
                          and p.products_view = '1'
                          and p.manufacturers_id = m.manufacturers_id
                          and m.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'
                          and p.products_id = p2c.products_id
                          and pd.products_id = p2c.products_id
                          and p.products_archive = '0'
                          and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                          and p2c.categories_id = '" . (int)$_GET['filter_id'] . "'
                          and m.manufacturers_status = 0

                         ";

        } else {

// Affichage des produits sur la selection d'une marque depuis la boxe manufacturer
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                          from products p left join specials s on p.products_id = s.products_id,
                               products_description pd,
                               manufacturers  m
                          where p.products_status = '1'
                          and p.products_view = '1'
                          and pd.products_id = p.products_id
                          and p.products_archive = '0'
                          and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                          and p.manufacturers_id = m.manufacturers_id
                          and m.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'
                          and m.manufacturers_status = 0
                         ";

        }
      } else {
        if (isset($_GET['filter_id']) && osc_not_null($_GET['filter_id'])) {
// Affichage general de la liste des produits d'une categorie avec un filtrage des Marques
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                          from products p left join specials s on p.products_id = s.products_id,
                               products_description pd,
                               manufacturers  m,
                               products_to_categories p2c
                          where p.products_status = '1'
                          and p.products_view = '1'
                          and p.manufacturers_id = m.manufacturers_id
                          and m.manufacturers_id = '" . (int)$_GET['filter_id'] . "'
                          and p.products_id = p2c.products_id
                          and pd.products_id = p2c.products_id
                          and p.products_archive = '0'
                          and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                          and p2c.categories_id = '" . (int)$current_category_id . "'
                          and m.manufacturers_status = 0
                         ";
        } else {
// Affichage general de la liste des produits d'une categorie avec tout les Marques
          $listing_sql = "select DISTINCTROW " . $select_column_list . " p.products_id
                          from products_description pd,
                               products p left join manufacturers  m on p.manufacturers_id = m.manufacturers_id left join specials s on p.products_id = s.products_id,
                               products_to_categories p2c
                          where p.products_status = '1'
                          and p.products_view = '1'
                          and p.products_id = p2c.products_id
                          and pd.products_id = p2c.products_id
                          and p.products_archive = '0'
                          and pd.language_id = '" . (int)$_SESSION['languages_id'] . "'
                          and p2c.categories_id = '" . (int)$current_category_id . "'
                         ";
        }
      }
    }
// ####### END B2B #######

    if ( (!isset($_GET['sort'])) || (!preg_match('/^[1-8][ad]$/', $_GET['sort'])) || (substr($_GET['sort'], 0, 1) > sizeof($column_list)) ) {
      for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
        if ($column_list[$i] == 'PRODUCT_LIST_NAME') {
          $_GET['sort'] = $i+1 . 'a';
          $listing_sql .= " order by p.products_sort_order, pd.products_name";
          break;
        }
      }
    } else {
      $sort_col = substr($_GET['sort'], 0 , 1);
      $sort_order = substr($_GET['sort'], 1);

      switch ($column_list[$sort_col-1]) {
        case 'PRODUCT_LIST_MODEL':
          $listing_sql .= " order by p.products_model " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
        case 'PRODUCT_LIST_NAME':
          $listing_sql .= " order by pd.products_name " . ($sort_order == 'd' ? 'desc' : '');
        break;
        case 'PRODUCT_LIST_MANUFACTURER':
          $listing_sql .= " order by m.manufacturers_name " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
        case 'PRODUCT_LIST_QUANTITY':
          $listing_sql .= " order by p.products_quantity " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
        case 'PRODUCT_LIST_IMAGE':
          $listing_sql .= " order by pd.products_name";
        break;
        case 'PRODUCT_LIST_WEIGHT':
          $listing_sql .= " order by p.products_weight " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
        case 'PRODUCT_LIST_PRICE':
          $listing_sql .= " order by products_price " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
          break;
        case 'PRODUCT_LIST_DATE':
          $listing_sql .= " order by products_date_added " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
          break;
      }
    }

//########################################################################################################
//                                               Manufacturer
//########################################################################################################

// display de manufactuer description
    $catname = HEADING_TITLE;

    if (isset($_GET['manufacturers_id']) && !empty($_GET['manufacturers_id'])) {

      $manufacturer_description = osc_get_manufacturers_description($_GET['manufacturers_id']);

      $QmanufacturerImage = $OSCOM_PDO->prepare('select manufacturers_image,
                                                       manufacturers_name as catname
                                                from :table_manufacturers
                                                where manufacturers_id = :manufacturers_id
                                                and manufacturers_status = 0
                                               ');
      $QmanufacturerImage->bindInt(':manufacturers_id',  (int)$_GET['manufacturers_id'] );
      $QmanufacturerImage->execute();

      $catname = $QmanufacturerImage->value('catname');

    } elseif ($current_category_id) {
      
      $QcategoriesImage = $OSCOM_PDO->prepare('select  c.categories_image,
                                                       cd.categories_name as catname,
                                                       cd.categories_description
                                               from :table_categories c,
                                                    :table_categories_description cd
                                               where c.categories_id = :categories_id
                                               and c.categories_id = cd.categories_id
                                               and cd.language_id = :language_id
                                               and c.virtual_categories = :virtual_categories
                                              ');
      $QcategoriesImage->bindInt(':categories_id', (int)$current_category_id );
      $QcategoriesImage->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QcategoriesImage->bindInt(':virtual_categories', '0');
      $QcategoriesImage->execute();

      $catname = $QcategoriesImage->value('catname');

    }

//########################################################################################################
//                                               LISTING PRODUITS                                            
//########################################################################################################

    $products_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');

    require($OSCOM_Template->getTemplateFiles('index_listing'));

// #########################################################################################################
//                                                PAGE D'ACCEUIL                                             
// #########################################################################################################
  } else {
    $products_short_description = (int)PRODUCTS_NEWS_INDEX_SHORT_DESCRIPTION;

// cache
    if ((USE_CACHE == 'true') && empty($SID)) {
      if (MODULE_FRONT_PAGE_UPCOMING_PRODUCTS_STATUS == 'True') {
        echo osc_cache_upcoming_products_box(3600);
      }
    }

    require($OSCOM_Template->getTemplateFiles('index_default'));
  }
