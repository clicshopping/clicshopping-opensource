<?php
/**
 * administrators.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Administration');

define('TABLE_HEADING_ADMINISTRATORS', 'Pseudonymes');
define('TABLE_HEADING_USER','Utilisateurs');
define('TABLE_HEADING_HTPASSWD', 'Sécurisé par htpasswd');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_NAME','Nom :');
define('TEXT_INFO_FIRSTNAME','Prénom :');
define('TEXT_INFO_ADMIN','Est ce un administrateur ?  : ');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez entrer un nouvel administrateur');
define('TEXT_INFO_EDIT_INTRO', 'Veuillez réaliser les changements nécessaires');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de supprimer cet administrateur ?');
define('TEXT_INFO_HEADING_NEW_ADMINISTRATOR', 'Nouvel Administrateur');
define('TEXT_INFO_USERNAME', 'Pseudonyme (votre email):');
define('TEXT_INFO_NEW_PASSWORD', 'Nouveau mot de passe :');
define('TEXT_INFO_PASSWORD', 'Mot de passe :');
define('TEXT_INFO_PROTECT_WITH_HTPASSWD', 'Protection avec htaccess/htpasswd');
define('ICON_EDIT_USER','Editer l\'utilisateur');
define('ERROR_ADMINISTRATOR_EXISTS', 'Erreur : L\administrateur existe déjà.');
define('HTPASSWD_INFO', '<strong>Protection additionnelle avec htaccess/htpasswd</strong><p>Originellement, ClicShopnng n\'est pas protégé via le système  htaccess/htpasswd.</p><p>En mettant en place ce système, les mots de passe et le nom d\utilisateur seront enregistrés dans un fichier.</p><p><strong>Veuillez noter</strong>, Si vous avez des problèmes avc ce système de sécurisation, vous pouvez l\'annuler en éditant ces fichiers:</p><p><u><strong>1. Etape 1:</strong></u><br /><br />' . DIR_FS_ADMIN . '.htaccess</p><p>Supprimer les lignes existantes:</p><p><i>%s</i></p><p><u><strong>Etape 2. Supprimer ce fichier:</strong></u><br /><br />' . DIR_FS_ADMIN . '.htpasswd_clicshopping</p>');
define('HTPASSWD_SECURED', '<strong>Protection additionnelle avec htaccess/htpasswd</strong>');
define('HTPASSWD_PERMISSIONS', '<strong>Protection addtionnelle avec htaccess/htpasswd</strong><p>Clicshopping n\est pas actuellement protégé avec le sytème htaccess/htpasswd .</p><p>Les fichiers suivants doivent etre mis en écriture sur le serveur web pour un fonctionnement optimle:</p><ul><li>' . DIR_FS_ADMIN . '.htaccess</li><li>' . DIR_FS_ADMIN . '.htpasswd_clicshopping</li></ul><p>Recharger la page pour voir les modifications.</p>');
define('TEXT_NO_SECURED','Non sécurisé');
define('TEXT_SECURED','Sécurisé');
?>