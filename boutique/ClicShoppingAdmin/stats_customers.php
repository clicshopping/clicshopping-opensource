<?php
/*
 * stats_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require('includes/application_top.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();

 if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN- MAX_DISPLAY_SEARCH_RESULTS_ADMIN;
  $customers_query_raw = "select c.customers_firstname, 
                                 c.customers_lastname, 
                                 c.customers_group_id,
                                 sum(op.products_quantity * op.final_price) as ordersum 
                         from customers c,
                              orders_products op,
                              orders o
                        where c.customers_id = o.customers_id 
                        and o.orders_id = op.orders_id 
                        group by c.customers_firstname, 
                                 c.customers_lastname 
                        order by ordersum DESC
                       ";
  $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $customers_query_raw, $customers_query_numrows);

  $QcustomersNumrow = $OSCOM_PDO->prepare('select customers_id
                                           from :table_orders
                                           group by customers_id
                                        ');
  $QcustomersNumrow->execute();

  $customers_query_numrows = $QcustomersNumrow->rowCount();

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_customers.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
  <span style="text-align:center">
    <?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?><br />
    <?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
  </span>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>


<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NUMBER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TOTAL_PURCHASED; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center"width="20"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php

  $rows = 0;
  $customers_query = osc_db_query($customers_query_raw);

  while ($customers = osc_db_fetch_array($customers_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href='<?php echo osc_href_link('customers.php', 'search=' . $customers['customers_lastname']); ?>'">
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('customers.php', 'search=' . $customers['customers_lastname']) . '">' . $customers['customers_firstname'] . ' ' . $customers['customers_lastname'] . '</a>'; ?></td>
                <td class="dataTableContent" align="right"><?php echo $currencies->format($customers['ordersum']); ?>&nbsp;</td>
<?php
    if ($customers['customers_group_id'] > 0 ) {
?>
                <td class="dataTableContent" align="right"><?php  echo '<a href="' . osc_href_link('customers.php',  'search=' . $customers['customers_lastname']) . '">' . osc_image(DIR_WS_ICONS . 'client_b2b.gif', ICON_EDIT_CUSTOMER) . '</a>'; ?></td>
<?php
    } else {
?>
                <td class="dataTableContent" align="right"><?php  echo '<a href="' . osc_href_link('customers.php',  'search=' . $customers['customers_lastname']) . '">' . osc_image(DIR_WS_ICONS . 'client_b2c.gif', ICON_EDIT_CUSTOMER) . '</a>'; ?></td>
<?php
    }
?>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
