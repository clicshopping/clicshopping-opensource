<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
  require('includes/application_top.php');

  require('includes/header.php');
?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
          <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_products_purchased.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="20"></td> 
                <td width="50"></td>             
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NUMBER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_UNSOLD; ?>&nbsp;</td>
              </tr>
<?php
  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN - MAX_DISPLAY_SEARCH_RESULTS_ADMIN;

$products_query_raw = "select p.products_id, 
                              p.products_quantity, 
                              p.products_model, 
                              pd.products_name 
                       from products p LEFT JOIN orders_products op ON p.products_id = op.products_id,
                            products_description pd
                       where op.products_id IS NULL 
                       and p.products_id = pd.products_id 
                       and p.products_id = op.products_id 
                       order by p.products_model, pd.products_name
                     ";

  $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_query_raw, $products_query_numrows);

  $rows = 0;
  $products_query = osc_db_query($products_query_raw);
  while ($products = osc_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                <td class="dataTableContent" width="50"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id'] . '&origin=' . 'stats_products_viewed.php' . '?page=' . $_GET['page']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id'] . '&origin=' . 'stats_products_purchased.php' . '?page=' . $_GET['page']) . '">' . $products['products_name'] . '</a>'; ?></td>
                <td class="dataTableContent"><?php echo $products['products_name']; ?>&nbsp;</td>
                <td class="dataTableContent"><?php echo $products['products_quantity']; ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>