<?php
/*
 * create_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

//Redirection vers la page de creation de compte professionnel si le compte particulier est inactif
  if ((MODE_MANAGEMENT_B2C_B2B == 'B2B') && (MODE_B2B_B2C == 'true')) {
    osc_redirect(osc_href_link('create_account_pro.php', '', 'SSL'));
  }
  
  require($OSCOM_Template->GeTemplatetLanguageFiles('create_account'));

  $process = false;
  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $process = true;

    $firstname = isset($_POST['firstname']) ? trim($_POST['firstname']) : null;
    $lastname = isset($_POST['lastname']) ? trim($_POST['lastname']) : null;
    if (ACCOUNT_DOB == 'true') $dob = isset($_POST['dob']) ? trim($_POST['dob']) : null;
    $email_address = isset($_POST['email_address']) ? trim($_POST['email_address']) : null;
    $email_address_confirm = isset($_POST['email_address_confirm']) ? trim($_POST['email_address_confirm']) : null;

    $newsletter = isset($_POST['newsletter']) ? trim($_POST['newsletter']) : null;
    $password = isset($_POST['password']) ? trim($_POST['password']) : null;
    $confirmation = isset($_POST['confirmation']) ? trim($_POST['confirmation']) : null;

    $error = false;

// Clients B2C : Controle entree du prenom
    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account', ENTRY_FIRST_NAME_ERROR);
    }

// Clients B2C : Controle entree du nom de famille
    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account', ENTRY_LAST_NAME_ERROR);
    }

// Clients B2C : Controle entree date de naissance
    if (ACCOUNT_DOB == 'true') {
      if ((strlen($dob) < ENTRY_DOB_MIN_LENGTH) || (!empty($dob) && (!is_numeric(osc_date_raw($dob)) || !@checkdate(substr(osc_date_raw($dob), 4, 2), substr(osc_date_raw($dob), 6, 2), substr(osc_date_raw($dob), 0, 4))))) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account', ENTRY_DATE_OF_BIRTH_ERROR);
      }
    }

// Clients B2C : Controle entree adresse e-mail
    if (osc_validate_email($email_address) == false) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
      
    } elseif ($email_address != $email_address_confirm) {
    
      $error = true;
      $OSCOM_MessageStack->addError('create_account', ENTRY_EMAIL_ADDRESS_CONFIRM_NOT_MATCHING);
      
    } else {

      $Qcheckemail = $OSCOM_PDO->prepare('select customers_id
                                          from :table_customers
                                          where customers_email_address = :customers_email_address
                                         ');
      $Qcheckemail->bindValue(':customers_email_address', $email_address);

      $Qcheckemail->execute();


      if ($Qcheckemail->fetch() !== false) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
      }

// check if mail on newsletter_no_account	  
      $email_address1 = osc_db_prepare_input($email_address);

      $QcheckEmailNoAccount = $OSCOM_PDO->prepare('select count(*) as total 
                                                   from :table_newsletter_no_account 
                                                   where customers_email_address = :customers_email_address
                                                  ');
      $QcheckEmailNoAccount->bindValue(':customers_email_address', $email_address1);
      $QcheckEmailNoAccount->execute();
      $check_email_no_account = $QcheckEmailNoAccount->fetch();

      if ($check_email_no_account['total'] > 0) {
        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_newsletter_no_account 
                                        where customers_email_address = :email_address
                                      ');
        $Qdelete->bindValue(':email_address', $email_address);
        $Qdelete->execute();
      }	  
    }

// Clients B2C : Controle  du mot de passe
    if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account', ENTRY_PASSWORD_ERROR);
    } elseif ($password != $confirmation) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
    }

    if ( $error === false ) {
      $sql_data_array = array('customers_firstname' => $firstname,
                              'customers_lastname' => $lastname,
                              'customers_email_address' => $email_address,
                              'customers_newsletter' => $newsletter,
                              'languages_id' => $_SESSION['languages_id'],
                              'customers_password' => osc_encrypt_password($password),
                              'member_level' => '1');

      if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = osc_date_raw($dob);

      $OSCOM_PDO->save('customers', $sql_data_array);

      $customer_id = $OSCOM_PDO->lastInsertId();
// save element in address book
      $sql_data_array = array('customers_id' => (int)$customer_id,
                              'entry_firstname' => $firstname,
                              'entry_lastname' => $lastname
                              );

      $OSCOM_PDO->save('address_book', $sql_data_array);

      $address_id = $OSCOM_PDO->lastInsertId();

      $OSCOM_PDO->save('customers', array('customers_default_address_id' => (int)$address_id),
                                       array('customers_id' => (int)$customer_id)
                          );

      $OSCOM_PDO->save('customers_info', array('customers_info_id' => (int)$customer_id,
                                                'customers_info_number_of_logons' => '0',
                                                'customers_info_date_account_created' => 'now()'
                                                )
                        );


       $OSCOM_Customer->setData($customer_id);

      if (SESSION_RECREATE == 'True') {
        osc_session_recreate();
      }

// reset session token
      $_SESSION['sessiontoken'] = md5(osc_rand() . osc_rand() . osc_rand() . osc_rand());

// restore cart contents
      $_SESSION['cart']->restore_contents();

// build the message content

// email template
      require (DIR_WS_FUNCTIONS .'template_email.php');

      $name = $firstname . ' ' . $lastname;
      
      $template_email_welcome_catalog = osc_get_template_email_welcome_catalog($template_email_welcome_catalog);

      if (COUPON_CUSTOMER != '') {
        $email_coupon_catalog = osc_get_template_email_coupon_catalog($template_email_coupon_catalog);
        $email_coupon = $email_coupon_catalog . COUPON_CUSTOMER;
      }

      $template_email_signature =osc_get_template_email_signature($template_email_signature);
      $template_email_footer = osc_get_template_email_text_footer($template_email_footer);
      $email_subject = utf8_encode(html_entity_decode(EMAIL_SUBJECT));
      
      $email_text = $email_gender .'<br /><br />'. $template_email_welcome_catalog .'<br /><br />'. $email_coupon .'<br /><br />' .   $template_email_signature . '<br /><br />' . $template_email_footer;

// Envoi du mail avec gestion des images pour Fckeditor et Imanager.
      $mimemessage = new email(array('X-Mailer: ClicShopping'));
      $message = html_entity_decode($email_text);
      $message = str_replace('src="/', 'src="' . HTTP_SERVER . '/', $message);
      $mimemessage->add_html_fckeditor($message);
      $mimemessage->build_message();
      $from = STORE_OWNER_EMAIL_ADDRESS;
      $mimemessage->send($name, $email_address, '', $from, $email_subject);


      if (EMAIL_INFORMA_ACCOUNT_ADMIN == 'true') {
// e-mail de notification a l'administrateur
        $admin_email_welcome = utf8_decode(html_entity_decode(ADMIN_EMAIL_WELCOME));
        $admin_email_text = utf8_decode(html_entity_decode(ADMIN_EMAIL_TEXT));
        $admin_email_text .= $admin_email_welcome . $admin_email_text . $email_warning;
        osc_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $admin_email_text, $name, $email_address, '');
      }

//***************************************
// odoo web service
//***************************************
      if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
        require ('ext/odoo_xmlrpc/xml_rpc_catalog_create_account.php');
      }
//***************************************
// End odoo web service
//***************************************

      osc_redirect(osc_href_link('create_account_success.php', '', 'SSL'));
    }
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('create_account.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('create_account'));
