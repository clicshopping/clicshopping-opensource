<?php
/*
 * checkout_process.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


  include('includes/application_top.php');

// if the customer is not logged on, redirect them to the login page
  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot(array('mode' => 'SSL', 'page' => 'checkout_payment.php'));
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($_SESSION['cart']->count_contents() < 1) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }

// if no shipping method has been selected, redirect the customer to the shipping method selection page
  if (!isset($_SESSION['shipping']) || !isset($_SESSION['sendto'])) {
    osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
  }

  if ( (osc_not_null(MODULE_PAYMENT_INSTALLED)) && (!isset($_SESSION['payment'])) ) {
    osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
 }

// avoid hack attempts during the checkout procedure by checking the internal cartID
  if (isset($_SESSION['cart']->cartID) && isset($_SESSION['cartID'])) {
    if ($_SESSION['cart']->cartID != $_SESSION['cartID']) {
      osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
    }
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_process'));

// Client ip and provider client ip
  $client_computer_ip = osc_get_ip_address();
  $provider_name_client = osc_get_provider_name_client();

// load selected payment module
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment($_SESSION['payment']);

// load the selected shipping module
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping($_SESSION['shipping']);

// Recuperations des informations du client et de la commandes
  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// Stock Check
  $any_out_of_stock = false;
  if (STOCK_CHECK == 'true') {
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
      if (osc_check_stock($order->products[$i]['id'], $order->products[$i]['qty'])) {
        $any_out_of_stock = true;
      }
    }
    // Out of Stock
    if ( (STOCK_ALLOW_CHECKOUT != 'true') && ($any_out_of_stock == true) ) {
      osc_redirect(osc_href_link('shopping_cart.php'));
    }
  }

  $payment_modules->update_status();

  if ( ($payment_modules->selected_module != $_SESSION['payment']) || ( is_array($payment_modules->modules) && (sizeof($payment_modules->modules) > 1) && !is_object($GLOBALS[$_SESSION['payment']]) ) || (is_object($GLOBALS[$_SESSION['payment']]) && ($GLOBALS[$_SESSION['payment']]->enabled == false)) ) {
    osc_redirect(osc_href_link('checkout_payment.php', 'error_message=' . urlencode(ERROR_NO_PAYMENT_MODULE_SELECTED), 'SSL'));
  }

  require(DIR_WS_CLASSES . 'order_total.php');
  $order_total_modules = new order_total;

  $order_totals = $order_total_modules->process();

// load the before_process function from the payment modules
  $payment_modules->before_process();

// Manage the atos module and the  Atos situation report in database. 
// Do not modify
  if (MODULE_PAYMENT_ATOS_STATUS == 'True') {
    $cc_owner = $order->info['transaction_id'];
  } else {
    $cc_owner = $order->info['cc_owner'];
  }

  $sql_data_array = array('customers_id' => $OSCOM_Customer->getID(),
                          'customers_group_id' => $order->customer['group_id'],
                          'customers_name' => $order->customer['firstname'] . ' ' . $order->customer['lastname'],
                          'customers_company' => $order->customer['company'],
                          'customers_street_address' => $order->customer['street_address'],
                          'customers_suburb' => $order->customer['suburb'],
                          'customers_city' => $order->customer['city'],
                          'customers_postcode' => $order->customer['postcode'], 
                          'customers_state' => $order->customer['state'], 
                          'customers_country' => $order->customer['country']['title'], 
                          'customers_telephone' => $order->customer['telephone'], 
                          'customers_email_address' => $order->customer['email_address'],
                          'customers_address_format_id' => $order->customer['format_id'], 
                          'delivery_name' => trim($order->delivery['firstname'] . ' ' . $order->delivery['lastname']),
                          'delivery_company' => $order->delivery['company'],
                          'delivery_street_address' => $order->delivery['street_address'], 
                          'delivery_suburb' => $order->delivery['suburb'], 
                          'delivery_city' => $order->delivery['city'], 
                          'delivery_postcode' => $order->delivery['postcode'], 
                          'delivery_state' => $order->delivery['state'], 
                          'delivery_country' => $order->delivery['country']['title'], 
                          'delivery_address_format_id' => $order->delivery['format_id'], 
                          'billing_name' => $order->billing['firstname'] . ' ' . $order->billing['lastname'], 
                          'billing_company' => $order->billing['company'],
                          'billing_street_address' => $order->billing['street_address'], 
                          'billing_suburb' => $order->billing['suburb'], 
                          'billing_city' => $order->billing['city'], 
                          'billing_postcode' => $order->billing['postcode'], 
                          'billing_state' => $order->billing['state'], 
                          'billing_country' => $order->billing['country']['title'], 
                          'billing_address_format_id' => $order->billing['format_id'], 
                          'payment_method' => $order->info['payment_method'], 
                          'cc_type' => $order->info['cc_type'], 
                          'cc_owner' => $cc_owner, 
                          'cc_number' => $order->info['cc_number'], 
                          'cc_expires' => $order->info['cc_expires'], 
                          'date_purchased' => 'now()', 
                          'orders_status' => $order->info['order_status'],
                          'orders_status_invoice' => $order->info['order_status_invoice'],
                          'currency' => $order->info['currency'], 
                          'currency_value' => $order->info['currency_value'],
                          'client_computer_ip' => $client_computer_ip,
                          'provider_name_client' => $provider_name_client,
                          'customers_cellular_phone' => $order->customer['cellular_phone']
                         );

// recuperation des informations societes pour les clients B2B (voir fichier classes/order.php)
  if ($OSCOM_Customer->getCustomersGroupID() != 0) {
    $sql_data_array['customers_siret'] = $order->customer['siret'];
    $sql_data_array['customers_ape'] = $order->customer['ape'];
    $sql_data_array['customers_tva_intracom'] = $order->customer['tva_intracom'];
  }

  $OSCOM_PDO->save('orders', $sql_data_array);

  $insert_id = $OSCOM_PDO->lastInsertId();

// insert the general condition of sales
  $page_manager_general_condition = osc_page_manager_general_condition();

  $sql_data_array = array('orders_id' => (int)$insert_id,
                          'customers_id' => (int)$OSCOM_Customer->getID(),
                          'page_manager_general_condition' => $page_manager_general_condition
                         );

  $OSCOM_PDO->save('orders_pages_manager', $sql_data_array);

// orders total
  for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
    $sql_data_array = array('orders_id' => $insert_id,
                            'title' => $order_totals[$i]['title'],
                            'text' => $order_totals[$i]['text'],
                            'value' => $order_totals[$i]['value'],
                            'class' => $order_totals[$i]['code'],
                            'sort_order' => $order_totals[$i]['sort_order']);
    $OSCOM_PDO->save('orders_total', $sql_data_array);
  }

  $customer_notification = (SEND_EMAILS == 'true') ? '1' : '0';

  $sql_data_array = array('orders_id' => $insert_id,
                          'orders_status_id' => $order->info['order_status'], 
                          'orders_status_invoice_id' => $order->info['order_status_invoice'],
                          'date_added' => 'now()', 
                          'customer_notified' => $customer_notification,
                          'comments' => $order->info['comments']);
                          
  $OSCOM_PDO->save('orders_status_history', $sql_data_array);

// discount coupons
  if( isset($_SESSION['coupon']) && is_object( $order->coupon ) ) {
    $sql_data_array = array( 'coupons_id' => $order->coupon->coupon['coupons_id'],
                             'orders_id' => $insert_id );

    $OSCOM_PDO->save('discount_coupons_to_orders', $sql_data_array);
  }

// initialized for the email confirmation
  $products_ordered = '';

  for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
// Stock Update - Joao Correia
    if (STOCK_LIMITED == 'true') {
      if (DOWNLOAD_ENABLED == 'true') {
        
        $stock_query_sql = 'select p.products_quantity, 
                                  pad.products_attributes_filename
                            from :table_products p
                            left join :table_products_attributes pa  on p.products_id = pa.products_id
                            left join :table_products_attributes_download pad  on pa.products_attributes_id = pad.products_attributes_id
                            where p.products_id = :products_id';

// Will work with only one option for downloadable products
// otherwise, we have to build the query dynamically with a loop
        $products_attributes = (isset($order->products[$i]['attributes'])) ? $order->products[$i]['attributes'] : '';

       if (is_array($products_attributes)) {

          $stock_query_sql .= ' and pa.options_id = :options_id 
                                and pa.options_values_id = :options_values_id
                                ';
        }

        $Qstock = $OSCOM_PDO->prepare($stock_query_sql);
        
        $Qstock->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));

        if (is_array($products_attributes)) {
          $Qstock->bindInt(':options_id', $products_attributes[0]['option_id']);
          $Qstock->bindInt(':options_values_id', $products_attributes[0]['value_id']);
        }

        $Qstock->execute();
      } else {
        $Qstock = $OSCOM_PDO->prepare('select products_quantity, 
                                              products_quantity_alert
                                      from :table_products 
                                      where products_id = :products_id
                                      ');
                                   
        $Qstock->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
        $Qstock->execute();                                    
      }

      if ($Qstock->fetch() !== false) {

        $stock_values = $Qstock->fetch();
// do not decrement quantities if products_attributes_filename exists

        if ((DOWNLOAD_ENABLED != 'true') || osc_not_null($Qstock->value('products_attributes_filename'))) {
// select the good qty in B2B ti decrease the stock. See shopping_cart top display out stock or not
        if ($OSCOM_Customer->getCustomersGroupID() != '0') {

          $QproductsQuantityCustomersGroup = $OSCOM_PDO->prepare('select products_quantity_fixed_group
                                                                  from :table_products_groups
                                                                  where products_id = :products_id
                                                                  and customers_group_id =  :customers_group_id                                   
                                                                 ');
          $QproductsQuantityCustomersGroup->bindInt(':products_id', osc_get_prid($order->products[$i]['id']) );
          $QproductsQuantityCustomersGroup->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID()  );
          $QproductsQuantityCustomersGroup->execute();

          $products_quantity_customers_group = $QproductsQuantityCustomersGroup->fetch();

// do the exact qty in function the customer group and product          
          $products_quantity_customers_group[$i] = $products_quantity_customers_group['products_quantity_fixed_group'];
        } else {
          $products_quantity_customers_group[$i] = 1;
        }

         $stock_left = $Qstock->valueInt('products_quantity') - ($order->products[$i]['qty'] * $products_quantity_customers_group[$i]);
        } else {
          $stock_left = $Qstock->valueInt('products_quantity');
          $stock_products_quantity_alert = $Qstock->valueInt('products_quantity_alert');
        }

// alert an email if the product stock is < stock reorder level
// Alert by mail if a product is 0 or < 0

        if (STOCK_ALERT_PRODUCT_REORDER_LEVEL == 'true') {
          if ((STOCK_ALLOW_CHECKOUT == 'false') && (STOCK_CHECK == 'true')) {
            $warning_stock = STOCK_REORDER_LEVEL;
            $current_stock = $stock_left;

// alert email if stock product alert < warning stock
            if (($stock_products_quantity_alert <= $warning_stock) && ($stock_products_quantity_alert != '0')) {
              $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_ALERT_STOCK);
              $email_text_subject_stock = html_entity_decode($email_text_subject_stock);

              $reorder_stock_email = stripslashes(EMAIL_REORDER_LEVEL_TEXT_ALERT_STOCK);
              $reorder_stock_email = html_entity_decode($reorder_stock_email);
              $reorder_stock_email .= "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" .   EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id'])  .  "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_URL . '</strong>' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php' . '?products_id=' . $order->products[$i]['id'] . "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_ALERT_STOCK . $stock_products_quantity_alert  .'</strong>';

              osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $reorder_stock_email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
           }

            if ($current_stock <= $warning_stock) {
              $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_STOCK);
              $email_text_subject_stock = html_entity_decode($email_text_subject_stock);

              $reorder_stock_email = stripslashes(EMAIL_REORDER_LEVEL_TEXT_STOCK);
              $reorder_stock_email = html_entity_decode($reorder_stock_email);
              $reorder_stock_email .= "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" .   EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id'])  .  "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_URL . '</strong>' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php' . '?products_id=' . $order->products[$i]['id'] . "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_STOCK . $current_stock  .'</strong>';

              osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $reorder_stock_email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
            }          
          }
        }

        if ($stock_left != $Qstock->valueInt('products_quantity')) {
          $OSCOM_PDO->save('products', ['products_quantity' => (int)$stock_left],
                                       ['products_id' => osc_get_prid($order->products[$i]['id'])]
                          );
        }

        if ( ($stock_left < 1) && (STOCK_ALLOW_CHECKOUT == 'false') ) {
          $OSCOM_PDO->save('products', ['products_status' => '0'],
                                       ['products_id' => osc_get_prid($order->products[$i]['id'])]);
        }

// Alert by mail product exhausted if a product is 0 or < 0
        if (STOCK_ALERT_PRODUCT_EXHAUSTED == 'true') {
          if (($stock_left < 1) && (STOCK_ALLOW_CHECKOUT == 'false') && (STOCK_CHECK == 'true')) {
            $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_STOCK);
            $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
            $email_product_exhausted_stock = stripslashes(EMAIL_TEXT_STOCK);
            $email_product_exhausted_stock = html_entity_decode($email_product_exhausted_stock);
            $email_product_exhausted_stock .=  "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" . EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id']) .  "\n";

            osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $email_product_exhausted_stock, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
          }
        } // end stock alert
      }
    }

// Update products_ordered (for bestsellers list)

    $Qupdate = $OSCOM_PDO->prepare('update :table_products set products_ordered = products_ordered + :products_ordered
                                   where products_id = :products_id
                                  ');
    $Qupdate->bindInt(':products_ordered', $order->products[$i]['qty']);
    $Qupdate->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
    $Qupdate->execute();


// search the good model
    if ($OSCOM_Customer->getCustomersGroupID() != '0') {
      $QproductsModuleCustomersGroup = $OSCOM_PDO->prepare('select products_model_group
                                                              from :table_products_groups
                                                              where products_id = :products_id
                                                              and customers_group_id =  :customers_group_id
                                                            ');
      $QproductsModuleCustomersGroup->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
      $QproductsModuleCustomersGroup->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
      $QproductsModuleCustomersGroup->execute();

      $productsModuleCustomersGroup = $QproductsModuleCustomersGroup->fetch();

      $products_model = $productsModuleCustomersGroup['products_model_group'];

      if (empty($products_model)) $products_model = $order->products[$i]['model'];

    } else {
      $products_model = $order->products[$i]['model'];
    }


    $sql_data_array = array('orders_id' => $insert_id, 
                            'products_id' => osc_get_prid($order->products[$i]['id']), 
                            'products_model' => $products_model,
                            'products_name' => $order->products[$i]['name'], 
                            'products_price' => $order->products[$i]['price'], 
                            'final_price' => $order->products[$i]['final_price'], 
                            'products_tax' => $order->products[$i]['tax'], 
                            'products_quantity' => $order->products[$i]['qty'],
                            'products_full_id' => $order->products[$i]['id']
                            );
    $OSCOM_PDO->save('orders_products', $sql_data_array);

    $order_products_id = $OSCOM_PDO->lastInsertId();

//------insert customer choosen option to order--------
    $attributes_exist = '0';
    $products_ordered_attributes = '';
    if (isset($order->products[$i]['attributes'])) {
      $attributes_exist = '1';
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        if (DOWNLOAD_ENABLED == 'true') {

          $Qattributes = $OSCOM_PDO->prepare('select popt.products_options_name,
                                                     poval.products_options_values_name,
                                                      pa.options_values_price,
                                                      pa.price_prefix,
                                                      pad.products_attributes_maxdays,
                                                      pad.products_attributes_maxcount,
                                                      pad.products_attributes_filename,
                                                      pa.products_attributes_reference
                                               from :table_products_options popt,
                                                    :table_products_options_values poval,
                                                    :table_products_attributes pa
                                                    :table_products_attributes pa
                                               left join :table_products_attributes_download pad on pa.products_attributes_id = pad.products_attributes_id
                                               where pa.products_id = :products_id
                                                and pa.options_id = :options_id
                                                and pa.options_id = popt.products_options_id
                                                and pa.options_values_id = :options_values_id
                                                and pa.options_values_id = poval.products_options_values_id
                                                and popt.language_id = :language_id
                                                and popt.language_id = poval.language_id
                                             ');

          $Qattributes->bindInt(':products_id', (int)$order->products[$i]['id'] );
          $Qattributes->bindInt(':options_id', (int)$order->products[$i]['attributes'][$j]['option_id'] );
          $Qattributes->bindInt(':options_values_id', (int)$order->products[$i]['attributes'][$j]['value_id'] );
          $Qattributes->bindInt(':language_id', (int)$_SESSION['languages_id'] );

        } else {

          $Qattributes = $OSCOM_PDO->prepare('select popt.products_options_name,
                                                     poval.products_options_values_name,
                                                     pa.options_values_price,
                                                     pa.price_prefix,
                                                     pa.products_attributes_reference
                                              from :table_products_options popt,
                                                   :table_products_options_values poval,
                                                   :table_products_attributes pa
                                              where pa.products_id = :products_id
                                              and pa.options_id = :options_id
                                              and pa.options_id = popt.products_options_id
                                              and pa.options_values_id = :options_values_id
                                              and pa.options_values_id = poval.products_options_values_id
                                              and popt.language_id = :language_id
                                              and popt.language_id = poval.language_id
                                            ');

          $Qattributes->bindInt(':products_id', (int)$order->products[$i]['id'] );
          $Qattributes->bindInt(':options_id', (int)$order->products[$i]['attributes'][$j]['option_id'] );
          $Qattributes->bindInt(':options_values_id', (int)$order->products[$i]['attributes'][$j]['value_id'] );
          $Qattributes->bindInt(':language_id', (int)$_SESSION['languages_id'] );
        }

        $Qattributes->execute();
        $attributes_values = $Qattributes->fetch();

        $sql_data_array = array('orders_id' => $insert_id,
                                'orders_products_id' => $order_products_id,
                                'products_options' => $Qattributes->value('products_options_name'),
                                'products_options_values' => $Qattributes->value('products_options_values_name'),
                                'options_values_price' => $Qattributes->value('options_values_price'),
                                'price_prefix' => $Qattributes->value('price_prefix'));

        $OSCOM_PDO->save('orders_products_attributes', $sql_data_array);

        if ((DOWNLOAD_ENABLED == 'true') && $Qattributes->hasValue('products_attributes_filename') && tep_not_null($Qattributes->value('products_attributes_filename'))) {
          $sql_data_array = array('orders_id' => $insert_id,
                                  'orders_products_id' => $order_products_id,
                                  'orders_products_filename' => $Qattributes->value('products_attributes_filename'),
                                  'download_maxdays' => $Qattributes->value('products_attributes_maxdays'),
                                  'download_count' => $Qattributes->value('products_attributes_maxcount'));

          $OSCOM_PDO->save('orders_products_download', $sql_data_array);
        }

        $products_ordered_attributes .= "\n\t" . $Qattributes->value('products_options_name') . ' ' . $Qattributes->value('products_options_values_name');
      }
    }
//------insert customer choosen option eof ----

    $products_ordered .= $order->products[$i]['qty'] . ' x ' . $order->products[$i]['name'] . ' (' . $order->products[$i]['model'] . ') = ' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . $products_ordered_attributes . "\n";
  }
 // ---------------------------------------------
 // ------             EMAIL SENT  --------------
 // ---------------------------------------------

// lets start with the email confirmation
  $message_order = stripslashes(EMAIL_TEXT_ORDER_NUMBER) . ' ' . $insert_id . "\n" . stripslashes(EMAIL_TEXT_INVOICE_URL);
  $message_order = $message_order;
  $email_order .= $message_order . ' ' . osc_href_link('account_history_info.php', 'order_id=' . $insert_id, 'SSL', false) . "\n" .  EMAIL_TEXT_DATE_ORDERED . ' ' . strftime(DATE_FORMAT_LONG) . "\n\n";

  if ($order->info['comments']) {
// pb utf8 avec le &  et "
    $email_order .= osc_db_output($order->info['comments']) . "\n\n";
  }

  $message_order = stripslashes(EMAIL_TEXT_PRODUCTS);
  $email_order .= html_entity_decode($message_order) . "\n" . 
                  EMAIL_SEPARATOR . "\n" . 
                  $products_ordered . 
                  EMAIL_SEPARATOR . "\n";

  for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
    $email_order .= strip_tags($order_totals[$i]['title']) . ' ' . strip_tags($order_totals[$i]['text']) . "\n";
  }

  if ($order->content_type != 'virtual') {
    $message_order = stripslashes(EMAIL_TEXT_DELIVERY_ADDRESS);
    $email_order .= "\n" . html_entity_decode($message_order) . "\n" . 
                    EMAIL_SEPARATOR . "\n" .
                    osc_address_label($OSCOM_Customer->getID(), $_SESSION['sendto'], 0, '', "\n") . "\n";
  }

  $message_order = stripslashes(EMAIL_TEXT_BILLING_ADDRESS);
  $email_order .= "\n" .html_entity_decode($message_order) . "\n" .
                  EMAIL_SEPARATOR . "\n" .
                  osc_address_label($OSCOM_Customer->getID(), $_SESSION['billto'], 0, '', "\n") . "\n\n";
  if (is_object($GLOBALS[$_SESSION['payment']])) {
    $message_order = stripslashes(EMAIL_TEXT_PAYMENT_METHOD);
    $email_order .= html_entity_decode($message_order) . "\n" . 
                    EMAIL_SEPARATOR . "\n";
    $payment_class = $GLOBALS[$_SESSION['payment']];
    $email_order .= $order->info['payment_method'] . "\n\n";
    if (isset($payment_class->email_footer)) {
      $email_order .= $payment_class->email_footer;
    }
  }

  $message_order = stripslashes(EMAIL_TEXT_FOOTER);
  $email_order .= html_entity_decode($message_order);
  
  osc_mail($order->customer['firstname'] . ' ' . $order->customer['lastname'], $order->customer['email_address'], EMAIL_TEXT_SUBJECT, $email_order, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

// send emails to other people


// SEND_EXTRA_ORDER_EMAILS_TO does'nt work like this, test<test@test.com>, just with test@test.com
  if (SEND_EXTRA_ORDER_EMAILS_TO != '') {
    $email_text_subject = stripslashes(EMAIL_TEXT_SUBJECT);
    $email_text_subject = html_entity_decode($email_text_subject);

    $text = extract_email_address (SEND_EXTRA_ORDER_EMAILS_TO);

    foreach($text as $email){
      osc_mail('', $email, $email_text_subject, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
    }
  }

// load the after_process function from the payment modules
  $payment_modules->after_process();

  $_SESSION['cart']->reset(true);

//***************************************
// odoo web service
//***************************************
  if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_ORDER_CATALOG != 'none') {
    $delivery_postcode = $order->delivery['postcode'];
    $billing_postcode = $order->billing['postcode'];
    if (ODOO_ACTIVATE_ORDER_CATALOG == 'order') {
      require ('ext/odoo_xmlrpc/xml_rpc_catalog_checkout_process_order.php');
    } else {
      require ('ext/odoo_xmlrpc/xml_rpc_catalog_checkout_process_invoice.php');
    }
  }
//***************************************
// End odoo web service
//***************************************

// unregister session variables used during checkout
  unset($_SESSION['sendto']);
  unset($_SESSION['billto']);
  unset($_SESSION['shipping']);
  unset($_SESSION['payment']);
  unset($_SESSION['comments']);
  unset($_SESSION['coupon']);

  osc_redirect(osc_href_link('checkout_success.php', '', 'SSL'));

  require('includes/application_bottom.php');
