<?php
/*
 * checkout_success.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  echo osc_draw_form('order', osc_href_link('checkout_success.php', 'action=update', 'SSL'));
?>
  <div class="contentContainer">

    <?php echo $OSCOM_Template->getBlocks('modules_checkout_success'); ?>

    <div class="clearfix"></div>
    <div class="buttonSet pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null); ?></div>
  </div>
</form>

<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>