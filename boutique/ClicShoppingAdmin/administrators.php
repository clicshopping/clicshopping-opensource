<?php
/**
 * administrators.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $htaccess_array = null;
  $htpasswd_array = null;
  $is_iis = stripos($_SERVER['SERVER_SOFTWARE'], 'iis');
 
  $authuserfile_array = array('##### ClicShopping admin protection - start #####',
                              'AuthType Basic',
                              'AuthName "ClicShopping administration"',
                              'AuthUserFile ' . DIR_FS_ADMIN . '.htpasswd_clicshopping',
                              'Require valid-user',
                              '##### ClicShopping admin protection - end #####',
                              '<IfModule mod_expires.c>',
                              'ExpiresActive On',
                              'ExpiresDefault "access plus 1 seconds"',
                              'ExpiresByType image/jpeg "access plus 2592000 seconds"',
                              'ExpiresByType image/png "access plus 2592000 seconds"',
                              'ExpiresByType image/gif "access plus 2592000 seconds"',
                              'ExpiresByType text/css "access plus 604800 seconds"',
                              'ExpiresByType text/javascript "access plus 604800 seconds"',
                              'ExpiresByType application/javascript "access plus 604800 seconds"',
                              'ExpiresByType text/html "access plus 2592000 seconds"',
                              'ExpiresByType image/x-icon "access plus 2592000 seconds"',
                              '</IfModule>',
                              '<FilesMatch ".(gif|jpg|jpeg|png|ico|)$">',
                              'Header set Cache-Control "max-age= 2592000"',
                              '</FilesMatch>'
                             );

  if (!$is_iis && file_exists(DIR_FS_ADMIN . '.htpasswd_clicshopping') && osc_is_writable(DIR_FS_ADMIN . '.htpasswd_clicshopping') && file_exists(DIR_FS_ADMIN . '.htaccess') && osc_is_writable(DIR_FS_ADMIN . '.htaccess')) {
    $htaccess_array = array();
    $htpasswd_array = array();

    if (filesize(DIR_FS_ADMIN . '.htaccess') > 0) {
      $fg = fopen(DIR_FS_ADMIN . '.htaccess', 'rb');
      $data = fread($fg, filesize(DIR_FS_ADMIN . '.htaccess'));
      fclose($fg);

      $htaccess_array = explode("\n", $data);
    }

    if (filesize(DIR_FS_ADMIN . '.htpasswd_clicshopping') > 0) {
      $fg = fopen(DIR_FS_ADMIN . '.htpasswd_clicshopping', 'rb');
      $data = fread($fg, filesize(DIR_FS_ADMIN . '.htpasswd_clicshopping'));
      fclose($fg);

      $htpasswd_array = explode("\n", $data);
    }
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':
        require('includes/functions/password_funcs.php');

        $name = osc_db_prepare_input($_POST['name']);
        $first_name = osc_db_prepare_input($_POST['first_name']);
        $username = osc_db_prepare_input($_POST['username']);
        $password = osc_db_prepare_input($_POST['password']);

        $Qcheck = $OSCOM_PDO->prepare('select id
                                      from :table_administrators
                                      where user_name = :user_name
                                      limit 1
                                    ');
        $Qcheck->bindValue(':user_name', osc_db_input($username));
        $Qcheck->execute();

        if ( !empty($username) ) {
            if ($Qcheck->rowCount() < 1) {
              osc_db_query("insert into administrators (user_name,
                                                        user_password,
                                                        name,
                                                        first_name)
                           values ('" . osc_db_input($username) . "',
                                   '" . osc_db_input(osc_encrypt_password($password)) . "',
                                   '" . osc_db_input($name) . "',
                                   '" . osc_db_input($first_name) . "'
                                  )
                        ");
          }

          if (is_array($htpasswd_array)) {
            for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
              list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

              if ($ht_username == $username) {
                unset($htpasswd_array[$i]);
              }
            }

            if (isset($_POST['htaccess']) && ($_POST['htaccess'] == 'true')) {
              $htpasswd_array[] = $username . ':' . osc_crypt_apr_md5($password);
            }

            $fp = fopen(DIR_FS_ADMIN . '.htpasswd_clicshopping', 'w');
            fwrite($fp, implode("\n", $htpasswd_array));
            fclose($fp);

            if (!in_array('AuthUserFile ' . DIR_FS_ADMIN . '.htpasswd_clicshopping', $htaccess_array) && !empty($htpasswd_array)) {
              array_splice($htaccess_array, sizeof($htaccess_array), 0, $authuserfile_array);
            } elseif (empty($htpasswd_array)) {
              for ($i=0, $n=sizeof($htaccess_array); $i<$n; $i++) {
                if (in_array($htaccess_array[$i], $authuserfile_array)) {
                  unset($htaccess_array[$i]);
                }
              }
            }

            $fp = fopen(DIR_FS_ADMIN . '.htaccess', 'w');
            fwrite($fp, implode("\n", $htaccess_array));
            fclose($fp);
          }
        } else {
          $OSCOM_MessageStack->add_session(ERROR_ADMINISTRATOR_EXISTS, 'error');
        }

        osc_redirect(osc_href_link('administrators.php'));
        break;

      case 'save':
        require('includes/functions/password_funcs.php');

        $name = osc_db_prepare_input($_POST['name']);
        $first_name = osc_db_prepare_input($_POST['first_name']);	
        $username = osc_db_prepare_input($_POST['username']);
        $password = osc_db_prepare_input($_POST['password']);

        $Qcheck = $OSCOM_PDO->prepare('select id, 
                                               user_name 
                                      from :table_administrators 
                                      where id = :id
                                    ');
        $Qcheck->bindInt(':id', (int)$_GET['aID']);
        $Qcheck->execute();

        $check = $Qcheck->fetch();

// update username in current session if changed
        if ( ($check['id'] == $_SESSION['admin']['id']) && ($check['user_name'] != $_SESSION['admin']['username']) ) {
          $_SESSION['admin']['username'] = $username;
          $_SESSION['admin']['name'] = $name;
          $_SESSION['ADMIN']['first_name'] = $first_name;
        }

// update username in htpasswd if changed
        if (is_array($htpasswd_array)) {
          for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
            list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

            if ( ($check['user_name'] == $ht_username) && ($check['user_name'] != $username) ) {
              $htpasswd_array[$i] = $username . ':' . $ht_password;
            }
          }
        }

        $Qupdate = $OSCOM_PDO->prepare('update :table_administrators
                                        set user_name = :user_name,
                                           name = :name,
                                           first_name = :first_name
                                         where id = :id
                                       ');
        $Qupdate->bindValue(':user_name', $username);
        $Qupdate->bindValue(':name', $name);
        $Qupdate->bindValue(':first_name', $first_name);
        $Qupdate->bindInt(':id', (int)$_GET['aID'] );

        $Qupdate->execute();

        if (osc_not_null($password)) {
// update password in htpasswd
          if (is_array($htpasswd_array)) {
            for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
              list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

              if ($ht_username == $username) {
                unset($htpasswd_array[$i]);
              }
            }
 
            if (isset($_POST['htaccess']) && ($_POST['htaccess'] == 'true')) {
                $htpasswd_array[] = $username . ':' . osc_crypt_apr_md5($password);

            }
          }

          $Qupdate = $OSCOM_PDO->prepare('update :table_administrators
                                          set user_password = :user_password,
                                          where id = :id
                                        ');
          $Qupdate->bindValue(':user_password', osc_db_input(osc_encrypt_password($password)) );
          $Qupdate->bindInt(':id', (int)$_GET['aID'] );


        } elseif (!isset($_POST['htaccess']) || ($_POST['htaccess'] != 'true')) {
          if (is_array($htpasswd_array)) {
            for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
              list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

              if ($ht_username == $username) {
                unset($htpasswd_array[$i]);
              }
            }
          }
        }

// write new htpasswd file
        if (is_array($htpasswd_array)) {
          $fp = fopen(DIR_FS_ADMIN . '.htpasswd_clicshopping', 'w');
          fwrite($fp, implode("\n", $htpasswd_array));
          fclose($fp);

          if (!in_array('AuthUserFile ' . DIR_FS_ADMIN . '.htpasswd_clicshopping', $htaccess_array) && !empty($htpasswd_array)) {
            array_splice($htaccess_array, sizeof($htaccess_array), 0, $authuserfile_array);
          } elseif (empty($htpasswd_array)) {
            for ($i=0, $n=sizeof($htaccess_array); $i<$n; $i++) {
              if (in_array($htaccess_array[$i], $authuserfile_array)) {
                unset($htaccess_array[$i]);
              }
            }
          }

          $fp = fopen(DIR_FS_ADMIN . '.htaccess', 'w');
          fwrite($fp, implode("\n", $htaccess_array));
          fclose($fp);
        }

        osc_redirect(osc_href_link('administrators.php', 'aID=' . (int)$_GET['aID']));
        break;
      case 'deleteconfirm':
        $id = osc_db_prepare_input($_GET['aID']);
 
        $Qcheck = $OSCOM_PDO->prepare('select id, 
                                       user_name 
                                      from :table_administrators 
                                      where id = :id
                                    ');
        $Qcheck->bindInt(':id', (int)$id);
        $Qcheck->execute();

        if ($_SESSION['admin']['id'] == $Qcheck->valueInt('id')) {
          unset($_SESSION['admin']);
        }

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_administrators
                                        where  id = :id
                                       ');
        $Qdelete->bindInt(':id', (int)$id);
        $Qdelete->execute();

        if (is_array($htpasswd_array)) {
          for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
            list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

            if ($ht_username == $Qcheck->value('user_name')) {
              unset($htpasswd_array[$i]);
            }
          }

          $fp = fopen(DIR_FS_ADMIN . '.htpasswd_clicshopping', 'w');
          fwrite($fp, implode("\n", $htpasswd_array));
          fclose($fp);

          if (empty($htpasswd_array)) {
            for ($i=0, $n=sizeof($htaccess_array); $i<$n; $i++) {
              if (in_array($htaccess_array[$i], $authuserfile_array)) {
                unset($htaccess_array[$i]);
              }
            }

            $fp = fopen(DIR_FS_ADMIN . '.htaccess', 'w');
            fwrite($fp, implode("\n", $htaccess_array));
            fclose($fp);
          }
        }

        osc_redirect(osc_href_link('administrators.php'));
        break;
    }
  }

  $secMessageStack = new messageStack();

  if (is_array($htpasswd_array)) {
    if (empty($htpasswd_array)) {
      $secMessageStack->add(sprintf(HTPASSWD_INFO, implode('<br />', $authuserfile_array)), 'error');
    } else {
      $secMessageStack->add(HTPASSWD_SECURED, 'success');
    }
  } else if (!$is_iis) {
    $secMessageStack->add(HTPASSWD_PERMISSIONS, 'error');
  }
  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/administrators.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
  <span class="pull-right"><?php echo '<a href="' . osc_href_link('administrators.php', 'action=new') . '">' . osc_image_button('button_insert_banner.gif',  IMAGE_INSERT) . '</a>'; ?></span>
<?php
  }
?>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>


<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ADMINISTRATORS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_USER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_HTPASSWD; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
// Affichage des utilisateurs du site
  $Qadmins = $OSCOM_PDO->prepare('select id,
                                         user_name,
                                         name,
                                         first_name
                                  from :table_administrators
                                  where id >= :id
                                  order by user_name
                                ');
  $Qadmins->bindInt(':id', 1);
  $Qadmins->execute();

  while ($admins = $Qadmins->fetch() ) {
    if ((!isset($_GET['aID']) || (isset($_GET['aID']) && ($_GET['aID'] == $admins['id']))) && !isset($aInfo) && (substr($action, 0, 3) != 'new')) {
      $aInfo = new objectInfo($admins);
    }
 
    $htpasswd_secured = osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', TEXT_NO_SECURED, 16, 16);

    if ($is_iis) {
      $htpasswd_secured = 'N/A';
    }

    if (is_array($htpasswd_array)) {
      for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
        list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

        if ($ht_username == $admins['user_name']) {
          $htpasswd_secured = osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', TEXT_SECURED, 16, 16);
          break;
        }
      }
    }

    if ( (isset($aInfo) && is_object($aInfo)) && ($admins['id'] == $aInfo->id) ) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('administrators.php', 'aID=' . $aInfo->id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('administrators.php', 'aID=' . $admins['id']) . '\'">' . "\n";
    }
?>
    <td class="dataTableContent"><?php echo $admins['user_name']; ?></td>
    <td class="dataTableContent" align="left"><?php echo $admins['first_name'] . ' ' .$admins['name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $htpasswd_secured; ?></td>
    <td class="dataTableContent" align="right">
<?php	
// Affichage des icones d'acces direct	
                echo '<a href="' . osc_href_link('administrators.php', osc_get_all_get_params(array('aID', 'action')) . 'aID=' .  $admins['id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_USER) . '</a>';
                echo osc_draw_separator('pixel_trans.gif', '6', '16');
                echo '<a href="' . osc_href_link('administrators.php', osc_get_all_get_params(array('aID', 'action')) . 'aID=' .  $admins['id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
                echo osc_draw_separator('pixel_trans.gif', '6', '16');
                if ( (isset($aInfo) && is_object($aInfo)) && ($admins['id'] == $aInfo->id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('administrators.php', 'aID=' . $admins['id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
    </td>
  </tr>

<?php
  }
?>
        </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_ADMINISTRATOR . '</strong>');

      $contents = array('form' => osc_draw_form('administrator', 'administrators.php', 'action=insert', 'post', 'autocomplete="off"'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_NAME . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_input_field('name') . '</td></tr></table>');
      $contents[] = array('text' => '<br />' . TEXT_INFO_FIRSTNAME . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_input_field('first_name') . '</td></tr></table>');	  
      $contents[] = array('text' => '<br />' . TEXT_INFO_USERNAME . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_input_field('username') . '</td></tr></table>');
      $contents[] = array('text' => '<br />' . TEXT_INFO_PASSWORD . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_password_field('password') . '</td></tr></table>');
 
      if (is_array($htpasswd_array)) {
        $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('htaccess', 'true') . ' ' . TEXT_INFO_PROTECT_WITH_HTPASSWD);
      }
 
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_save.gif', IMAGE_SAVE) . '&nbsp;<a href="' . osc_href_link('administrators.php') . '"></div><div class="pull-right" style="padding-right:50px; padding-top:10px;">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');

      break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . $aInfo->user_name . '</strong>');

      $contents = array('form' => osc_draw_form('administrator', 'administrators.php', 'aID=' . $aInfo->id . '&action=save', 'post', 'autocomplete="off"'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_NAME . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_input_field('name', $aInfo->name) . '</td></tr></table>');
      $contents[] = array('text' => '<br />' . TEXT_INFO_FIRSTNAME . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_input_field('first_name', $aInfo->first_name) . '</td></tr></table>');	  
      $contents[] = array('text' => '<br />' . TEXT_INFO_USERNAME . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_input_field('username', $aInfo->user_name) . '</td></tr></table>');
      $contents[] = array('text' => '<br />' . TEXT_INFO_NEW_PASSWORD . '<br /><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformBoxe"><tr><td class="man">' . osc_draw_password_field('password') . '</td></tr></table>');
 
      if (is_array($htpasswd_array)) {
        $default_flag = false;

        for ($i=0, $n=sizeof($htpasswd_array); $i<$n; $i++) {
          list($ht_username, $ht_password) = explode(':', $htpasswd_array[$i], 2);

          if ($ht_username == $aInfo->user_name) {
            $default_flag = true;
            break;
          }
        }

        $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('htaccess', 'true', $default_flag) . ' ' . TEXT_INFO_PROTECT_WITH_HTPASSWD);
      }
 
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . '&nbsp;<a href="' . osc_href_link('administrators.php', 'aID=' . $aInfo->id) . '"></div><div class="pull-right" style="padding-right:50px; padding-top:10px;">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');

      break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . $aInfo->user_name . '</strong>');

      $contents = array('form' => osc_draw_form('administrator', 'administrators.php', 'aID=' . $aInfo->id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $aInfo->user_name . '</strong>');
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_UPDATE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('administrators.php', 'aID=' . $aInfo->id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');

      break;
    default:
/*
      if (isset($aInfo) && is_object($aInfo)) {
        $heading[] = array('text' => '<strong>' . $aInfo->user_name . '</strong>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . osc_href_link('administrators.php', 'aID=' . $aInfo->id . '&action=edit') . '">' . osc_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . osc_href_link('administrators.php', 'aID=' . $aInfo->id . '&action=delete') . '">' . osc_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
      }
*/			
      break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
