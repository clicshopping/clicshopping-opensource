<?php
/**
 * invoice.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

      class PDF extends FPDF {
//Page header
        function RoundedRect($x, $y, $w, $h,$r, $style = '') {
          $k = $this->k;
          $hp = $this->h;
          if($style=='F')
              $op='f';
          elseif($style=='FD' or $style=='DF')
              $op='B';
          else
              $op='S';
          $MyArc = 4/3 * (sqrt(2) - 1);
          $this->_out(sprintf('%.2f %.2f m',($x+$r)*$k,($hp-$y)*$k ));
          $xc = $x+$w-$r ;
          $yc = $y+$r;
          $this->_out(sprintf('%.2f %.2f l', $xc*$k,($hp-$y)*$k ));

          $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
          $xc = $x+$w-$r ;
          $yc = $y+$h-$r;
          $this->_out(sprintf('%.2f %.2f l',($x+$w)*$k,($hp-$yc)*$k));
          $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
          $xc = $x+$r ;
          $yc = $y+$h-$r;
          $this->_out(sprintf('%.2f %.2f l',$xc*$k,($hp-($y+$h))*$k));
          $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
          $xc = $x+$r ;
          $yc = $y+$r;
          $this->_out(sprintf('%.2f %.2f l',($x)*$k,($hp-$yc)*$k ));
          $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
          $this->_out($op);
        }

        function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
            $h = $this->h;
            $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k,
                $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
        }

        function Header() {
          global $oID;
          $date = strftime('%A, %d %B %Y');

          if (DISPLAY_INVOICE_HEADER == 'false') {

            //Logo
            $this->Image(DIR_WS_IMAGES.'image/facture/'.INVOICE_LOGO,5,10,50);

            // Company Name
            $this->SetX(0);
            $this->SetY(10);
            $this->SetFont('Arial','B',10);
            $this->SetTextColor(158,11,14);
            $this->Ln(0);
            $this->Cell(125);
            $this->MultiCell(100, 3.5, STORE_NAME,0,'L');

            // Company Address
            $this->SetX(0);
            $this->SetY(15);
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Ln(0);
            $this->Cell(125);
            $this->MultiCell(100, 3.5, STORE_NAME_ADDRESS,0,'L');

            //email
            $this->SetX(0);
            $this->SetY(25);
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Ln(0);
            $this->Cell(5);
            $this->MultiCell(100, 3.5, ENTRY_EMAIL .' : ' . STORE_OWNER_EMAIL_ADDRESS,0,'L');

            //website
            $this->SetX(0);
            $this->SetY(29);
            $this->SetFont('Arial','',7);
            $this->SetTextColor(158,11,14);
            $this->Ln(0);
            $this->Cell(5);
            $this->MultiCell(100, 3.5, ENTRY_HTTP_SITE . HTTP_SERVER,0,'L');
          }
        }

        function Footer() {

          //Customer Thank you
          //Position from bottom
          $this->SetY(-60);
          //Arial italic 8
          $this->SetFont('Arial','B',8);
          $this->SetTextColor(158,11,14);
          $this->Cell(0,10, THANK_YOU_CUSTOMER, 0,0,'C');

          if (DISPLAY_INVOICE_FOOTER == 'false') {

            //Proprieties Legal
            //Position from bottom
            $this->SetY(-50);
            //Arial italic 8
            $this->SetFont('Arial','B',8);
            $this->SetTextColor(158,11,14);
            $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE), 0,0,'L');

            //Position from bottom
            $this->SetY(-45);
            //Arial italic 8
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE_NEXT), 0,0,'C');

            //Position from bottom
            $this->SetY(-40);
            //Arial italic 8
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE_NEXT1), 0,0,'C');

            //Position from bottom
            $this->SetY(-35);
            //Arial italic 8
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Cell(0,10, utf8_decode(RESERVE_PROPRIETE_NEXT2), 0,0,'C');


            // Company Information
            //Position at 1.5 cm (17) from bottom
            $this->SetY(-26);
            //Arial italic 8
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Cell(0,10, ENTRY_INFO_SOCIETE, 0,0,'C');

            //Position at 1.5 cm (17) from bottom
            $this->SetY(-20);
            //Arial italic 8
            $this->SetFont('Arial','',8);
            $this->SetTextColor(158,11,14);
            $this->Cell(0,10, ENTRY_INFO_SOCIETE_NEXT, 0,0,'C');

          }

        //	$$this->SetY(-15);
        //  $this->Cell(0,10, PRINT_INVOICE_URL, 0,0,'C');

        //	Page number
        //  $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        }
      }
