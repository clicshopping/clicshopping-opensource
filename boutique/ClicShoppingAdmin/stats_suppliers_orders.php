<?php
/*
 * stats_suppliers_orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


  define('FPDF_FONTPATH','../includes/javascript/fpdf/font/');
  require('../includes/javascript/fpdf/fpdf.php');

  require('includes/application_top.php');

  require('includes/classes/pdf.php');
  require('includes/functions/pdf.php');

  $QuppliersProducts = $OSCOM_PDO->prepare('select distinct s.suppliers_id,
                                                           s.suppliers_name,
                                                           s.suppliers_manager,
                                                           s.suppliers_phone,
                                                           s.suppliers_email_address,
                                                           s.suppliers_fax,
                                                           s.suppliers_address,
                                                           s.suppliers_suburb,
                                                           s.suppliers_postcode,
                                                           s.suppliers_city,
                                                           s.suppliers_states,
                                                           s.suppliers_country_id
                                             from :table_orders_products  op
                                               left join :table_products  p ON op.products_id = p.products_id
                                               left join :table_suppliers s on p.suppliers_id = s.suppliers_id
                                               left join :table_orders  o ON op.orders_id = o.orders_id
                                               left join :table_orders_products_attributes opa ON op.orders_products_id = opa.orders_products_id
                                            where o.date_purchased between :start  and :end
                                            and s.suppliers_id = :suppliers_id
                                            and o.orders_status = :orders_status
                                            group by op.products_name,
                                                     opa.products_options,
                                                     opa.products_options_values
                                            order by p.products_model,
                                                     op.products_name
                                           ');

  $QuppliersProducts->bindInt(':start', (int)$_GET['bDS'] );
  $QuppliersProducts->bindInt(':end', (int)$_GET['bED'] );
  $QuppliersProducts->bindInt(':suppliers_id', (int)$_GET['bID'] );
  $QuppliersProducts->bindInt(':orders_status', (int)$_GET['bOS'] );

  $QuppliersProducts->execute();

  $suppliers = $QuppliersProducts->fetch();

// Classe pdf.php
  $pdf = new PDF();

// Marge de la page
  $pdf->SetMargins(10,2,6);

// Ajoute page
  $pdf->AddPage();


// Cadre pour l'adresse de livraison
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(255);
  $pdf->RoundedRect(108, 40, 90, 35, 2, 'DF');

// Adresse de livraison
  $pdf->SetFont('Arial','B',8);
  $pdf->SetTextColor(0);
  $pdf->Text(113,44,ENTRY_SHIP_TO);
  $pdf->SetX(0);
  $pdf->SetY(47);
  $pdf->Cell(111);
//  $pdf->MultiCell(70, 3.3, osc_address_format($order->delivery['format_id'], $order->delivery, '', '', "\n"),0,'L');
  $pdf->Text(113,50, utf8_decode($suppliers['suppliers_address']));
  $pdf->Text(113,55, utf8_decode($suppliers['suppliers_suburb']));
  $pdf->Text(113,60, utf8_decode($suppliers['suppliers_postcode']));
  $pdf->Text(113,65, utf8_decode($suppliers['suppliers_city']));
  $pdf->Text(113,70, utf8_decode($suppliers['suppliers_states']));
  
  
// Information fournisseur
  $pdf->SetFont('Arial','B',8);
  $pdf->SetTextColor(0);
  $pdf->Text(10,85,ENTRY_SUPPLIER_INFORMATION);


// Manager du fournisseur
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(113,85,ENTRY_MANAGER . utf8_decode($suppliers['suppliers_manager']));
  

// Email du fournisseur
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(113,90,ENTRY_EMAIL . $suppliers['suppliers_email_address']);

// Manager 
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(113,95,ENTRY_PHONE . $suppliers['suppliers_phone']);	

// Telephone du client
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(113,100,ENTRY_FAX . $suppliers['suppliers_fax']);	


// Cadre du numero de fournisseur, date debut analyse et date fin analyse
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(245);
  $pdf->RoundedRect(6, 107, 192, 11, 2, 'DF');

// Numero de commande ou de facture
// Date de commande ou de facture
// Methode de paiement

    $pdf->Text(10,113, ENTRY_SUPPLIERS_NUMBER . '  ' . (int)$_GET['bID']);
    $pdf->Text(60,113, START_ANALYSE . ' ' . $_GET['bDS']);
    $pdf->Text(130,113, END_ANALYSE . ' '. $_GET['bED']);	

// Cadre pour afficher du Titre
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(245);
  $pdf->RoundedRect(108, 32, 90, 7, 2, 'DF');

// Affichage du titre
  $pdf->SetFont('Arial','',10);
  $pdf->SetY(32);
  $pdf->SetX(108);
  $pdf->MultiCell(90,7,PRINT_SUPPLIERS_TITLE . utf8_decode($suppliers['suppliers_name']),0,'C');

// Fields Name position
  $Y_Fields_Name_position = 125;

// Table position, under Fields Name
  $Y_Table_Position = 131;

// Entete du tableau des produits a commander	
  output_table_suppliers($Y_Fields_Name_position);

  $QuppliersProducts1 = $OSCOM_PDO->prepare('select distinct s.suppliers_id,
                                                             sum(op.products_quantity) as sum_qty,
                                                             op.products_name,
                                                             op.products_model,
                                                             opa.products_options,
                                                             opa.products_options_values
                                             from :table_orders_products  op
                                               left join :table_products  p ON op.products_id = p.products_id
                                               left join :table_suppliers s on p.suppliers_id = s.suppliers_id
                                               left join :table_orders  o ON op.orders_id = o.orders_id
                                               left join :table_orders_products_attributes opa ON op.orders_products_id = opa.orders_products_id
                                            where o.date_purchased between :start  and :end
                                            and s.suppliers_id = :suppliers_id
                                            and o.orders_status = :orders_status
                                            group by op.products_name,
                                                     opa.products_options,
                                                     opa.products_options_values
                                            order by p.products_model,
                                                     op.products_name
                                           ');

  $QuppliersProducts1->bindInt(':start', (int)$_GET['bDS'] );
  $QuppliersProducts1->bindInt(':end', (int)$_GET['bED'] );
  $QuppliersProducts1->bindInt(':suppliers_id', (int)$_GET['bID'] );
  $QuppliersProducts1->bindInt(':orders_status', (int)$_GET['bOS'] );

  $QuppliersProducts1->execute();

  while ($suppliers =$QuppliersProducts1->fetch() ) {
// Quantite
    $pdf->SetFont('Arial','',7);
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(6);
    $pdf->MultiCell(9,6,$suppliers['sum_qty'],1,'C');

// products model	
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(15);
    $pdf->SetFont('Arial','',7);
    $pdf->MultiCell(25,6,utf8_decode($suppliers['products_model']),1,'C');

// products name	
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(40);
    $pdf->SetFont('Arial','',7);
    $pdf->MultiCell(65,6,utf8_decode($suppliers['products_name']),1,'C');

// products options	
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(105);
    $pdf->SetFont('Arial','',7);
    $pdf->MultiCell(45,6,utf8_decode($suppliers['products_options']),1,'C');


// products options values	
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(150);
    $pdf->SetFont('Arial','',7);
    $pdf->MultiCell(45,6,$suppliers['products_options_values'],1,'C');
    $Y_Table_Position += 6;

// Check for product line overflow
    $item_count++;
    if ((is_long($item_count / 32) && $i >= 20) || ($i == 20)) {
      $pdf->AddPage();
// Fields Name position
      $Y_Fields_Name_position = 125;
// Table position, under Fields Name
      $Y_Table_Position = 70;
      output_table_suppliers($Y_Table_Position-6);
      if ($i == 20) $item_count = 1;
    }
  }

// PDF's created now output the file
  $pdf->Output();
