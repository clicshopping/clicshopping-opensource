<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  $www_location = 'http://' . $_SERVER['HTTP_HOST'];

  if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI'])) {
    $www_location .= $_SERVER['REQUEST_URI'];
  } else {
    $www_location .= $_SERVER['SCRIPT_FILENAME'];
  }

  $www_location = substr($www_location, 0, strpos($www_location, 'install'));
?>
<!--
<script type="text/javascript">
  function checkLicense() {
    if(document.getElementById('license').checked){
      return true;
    } else {
      alert("Error Agreement License");
      return false;
    }
  }

</script>
!-->

<!--
<script type="text/javascript">
function apply()
{
  document.frm.sub.disabled=true;
  if(document.frm.chk.checked==true)
  {
    document.frm.sub.disabled=false;
  }
  if(document.frm.chk.checked==false)
  {
    document.frm.sub.enabled=false;
  }
}
</script> 
  <form name ="frm"  id="installForm" action="index.php?step=1" method="post"> 
     <p align="right">
      <span>License agreement</span>&nbsp;<input type="checkbox" name="chk" onClick="apply()">
    </p>
    <p align="right">
      <input type="button" name="sub" value="submit" disabled>
    </p>
  </form>
-->
<SCRIPT language=JavaScript>
function checkCheckBox(f){
if (f.agree.checked == false )
{
alert('Error Agreement License : Please check the box to continue.');
return false;
}else
return true;
}
//-->
</SCRIPT>

<div class="contentBlock">
  <ul style="list-style-type: none; padding: 5px; margin: 0px; display: inline; float: right;">
    <li style="font-weight: bold; display: inline;"></li>
  </ul>
  <div class="pull-left">
    <h1><?php echo TEXT_TITLE_WELCOME; ?></h1>
  </div>
  <div class="pull-right">
    <form action="index.php" method="get">
    <?php echo osc_draw_pull_down_menu('language', $languages_array, $language, 'onChange="this.form.submit();"'); ?>
    </form>
  </div>
  <div class="clearfix"></div>
    <p><?php echo TEXT_LICENCE; ?></p>
  </div>
</div>

<div class="contentBlock">
  <h2>License</h2>
  <div class="license"><?php include('license.txt'); ?></div>
  <form action="<?php echo $www_location .'install/index.php?step=2'; ?>" method="POST" onsubmit="return checkCheckBox(this)">
    <div class="pull-right"  style="padding-bottom: 10px;"><?php echo TEXT_AGREEMENT; ?>&nbsp;
      <input type="checkbox" value="" name="agree"><br /><br />
      <input type="submit" value="<?php echo TEXT_ACCEPT_LICENCE; ?>">
    </div>
  </form>
</div>






