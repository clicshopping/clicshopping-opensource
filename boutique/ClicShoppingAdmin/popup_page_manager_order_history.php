<?php
/*
 * pop_up_manageee_order_history.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

require('includes/application_top.php');

 // select the conditions of sales

  $QconditionGeneralOfSales = $OSCOM_PDO->prepare("select page_manager_general_condition                                               
                                                   from :table_orders_pages_manager
                                                   where orders_id = :orders_id
                                                   and customers_id = :customers_id
                                                  ");
  $QconditionGeneralOfSales->bindInt(':orders_id', (int)$_GET['order_id'] );
  $QconditionGeneralOfSales->bindInt(':customers_id', (int)$_GET['customer_id'] );
  $QconditionGeneralOfSales->execute();

  $condition_general_of_sales = $QconditionGeneralOfSales->fetch();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<title><?php echo 'Conditions of sales'; ?></title>
</head>
<body onload="resize();">
  <div>
    <?php echo $condition_general_of_sales['page_manager_general_condition']; ?>
  </div>
</body>
</html>
<?php
  require('includes/application_bottom.php');
