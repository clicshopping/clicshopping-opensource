<?php
/**
 * sec_dir_permissions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Permissions de sécurité des répertoires');

define('TABLE_HEADING_DIRECTORIES', 'Répertoire');
define('TABLE_HEADING_WRITABLE', 'Ecriture');
define('TABLE_HEADING_RECOMMENDED', 'Recommandé');

define('TEXT_DIRECTORY', 'Répertoire :');
?>
