<?php
/**
 * export_price_comparison.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: export_price_comparison.php 
*/
// temps d'execution infini
// ne fonctionne pas sur tous les serveurs, dans ce cas il y aura des timeouts
// et les fichiers seront incomplets. Mieux vaut alors opter pour un hebergement plus performant.
  set_time_limit(0);
  require('includes/application_top.php');
  $output = '';

// securisation des variables et verification
//$port = (isset($_GET['port']) && osc_not_null($_GET['port'])) ? osc_db_prepare_input($_GET['port']) : "-1";
  $verif = true;
  $ecotax = false;
  $tax = osc_db_prepare_input($_GET['tax']);
  $ean = osc_db_prepare_input($_GET['ean']);
  $pass = EXPORT_CODE;
  $language_code = (isset($_GET['language']) && osc_not_null($_GET['language'])) ? osc_db_prepare_input($_GET['language']) : DEFAULT_LANGUAGE;
  $p = osc_db_prepare_input($_GET['p']);
  $format = basename(osc_db_prepare_input($_GET['format']));
  $cache = osc_db_prepare_input($_GET['cache']);
  $fichier = osc_db_prepare_input($_GET['fichier']);
  $libre = osc_db_prepare_input($_GET['libre']);


  if ($_GET['rep'] == "1") {
    $rep = 'export/secure/';
  } else {
    $rep = 'export/';
  }


// fonction de nettoyage des donnees si presence d'un editeur html
function netoyage_html($CatList, $length) {
  $CatList = html_entity_decode($CatList);
  $CatList = strip_tags ($CatList);
  $CatList = trim ($CatList);
  $CatList = strtolower ($CatList);
  $CatList = str_replace(chr(9),"",$CatList); 
  $CatList = str_replace(chr(10),"",$CatList);
  $CatList = str_replace(chr(13),"",$CatList);
  $CatList = str_replace('&#39;',"'",$CatList);
  $CatList =preg_replace('/\s&nbsp;\s/i', ' ',  $products_description);
  $CatList = preg_replace("[<(.*'?)>]","",$CatList);


  if (strlen($CatList) > $length) {
    $CatList = substr($CatList, 0, $length-3) . "...";
  }
  return $CatList;  
}
 
//On verifie le code avant de lancer les requetes
  if (($verif == true and $p==$pass) OR $verif == false ) {

    $QincludedCategories = $OSCOM_PDO->prepare('select c.categories_id,
                                                    c.parent_id,
                                                    cd.categories_name
                                               from :table_categories c,
                                                    :table_categories_description cd
                                               where c.categories_id = cd.categories_id
                                               and cd.language_id = :language_id
                                             ');
    $QincludedCategories->bindInt(':language_id', (int)$_SESSION['languages_id']);

    $QincludedCategories->execute();
/*
    $included_categories_query = osc_db_query("select c.categories_id,
                                                    c.parent_id, 
                                                    cd.categories_name 
                                               from categories c,
                                                    categories_description cd
                                               where c.categories_id = cd.categories_id 
                                               and cd.language_id = " . (int)$_SESSION['languages_id'] ."
                                           ");
*/
    $inc_cat = array();

  // Identification du nom de la categorie, et l'id de la categorie parent
    while ($included_categories = $QincludedCategories->fetch() ) {
      $inc_cat[] = array ('id' => $included_categories['categories_id'],
                          'parent' => $included_categories['parent_id'],
                          'name' => utf8_decode($included_categories['categories_name'])
//                          'name' => $included_categories['categories_name']
                         );
    }

  $cat_info = array();
  for ($i=0; $i<sizeof($inc_cat); $i++)

    $cat_info[$inc_cat[$i]['id']] = array ('parent'=> $inc_cat[$i]['parent'],
                                           'name'  => $inc_cat[$i]['name'],
                                           'path'  => $inc_cat[$i]['id'],
                                           'link'  => '' 
                                           );

    for ($i=0; $i<sizeof($inc_cat); $i++) {
      $cat_id = $inc_cat[$i]['id'];
      while ($cat_info[$cat_id]['parent'] != 0){
        $cat_info[$inc_cat[$i]['id']]['path'] = $cat_info[$cat_id]['parent'] . '_' . $cat_info[$inc_cat[$i]['id']]['path'];
        $cat_id = $cat_info[$cat_id]['parent'];
      }

      $link_array = preg_split('#_#', $cat_info[$inc_cat[$i]['id']] ['path']);

      for ($j=0; $j<sizeof($link_array); $j++) {
        $cat_info[$inc_cat[$i]['id']]['link'] .= '&nbsp;<a href="' . osc_href_link('index.php', 'cPath=' . $cat_info[$link_array[$j]]['path']) . '"><nobr>' . $cat_info[$link_array[$j]]['name'] . '</nobr></a>&nbsp;&raquo;&nbsp;';
      }
    }

// Requete identifiant les produits disponibles dans le catalogue
    $Qproducts = $OSCOM_PDO->prepare('select p.*,
                                           pd.products_name,
                                           pd.products_description,
                                           pc.categories_id,
                                           pr.date_added as review_date,
                                           pr.customers_name,
                                           pr.reviews_rating,
                                           pt.reviews_text,
                                           pt.languages_id as lngr
                                      from (:table_products p,
                                            :table_products_description pd,
                                            :table_products_to_categories pc)
                                      left join reviews as pr on (p.products_id = pr.products_id)
                                      left join reviews_description as pt ON (pr.reviews_id = pt.reviews_id)
                                      where p.products_id = pd.products_id
                                      and p.products_id = pc.products_id
                                      and p.products_status = :products_status
                                      and p.products_archive = :products_archive
                                      and p.products_quantity > :products_quantity
                                      and products_price_comparison = :products_price_comparison
                                      and pd.language_id = :language_id
                                      order by pc.categories_id,
                                      pd.products_name
                                    ');
    $Qproducts->bindValue(':products_status', '1');
    $Qproducts->bindValue(':products_archive', '0');
    $Qproducts->bindValue(':products_quantity', '0');
    $Qproducts->bindValue(':products_price_comparison', '0');
    $Qproducts->bindValue(':language_id', (int)$_SESSION['languages_id']);

    $Qproducts->execute();



    $product_num = 0;

    while($products =  $Qproducts->fetch() ) {

      $products['products_name'] = utf8_decode($products['products_name']);

      if (intval($products['manufacturers_id']) > 0) {

        $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturers_name
                                               from :table_manufacturers
                                               where manufacturers_id = :manufacturers_id
                                              ');
        $Qmanufacturers->bindInt(':manufacturers_id', (int)$products['manufacturers_id']);

        $Qmanufacturers->execute();

/*
        $manufacturers_query = osc_db_query("select manufacturers_name 
                                             from manufacturers
                                             where manufacturers_id = " . (int)$products['manufacturers_id']
                                           );
*/
        $manufacturers_result = $Qmanufacturers->fetch();
        $products['manufacturers_name'] = utf8_decode($manufacturers_result['manufacturers_name']);
      }
/*
      $special_query = osc_db_query("select specials_new_products_price , 
                                            expires_date , 
                                            specials_date_added 
                                     from specials
                                     where products_id = " . (int)$products['products_id'] . " 
                                     and status = '1' 
                                     limit 1
                                    ");
      $special_result = osc_db_fetch_array($special_query);
*/
      $Qspecials = $OSCOM_PDO->prepare('select specials_new_products_price ,
                                            expires_date ,
                                            specials_date_added
                                         from :table_specials
                                         where products_id = :products_id
                                         and status = :status
                                         limit 1
                                        ');
      $Qspecials->bindInt(':products_id', (int)$products['products_id']);
      $Qspecials->bindValue(':status', '1');

      $Qspecials->execute();

      $special_result = $Qspecials->fetch();

      $product_num++;

//calcul des prix
// la variable $reduc permet de tester s'il y a une promo
      if ($tax == 'true') {
        $price = osc_add_tax($products['products_price'], osc_get_tax_rate($products['products_tax_class_id']));
         $featured_product = 'n';
      } else {
        $price = $products['products_price'];
         $featured_product = 'n';
      }
      if($special_result['specials_new_products_price'] == '' )   {
        $discount_price = '' ;
        $regular_price = $price;
        $reduc = false;
      } else {
         if ($tax == 'true') {
         $discount_price = osc_add_tax($special_result['specials_new_products_price'], osc_get_tax_rate($products['products_tax_class_id']));
         } else {
         $discount_price = $special_result['specials_new_products_price'];
         }
        $regular_price = $price;
        $featured_product = 'y';
        $reduc = true;
      }
  
// Test barcod mod
      if ($ean == 'true') {
        $ean13 = $products['products_ean'];
      }
// Test ecotax
      if ($ecotax) {
        $ecotax_montant = osc_get_ecotax_price_value($products['ecotax_rates_id']);
      } else {
        $ecotax_montant = 0;
      }
      include(DIR_WS_MODULES . 'export/' . $format);
    }

    $content =   $head . $output . $foot;
//Soit on met en cache, soit on affiche le resulat
    if ($cache != "true") {
      Header( $header );
      if ($header2) Header( $header2 );
        echo $content;
      } else {
        $fp = fopen(DIR_FS_CATALOG . DIR_WS_EXT . $rep . $fichier,"w");
        fputs($fp,"$content");
        fclose($fp);
?>

  <div class="contentext">
   <div style="text-align: center; padding-top:200px;">
     <p>Op&eacute;ration r&eacute;alis&eacute;e avec succ&egrave;s - Veuillez fermer cette page <br /></p>
   </div>
   <div style="text-align: center; padding-top:10px;"> Success Operation - Please close this page</div>
   <div style="text-align: center;  padding-top:10px">
    <img src="<?php echo 'images/logo_clicshopping_1.png'; ?>" ></td>
   </div>
 </div>
<?php
      }  
    }
  require('includes/application_bottom.php');
