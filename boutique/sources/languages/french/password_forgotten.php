<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Connexion');
define('NAVBAR_TITLE_2', 'Mot de passe oublié');

define('HEADING_TITLE', 'J\'ai oublié mon mot de passe !');

define('TEXT_MAIN', 'Si vous avez oublié votre mot de passe, entrez votre adresse électronique ci-dessous et nous vous enverrons des instructions pour créer et changer votre mot de passe.');

define('TEXT_PASSWORD_RESET_INITIATED', 'Veuillez vérifier votre email pour les instructions concernant le changement de votre mot de passe. Les instructions contiennent un lien qui sera valide uniquement pendant 24 heures.');

define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Erreur : L\'adresse électronique n\'a pas été trouvée dans notre base, veuillez réessayer. ');
define('TEXT_NO_CUSTOMER_LEVEL', 'Votre compte n\'a pas été validé par notre service commercial, ce qui ne nous permet pas de vous envoyer vos nouveaux paramètres de connexion. N\'hésitez pas à contacter notre support pour toute demande de renseignement complémentaire.');

define('EMAIL_PASSWORD_RESET_SUBJECT', STORE_NAME . ' - Nouveau mot de passe');
define('EMAIL_PASSWORD_RESET_BODY', 'Un nouveau mot de passe a été demandé sur le site  ' . STORE_NAME . '.' . "\n\n" . 'Veuillez suivre ce lien afin de terminer la procédure :' . "\n\n" . '%s' . "\n\n" . 'Ce lien ne sera plus utilisable apre&eagrave;s 24 heures et sera automatiquement changé.' . "\n\n" . 'Pour toute demande ou aide, veuillez nous contacter par email : ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");

define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Envoi du nouveau mot de passe');

define('EMAIL_PASSWORD_REMINDER_BODY', 'Bonjour, ' . "\n\n" . 'Veuillez trouver ci-dessous votre nouveau mot de passe pour vous connecter sur ' . STORE_NAME . ' ' . "\n\n" . 'Votre nom utilisateur :  ' . $email_address . ' ' . "\n\n" . 'Votre nouveau mot de passe : %s' . "\n\n".'Très cordialement,'. "\n\n" . '<strong>L\'équipe '.STORE_NAME.'</strong>'. "\n\n" . '
<p style="font-size:10px;"><u>Avis de confidentialité :</u>'. "\n" . '
Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans être le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez recu ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n" . '
Conformément à la <strong>Loi</strong> dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</p>');

define('SUCCESS_PASSWORD_SENT', 'Un email contenant votre mot de passe va vous être envoyé dans les prochaines minutes...');
define('ERROR_ACTION_RECORDER', 'Erreur: un mot de de passe a déjà été envoyé. Veuillez attendre %s minutes.');
?>
