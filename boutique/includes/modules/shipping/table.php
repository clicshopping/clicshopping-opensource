<?php
  /*
   * table
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:

  */

  class table {
    var $code, $title, $description, $icon, $enabled;

// class constructor
    function table() {
      global $order;

      $this->code = 'table';


      if (MODULE_SHIPPING_TABLE_NAME_TITLE != 'MODULE_SHIPPING_TABLE_NAME_TITLE') {
      $this->title = MODULE_SHIPPING_TABLE_NAME_TITLE;
    } else {
      $this->title = MODULE_SHIPPING_TABLE_TEXT_TITLE;
    }

      $this->description = MODULE_SHIPPING_TABLE_TEXT_DESCRIPTION;
      $this->sort_order = MODULE_SHIPPING_TABLE_SORT_ORDER;


    if (MODULE_SHIPPING_TABLE_IMAGE) {
        $this->icon  = DIR_WS_IMAGES . 'logos/shipping/' . MODULE_SHIPPING_TABLE_IMAGE;
      } else {
      $this->icon  = '';
    }


      if ( osc_get_tax_unallowed($this->code) ) {
        $this->tax_class = MODULE_SHIPPING_TABLE_TAX_CLASS;
    }


      if ( osc_get_shipping_unallowed($this->code) ) {
        $this->enabled = ((MODULE_SHIPPING_TABLE_STATUS == 'True') ? true : false);
      }

      if ( ($this->enabled == true) && ((int)MODULE_SHIPPING_TABLE_ZONE > 0) ) {
        $check_flag = false;
        $check_query = osc_db_query("select zone_id from zones_to_geo_zones where geo_zone_id = '" . MODULE_SHIPPING_TABLE_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
        while ($check = osc_db_fetch_array($check_query)) {
          if ($check['zone_id'] < 1) {
            $check_flag = true;
            break;
          } elseif ($check['zone_id'] == $order->delivery['zone_id']) {
            $check_flag = true;
            break;
          }
        }

        if ($check_flag == false) {
          $this->enabled = false;
        }
      }
    }

// class methods
    function quote($method = '') {
      global $order, $shipping_weight, $shipping_num_boxes;

      if (MODULE_SHIPPING_TABLE_MODE == 'price') {
        $order_total = $this->getShippableTotal();
      } else {
        $order_total = $shipping_weight;
      }

      $table_cost = preg_split("/[:,]/" , MODULE_SHIPPING_TABLE_COST);

      $size = sizeof($table_cost);
      for ($i=0, $n=$size; $i<$n; $i+=2) {
        if ($order_total <= $table_cost[$i]) {
          $shipping = $table_cost[$i+1];
          break;
        }
      }

      if (MODULE_SHIPPING_TABLE_MODE == 'weight') {
        $shipping = $shipping * $shipping_num_boxes;
      }

      $this->quotes = array('id' => $this->code,
                            'module' => MODULE_SHIPPING_TABLE_TEXT_TITLE,
                            'methods' => array(array('id' => $this->code,
                                                     'title' => MODULE_SHIPPING_TABLE_TEXT_WAY,
                                                     'cost' => $shipping + MODULE_SHIPPING_TABLE_HANDLING)));

      if ($this->tax_class > 0) {
        $this->quotes['tax'] = osc_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
      }

      if (osc_not_null($this->icon)) $this->quotes['icon'] = osc_image($this->icon, $this->title);

      return $this->quotes;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = osc_db_query("select configuration_value from configuration where configuration_key = 'MODULE_SHIPPING_TABLE_STATUS'");
        $this->_check = osc_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Livraison au poids ou au montant', 'MODULE_SHIPPING_TABLE_STATUS', 'True', 'Voulez-vous permettre la livraison en fonction du poids total ou du montant de la commande ?', '6', '0', 'osc_cfg_select_option(array(\'True\', \'False\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Titre', 'MODULE_SHIPPING_TABLE_NAME_TITLE', 'Frais sur le Montant total', 'Permet de modifier le titre du module qui s\'affichera dans la page exp&eacute;dition de la boutique.', '6', '1', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Logo', 'MODULE_SHIPPING_TABLE_IMAGE', '', 'Veuillez indiquer le nom de l\'image avec son extension (.gif, .jpg, etc...) &agrave; afficher dans la page paiement de la boutique.<br /><br /><font color=\"#FF0000\"><strong>Note :</strong></font> Le logo doit se trouver dans le dossier sources/image/logos/shipping/', '6', '2', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Tableau de livraison', 'MODULE_SHIPPING_TABLE_COST', '25:8.50,50:5.50,10000:0.00', 'Le co&ucirc;t de livraison est bas&eacute; sur le poids des articles. Exemple : 25:8.50,50:5.50,etc.. Jusqu\'&agrave; 25 Kg -> Prix 8.50, de 50 Kg -> Prix 5.50, etc.. ', '6', '3', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('M&eacute;thode de Tableau', 'MODULE_SHIPPING_TABLE_MODE', 'weight', 'Le co&ucirc;t de livraison est bas&eacute; sur le total de la commande ou sur le poids des articles command&eacute;s ?', '6', '04', 'osc_cfg_select_option(array(\'weight\', \'price\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Co&ucirc;ts de manutention', 'MODULE_SHIPPING_TABLE_HANDLING', '0', 'Les co&ucirc;ts de manutention pour cette m&eacute;thode de livraison.', '6', '5', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Type de taxe', 'MODULE_SHIPPING_TABLE_TAX_CLASS', '0', 'Employez la classe suivante de taxe sur les co&ucirc;ts de livraison.', '6', '6', 'osc_get_tax_class_title', 'osc_cfg_pull_down_tax_classes(', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone de livraison', 'MODULE_SHIPPING_TABLE_ZONE', '0', 'Permettre seulement cette m&eacute;thode de livraison pour la zone choisie.', '6', '7', 'osc_get_zone_class_title', 'osc_cfg_pull_down_zone_classes(', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('order de tri d\'affichage', 'MODULE_SHIPPING_TABLE_SORT_ORDER', '0', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier)', '6', '8', now())");
      osc_db_query("update configuration set configuration_value = '1' where configuration_key = 'WEBSITE_MODULE_INSTALLED'");

    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }


    function keys() {
      return array('MODULE_SHIPPING_TABLE_STATUS',
                  'MODULE_SHIPPING_TABLE_NAME_TITLE',
                  'MODULE_SHIPPING_TABLE_IMAGE',
                  'MODULE_SHIPPING_TABLE_COST',
                  'MODULE_SHIPPING_TABLE_MODE',
                  'MODULE_SHIPPING_TABLE_HANDLING',
                  'MODULE_SHIPPING_TABLE_TAX_CLASS',
                  'MODULE_SHIPPING_TABLE_ZONE',
                  'MODULE_SHIPPING_TABLE_SORT_ORDER'
                  );
    }

    function getShippableTotal() {
      global $order, $currencies;

      $order_total = $_SESSION['cart']->show_total();

      if ($order->content_type == 'mixed') {
        $order_total = 0;

        for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
          $order_total += $currencies->calculate_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']);

          if (isset($order->products[$i]['attributes'])) {
            foreach ( $order->products[$i]['attributes'] as $option => $value ) {
              $virtual_check_query = osc_db_query("select count(*) as total from products_attributes pa, products_attributes_download pad where pa.products_id = '" . (int)$order->products[$i]['id'] . "' and pa.options_values_id = '" . (int)$value['value_id'] . "' and pa.products_attributes_id = pad.products_attributes_id");
              $virtual_check = osc_db_fetch_array($virtual_check_query);

              if ($virtual_check['total'] > 0) {
                $order_total -= $currencies->calculate_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']);
              }
            }
          }
        }
      }

      return $order_total;
    }
  }
?>
