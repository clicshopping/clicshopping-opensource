<?php
/**
 * d_twitter.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TWITTER_TITLE', 'Souhaitez-installer le module Twitter pour envoyer des Twits ?');
define('MODULE_ADMIN_DASHBOARD_TWITTER_DESCRIPTION', 'Ce module nécessite de s\'inscrire sur <a href=\"http://www.twitter.com\" target=\"_blank\">twitter.com</a>');
define('TEXT_TITLE_TWITTER','Mon twit du jour');
?>
