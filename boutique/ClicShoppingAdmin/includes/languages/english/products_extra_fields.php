<?php
/**
 * products_extra_fields.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Product Extra Fields');
define('SUBHEADING_TITLE', 'Add a new field');

define('HEADING_TITLE_INSERT','Insert a new field');
define('HEADING_TITLE_MODIFIY','Modify the field');

define('TABLE_HEADING_ID_FIELDS', 'Id field');
define('TABLE_HEADING_DELETE_FIELDS', 'Delete');
define('TABLE_HEADING_FIELDS', 'Field Name');
define('TABLE_HEADING_FIELDS_TYPE','Fied tupe');
define('TABLE_HEADING_CUSTOMERS_GROUP','Customer group');
define('TABLE_HEADING_ORDER', 'Sort Order');
define('TABLE_HEADING_LANGUAGE', 'Language');
define('TABLE_HEADING_STATUS_DIPSLAY', 'Catalog status display');

define ('TEXT_ALL_LANGUAGES', 'All');
define('ENTRY_TEXT','Simple text field (short)');
define('ENTRY_TEXT_AREA','Description field (long)');
define('ENTRY_TEXT_CHECKBOX','Checkbox');
define('TITLE_AIDE_OPTIONS','Help on the add field');
define('TEXT_AIDE_OPTIONS','<li>In mode B2B (without new client) allow to see all the fields created for a new client in the description product');

?>