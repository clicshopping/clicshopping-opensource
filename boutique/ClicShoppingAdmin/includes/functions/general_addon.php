<?php
/*
 * general_addon.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/


/**
 *  This will remove HTML tags, javascript sections and white space. It will also convert some common HTML entities to their text equivalent.
 *
 * @param string $str, html code
 * @return string $str html converted
 * @access public
*/
  function osc_strip_html_tags($str) {

    $search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                      "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
                      //"'([rn])[s]+'",                // Strip out white space
                      "'&(quot|#34);'i",                // Replace HTML entities
                      "'&(amp|#38);'i",
                      "'&(lt|#60);'i",
                      "'&(gt|#62);'i",
                      "'&(nbsp|#160);'i",
                      "'&(iexcl|#161);'i",
                      "'&(cent|#162);'i",
                      "'&(pound|#163);'i",
                      "'&(copy|#169);'i",
                      "'&#(d+);'e"
                   );                    // evaluate as php

    $replace = array ("",
                      "",
                      //"\1",
                      "\"",
                      "&",
                      "<",
                      ">",
                      " ",
                      chr(161),
                      chr(162),
                      chr(163),
                      chr(169),
                      "chr(\1)"
                    );

    return preg_replace($search, $replace, $str);
  }


/**
 *  Alias function to osc_get_countries, which also returns the countries iso codes
 * @param string $countries_id
 * @access public
 */
////

  function osc_get_countries_with_iso_codes($countries_id) {
    return osc_get_countries($countries_id, true);
  }

/**
 * Status pcountries -  Sets the status of  countries
 * 
 * @param string countries_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_countries_status($countries_id, $status) {
    if ($status == '1') {
      return osc_db_query("update countries
                           set status = '1' 
                           where countries_id = '" . (int)$countries_id . "'
                         ");
    } elseif ($status == '0') {
      return osc_db_query("update countries
                           set status = '0' 
                           where countries_id = '" . (int)$countries_id . "'
                         ");
    } else {
      return -1;
    }
  } 


/**
 * the name of status invoice
 * 
 * @param string  $orders_status_invoice_id, $language_id
 * @return string $orders_invoice_status['orders_status_invoice_name'],  name of the  status invoice
 * @access public
 */
  function osc_get_orders_status_invoice_name($orders_status_invoice_id, $language_id = '') {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qinvoice = $OSCOM_PDO->prepare('select orders_status_invoice_name
                                     from :table_orders_status_invoice
                                     where orders_status_invoice_id = :orders_status_invoice_id
                                     and language_id =:language_id
                                  ');
    $Qinvoice->bindInt(':orders_status_invoice_id', (int)$orders_status_invoice_id);
    $Qinvoice->bindInt(':language_id', (int)$language_id);

    $Qinvoice->execute();

    return $Qinvoice->value('orders_status_invoice_name');
  }

/**
 * Array of the name of status invoice
 * 
 * @param string  $orders_status_invoice_id, $language_id
 * @return string orders_invoice_status_array,  array if the name status invoice
 * @access public
 */
  function osc_get_orders_invoice_status() {
    global $OSCOM_PDO;

    $orders_status_invoice_array = array();

    $Qinvoice = $OSCOM_PDO->prepare('select orders_status_invoice_id,
                                            orders_status_invoice_name
                                     from :table_orders_status_invoice
                                     where language_id = :language_id
                                     order by orders_status_invoice_id
                                  ');
    $Qinvoice->bindInt(':language_id', (int)$_SESSION['languages_id']);

    $Qinvoice->execute();


    while ($orders_invoice_status = $Qinvoice->fetch() ) {
      $orders_status_invoice_array[] = array('id' => $orders_invoice_status['orders_status_invoice_id'],
                                     'text' => $orders_invoice_status['orders_status_invoice_name']);
    }
    return $orders_status_invoice_array;
  }


/**
 * the name of tracking name 
 * 
 * @param string  $orders_status_tracking_id, $language_id
 * @return string $orders_tracking_status['orders_status_tracking_name'],  name of the  tracking status
 * @access public
 */
  function osc_get_orders_status_tracking_name($orders_status_tracking_id, $language_id = '') {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qtracking = $OSCOM_PDO->prepare('select orders_status_tracking_name
                                       from :table_orders_status_tracking
                                       where orders_status_tracking_id = :orders_status_tracking_id
                                       and language_id = :language_id
                                      ');
    $Qtracking->bindInt(':orders_status_tracking_id', (int)$orders_status_tracking_id);
    $Qtracking->bindInt(':language_id', (int)$language_id);

    $Qtracking->execute();

    return $Qtracking->value('orders_status_tracking_name');
  }


/**
 * the name of tracking link 
 * 
 * @param string  $orders_status_tracking_id, $language_id
 * @return string $orders_tracking_status['orders_status_tracking_name'],  name of the  tracking status
 * @access public
 */
  function osc_get_orders_tracking_link($orders_status_tracking_id, $language_id = '') {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qtracking = $OSCOM_PDO->prepare('select orders_status_tracking_link
                                     from :table_orders_status_tracking
                                     where orders_status_tracking_id = :orders_status_tracking_id
                                     and language_id = :language_id
                                     ');
    $Qtracking->bindInt(':orders_status_tracking_id', (int)$orders_status_tracking_id);
    $Qtracking->bindInt(':language_id', (int)$language_id);

    $Qtracking->execute();

    return $Qtracking->value('orders_status_tracking_link');
  }


/**
 * Array of the name of tracking status
 * 
 * @param string  $orders_status_tracking_id, $language_id
 * @return string orders_tracking_status_array,  array if the name status tracking
 * @access public
 */
  function osc_get_orders_tracking_status() {
    global $OSCOM_PDO;

    $orders_status_tracking_array = array();

    $QordersStatusTracking = $OSCOM_PDO->prepare('select orders_status_tracking_id,
                                                        orders_status_tracking_name
                                                  from :table_orders_status_tracking
                                                  where language_id = :language_id
                                                  order by orders_status_tracking_id
                                        ');
    $QordersStatusTracking->bindInt(':language_id', (int)$_SESSION['languages_id'] );

    $QordersStatusTracking->execute();

    while ($orders_tracking_status = $QordersStatusTracking->fetch() ) {
      $orders_status_tracking_array[] = array('id' => $orders_tracking_status['orders_status_tracking_id'],
                                              'text' => $orders_tracking_status['orders_status_tracking_name']);
    }
    return $orders_tracking_status_array;
  }

/**
 * the products quantity unit title
 * 
 * @param string  $products_quantity_unit_id, $language_id
 * @return string $products_quantity_unit_['products quantity unit_title'],  name of the he products quantity unit
 * @access public
 */
  function osc_get_products_quantity_unit_title($products_quantity_unit_id, $language_id = '') {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QproductsQuantityUnitTitle = $OSCOM_PDO->prepare('select products_quantity_unit_title
                                                       from :table_products_quantity_unit
                                                       where products_quantity_unit_id = :products_quantity_unit_id
                                                       and language_id = :language_id
                                                     ');

    $QproductsQuantityUnitTitle->bindInt(':products_quantity_unit_id', (int)$products_quantity_unit_id );
    $QproductsQuantityUnitTitle->bindInt(':language_id', (int)$language_id );

    $QproductsQuantityUnitTitle->execute();

    return $QproductsQuantityUnitTitle->value('products_quantity_unit_title');
  }


/**
 * the products quantity unit name
 * 
 * @param string  $products_quantity_unit_id, $language_id
 * @return string $products_quantity_unit['products_quantity_unit_title'],  name of the products quantity unit
 * @access public
 */
  function osc_get_products_quantity_unit_tilte() {
    global $OSCOM_PDO;

    $products_quantity_unit_array = array();

    $QproductsQuantityUnitTilte = $OSCOM_PDO->prepare('select products_quantity_unit_id,
                                                                products_quantity_unit_title
                                                       from :table_products_quantity_unit
                                                       where language_id = :language_id
                                                       order by products_quantity_unit_id
                                                    ');

    $QproductsQuantityUnitTilte->bindInt(':language_id', (int)$_SESSION['languages_id'] );

    $QproductsQuantityUnitTilte->execute();

    while ($products_quantity_unit = $QproductsQuantityUnitTilte->fetch() ) {
      $products_quantity_unit_array[] = array('id' => $products_quantity_unit['products_quantity_unit_id'],
                                              'text' => $products_quantity_unit['products_quantity_unit_title']);
    }
    return $products_quantity_unit_array;
  }

/**
 * Products model
 * 
 * @param string  $product_id
 * @return string $product['products_model'], products model
 * @access public
 */
    function osc_get_products_model($product_id) {
    global $OSCOM_PDO;

      $QproductsModel = $OSCOM_PDO->prepare('select products_model
                                            from :table_products
                                            where products_id = :products_id
                                           ');

      $QproductsModel->bindInt(':products_id', (int)$product_id );

      $QproductsModel->execute();

      return $QproductsModel->value('products_model');
    }

/**
 * Title Name of the submit
 * 
 * @param string  $product_id, $language_id
 * @return string product['products_head_title_tag'], description name
 * @access public
 */
  function osc_get_products_head_title_tag($product_id, $language_id) {
    global $OSCOM_PDO;

    $Qproduct = $OSCOM_PDO->prepare('select products_head_title_tag
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                    ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);

    $Qproduct->execute();

    return $Qproduct->value('products_head_title_tag');
  }

/**
 * Description Name
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_head_desc_tag'], description name
 * @access public
 */
  function osc_get_products_head_desc_tag($product_id, $language_id) {
    global $OSCOM_PDO;

    $Qproduct = $OSCOM_PDO->prepare('select products_head_desc_tag
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);

    $Qproduct->execute();

    return $Qproduct->value('products_head_desc_tag');
  }

/**
 * keywords Name
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_head_keywords_tag'], keywords name
 * @access public
 */
  function osc_get_products_head_keywords_tag($product_id, $language_id) {
    global $OSCOM_PDO;

    $Qproduct = $OSCOM_PDO->prepare('select products_head_keywords_tag
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);

    $Qproduct->execute();

    return $Qproduct->value('products_head_keywords_tag');
  }


/**
 * Tag Name
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_head_tag'], keywords name
 * @access public
 */
  function osc_get_products_tag($product_id, $language_id) {
    global $OSCOM_PDO;

    $Qproduct = $OSCOM_PDO->prepare('select products_head_tag
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);

    $Qproduct->execute();

    return $Qproduct->value('products_head_tag');
  }

/**
 * Shipping delay of the product
 * 
 * @param string  $product_id, $language_id
 * @return string $product['products_shipping_delay'], url of the product
 * @access public
 */
  function osc_get_products_shipping_delay($product_id, $language_id) {
    global $OSCOM_PDO;

    $Qproduct = $OSCOM_PDO->prepare('select products_shipping_delay
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                   ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);

    $Qproduct->execute();

    return $Qproduct->value('products_shipping_delay');
  }


/**
 * Display the list of the manufacturers
 * 
 * @param string 
 * @return string 
 * @access public
 */
  function manufacturers_list(){
    global $manufacturer, $OSCOM_PDO;;

    $Qmanufacturers = $OSCOM_PDO->prepare('select m.manufacturers_id,
                                                m.manufacturers_name
                                          from :table_manufacturers m
                                          order by m.manufacturers_name ASC
                                          ');

    $Qmanufacturers->execute();

    $return_string = '<select name="manufacturer" onChange="this.form.submit();">';
    $return_string .= '<option value="' . 0 . '">' . TEXT_ALL_MANUFACTURERS . '</option>';

    while($manufacturers = $Qmanufacturers->fetch() ){
      $return_string .= '<option value="' . $manufacturers['manufacturers_id'] . '"';
      if($manufacturer && $manufacturers['manufacturers_id'] == $manufacturer) $return_string .= ' SELECTED';
      $return_string .= '>' . $manufacturers['manufacturers_name'] . '</option>';
    }

    $return_string .= '</select>';

    return $return_string;
  }

/**
 * the manufacturer_description
 * 
 * @param string  $manufacturer_id, $language_id
 * @return string $manufacturer['manufacturer_description'],  description of the manufacturer
 * @access public
 */
  function osc_get_manufacturer_description($manufacturers_id, $language_id) {
    global $OSCOM_PDO;
    
    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturer_description
                                          from :table_manufacturers_info
                                          where manufacturers_id = :manufacturers_id
                                          and languages_id = :language_id
                                        ');

    $Qmanufacturers->bindInt(':manufacturers_id', (int)$manufacturers_id);
    $Qmanufacturers->bindInt(':language_id', (int)$language_id);
    $Qmanufacturers->execute();

    return $Qmanufacturers->value('manufacturer_description');
  }

/**
 * the manufacturer seo description
 * 
 * @param string  $manufacturer_id, $language_id
 * @return string $manufacturer['manufacturers_seo_description'],  seo description of the manufacturer
 * @access public
 */
  function osc_get_manufacturer_seo_description($manufacturer_id, $language_id) {
    global $OSCOM_PDO;
    
    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturer_seo_description
                                          from :table_manufacturers_info
                                          where manufacturers_id = :manufacturers_id
                                          and languages_id = :language_id
                                        ');

    $Qmanufacturers->bindInt(':manufacturers_id', (int)$manufacturer_id);
    $Qmanufacturers->bindInt(':language_id', (int)$language_id);
    $Qmanufacturers->execute();


    return $Qmanufacturers->value('manufacturer_seo_description');
  }

/**
 * the manufacturer seo title
 * 
 * @param string  $manufacturer_id, $language_id
 * @return string $manufacturer['manufacturers_seo_title'],  seo title of the manufacturer
 * @access public
 */
  function osc_get_manufacturer_seo_title($manufacturer_id, $language_id) {
    global $OSCOM_PDO;

    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturer_seo_title
                                          from :table_manufacturers_info
                                          where manufacturers_id = :manufacturers_id
                                          and languages_id = :language_id
                                        ');

    $Qmanufacturers->bindInt(':manufacturers_id', (int)$manufacturer_id);
    $Qmanufacturers->bindInt(':language_id', (int)$language_id);
    $Qmanufacturers->execute();

    return $Qmanufacturers->value('manufacturer_seo_title');
  }


/**
 * the manufacturer seo keyword
 * 
 * @param string  $manufacturer_id, $language_id
 * @return string $manufacturer['manufacturers_seo_keyword'],  seo keyword of the manufacturer
 * @access public
 */
  function osc_get_manufacturer_seo_keyword($manufacturer_id, $language_id) {
    global $OSCOM_PDO;

    $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturer_seo_keyword
                                          from :table_manufacturers_info
                                          where manufacturers_id = :manufacturers_id
                                          and languages_id = :language_id
                                        ');

    $Qmanufacturers->bindInt(':manufacturers_id', (int)$manufacturer_id);
    $Qmanufacturers->bindInt(':language_id', (int)$language_id);
    $Qmanufacturers->execute();

    return $Qmanufacturers->value('manufacturer_seo_keyword');
  }


/**
 * Status products manufacturers  - Sets the status of a product on manufacturers
 * 
 * @param string manufacturers_id, status
 * @return string status on or off
 * @access public 
 */
  
  function osc_set_manufacturers_status($manufacturers_id, $status) {
    global $OSCOM_PDO;
    
    if ($status == '1') {

      return $OSCOM_PDO->save('manufacturers', ['manufacturers_status' => 1,
                                              'date_added' => 'null',
                                              'last_modified' => 'null'],
                                              ['manufacturers_id' => (int)$manufacturers_id]
                              );

    } elseif ($status == '0') {

      return $OSCOM_PDO->save('manufacturers', ['manufacturers_status' => 0,
                                                'last_modified' => 'now()'],
                                                ['manufacturers_id' => (int)$manufacturers_id]
                              );

   } else {
      return -1;
    }
  }

/**
 * Display the list of the suppliers
 * 
 * @param string 
 * @return string 
 * @access public
 */
  function osc_suppliers_list(){
    global $suppliers, $OSCOM_PDO;;

    $Qsuppliers = $OSCOM_PDO->prepare('select suppliers_id,
                                              suppliers_name
                                       from :table_suppliers
                                       order by suppliers_name ASC
                                      ');
    $Qsuppliers->execute();

    $return_string = '<select name="supplier" onChange="this.form.submit();">';
    $return_string .= '<option value="' . 0 . '">' . TEXT_ALL_SUPPLIERS . '</option>';

      while($suppliers = $Qsuppliers->fetch() ){
        $return_string .= '<option value="' . $suppliers['suppliers_id'] . '"';
        if($suppliers && $suppliers['suppliers_id'] == $suppliers) $return_string .= ' SELECTED';
        $return_string .= '>' . $suppliers['suppliers_name'] . '</option>';
      }
    $return_string .= '</select>';
    return $return_string;
  }

/**
 * the supplier_url
 * 
 * @param string  $supplier_id, $language_id
 * @return string $supplier['supplier_description'],  description of the supplier
 * @access public
 */
  function osc_get_supplier_url($supplier_id, $language_id) {
    global $OSCOM_PDO;

    $Qsuppliers = $OSCOM_PDO->prepare('select suppliers_url
                                    from suppliers_info
                                    where suppliers_id = :suppliers_id
                                    and languages_id = :language_id
                                      ');
    $Qsuppliers->bindInt(':suppliers_id', (int)$supplier_id);
    $Qsuppliers->bindInt(':language_id', (int)$language_id);

    $Qsuppliers->execute();

    return $Qsuppliers->value('suppliers_url');
  }


/**
 * Status products suppliers  - Sets the status of a product on suppliers
 * 
 * @param string suppliers_id, status
 * @return string status on or off
 * @access public 
 */
  
  function osc_set_suppliers_status($suppliers_id, $status) {
    global $OSCOM_PDO;

    if ($status == '1') {

      return $OSCOM_PDO->save('suppliers', ['suppliers_status' => 1,
                                            'date_added' => 'null',
                                            'last_modified' => 'null'
                                           ],
                                           ['suppliers_id' => (int)$suppliers_id]
                              );

    } elseif ($status == '0') {

      return $OSCOM_PDO->save('suppliers', ['suppliers_status' => 0,
                                            'last_modified' => 'now()'
                                           ],
                                           ['suppliers_id' => (int)$suppliers_id]
                              );

    } else {
      return -1;
    }
  }


////
// Wrapper for class_exists() function
// This function is not available in all PHP versions so we test it before using it.
  function osc_class_exists($class_name) {
    if (function_exists('class_exists')) {
      return class_exists($class_name);
    } else {
      return true;
    }
  }

////
// Alias function pour afficher la liste des status dans l'administration configuration
  function osc_cfg_pull_down_order_status_list($order_statut_id) {
    return osc_draw_pull_down_menu('configuration_value', osc_get_orders_status(), $order_statut_id);
  }

/**
 * Status modification of page manager - Sets the status of a page
 * 
 * @param string pages_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_page_status($pages_id, $status) {
    global $OSCOM_PDO;
    
    if ($status == '1') {

      return $OSCOM_PDO->save('pages_manager', ['status' => 1,
                                                'page_date_closed' => 'null',
                                                'date_status_change' => 'now()'
                                               ],
                                               ['pages_id' => (int)$pages_id]
                            );

    } elseif ($status == '0') {

      return $OSCOM_PDO->save('pages_manager', ['status' => 0,
                                                'date_status_change' => 'now()'
                                               ],
                                               ['pages_id' => (int)$pages_id]
                            );

    } else {
      return -1;
    }
  }

/**
 * Status products comparison - Sets the products_price_comparison of priducts
 * 
 * @param string products_id, products_price_comparison
 * @return string status on or off
 * @access public 
 */
  function osc_set_product_products_price_comparison($products_id, $products_price_comparison) {
    global $OSCOM_PDO;

    if ($products_price_comparison == '1') {

      return $OSCOM_PDO->save('products', ['products_price_comparison' => 1,
                                           'products_last_modified' => 'now()'
                                          ],
                                          ['products_id' => (int)$products_id]
                              );

    } elseif ($products_price_comparison == '0') {

      return $OSCOM_PDO->save('products', ['products_price_comparison' => 0,
                                          'products_last_modified' => 'now()'
                                          ],
                                          ['products_id' => (int)$products_id]
                              );

    } else {
      return -1;
    }
  }


/**
 * Status products only online - Sets the products_only_online of products
 * 
 * @param string products_id, products_only_online
 * @return string status on or off
 * @access public 
 */
  function osc_set_product_products_only_online($products_id, $products_only_online) {
    global $OSCOM_PDO;

    if ($products_only_online == '1') {

      return $OSCOM_PDO->save('products', ['products_only_online' => 1,
                                          'products_last_modified' => 'now()'
                                          ],
                                          ['products_id' => (int)$products_id]
                             );

    } elseif ($products_only_online == '0') {

      return $OSCOM_PDO->save('products', ['products_only_online' => 0,
                                          'products_last_modified' => 'now()'
                                          ],
                                          ['products_id' => (int)$products_id]
                              );

    } else {
      return -1;
    }
  }

/**
 * Status products archive - Sets the archive of a productts
 * 
 * @param string products_id, archive
 * @return string status on or off
 * @access public 
 */
  function osc_set_product_archive_status($products_id, $archive) {
    global $OSCOM_PDO;
    
    if ($archive == '1') {

      return $OSCOM_PDO->save('products', ['products_archive' => 1,
                                          'products_last_modified' => 'now()'
                                          ],
                                          ['products_id' => (int)$products_id]
                               );

    } elseif ($archive == '0') {

      return $OSCOM_PDO->save('products', ['products_archive' => 0,
                                          'products_last_modified' => 'now()'
                                          ],
                                          ['products_id' => (int)$products_id]
                              );
    } else {
      return -1;
    }
  }



/**
 * Status products favorites products -  Sets the status of a favrite product
 * 
 * @param string products_heart_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_products_heart_status($products_heart_id, $status) {
    global $OSCOM_PDO;

    if ($status == '1') {

      return $OSCOM_PDO->save('products_heart', ['status' => 1,
                                                'scheduled_date' => 'null',
                                                'expires_date' => 'null',
                                                'date_status_change' => 'null'
                                                ],
                                                ['products_heart_id' => (int)$products_heart_id]
                            );

    } elseif ($status == '0') {

      return $OSCOM_PDO->save('products_heart', ['status' => 0,
                                                 'date_status_change' => 'now()'
                                                ],
                                                ['products_heart_id' => (int)$products_heart_id]
                             );

    } else {
      return -1;
    }
  }

/**
 * Status contact customers -  Sets the archive of a contact customers
 * 
 * @param string contact_customers_id, contact_customers_archive
 * @return string contact_customers_archive on or off
 * @access public 
 */
  function osc_set_contact_customers_archive($contact_customers_id, $contact_customers_archive) {
    global $OSCOM_PDO;

    if ($contact_customers_archive == '1') {

      return $OSCOM_PDO->save('contact_customers', ['contact_customers_archive' => 1],
                                                  ['contact_customers_id' => (int)$contact_customers_id]
      );

    } elseif ($contact_customers_archive == '0') {

      return $OSCOM_PDO->save('contact_customers', ['contact_customers_archive' => 0],
                                                    ['contact_customers_id' => (int)$contact_customers_id]
                              );

    } else {
      return -1;
    }
  } 

/*
 * Status language -  Sets the status of a language
 * 
 * @param string languages_id, status
 * @return string status on or off
 * @access public 
 */

  function osc_set_language_status($languages_id, $status) {
    global $OSCOM_PDO;

    if ($status == '1') {

      return $OSCOM_PDO->save('languages', ['status' => 1],
                                           ['languages_id' => (int)$languages_id]
                              );

    } elseif ($status == '0') {

      return $OSCOM_PDO->save('languages', ['status' => 0],
                                            ['languages_id' => (int)$languages_id]
                              );

    } else {
      return -1;
    }
  } 


/**
 * osc_set_products_related_cross_sell_status : Sets the status of a related products cross sell
 * 
 * @param string $products_related_id_master, $products_cross_sell
 * @return string , the status of cross sell status
 * @access public
 */
  function osc_set_products_cross_sell_status($products_related_id_master, $products_cross_sell, $products_related_id) {
    global $OSCOM_PDO;
    
    if ($products_cross_sell == '1') {

      return $OSCOM_PDO->save('products_related', ['products_cross_sell' => 1],
                                                  ['products_related_id_master' => (int)$products_related_id_master,
                                                    'products_related_id' => (int)$products_related_id
                                                  ]
                             );

    } elseif ($products_cross_sell == '0') {

       return $OSCOM_PDO->save('products_related', ['products_cross_sell' => 0],
                                                   ['products_related_id_master' => (int)$products_related_id_master,
                                                    'products_related_id' => (int)$products_related_id
                                                   ]
                              );

    } else {
      return -1;
    }
  }

/**
 * osc_set_products_related_status : Sets the status of a related products
 * 
 * @param string $products_related_id_master, $products_related
 * @return string , the status of related products 
 * @access public
 */
  function osc_set_products_related_status($products_related_id_master, $products_related, $products_related_id) {
    global $OSCOM_PDO;

    if ($products_related == '1') {

      return $OSCOM_PDO->save('products_related', ['products_related' => 1],
                                                  ['products_related_id_master' => (int)$products_related_id_master,
                                                   'products_related_id' => (int)$products_related_id
                                                  ]
                             );

    } elseif ($products_related == '0') {

      return $OSCOM_PDO->save('products_related', ['products_related' => 0],
                                                  ['products_related_id_master' => (int)$products_related_id_master,
                                                   'products_related_id' => (int)$products_related_id
                                                  ]
                              );

    } else {
      return -1;
    }
  }


/**
 * osc_set_products_mode_B2B : Sets the status of a related products 2b2 mode
 * 
 * @param string $products_related_id_master, $products_related
 * @return string , the status of related products 
 * @access public
 */
  function osc_set_products_mode_b2b_status($products_related_id_master, $products_mode_b2b, $products_related_id) {
    global $OSCOM_PDO;

    if ($products_mode_b2b == '1') {
      return $OSCOM_PDO->save('products_related', ['products_mode_b2b' => 1],
                                                  ['products_related_id_master' => (int)$products_related_id_master,
                                                    'products_related_id' => (int)$products_related_id
                                                  ]
                              );

    } elseif ($products_mode_b2b == '0') {
      return $OSCOM_PDO->save('products_related', ['products_mode_b2b' => 0],
                                                  ['products_related_id_master' => (int)$products_related_id_master,
                                                    'products_related_id' => (int)$products_related_id
                                                  ]
                             );
    } else {
      return -1;
    }
  }


// generation du statut d'edition de la commande
  function osc_cfg_pull_down_order_statuses_invoice($order_status_invoice_id, $key = '') {
    global $OSCOM_PDO;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $statuses_invoice_array = array(array('id' => '0',
                                          'text' => TEXT_DEFAULT)
                                   );

    $QstatusesInvoice = $OSCOM_PDO->prepare('select orders_status_invoice_id,
                                                   orders_status_invoice_name
                                            from :table_orders_status_invoice
                                            where language_id = :language_id
                                        ');
    $QstatusesInvoice->bindInt(':language_id', (int)$_SESSION['languages_id']);

    $QstatusesInvoice->execute();

    while ($statuses_invoice = $QstatusesInvoice->fetch() ) {
      $statuses_invoice_array[] = array('id' => $statuses_invoice['orders_status_invoice_id'],
                                        'text' => $statuses_invoice['orders_status_invoice_name']);
    }

    return osc_draw_pull_down_menu($name, $statuses_invoice_array, $order_status_invoice_id);
  }

// generation du statut d'edition de la commande dans le module de paiement
  function osc_get_order_status_invoice_name($order_status_invoice_id, $language_id = '') {
    global $OSCOM_PDO;
    

    if ($order_status_invoice_id < 1) return TEXT_DEFAULT;

    if (!is_numeric($language_id)) $language_id = $_SESSION['languages_id'];

    $QstatusesInvoice = $OSCOM_PDO->prepare('select orders_status_invoice_name
                                            from :table_orders_status_invoice
                                            where orders_status_invoice_id = :orders_status_invoice_id
                                            and language_id = :language_id
                                          ');
    $QstatusesInvoice->bindInt(':orders_status_invoice_id',  (int)$order_status_invoice_id);
    $QstatusesInvoice->bindInt(':language_id', (int)$language_id );

    $QstatusesInvoice->execute();

    return $QstatusesInvoice->value('orders_status_invoice_name');
  }


//  Dynamic Template System
//// Return an array of the catalog directory. mechanism for reading this.  

  function osc_list_catalog_files () {
    $dir = dir(DIR_FS_CATALOG);
    $result = array(); 

    $exclude = array('ajax_search.php',
                     'autocomplete_search.php',
                     'checkout_process.php',
                     'checkout_no_success.php',
                     'download.php',
                     'export_price_comparison.php',
                     'google_sitemap_categories.php',
                     'google_sitemap_index.php',
                     'google_sitemap_products.php',
                     'google_sitemap_blog_categories.php',
                     'google_sitemap_featured_products.php',
                     'google_sitemap_blog_content.php',
                     'google_sitemap_manufacturers.php',
                     'google_sitemap_page_manager.php',
                     'google_sitemap_products_heart.php',
                     'google_sitemap_specials.php',
                     'google_sitemap_fetured_products.php',
                     'opensearch.php',
                     'orders_invoice.php',
                     'popup_search_help.php', 
                     'popup_image.php', 
                     'popup_page_manager_account_history.php',
                     'redirect.php', 
                     'response_paybox.php',
                     'rss.php',
                     'thema_template.php',
                     );

    while (false !== ($file = $dir->read())) {
      if($file != '.' && $file != '..' && $file != '.html' && !is_dir($file) && (substr($file, -3, 3) == 'php') && !in_array($file, $exclude)) {
          $result[] =  $file;
      }
    }

     $dir->close();

   return $result;
  }

// Alias function for module [boxes] configuration value
// template system  
  function osc_cfg_select_pages($key_value, $key = '') {
    $name = ((osc_not_null($key)) ? 'configuration[' . $key . '][]' : 'configuration_value');
    $select_array = osc_list_catalog_files();
    sort($select_array);

    $selected_array = explode(';', $key_value);

    if($key_value === 'all') { 
      $checkall = "CHECKED"; 
    } else { 
      $checkall = "UNCHECKED"; 
    }

      $string = '<fieldset>';    
      $string .= '<input type="radio" class="AllPages"  name="' . $name . '" value="all" ' . $checkall . ' />' . ALL_PAGES . '<br />';
      $string .= '<p><strong>&nbsp;&nbsp;' . ONE_BY_ONE . '</strong><br />';
      $string .= '<input type="checkbox" id="CheckAll" class="CheckAll" name="CheckAll" /><label id="CheckAllLabel" for="CheckAll">' . CHECK_ALL . '</label></p>';

      for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
        $string .= '&nbsp;&nbsp;<input type="checkbox" id="file_' . $i . '" class="ThisPage" name="' . $name . '" value="' . $select_array[$i] . ';"';
        if ( isset($selected_array) ) {
          foreach($selected_array as $value) {
            if ($select_array[$i] == $value) $string .= ' CHECKED';
          }
        }
        $string .= '><label class="ThisPage" for="file_' . $i . '">' . $select_array[$i] . '</label><br />';
      }
      $string .= '</fieldset>';
      $string .= "<script type=\"text/javascript\">
  jQuery(document).ready(function () {
    $('.AllPages').click(
      function() {
        $('.ThisPage').prop('checked', false);
        $('.CheckAll').prop('checked', false);
        $('#CheckAllLabel').text('" . CHECK_ALL . "');
      }
    );
    $('.CheckAll').click(
      function () {
        $(this).parents('fieldset:eq(0)').find(':checkbox').prop('checked', this.checked);
        $('.AllPages').prop('checked', (!this.checked));
        if (this.checked) {
          $('#CheckAllLabel').text('" . DESELECT_ALL . "');
        } else {
          $('#CheckAllLabel').text('" . CHECK_ALL . "');
        }
      }
    );
    $('.ThisPage').click(
      function() {
        var n = $( \"input.ThisPage:checked\" ).length;
        if (n >0) {
          $('.AllPages').prop('checked', false);
        } else {
          $('.AllPages').prop('checked', true);
        }
      }
    );
  });
</script>";
      return $string;
  }
// eof Dynamic Template System    



/**
 * Return human readable sizes
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.3.0
 * @link        http://aidanlister.com/repos/v/function.size_readable.php
 * @param       int     $size        size in bytes
 * @param       string  $max         maximum unit
 * @param       string  $system      'si' for SI, 'bi' for binary prefixes
 * @param       string  $retstring   return string format
 */
function osc_size_readable($size, $max = null, $system = 'si', $retstring = '%01.2f %s') {
    // Pick units
    $systems['si']['prefix'] = array('B', 'K', 'MB', 'GB', 'TB', 'PB');
    $systems['si']['size']   = 1000;
    $systems['bi']['prefix'] = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
    $systems['bi']['size']   = 1024;
    $sys = isset($systems[$system]) ? $systems[$system] : $systems['si'];

    // Max unit to display
    $depth = count($sys['prefix']) - 1;
    if ($max && false !== $d = array_search($max, $sys['prefix'])) {
        $depth = $d;
    }

    // Loop
    $i = 0;
    while ($size >= $sys['size'] && $i < $depth) {
        $size /= $sys['size'];
        $i++;
    }

    return sprintf($retstring, $size, $sys['prefix'][$i]);
}




/**
 * Calculate the size of a directory by iterating its contents
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.2.0
 * @link        http://aidanlister.com/repos/v/function.dirsize.php
 * @param       string   $directory    Path to directory
 */
function osc_dirsize($path) {
    // Init
    $size = 0;

    // Trailing slash
    if (substr($path, -1, 1) !== DIRECTORY_SEPARATOR) {
        $path .= DIRECTORY_SEPARATOR;
    }
// path
    $path = DIR_FS_CATALOG;
    // Sanity check
    if (is_file($path)) {
        return filesize($path);
    } elseif (!is_dir($path)) {
        return false;
    }

    // Iterate queue
    $queue = array($path);
    for ($i = 0, $j = count($queue); $i < $j; ++$i) {
        // Open directory
        $parent = $i;
        if (is_dir($queue[$i]) * $dir = @dir($queue[$i])) {
            $subdirs = array();
            while (false !== ($entry = $dir->read())) {
                // Skip pointers
                if ($entry == '.' || $entry == '..') {
                    continue;
                }

                // Get list of directories or filesizes
                $path = $queue[$i] . $entry;
                if (is_dir($path)) {
                    $path .= DIRECTORY_SEPARATOR;
                    $subdirs[] = $path;
                } elseif (is_file($path)) {
                    $size += filesize($path);
                }
            }

            // Add subdirectories to start of queue
            unset($queue[0]);
            $queue = array_merge($subdirs, $queue);

            // Recalculate stack size
            $i = -1;
            $j = count($queue);

            // Clean up
            $dir->close();
            unset($dir);
        }
    }

    return $size;
}

/**
 * Get the the user name of the administrator
 * @param string
 * @param return  $check_admin['user_admin'], the name of the user admin
 * @access public
 */

  function osc_administrators() {
    global $aID, $OSCOM_PDO;

    $QcheckAdmin = $OSCOM_PDO->prepare('select id
                                        from :table_administrators
                                        where id = :id
                                       ');
    $QcheckAdmin->bindInt(':id', (int)$aID);
    $QcheckAdmin->execute();

    $check_admin = $QcheckAdmin->fetch();

     return $check_admin['user_admin'];
  } 



  /**
  * get the user administrator
  * @param string $user_administrator
  */

  function  osc_user_admin() {
    global $OSCOM_PDO;

    $username = array($_SESSION['admin']);
    $username = $username[0]['username'];

    $Qlogins = $OSCOM_PDO->prepare('select a.name,
                                           a.first_name
                                    from :table_action_recorder ar,
                                         :table_administrators a
                                    where  ar.user_id = a.id
                                    and ar.module = :module
                                    and ar.user_name = :user_name
                                    limit 1
                                   ');

    $Qlogins->bindValue(':module',  'ar_admin_login');
    $Qlogins->bindValue(':user_name',  $username);

    $Qlogins->execute();

    $logins = $Qlogins->fetch();

    $administrator =  osc_output_string_protected($logins['first_name'] . ' ' . $logins['name']);

    return $administrator;
  }

/**
  * Reset the various cache systems
  * @param string $action
*/

  function osc_reset_cache_data_usu5( $action = false ) {
    global $OSCOM_PDO;

    if ( $action == 'reset' ) {
      $usu5_path = realpath( dirname( __FILE__ ) . '/../../../' ) . '/' . DIR_WS_MODULES . 'ultimate_seo_urls5/';
      switch( USU5_CACHE_SYSTEM ) {
        case 'file':
          $path_to_cache = $usu5_path . 'cache_system/cache/';
          $it = new DirectoryIterator( $path_to_cache );
          while( $it->valid() ) {
            if ( !$it->isDot() && is_readable( $path_to_cache . $it->getFilename() ) && ( substr( $it->getFilename(), -6 ) == '.cache' ) ) {
              @unlink( $path_to_cache . $it->getFilename() );
            }
            $it->next();
          }
          break;
        case 'mysql':
          osc_db_query( 'TRUNCATE TABLE `usu_cache`' );
          break;
        case 'memcache':
          if ( class_exists('Memcache') ){
            include $usu5_path . 'interfaces/cache_interface.php';
            include $usu5_path . 'cache_system/memcache.php';
            Memcache_Cache_Module::iAdmin()->initiate()
                                           ->flushOut();
          }
          break;
        case 'sqlite':
          include $usu5_path . 'interfaces/cache_interface.php';
          include $usu5_path . 'cache_system/sqlite.php';
          Sqlite_Cache_Module::admini()->gc();
          break;
      }

      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                      set configuration_value = :configuration_value
                                      where configuration_key = :configuration_key
                                    ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'USU5_RESET_CACHE');
      $Qupdate->execute();

    }
  }


/**
 * Get from php ini settings exist or not
 * @param string
 * @access public
 */
  function osc_get_php_ini_functions($mod_name, $parameter = 'disable_functions') {
    $php_ini = ini_get($parameter);
    $find = strstr($php_ini, $mod_name);
    if ( !$find ) {
      return false;
    }
    return true;
  }


/**
 * Recursive Directory list file with all css under a drop down
 *
 * @param string $filename : name of the file
 * @return string c $filename_array, the file name in the  css subdirectory
 * @access public
 */

  function osc_filename_css() {

    if (isset($_POST['directory_css'])) {
      $directory_selected = $_POST['directory_css'];
    } else {
      $directory_selected = $_GET['directory_css'];
    }

    if (file_exists(DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/graphism/'.$_SESSION['language'].'/css/' . $directory_selected)) {
      $template_directory =  DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/graphism/'.$_SESSION['language'].'/css/' . $directory_selected.'/';
    } else {
      $template_directory =  DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/graphism/'.$_SESSION['language'].'/css/';
    }

    $found = array(); //initialize an array for matching files
    $fileTypes = array('css'); // Create an array of file types
    $found = array(); // Traverse the folder, and add filename to $found array if type matches

    /* if empty error is produced : Fatal error: Uncaught exception 'RuntimeException'*/
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($template_directory)) as $filename=>$current) {
      $fileInfo = pathinfo($current->getFileName());

      if(array_key_exists('extension', $fileInfo) && in_array($fileInfo['extension'],$fileTypes)) {
        $found[] = $current->getFileName();
      }
    }

    if ($found) { // Check the $found array is not empty
      natcasesort($found); // Sort in natural, case-insensitive order, and populate menu

      $filename_array[0] = array('id' => '0',
                                 'text' => SELECT_DATAS);
      foreach ($found as $filename){
        $filename_array[] = array('id' => $filename,
                                   'text' => $filename);
      }
    }
    return $filename_array;
  }

/**
  * CSS Directory list
  *
  * @param string $filename : name of the file
  * @return string $directory_array, the directories name in css directory
  * @access public
*/

  function  osc_directory_css() {

    $template_directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/graphism/'.$_SESSION['language'].'/css/';
    $weeds = array('.', '..', '_notes');
    $directories = array_diff(scandir($template_directory), $weeds);

    $directory_array[0] = array('id' => '0',
                                 'text' => SELECT_DATAS);
    foreach($directories as $directory) {
      if(is_dir($template_directory.$directory)) {
        $directory_array[] = array('id' => $directory,
                                  'text' => $directory);
      }
    }
   return $directory_array;
  }


/**
 * Recursive Directory list file with all css under a drop down
 *
 * @param string $filename : name of the file
 * @return string c $filename_array, the file name in the  css subdirectory
 * @access public
 */

  function osc_filename_html() {

    if (isset($_POST['directory_html'])) {
      $directory_selected = $_POST['directory_html'];
    } else {
      $directory_selected = $_GET['directory_html'];
    }

    if (file_exists(DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/' . $directory_selected .'/content/')) {
      $template_directory =  DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/' . $directory_selected.'/content/';
    } else {
      $template_directory =  DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA .'/modules/';
    }

    $found = array(); //initialize an array for matching files
    $fileTypes = array('php'); // Create an array of file types
    $found = array(); // Traverse the folder, and add filename to $found array if type matches

    /* if empty error is produced : Fatal error: Uncaught exception 'RuntimeException'*/
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($template_directory)) as $filename=>$current) {
      $fileInfo = pathinfo($current->getFileName());

      if(array_key_exists('extension', $fileInfo) && in_array($fileInfo['extension'],$fileTypes)) {
        $found[] = $current->getFileName();
      }
    }

    if ($found) { // Check the $found array is not empty
      natcasesort($found); // Sort in natural, case-insensitive order, and populate menu

      $filename_array[0] = array('id' => '0',
                                'text' => SELECT_DATAS);

      foreach ($found as $filename){
        $filename_array[] = array('id' => $filename,
          'text' => $filename);
      }
    }
    return $filename_array;
  }

/*
  **
  * HTML Directory list
  *
  * @param string $filename : name of the file
* @return string $directory_array, the directories name in css directory
* @access public
*/

  function  osc_directory_html() {

    $template_directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/';

    $weeds = array('.', '..', '_notes', );
    $directories = array_diff(scandir($template_directory), $weeds);

    $directory_array[0] = array('id' => '0',
                               'text' => SELECT_DATAS);

    foreach($directories as $directory) {
      if(is_dir($template_directory.$directory)) {
        $directory_array[] = array('id' => $directory,
                                   'text' => $directory);
      }
    }
    return $directory_array;
  }

/*
  **
  * Recursive Directory list file with all template products under a drop down
  *
  * @param string $filename : name of the file
  * @return string c $filename_array, the file name in the  template products subdirectory
  * @access public
 */

  function osc_filename_template_products() {

    if (isset($_POST['directory_html'])) {
      $directory_selected = $_POST['directory_html'];
    } else {
      $directory_selected = $_GET['directory_html'];
    }

    if (file_exists(DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/' . $directory_selected .'/template_html/')) {
      $template_directory =  DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/' . $directory_selected.'/template_html/';
    } else {
      $template_directory =  DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA .'/modules/';
    }

 //   $found = array(); //initialize an array for matching files
    $fileTypes = array('php'); // Create an array of file types
    $found = array(); // Traverse the folder, and add filename to $found array if type matches

    /* if empty error is produced : Fatal error: Uncaught exception 'RuntimeException'*/
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($template_directory)) as $filename=>$current) {
      $fileInfo = pathinfo($current->getFileName());

      if(array_key_exists('extension', $fileInfo) && in_array($fileInfo['extension'],$fileTypes)) {
        $found[] = $current->getFileName();
      }
    }

    if ($found) { // Check the $found array is not empty
      natcasesort($found); // Sort in natural, case-insensitive order, and populate menu

      $filename_array[0] = array('id' => '0',
                                 'text' => SELECT_DATAS);

      foreach ($found as $filename){
        $filename_array[] = array('id' => $filename,
                                 'text' => $filename);
      }
    }
    return $filename_array;
  }



/*
  **
  * Template_products Directory list
  *
  * @param string $filename : name of the file
  * @return string $directory_array, the directories name in css directory
  * @access public
*/

  function  osc_directory_template_products() {

    $template_directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/';

    $weeds = array('.', '..', '_notes');
    $directories = array_diff(scandir($template_directory), $weeds);

    $directory_array[0] = array('id' => '0',
                                'text' => SELECT_DATAS);

    foreach($directories as $directory) {
      if(is_dir($template_directory.$directory)) {
        $directory_array[] = array('id' => $directory,
          'text' => $directory);
      }
    }
    return $directory_array;
  }


/**
 * Email password config
 *
 * @param string $password
 * @return string  $password
 * @access public
 */
function osc_cfg_password($password) {
  return preg_replace("|.|", "*", $password);
}

/**
 * Email password input
 *
 * @param string $password
 * @return string  $password, the password
 * @access public
 */
function osc_cfg_input_password($password) {
  return osc_draw_password_field('configuration_value', $password);
}

/**
 * Delete cached files by their key ID for module and configuration.php
 *
 * @param string $key The key ID of the cached files to delete
 * @access public
 */

  function osc_cache_clear($key) {

    $directory_cache = DIR_FS_CACHE2;
    if ( is_writable($directory_cache) ) {
      $key_length = strlen($key);

      $d = dir($directory_cache);

      while ( ($entry = $d->read()) !== false ) {
        if ( (strlen($entry) >= $key_length) && (substr($entry, 0, $key_length) == $key) ) {
          @unlink($directory_cache . $entry);
        }
      }

      $d->close();
    }
  }


/**
 * Reset cached files by their key  for module and configuration.php
 *
 * @param string $key The key ID of the cached files to delete
 * @access public
 */

  function osc_cache_reset($key) {

    $languages = osc_get_languages();
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      $language_id = $languages[$i]['id'];
      osc_cache_clear($key . $language_id  .'.cache');
    }
  }


/**
 * Description summary
 *
 * @param string  $product_id, $language_id
 * @return string $product['products_description'], description name
 * @access public
 */
  function osc_get_products_description_summary($product_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qproduct = $OSCOM_PDO->prepare('select products_description_summary
                                     from :table_products_description
                                     where products_id = :products_id
                                     and language_id = :language_id
                                  ');
    $Qproduct->bindInt(':products_id', (int)$product_id);
    $Qproduct->bindInt(':language_id', (int)$language_id);

    $Qproduct->execute();

    return $Qproduct->value('products_description_summary');
  }


/**
 * Select the product packaging
 *
 * @param string
 * @return $product_packaging, the packaging selected
 * @access public
 */
  function osc_product_packaging()  {
    global $product;

    if ($product['products_packaging'] == 1) {
      $product_packaging = 'New product';
    } elseif ($product['products_packaging'] == 2) {
      $product_packaging = 'Product repackaged';
    } else {
      $product_packaging = 'Product used';
    }
    return $product_packaging;
  }
