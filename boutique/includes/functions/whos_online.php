<?php
/**
 * who_online.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  function osc_update_whos_online() {
    global $OSCOM_Customer, $spider_flag, $OSCOM_PDO;

      $wo_session_id = session_id();
      $wo_ip_address = osc_get_ip_address();
      $wo_last_page_url = osc_output_string_protected(substr($_SERVER['REQUEST_URI'], 0, 255));
      $user_agent = $_SERVER['HTTP_USER_AGENT'];

      if ( $OSCOM_Customer->isLoggedOn() ) {

        $wo_customer_id = $OSCOM_Customer->getID();
        $wo_full_name = $OSCOM_Customer->getName();

      } else {

        $wo_customer_id = null;
        $wo_full_name = 'Guest';

      if (( $spider_flag ) or (strpos ($user_agent, "Googlebot") > 0 )) {
          $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);

          if ( !empty($user_agent) ) {
            $spiders = file(DIR_FS_CATALOG . 'includes/spiders.txt');

            foreach ( $spiders as $spider ) {
              if ( !empty($spider) ) {
                if ( (strpos($user_agent, trim($spider))) !== false ) {
                  $wo_full_name = $spider;
                  break;
                }
              }
            }
          }
        }
      }

      $current_time = time();
      $xx_mins_ago = ($current_time - 900);

// remove entries that have expired
      $Qwhosonline = $OSCOM_PDO->prepare('delete
                                          from :table_whos_online
                                          where time_last_click < :time_last_click
                                         ');
      $Qwhosonline->bindInt(':time_last_click', $xx_mins_ago);
      $Qwhosonline->execute();

      $Qsession = $OSCOM_PDO->prepare('select session_id 
                                       from :table_whos_online
                                       where session_id = :session_id
                                       limit 1
                                      ');
      $Qsession->bindValue(':session_id', $wo_session_id);
      $Qsession->execute();

    if ($Qsession->fetch() !== false) {

      $OSCOM_PDO->save('whos_online', ['customer_id' => $wo_customer_id, 
                                      'full_name' => $wo_full_name, 
                                      'ip_address' => $wo_ip_address,
                                      'time_last_click' => $current_time, 
                                      'last_page_url' => $wo_last_page_url
                                      ], 
                                      ['session_id' => $wo_session_id]
                       );

      } else {

      if ($_SERVER['HTTP_REFERER'] == null) {
        $http_referer = '';
      } else {
        $http_referer = $_SERVER['HTTP_REFERER'];
      }

      $OSCOM_PDO->save('whos_online', ['customer_id' => $wo_customer_id,
                                      'full_name' => $wo_full_name, 
                                      'session_id' => $wo_session_id, 
                                      'ip_address' => $wo_ip_address, 
                                      'time_entry' => $current_time, 
                                      'time_last_click' => $current_time, 
                                      'last_page_url' => $wo_last_page_url,
                                      'http_referer' => $http_referer,
                                      'user_agent' => $user_agent
                                     ]
                      );
      }
    }

  function osc_whos_online_update_session_id($old_id, $new_id) {
    global $OSCOM_PDO;

      $OSCOM_PDO->save('whos_online', ['session_id' => $new_id], 
                                      ['session_id' => $old_id]
                    );

  }
