<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  define('MODULE_PAYMENT_COD_TEXT_TITLE', 'Cash on Delivery');
  define('MODULE_PAYMENT_COD_TEXT_DESCRIPTION', 'Cash on Delivery');
  define('TEXT_INFO_ONLINE_STATUS','More informations');
  define('TEXT_INFO_VERSION','Version : ');
?>