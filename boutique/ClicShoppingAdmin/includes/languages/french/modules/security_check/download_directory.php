<?php
/**
 * download_directory.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_DOWNLOAD_DIRECTORY_NON_EXISTENT', 'Le répertoire de t&eacutel&eacutechargement n\'existe pas : ' . DIR_FS_DOWNLOAD . '. Vous ne pourrez pas t&eacutel&eacutecharger des fichiers tant que celui-ci n\'exite pas.');
?>
