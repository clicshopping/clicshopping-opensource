<?php
/**
 * countires.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Pays');

define('TABLE_HEADING_COUNTRY_NAME', 'Pays');
define('TABLE_HEADING_COUNTRY_CODES', 'Codes ISO');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_COUNTRY_STATUS','Statut');

define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_COUNTRY_NAME', 'Nom :');
define('TEXT_INFO_COUNTRY_CODE_2', 'Code ISO (2) :');
define('TEXT_INFO_COUNTRY_CODE_3', 'Code ISO (3) :');
define('TEXT_INFO_ADDRESS_FORMAT', 'Format de l\'adresse :');
define('TEXT_INFO_INSERT_INTRO', 'Merci d\'entrer le nouveau pays avec ses données liées');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer ce pays ?');
define('TEXT_INFO_HEADING_NEW_COUNTRY', 'Nouveau pays');
define('TEXT_INFO_HEADING_EDIT_COUNTRY', 'Editer pays');
define('TEXT_INFO_HEADING_DELETE_COUNTRY', 'Supprimer pays');
?>
