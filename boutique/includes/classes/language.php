<?php
/**
 * language.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

  browser language detection logic Copyright phpMyAdmin (select_lang.lib.php3 v1.24 04/19/2002)
                                   Copyright Stephane Garin <sgarin@sgarin.com> (detect_language.php v0.1 04/02/2002)
*/

  class language {
    var $languages, $catalog_languages, $browser_languages;

    function language($lng = '') {
      global $OSCOM_PDO;

      $this->languages = array('af' => 'af|afrikaans',
                               'ar' => 'ar([-_][[:alpha:]]{2})?|arabic',
                               'be' => 'be|belarusian',
                               'bg' => 'bg|bulgarian',
                               'br' => 'pt[-_]br|brazilian portuguese',
                               'ca' => 'ca|catalan',
                               'cs' => 'cs|czech',
                               'da' => 'da|danish',
                               'de' => 'de([-_][[:alpha:]]{2})?|german',
                               'el' => 'el|greek',
                               'en' => 'en([-_][[:alpha:]]{2})?|english',
                               'es' => 'es([-_][[:alpha:]]{2})?|spanish',
                               'et' => 'et|estonian',
                               'eu' => 'eu|basque',
                               'fa' => 'fa|farsi',
                               'fi' => 'fi|finnish',
                               'fo' => 'fo|faeroese',
                               'fr' => 'fr([-_][[:alpha:]]{2})?|french',
                               'ga' => 'ga|irish',
                               'gl' => 'gl|galician',
                               'he' => 'he|hebrew',
                               'hi' => 'hi|hindi',
                               'hr' => 'hr|croatian',
                               'hu' => 'hu|hungarian',
                               'id' => 'id|indonesian',
                               'it' => 'it|italian',
                               'ja' => 'ja|japanese',
                               'ji' => 'ji|yiddish',
                               'ka' => 'ka|georgian',
                               'ko' => 'ko|korean',
                               'lt' => 'lt|lithuanian',
                               'lv' => 'lv|latvian',
                               'mk' => 'mk|macedonian',
                               'mt' => 'mt|maltese',
                               'ms' => 'ms|malaysian',
                               'nl' => 'nl([-_][[:alpha:]]{2})?|dutch',
                               'no' => 'no|norwegian',
                               'pl' => 'pl|polish',
                               'pt' => 'pt([-_][[:alpha:]]{2})?|portuguese',
                               'ro' => 'ro|romanian',
                               'ru' => 'ru|russian',
                               'sk' => 'sk|slovak',
                               'sq' => 'sq|albanian',
                               'sr' => 'sr|serbian',
                               'sv' => 'sv|swedish',
                               'sz' => 'sz|sami',
                               'sx' => 'sx|sutu',
                               'th' => 'th|thai',
                               'ts' => 'ts|tsonga',
                               'tr' => 'tr|turkish',
                               'tn' => 'tn|tswana',
                               'tw' => 'zh[-_]tw|chinese traditional',
                               'uk' => 'uk|ukrainian',
                               'ur' => 'ur|urdu',
                               'vi' => 'vi|vietnamese',
                               'zh' => 'zh|chinese simplified',
                               'zu' => 'zu|zulu');

      $this->catalog_languages = array();

      $Qlanguages = $OSCOM_PDO->prepare('select languages_id,
                                                name,
                                                code,
                                                image,
                                                directory,
                                                status
                                         from :table_languages
                                         where status = 1
                                         order by sort_order
                                        ');
      $Qlanguages->bindInt(':status', 1);
      $Qlanguages->execute();

      while ($Qlanguages->fetch()) {
        $this->catalog_languages[$Qlanguages->value('code')] = array('id' => $Qlanguages->valueInt('languages_id'),
                                                                     'name' => $Qlanguages->value('name'),
                                                                     'image' => $Qlanguages->value('image'),
                                                                     'directory' => $Qlanguages->value('directory'));
      }

      $this->browser_languages = '';
      $this->language = '';

      $this->set_language($lng);
    }

    function set_language($language) {
      if ( (osc_not_null($language)) && (isset($this->catalog_languages[$language])) ) {
        $this->language = $this->catalog_languages[$language];
      } else {
        $this->language = $this->catalog_languages[DEFAULT_LANGUAGE];
      }
    }

    function get_browser_language() {
 
      if ( isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ){
        $this->browser_languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

        $n=sizeof($this->browser_languages);
        for ($i=0; $i<$n; $i++) {
         foreach($this->languages as $key => $value) {
           if (preg_match('/^(' . $value . ')(;q=[0-9]\\.[0-9])?$/i', $this->browser_languages[$i]) && isset($this->catalog_languages[$key])) {
             $this->language = $this->catalog_languages[$key];
             break 2;
           }
         }
       }
      } else {
        return false;
      }
    }
  }