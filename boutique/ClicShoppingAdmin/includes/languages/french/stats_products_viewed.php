<?php
/**
 * stats_products_viewed.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */


define('HEADING_TITLE', 'Les produits les plus consultés');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_VIEWED', 'Vu');
define('TABLE_HEADING_CLEAR','Supprimer');
?>