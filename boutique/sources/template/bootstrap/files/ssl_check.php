<?php
/*
 * ssl_check.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Message  -----  
// ---------------------- 
?>
<div class="contentContainer">
  <div class="contentText">
    <div class="panel panel-danger">
      <div class="panel-heading"><?php echo BOX_INFORMATION_HEADING; ?></div>
      <div class="panel-body">
        <?php echo BOX_INFORMATION; ?>
      </div>
    </div>

    <div class="panel panel-danger">
      <div class="panel-body">
        <?php echo TEXT_INFORMATION; ?>
      </div>
    </div>
  </div>
<?php
// ---------------------- 
// --- Button  -----  
// ---------------------- 
?>
  <div class="buttonSet" align="right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, osc_href_link('login.php'),'success'); ?></div>
</div>

<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
