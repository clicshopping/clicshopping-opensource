<?php
/**
 * suppliers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {

      case 'setflag':
        osc_set_suppliers_status($_GET['id'], $_GET['flag']);
        osc_redirect(osc_href_link('suppliers.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'mID=' . $_GET['id']));
        break;

      case 'insert':

        if (isset($_GET['mID'])) $suppliers_id = osc_db_prepare_input($_GET['mID']);

        $suppliers_name = osc_db_prepare_input($_POST['suppliers_name']);
        $suppliers_manager = osc_db_prepare_input($_POST['suppliers_manager']);
        $suppliers_phone = osc_db_prepare_input($_POST['suppliers_phone']);
        $suppliers_email_address = osc_db_prepare_input($_POST['suppliers_email_address']);
        $suppliers_fax = osc_db_prepare_input($_POST['suppliers_fax']);
        $suppliers_address = osc_db_prepare_input($_POST['suppliers_address']);
        $suppliers_suburb = osc_db_prepare_input($_POST['suppliers_suburb']);
        $suppliers_postcode = osc_db_prepare_input($_POST['suppliers_postcode']);
        $suppliers_city = osc_db_prepare_input($_POST['suppliers_city']);
        $suppliers_states = osc_db_prepare_input($_POST['suppliers_states']);
        $suppliers_country_id = osc_db_prepare_input($_POST['suppliers_country_id']);
        $suppliers_notes = osc_db_prepare_input($_POST['suppliers_notes']);
        $suppliers_image = osc_db_prepare_input($_POST['suppliers_image']);

// Insertion images des fabricants via l'éditeur FCKeditor (fonctionne sur les nouvelles et éditions des fabricants)
        if (isset($_POST['suppliers_image']) && osc_not_null($_POST['suppliers_image']) && ($_POST['suppliers_image'] != 'none') && ($_POST['delete_image'] != 'yes')) {
          $suppliers_image = htmlspecialchars($suppliers_image);
          $suppliers_image = strstr($suppliers_image, DIR_WS_CATALOG_IMAGES);
          $suppliers_image = str_replace(DIR_WS_CATALOG_IMAGES, '', $suppliers_image);
          $suppliers_image_end = strstr($suppliers_image, '&quot;');
          $suppliers_image = str_replace($suppliers_image_end, '', $suppliers_image);
          $suppliers_image = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $suppliers_image);
        } else {
          $suppliers_image = 'null';
        }

        $sql_data_array = array('suppliers_name' => $suppliers_name,
                                'suppliers_manager' => $suppliers_manager,
                                'suppliers_phone' => $suppliers_phone,
                                'suppliers_email_address' => $suppliers_email_address,
                                'suppliers_fax' => $suppliers_fax,
                                'suppliers_address' => $suppliers_address,
                                'suppliers_suburb' => $suppliers_suburb,
                                'suppliers_postcode' => $suppliers_postcode,
                                'suppliers_city' => $suppliers_city,
                                'suppliers_states' => $suppliers_states,
                                'suppliers_country_id' => $suppliers_country_id,
                                'suppliers_notes' => $suppliers_notes
                                );

          if ($suppliers_image != 'null') {
            $insert_image_sql_data = array('suppliers_image' => $suppliers_image);
            $sql_data_array = array_merge($sql_data_array, $insert_image_sql_data);
         }

          $insert_sql_data = array('date_added' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);
          osc_db_perform('suppliers', $sql_data_array);

          $suppliers_id = osc_db_insert_id();
          $languages = osc_get_languages();

          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $suppliers_url_array = $_POST['suppliers_url'];
            $language_id = $languages[$i]['id'];

            $sql_data_array = array('suppliers_url' => osc_db_prepare_input($suppliers_url_array[$language_id]));

             if ($action == 'insert') {
               $insert_sql_data = array('suppliers_id' => $suppliers_id,
                                        'languages_id' => $language_id);

               $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

               osc_db_perform('suppliers_info', $sql_data_array);
             }
          }

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_SUPPLIERS_ADMIN == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_suppliers.php');
        }
//***************************************
// End odoo web service
//***************************************

        osc_redirect(osc_href_link('suppliers.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'mID=' . $suppliers_id));
        break;

      case 'update':

        if (isset($_GET['mID'])) $suppliers_id = osc_db_prepare_input($_GET['mID']);
        $suppliers_name = osc_db_prepare_input($_POST['suppliers_name']);
        $suppliers_manager = osc_db_prepare_input($_POST['suppliers_manager']);
        $suppliers_phone = osc_db_prepare_input($_POST['suppliers_phone']);
        $suppliers_email_address = osc_db_prepare_input($_POST['suppliers_email_address']);
        $suppliers_fax = osc_db_prepare_input($_POST['suppliers_fax']);
        $suppliers_address = osc_db_prepare_input($_POST['suppliers_address']);
        $suppliers_suburb = osc_db_prepare_input($_POST['suppliers_suburb']);
        $suppliers_postcode = osc_db_prepare_input($_POST['suppliers_postcode']);
        $suppliers_city = osc_db_prepare_input($_POST['suppliers_city']);
        $suppliers_states = osc_db_prepare_input($_POST['suppliers_states']);
        $suppliers_country_id = osc_db_prepare_input($_POST['suppliers_country_id']);
        $suppliers_notes = osc_db_prepare_input($_POST['suppliers_notes']);
        $suppliers_image = osc_db_prepare_input($_POST['suppliers_image']);

        $sql_data_array = array('suppliers_name' => $suppliers_name,
                                'suppliers_manager' => $suppliers_manager,
                                'date_added'        => 'now()',
                                'suppliers_phone' => $suppliers_phone,
                                'suppliers_email_address' => $suppliers_email_address,
                               	'suppliers_fax' => $suppliers_fax,
                                'suppliers_address' => $suppliers_address,
                                'suppliers_suburb' => $suppliers_suburb,
                                'suppliers_postcode' => $suppliers_postcode,
                                'suppliers_city' => $suppliers_city,
                                'suppliers_states' => $suppliers_states,
                                'suppliers_country_id' => $suppliers_country_id,
                                'suppliers_notes' => $suppliers_notes
                                );

          $update_sql_data = array('last_modified' => 'now()');
          $sql_data_array = array_merge($sql_data_array, $update_sql_data);
          osc_db_perform('suppliers', $sql_data_array, 'update', "suppliers_id = '" . (int)$suppliers_id . "'  ");


// Insertion images des fabricants via l'éditeur FCKeditor (fonctionne sur les nouvelles et éditions des fabricants)

        if (isset($_POST['suppliers_image']) && osc_not_null($_POST['suppliers_image']) && ($_POST['suppliers_image'] != 'none') && ($_POST['delete_image'] != 'yes')) {
          $suppliers_image = htmlspecialchars($suppliers_image);
          $suppliers_image = strstr($suppliers_image, DIR_WS_CATALOG_IMAGES);
          $suppliers_image = str_replace(DIR_WS_CATALOG_IMAGES, '', $suppliers_image);
          $suppliers_image_end = strstr($suppliers_image, '&quot;');
          $suppliers_image = str_replace($suppliers_image_end, '', $suppliers_image);
          $suppliers_image = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $suppliers_image);

          $Qupdate = $OSCOM_PDO->prepare('update :table_suppliers
                                          set suppliers_image = :suppliers_image
                                          where suppliers_id = :suppliers_id
                                          ');
          $Qupdate->bindValue(':suppliers_image', $suppliers_image);
          $Qupdate->bindInt(':suppliers_id',  (int)$suppliers_id);
          $Qupdate->execute();

        }

// Suppression de l'image
        if ($_POST['delete_image'] == 'yes') {

          $Qupdate = $OSCOM_PDO->prepare('update :table_suppliers
                                          set suppliers_image = :suppliers_image
                                          where suppliers_id = :suppliers_id
                                          ');
          $Qupdate->bindValue(':suppliers_image', '');
          $Qupdate->bindInt(':suppliers_id',  (int)$suppliers_id);
          $Qupdate->execute();

        }

        $languages = osc_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $suppliers_url_array = $_POST['suppliers_url'];
          $language_id = $languages[$i]['id'];

          $sql_data_array = array('suppliers_url' => osc_db_prepare_input($suppliers_url_array[$language_id]));

          $OSCOM_PDO->save('suppliers_info', $sql_data_array, ['suppliers_id' => (int)$suppliers_id,
                                                               'languages_id' => (int)$language_id
                                                              ]
                          );
        }
/*

        if (USE_CACHE == 'true') {
          osc_reset_cache_block('suppliers');
        }
*/

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_SUPPLIERS_ADMIN == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_suppliers.php');
        }
//***************************************
// End odoo web service
//***************************************

       osc_redirect(osc_href_link('suppliers.php', 'page=' . $_GET['page']));

     break;


// the images are not deleted in this case
      case 'delete_all':

       if ($_POST['selected'] != '') {
          foreach ($_POST['selected'] as $suppliers['suppliers_id'] ) {

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_suppliers
                                            where suppliers_id = :suppliers_id 
                                          ');
            $Qdelete->bindInt(':suppliers_id',   (int)$suppliers['suppliers_id'] );
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_suppliers_info
                                            where suppliers_id = :suppliers_id 
                                          ');
            $Qdelete->bindInt(':suppliers_id',   (int)$suppliers['suppliers_id'] );
            $Qdelete->execute();

            $products_query = osc_db_query("select products_id from products
                                           where suppliers_id = '" . (int)$suppliers_id . "'
                                           ");

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set suppliers_id = :suppliers_id,
                                                products_status = :products_status
                                            where suppliers_id = :suppliers_id1
                                          ');
            $Qupdate->bindInt(':suppliers_id', '');
            $Qupdate->bindInt(':products_status', (int)$products_id);
            $Qupdate->bindInt(':suppliers_id1', (int)$suppliers['suppliers_id']);  
             
            $Qupdate->execute();

          }
       }
/*
        if (USE_CACHE == 'true') {
          osc_reset_cache_block('suppliers');
        }
*/
       osc_redirect(osc_href_link('suppliers.php'));
      break;
    }
  }

  if (empty($action)) {
    $suppliers_query_raw = "select suppliers_id,
                                   suppliers_name,
                                   suppliers_image,
                                   date_added,
                                   last_modified,
                                   suppliers_manager,
                                   suppliers_phone,
                                   suppliers_email_address,
                                   suppliers_fax,
                                   suppliers_address,
                                   suppliers_suburb,
                                   suppliers_postcode,
                                   suppliers_city,
                                   suppliers_states,
                                   suppliers_country_id,
                                   suppliers_notes,
                                   suppliers_status
                            from suppliers
                            order by suppliers_name
                           ";
    $suppliers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $suppliers_query_raw, $suppliers_query_numrows);
    $suppliers_query = osc_db_query($suppliers_query_raw);
  }

  require('includes/header.php');
?>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
      <div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        <div class="adminTitle">
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/suppliers.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
          <span class="col-md-4 smallText" style="text-align: center;">
  <?php echo $suppliers_split->display_count($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SUPPLIERS); ?></br />
  <?php echo $suppliers_split->display_links($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
          </span>
          <span class="pull-right">
            <?php  echo '<a href="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&mID=' . $mInfo->suppliers_id . '&action=new') . '">' . osc_image_button('button_insert_suppliers.gif', IMAGE_INSERT) . '</a>'; ?></span>
          </span>
          <span class="pull-right">
             <form name="delete_all" <?php echo 'action="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post"><a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
          </span>

<?php
  } else if ( ($action == 'new') || ($action == 'edit') ) {
    $form_action = 'insert';
  if ( ($action == 'edit') && isset($_GET['mID']) ) {
    $form_action = 'update';
  }
?>
          <form name="suppliers" <?php echo 'action="' . osc_href_link('suppliers.php', osc_get_all_get_params(array('action', 'info', 'mID')) . 'action=' . $form_action . '&mID=' . $_GET['mID']) . '"'; ?> method="post"><?php if ($form_action == 'update') echo osc_draw_hidden_field('suppliers_id', $_GET['mID']); ?>
            <span></span>
            <span class="pull-right">&nbsp;<?php echo (($form_action == 'insert') ? osc_image_submit('button_insert_specials.gif', IMAGE_INSERT) : osc_image_submit('button_update.gif', IMAGE_UPDATE)); ?></span>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&mID=' . $_GET['mID']).'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
<?php
  }
?>
        </div>
      </div>
      <div class="clearfix"></div>
   </tr>
<!-- //################################################################################################################ -->
<!-- //                                      EDITION ET INSERTION D'UN FOURNISSEUR                                     -->
<!-- //################################################################################################################ -->
<?php
  if ( ($action == 'new') || ($action == 'edit') )  {
    $form_action = 'insert';

    if ( ($action == 'edit') && isset($_GET['mID']) ) {
     $form_action = 'update';

      $Qsuppliers = $OSCOM_PDO->prepare('select suppliers_id,
                                                 suppliers_name,
                                                 suppliers_image,
                                                 suppliers_manager,
                                                 suppliers_phone,
                                                 suppliers_email_address,
                                                 suppliers_fax,
                                                 suppliers_address,
                                                 suppliers_suburb,
                                                 suppliers_postcode,
                                                 suppliers_city,
                                                 suppliers_states,
                                                 suppliers_country_id,
                                                 suppliers_notes
                                          from :table_suppliers
                                          where suppliers_id = :suppliers_id
                                        ');
      $Qsuppliers->bindInt(':suppliers_id',(int)$_GET['mID']);
      $Qsuppliers->execute();

      $suppliers = $Qsuppliers->fetch();

      $mInfo = new objectInfo($suppliers);

    } else {
      $mInfo = new objectInfo(array());
   }  
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="5" cellpadding="0" class="<?php echo $ClassMessageStack; ?>">
          <tr>
            <td class="<?php echo $ClassMessageStack; ?>">
              <table width="100%">
                <?php echo $OSCOM_MessageStack->output(); ?>
            </table>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
              <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_SUPPLIERS_NOTE; ?></a></li>
              <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_VISUEL; ?></a></li>
            </ul>

            <div class="tabsClicShopping">
              <div class="tab-content">
<?php
// -- ------------------------------------------------------------ //
// --          ONGLET Information Général de la Marque          //
// -- ------------------------------------------------------------ //
?>
                <div class="tab-pane active" id="tab1">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_SUPPLIERS_GENERAL; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_NAME; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_input_field('suppliers_name',  $mInfo->suppliers_name, 'required aria-required="true" id="suppliers_name"'); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_MANAGER; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_manager', $mInfo->suppliers_manager); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_PHONE; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_phone', $mInfo->suppliers_phone); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_FAX; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_fax', $mInfo->suppliers_fax); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_EMAIL_ADDRESS; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_email_address', $mInfo->suppliers_email_address); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_ADDRESS; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_address', $mInfo->suppliers_address); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_SUBURB; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_suburb', $mInfo->suppliers_suburb); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_POSTCODE; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_input_field('suppliers_postcode', $mInfo->suppliers_postcode); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_CITY; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_city', $mInfo->suppliers_city); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2"><?php echo TEXT_SUPPLIERS_COUNTRY; ?></span>
                        <span class="col-md-4">
<?php
  if ($error == true) {
    if ($entry_country_error == true) {
      echo osc_draw_pull_down_menu('suppliers_country_id', osc_get_countries(), $mInfo->suppliers_country_id) . '&nbsp;' . ENTRY_COUNTRY_ERROR;
    } else {
      echo osc_get_country_name($mInfo->suppliers_country_id) . osc_draw_hidden_field('suppliers_country_id');
    }
  } else {
    echo osc_draw_pull_down_menu('suppliers_country_id', osc_get_countries(), $mInfo->suppliers_country_id);
  }
?>
                        </span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_STATES; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_input_field('suppliers_states', $mInfo->suppliers_states); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_SUPPLIERS_URL; ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                      <div class="row">
<?php
  $languages = osc_get_languages();
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                          <div class="col-md-12">
                            <span class="col-md-1"></span>
                            <span class="col-md-1 centerInputFields"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                            <span class="col-md-3 pull-left" style="height:50px;"><?php echo osc_draw_input_field('suppliers_url[' . $languages[$i]['id'] . ']', osc_get_supplier_url($mInfo->suppliers_id, $languages[$i]['id'])) ?></span>
                          </div>
                          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
  }
?>
                      </div>
                    </div>
                  </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Information note complementaire          //-->
<!-- //################################################################################################################ -->
                  <div class="tab-pane" id="tab2">
                    <div class="col-md-12 mainTitle">
                      <div class="pull-left"><?php echo TITLE_SUPPLIERS_GENERAL; ?></div>
                    </div>
                    <div class="adminformTitle">
                      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2"><?php echo TEXT_SUPPLIERS_NOTES; ?></span>
                          <span class="col-md-4"><?php echo  osc_draw_textarea_field('suppliers_notes', false, 70,10, $mInfo->suppliers_notes); ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Information visuelle          //-->
<!-- //################################################################################################################ -->
                      <div class="tab-pane" id="tab3">
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                          <tr>
                            <td class="mainTitle"><?php echo TITLE_SUPPLIERS_IMAGE; ?></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                          <tr>
                            <td><table border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                  <td class="main" valign="top" width="250"><?php  echo TEXT_SUPPLIERS_NEW_IMAGE; ?>&nbsp;</td>
                                  <td class="man" align="center" valign="top">
                                    <table  width="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="20"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_PRODUCTS_IMAGE_VIGNETTE, '40', '40'); ?></td>
                                        <td class="main"><?php echo TEXT_PRODUCTS_IMAGE_VIGNETTE; ?></td>
                                      </tr>
                                    </table>
                                    <?php echo  osc_draw_file_field_image_ckeditor('suppliers_image', '212', '212', '') ; ?>
                                  </td>
                                  <td align="center" valign="top" width="100%">
                                    <table  width="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product_images.gif', TEXT_PRODUCTS_IMAGE_VISUEL, '40', '40'); ?></td>
                                        <td class="main" align="left"><?php echo TEXT_PRODUCTS_IMAGE_VISUEL; ?></td>
                                      </tr>
                                    </table>
                                    <table  width="100%" border="0" class="adminformAide">
                                      <tr>
                                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                      </tr>
                                      <tr>
                                        <td align="center"><?php echo osc_info_image($mInfo->suppliers_image, TEXT_PRODUCTS_IMAGE_VIGNETTE); ?></td>
                                      </tr>
                                      <tr>
                                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                      </tr>
                                      <tr>
                                        <td class="main" align="right" colspan="2"><?php echo TEXT_SUPPLIERS_DELETE_IMAGE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table>
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                        <div class="adminformAide">
                          <div class="row">
                            <span class="col-md-12">
                              <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_IMAGE); ?>
                              <strong><?php echo '&nbsp;' . TITLE_HELP_STITLE_AIDE_IMAGEUBMIT; ?></strong>
                            </span>
                          </div>
                          <div class="spaceRow"></div>
                          <div class="row">
                            <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_IMAGE_SUPPLIERS; ?></span>
                          </div>
                        </div>
                      </div>
                    </div>
                 </div>
               </div>
             </td>
           </tr>
        </form>
<?php
  } else {
?>
<!-- //################################################################################################################ -->
<!-- //                                             LISTING DES FOURNISSEURS                                       -->
<!-- //################################################################################################################ -->
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SUPPLIERS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MANAGER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PHONE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FAX; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_EMAIL_ADDRESS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  while ($suppliers = osc_db_fetch_array($suppliers_query)) {
    if ((!isset($_GET['mID']) || (isset($_GET['mID']) && ($_GET['mID'] == $suppliers['suppliers_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {

      $Qsuppliers = $OSCOM_PDO->prepare('select count(*) as products_count
                                         from :table_products
                                         where suppliers_id = :suppliers_id
                                        ');
      $Qsuppliers->bindInt(':suppliers_id', (int)$suppliers['suppliers_id']);
      $Qsuppliers->execute();

      $supplier_products = $Qsuppliers->fetch();

      $mInfo_array = array_merge($suppliers, $supplier_products);
      $mInfo = new objectInfo($mInfo_array);
      }

      if (isset($mInfo) && is_object($mInfo) && ($suppliers['suppliers_id'] == $mInfo->suppliers_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                <td>
<?php 
      if ($suppliers['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $suppliers['suppliers_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $suppliers['suppliers_id']; ?>" />
<?php 
      } 
?>
                </td>
                <td class="dataTableContent"><?php echo $suppliers['suppliers_name']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers['suppliers_manager']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers['suppliers_phone']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers['suppliers_fax']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers['suppliers_email_address']; ?></td>
                <td  class="dataTableContent" align="center">
<?php
      if ($suppliers['suppliers_status'] == '0') {
        echo '<a href="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&action=setflag&flag=1&id=' . $suppliers['suppliers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&action=setflag&flag=0&id=' . $suppliers['suppliers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&mID=' . $suppliers['suppliers_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($mInfo) && is_object($mInfo) && ($suppliers['suppliers_id'] == $mInfo->suppliers_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . osc_href_link('suppliers.php', 'page=' . $_GET['page'] . '&mID=' . $suppliers['suppliers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    } // end while
?>
              </form><!-- end form delete all -->
              <tr>
                <td colspan="12" class="smallText" valign="top"><?php echo $suppliers_split->display_count($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SUPPLIERS); ?></td>
              </tr>
            </table></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

