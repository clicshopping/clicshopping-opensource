<?php
/**
 * customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $error = false;
  $processed = false;

  if (osc_not_null($action)) {
    switch ($action) {
      case 'update':

        $customers_id = osc_db_prepare_input($_GET['cID']);
        $customers_firstname = osc_db_prepare_input($_POST['customers_firstname']);
        $customers_lastname = osc_db_prepare_input($_POST['customers_lastname']);
        $customers_email_address = osc_db_prepare_input($_POST['customers_email_address']);
        $customers_telephone = osc_db_prepare_input($_POST['customers_telephone']);
        $customers_fax = osc_db_prepare_input($_POST['customers_fax']);
        $customers_newsletter = osc_db_prepare_input($_POST['customers_newsletter']);
        $language_id = osc_db_prepare_input($_POST['languages_id']);
        $customers_group_name = osc_db_prepare_input($_POST['customers_group_name']);
        $customers_gender = osc_db_prepare_input($_POST['customers_gender']);
        $customers_dob = osc_db_prepare_input($_POST['customers_dob']);
        $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);
        $customers_cellular_phone = osc_db_prepare_input($_POST['customers_cellular_phone']);
        $customers_notes = osc_db_prepare_input($_POST['customers_notes']);
        $customer_discount = osc_db_prepare_input($_POST['customer_discount']);

        $QmultipleGroups = $OSCOM_PDO->prepare('select distinct customers_group_id
                                                from :table_products_groups
                                               ');

        $QmultipleGroups->execute();

        while ($group_ids = $QmultipleGroups->fetch() ) {

          $QmultipleCustomers = $OSCOM_PDO->prepare('select distinct customers_group_id
                                                      from :table_customers_groups
                                                      where customers_group_id = :customers_group_id
                                                    ');
          $QmultipleCustomers->bindInt(':customers_group_id', (int)$group_ids['customers_group_id'] );
          $QmultipleCustomers->execute();

          if (!($multiple_groups = $QmultipleCustomers->fetch() )) {

            $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_products_groups
                                            where customers_group_id = :customers_group_id
                                           ');
            $Qdelete->bindInt(':customers_group_id', (int)$group_ids['customers_group_id']);

            $Qdelete->execute();

          }
        } // end while

        $QcustomersGroupName = $OSCOM_PDO->prepare('select distinct customers_group_name, 
                                                                    customers_group_id 
                                                    from :table_customers_groups
                                                    order by customers_group_id 
                                                  ');
        $QcustomersGroupName->execute();

        $group_name_check = $QcustomersGroupName->fetch();

// Informations sur la société
       if (ACCOUNT_COMPANY_PRO == 'true') $customers_company = osc_db_prepare_input($_POST['customers_company']);
       if (ACCOUNT_SIRET_PRO == 'true') $customers_siret = osc_db_prepare_input($_POST['customers_siret']);
       if (ACCOUNT_APE_PRO == 'true') $customers_ape = osc_db_prepare_input($_POST['customers_ape']);

// Information numéro de TVA avec transformation de code ISO en majuscule
       if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $customers_tva_intracom_code_iso = osc_db_prepare_input($_POST['customers_tva_intracom_code_iso']);
       if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $customers_tva_intracom_code_iso = strtoupper($customers_tva_intracom_code_iso);
       if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $customers_tva_intracom = osc_db_prepare_input($_POST['customers_tva_intracom']);

// Autorisation aux clients de modifier Les informations de la société
        $customers_modify_company = osc_db_prepare_input($_POST['customers_modify_company']);

// Informations sur le type de facturation
        $customers_options_order_taxe = osc_db_prepare_input($_POST['customers_options_order_taxe']);
        $default_address_id = osc_db_prepare_input($_POST['default_address_id']);
        $entry_street_address = osc_db_prepare_input($_POST['entry_street_address']);
        $entry_suburb = osc_db_prepare_input($_POST['entry_suburb']);
        $entry_postcode = osc_db_prepare_input($_POST['entry_postcode']);
        $entry_city = osc_db_prepare_input($_POST['entry_city']);
        $entry_country_id = osc_db_prepare_input($_POST['entry_country_id']);
        $entry_company = osc_db_prepare_input($_POST['entry_company']);
        $entry_state = osc_db_prepare_input($_POST['entry_state']);
        $entry_telephone = osc_db_prepare_input($_POST['entry_telephone']);

        if (isset($_POST['entry_zone_id'])) $entry_zone_id = osc_db_prepare_input($_POST['entry_zone_id']);

// Autorisation aux clients de modifier adresse principal
        $customers_modify_address_default = osc_db_prepare_input($_POST['customers_modify_address_default']);
        $customers_add_address = osc_db_prepare_input($_POST['customers_add_address']);

// Contrôle des saisies faites sur les champs TVA Intracom
        if ((strlen($customers_tva_intracom_code_iso) > '0') || (strlen($customers_tva_intracom) > '0')) {

          $QcustomersTva = $OSCOM_PDO->prepare('select countries_iso_code_2
                                               from countries
                                               where countries_iso_code_2 = :countries_iso_code_2
                                              ');
          $QcustomersTva->bindValue(':countries_iso_code_2',$customers_tva_intracom_code_iso );

          $QcustomersTva->execute();

          if ($QcustomersTva->fetch() ) {
            $customers_tva_intracom_code_iso_error = false;
          } else {
            $error = true;
            $customers_tva_intracom_code_iso_error = true;
          }
        }

        if (strlen($customers_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
          $error = true;
          $entry_firstname_error = true;
        } else {
          $entry_firstname_error = false;
        }

        if (strlen($customers_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
          $error = true;
          $entry_lastname_error = true;
        } else {
          $entry_lastname_error = false;
        }


// suppression de la date de vérification de l'anniversaire

        $entry_email_address_error = false;

        if (!osc_validate_email($customers_email_address)) {
          $error = true;
          $entry_email_address_check_error = true;
        } else {
          $entry_email_address_check_error = false;
        }

        if (strlen($entry_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
          $error = true;
          $entry_street_address_error = true;
        } else {
          $entry_street_address_error = false;
        }

        if (strlen($entry_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
          $error = true;
          $entry_post_code_error = true;
        } else {
          $entry_post_code_error = false;
        }

        if (strlen($entry_city) < ENTRY_CITY_MIN_LENGTH) {
          $error = true;
          $entry_city_error = true;
        } else {
          $entry_city_error = false;
        }

        if ($entry_country_id == false) {
          $error = true;
          $entry_country_error = true;
        } else {
          $entry_country_error = false;
        }

        if (ACCOUNT_STATE == 'true') {
          if ($entry_country_error == true) {
            $entry_state_error = true;
          } else {
            $zone_id = 0;
            $entry_state_error = false;

            $Qcheck = $OSCOM_PDO->prepare("select count(*) as total 
                                             from :table_zones
                                             where zone_country_id = :zone_country_id
                                          ");
            $Qcheck->bindValue(':zone_country_id',(int)$entry_country_id);
            $Qcheck->execute();

            $check_value = $Qcheck->fetch();
            $entry_state_has_zones = ($check_value['total'] > 0);

            if ($entry_state_has_zones == true) {
            
              $Qzone = $OSCOM_PDO->prepare('select zone_code
                                             from :table_zones
                                             where zone_country_id = :zone_country_id
                                             and zone_name = :zone_name
                                           ');
              $Qzone->bindInt(':zone_country_id', (int)$entry_country_id);
              $Qzone->bindValue(':zone_name',  $entry_state);

              $Qzone->execute();


              if ($Qzone->rowCount()  == 1) {
                $zone_values = $Qzone->fetch();
                $entry_zone_id = $zone_values['zone_id'];
              } else {
                $error = true;
                $entry_state_error = true;
              }
            } else {
              if (strlen($entry_state) < ENTRY_STATE_MIN_LENGTH) {
                $error = true;
                $entry_state_error = true;
              }
            }
         }
      }

      if (strlen($customers_telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;
        $entry_telephone_error = true;
      } else {
        $entry_telephone_error = false;
      }

      $QcheckEmail = $OSCOM_PDO->prepare('select customers_email_address
                                         from :table_customers
                                         where customers_email_address = :customers_email_address
                                         and customers_id <> :customers_id
                                        ');
      $QcheckEmail->bindValue(':customers_email_address', $customers_email_address);
      $QcheckEmail->bindInt(':customers_id',(int)$customers_id);
      $QcheckEmail->execute();

      if ($QcheckEmail->rowCount() > 0) {
        $error = true;
        $entry_email_address_exists = true;
      } else {
        $entry_email_address_exists = false;
      }

      if ($error == false) {

        $sql_data_array = array('customers_firstname' => $customers_firstname,
                                'customers_lastname' => $customers_lastname,
                                'customers_email_address' => $customers_email_address,
                                'customers_telephone' => $customers_telephone,
                                'customers_fax' => $customers_fax,
                                'customers_newsletter' => $customers_newsletter,
                                'languages_id' => $language_id,
                                'customers_group_id' => $customers_group_id,
                                'customers_cellular_phone' => $customers_cellular_phone,
                                'customer_discount' => $customer_discount                                
                             );

//        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = osc_date_raw($customers_dob);
        $sql_data_array['customers_dob'] = osc_date_raw($customers_dob);

        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $customers_gender;

// Informations sur la société
        if (ACCOUNT_COMPANY_PRO == 'true') $sql_data_array['customers_company'] = $customers_company;
        if (ACCOUNT_SIRET_PRO == 'true') $sql_data_array['customers_siret'] = $customers_siret;
        if (ACCOUNT_APE_PRO == 'true') $sql_data_array['customers_ape'] = $customers_ape;
        if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $sql_data_array['customers_tva_intracom_code_iso'] = $customers_tva_intracom_code_iso;
        if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $sql_data_array['customers_tva_intracom'] = $customers_tva_intracom;

// Autorisation aux clients de modifier informations société et adresse principal + Ajout adresse
        if ($customers_modify_company != '1') $customers_modify_company = '0';
        if ($customers_modify_address_default != '1') $customers_modify_address_default = '0';
        if ($customers_add_address != '1') $customers_add_address = '0';

        $sql_data_array['customers_modify_company'] = $customers_modify_company;
        $sql_data_array['customers_modify_address_default'] = $customers_modify_address_default;
        $sql_data_array['customers_add_address'] = $customers_add_address;

        if (!is_numeric($customer_discount)) {
          $customer_discount = '';
          $sql_data_array['customer_discount'] = $customer_discount;
        } else {
           $sql_data_array['customer_discount'] = $customer_discount;         
        }

        $OSCOM_PDO->save('customers', $sql_data_array, ['customers_id' => (int)$customers_id ] );

        $Qupdate = $OSCOM_PDO->prepare('update :table_customers_info
                                        set customers_info_date_account_last_modified = now()
                                        where customers_info_id = :customers_info_id
                                      ');

        $Qupdate->bindInt(':customers_info_id', (int)$customers_id);
        $Qupdate->execute();

// notes clients
        if (!empty($customers_notes)) {
          osc_db_query("insert into customers_notes (customers_id,
                                                     customers_notes,
                                                     customers_notes_date,
                                                     user_administrator
                                                    )
                        values ('" . (int)$customers_id . "', 
                                '" . $customers_notes . "',
                                 now(),                               
                                '" . osc_user_admin ($user_administrator) . "'
                                )
                      ");

        } // end empty($customers_notes)

        if ($entry_zone_id > 0) $entry_state = '';

        $sql_data_array = array('entry_firstname' => $customers_firstname,
                                'entry_lastname' => $customers_lastname,
                                'entry_street_address' => $entry_street_address,
                                'entry_postcode' => $entry_postcode,
                                'entry_city' => $entry_city,
                                'entry_country_id' => $entry_country_id,
                                'entry_telephone' => $entry_telephone,
                                );

        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $entry_company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $entry_suburb;

        if (ACCOUNT_STATE == 'true') {
          if ($entry_zone_id > 0) {
            $sql_data_array['entry_zone_id'] = $entry_zone_id;
            $sql_data_array['entry_state'] = '';
          } else {
            $sql_data_array['entry_zone_id'] = '0';
            $sql_data_array['entry_state'] = $entry_state;
          }
        }

        $OSCOM_PDO->save('address_book', $sql_data_array, ['customers_id' => (int)$customers_id,
                                                           'address_book_id' => (int)$default_address_id
                                                          ]
                        );

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_ADMIN =='true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_customers.php');
        }
//***************************************
// End odoo web service
//***************************************

        osc_redirect(osc_href_link('customers.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_id));

        } else if ($error == true) {
          $cInfo = new objectInfo($_POST);
          $processed = true;

// Requête SQL afin de récuperer les informations sur l'ID de l'adresse par défaut en cas d'une erreur de saisie pour l'onglet carnet d'adresse.
          $QcustomersDefaultAddress = $OSCOM_PDO->prepare("select customers_id, 
                                                                  customers_default_address_id 
                                                           from :table_customers
                                                           where customers_id = :customers_id
                                                          ");
          $QcustomersDefaultAddress->bindInt(':customers_id',(int)$_GET['cID']);
          $QcustomersDefaultAddress->execute();

          $customers_default_address = $QcustomersDefaultAddress->fetch();

          $cInfo->customers_default_address_id = $customers_default_address['customers_default_address_id'];

// Requête SQL afin de récuperer les informations facturations et livraisons du groupe client en cas d'une erreur de saisie pour l'onglet carnet d'adresse.
          if ($cInfo->customers_group_id != 0 ) {
            $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name, 
                                                          group_order_taxe, 
                                                          group_payment_unallowed, 
                                                          group_shipping_unallowed 
                                                   from :table_customers_groups 
                                                   where customers_group_id = :customers_group_id
                                                  ');
            $QcustomersGroup->bindInt(':customers_group_id',(int)$cInfo->customers_group_id);
            $QcustomersGroup->execute();

            $customers_group = $QcustomersGroup->fetch();

            $cInfo_group = new objectInfo($customers_group);
           }
        } // end $error

      break;

// the images are not deleted in this case

      case 'delete_all': 
 
       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $customers['customers_id'] ) {

           $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_address_book
                                            where customers_id = :customers_id
                                          ');
           $Qdelete->bindInt(':customers_id', (int)$customers['customers_id']);
           $Qdelete->execute();

           $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_customers
                                            where customers_id = :customers_id
                                          ');
           $Qdelete->bindInt(':customers_id', (int)$customers['customers_id']);
           $Qdelete->execute();

           $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_customers_info
                                            where customers_info_id = :customers_info_id
                                          ');
           $Qdelete->bindInt(':customers_info_id', (int)$customers['customers_id']);
           $Qdelete->execute();

           $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_customers_basket
                                            where customers_id = :customers_id
                                          ');
           $Qdelete->bindInt(':customers_id', (int)$customers['customers_id']);
           $Qdelete->execute();

           $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_customers_basket_attributes
                                            where customers_id = :customers_id
                                          ');
           $Qdelete->bindInt(':customers_id', (int)$customers['customers_id']);
           $Qdelete->execute();

           $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_whos_online
                                            where customer_id = :customer_id
                                          ');
           $Qdelete->bindInt(':customer_id', (int)$customers['customers_id']);
           $Qdelete->execute();
           
           if (isset($_POST['delete_reviews']) && ($_POST['delete_reviews'] == 'on')) {

             $Qreviews = $OSCOM_PDO->prepare('select reviews_id
                                             from :table_reviews
                                             and customers_id = :customers_id
                                            ');
             $Qreviews->bindInt(':customers_id',(int)$customers['customers_id']);
             $Qreviews->execute();

             while ($reviews =$Qreviews->fetch() ) {

               $Qdelete = $OSCOM_PDO->prepare('delete
                                              from :table_reviews_description
                                              where reviews_id = :reviews_id
                                            ');
               $Qdelete->bindInt(':reviews_id',  (int)$reviews['reviews_id']);
               $Qdelete->execute();

             }

            $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_reviews
                                            where customers_id = :customers_id 
                                          ');
            $Qdelete->bindInt(':customers_id', (int)$customers['customers_id']);
            $Qdelete->execute();

           } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_reviews
                                            set customers_id = :customers_id 
                                            where customers_id = :customers_id
                                          ');
            $Qupdate->bindInt(':customers_id', null );
            $Qupdate->bindInt(':customers_info_id', (int)$customers['customers_id']);
            $Qupdate->execute();
           }
         }
       } 
       osc_redirect(osc_href_link('customers.php'));
      break;

      default:

          $Qcustomers = $OSCOM_PDO->prepare("select c.customers_id,
                                                    c.customers_company,
                                                    c.customers_siret,
                                                    c.customers_ape,
                                                    c.customers_tva_intracom,
                                                    c.customers_tva_intracom_code_iso,
                                                    c.customers_gender,
                                                    c.customers_firstname,
                                                    c.customers_lastname,
                                                    c.customers_dob,
                                                    c.customers_email_address,
                                                    c.customers_options_order_taxe,
                                                    c.customers_modify_company,
                                                    c.customers_modify_address_default,
                                                    c.customers_add_address,
                                                    a.entry_company,
                                                    a.entry_street_address,
                                                    a.entry_suburb,
                                                    a.entry_postcode,
                                                    a.entry_city,
                                                    a.entry_state,
                                                    a.entry_zone_id,
                                                    a.entry_country_id,
                                                    c.customers_telephone,
                                                    c.customers_fax,
                                                    c.customers_newsletter,
                                                    c.languages_id,
                                                    c.customers_group_id,
                                                    c.customers_default_address_id,
                                                    c.customers_cellular_phone,
                                                    c.customer_discount
                                            from :table_customers c left join :table_address_book a on c.customers_default_address_id = a.address_book_id
                                            where a.customers_id = c.customers_id
                                            and c.customers_id = '" . (int)$_GET['cID'] . "'
                                          ");
          $Qcustomers->bindValue(':customers_id',(int)$_GET['cID']);
          $Qcustomers->execute();

          $customers = $Qcustomers->fetch();


          $cInfo = new objectInfo($customers);

// Lecture sur la base de données des informations facturations et livraison du groupe client
        if ($cInfo->customers_group_id != 0 ) {

          $QcustomersGroup = $OSCOM_PDO->prepare("select customers_group_name,
                                                          group_order_taxe,
                                                          group_payment_unallowed,
                                                          group_shipping_unallowed
                                                   from :table_customers_groups
                                                   where customers_group_id = :customers_group_id
                                                  ");
          $QcustomersGroup->bindInt(':customers_group_id', (int)$cInfo->customers_group_id );
          $QcustomersGroup->execute();

          $customers_group = $QcustomersGroup->fetch();

           $cInfo_group = new objectInfo($customers_group);
        }

        $languages = osc_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $values_languages_id[$i]=array ('id' =>$languages[$i]['id'], 'text' =>$languages[$i]['name']);
        }
 
// Affiche la case cochée par défaut pour le mode de facturation utilisée avec taxe ou non
         if (!isset($cInfo->customers_options_order_taxe)) $cInfo->customers_options_order_taxe = '0';
         switch ($cInfo->customers_options_order_taxe) {
           case '0': $status_order_taxe = true; $status_order_no_taxe = false; break;
           case '1': $status_order_taxe = false; $status_order_no_taxe = true; break;
          default: $status_order_taxe = true; $status_order_no_taxe = false;
         }
      break;
    }
  }

  if(empty($action)) {

// Recherche
    $search = '';
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "where  (c.customers_id like '" . $keywords . "' or 
                         c.customers_lastname like '%" . $keywords . "%' 
                         or c.customers_firstname like '%" . $keywords . "%' 
                         or c.customers_email_address like '%" . $keywords . "%' 
                         or a.entry_company like '%" . $keywords . "%'
                        ) 
               ";
    }

// affichage des données du tableau
    $customers_query_raw = "select distinct c.customers_id, 
                                            c.customers_company, 
                                            c.customers_lastname, 
                                            c.customers_firstname, 
                                            c.customers_group_id, 
                                            a.entry_company, 
                                            c.customers_email_address, 
                                            a.entry_country_id, 
                                            c.member_level,
                                            c.customers_email_validation
                            from customers c left join address_book a on c.customers_id = a.customers_id
                            and c.customers_default_address_id = a.address_book_id " . $search . " 
                            order by c.customers_id DESC
                          ";
    $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $customers_query_raw, $customers_query_numrows);
    $customers_query = osc_db_query($customers_query_raw);
  } // end empty

  require('includes/header.php');
?>
<?php
  if ($action == 'edit' || $action == 'update') {
?>
<script type="text/javascript"><!--

function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

<?php if (ACCOUNT_GENDER == 'true') { ?>
  if (document.customers.customers_gender[0].checked || document.customers.customers_gender[1].checked) {
  } else {
    error_message = error_message + "<?php echo JS_GENDER; ?>";
    error = 1;
  }
<?php } ?>

  if (customers_firstname.length < <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_FIRST_NAME; ?>";
    error = 1;
  }

  if (customers_lastname.length < <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_LAST_NAME; ?>";
    error = 1;
  }

  if (customers_dob.length < <?php echo ENTRY_DOB_MIN_LENGTH; ?>) {
       error_message = error_message + "<?php echo JS_DOB; ?>";
       error = 1;
  }

  if (entry_street_address.length < <?php echo ENTRY_STREET_ADDRESS_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_ADDRESS; ?>";
    error = 1;
  }

  if (entry_postcode.length < <?php echo ENTRY_POSTCODE_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_POST_CODE; ?>";
    error = 1;
  }

  if (entry_city.length < <?php echo ENTRY_CITY_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_CITY; ?>";
    error = 1;
  }

<?php
  if (ACCOUNT_STATE == 'true') {
?>
  if (document.customers.elements['entry_state'].type != "hidden") {
    if (document.customers.entry_state.value.length < <?php echo ENTRY_STATE_MIN_LENGTH; ?>) {
       error_message = error_message + "<?php echo JS_STATE; ?>";
       error = 1;
    }
  }
<?php
  }
?>

  if (document.customers.elements['entry_country_id'].type != "hidden") {
    if (document.customers.entry_country_id.value == 0) {
      error_message = error_message + "<?php echo JS_COUNTRY; ?>";
      error = 1;
    }
  }

  if (customers_telephone.length < <?php echo ENTRY_TELEPHONE_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_TELEPHONE; ?>";
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}
//--></script>
<?php
  }
?>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="90%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<!-- //################################################################################################################ -->
<!-- //                                               FICHE CLIENT                                                      -->
<!-- //################################################################################################################ -->
<?php
  if ($action == 'edit' || $action == 'update') {
    $newsletter_array = array(array('id' => '1', 'text' => ENTRY_NEWSLETTER_YES),
                              array('id' => '0', 'text' => ENTRY_NEWSLETTER_NO));

    $customers_email_array = array(array('id' => '1', 'text' => ENTRY_CUSTOMERS_YES),
                              array('id' => '0', 'text' => ENTRY_CUSTOMERS_NO));


// B2B Shipping
// Recherche des modules de paiement en production
    $payments_unallowed = explode (",",$cInfo_group->group_payment_unallowed);
    $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
    $module_key = 'MODULE_PAYMENT_INSTALLED';

    $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_array = array();
    if ($dir = @dir($module_directory)) {
      while ($file = $dir->read()) {
        if (!is_dir($module_directory . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $directory_array[] = $file;
          }
        }
      }
      sort($directory_array);
      $dir->close();
    }
// Recherche des modules de livraison en production
    $shipping_unallowed = explode (",",$cInfo_group->group_shipping_unallowed);
    $module_shipping_directory = DIR_FS_CATALOG_MODULES . 'shipping/';
    $module_shipping_key = 'MODULE_SHIPPING_INSTALLED';

    $file_shipping_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_shipping_array = array();
    if ($dir_shipping = @dir($module_shipping_directory)) {
      while ($file_shipping = $dir_shipping->read()) {
        if (!is_dir($module_shipping_directory . $file_shipping)) {
          if (substr($file_shipping, strrpos($file_shipping, '.')) == $file_shipping_extension) {
            $directory_shipping_array[] = $file_shipping;
          }
        }
      }
      sort($directory_shipping_array);
      $dir_shipping->close();
    }
// ####### END - Shipping B2B #######
?>
     <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
     <div class="adminTitle">
        <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/client_editer.gif', HEADING_TITLE, '40', '40'); ?></span>
        <span class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_EDIT . (int)$_GET['cID'] . '&nbsp;-&nbsp;' . $cInfo->customers_lastname . '&nbsp;' . $cInfo->customers_firstname; ?></span>
        <span class="pull-right"><?php echo osc_draw_form('customers', 'customers.php', osc_get_all_get_params(array('action')) . 'action=update', 'post', 'onSubmit="return check_form();"') . osc_draw_hidden_field('default_address_id', $cInfo->customers_default_address_id); ?></span>
        <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></span>
        <span class="pull-right"><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></span>
        <span class="pull-right"><?php echo '<a href="' . osc_href_link('customers.php', osc_get_all_get_params(array('action'))) .'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
     </div>
<?php
    if ($error == true) {
?>
    <div class=""row">
     <?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?>
    </div>
    <table border="0" width="100%" cellspacing="3" cellpadding="0" class="messageStackError">
       <tr>
        <td class="messageStackError" height="20" colspan="2"><table width="100%">
          <tr>
           <td width="27" align="center"><?php echo osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING); ?></td>
           <td><?php echo WARNING_EDIT_CUSTOMERS; ?></td>
          </tr>
         </table></td>
       </tr>
    </table>
<?php
    }
?>
    <div class=""row">
     <?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?>
    </div>
    <tr>
<!-- //################################################################################################################ -->
<!--          ONGLET NOM & ADRESSE          //-->
<!-- //################################################################################################################ -->
     <td>
       <div>
         <ul class="nav nav-tabs" role="tablist"  id="myTab">
           <li class="active"><?php echo '<a href="' . substr(osc_href_link('customers.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' . TAB_GENERAL . '</a>'; ?></a></li>
           <li><?php echo '<a href="' . substr(osc_href_link('customers.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' . TAB_SOCIETE; ?></a></li>
           <li><?php echo '<a href="' . substr(osc_href_link('customers.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' . TAB_ADRESSE_BOOK; ?></a></li>
           <li><?php echo '<a href="' . substr(osc_href_link('customers.php', osc_get_all_get_params()), strlen($base_url)) . '#tab4" role="tab" data-toggle="tab">' . TAB_ORDERS; ?></a></li>
           <li><?php echo '<a href="' . substr(osc_href_link('customers.php', osc_get_all_get_params()), strlen($base_url)) . '#tab5" role="tab" data-toggle="tab">' . TAB_SHIPPING; ?></a></li>
           <li><?php echo '<a href="' . substr(osc_href_link('customers.php', osc_get_all_get_params()), strlen($base_url)) . '#tab6" role="tab" data-toggle="tab">' . TAB_NOTES; ?></a></li>
         </ul>
         <div class="tabsClicShopping">
          <div class="tab-content">
<!-- //################################################################################################################ -->
<!--          ONGLET NOM & ADRESSE          //-->
<!-- //################################################################################################################ -->
            <div class="tab-pane active" id="tab1">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td class="mainTitle"><?php echo CATEGORY_PERSONAL; ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    if (ACCOUNT_GENDER == 'true') {
?>
                  <tr>
                   <td class="main"><?php echo ENTRY_GENDER; ?></td>
                   <td class="main">
<?php
      if ($error == true) {
        if ($entry_gender_error == true) {
          echo osc_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . osc_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . ENTRY_GENDER_ERROR;
        } else {
          echo ($cInfo->customers_gender == 'm') ? MALE : FEMALE;
          echo osc_draw_hidden_field('customers_gender');
        }
      } else {
        echo osc_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . osc_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . FEMALE;
      }
?>
                   </td>
                  </tr>
<?php
    }
?>
                  <tr>
                   <td class="main"><?php echo ENTRY_FIRST_NAME; ?></td>
                   <td class="main">
<?php
    if ($error == true) {
      if ($entry_firstname_error == true) {
        echo osc_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_FIRST_NAME_ERROR;
      } else {
        echo $cInfo->customers_firstname . osc_draw_hidden_field('customers_firstname');
      }
    } else {
      echo osc_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32"', true);
    }
?>
                   </td>
                  </tr>
                  <tr>
                   <td class="main"><?php echo ENTRY_LAST_NAME; ?></td>
                   <td class="main">
<?php
    if ($error == true) {
      if ($entry_lastname_error == true) {
        echo osc_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_LAST_NAME_ERROR;
      } else {
        echo $cInfo->customers_lastname . osc_draw_hidden_field('customers_lastname');
      }
    } else {
      echo osc_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32"', true);
    }
?>
                   </td>
                  </tr>
  <script type="text/javascript">
  $(function() {
    $('#customers_dob').datepicker({
/*dateFormat: '<?php echo JQUERY_DATEPICKER_FORMAT; ?>', */
      dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0'      
    });
  });
  </script>

                  <tr>
                    <td class="main"><?php echo ENTRY_DATE_OF_BIRTH; ?></td>
                    <td class="main">

<?php

        if ($error == true) {
          if ($entry_date_of_birth_error == true) {
            echo osc_draw_input_field('customers_dob', $OSCOM_Date->getShort($cInfo->customers_dob), 'id ="customers_dob"  maxlength="10" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_DATE_OF_BIRTH_ERROR;
          } else {
            echo $cInfo->customers_dob . osc_draw_hidden_field('customers_dob');
          }
        } else {         
          echo osc_draw_input_field('customers_dob', $OSCOM_Date->getShort($cInfo->customers_dob), 'id ="customers_dob"', false);
        }
?>
                     </td>
                   </tr>
                   <tr>
                      <td class="main"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                      <td class="main">
<?php
    if ($error == true) {
      if ($entry_email_address_error == true) {
        echo osc_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR;
      } elseif ($entry_email_address_check_error == true) {
        echo osc_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_CHECK_ERROR;
      } elseif ($entry_email_address_exists == true) {
        echo osc_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
      } else {
        echo $customers_email_address . osc_draw_hidden_field('customers_email_address');
      }
    } else {
      echo osc_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"', true);
    }
?>
                      </td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
                      <td class="main">
<?php
    if ($error == true) {
      if ($entry_telephone_error == true) {
        echo osc_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_TELEPHONE_NUMBER_ERROR;
      } else {
        echo $cInfo->customers_telephone . osc_draw_hidden_field('customers_telephone');
      }
    } else {
      echo osc_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32"', true);
    }
?>
                      </td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_CELLULAR_PHONE_NUMBER; ?></td>
                      <td class="main">
<?php
    if ($processed == true) {
      echo $cInfo->customers_cellular_phone . osc_draw_hidden_field('customers_cellular_phone');
    } else {
      echo osc_draw_input_field('customers_cellular_phone', $cInfo->customers_cellular_phone, 'maxlength="32"', true);
    }
?>
                      </td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_FAX_NUMBER; ?></td>
                      <td class="main">
<?php
    if ($processed == true) {
      echo $cInfo->customers_fax . osc_draw_hidden_field('customers_fax');
    } else {
      echo osc_draw_input_field('customers_fax', $cInfo->customers_fax, 'maxlength="32"');
    }
?>
                      </td>
                    </tr>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="mainTitle"><?php echo CATEGORY_ADDRESS_DEFAULT; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                     <td class="main"><?php echo ENTRY_STREET_ADDRESS; ?></td>
                     <td class="main">
<?php
    if ($error == true) {
      if ($entry_street_address_error == true) {
        echo osc_draw_input_field('entry_street_address', $cInfo->entry_street_address, 'maxlength="64" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_STREET_ADDRESS_ERROR;
      } else {
        echo $cInfo->entry_street_address . osc_draw_hidden_field('entry_street_address');
      }
    } else {
      echo osc_draw_input_field('entry_street_address', $cInfo->entry_street_address, 'maxlength="64"', true);
    }
?>
                     </td>
                    </tr>
<?php
    if (ACCOUNT_SUBURB == 'true') {
?>
                    <tr>
                      <td class="main"><?php echo ENTRY_SUBURB; ?></td>
                      <td class="main">
<?php
     if ($error == true) {
       if ($entry_suburb_error == true) {
         echo osc_draw_input_field('suburb', $cInfo->entry_suburb, 'maxlength="32"') . '&nbsp;' . ENTRY_SUBURB_ERROR;
       } else {
         echo $cInfo->entry_suburb . osc_draw_hidden_field('entry_suburb');
       }
     } else {
       echo osc_draw_input_field('entry_suburb', $cInfo->entry_suburb, 'maxlength="32"');
     }
?>
                      </td>
                    </tr>
<?php
  }
?>
                    <tr>
                     <td class="main"><?php echo ENTRY_POST_CODE; ?></td>
                     <td class="main">
<?php
    if ($error == true) {
      if ($entry_post_code_error == true) {
        echo osc_draw_input_field('entry_postcode', $cInfo->entry_postcode, 'maxlength="8" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_POST_CODE_ERROR;
      } else {
        echo $cInfo->entry_postcode . osc_draw_hidden_field('entry_postcode');
      }
    } else {
      echo osc_draw_input_field('entry_postcode', $cInfo->entry_postcode, 'maxlength="8"', true);
    }
?>
                     </td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_CITY; ?></td>
                      <td class="main">
<?php
    if ($error == true) {
      if ($entry_city_error == true) {
        echo osc_draw_input_field('entry_city', $cInfo->entry_city, 'maxlength="32" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_CITY_ERROR;
      } else {
        echo $cInfo->entry_city . osc_draw_hidden_field('entry_city');
      }
    } else {
      echo osc_draw_input_field('entry_city', $cInfo->entry_city, 'maxlength="32"', true);
    }
?>
                      </td>
                    </tr>
<?php
  if (ACCOUNT_STATE == 'true') {
?>
                    <tr>
                      <td class="main"><?php echo ENTRY_STATE; ?></td>
                      <td class="main">
<?php
    $entry_state = osc_get_zone_name($cInfo->entry_country_id, $cInfo->entry_zone_id, $cInfo->entry_state);
    if ($error == true) {
      if ($entry_state_error == true) {
        if ($entry_state_has_zones == true) {
          $zones_array = array();

          $Qzones = $OSCOM_PDO->prepare('select zone_name
                                         from :table_zones
                                         where zone_country_id = :zone_country_id
                                         order by zone_name
                                       ');
          $Qzones->bindInt(':zone_country_id', osc_db_input($cInfo->entry_country_id));
          $Qzones->execute();

          while ($zones_values = $Qzones->fetch() ) {
            $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
          }
          echo osc_draw_pull_down_menu('entry_state', $zones_array) . '&nbsp;' . ENTRY_STATE_ERROR;
        } else {
          echo osc_draw_input_field('entry_state', osc_get_zone_name($cInfo->entry_country_id, $cInfo->entry_zone_id, $cInfo->entry_state)) . '&nbsp;' . ENTRY_STATE_ERROR;
        }
      } else {
        echo $entry_state . osc_draw_hidden_field('entry_zone_id') . osc_draw_hidden_field('entry_state');
      }
    } else {
      echo osc_draw_input_field('entry_state', osc_get_zone_name($cInfo->entry_country_id, $cInfo->entry_zone_id, $cInfo->entry_state));
    }
?>
                        </td>
                      </tr>
 <?php
  }
?>
                      <tr>
                        <td class="main"><?php echo ENTRY_COUNTRY; ?></td>
                        <td class="main">
<?php
    if ($error == true) {
      if ($entry_country_error == true) {
        echo osc_draw_pull_down_menu('entry_country_id', osc_get_countries(), $cInfo->entry_country_id) . '&nbsp;' . ENTRY_COUNTRY_ERROR;
      } else {
        echo osc_get_country_name($cInfo->entry_country_id) . osc_draw_hidden_field('entry_country_id');
      }
    } else {
      echo osc_draw_pull_down_menu('entry_country_id', osc_get_countries(), $cInfo->entry_country_id);
    }
?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                 <tr>
                    <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                 </tr>
                 <tr>
                    <td class="mainTitle"><?php echo CATEGORY_NEWSLETTER; ?></td>
                 </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                 <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                     <td class="main"><?php echo ENTRY_NEWSLETTER; ?></td>
                     <td class="main">
<?php
    if ($processed == true) {
      if ($cInfo->customers_newsletter == '1') {
        echo ENTRY_NEWSLETTER_YES;
      } else {
        echo ENTRY_NEWSLETTER_NO;
      }
      echo osc_draw_hidden_field('customers_newsletter');
    } else {
      echo osc_draw_pull_down_menu('customers_newsletter', $newsletter_array, (($cInfo->customers_newsletter == '1') ? '1' : '0'));
    }
?>
                     </td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_NEWSLETTER_LANGUAGE; ?></td>
                      <td class="main"><?php echo osc_draw_pull_down_menu('languages_id', $values_languages_id,  $customers['languages_id']); ?> </td>
                    </tr>
                  </table></td>
                </tr>
              </table>
            </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Infos Société          //-->
<!-- //################################################################################################################ -->
<?php
// Insertion du numéro de Siret, code APE et TVA Intracom
    if ((ACCOUNT_COMPANY == 'true') || (ACCOUNT_COMPANY_PRO == 'true') || (ACCOUNT_SIRET_PRO == 'true') || (ACCOUNT_APE_PRO == 'true') || (ACCOUNT_TVA_INTRACOM_PRO == 'true')) {
?>
            <div class="tab-pane" id="tab2">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                 <td class="mainTitle"><?php echo CATEGORY_COMPANY; ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                 <td class="mainTitle" align="right"><table border="0" cellpadding="0" cellspacing="0">
                   <tr>
                     <td class="mainTitleTexteSeul"><?php echo '&nbsp;' . ENTRY_CUSTOMERS_MODIFY_COMPANY . '&nbsp;'; ?></td>
<?php
      if ($error == true) {
?>
                    <td class="mainTitleTexteSeul"><?php
        if ($cInfo->customers_modify_company != '1') echo ':&nbsp;' . ERROR_ENTRY_CUSTOMERS_MODIFY_NO;
        if ($cInfo->customers_modify_company == '1') echo ':&nbsp;' . ERROR_ENTRY_CUSTOMERS_MODIFY_YES;
?>
                     </td>
<?php
        echo osc_draw_hidden_field('customers_modify_company');
      } else {
?>
                     <td class="mainTitleTexteSeul"><?php echo osc_draw_checkbox_field('customers_modify_company', '1', $cInfo->customers_modify_company); ?></td>
<?php
      }
?>
                   </tr>
                 </table></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
<?php
// Insertion du numéro de Siret, code APE et TVA Intracom
      if (ACCOUNT_COMPANY_PRO == 'true') {
?>
                  <tr>
                    <td class="main"><?php echo ENTRY_COMPANY; ?></td>
                    <td class="main">
<?php
       if ($error == true) {
         if ($entry_company_error == true) {
           echo osc_draw_input_field('customers_company', $cInfo->customers_company, 'maxlength="32" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_COMPANY_ERROR;
         } else {
           echo $cInfo->customers_company . osc_draw_hidden_field('customers_company');
         }
       } else {
         echo osc_draw_input_field('customers_company', $cInfo->customers_company, 'maxlength="32"');
       }
?>
                    </td>
                  </tr>
<?php
    }
    if (ACCOUNT_SIRET_PRO == 'true') {
?>
                  <tr>
                    <td class="main"><?php echo ENTRY_SIRET; ?></td>
                    <td class="main">
<?php
      if ($error == true) {
        echo $cInfo->customers_siret . osc_draw_hidden_field('customers_siret');
      } else {
        echo osc_draw_input_field('customers_siret', $cInfo->customers_siret, 'maxlength="14"');
      }
        echo '&nbsp;<span class="fieldRequired">' . ENTRY_SIRET_EXEMPLE . '</span>';
?>
                    </td>
                  </tr>
<?php
      }
      if (ACCOUNT_APE_PRO == 'true') {
?>
                  <tr>
                    <td class="main"><?php echo ENTRY_APE; ?></td>
                    <td class="main">
<?php
       if ($error == true) {
         echo $cInfo->customers_ape . osc_draw_hidden_field('customers_ape');
       } else {
         echo osc_draw_input_field('customers_ape', $cInfo->customers_ape, 'maxlength="4"');
       }
       echo '&nbsp;<span class="fieldRequired">' . ENTRY_APE_EXEMPLE . '</span>';
?>
                    </td>
                  </tr>
<?php
      }
      if (ACCOUNT_TVA_INTRACOM_PRO == 'true') {
?>
                  <tr>
                    <td class="main"><?php echo ENTRY_TVA; ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td class="main">
<?php
       if ($error == true) {
         if ($customers_tva_intracom_code_iso_error == true) {
            echo ENTRY_COUNTRY . osc_draw_input_field('customers_tva_intracom_code_iso', $cInfo->customers_tva_intracom_code_iso, 'maxlength="2" size="2" style="border: 2px solid #FF0000"') . '&nbsp;' . $cInfo->customers_tva_intracom . osc_draw_hidden_field('customers_tva_intracom') . '&nbsp;' . ENTRY_TVA_ISO_ERROR;
         } else {
            echo ENTRY_TVA .  $cInfo->customers_tva_intracom . osc_draw_hidden_field('customers_tva_intracom_code_iso') . osc_draw_hidden_field('customers_tva_intracom');
         }
       } else {
           echo ENTRY_COUNTRY . osc_draw_input_field('customers_tva_intracom_code_iso', $cInfo->customers_tva_intracom_code_iso, 'maxlength="2" size="2"');
           echo '<br />'. ENTRY_TVA . osc_draw_input_field('customers_tva_intracom', $cInfo->customers_tva_intracom, 'maxlength="14"');
       }
?>
<!-- lien pointant sur le site de vérification -->
                       <a href="<?php echo 'http://ec.europa.eu/taxation_customs/vies/vieshome.do?ms=' . $cInfo->customers_tva_intracom_code_iso . '&iso='.$cInfo->customers_tva_intracom_code_iso.'&vat=' . $cInfo->customers_tva_intracom; ?>" target="_blank"><?php echo TVA_INTRACOM_VERIFY; ?></a>
                    </td>
                  </tr>
 <?php
     }
?>
                 </table></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                   <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_CUSTOMERS_IMAGE); ?></td>
                   <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_CUSTOMERS_TVA; ?></strong></td>
                  </tr>
                  <tr>
                   <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                   <td class="main"><?php echo TITLE_AIDE_TVA_CUSTOMERS; ?></td>
                  </tr>
                 </table></td>
               </tr>
              </table>
             </div>
<?php
    }
?>
<!-- //################################################################################################################ -->
<!--          ONGLET Carnet d'adresses          //-->
<!-- //################################################################################################################ -->
            <div class="tab-pane" id="tab3">
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminform">
               <tr>
                <td align="right"><table border="0" cellpadding="0" cellspacing="0">
<?php
    if ($error == true) {
?>
                  <tr>
                    <td class="main"><?php echo '&nbsp;' . ENTRY_CUSTOMERS_ADD_ADDRESS . '&nbsp;:&nbsp;'; ?></td>
                    <td class="main">
<?php
      if ($cInfo->customers_add_address != '1') echo ERROR_ENTRY_CUSTOMERS_MODIFY_NO;
      if ($cInfo->customers_add_address == '1') echo ERROR_ENTRY_CUSTOMERS_MODIFY_YES;
?>
                    </td>
                  </tr>
<?php
      echo osc_draw_hidden_field('customers_add_address');
    } else {
?>
                  <tr>
                    <td class="main"><?php echo '&nbsp;' . ENTRY_CUSTOMERS_ADD_ADDRESS . '&nbsp;'; ?></td>
                    <td class="main"><?php echo osc_draw_checkbox_field('customers_add_address', '1', $cInfo->customers_add_address); ?></td>
                  </tr>
 <?php
    }
?>
                 </table></td>
               </tr>
              </table>
<?php
    $QaddressesBook = $OSCOM_PDO->prepare('select address_book_id,
                                                   entry_firstname as firstname,
                                                   entry_lastname as lastname,
                                                   entry_company as company,
                                                   entry_street_address as street_address,
                                                   entry_suburb as suburb,
                                                   entry_city as city,
                                                   entry_postcode as postcode,
                                                   entry_state as state,
                                                   entry_zone_id as zone_id,
                                                   entry_country_id as country_id ,
                                                   entry_telephone as telephone
                                            from :table_address_book
                                            where customers_id = :customers_id
                                            order by address_book_id
                                            ');
    $QaddressesBook->bindInt(':customers_id', (int)$_GET['cID'] );
    $QaddressesBook->execute();

    $number_address = '1';

    while ($addresses_book = $QaddressesBook->fetch() ) {

      $QcountryAddressesBook = $OSCOM_PDO->prepare('select countries_name
                                                   from :table_countries
                                                   where countries_id = :countries_id
                                                  ');
      $QcountryAddressesBook->bindInt(':countries_id', (int)$addresses_book['country_id'] );
      $QcountryAddressesBook->execute();

      $country_addresses_book = $QcountryAddressesBook->fetch();
?>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
<?php
      if ($addresses_book['address_book_id'] == $cInfo->customers_default_address_id) {
?>
                  <td class="mainTitle"><?php echo ENTRY_ADDRESS_NUMBER . $number_address . '&nbsp;<i>' . ENTRY_ADRESS_DEFAULT . '</i>'; ?></td>
                  <td class="mainTitle" align="right"><table border="0" cellpadding="0" cellspacing="0">
<?php
       if ($error == true) {
?>
                    <tr>
                      <td class="mainTitleTexteSeul"><?php echo '&nbsp;' . ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT . '&nbsp;:&nbsp;'; ?></td>
                      <td class="mainTitleTexteSeul">
<?php
        if ($cInfo->customers_modify_address_default != '1') echo ERROR_ENTRY_CUSTOMERS_MODIFY_NO;
        if ($cInfo->customers_modify_address_default == '1') echo ERROR_ENTRY_CUSTOMERS_MODIFY_YES;
?>
                      </td>
                    </tr>
<?php
        echo osc_draw_hidden_field('customers_modify_address_default');
        } else {
?>
                    <tr>
                      <td class="mainTitleTexteSeul"><?php echo '&nbsp;' . ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT . '&nbsp;'; ?></td>
                      <td class="mainTitleTexteSeul"><?php echo osc_draw_checkbox_field('customers_modify_address_default', '1', $cInfo->customers_modify_address_default); ?></td>
                    </tr>
<?php
        }
?>
                  </table></td>
 <?php
      } else {
?>
                  <td class="mainTitle"><?php echo ENTRY_ADDRESS_NUMBER . $number_address; ?></td>
<?php
      }
?>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
<?php
      if ((strlen($addresses_book['company']) > '0')) {
?>
                    <tr>
                      <td class="main"><?php echo ENTRY_COMPANY; ?></td>
                      <td class="main"><?php echo $addresses_book['company']; ?></td>
                    </tr>
<?php
      }
?>
                    <tr>
                      <td class="main"><?php echo ENTRY_FIRST_NAME; ?></td>
                      <td class="main"><?php echo $addresses_book['firstname']; ?></td>
                      <td class="main" width="200" align="right"><?php echo ENTRY_CUSTOMER_LOCATION; ?></td>
                      <td class="main"><a target="_blank" href="http://maps.google.com/maps?q=<?php echo $addresses_book['street_address'], ',',$addresses_book['suburb'], ',',$addresses_book['postcode'], ',',$addresses_book['city'], ',', $country_addresses_book['countries_name']; ?>&hl=fr&um=1&ie=UTF-8&sa=N&tab=wl"><img src="images/google_map.gif" border="0" alt="<?php echo ENTRY_CUSTOMER_LOCATION; ?>" title="<?php echo ENTRY_CUSTOMER_LOCATION; ?>"></a>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_LAST_NAME; ?></td>
                      <td class="main"><?php echo $addresses_book['lastname']; ?></td>
                    </tr>
                      <tr>
                        <td class="main"><?php echo ENTRY_TELEPHONE; ?></td>
                        <td class="main"><?php echo $addresses_book['telephone']; ?></td>
                      </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_STREET_ADDRESS; ?></td>
                      <td class="main"><?php echo $addresses_book['street_address']; ?></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_SUBURB; ?></td>
                      <td class="main"><?php echo $addresses_book['suburb']; ?></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_POST_CODE; ?></td>
                      <td class="main"><?php echo $addresses_book['postcode']; ?></td>
                    <tr>
                      <td class="main"><?php echo ENTRY_CITY; ?></td>
                      <td class="main"><?php echo $addresses_book['city']; ?></td>
                    <tr>
                      <td class="main"><?php echo ENTRY_STATE; ?></td>
                      <td class="main"><?php echo osc_get_zone_name($addresses_book['country_id'], $addresses_book['zone_id'], $addresses_book['state']); ?></td>
                    <tr>
                      <td class="main"><?php echo ENTRY_COUNTRY; ?></td>
                      <td class="main"><?php echo $country_addresses_book['countries_name']; ?></td>
                   </table></td>
                 </tr>
               </table>
<?php
      $number_address = $number_address + 1;
    }
?>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_CUSTOMERS_IMAGE); ?></td>
                      <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_CUSTOMERS_DEFAULT_ADRESSE; ?></strong></td>
                    </tr>
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                      <td class="main"><?php echo TEXT_AIDE_CUSTOMERS_DEFAULT_ADRESSE; ?></td>
                    </tr>
                  </table></td>
                 </tr>
              </table>
            </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Facturation          //-->
<!-- //################################################################################################################ -->
            <div class="tab-pane" id="tab4">
<?php
// Activation du module B2B
    if (MODE_B2B_B2C == 'true') {
?>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="mainTitle" colspan="2"><?php echo CATEGORY_GROUP_CUSTOMER; ?></td>
 <?php
      if ($cInfo->customers_group_id != 0) {
?>
                  <td class="mainTitle" align="right"><table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                     <td class="mainTitleTexteSeul"><?php echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_group_id . '&action=edit') . '" class="mainTitleTexteSeul">' .  TEXT_EDIT_GROUP_CUSTOMER . '&nbsp;' . $cInfo_group->customers_group_name . '</a>&nbsp;'; ?></td>
                    </tr>
                   </table></td>
<?php
      }
?>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                   <td class="main"><?php echo ENTRY_CUSTOMERS_GROUP_NAME; ?></td>
                   <td class="main">
<?php
      if ($error == true) {
        echo $cInfo_group->customers_group_name;
        echo osc_draw_hidden_field('customers_group_id');
      } else {
        echo osc_draw_pull_down_menu('customers_group_id', osc_get_customers_group(''. VISITOR_NAME .''), $cInfo->customers_group_id);
      }
?>
                   </td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo TEXT_CUSTOMER_DISCOUNT; ?></td>
                    <td class="main"><?php echo osc_draw_input_field('customer_discount', $cInfo->customer_discount, 'maxlength="6"'); ?>%</td>
                  </tr>
                 </table></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               </tr>
              </table>
<?php
    }
    if ($cInfo->customers_group_id != 0) {
?>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="mainTitle" colspan="2"><?php echo CATEGORY_ORDER_TAXE_GROUP . '&nbsp;' . $cInfo_group->customers_group_name; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
<?php
      if ($cInfo_group->group_order_taxe == 0) {
?>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo OPTIONS_ORDER_TAXE; ?></td>
                    </tr>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo OPTIONS_ORDER_NO_TAXE; ?></td>
                    </tr>
<?php
      } else {
?>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo OPTIONS_ORDER_TAXE; ?></td>
                    </tr>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo OPTIONS_ORDER_NO_TAXE; ?></td>
                    </tr>
<?php
      } //end group_order_taxe
?>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
              </table>
<?php
    } // end customers_group_id
?>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
<?php
    if ($cInfo->customers_group_id != 0) {
?>
                  <td class="mainTitle" colspan="2"><?php echo CATEGORY_ORDER_CUSTOMER_GROUP . '&nbsp;' . $cInfo_group->customers_group_name; ?></td>
<?php
    } else {
?>
                   <td class="mainTitle" colspan="2"><?php echo CATEGORY_ORDER_CUSTOMER; ?></td>
<?php
    } // end customers_group_id
?>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
 <?php
// paiement B2B
    $module_active = explode (";",MODULE_PAYMENT_INSTALLED);
    $installed_modules = array();
    for ($i = 0, $n = sizeof($directory_array); $i < $n; $i++) {
      $file = $directory_array[$i];
      if (in_array ($directory_array[$i], $module_active)) {
        include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/payment/' . $file);
        include($module_directory . $file);
        $class = substr($file, 0, strrpos($file, '.'));
        if (osc_class_exists($class)) {
          $module = new $class;
          if ($module->check() > 0) {
            $installed_modules[] = $file;
          }
         } // end osc_class_exists
        if (($cInfo->customers_group_id != 0) && (in_array($module->code, $payments_unallowed))) {
?>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo $module->title; ?></td>
                    </tr>
<?php
        } elseif (($cInfo->customers_group_id != 0) && (!in_array($module->code, $payments_unallowed))) {
?>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo $module->title; ?></td>
                    </tr>
 <?php
        } elseif ($cInfo->customers_group_id == 0) {
?>
                    <tr>
                      <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                      <td class="main"><?php echo $module->title; ?></td>
                    </tr>
<?php
       } // end customers_group_id
     } // end in_array
   } // end for
?>
                   </table></td>
                 </tr>
                </table>
              </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Livraisons          //-->
<!-- //################################################################################################################ -->
             <div class="tab-pane" id="tab5">
               <table width="100%" border="0" cellspacing="0" cellpadding="5">
                 <tr>
<?php
   if ($cInfo->customers_group_id != 0) {
?>
                   <td class="mainTitle" colspan="2"><?php echo CATEGORY_SHIPPING_CUSTOMER_GROUP . '&nbsp;' . $cInfo_group->customers_group_name; ?></td>
<?php
   } else {
?>
                    <td class="mainTitle" colspan="2"><?php echo CATEGORY_SHIPPING_CUSTOMER; ?></td>
<?php
   }
   if ($cInfo->customers_group_id != 0) {
?>
                    <td class="mainTitle" align="right"><table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="mainTitleTexteSeul"><?php echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_group_id . '&action=edit') . '" class="mainTitleTexteSeul">' .  TEXT_EDIT_GROUP_CUSTOMER . '&nbsp;' . $cInfo_group->customers_group_name . '</a>&nbsp;'; ?></td>
                      </tr>
                    </table></td>
<?php
   }
?>
                   </tr>
                 </table>
                 <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                   <tr>
                     <td><table border="0" cellpadding="2" cellspacing="2">
<?php
// Shipping
   $module_shipping_active = explode (";",MODULE_SHIPPING_INSTALLED);
   $installed_shipping_modules = array();
   for ($i = 0, $n = sizeof($directory_shipping_array); $i < $n; $i++) {
     $file_shipping = $directory_shipping_array[$i];
     if (in_array ($directory_shipping_array[$i], $module_shipping_active)) {
       include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/shipping/' . $file_shipping);
       include($module_shipping_directory . $file_shipping);
       $class_shipping = substr($file_shipping, 0, strrpos($file_shipping, '.'));
       if (osc_class_exists($class_shipping)) {
         $module_shipping = new $class_shipping;
         if ($module_shipping->check() > 0) {
           $installed_modules_shipping[] = $file_shipping;
         }
        }
        if (($cInfo->customers_group_id != 0) && (in_array($module_shipping->code, $shipping_unallowed))) {
?>
                      <tr>
                        <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                        <td class="main"><?php echo $module_shipping->title; ?></td>
                      </tr>
<?php
        } elseif (($cInfo->customers_group_id != 0) && (!in_array($module_shipping->code, $shipping_unallowed))) {
?>
                      <tr>
                        <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                        <td class="main"><?php echo $module_shipping->title; ?></td>
                      </tr>
<?php
        } elseif ($cInfo->customers_group_id == 0) {
?>
                      <tr>
                        <td><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16); ?>
                        <td class="main"><?php echo $module_shipping->title; ?></td>
                      </tr>
<?php
       } // end customers_group_id
     } // end in_array
   } // end for
?>
                    </table></td>
                  </tr>
                </table>
              </div>
<!-- //################################################################################################################ -->
<!--          ONGLET customers notes     //-->
<!-- //################################################################################################################ -->
              <div class="tab-pane" id="tab6">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle" colspan="2"><?php echo CUSTOMERS_NOTE; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main">
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>                   
<?php echo osc_draw_textarea_ckeditor('customers_notes', 'soft', '750','200',  ''); ?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <div><?php echo osc_draw_separator('pixel_trans.gif', '2', '10'); ?></div>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle" colspan="2"><?php echo CUSTOMERS_NOTE_SUMMARY; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    $QcustomersNotes = $OSCOM_PDO->prepare('select customers_notes_id,
                                                   customers_id,
                                                   customers_notes,
                                                   customers_notes_date,
                                                   user_administrator
                                            from :table_customers_notes
                                            where customers_id = :customers_id
                                            order by customers_notes_date desc
                                          ');
    $QcustomersNotes->bindInt(':customers_id', (int)$_GET['cID']);
    $QcustomersNotes->execute();

    while ($customers_notes = $QcustomersNotes->fetch() ) {
?>
                      <tr>
                         <td class="main"><?php echo $OSCOM_Date->getShortTime($customers_notes['customers_notes_date']) .' : '. $customers_notes['user_administrator']; ?></td>
                      </tr>
                      <tr>
                        <td class="main"><blockquote><?php echo $customers_notes['customers_notes']; ?></blockquote></td>
                      </tr>
<?php
    } //end while
?>
                  </table></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
     </td>
    </tr>
 </form>

<?php
 } else {
?>    
<!-- //################################################################################################################ -->
<!-- //                                             LISTING DES CLIENTS                                                 -->
<!-- //################################################################################################################ -->
    <tr>
     <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
     <div class="adminTitle">
        <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/client.gif', HEADING_TITLE, '40', '40'); ?></span>
        <span class="col-md-1 pageHeading pull-left"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
        <span class="col-md-5" style="text-align:center;">
          <?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></br />
          <?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], osc_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?>
        </span>
        <span class="col-md-2 pull-left">
           <div class="form-group">
             <div class="controls">
<?php
  echo osc_draw_form('search', 'customers.php', '', 'get');
  echo  osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"') . osc_draw_hidden_field('search_in_description', '1');
?>
               </form>
             </div>
            </div>
         </span>
         <span class="pull-right">
<?php
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
?>
          <?php echo '<a href="' . osc_href_link('customers.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?>
<?php
    }
?>
          </span>
         </form>
<?php echo osc_hide_session_id(); ?>
           <span class="pull-right">
<?php
   if ((MODE_B2B_B2C == 'true')) {
       echo '<a href="' . osc_href_link('create_account.php') . '">' . osc_image_button('button_new_customer.png', IMAGE_CUSTOMER) . '</a>';
   }
?>               
             <form name="delete_all" <?php echo 'action="' . osc_href_link('customers.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
             <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></a>&nbsp;
         </span>
      </div>
    </tr>
    <tr>
     <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
    </tr>
    <tr>
     <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr class="dataTableHeadingRow">
           <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS_ID; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LASTNAME; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIRSTNAME; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ENTRY_COMPANY; ?></td>
<?php
// Permettre le changement de groupe en mode B2B
  if ((MODE_B2B_B2C == 'true')) {
?>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ENTRY_COMPANY_B2B; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_ENTRY_GROUPS_NAME; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_ENTRY_VALIDATE; ?></td>
<?php
  }
?>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ENTRY_EMAIL_VALIDATION; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_COUNTRY; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_NUMBER_OF_REVIEWS; ?></td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACCOUNT_CREATED; ?></td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
          </tr>
<?php

// Recherche
    $search = '';
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "where  (c.customers_id like '" . $keywords . "' or 
                         c.customers_lastname like '%" . $keywords . "%' 
                         or c.customers_firstname like '%" . $keywords . "%' 
                         or c.customers_email_address like '%" . $keywords . "%' 
                         or a.entry_company like '%" . $keywords . "%'
                        ) 
               ";
    }

// affichage des données du tableau
    $customers_query_raw = "select distinct c.customers_id, 
                                            c.customers_company, 
                                            c.customers_lastname, 
                                            c.customers_firstname, 
                                            c.customers_group_id, 
                                            a.entry_company, 
                                            c.customers_email_address, 
                                            a.entry_country_id, 
                                            c.member_level,
                                            c.customers_email_validation
                            from customers c left join address_book a on c.customers_id = a.customers_id
                            and c.customers_default_address_id = a.address_book_id " . $search . " 
                            order by c.customers_id DESC
                          ";
    $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $customers_query_raw, $customers_query_numrows);
    $customers_query = osc_db_query($customers_query_raw);

// Boucle d'affichage des données
    while ($customers = osc_db_fetch_array($customers_query)) {
// suppression du membre non approuvé

     $Qinfo = $OSCOM_PDO->prepare("select customers_info_date_account_created as date_account_created, 
                                           customers_info_date_account_last_modified as date_account_last_modified, 
                                           customers_info_date_of_last_logon as date_last_logon, 
                                           customers_info_number_of_logons as number_of_logons 
                                   from :table_customers_info
                                   where customers_info_id = :customers_id
                                  ");
      $Qinfo->bindInt(':customers_id', (int)$customers['customers_id'] );
      $Qinfo->execute();

      $info = $Qinfo->fetch();

      $QcustColl = $OSCOM_PDO->prepare("select customers_group_id, 
                                               customers_group_name
                                       from :table_customers_groups
                                       where customers_group_id = :customers_group_id
                                      ");
      $QcustColl->bindInt(':customers_group_id', (int)$customers['customers_group_id'] );
      $QcustColl->execute();

      $cust_ret = $QcustColl->fetch();

      if ($cust_ret['customers_group_id'] == 0){
        $cust_ret['customers_group_name'] = ''. VISITOR_NAME .'' ;
      }

      if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $customers['customers_id']))) && !isset($cInfo)) {

        $Qcountry = $OSCOM_PDO->prepare("select countries_name
                                         from countries
                                         where countries_id = :countries_id
                                        ");
        $Qcountry->bindInt(':countries_id', (int)$customers['entry_country_id']);
        $Qcountry->execute();

        $country = $Qcountry->fetch();


        $Qreviews = $OSCOM_PDO->prepare("select count(*) as number_of_reviews
                                         from reviews
                                         where customers_id = :customers_id
                                        ");
        $Qreviews->bindInt(':customers_id', (int)$customers['customers_id']);
        $Qreviews->execute();

        $reviews = $Qreviews->fetch();

// recover from bad records
        if (!is_array($country)) {
            $country = array ('Country is NULL');
        }

        if (!is_array($info)) {
            $info = array ('Info is NULL');
        }

        if (!is_array($reviews)) {
            $reviews = array ('Reviews is NULL');
        }

        $customer_info = array_merge((array)$country, (array)$info, (array)$reviews);

        $cInfo_array = array_merge((array)$customers, (array)$customer_info);

        $Qorders = $OSCOM_PDO->prepare("select count(*) as number_of_orders
                                        from orders
                                        where customers_id = :customers_id
                                        ");
        $Qorders->bindInt(':customers_id', (int)$customers['customers_id']);
        $Qorders->execute();

        $orders = $Qorders->fetch();

        $customer_info = array_merge((array)$country, (array)$info, (array)$reviews, (array)$orders);
        $cInfo_array = array_merge((array)$customers, (array)$customer_info, (array)$cust_ret);

        $cInfo = new objectInfo($cInfo_array);
      }

      if (isset($cInfo) && is_object($cInfo) && ($customers['customers_id']  == $cInfo->customers_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                <td>
<?php 
      if ($customers['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $customers['customers_id'] ; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $customers['customers_id'] ; ?>" />
<?php 
      } 
?>
           </td>
           <td class="dataTableContent"><?php echo $customers['customers_id']; ?></td>
           <td class="dataTableContent"><?php echo $customers['customers_lastname']; ?></td>
           <td class="dataTableContent"><?php echo $customers['customers_firstname']; ?></td>
           <td class="dataTableContent"><?php echo $customers['entry_company']; ?></td>
<?php
// Permettre le changement de groupe en mode B2B
      if ((MODE_B2B_B2C == 'true')) {
?>
           <td class="dataTableContent"><?php echo $customers['customers_company']; ?></td>
           <td class="dataTableContent"><?php echo $cust_ret['customers_group_name']; ?></td>
           <td class="dataTableContent" align="center">
<?php
        if ($customers['member_level'] == '0') {
          echo '<a href="' . osc_href_link('members.php') . '"> '. osc_image(DIR_WS_IMAGES . 'icons/locked.gif', APPROVED_CLIENT). '</a>';
        }
?>
           </td>
<?php
      }

      if  ($customers['customers_email_validation'] == '0') {
        $email_validation = osc_image(DIR_WS_IMAGES . 'icons/tick.gif', IMAGE_EMAIL_APPROVED);
      } else {
        $email_validation = osc_image(DIR_WS_IMAGES . 'icons/cross.gif', IMAGE_EMAIL_NOT_APPROVED);
      }
?>
           <td class="dataTableContent" align="center"><?php echo $email_validation; ?></td>
<?php
     $QcustomersCountry = $OSCOM_PDO->prepare("select a.entry_country_id, 
                                                      c.countries_name
                                               from :table_address_book a,
                                                    :table_countries c
                                               where customers_id = :customers_id
                                               and a.entry_country_id = :entry_country_id
                                              ");
      $QcustomersCountry->bindInt(':customers_id', (int)$customers['customers_id']  );
      $QcustomersCountry->bindInt(':entry_country_id', countries_id );
      $QcustomersCountry->execute();

      $customers_country = $QcustomersCountry->fetch();
?>
           <td class="dataTableContent" align="left"><?php echo $customers_country['countries_name']; ?></td>
<?php
      $Qreviews = $OSCOM_PDO->prepare('select count(*) as number_of_reviews
                                             from :table_reviews
                                             where customers_id = :customers_id
                                            ');
      $Qreviews->bindInt(':customers_id',  (int)$customers['customers_id'] );
      $Qreviews->execute();

      $reviews = $Qreviews->fetch();
?>
           <td class="dataTableContent" align="center"><?php echo $reviews['number_of_reviews']; ?></td>
           <td class="dataTableContent" align="right"><?php echo $OSCOM_Date->getShort($info['date_account_created']); ?></td>
           <td class="dataTableContent" align="right">
<?php
// Edition directe du groupe client
            if ($customers['customers_group_id'] > '0') {
               echo '<a href="' . osc_href_link(('customers_groups.php') . '?cID=' . $cust_ret['customers_group_id']) . '&action=edit">' . osc_image(DIR_WS_ICONS . 'group_client.gif', ICON_EDIT_CUSTOMERS_GROUP, 16,16) . '</a>';
               echo osc_draw_separator('pixel_trans.gif', '6', '16');
            }
            echo '<a href="' . osc_href_link('customers.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers['customers_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_CUSTOMER) . '</a>';
            echo osc_draw_separator('pixel_trans.gif', '6', '16');
            echo '<a href="' . osc_href_link('mail.php', 'customer=' . $customers['customers_email_address']) . '">' . osc_image(DIR_WS_ICONS . 'email.gif', IMAGE_EMAIL) . '</a>';
            echo osc_draw_separator('pixel_trans.gif', '6', '16');
            echo '<a href="' . osc_href_link('orders.php', 'cID=' . $customers['customers_id']) . '">' . osc_image(DIR_WS_ICONS . 'order.gif', ICON_EDIT_ORDERS) . '</a>';
            echo osc_draw_separator('pixel_trans.gif', '6', '16');
            echo '<a href="' . osc_href_link('password_forgotten.php', 'cID=' . $customers['customers_id']) . '">' . osc_image(DIR_WS_ICONS . 'new_password.gif', ICON_EDIT_NEW_PASSWORD) . '</a>';
            echo osc_draw_separator('pixel_trans.gif', '6', '16');
            if ( (is_object($cInfo)) && ($customers['customers_id'] == $cInfo->customers_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('customers.php', osc_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
           </td>
          </tr>
<?php
    } // end while
?>
              </form><!-- end form delete all -->
              <tr>
                <td colspan="12" class="smallText" valign="top"><?php echo  $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
              </tr>
            </table></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

