<?php
/**
 * mysql_utf8gr.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_MYSQL_UTF8_ERROR', 'Some database tables need to be converted to UTF-8 (utf8_unicode_ci). Please review the tables under Tools -&gt; Database Tables.');
?>