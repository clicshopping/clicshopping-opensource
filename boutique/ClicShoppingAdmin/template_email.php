<?php
/**
 * template_email.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/functions/template_email.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'update':

        if (isset($_GET['tID'])) $template_email_id = osc_db_prepare_input($_GET['tID']);

        $languages = osc_get_languages();

        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $template_language_id = $languages[$i]['id'];

          $sql_data_array['template_email_name'] = osc_db_prepare_input($_POST['template_email_name'][$template_language_id]);
          $sql_data_array['template_email_short_description'] = osc_db_prepare_input($_POST['template_email_short_description'][$template_language_id]);
          $sql_data_array['template_email_description'] = osc_db_prepare_input($_POST['template_email_description'][$template_language_id]);
           
          $sql_data_array = array_merge($sql_data_array);  
          osc_db_perform('template_email_description', $sql_data_array, 'update', "template_email_id = '" . (int)$template_email_id ."' and  language_id = '" . (int)$template_language_id ."'");
      }

       osc_redirect(osc_href_link('template_email.php', 'page=' . $_GET['page']));
     break;
    } 
  }

  require('includes/header.php');
?>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>
  <div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <div class="adminTitle">
      <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/mail.gif', HEADING_TITLE, '40', '40'); ?></span>
      <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
      if (empty($action)) {
?>
      <span></span>
<?php
  } else if (($action == 'edit') && isset($_GET['tID']) ) {
  $form_action = 'update';
?>
      <form name="template_emails" <?php echo 'action="' . osc_href_link('template_email.php', osc_get_all_get_params(array('action', 'info', 'tID')) . 'action=' . $form_action . '&tID=' . $_GET['tID']) . '"'; ?> method="post">
        <?php if ($form_action == 'update') echo osc_draw_hidden_field('template_email', $_GET['tID']); ?>
        <span></span>
        <span class="pull-right">&nbsp;<?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></span>
        <span class="pull-right"><?php echo '<a href="' . osc_href_link('template_email.php', 'page=' . $_GET['page'] . '&tID=' . $_GET['tID']).'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
<?php
  }
?>
    </div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  </div>
  <div class="clearfix"></div>
<!-- //################################################################################################################ -->
<!-- //                                      EDITION ET MAJ D'UN TEMPLATE                                       -->
<!-- //################################################################################################################ -->
<?php	 

  if (osc_not_null($action))  {

    if ( ($action == 'edit') && isset($_GET['tID']) ) {
     $form_action = 'update';

      $QtemplateEmailDescription = $OSCOM_PDO->prepare("select ted.template_email_description_id, 
                                                              ted.language_id,
                                                              ted.template_email_name,
                                                              ted.template_email_short_description,
                                                              ted.template_email_description,
                                                              te.template_email_variable,
                                                              te.template_email_id,
                                                              te.customers_group_id
                                                       from :table_template_email te,
                                                            :table_template_email_description ted
                                                       where te.template_email_id = :template_email_id
                                                       and te.template_email_id = ted.template_email_id
                                                ");
      $QtemplateEmailDescription->bindInt(':template_email_id',(int)$_GET['tID']);
      $QtemplateEmailDescription->execute();

      $template_email_description = $QtemplateEmailDescription->fetch();

      $tInfo = new objectInfo($template_email_description);

   } else {
      $tInfo = new objectInfo(array());
      $template_email_name = $_POST['template_email_name'];
      $template_email_short_description = $_POST['template_email_short_description'];
      $template_email_description = $_POST['template_email_description'];
      $template_email_id = $_POST['template_email_id'];
   }  
?>
    <?php echo osc_draw_separator('pixel_trans.gif', '2', '10'); ?>
    <div>
       <ul class="nav nav-tabs" role="tablist"  id="myTab">
         <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
         <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_DESCRIPTION; ?></a></li>
       </ul>
       <div class="tabsClicShopping">
         <div class="tab-content">
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General                          //-->
<!-- ------------------------------------------------------------ //-->
           <div class="tab-pane active" id="tab1">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="mainTitle"><?php echo TITLE_INFORMATION_NAME; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
       
                <tr>
                  <td class="main" width="250"><?php echo TEMPLATE_EMAIL_TEXT_NAME; ?></td>
                  <td align="left">
<?php
      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                    <tr>
                      <td></td>
                      <td class="main"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp; <?php echo  osc_draw_input_field('template_email_name[' . $languages[$i]['id'] . ']', (isset($template_email_name[$languages[$i]['id']]) ? $template_email_name[$languages[$i]['id']] : osc_get_template_email_name($tInfo->template_email_id, $languages[$i]['id'])), 'maxlength="250", size="50"',  true) . '&nbsp;'; ?></td>
                    </tr>
<?php
      }
?>
                  </td> 
                </tr>
                <tr>
                  <td class="main"><?php echo TEMPLATE_EMAIL_TEXT_SHORT_DESCRIPTION; ?></td>
                  <td>
<?php
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                    <tr>
                      <td></td>
                      <td class="main"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp; <?php echo  osc_draw_input_field('template_email_short_description[' . $languages[$i]['id'] . ']', (isset($template_email_short_description[$languages[$i]['id']]) ? $template_email_short_description[$languages[$i]['id']] : osc_get_template_email_short_description($tInfo->template_email_id, $languages[$i]['id'])), 'maxlength="250", size="50"',  true) . '&nbsp;'; ?></td>
                    </tr>
<?php
      }
?>
                  </td>
                </tr>
<?php
/*
                <tr>
                  <td width="10%" class="main"><?php echo TEMPLATE_EMAIL_TEXT_VARIABLE; ?></td>
                  <td><?php echo osc_draw_input_field('template_email_variable',  $tInfo->template_email_variable); ?></td>
                </tr>
                <tr>
                  <td class="main"><?php echo TEMPLATE_EMAIL_TEXT_CUSTOMER_GROUP; ?></td>
                  <td><?php echo osc_draw_input_field('customers_group_id', $tInfo->customers_group_id); ?></td>
                </tr> 
*/
?>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
              </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', AIDE_TITLE_ONGLET_GENERAL); ?></td>
                    <td class="main"><strong><?php echo '&nbsp;' . AIDE_TITLE_ONGLET_GENERAL; ?></strong></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                      <td class="main"><?php echo TEXT_AIDE_TEMPLATE; ?></td>
                  </tr>
                </table></td>
              </tr>
              </table>
            </div>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information Description                      //-->
<!-- ------------------------------------------------------------ //-->
            <div class="tab-pane" id="tab2">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="mainTitle"><?php echo TITLE_MESSAGE; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td>
<?php
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                   <tr>
                     <td valign="top"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                     <td class="main"><div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('template_email_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300', (isset($template_email_description[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($template_email_description[$languages[$i]['id']])) : osc_get_template_email_description($tInfo->template_email_id, $languages[$i]['id'])));?> </div></td>
                   </tr>
<?php
    }
?>
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', AIDE_TITLE_ONGLET_GENERAL); ?></td>
                      <td class="main"><strong><?php echo '&nbsp;' . AIDE_TITLE_ONGLET_GENERAL; ?></strong></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                      <td class="main"><?php echo TEXT_AIDE_TEMPLATE; ?></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
 </form>
<?php
  } else {
?>
<tr>
  <td><table border="0" width="100%" cellspacing="5" cellpadding="0">
      <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
            <tr class="dataTableHeadingRow">
              <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TEMPLATE_EMAIL_NAME; ?></td>
              <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TEMPLATE_EMAIL_TYPE; ?></td>
              <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TEMPLATE_EMAIL_DESCRIPTION; ?></td>
              <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_TEMPLATE_CUSTOMER_GROUPS; ?></td>
              <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
            </tr>
            <!-- partie lecture du tableau -->
<?php
    $QtemplateEmail = $OSCOM_PDO->prepare('select ted.template_email_description_id,
                                                  ted.language_id,
                                                  ted.template_email_name,
                                                  ted.template_email_short_description,
                                                  te.template_email_variable,
                                                  te.customers_group_id,
                                                  te.template_email_type,
                                                  te.template_email_id
                                           from  :table_template_email te,
                                                 :table_template_email_description ted
                                           where  ted.language_id = :language_id
                                           and te.template_email_id = ted.template_email_id
                                     ');

    $QtemplateEmail->bindInt(':language_id', (int)$_SESSION['languages_id'] );
    $QtemplateEmail->execute();

    while ($template =  $QtemplateEmail->fetch() ) {

          if ($template['template_email_type'] == '1') {
              $template_email_type =  TEXT_TEMPLATE_EMAIL_CATALOG;

            } elseif ($template['template_email_type'] == '0') {
              $template_email_type = TEXT_TEMPLATE_EMAIL_ADMIN;
            } else {
              $template_email_type = TEXT_TEMPLATE_EMAIL_ADMIN_CATALOG;
           }

            if ( $template['customers_group_id'] == '0') {
              $template_email_customer_group =  TEXT_TEMPLATE_EMAIL_B2C;
            } elseif ($template['template_email_type'] == '1') {
              $template_email_customer_group = TEXT_TEMPLATE_EMAIL_B2C_B2B;
           }

?>
            <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href='<?php echo osc_href_link('template_email.php', 'page=' . $_GET['page'] . '&tID=' . $template['template_email_id'] . '&action=edit'); ?>'">
              <td class="dataTableContent"><?php echo $template['template_email_name']; ?></td>
              <td class="dataTableContent"><?php echo $template_email_type; ?></td>
              <td class="dataTableContent"><?php echo $template['template_email_short_description']; ?></td>
              <td class="dataTableContent" align="center"><?php echo $template_email_customer_group; ?></td>
              <td class="dataTableContent" align="right">
<?php
  echo '<a href="' . osc_href_link('template_email.php', 'page=' . $_GET['page'] . '&tID=' . $template['template_email_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>';
  echo osc_draw_separator('pixel_trans.gif', '6', '16');
  if ( (isset($tInfo) && is_object($tInfo)) && ($actions['id'] == $tInfo->id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('template_email.php', 'tID=' . $template_email_description_id['id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
              &nbsp; </td>
            </tr>
<?php
    }
  }
?>
         </table></td>
      </tr>
    </table></td>
  </tr>
</table></td>
</tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
