<?php
/*
 * blog.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  require(DIR_WS_FUNCTIONS .'blog.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('blog'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('blog.php'));

  $blog_array = array();

  if (isset($_GET['blog_keywords'])) {
    $blog_keywords = osc_db_prepare_input($_GET['blog_keywords']);
  }

  if ($OSCOM_Customer->getCustomersGroupID() != 0) {

    if ($_GET['current'] != 0 || $_GET['current'] != '') {

      $blog_query_raw = " select bc.*,
                                 bcd.*
                          from blog_content bc,
                               blog_content_description bcd,
                               blog_content_to_categories p2c
                          where bc.blog_content_status = '1' 
                          and bcd.blog_content_id = bc.blog_content_id 
                          and bcd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                          and bc.blog_content_archive = '0'
                          and bc.customers_group_id = '0'
                          and p2c.blog_categories_id = '".(int)$_GET['current']."'
                          and p2c.blog_content_id = bc.blog_content_id
                          and bc.customers_group_id = '". (int)$OSCOM_Customer->getCustomersGroupID() ."'
                          order by  bc.blog_content_sort_order,
                                    bc.blog_content_date_added DESC
                       ";

    } elseif (!empty($_GET['bPath'])) {

      $blog_query_raw = " select bc.*,
                                 bcd.*
                          from blog_content bc,
                               blog_content_description bcd,
                               blog_content_to_categories p2c
                          where bc.blog_content_status = '1' 
                          and bcd.blog_content_id = bc.blog_content_id 
                          and bcd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                          and bc.blog_content_archive = '0'
                          and bc.customers_group_id = '0'
                          and p2c.blog_categories_id = '".(int)$_GET['bPath']."'
                          and p2c.blog_content_id = bc.blog_content_id
                          and bc.customers_group_id = '". (int)$OSCOM_Customer->getCustomersGroupID() ."'
                          order by  bc.blog_content_sort_order,
                                    bc.blog_content_date_added DESC
                       ";
    } else {

      $blog_query_raw = " select  bc.*,
                                  bcd.*
                          from blog_content bc,
                               blog_content_description bcd
                          where bc.blog_content_status = '1' 
                          and bcd.blog_content_id = bc.blog_content_id 
                          and bcd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                          and bc.blog_content_archive = '0'
                          and bc.customers_group_id = '". (int)$OSCOM_Customer->getCustomersGroupID() ."'
                          and (bc.blog_content_author  like '%" . osc_db_input($blog_keywords) . "%' or  
                               bcd.blog_content_name like '%" . osc_db_input($blog_keywords) . "%' or 
                               bcd.blog_content_head_tag_blog like '%" . osc_db_input($blog_keywords) . "% or
                             ')
                          order by  bc.blog_content_sort_order,
                                    bc.blog_content_date_added DESC
                     ";
    }

  } else {
// Mode normal

    if ($_GET['current'] != 0 || $_GET['current'] != '') {

      $blog_query_raw = " select bc.*,
                                 bcd.*
                          from blog_content bc,
                               blog_content_description bcd,
                               blog_content_to_categories p2c
                          where bc.blog_content_status = '1' 
                          and bcd.blog_content_id = bc.blog_content_id 
                          and bcd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                          and bc.blog_content_archive = '0'
                          and bc.customers_group_id = '0'
                          and p2c.blog_categories_id = '".(int)$_GET['current']."'
                          and p2c.blog_content_id = bc.blog_content_id
                          order by  bc.blog_content_sort_order,
                                    bc.blog_content_date_added DESC
                       ";

    } elseif (!empty($_GET['bPath'])) {

      $blog_query_raw = " select bc.*,
                                 bcd.*
                          from blog_content bc,
                               blog_content_description bcd,
                               blog_content_to_categories p2c
                          where bc.blog_content_status = '1' 
                          and bcd.blog_content_id = bc.blog_content_id 
                          and bcd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                          and bc.blog_content_archive = '0'
                          and bc.customers_group_id = '0'
                          and p2c.blog_categories_id = '".(int)$_GET['bPath']."'
                          and p2c.blog_content_id = bc.blog_content_id
                          order by  bc.blog_content_sort_order,
                                    bc.blog_content_date_added DESC
                       ";
    } else {

      $blog_query_raw = " select  bc.*,
                                  bcd.*
                          from blog_content bc,
                               blog_content_description bcd
                          where bc.blog_content_status = '1' 
                          and bcd.blog_content_id = bc.blog_content_id 
                          and bcd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                          and bc.blog_content_archive = '0'
                          and bc.customers_group_id = '0'
                          and (bc.blog_content_author  like '%" . osc_db_input($blog_keywords) . "%' or  
                               bcd.blog_content_name like '%" . osc_db_input($blog_keywords) . "%' or 
                               bcd.blog_content_head_tag_blog like '%" . osc_db_input($blog_keywords) . "%')
                          order by  bc.blog_content_sort_order,
                                    bc.blog_content_date_added DESC
                       ";
    }
  }
  
  require($OSCOM_Template->getTemplateFiles('blog'));
