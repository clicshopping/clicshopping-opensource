<?php
/**
 * session_auto_start.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_SESSION_AUTO_START', 'session.auto_start est activé - veuillez le désactivé dans votre php.ini et redémarrer votre serveur web.');
?>
