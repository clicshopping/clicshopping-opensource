<?php
/*
 * pop_up_manager_account_history.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  if (!isset($_GET['order_id']) || (isset($_GET['order_id']) && !is_numeric($_GET['order_id']))) {
    osc_redirect(osc_href_link('account_history.php', '', 'SSL'));
  }

  $QcustomerInfo = $OSCOM_PDO->prepare('select o.customers_id
                                        from :table_orders o, 
                                             :table_orders_status s
                                        where  o.orders_id = :orders_id
                                        and o.orders_status = s.orders_status_id  
                                        and s.language_id = :language_id
                                        and s.public_flag = :public_flag
                                      ');

  $QcustomerInfo->bindInt(':orders_id', (int)$_GET['order_id']);
  $QcustomerInfo->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $QcustomerInfo->bindValue(':public_flag', '1');

  $QcustomerInfo->execute();

  $customer_info = $QcustomerInfo->fetch();

 // select the conditions of sales

  $QconditionGeneralOfSales = $OSCOM_PDO->prepare('select page_manager_general_condition
                                                   from  :table_orders_pages_manager
                                                   where orders_id = :orders_id
                                                   and customers_id = :customers_id
                                                ');

  $QconditionGeneralOfSales->bindInt(':orders_id', (int)$_GET['order_id']);
  $QconditionGeneralOfSales->bindInt(':customers_id', (int)$customer_info['customers_id']);

  $QconditionGeneralOfSales->execute();

  $condition_general_of_sales = $QconditionGeneralOfSales->fetch();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<title><?php echo 'Conditions of sales'; ?></title>
</head>
<body onload="resize();">
  <?php echo $condition_general_of_sales['page_manager_general_condition']; ?>
</body>
</html>
<?php 
require('includes/application_bottom.php');
