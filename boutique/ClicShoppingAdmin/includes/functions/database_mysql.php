<?php
/*
 * database_mysql.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  function osc_db_connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $link = 'db_link') {
    global $$link;

    if (USE_PCONNECT == 'true') {
      $server = 'p:' . $server;
    }

    $$link = mysqli_connect($server, $username, $password, $database);

    return $$link;
  }

  function osc_db_close($link = 'db_link') {
    global $$link;

    return mysqli_close($$link);
  }

  function osc_db_error($query, $errno, $error) { 
    die('<p style="color:#000000;"><strong>' . $errno . ' - ' . $error . '<br /><br />' . $query . '</strong><p><p style="color:#ff0000;">[TEP STOP]</p>');
  }

  function osc_db_query($query, $link = 'db_link') {
    global $$link, $logger;

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
      if (!is_object($logger)) $logger = new logger;
      $logger->write($query, 'QUERY');
    }

    $result = mysqli_query($$link, $query) or osc_db_error($query, mysqli_errno($$link), mysqli_error($$link));

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
      if (mysqli_error($$link)) $logger->write(mysqli_error($llink), 'ERROR');
    }

    return $result;
  }

  function osc_db_perform($table, $data, $action = 'insert', $parameters = '', $link = 'db_link') {
    reset($data);
    if ($action == 'insert') {
      $query = 'insert into ' . $table . ' (';
      while (list($columns, ) = each($data)) {
        $query .= $columns . ', ';
      }
      $query = substr($query, 0, -2) . ') values (';
      reset($data);
      while (list(, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= 'now(), ';
            break;
          case 'null':
            $query .= 'null, ';
            break;
          default:
            $query .= '\'' . osc_db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ')';
    } elseif ($action == 'update') {
      $query = 'update ' . $table . ' set ';
      while (list($columns, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= $columns . ' = now(), ';
            break;
          case 'null':
            $query .= $columns .= ' = null, ';
            break;
          default:
            $query .= $columns . ' = \'' . osc_db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ' where ' . $parameters;
    }

    return osc_db_query($query, $link);
  }

  function osc_db_fetch_array($db_query) {
    return mysqli_fetch_array($db_query, MYSQLI_ASSOC);
  }

  function osc_db_result($result, $row, $field = '') {
    if ( $field === '' ) {
      $field = 0;
    }

    osc_db_data_seek($result, $row);
    $data = osc_db_fetch_array($result);

    return $data[$field];
  }

  function osc_db_num_rows($db_query) {
    return mysqli_num_rows($db_query);
  }

  function osc_db_data_seek($db_query, $row_number) {
    return mysqli_data_seek($db_query, $row_number);
  }

  function osc_db_insert_id($link = 'db_link') {
    global $$link;

    return mysqli_insert_id($$link);
  }

  function osc_db_free_result($db_query) {
    return mysqli_free_result($db_query);
  }

  function osc_db_fetch_fields($db_query) {
    return mysqli_fetch_field($db_query);
  }

  function osc_db_output($string) {
    return htmlspecialchars($string);
  }

  function osc_db_input($string, $link = 'db_link') {
    global $$link;

    return mysqli_real_escape_string($$link, $string);
  }

  function osc_db_prepare_input($string) {
    if (is_string($string)) {
//      return trim(stripslashes($string));
       return trim($string);
    } elseif (is_array($string)) {
      reset($string);
      while (list($key, $value) = each($string)) {
        $string[$key] = osc_db_prepare_input($value);
      }
      return $string;
    } else {
      return $string;
    }
  }

  function osc_db_affected_rows($link = 'db_link') {
    global $$link;

    return mysqli_affected_rows($$link);
  }

  function osc_db_get_server_info($link = 'db_link') {
    global $$link;

    return mysqli_get_server_info($$link);
  }

  if ( !function_exists('mysqli_connect') ) {
    define('MYSQLI_ASSOC', MYSQL_ASSOC);

    function mysqli_connect($server, $username, $password, $database) {
      if ( substr($server, 0, 2) == 'p:' ) {
        $link = mysql_pconnect(substr($server, 2), $username, $password);
      } else {
        $link = mysql_connect($server, $username, $password);
      }

      if ( $link ) {
        mysql_select_db($database, $link);
      }

      return $link;
    }

    function mysqli_close($link) {
      return mysql_close($link);
    }

    function mysqli_query($link, $query) {
      return mysql_query($query, $link);
    }

    function mysqli_errno($link = null) {
      return mysql_errno($link);
    }

    function mysqli_error($link = null) {
      return mysql_error($link);
    }

    function mysqli_fetch_array($query, $type) {
      return mysql_fetch_array($query, $type);
    }

    function mysqli_num_rows($query) {
      return mysql_num_rows($query);
    }

    function mysqli_data_seek($query, $offset) {
      return mysql_data_seek($query, $offset);
    }

    function mysqli_insert_id($link) {
      return mysql_insert_id($link);
    }

    function mysqli_free_result($query) {
      return mysql_free_result($query);
    }

    function mysqli_fetch_field($query) {
      return mysql_fetch_field($query);
    }

    function mysqli_real_escape_string($link, $string) {
      if ( function_exists('mysql_real_escape_string') ) {
        return mysql_real_escape_string($string, $link);
      } elseif ( function_exists('mysql_escape_string') ) {
        return mysql_escape_string($string);
      }

      return addslashes($string);
    }

    function mysqli_affected_rows($link) {
      return mysql_affected_rows($link);
    }

    function mysqli_get_server_info($link) {
      return mysql_get_server_info($link);
    }
  }

/**
 * Calculate the size of databse
 *
 * @author      loic richard - e-imaginis
 * @version     1.0.0
 * @param       string   $size_bd   
 * @return      $size_db
 * @access public
 */
  function osc_size_bd($size_bd) {
    $result = osc_db_query('show table status from '. DB_DATABASE .' ');
    $size = 0;
    $out = '';
    while($row = mysqli_fetch_array($result)) {
      $size += $row['Data_length'];
      $out .= $row['Name'] .': '. round(($row['Data_length']/1024)/1024, 4) .'<br />\n';
    }
    $size_db = round(($size/1024)/1024, 1);
    return $size_db;
  }

?>