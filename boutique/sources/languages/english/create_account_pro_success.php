<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Create an Account');
define('NAVBAR_TITLE_2', 'Success');

// Affichage texte d'ouverture d'une session si l'approbation n'est pas obligatoire
if (MEMBER == 'false') {
  define('HEADING_TITLE', 'Your Account Has Been Created!');
  define('TEXT_ACCOUNT_CREATED', 'Congratulations! Your new account has been successfully created! You can now take advantage of member priviledges to enhance your online shopping experience with us. If you have <small><strong>ANY</strong></small> questions about the operation of this online shop, please email the <a href="' . osc_href_link('contact_us.php') . '">store owner</a>.<br /><br />A confirmation has been sent to the provided email address. If you have not received it within the hour, please <a href="' . osc_href_link('contact_us.php') . '">contact us</a>.');
    define('MIN_QTY_ORDER_PRODUCT','Qty; min : ');
} else {
  define('HEADING_TITLE', 'Ask for validation of an office account!');
  define('TEXT_ACCOUNT_CREATED', 'We took your request.<br /><br />You will received an email with your access codes allowing you to have an access on our website after a validation and checking of our sales department.<br /><br />Do not hesitate to contact our support on line for all request for additional information.<br /><br />Regards,<br />The ' . STORE_NAME . ' team');
}
?>