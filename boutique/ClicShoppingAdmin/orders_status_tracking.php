<?php
/**
 * orders_status_tracking.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':
      case 'save':
        if (isset($_GET['oID'])) $orders_status_tracking_id = osc_db_prepare_input($_GET['oID']);

        $languages = osc_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $orders_status_tracking_name_array = $_POST['orders_status_tracking_name'];
          $orders_status_tracking_link_array = $_POST['orders_status_tracking_link'];
          $language_id = $languages[$i]['id'];

          $sql_data_array = array('orders_status_tracking_name' => osc_db_prepare_input($orders_status_tracking_name_array[$language_id]),
                                  'orders_status_tracking_link' => osc_db_prepare_input($orders_status_tracking_link_array[$language_id])
                                 );

          if ($action == 'insert') {
            if (empty($orders_status_tracking_id)) {
              $QnextId = $OSCOM_PDO->prepare('select max(orders_status_tracking_id) as orders_status_tracking_id 
                                              from :table_orders_status_tracking
                                            ');
              $QnextId->execute();

              $next_id = $QnextId->fetch();

              $orders_status_tracking_id = $next_id['orders_status_tracking_id'] + 1;
            }

            $insert_sql_data = array('orders_status_tracking_id' => $orders_status_tracking_id,
                                     'language_id' => $language_id);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            $OSCOM_PDO->save('orders_status_tracking', $sql_data_array);

          } elseif ($action == 'save') {

            $OSCOM_PDO->save('orders_status_tracking', $sql_data_array, ['orders_status_tracking_id' => (int)$orders_status_tracking_id,
                                                                          'language_id' => (int)$language_id
                                                                        ]
                            );

          }
        }

        if (isset($_POST['default']) && ($_POST['default'] == 'on')) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', $orders_status_tracking_id);
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_ORDERS_STATUS_TRACKING_ID');
          $Qupdate->execute();
        }

        osc_redirect(osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status_tracking_id));
        break;
      case 'deleteconfirm':
        $oID = osc_db_prepare_input($_GET['oID']);
/*
        $orders_status_tracking_query = osc_db_query("select configuration_value 
                                                      from configuration
                                                      where configuration_key = 'DEFAULT_ORDERS_STATUS_TRACKING_ID'
                                                    ");
        $orders_status_tracking = osc_db_fetch_array($orders_status_tracking_query);
*/

        $QordersStatusTracking = $OSCOM_PDO->prepare('select configuration_value 
                                                     from :table_configuration
                                                     where configuration_key = :configuration_key
                                                    ');
        $QordersStatusTracking->bindValue(':configuration_key', 'DEFAULT_ORDERS_STATUS_TRACKING_ID');
        $QordersStatusTracking->execute();

        $orders_status_tracking = $QordersStatusTracking->fetch();


        if ($orders_status_tracking['configuration_value'] == $oID) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', '' );
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_ORDERS_STATUS_TRACKING_ID');
          $Qupdate->execute();
        }

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_orders_status_tracking
                                        where orders_status_tracking_id = :orders_status_tracking_id 
                                      ');
        $Qdelete->bindInt(':orders_status_tracking_id',  (int)osc_db_input($oID) );
        $Qdelete->execute();

        osc_redirect(osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page']));
        break;
        
      case 'delete':
        $oID = osc_db_prepare_input($_GET['oID']);
        $remove_status = true;

        if ($oID == DEFAULT_ORDERS_STATUS_TRACKING_ID) {
          $remove_status = false;
          $OSCOM_MessageStack->add(ERROR_REMOVE_DEFAULT_ORDER_STATUS, 'error');
        } else {
          $remove_status = true;
        }

        break;
    }
  }

  require('includes/header.php');
?>
  <!-- body_text //-->
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/tracking.png', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
    <span class="pull-right"><?php echo '<a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_order_status.gif', IMAGE_INSERT) . '</a>'; ?></span>
<?php
}
?>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>

    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDERS_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $orders_status_tracking_query_raw = "select *
                                      from orders_status_tracking
                                      where language_id = '" . (int)$_SESSION['languages_id'] . "'
                                      order by orders_status_tracking_id
                                     ";

  $orders_status_tracking_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $orders_status_tracking_query_raw, $orders_status_tracking_query_numrows);
  $orders_status_tracking_query = osc_db_query($orders_status_tracking_query_raw);

  while ($orders_status_tracking = osc_db_fetch_array($orders_status_tracking_query)) {

    if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders_status_tracking['orders_status_tracking_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
      $oInfo = new objectInfo($orders_status_tracking);
    }

    if (isset($oInfo) && is_object($oInfo) && ($orders_status_tracking['orders_status_tracking_id'] == $oInfo->orders_status_tracking_id)) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_tracking_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status_tracking['orders_status_tracking_id']) . '\'">' . "\n";
    }

    if (DEFAULT_ORDERS_STATUS_TRACKING_ID == $orders_status_tracking['orders_status_tracking_id']) {
      echo '                <td class="dataTableContent"><strong>' . $orders_status_tracking['orders_status_tracking_name'] . ' (' . TEXT_DEFAULT . ')</strong></td>' . "\n";
    } else {
      echo '                <td class="dataTableContent">' . $orders_status_tracking['orders_status_tracking_name'] . '</td>' . "\n";
    }
?>
                <td class="dataTableContent" align="right">
<?php
        if ($orders_status_tracking['orders_status_tracking_id'] > 1) {
                  echo '<a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status_tracking['orders_status_tracking_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
        }
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status_tracking['orders_status_tracking_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($oInfo) && is_object($oInfo) && ($orders_status_tracking['orders_status_tracking_id'] == $oInfo->orders_status_tracking_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status_tracking['orders_status_tracking_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $orders_status_tracking_split->display_count($orders_status_tracking_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS); ?></td>
                    <td class="smallText" align="right"><?php echo $orders_status_tracking_split->display_links($orders_status_tracking_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_ORDERS_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status_tracking', 'orders_status_tracking.php', 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);

      $orders_status_tracking_inputs_string = '';
      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_tracking_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('orders_status_tracking_name[' . $languages[$i]['id'] . ']');
      }

     for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_tracking_link_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('orders_status_tracking_link[' . $languages[$i]['id']  . ']');
      }

      $contents[] = array('text' => '<br />' . TEXT_INFO_ORDERS_STATUS_NAME . $orders_status_tracking_inputs_string);
      $contents[] = array('text' => '<br />' . TEXT_INFO_ORDERS_STATUS_LINK .  $orders_status_tracking_link_inputs_string);

      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_ORDERS_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status_tracking', 'orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_tracking_id  . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);

      $orders_status_tracking_inputs_string = '';
      $languages = osc_get_languages();

     for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_tracking_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('orders_status_tracking_name[' . $languages[$i]['id'] . ']', osc_get_orders_status_tracking_name($oInfo->orders_status_tracking_id, $languages[$i]['id']));
      }


     for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_tracking_link_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('orders_status_tracking_link[' . $languages[$i]['id'] . ']', osc_get_orders_tracking_link($oInfo->orders_status_tracking_id, $languages[$i]['id']));
      }

      $contents[] = array('text' => '<br />' . TEXT_INFO_ORDERS_STATUS_NAME .  $orders_status_tracking_inputs_string);
      $contents[] = array('text' => '<br />' . TEXT_INFO_ORDERS_STATUS_LINK .  $orders_status_tracking_link_inputs_string);

      if (DEFAULT_ORDERS_STATUS_TRACKING_ID != $oInfo->orders_status_tracking_id) $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_tracking_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_ORDERS_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status_tracking', 'orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_tracking_id  . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $oInfo->orders_status_tracking_name . '</strong>');

      $contents[] = array('text' => '<br /><strong>' . $oInfo->orders_status_tracking_link . '</strong>');

      if ($remove_status) {
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_tracking_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      } else {
       $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('orders_status_tracking.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_tracking_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      }
    break;
    default:
    break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>