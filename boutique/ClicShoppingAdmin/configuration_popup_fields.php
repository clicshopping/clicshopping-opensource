<?php
/**
 * configuration_popup_fields.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */

  require('includes/application_top.php');

  $cKey = $_GET['cKey'];

  $Qconfiguration = $OSCOM_PDO->prepare('select configuration_id,
                                                configuration_title,
                                                configuration_value,
                                                use_function
                                         from :table_configuration
                                         where configuration_key = :configuration_key
                                         order by sort_order
                                    ');
  $Qconfiguration->bindValue(':configuration_key', $cKey);
  $Qconfiguration->execute();

  while ($configuration = $Qconfiguration->fetch() ) {
    if (osc_not_null($configuration['use_function'])) {
      $use_function = $configuration['use_function'];
      if (preg_match('/->/', $use_function)) {
        $class_method = explode('->', $use_function);
        if (!is_object(${$class_method[0]})) {
          include('includes/classes/'. $class_method[0] . '.php');
          ${$class_method[0]} = new $class_method[0]();
        }
        $cfgValue = osc_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
      } else {
        $cfgValue = osc_call_function($use_function, $configuration['configuration_value']);
      }
    } else {
      $cfgValue = $configuration['configuration_value'];
    }

    if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {

      $QcfgExtra = $OSCOM_PDO->prepare("select configuration_key,
                                                configuration_description,
                                                date_added,
                                                last_modified,
                                                use_function,
                                                set_function
                                         from :table_configuration
                                         where configuration_id = :configuration_id
                                        ");

      $QcfgExtra->bindInt(':configuration_id', (int)(int)$configuration['configuration_id']);
      $QcfgExtra->execute();

      $cfg_extra = $QcfgExtra->fetch();

      $cInfo_array = array_merge($configuration, $cfg_extra);
      $cInfo = new objectInfo($cInfo_array);
    }

  }
  if ($cInfo->set_function) {
    eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
  } else {
    $value_field = osc_draw_input_field('configuration_value', $cInfo->configuration_value);
  }
?>
  <form name="ajaxform" id="ajaxform" <?php echo 'action="' . osc_href_link('configuration_popup_fields_ajax.php', 'cKey='  .$_GET['cKey']) . '"'; ?> method="post">

    <table border="0" width="100%" cellspacing="2" cellpadding="2">
      <tr>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
              <tr>
                <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/configuration_1.gif', 'configuration', '40', '40'); ?></td>
                <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
                <td align="right" class="pageHeading"><table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>
                        <div id="simple-msg" style="float:left;"></div>
                        <div style="float:right;">&nbsp;<?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE, 'id="simple-post"'); ?></div>
                      </td>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
          </tr>
         </table></td>
      </tr>
     </table>

    <div style="padding:20px 10px 30px 10px; text-align:left;">
      <div  style="font-weight: bold; font-size:12px;"><?php echo '&nbsp;' . $cInfo->configuration_title; ?></div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <div><?php echo $cInfo->configuration_description; ?></div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <div><?php echo $value_field; ?></div>
    </div>

  </form>
  <script type="text/javascript" src="ext/javascript/bootstrap/js/bootstrap_ajax_form_fields_configuration.js" /></script>
  <!-- footer //-->
<?php
 require('includes/application_bottom.php');
