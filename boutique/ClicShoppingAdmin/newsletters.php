<?php
/**
 * newsletters.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/
  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'lock':
      case 'unlock':
        $newsletter_id = osc_db_prepare_input($_GET['nID']);
        $status = (($action == 'lock') ? '1' : '0');

        $Qupdate = $OSCOM_PDO->prepare('update :table_newsletters
                                        set locked = :locked
                                         where newsletters_id = :newsletters_id
                                      ');

        $Qupdate->bindInt(':locked', $status);
        $Qupdate->bindInt(':newsletters_id', (int)$newsletter_id);

        $Qupdate->execute();

        osc_redirect(osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']));

      break;
      case 'insert':
      case 'update':
        if (isset($_POST['newsletter_id'])) $newsletter_id = osc_db_prepare_input($_POST['newsletter_id']);
        $newsletter_module = osc_db_prepare_input($_POST['module']);
        $title = osc_db_prepare_input($_POST['title']);
        $content = osc_db_prepare_input($_POST['message']);
        $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);
        $languages_newsletter_id = osc_db_prepare_input($_POST['languages_id']);
        $newsletters_accept_file = osc_db_prepare_input($_POST['newsletters_accept_file']);
        $newsletters_twitter = osc_db_prepare_input($_POST['newsletters_twitter']);
        $newsletters_customer_no_account = osc_db_prepare_input($_POST['newsletters_customer_no_account']);

        if($newsletter_module != "newsletter" || $newsletter != "product_notification") osc_redirect(osc_href_link('newsletters.php')); // Redirect and exit executio

        $newsletter_error = false;
        if (empty($title)) {
          $OSCOM_MessageStack->add(ERROR_NEWSLETTER_TITLE, 'error');
          $newsletter_error = true;
        }

        if (empty($newsletter_module)) {
          $OSCOM_MessageStack->add(ERROR_NEWSLETTER_MODULE, 'error');
          $newsletter_error = true;
        }

        if (empty($newsletters_accept_file)) {
          $newsletters_accept_file = 0;
        }

        if (empty($newsletters_twitter)) {
          $newsletters_twitter =0;
        }

        if(empty($newsletters_customer_no_account)) {
          $newsletters_customer_no_account = 0;
        }

        if ($newsletter_error == false) {
          $sql_data_array = array('title' => $title,
                                  'content' => $content,
                                  'module' => $newsletter_module,
                                  'languages_id' => $languages_newsletter_id,
                                  'customers_group_id' => $customers_group_id,
                                  'newsletters_accept_file' => $newsletters_accept_file,
                                  'newsletters_twitter' => $newsletters_twitter,
                                  'newsletters_customer_no_account' => $newsletters_customer_no_account
                                 );

          if ($action == 'insert') {
            $sql_data_array['date_added'] = 'now()';
            $sql_data_array['status'] = '0';
            $sql_data_array['locked'] = '0';

            $OSCOM_PDO->save('newsletters', $sql_data_array);
            $newsletter_id = $OSCOM_PDO->lastInsertId();

          } elseif ($action == 'update') {

            $OSCOM_PDO->save('newsletters', $sql_data_array, ['newsletters_id' => (int)$newsletter_id ] );

          }

          osc_redirect(osc_href_link('newsletters.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'nID=' . $newsletter_id));
        } else {
          $action = 'new';
        }
        break;

      case 'delete':
      case 'new': if (!isset($_GET['nID'])) break;
      case 'send':
      case 'confirm_send':
        $newsletter_id = osc_db_prepare_input($_GET['nID']);

        $Qcheck = $OSCOM_PDO->prepare('select locked
                                       from :table_newsletters
                                       where newsletters_id = :newsletters_id
                                      ');
        $Qcheck->bindInt(':newsletters_id', (int)$newsletter_id);
        $Qcheck->execute();

        $check = $Qcheck->fetch();



        if ($check['locked'] < 1) {
          switch ($action) {
            case 'delete': $error = ERROR_REMOVE_UNLOCKED_NEWSLETTER; break;
            case 'new': $error = ERROR_EDIT_UNLOCKED_NEWSLETTER; break;
            case 'send': $error = ERROR_SEND_UNLOCKED_NEWSLETTER; break;
            case 'confirm_send': $error = ERROR_SEND_UNLOCKED_NEWSLETTER; break;
          }

          $OSCOM_MessageStack->add_session($error, 'error');

          osc_redirect(osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] ));
        }
        break;

      case 'delete_all':

       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $newsletters['newsletters_id'] ) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_newsletters
                                          where newsletters_id = :newsletters_id
                                        ');
          $Qdelete->bindInt(':newsletters_id', (int)$newsletters['newsletters_id']);
          $Qdelete->execute();
         }
       }

       osc_redirect(osc_href_link('newsletters.php'));
    }
  }

  if (empty($action)) {
    $newsletters_query_raw = "select newsletters_id, 
                                     title, 
                                     length(content) as content_length, 
                                     module, 
                                     date_added, 
                                     date_sent, 
                                     status, 
                                     languages_id,
                                     customers_group_id,
                                     locked,
                                     newsletters_accept_file,
                                     newsletters_twitter,
                                     newsletters_customer_no_account
                              from newsletters
                              order by date_added desc
                             ";
    $newsletters_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $newsletters_query_raw, $newsletters_query_numrows);
    $newsletters_query = osc_db_query($newsletters_query_raw);
  }

  require('includes/header.php');
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
 <tr>
  <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
      <div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        <div>
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/newsletters.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
if (empty($action)) {
?>
          <div class="adminTitle">
            <span class="col-md-4 smallText" style="text-align: center;">
<?php echo $newsletters_split->display_count($newsletters_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS); ?><br />
<?php echo $newsletters_split->display_links($newsletters_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
            </span>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('newsletters.php', 'action=new') . '">' . osc_image_button('button_new_newsletter.gif', IMAGE_NEW_NEWSLETTER) . '</a>'; ?></span>
            <span><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></span>
            <span class="pull-right">
              <form name="delete_all" <?php echo 'action="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">&nbsp;<a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span> <?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
            </span>
          </div>
<?php
  } elseif ($action == 'preview') {
?>
          <div class="adminTitle">
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></span>
          </div>

<?php
  } elseif (($action == 'new')) {
    $form_action = 'insert';
    if (isset($_GET['nID'])) {
      $form_action = 'update';
    }
    echo osc_draw_form('newsletter', 'newsletters.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'action=' . $form_action); if ($form_action == 'update') echo osc_draw_hidden_field('newsletter_id', $nID);
?>
            <div class="adminTitle">
<?php
    if ($form_action == 'insert') {
?>
                <span class="pull-right"><?php echo osc_image_submit('button_save.gif', IMAGE_SAVE); ?>&nbsp;</span>
                <span class="pull-right"><?php echo '<a href="' . osc_href_link('newsletters.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . (isset($_GET['nID']) ? 'nID=' . $_GET['nID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
  <?php
  } elseif ($form_action == 'update') {
?>
                <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
                <span class="pull-right main"><?php echo '<a href="' . osc_href_link('newsletters.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . (isset($_GET['nID']) ? 'nID=' . $_GET['nID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
<?php
  }
?>
              </div>
<?php
          } elseif (($action != 'send') && ($action != 'confirm') && ($action != 'confirm_send') && ($action != 'confirm_send_valid')) {
?>
              <div class="adminTitle">
<?php
          if (SEND_EMAILS =='true') {
?>
                <span class="pull-right"><?php echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&action=confirm_send') . '">' . osc_image_button('button_send_mail.gif', IMAGE_SEND) . '</a>'; ?></span>
<?php
           }
?>
                <span class="pull-right"><?php echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></span>
              </div>
          </div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        </div>
        <div class="clearfix"></div>
<?php
  }
  if (($action != 'send') && ($action != 'confirm')  && ($action != 'confirm_send') && ($action != 'confirm_send_valid')) {
?>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<?php
  }

// --------------------------------------------------------
//       Création / mise à jour newsletter
// -------------------------------------------------------	
  if ($action == 'new') {
    $form_action = 'insert';

    $parameters = array('title' => '',
                        'content' => '',
                        'module' => '',
                        'languages_id' => '',
                        'customers_group_id' => ''
                       );

    $nInfo = new objectInfo($parameters);

    if (isset($_GET['nID'])) {
      $form_action = 'update';

      $nID = osc_db_prepare_input($_GET['nID']);

      $Qnewsletter = $OSCOM_PDO->prepare('select title, 
                                               content, 
                                               module,
                                               languages_id,
                                               customers_group_id,
                                               newsletters_accept_file,
                                               newsletters_twitter,
                                               newsletters_customer_no_account
                                     from :table_newsletters
                                     where newsletters_id = :newsletters_id
                                    ');
      $Qnewsletter->bindInt(':newsletters_id', (int)$nID);
      $Qnewsletter->execute();

      $newsletter = $Qnewsletter->fetch();


      $nInfo->objectInfo($newsletter);


     if (!isset($nInfo->newsletters_accept_file)) $nInfo->newsletters_accept_file = '1';
       switch ($nInfo->newsletters_accept_file) {
       case '0': $in_accept_file = false; $out_accept_file = true; break;
       case '1':
       default: $in_accept_file = true; $out_accept_file = false;
     }

     if (!isset($nInfo->newsletters_twitter)) $nInfo->newsletters_twitter = '1';
       switch ($nInfo->newsletters_twitter) {
       case '0': $in_accept_twitter = false; $out_accept_twitter = true; break;
       case '1':
       default: $in_accept_twitter = true; $out_accept_twitter = false;
     }


     if (!isset($nInfo->newsletters_customer_no_account)) $nInfo->newsletters_customer_no_account = '1';
       switch ($nInfo->newsletters_customer_no_account) {
       case '0': $in_accept_customer_no_account = false; $out_accept_customer_no_account = true; break;
       case '1':
       default: $in_accept_customer_no_account = true; $out_accept_customer_no_account = false;
     }

    } elseif ($_POST) {
      $nInfo->objectInfo($_POST);
    }

    $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_array = array();
    if ($dir = dir(DIR_WS_MODULES . 'newsletters/')) {
      while ($file = $dir->read()) {
        if (!is_dir(DIR_WS_MODULES . 'newsletters/' . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $directory_array[] = $file;
          }
        }
      }
      sort($directory_array);
      $dir->close();
    }

    for ($i=0, $n=sizeof($directory_array); $i<$n; $i++) {
      $modules_array[] = array('id' => substr($directory_array[$i], 0, strrpos($directory_array[$i], '.')), 'text' => substr($directory_array[$i], 0, strrpos($directory_array[$i], '.')));
    }

// Put languages information into an array for drop-down boxes
  $customers_group = osc_get_customers_group();
  $values_customers_group_id[0] = array ('id' =>'0', 'text' => TEXT_ALL_CUSTOMERS);
  for ($i=0, $n=sizeof($customers_group); $i<$n; $i++) {
  $values_customers_group_id[$i+1] = array ('id' =>$customers_group[$i]['id'], 'text' =>$customers_group[$i]['text']);
  }

// Put languages information into an array for drop-down boxes
  $languages = osc_get_languages();
  $values_languages_id[0]=array ('id' =>'0', 'text' => TEXT_ALL_LANGUAGES);
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
  $values_languages_id[$i+1]=array ('id' =>$languages[$i]['id'], 'text' =>$languages[$i]['name']);
  }
?>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr><?php echo osc_draw_form('newsletter', 'newsletters.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'action=' . $form_action); if ($form_action == 'update') echo osc_draw_hidden_field('newsletter_id', $nID); ?>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_NEWSLETTER_MODULE; ?></td>
            <td class="main"><?php echo osc_draw_pull_down_menu('module', $modules_array, $nInfo->module); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
            <tr>
             <td class="main" valign="top"><?php echo TEXT_NEWSLETTER_CUSTOMERS_GROUP; ?></td>
             <td class="main" align="left"><?php echo  osc_draw_pull_down_menu('customers_group_id', $values_customers_group_id,  $nInfo->customers_group_id); ?></td>
            </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
  }
?>
          <tr>
            <td class="main"><?php echo TEXT_NEWSLETTER_CUSTOMER_NO_ACCOUNT; ?></td>
            <td class="main"><?php echo  osc_draw_radio_field('newsletters_customer_no_account', '1', $in_accept_customer_no_account) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('newsletters_customer_no_account', '0', $out_accept_customer_no_account) . '&nbsp;' . TEXT_NO; 
?>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>           
          <tr>
            <td class="main" valign="top"><?php echo TEXT_NEWSLETTER_LANGUAGE; ?></td>
            <td class="main" align="left"><?php echo osc_draw_pull_down_menu('languages_id', $values_languages_id,  $nInfo->languages_id);?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_NEWSLETTER_TITLE; ?></td>
            <td class="main"><?php echo osc_draw_input_field('title', $nInfo->title, 'required aria-required="true" id="tilte"', true); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_NEWSLETTER_CREATE_FILE_HTML; ?></td>
            <td class="main"><?php echo  osc_draw_radio_field('newsletters_accept_file', '1', $in_accept_file) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('newsletters_accept_file', '0', $out_accept_file) . '&nbsp;' . TEXT_NO; 
?>
            </td>
          </tr>
<?php
     if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
?>
          <tr>
            <td class="main"><?php echo TEXT_NEWSLETTER_TWITTER; ?></td>
            <td class="main"><?php echo  osc_draw_radio_field('newsletters_twitter', '1', $in_accept_twitter) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('newsletters_twitter', '0', $out_accept_twitter) . '&nbsp;' . TEXT_NO; 
?>
            </td>
          </tr>
<?php
   }
?>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main" valign="top"><?php echo TEXT_NEWSLETTER_CONTENT; ?></td>
            <td class="main">
              <script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>		
<!-- osc_draw_textarea_ckeditor ne fonctionne pas dans ce cadre-->
                <?php echo osc_draw_textarea_ckeditor('message', 'soft', '750','300',  $nInfo->content);	?>
            </td>
          </tr>
          <tr>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
              <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                 <tr>
                    <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_DESCRIPTION); ?></td>
                    <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_DESCRIPTION; ?></strong></td>
                 </tr>
              </table></td>
            </tr>
            <tr>
              <td><table border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                  <td class="main">
                    <?php echo HELP_DESCRIPTION; ?>
                    <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg1"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                    <div class="modal fade" id="myModalWysiwyg1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                          </div>
                          <div class="modal-body" style="text-align:center;">
                            <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
             </table></td>
           </tr>
         </table>
        </tr>
       </table></td>
      </tr>
    </form>
<?php
  } elseif ($action == 'preview') {

// --------------------------------------------------------
//       Previsualisation
// -------------------------------------------------------
    $nID = osc_db_prepare_input($_GET['nID']);

    $Qnewsletter = $OSCOM_PDO->prepare('select title, 
                                               content, 
                                               module 
                                       from :table_newsletters
                                       where newsletters_id = :newsletters_id
                                      ');
    $Qnewsletter->bindInt(':newsletters_id', (int)$nID);
    $Qnewsletter->execute();

    $newsletter = $Qnewsletter->fetch();

    $nInfo = new objectInfo($newsletter);
?>
      <tr>
<!-- Effacer nl2br qui provoque des br suplémentaire à chaque ligne //-->
        <td><?php echo $nInfo->content; ?></td>
      </tr>
<?php
  } elseif ($action == 'send') {

// --------------------------------------------------------
//        Envoi newsletter
// -------------------------------------------------------	

    $nID = osc_db_prepare_input($_GET['nID']);
   
    $Qnewsletter = $OSCOM_PDO->prepare('select title, 
                                               content, 
                                               module 
                                       from :table_newsletters
                                       where newsletters_id = :newsletters_id
                                      ');
    $Qnewsletter->bindInt(':newsletters_id', (int)$nID);
    $Qnewsletter->execute();

    $newsletter = $Qnewsletter->fetch();

    $nInfo = new objectInfo($newsletter);

    include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/newsletters/' . $nInfo->module . substr($PHP_SELF, strrpos($PHP_SELF, '.')));
    include(DIR_WS_MODULES . 'newsletters/' . $nInfo->module . substr($PHP_SELF, strrpos($PHP_SELF, '.')));
    $module_name = $nInfo->module;
    $module = new $module_name($nInfo->title, $nInfo->content);

    if ($module->show_choose_audience) { echo $module->choose_audience(); } else { echo $module->confirm(); }

  } elseif ($action == 'confirm') {
    $nID = osc_db_prepare_input($_GET['nID']);

    $Qnewsletter = $OSCOM_PDO->prepare('select title, 
                                               content, 
                                               module,
                                               languages_id,
                                               customers_group_id,
                                               newsletters_accept_file,
                                               newsletters_twitter,
                                               newsletters_customer_no_account
                                   from :table_newsletters
                                   where newsletters_id = :newsletters_id
                                  ');
    $Qnewsletter->bindInt(':newsletters_id', (int)$nID);
    $Qnewsletter->execute();

    $newsletter = $Qnewsletter->fetch();

    $nInfo = new objectInfo($newsletter);

    include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/newsletters/' . $nInfo->module . substr($PHP_SELF, strrpos($PHP_SELF, '.')));
    include(DIR_WS_MODULES . 'newsletters/' . $nInfo->module . substr($PHP_SELF, strrpos($PHP_SELF, '.')));
    $module_name = $nInfo->module;
    $module = new $module_name($nInfo->title, $nInfo->content);
    echo $module->confirm();

  } elseif ($action == 'confirm_send') {
    $nID = osc_db_prepare_input($_GET['nID']);
    $nlID = osc_db_prepare_input($_GET['nlID']);
    $cgID = osc_db_prepare_input($_GET['cgID']);
    $ac = osc_db_prepare_input($_GET['ac']);

    $Qnewsletter = $OSCOM_PDO->prepare('select newsletters_id, 
                                               title, 
                                               content,  
                                               module,
                                               languages_id,
                                               customers_group_id ,
                                               newsletters_accept_file,
                                               newsletters_twitter,
                                               newsletters_customer_no_account
                                   from :table_newsletters
                                   where newsletters_id = :newsletters_id
                                  ');
    $Qnewsletter->bindInt(':newsletters_id', (int)$nID);
    $Qnewsletter->execute();

    $newsletter = $Qnewsletter->fetch();

    $nInfo = new objectInfo($newsletter);

    include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/newsletters/' . $nInfo->module . substr($PHP_SELF, strrpos($PHP_SELF, '.')));
    include(DIR_WS_MODULES . 'newsletters/' . $nInfo->module . substr($PHP_SELF, strrpos($PHP_SELF, '.')));
    $module_name = $nInfo->module;
    $module = new $module_name($nInfo->title, $nInfo->content);
?>
            <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><?php echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '30'); ?></td>
      </tr>
      <tr>
        <td align="center"><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main" align="center"><strong><?php echo TEXT_PLEASE_WAIT; ?></strong></td>
          </tr>
        </table></td>
      </tr>
<?php
// delete the timeout
//   osc_set_time_limit(0);
   flush();
    $module->send_fckeditor($nInfo->newsletters_id);
?>

    <meta http-equiv="refresh" content="5; URL=<?php echo osc_href_link('newsletters.php', 'action=confirm_send_valid'); ?>">

<?php
  } elseif ($action == 'confirm_send_valid') {

// --------------------------------------------------------
//       Confirmation envoi
// -------------------------------------------------------
?>
            <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><?php echo '<a href="' . osc_href_link('newsletters.php') . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '30'); ?></td>
      </tr>
      <tr>
        <td class="main" align="center"><font color="#ff0000"><strong><?php echo TEXT_FINISHED_SENDING_EMAILS; ?></strong></font></td>
      </tr>

<?php
  } else {

// --------------------------------------------------------
//        Listing newsletter
// -------------------------------------------------------
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NEWSLETTERS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SIZE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MODULE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LANGUAGE; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
  if (MODE_B2B_B2C == 'true') {
?>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_B2B; ?></td>
<?PHP
  }
?>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SENT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    while ($newsletters = osc_db_fetch_array($newsletters_query)) {

// Permettre l'affichage des groupes en mode B2B
     if (MODE_B2B_B2C == 'true') {

      $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name 
                                              from :table_customers_groups
                                              where customers_group_id = :customers_group_id
                                            ');
      $QcustomersGroup->bindInt(':customers_group_id', (int)$newsletters['customers_group_id']);
      $QcustomersGroup->execute();

      $customers_group = $QcustomersGroup->fetch();

       if ($customers_group['customers_group_name'] == '') {
          $customers_group['customers_group_name'] =  TEXT_ALL_CUSTOMERS;
       }
     }

     if ($newsletters['languages_id'] != '0') {

       $QnewslettersLanguages = $OSCOM_PDO->prepare('select name
                                                     from :table_languages
                                                     where languages_id = :language_id
                                                    ');
       $QnewslettersLanguages->bindInt(':language_id',  (int)$newsletters['languages_id']);
       $QnewslettersLanguages->execute();

       $newsletters_language = $QnewslettersLanguages->fetch();

      } else {
        $newsletters_language['name'] =  TEXT_ALL_LANGUAGES;
      }

    if ((!isset($_GET['nID']) || (isset($_GET['nID']) && ($_GET['nID'] == $newsletters['newsletters_id']))) && !isset($nInfo) && (substr($action, 0, 3) != 'new')) {
        $nInfo = new objectInfo($newsletters);
      }

      if (isset($nInfo) && is_object($nInfo) && ($newsletters['newsletters_id'] == $nInfo->newsletters_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
/*
      if (isset($nInfo) && is_object($nInfo) && ($newsletters['newsletters_id'] == $nInfo->newsletters_id) ) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $nInfo->newsletters_id . '&action=preview') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id']) . '\'">' . "\n";
      }
*/
?>
               <td>
<?php 
      if ($newsletter['selected']) { 
?>
                 <input type="checkbox" name="selected[]" value="<?php echo $newsletters['newsletters_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $newsletters['newsletters_id']; ?>" />
<?php 
      } 
?>
               </td>
               <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id'] . '&action=preview') . '">' . $newsletters['title'] .'</a>'; ?></td>
                <td class="dataTableContent" align="center"><?php echo number_format($newsletters['content_length']) . ' bytes'; ?></td>
                <td class="dataTableContent" align="center"><?php echo $newsletters['module']; ?></td>
                <td class="dataTableContent" align="center"><?php echo  $newsletters_language['name']; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
   if (MODE_B2B_B2C == 'true') {
?>
                <td class="dataTableContent" align="center"><?php echo $customers_group['customers_group_name']; ?></td>

<?PHP
  }
?>
                <td class="dataTableContent" align="center"><?php if ($newsletters['status'] == '1') { echo osc_image(DIR_WS_ICONS . 'tick.gif', ICON_TICK); } else { echo osc_image(DIR_WS_ICONS . 'cross.gif', ICON_CROSS); } ?></td>
                <td class="dataTableContent" align="center"><?php if ($newsletters['locked'] > 0) { echo osc_image(DIR_WS_ICONS . 'locked.gif', ICON_LOCKED); } else { echo osc_image(DIR_WS_ICONS . 'unlocked.gif', ICON_UNLOCKED); } ?></td>
                <td class="dataTableContent" align="right">
<?php
                  if ($newsletters['locked'] > 0) { echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id'] . '&action=new') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' . osc_draw_separator('pixel_trans.gif', '6', '16'); }
                  echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id'] . '&action=preview') . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if ($newsletters['locked'] > 0) { echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id'] . '&action=unlock') . '">' . osc_image(DIR_WS_ICONS . 'unlock.gif', IMAGE_UNLOCK) . '</a>'; } else { echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id'] . '&action=lock') . '">' . osc_image(DIR_WS_ICONS . 'lock.gif', IMAGE_LOCK) . '</a>'; }
                  if ($newsletters['locked'] > 0) { echo osc_draw_separator('pixel_trans.gif', '6', '16') . '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id'] . '&nlID=' .$newsletters['languages_id'] .'&cgID=' .$newsletters['customers_group_id'] . '&ac='. $newsletters['newsletters_accept_file'] . '&at='. $newsletters['newsletters_twitter'] . '&ana='. $newsletters['newsletters_customer_no_account'] . '&action=send') . '">' . osc_image(DIR_WS_ICONS . 'send.gif', IMAGE_SEND) . '</a>'; }
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($nInfo) && is_object($nInfo) && ($newsletters['newsletters_id'] == $nInfo->newsletters_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $newsletters['newsletters_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    }
?>
              </form><!-- end form delete all -->
              <tr>
                  <td colspan="12" class="smallText" valign="top"><?php echo $newsletters_split->display_count($newsletters_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS); ?></td>
              </tr>
            </table></td>
<?php
  }
 ?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>