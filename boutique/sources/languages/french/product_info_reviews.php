<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Commentaires');
define('HEADING_TITLE_REVIEWS', ' Les Commentaires de %s');
define('SUB_TITLE_PRODUCT', 'Article :');
define('SUB_TITLE_FROM', 'De :');
define('SUB_TITLE_DATE', 'Date :');
define('SUB_TITLE_REVIEW', 'Commentaire(s) :');
define('SUB_TITLE_RATING', 'Note :');
define('TEXT_OF_5_STARS', '%s sur 5 Etoiles !');
define('TEXT_CLICK_TO_ENLARGE', 'Cliquer pour agrandir');
?>