<?php
/*
 * define_language.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Editer les fichiers langues');
define('TABLE_HEADING_FILES', 'Fichiers');
define('TABLE_HEADING_WRITABLE', 'Ecriture');
define('TABLE_HEADING_LAST_MODIFIED', 'Dernière modification');

define('TEXT_EDIT_NOTE', '<strong>Editing Definitions</strong><br /><br />Each language definition is set using the PHP <a href="http://www.php.net/define" target="_blank">define()</a> function in the following manner:<br /><br /><nobr>define(\'TEXT_MAIN\', \'<span style="background-color: #FFFF99;">This text can be edited. It\\\'s really easy to do!</span>\');</nobr><br /><br />The highlighted text can be edited. As this definition is using single quotes to contain the text, any single quotes within the text definition must be escaped with a backslash (eg, It\\\'s).');
define('TEXT_FILE_DOES_NOT_EXIST', 'Le fichier n\'existe pas.');

define('ERROR_FILE_NOT_WRITEABLE', 'Erreur : Impossible d\'écrire dans le fichier. Merci de vérifier les droits d\'accès sur : %s');
?>