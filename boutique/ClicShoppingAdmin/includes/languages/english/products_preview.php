<?php
/*
 * products_preview.php 
 * @copyright Copyright 2010 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Product preview');

define('TEXT_NO_PRODUCTS','The product to preview does\'nt exist');

define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Product Meta Data');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Title');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_KEYWORDS', 'Keywords');
define('TEXT_PRODUCTS_TAG','Tags');

define('TEXT_NO_PRODUCTS','The product to preview does not exist');

define('TEXT_PRODUCTS', 'Products:');
define('TEXT_PRODUCTS_PRICE_INFO', 'Sell price :');
define('TEXT_PRODUCTS_TAX_CLASS', 'Tax Class :');
define('TEXT_PRODUCTS_COST','Supplier Cost :');
define('TEXT_PRODUCTS_HANDLING','Others costs :');

define('TEXT_USER_NAME','Created / Modified by : ');
define('TEXT_PRODUCT_DATE_ADDED', 'Product added our catalog on:');
define('TEXT_PRODUCTS_SHIPPING_DELAY','Specific delivery (other than by default) : ');
define('TEXT_PRODUCTS_VIEW','Display options');

define('TEXT_PRODUCTS_STATUS', 'Status:');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date available:');
define('TEXT_PRODUCTS_DIMENSION','Dimensions (LxHxW)');
define('TEXT_PRODUCTS_DIMENSION_TYPE','Dimension type');
define('TEXT_PRODUCT_AVAILABLE', 'In Stock');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Out of stock');
define('TEXT_PRODUCTS_MANUFACTURER', 'Product brand:');
define('TEXT_PRODUCTS_WEIGHT_POUNDS','Weight (pounds)');

define('TEXT_PRODUCTS_TIME_REPLENISHMENT','Resupplying:');
define('TEXT_PRODUCTS_WHAREHOUSE','Wharehouse name');
define('TEXT_PRODUCTS_WHAREHOUSE_ROW','Wharehouse row:');
define('TEXT_PRODUCTS_WHAREHOUSE_LEVEL_LOCATION','Location level:');
define('TEXT_PRODUCTS_WHAREHOUSE_PACKAGING','Packaging type:');
define('TEXT_WHAREHOUSE','Storage');
define('TEXT_PRODUCTS_PACKAGING_NEW','New product');
define('TEXT_PRODUCTS_PACKAGING_REPACKAGED','Product repackaged');
define('TEXT_PRODUCTS_PACKAGING_USED','Product Used');



define('TEXT_PRODUCTS_NAME', 'Products Name');
define('TEXT_PRODUCTS_DESCRIPTION', 'Products Description');
define('TEXT_PRODUCTS_QUANTITY', 'Stock quantity:');
define('TEXT_PRODUCTS_MIN_ORDER_QUANTITY', 'Quantity minimal for a customer order :');
define('TEXT_PRODUCTS_MODEL', 'Model:');
define('TEXT_PRODUCTS_EAN', 'Barcode (EAN13):');
define('TEXT_PRODUCTS_SUPPLIERS', 'Product supplier:');
define('TEXT_PRODUCTS_SKU','SKU : ');
define('TEXT_PRODUCTS_PRICE_COMPARISON','Excluding price comparison ? ');
define('TEXT_PRODUCTS_VOLUME','Product volume :');
define('TEXT_PRODUCTS_QUANTITY_UNIT','Quantity unit');
define('TEXT_PRODUCTS_ONLY_ONLINE','Only on the web (no selling in shop)');

define('TEXT_PRODUCTS_URL', 'Products URL:');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(without http://)</small>');
define('TEXT_PRODUCTS_PRESENTATION', 'Presentation');
define('TEXT_PRODUCTS_STOCK', 'Stock');
define('TEXT_PRODUCTS_PRICE_PUBLIC', 'Public price & TAXE');
define('TEXT_PRODUCTS_PRICE', 'Price:');
define('TEXT_PRODUCTS_PRICE_NET', 'TAX exc.');
define('TEXT_PRODUCTS_WEIGHT', 'Product weight:');
define('TEXT_PRODUCTS_PRICE_KILO','Display the price per kilo:');

define('TAB_PRICE_GROUP_VIEW', 'CREDIT:  Post the price of the group - NOT CREDIT:  Post the public price');
define('TAB_PRODUCTS_GROUP_VIEW', 'To post the product for the customers of the group ');
define('TAB_PRODUCTS_VIEW', 'To post the product for the customers general public ');
define('TAB_ORDERS_GROUP_VIEW', 'To authorize the ordering of the product for the customers of the group ');
define('TAB_ORDERS_VIEW', 'To authorize the ordering of the product for the customers General public');
?>
