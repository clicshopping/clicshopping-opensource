<?php
/*
 * stats_products_no_purchased.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
 */


define('HEADING_TITLE', 'Les produits non achetés');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_NO_PURCHASED', 'Non Acheté');
?>