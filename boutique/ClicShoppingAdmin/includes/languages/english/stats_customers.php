<?php
/*
 * stats_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Best Customer Orders-Total');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_CUSTOMERS', 'Customers');
define('TABLE_HEADING_TOTAL_PURCHASED', 'Total Purchased');
define('TABLE_HEADING_ACTION','Action');

?>