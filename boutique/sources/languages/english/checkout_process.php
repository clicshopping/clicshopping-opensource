<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('EMAIL_TEXT_SUBJECT', 'Order Process '. STORE_NAME);
define('EMAIL_TEXT_ORDER_NUMBER', 'Hello,' . "\n\n" . 'Please find the elements concerning your order into'. STORE_NAME .' '."\n\n".'We thank you for your confidence and we make our possible as well as possible to treat the sending of your order.' . "\n\n" . 'Order Number :');
define('EMAIL_TEXT_INVOICE_URL', 'Detailed Invoice:');
define('EMAIL_TEXT_DATE_ORDERED', 'Date Ordered:');
define('EMAIL_TEXT_PRODUCTS', 'Products');
define('EMAIL_TEXT_SUBTOTAL', 'Sub-Total:');
define('EMAIL_TEXT_TAX', 'Tax:        ');
define('EMAIL_TEXT_SHIPPING', 'Shipping: ');
define('EMAIL_TEXT_TOTAL', 'Total:    ');
define('EMAIL_TEXT_DELIVERY_ADDRESS', 'Delivery Address');
define('EMAIL_TEXT_BILLING_ADDRESS', 'Billing Address');
define('EMAIL_TEXT_PAYMENT_METHOD', 'Payment Method');

define('EMAIL_SEPARATOR', '');
define('TEXT_EMAIL_VIA', 'via');
define('EMAIL_TEXT_FOOTER', 'Your order is being prepared and you will be sent soon. Please print this email certifying that you have to take order well on '.STORE_NAME.' or for any later correspondence.' . "\n\n" . 'Receive, dear customer, our sincere greetings.'. "\n" . '
Regards,'. "\n" . '<strong>'.STORE_NAME.' Team</strong>'. "\n" . '
<p>'.STORE_NAME_ADDRESS.' '. "\n" . '
<U><font size="2">Confidentiality Note :</font></U><font size="2">'. "\n" . '
This message as well as the documents which would be appended are addressed exclusively to their recipient and could contain confidential information subjected to the professional secrecy or whose disclosure is prohibited under the terms of the legislation in force. So we inform the person who would receive it without being the recipient or an authorized person, that this information is confidential and that any use, copies, files or disclosure is prohibited.  If you received this message, we ask you to agree to communicate it to us by email : ' . STORE_OWNER_EMAIL_ADDRESS . '. and to proceed directly to its destruction.'. "\n" . '
<p><font size="2">In accordance with the <strong>Law</strong> no 78-17 of January 6, 1978 known as Data-processing Law and Freedom, you are entitled to correction of your personal data or on request by email</font></div>');


define('EMAIL_TEXT_SUBJECT_STOCK','Stock Alert on product on ' . STORE_NAME);

define('EMAIL_REORDER_LEVEL_TEXT_STOCK', 'Hello,' . "\n\n" . 'You recieved an warning email concernng your warning stock (general configuration) for this product. This warning stock is the alrt stock where on or several procducs is reached. '. "\n" . 'There the differents caracteristics on the product : '. "\n" . ' ');
define('EMAIL_TEXT_STOCK', 'Hello, '. "\n\n" . ' You receive an  warning about the exhausted stock on one of your products. Your product is no available on your store. However, you can reactivate it without modifying your stock. The Add button will be replaced by the following exhausted button. '. "\n" . '  There characteristics of the products out of stock :'. "\n" . '');

define('EMAIL_TEXT_SUBJECT_ALERT_STOCK','Stock alert on specific product on ' . STORE_NAME);
define('EMAIL_REORDER_LEVEL_TEXT_ALERT_STOCK', 'Hello,' . "\n\n" . 'You receive an  warning about the alert stock (defined in the product) concerning a product.  Your product is available on your store. '. "\n" . 'There characteristics of the products out of stock : '. "\n" . ' ');
define('EMAIL_TEXT_PRODUCT_ALERT_STOCK','Stock on this product : ');


define('EMAIL_TEXT_ID_PRODUCT','Product id : ');
define('EMAIL_TEXT_MODEL','Model : ');
define('EMAIL_TEXT_PRODUCTS_NAME','Product name : ');
define('EMAIL_TEXT_PRODUCT_URL:','Product URL : ');
define('EMAIL_TEXT_PRODUCT_STOCK','New product stock : ');
define('EMAIL_TEXT_DATE_ALERT','Alert date');
?>
