<?php
/**
 * customers_group.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

// Permettre l'utilisation de des groupes clients
  if (MODE_B2B_B2C == 'false')  osc_redirect(osc_href_link('index.php', '', 'SSL'));


  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (osc_not_null($action)) {
    switch ($_GET['action']) {
      case 'update':

          $customers_groups_id = osc_db_prepare_input($_GET['cID']);
          $customers_groups_name = osc_db_prepare_input($_POST['customers_group_name']); 
          $customers_groups_discount = osc_db_prepare_input($_POST['customers_group_discount']);
          $color_bar = osc_db_prepare_input($_POST['color_bar']);
          $customers_group_quantity_default =  osc_db_prepare_input($_POST['customers_group_quantity_default']);

// Supprimer (|| $customers_group_discount ==  0) dans la condition IF pour pouvoir cree un groupe a 0% par defaut

          if (strlen($customers_groups_name) == "") {
// error return if group name error
            $error = true;
            $OSCOM_MessageStack->add(ENTRY_GROUPS_NAME_ERROR);
            $group_id = osc_db_prepare_input($_GET['cID']);
            osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID')) . 'action=edit&cID=' . $group_id . '&error=name'));
          } else {
             if (empty($customers_groups_discount)) {
// valeur discount mis a zero si le champs est vide
               $customers_group_discount = '0.00';
          }


// Module de paiment autorise
          if ($_POST['payment_unallowed']) {
            while(list($key, $val) = each($_POST['payment_unallowed'])) {
              if ($val == true) { $group_payment_unallowed .= $val.','; }
            }
            $group_payment_unallowed = substr($group_payment_unallowed,0,strlen($group_payment_unallowed)-1);
          }

// Module de livraison autorise
          if ($_POST['shipping_unallowed']) {
            while(list($key, $val) = each($_POST['shipping_unallowed'])) {
              if ($val == true) { 
                $group_shipping_unallowed .= $val.','; 
              }
            }
          $group_shipping_unallowed = substr($group_shipping_unallowed,0,strlen($group_shipping_unallowed)-1);
          }

// Assujetti ou non a la TVA
          $group_order_taxe = osc_db_prepare_input($_POST['group_order_taxe']);
         
          
// Affichage des prix en HT ou en TTC (Mis automatiquement en false "HT" si l'on coche non-assujetti a la TVA)
         if ($group_order_taxe == 1) {
           $group_tax = 'false';
         } else {
           $group_tax = osc_db_prepare_input($_POST['group_tax']);
         }

          $Qupdate = $OSCOM_PDO->prepare('update :table_customers_groups  
                                          set customers_group_name = :customers_group_name,
                                              customers_group_discount = :customers_group_discount, 
                                              color_bar = :color_bar,  
                                              group_order_taxe = :group_order_taxe,  
                                              group_payment_unallowed = :group_payment_unallowed, 
                                              group_shipping_unallowed = :group_shipping_unallowed, 
                                              group_tax = :group_tax,
                                              customers_group_quantity_default = :customers_group_quantity_default
                                          where customers_group_id = :customers_info_id
                                        ');
          $Qupdate->bindValue(':customers_group_name', $customers_groups_name );
          $Qupdate->bindValue(':customers_group_discount',$customers_groups_discount);
          $Qupdate->bindValue(':color_bar',$color_bar);
          $Qupdate->bindValue(':group_order_taxe', $group_order_taxe);
          $Qupdate->bindValue(':group_payment_unallowed', $group_payment_unallowed);
          $Qupdate->bindValue(':group_shipping_unallowed', $group_shipping_unallowed);
          $Qupdate->bindValue(':group_tax', $group_tax);
          $Qupdate->bindInt(':customers_group_quantity_default', (int)$customers_group_quantity_default);
          $Qupdate->bindInt(':customers_info_id', (int)$customers_groups_id);
          $Qupdate->execute();

//***************************************
// odoo web service
//***************************************

            if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_GROUP == 'true') {
              require('ext/odoo_xmlrpc/xml_rpc_admin_customers_group.php');
            }
//***************************************
// End odoo web service
//***************************************

          osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_groups_id));
        }
      break;

      case 'deleteconfirm':

        $group_id = osc_db_prepare_input($_GET['cID']);

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_groups_to_categories
                                        where customers_group_id = :customers_group_id 
                                      ');
        $Qdelete->bindInt(':customers_group_id', (int)$group_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_customers_groups
                                        where customers_group_id = :customers_group_id 
                                      ');
        $Qdelete->bindInt(':customers_group_id', (int)$group_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_products_groups
                                        where customers_group_id = :customers_group_id 
                                      ');
        $Qdelete->bindInt(':customers_group_id', (int)$group_id);
        $Qdelete->execute();

// delete all banners
        $QbannerCustomersId = $OSCOM_PDO->prepare("select count(customers_group_id) as count 
                                                   from :table_banners
                                                   where customers_group_id = :customers_group_id
                                                 ");
        $QbannerCustomersId->bindInt(':customers_group_id', (int)$group_id );
        $QbannerCustomersId->execute();

        $banner_customers_id = $QbannerCustomersId->fetch();

        if ($banner_customers_id['count'] > 0) {

          $Qdelete = $OSCOM_PDO->prepare('delete from :table_banners
                                          where customers_group_id = :customers_group_id
                                      ');
          $Qdelete->bindInt(':customers_group_id', (int)$group_id);
          $Qdelete->execute();
        }

// delete all extra fields

        $QproductsExtraFieldsCustomersId = $OSCOM_PDO->prepare("select count(customers_group_id) as count 
                                                               from :table_products_extra_fields
                                                               where customers_group_id = :customers_group_id
                                                             ");
        $QproductsExtraFieldsCustomersId->bindInt(':customers_group_id', (int)$group_id );
        $QproductsExtraFieldsCustomersId->execute();

        $products_extra_fields_customers_id = $QproductsExtraFieldsCustomersId->fetch();


        if ($products_extra_fields_customers_id['count'] > 0) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_products_extra_fields
                                          where customers_group_id = :customers_group_id 
                                        ');
          $Qdelete->bindInt(':customers_group_id', (int)$group_id);
          $Qdelete->execute();
        }

        $QpageManagerCustomersId = $OSCOM_PDO->prepare("select count(customers_group_id) as count 
                                                       from :table_pages_manager
                                                       where customers_group_id = :customers_group_id
                                                     ");
        $QpageManagerCustomersId->bindInt(':customers_group_id', (int)$group_id );
        $QpageManagerCustomersId->execute();

        $page_manager_customers_id = $QpageManagerCustomersId->fetch();

        if ($page_manager_customers_id['count'] > 0) {
// delete all page manager
          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_pages_manager
                                          where customers_group_id = :customers_group_id 
                                        ');
          $Qdelete->bindInt(':customers_group_id', (int)$group_id);
          $Qdelete->execute();
        }

// delete all specials
        $QspecialsProductsCustomersId = $OSCOM_PDO->prepare("select count(customers_group_id) as count 
                                                               from :table_specials
                                                               where customers_group_id = :customers_group_id
                                                             ");
        $QspecialsProductsCustomersId->bindInt(':customers_group_id', (int)$group_id );
        $QspecialsProductsCustomersId->execute();

        $specials_products_customers_id = $QspecialsProductsCustomersId->fetch();

        if ($specials_products_customers_id['count'] > 0) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_specials
                                          where customers_group_id = :customers_group_id 
                                        ');
          $Qdelete->bindInt(':customers_group_id', (int)$group_id);
          $Qdelete->execute();
        }

// delete all products heart

        $QProductsHeartCustomersId = $OSCOM_PDO->prepare('select count(customers_group_id) as count
                                                           from :table_products_heart
                                                           where customers_group_id = :customers_group_id
                                                         ');
        $QProductsHeartCustomersId->bindInt(':customers_group_id', (int)$group_id );
        $QProductsHeartCustomersId->execute();

        $products_heart_customers_id = $QProductsHeartCustomersId->fetch();

        if ($products_heart_customers_id['count'] > 0) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_products_heart
                                          where customers_group_id = :customers_group_id 
                                        ');
          $Qdelete->bindInt(':customers_group_id', (int)$group_id);
          $Qdelete->execute();
       }
// delete all newsletter

       $QnewsletteCustomersId = $OSCOM_PDO->prepare('select count(customers_group_id) as count
                                                     from :table_newsletters
                                                     where customers_group_id = :customers_group_id
                                                   ');
        $QnewsletteCustomersId->bindInt(':customers_group_id', (int)$group_id );
        $QnewsletteCustomersId->execute();

        $newsletter_customers_id = $QnewsletteCustomersId->fetch();


        if ($newsletter_customers_id['count'] > 0) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_newsletters
                                          where customers_group_id = :customers_group_id 
                                        ');
          $Qdelete->bindInt(':customers_group_id', (int)$group_id);
          $Qdelete->execute();
        }

// update all customers

        $QcustomersId = $OSCOM_PDO->prepare('select customers_id
                                             from :table_customers
                                             where customers_group_id = :customers_group_id
                                           ');
        $QcustomersId->bindInt(':customers_group_id',(int)$group_id);
        $QcustomersId->execute();

        
        while($customers_id = $QcustomersId->fetch() ) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_customers
                                          set customers_group_id = :customers_group_id
                                          where customers_id = :customers_id
                                          ');
          $Qupdate->bindValue(':customers_group_id', 1);
          $Qupdate->bindInt(':customers_id', (int)$customers_id['customers_id']);
          $Qupdate->execute();

        }

        osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action'))));
        break;

      case 'newconfirm':

        $customers_groups_name = osc_db_prepare_input($_POST['customers_group_name']);
        $customers_groups_discount = osc_db_prepare_input($_POST['customers_group_discount']);
        $color_bar = osc_db_prepare_input($_POST['color_bar']); //ok      
        $customers_group_quantity_default =  osc_db_prepare_input($_POST['customers_group_quantity_default']);
          $group_order_taxe = osc_db_prepare_input($_POST['group_order_taxe']);

// Supprimer (|| $customers_group_discount ==  0) dans la condition IF pour pouvoir cree un groupe a 0% par defaut
        if (strlen($customers_groups_name) == "") {
           $error = true;
           $OSCOM_MessageStack->add(ENTRY_GROUPS_NAME_ERROR);
           osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID')) . 'action=new&error=name'));
        } else { 
          if (empty($customers_groups_discount)) {
// valeur discount mis a zero si le champs est vide
            $customers_group_discount = '0.00';
          }

// Affichage des prix produits en HT ou TTC (Mis automatiquement en false "HT" si l'on coche non-assujetti a la TVA)
          if ($group_order_taxe == 1) {
            $group_tax = 'false';
          } else {
            $group_tax = osc_db_prepare_input($_POST['group_tax']);
          }

// Module de paiment autorise
          if ($_POST['payment_unallowed']) {

            while(list($key, $val) = each($_POST['payment_unallowed'])) {
              if ($val == true) { $group_payment_unallowed .= $val.','; }
            }

            $group_payment_unallowed = substr($group_payment_unallowed,0,strlen($group_payment_unallowed)-1);
          }

// Module de livraison autorise
          if ($_POST['shipping_unallowed']) {

            while(list($key, $val) = each($_POST['shipping_unallowed'])) {

              if ($val == true) {
                $group_shipping_unallowed .= $val.',';
              }
            }

            $group_shipping_unallowed = substr($group_shipping_unallowed,0,strlen($group_shipping_unallowed)-1);
          }

          $OSCOM_PDO->save('customers_groups', [
                                                'customers_group_name' => $customers_groups_name,
                                                'customers_group_discount' => $customers_groups_discount,
                                                'color_bar' => $color_bar,
                                                'group_payment_unallowed' => $group_payment_unallowed,
                                                'group_shipping_unallowed' => $group_shipping_unallowed ,
                                                'group_tax' => $group_tax,
                                                'group_order_taxe' => $group_order_taxe,
                                                'customers_group_quantity_default' => $customers_group_quantity_default
                                              ]
                          );

//***************************************
// odoo web service
//***************************************

            if (ODOO_ACTIVATE_WEB_SERVICE == 'true' &&  ODOO_ACTIVATE_CUSTOMERS_GROUP == 'true')  {
              require('ext/odoo_xmlrpc/xml_rpc_admin_customers_group.php');
            }
//***************************************
// End odoo web service
//***************************************

            osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action'))));
      } // end strlen($customers_groups_name)

      break;

      case 'newdiscountconfirm':
// Supprimer (|| $_POST['discount'] == 0) dans la condition IF pour pouvoir cree un groupe a 0% par defaut


        if ((empty($_POST['discount'])) || ($_POST['categories_id']) == 0) {
          $error = true;
          $OSCOM_MessageStack->add(ENTRY_GROUPS_CATEGORIE_ERROR);
          $group_id = osc_db_prepare_input($_POST['cID']);
          osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID')) . 'action=edit&cID=' . $group_id . '&error=categorie'));

        } else {

          $group_id = osc_db_prepare_input($_POST['cID']);
          $new_category_discount = osc_db_prepare_input($_POST['discount']);
          $new_category_id = osc_db_prepare_input($_POST['categories_id']);

          $new_category_discount = round($new_category_discount,2);

          $Qcheck = $OSCOM_PDO->prepare("select * 
                                         from :table_groups_to_categories
                                         where customers_group_id = :customers_group_id
                                         and categories_id = :categories_id
                                       ");
          $Qcheck->bindInt(':customers_group_id', (int)$group_id );
          $Qcheck->bindInt(':categories_id', (int)$new_category_id );
          $Qcheck->execute();

          if ($Qcheck->fetch() === false) {

            $OSCOM_PDO->save('groups_to_categories', [
                                                      'customers_group_id' =>  (int)$group_id,
                                                      'categories_id' => (int)$new_category_id,
                                                      'discount' => $new_category_discount
                                                     ]
                            );
          }
          osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID')) . '&action=edit&cID=' . $group_id));
        }

      break;

      case 'deletediscount':
        $group_id = osc_db_prepare_input($_GET['cID']);
        $category_id = osc_db_prepare_input($_GET['catID']);

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_groups_to_categories
                                        where  customers_group_id = :customers_id
                                        and categories_id = :products_id
                                        ');
        $Qdelete->bindInt(':customers_id', (int)$group_id);
        $Qdelete->bindInt(':products_id', (int)$category_id);
        $Qdelete->execute();

        osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action')) . '&action=edit&cID=' . $group_id));
      break;

      case 'updatediscount':
// Supprimer (|| $_POST['upddiscount'] == 0) dans la condition IF pour pouvoir cree un groupe a 0% par defaut
        if (empty($_POST['upddiscount'])) {
          $error = true;
          $OSCOM_MessageStack->add(ENTRY_GROUPS_NAME_ERROR_ZERO);

        } else {

          $group_id = osc_db_prepare_input($_GET['cID']);
          $new_discount = osc_db_prepare_input($_POST['upddiscount']);
          $category_id = osc_db_prepare_input($_POST['catID']);

          $Qupdate = $OSCOM_PDO->prepare('update groups_to_categories
                                          set discount = :discount
                                          where customers_group_id = :customers_group_id
                                          and categories_id = :categories_id
                                         ');
          $Qupdate->bindValue(':discount', $new_discount);
          $Qupdate->bindInt(':customers_group_id',(int)$group_id);
          $Qupdate->bindInt(':categories_id',(int)$category_id);
          $Qupdate->execute();

          osc_redirect(osc_href_link('customers_groups.php', osc_get_all_get_params(array('action')) . '&action=edit&cID=' . (int)$group_id));
        }
    break;

    case 'updateallprice':
      $groups_id = osc_db_prepare_input($_GET['cID']);

// Requetes SQL pour selectionner les prix grand public
      $Qpricek = $OSCOM_PDO->prepare('select p.products_price,
                                           p.products_id,
                                           p.products_percentage,
                                           pc.categories_id
                                    from :table_products p,
                                         :table_products_to_categories pc
                                    where pc.products_id = p.products_id
                                    ');
      $Qpricek->execute();


      if ($Qpricek->fetch() !== false) {

        while ($Qpricek->fetch() ) {

// Requetes SQL pour pour connaitre la valeur promotion/majoration sur le groupe
          $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                                 customers_group_name,
                                                                 customers_group_discount,
                                                                 customers_group_quantity_default
                                                  from :table_customers_groups
                                                  where customers_group_id = :customers_group_id
                                                ');
          $QcustomersGroup->bindInt(':customers_group_id', $groups_id);
          $QcustomersGroup->execute();


          if ($QcustomersGroup->rowCount() > 0) {
          $Qattributes = $OSCOM_PDO->prepare('select customers_group_id
                                                from :table_products_groups
                                                where customers_group_id = :customers_group_id
                                                and products_id = :products_id
                                              ');
          $Qattributes->bindInt(':customers_group_id', (int)$groups_id);
          $Qattributes->bindInt(':products_id', (int)$Qpricek->valueInt('products_id'));
          $Qattributes->execute();
          
          
            $Qdiscount = $OSCOM_PDO->prepare('select discount
                                              from :table_groups_to_categories
                                              where customers_group_id = :customers_group_id
                                              and categories_id = :categories_id
                                            ');
            $Qdiscount->bindInt(':customers_group_id', $groups_id);
            $Qdiscount->bindInt(':categories_id', $Qpricek->valueInt('categories_id') );
            $Qdiscount->execute();

            if (is_null($Qdiscount->value('discount')) ) {
              $ricarico = $QcustomersGroup->value('customers_group_discount');
            } else {
              $ricarico = $Qdiscount->value('discount');
            }
          } // end num_rows

// Applique le nouveau prix
          $pricek = $Qpricek->value('products_price');
           if ($pricek > 0){
             if (B2B == 'true') {
               if ($ricarico > 0) $newprice = $pricek+($pricek/100)*$ricarico;
               if ($ricarico == 0) $newprice = $pricek;
             }
             if (B2B == 'false') {
              if ($ricarico > 0) $newprice = $pricek-($pricek/100)*$ricarico;
              if ($ricarico == 0) $newprice = $pricek;
             }
           } else {
             $newprice = 0;
           } // end $pricek




// Mise a jour de la base produits sur les groupes
          if ($Qattributes->valueInt('customers_group_id') == null ) {

            if ($groups_id != 0) {

              $OSCOM_PDO->save('products_groups', [
                                                    'customers_group_id' =>  (int)$groups_id,
                                                    'customers_group_price' => $newprice,
                                                    'products_id' =>  (int)$Qpricek->valueInt('products_id'),
                                                    'products_price' => $pricek
                                                   ]
                              );
//***************************************
// odoo web service
//***************************************
              if (ODOO_ACTIVATE_WEB_SERVICE == 'true' &&  ODOO_ACTIVATE_CUSTOMERS_GROUP == 'true')  {
                require('ext/odoo_xmlrpc/xml_rpc_admin_customers_group.php');
              }
            }

//***************************************
// End odoo web service
//***************************************

          } else {

            if ($groups_id != 0) {

              $Qupdate = $OSCOM_PDO->prepare('update :table_products_groups
                                              set customers_group_price = :customers_group_price
                                              where customers_group_id = :customers_group_id
                                              and products_id = :products_id
                                            ');
              $Qupdate->bindValue(':customers_group_price', $newprice);
              $Qupdate->bindInt(':customers_group_id', (int)$groups_id );
              $Qupdate->bindInt(':products_id', (int)$Qpricek->valueInt('products_id') );
              $Qupdate->execute();

//***************************************
// odoo web service
//***************************************
              if (ODOO_ACTIVATE_WEB_SERVICE == 'true' &&  ODOO_ACTIVATE_CUSTOMERS_GROUP == 'true')  {
                require('ext/odoo_xmlrpc/xml_rpc_admin_customers_group.php');
              }
//***************************************
// End odoo web service
//***************************************
            }
          }

          $count_update++;
          $item_updated[$id] = 'updated';
        } // end while
      }

      $count_item = array_count_values($item_updated);
      if ($count_item['updated'] > 0) $OSCOM_MessageStack->add(TEXT_PRODUCTS_UPDATED . ' ' . $count_update . ' ' .  TEXT_QTY_UPDATED . ' ' . $customers_group['customers_group_name'], 'success');

      break;
    }
  }

  require('includes/header.php');
?>

<script type="text/javascript" src="ext/javascript/colorpicker/jscolor.js"></script>

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<!-- //################################################################################################################ -->
<!-- //                                               EDITION D'UN GROUPE                                               -->
<!-- //################################################################################################################ -->
<?php
  if ($_GET['action'] == 'edit') {
// Requetes SQL pour recuperer toutes les informations du groupe client
    $Qcustomers_group = $OSCOM_PDO->prepare("select * 
                                             from :table_customers_groups c  
                                             where c.customers_group_id = :customers_group_id
                                             ORDER BY c.customers_group_id
                                         ");
    $Qcustomers_group->bindInt(':customers_group_id', (int)$_GET['cID'] );
    $Qcustomers_group->execute();

    $customers_group = $Qcustomers_group->fetch();

    $cInfo = new objectInfo($customers_group);
    
    $customers_groups_id = $customers_group['customers_group_id'];
    $customers_groups_name = $customers_group['customers_group_name'];
    $customers_groups_discount = $customers_group['customers_group_discount'];
    $color_bar = $customers_group['color_bar'];  
    $customers_group_quantity_default = $customers_group['customers_group_quantity_default'];  

// Recherche des modules de paiement en production
    $payments_unallowed = explode (",",$cInfo->group_payment_unallowed);
    $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
    $module_key = 'MODULE_PAYMENT_INSTALLED';

    $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_array = array();
    if ($dir = @dir($module_directory)) {
      while ($file = $dir->read()) {
        if (!is_dir($module_directory . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $directory_array[] = $file;
          }
        }
      }
      sort($directory_array);
      $dir->close();
    } // END $dir

// Recherche des modules de livraison en production
    $shipping_unallowed = explode (",",$cInfo->group_shipping_unallowed);
    $module_shipping_directory = DIR_FS_CATALOG_MODULES . 'shipping/';
    $module_shipping_key = 'MODULE_SHIPPING_INSTALLED';

    $file_shipping_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_shipping_array = array();
    if ($dir_shipping = @dir($module_shipping_directory)) {
      while ($file_shipping = $dir_shipping->read()) {
        if (!is_dir($module_shipping_directory . $file_shipping)) {
          if (substr($file_shipping, strrpos($file_shipping, '.')) == $file_shipping_extension) {
            $directory_shipping_array[] = $file_shipping;
          }
        }
      }
      sort($directory_shipping_array);
      $dir_shipping->close();
    } // end $dir_shipping

// Affichage des prix de la boutique pour le groupe avec une taxe inclus ou exclus
    if (!isset($cInfo->group_tax)) $cInfo->group_tax = 'true';
      switch ($cInfo->group_tax) {
        case 'false': 
          $group_tax_inc = false; 
          $group_tax_ex = true; 
        break;
        case 'true':
        default: 
          $group_tax_inc = true; 
          $group_tax_ex = false;
    } // end !isset

// Affiche la case coche par defaut pour le mode de facturation utilisee avec taxe ou non
    if (!isset($cInfo->group_order_taxe)) $cInfo->group_order_taxe = '0';
    switch ($cInfo->group_order_taxe) {
      case '0': 
        $status_order_taxe = true; 
        $status_order_no_taxe = false; 
      break;
      case '1': 
        $status_order_taxe = false; 
        $status_order_no_taxe = true; 
      break;
      default: 
        $status_order_taxe = true; 
        $status_order_no_taxe = false;
    } // end !isset

?>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <div class="adminTitle">
        <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/group_client_editer.gif', HEADING_TITLE_EDIT, '40', '40'); ?></span>
        <span class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_EDIT; ?><span>

<!--  Mise a jour action = Update -->
        <span class="pull-right"><?php echo osc_draw_form('customers_group', 'customers_groups.php', osc_get_all_get_params(array('action')) . 'action=update', 'post', 'onSubmit="return check_form();"'); ?></span>
        <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
        <span class="pull-right"><?php echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID'))) .'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
      </div>
<?php
    if ($error == true) {
?>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <tr>
        <td class="messageStackError" height="20" colspan="3">
<?php
      if ($error == 'categorie') {
        echo '&nbsp;' . osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING) . '&nbsp;' . ENTRY_GROUPS_CATEGORIE_ERROR;
      } else if ($error == 'name') {
        echo '&nbsp;' . osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING) . '&nbsp;' . ENTRY_GROUPS_NAME_ERROR;
      }
?>
        </td>
      </tr>
<?php
    } // end $error
?>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<!-- // FORM main screen -->
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><?php echo '<a href="' . substr(osc_href_link('customers_groups.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' . TAB_GENERAL . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('customers_groups.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' . TAB_ORDERS; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('customers_groups.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' . TAB_SHIPPING; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('customers_groups.php', osc_get_all_get_params()), strlen($base_url)) . '#tab4" role="tab" data-toggle="tab">' . TAB_CATEGORIE; ?></a></li>
            </ul>

            <div class="tabsClicShopping">
              <div class="tab-content">
<!-- //################################################################################################################ -->
<!--          ONGLET General informations groupe          //-->
<!-- //################################################################################################################ -->
                <div class="tab-pane active" id="tab1">
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                       <td class="mainTitle"><?php echo TITLE_GROUP_NAME; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                          <td class="main"><?php echo ENTRY_GROUPS_NAME; ?></td>
                          <td class="main">
<?php
    if ($error == "name") {
      echo osc_draw_input_field('customers_group_name', '', 'maxlength="32" style="border: 2px solid #FF0000"', true);
    } else {
      echo osc_draw_input_field('customers_group_name', $cInfo->customers_group_name, 'required aria-required="true" id="customers_group_name" maxlength="32"', true);
    }
?>
                          </td>
                        </tr>
                        <tr>
                          <td class="main"><?php echo ENTRY_COLOR_BAR; ?></td>
                          <td><table width="50%" align="left">
                            <tr>
                              <td class="main"><?php echo osc_draw_input_field('color_bar', $cInfo->color_bar, 'size=12 class="color {pickerPosition:\'right\'}"', false); ?></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                    </tr>
                    <tr>
                      <td class="mainTitle"><?php echo TITLE_DEFAULT_DISCOUNT; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                          <td class="main"><?php echo ENTRY_VALEUR_DISCOUNT; ?></td>
                          <td class="main"><?php echo osc_draw_input_field('customers_group_discount', $cInfo->customers_group_discount, 'required aria-required="true" id="customers_group_discount" maxlength="5" size=5', false); ?></td>
                          <td class="main"><?php echo ENTRY_VALEUR_DISCOUNT_NOTE; ?></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                    </tr>
                    <tr>
                      <td class="mainTitle"><?php echo TITLE_DEFAULT_QUANTITY; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                          <td class="main"><?php echo ENTRY_TEXT_QUANTITY_DEFAULT; ?></td>
                          <td class="main"><?php echo osc_draw_input_field('customers_group_quantity_default', $cInfo->customers_group_quantity_default, 'maxlength="5" size=5', false); ?></td>
                          <td class="main"><?php echo ENTRY_TEXT_QUANTITY_NOTE; ?></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                    </tr>
                    <tr>
                      <td class="mainTitle"><?php echo TITLE_GROUP_TAX; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                          <td class="main"><?php echo ENTRY_GROUP_TAX; ?></td>
                          <td class="main"><?php echo osc_draw_radio_field('group_tax', 'true', $group_tax_inc) . '&nbsp;' . TEXT_GROUP_TAX_INC . '&nbsp;' . osc_draw_radio_field('group_tax', 'false', $group_tax_ex). '&nbsp;' . TEXT_GROUP_TAX_EX; ?></td>
                          <td class="main"><?php echo ENTRY_GROUP_TAX_NOTE; ?></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                    <tr>
                      <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', AIDE_TITLE_ONGLET_GENERAL); ?></td>
                      <td class="main"><strong><?php echo '&nbsp;' . AIDE_TITLE_ONGLET_GENERAL; ?></strong></td>
                    </tr>
                  </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo AIDE_GROUP_TAX; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                </div>


<!-- //################################################################################################################ -->
<!--          ONGLET Facturation          //-->
<!-- //################################################################################################################ -->
              <div class="tab-pane" id="tab2">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_ORDER_CUSTOMER_DEFAULT; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main">
<?php
    if ($error == true) {
      if ($cInfo->group_order_taxe == '0') echo OPTIONS_ORDER_TAXE;
      if ($cInfo->group_order_taxe == '1') echo OPTIONS_ORDER_NO_TAXE;
      echo osc_draw_hidden_field('group_order_taxe');
    } else {
      echo osc_draw_radio_field('group_order_taxe', '0', $status_order_taxe) . '&nbsp;' . OPTIONS_ORDER_TAXE . '<br />' . osc_draw_radio_field('group_order_taxe', '1', $status_order_no_taxe) . '&nbsp;' . OPTIONS_ORDER_NO_TAXE . ENTRY_GROUP_TAX_NOTE;
    }
?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_GROUP_PAIEMENT_DEFAULT; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    $module_active = explode (";",MODULE_PAYMENT_INSTALLED);
    $installed_modules = array();
    for ($i = 0, $n = sizeof($directory_array); $i < $n; $i++) {
      $file = $directory_array[$i];
      if (in_array ($directory_array[$i], $module_active)) {
        include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/payment/' . $file);
        include($module_directory . $file);
        $class = substr($file, 0, strrpos($file, '.'));
        if (osc_class_exists($class)) {
          $module = new $class;
          if ($module->check() > 0) {
             $installed_modules[] = $file;
          }
        }
?>
                      <tr>
                        <td class="main"><?php echo osc_draw_checkbox_field('payment_unallowed[' . $i . ']', $module->code , (in_array ($module->code, $payments_unallowed)) ?  1 : 0); ?> </td>
                        <td class="main"><?php echo $module->title; ?></td>
                      </tr>
<?php
        if ($_POST['payment_unallowed'][$i]) {
          $_POST['group_payment_unallowed'] .= $_POST['payment_unallowed'][$i] . ',';
        }
      } // end in_array
    } // end for
?>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', AIDE_TITLE_ONGLET_FACTURATION); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . AIDE_TITLE_ONGLET_FACTURATION; ?></strong></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo AIDE_ORDER_TAX; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Livraison          //-->
<!-- //################################################################################################################ -->
              <div class="tab-pane" id="tab3">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_GROUP_SHIPPING_DEFAULT; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
        $module_shipping_active = explode (";",MODULE_SHIPPING_INSTALLED);
        $installed_shipping_modules = array();

        for ($i = 0, $n = sizeof($directory_shipping_array); $i < $n; $i++) {

          $file_shipping = $directory_shipping_array[$i];

          if (in_array ($directory_shipping_array[$i], $module_shipping_active)) {

            include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/shipping/' . $file_shipping);
            include($module_shipping_directory . $file_shipping);

            $class_shipping = substr($file_shipping, 0, strrpos($file_shipping, '.'));
            if (osc_class_exists($class_shipping)) {

              $module_shipping = new $class_shipping;


              if ($module_shipping->check() > 0) {
                $installed_modules_shipping[] = $file_shipping;
              }
            }
?>
                      <tr>
                        <td class="main"><?php echo osc_draw_checkbox_field('shipping_unallowed[' . $i . ']', $module_shipping->code , (in_array ($module_shipping->code, $shipping_unallowed)) ?  1 : 0); ?></td>
                        <td class="main"><?php echo $module_shipping->title; ?></td>
                      </tr>
<?php

            if ($_POST['shipping_unallowed'][$i]) {
              $_POST['group_shipping_unallowed'] .= $_POST['shipping_unallowed'][$i] . ',';
            }
          } //end in_array
        }// end for
?>
                    </table></td>
                  </tr>
                </table>
              </div>
            </form>
<!-- //END FORM -->
<!-- //################################################################################################################ -->
<!--          ONGLET General discount des categories          //-->
<!-- //################################################################################################################ -->
              <div class="tab-pane" id="tab4">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_CATEGORIES_DISCOUNT; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php


    echo osc_draw_form('newdiscountconfirm', 'customers_groups.php', osc_get_all_get_params(array('action')) . 'action=newdiscountconfirm#tab4', 'post', '') . osc_draw_hidden_field('cID', $_GET['cID']);
?>
                      <tr>
                        <td class="main"><?php echo ENTRY_GROUP_CATEGORIES; ?></td>
                        <td class="main"><?php echo osc_draw_pull_down_menu('categories_id', osc_get_category_tree()); ?></td>
                      </tr>
                      <tr>
                        <td class="main"><?php echo ENTRY_VALEUR_DISCOUNT; ?></td>
                        <td><table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td class="main"><?php echo osc_draw_input_field('discount', '0', 'maxlength="5" size="5"', false); ?></td>
                            <td class="main"><?php echo ENTRY_VALEUR_DISCOUNT_NOTE; ?></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                    <td class="main" align="right"><?php echo  osc_image_submit('button_insert.gif', IMAGE_INSERT);  ?></td>
                    </form>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                      <tr>
                        <td class="mainTitle"><?php echo TITLE_LIST_CATEGORIES_DISCOUNT; ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td><table border=0 cellspacing=0 cellpadding=0 width="100%">
                              <tr>
                                <td class="formAreaTitle"><?PHP echo TEXT_CATEGORIES; ?></td>
                                <td align="center" class="formAreaTitle"><?PHP echo TABLE_HEADING_DISCOUNT; ?></td>
                                <td class="formAreaTitle" align="right"><?PHP echo TABLE_HEADING_ACTION; ?></td>
                              </tr>
<?php
    $index = 0;

    $QgroupToCategories = $OSCOM_PDO->prepare('select distinct c.discount,
                                                                 c.categories_id,
                                                                 c.customers_group_id,
                                                                 g.categories_name,
                                                                 g.language_id,
                                                                 f.parent_id
                                                from :table_groups_to_categories  c,
                                                     categories_description g,
                                                     categories f
                                                where c.customers_group_id = :customers_group_id
                                                and c.categories_id = g.categories_id
                                                and c.categories_id = f.categories_id
                                                and g.language_id = :language_id
                                                order by g.categories_name
                                        ');
    $QgroupToCategories->bindInt(':customers_group_id', (int)$_GET['cID']);
    $QgroupToCategories->bindInt(':language_id', (int)$_SESSION['languages_id']);

    $QgroupToCategories->execute();

    while ($group_to_categories =  $QgroupToCategories->fetch() ) {
      if ($index == 0){
        $index = 1;
        $background = 'bgcolor="white"';
      } else {
        $index = 0;
        $background = '';
      }

      $Qparents = $OSCOM_PDO->prepare('select categories_name
                                       from :table_categories_description
                                       where categories_id = :categories_id
                                     ');
      $Qparents->bindInt(':categories_id', (int)$group_to_categories['parent_id'] );
      $Qparents->execute();

      $parents = $Qparents->fetch();

      if($parents['categories_name'] != null) {
        $add = $parents['categories_name'] . " - ";
      } else {
        $add = '';
      }


      echo osc_draw_form('form', 'customers_groups.php', osc_get_all_get_params(array('action')) . 'action=updatediscount#tab4', 'post', '');


?>
                              <tr class="<?php echo $background; ?>">
                                <?php echo osc_draw_hidden_field('catID', $group_to_categories['categories_id']); ?>
                                <td class="main"> <?php echo $group_to_categories['categories_name'];?></td>
                                <td align="center" class="main"><?php echo osc_draw_input_field('upddiscount', $group_to_categories['discount'], 'maxlength="5" size="5"', false); ?></td>
                                <td class="main" align="right"><?php echo  osc_image_submit('button_mini_update.gif', IMAGE_UPDATE); ?></form>
                                  <?php echo osc_draw_form('customers_groups', 'customers_groups.php', osc_get_all_get_params(array('action')) . 'action=deletediscount&catID='. (int)$group_to_categories['categories_id'].'#tab4', 'post', ''); ?>
                                  <?php echo osc_image_submit('button_delete.gif', IMAGE_DELETE); ?>
                                </td>
                              </tr>
                            </form>
<?php 
    } // end while
?>                      
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                  </td></table>
                 </tr>
              </td></table>
            </div>
          </div>
        </div>
      </div>
    </td>
  </tr>
<?php
  } else if ($_GET['action'] == 'newdiscount') {
?>
<!-- //################################################################################################################ -->
<!-- //                                           CREATION D'UN NOUVEAU GROUPE                                          -->
<!-- //################################################################################################################ -->
  <tr>
    <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
      <tr>
        <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/group_client_nouveau.gif', HEADING_TITLE, '40', '40'); ?></td>
        <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
        <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">
          <tr><?php echo osc_draw_form('customers', 'customers_groups.php', osc_get_all_get_params(array('action')) . 'action=newconfirm', 'post', 'onSubmit="return check_form();"'); ?>
            <td><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
            <td><?php echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID'))) .'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
  </tr>
<?php
  } else if ($_GET['action'] == 'new') {
?>
  <tr>
    <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
      <tr>
        <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/group_client_nouveau.gif', HEADING_TITLE_NEW, '40', '40'); ?></td>
        <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_NEW; ?></td>
        <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">
          <tr><?php echo osc_draw_form('customers', 'customers_groups.php', osc_get_all_get_params(array('action')) . 'action=newconfirm', 'post', 'onSubmit="return check_form();"'); ?>
            <td><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
            <td><?php echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('action','cID'))) .'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<?php
  if ($error == true) {
?>
  <tr>
    <td colspan="3"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
  </tr>
  <tr>
    <td class="messageStackError" height="20" colspan="3">
<?php
    if ($error == 'categorie') {
      echo '&nbsp;' . osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING) . '&nbsp;' . ENTRY_GROUPS_CATEGORIE_ERROR;
    } else if ($error == 'name') {
      echo '&nbsp;' . osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING) . '&nbsp;' . ENTRY_GROUPS_NAME_ERROR;
    }
?>
    </td>
  </tr>
<?php
  }
?>
  <tr>
    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
  </tr>
  <tr>
    <td>
<!-- //################################################################################################################ -->
<!--          ONGLET General informations groupe          //-->
<!-- //################################################################################################################ -->
      <div>
        <ul class="nav nav-tabs" role="tablist"  id="myTab">
          <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
          <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_ORDERS; ?></a></li>
          <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_SHIPPING; ?></a></li>
          <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_CATEGORIE; ?></a></li>
        </ul>

        <div class="tabsClicShopping">
          <div class="tab-content">
<!-- //################################################################################################################ -->
<!--          ONGLET General informations groupe          //-->
<!-- //################################################################################################################ -->
            <div class="tab-pane active" id="tab1">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="mainTitle"><?php echo TITLE_GROUP_NAME; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo ENTRY_GROUPS_NAME; ?></td>
                      <td class="main">
<?php

  if ($error == "name") {
    echo osc_draw_input_field('customers_group_name', '', 'maxlength="32" style="border: 2px solid #FF0000"', true) . '&nbsp;' . (osc_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement"></span>': '');
  } else {
    echo osc_draw_input_field('customers_group_name', '', 'maxlength="32"', true) . '&nbsp;' . (osc_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement"></span>': '');
  }
?>
                      </td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo ENTRY_COLOR_BAR; ?></td>
                      <td><table width="50%" align="left">
                        <tr>
                          <td align="center">
                            <?php echo osc_draw_input_field('color_bar', $cInfo->color_bar, 'size=12 class="color {pickerPosition:\'right\'}"', false); ?></td>
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="mainTitle"><?php echo TITLE_DEFAULT_DISCOUNT; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo ENTRY_VALEUR_DISCOUNT; ?></td>
                      <td class="main"><?php echo osc_draw_input_field('customers_group_discount', '', 'maxlength="5" size=5', false); ?></td>
                      <td class="main"><?php echo ENTRY_VALEUR_DISCOUNT_NOTE; ?></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="mainTitle"><?php echo TITLE_DEFAULT_QUANTITY; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo ENTRY_TEXT_QUANTITY_DEFAULT; ?></td>
                        <td class="main"><?php echo osc_draw_input_field('customers_group_quantity_default', $cInfo->customers_group_quantity_default, 'maxlength="5" size=5', false); ?></td>
                        <td class="main"><?php echo ENTRY_TEXT_QUANTITY_NOTE; ?></td>
                      </tr>
                    </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="mainTitle"><?php echo TITLE_GROUP_TAX; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo ENTRY_GROUP_TAX; ?></td>
                      <td class="main"><?php echo osc_draw_radio_field('group_tax', 'true', true) . '&nbsp;' . TEXT_GROUP_TAX_INC . '&nbsp;' . osc_draw_radio_field('group_tax', 'false'). '&nbsp;' . TEXT_GROUP_TAX_EX; ?></td>
                      <td class="main"><?php echo ENTRY_GROUP_TAX_NOTE; ?></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', AIDE_TITLE_ONGLET_GENERAL); ?></td>
                      <td class="main"><strong><?php echo '&nbsp;' . AIDE_TITLE_ONGLET_GENERAL; ?></strong></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                      <td class="main"><?php echo AIDE_GROUP_TAX; ?></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
            </div>
          
<!-- //################################################################################################################ -->
<!--          ONGLET Facturation          //-->
<!-- //################################################################################################################ -->
<?php
// Recherche des modules de Paiement true
    $module_directory = DIR_FS_CATALOG_MODULES . 'payment/';
    $module_key = 'MODULE_PAYMENT_INSTALLED';

    $file_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_array = array();
    if ($dir = @dir($module_directory)) {
      while ($file = $dir->read()) {
        if (!is_dir($module_directory . $file)) {
          if (substr($file, strrpos($file, '.')) == $file_extension) {
            $directory_array[] = $file;
          }
        }
      }
      sort($directory_array);
      $dir->close();
    }
?>
            <div class="tab-pane" id="tab2">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td class="mainTitle"><?php echo TITLE_ORDER_CUSTOMER_DEFAULT; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo osc_draw_radio_field('group_order_taxe', '0', true) . '&nbsp;' . OPTIONS_ORDER_TAXE . '<br />' . osc_draw_radio_field('group_order_taxe', '1') . '&nbsp;' . OPTIONS_ORDER_NO_TAXE; ?></td>
                    </tr>
                  </table></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <td class="mainTitle"><?php echo TITLE_GROUP_PAIEMENT_DEFAULT; ?></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
<?php
  $module_active = explode (";",MODULE_PAYMENT_INSTALLED);
  $installed_modules = array();
  for ($i = 0, $n = sizeof($directory_array); $i < $n; $i++) {
    $file = $directory_array[$i];
    if (in_array ($directory_array[$i], $module_active)) {
      include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/payment/' . $file);
      include($module_directory . $file);
      $class = substr($file, 0, strrpos($file, '.'));
      if (osc_class_exists($class)) {
        $module = new $class;
        if ($module->check() > 0) {
           $installed_modules[] = $file;
        }
    }
?>
                    <tr>
                      <td class="main"><?php echo osc_draw_checkbox_field('payment_unallowed[' . $i . ']', $module->code , 1); ?></td>
                      <td class="main"><?php echo $module->title; ?></td>
                    </tr>
<?php
      if ($_POST['payment_unallowed'][$i]) {
        $_POST['group_payment_unallowed'] .= $_POST['payment_unallowed'][$i] . ',';
      }
    }
  }
?>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', AIDE_TITLE_ONGLET_FACTURATION); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . AIDE_TITLE_ONGLET_FACTURATION; ?></strong></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo AIDE_ORDER_TAX; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Livraison          //-->
<!-- //################################################################################################################ -->
<?php
// Recherche des modules de Livraison true
    $module_shipping_directory = DIR_FS_CATALOG_MODULES . 'shipping/';
    $module_shipping_key = 'MODULE_SHIPPING_INSTALLED';

    $file_shipping_extension = substr($PHP_SELF, strrpos($PHP_SELF, '.'));
    $directory_shipping_array = array();
    if ($dir_shipping = @dir($module_shipping_directory)) {
      while ($file_shipping = $dir_shipping->read()) {
        if (!is_dir($module_shipping_directory . $file_shipping)) {
          if (substr($file_shipping, strrpos($file_shipping, '.')) == $file_shipping_extension) {
            $directory_shipping_array[] = $file_shipping;
          }
        }
      }
      sort($directory_shipping_array);
      $dir_shipping->close();
    }
?>
              <div class="tab-pane" id="tab3">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TITLE_GROUP_SHIPPING_DEFAULT; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
  $module_shipping_active = explode (";",MODULE_SHIPPING_INSTALLED);
  $installed_shipping_modules = array();
  for ($i = 0, $n = sizeof($directory_shipping_array); $i < $n; $i++) {
    $file_shipping = $directory_shipping_array[$i];
    if (in_array ($directory_shipping_array[$i], $module_shipping_active)) {
      include(DIR_FS_CATALOG_LANGUAGES . $_SESSION['language'] . '/modules/shipping/' . $file_shipping);
      include($module_shipping_directory . $file_shipping);
      $class_shipping = substr($file_shipping, 0, strrpos($file_shipping, '.'));
      if (osc_class_exists($class_shipping)) {
        $module_shipping = new $class_shipping;
        if ($module_shipping->check() > 0) {
           $installed_modules_shipping[] = $file_shipping;
        }
    }
?>
                      <tr>
                        <td class="main"><?php echo osc_draw_checkbox_field('shipping_unallowed[' . $i . ']', $module_shipping->code , 1); ?></td>
                        <td class="main"><?php echo $module_shipping->title; ?></td>
                      </tr>
<?php
      if ($_POST['shipping_unallowed'][$i]) {
        $_POST['group_shipping_unallowed'] .= $_POST['shipping_unallowed'][$i] . ',';
      }
    }
  }
?>
                    </table></td>
                  </tr>
                </table>
              </div>
              </form>
<!-- //END FORM -->
<!-- //################################################################################################################ -->
<!--          ONGLET General discount des categories          //-->
<!-- //################################################################################################################ -->
              <div class="tab-pane" id="tab4">
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminform">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td align="center" height="50" class="main" style="color:#FF0000;"><?php echo '<strong>' . TEXT_CATEGORIES_NO_NEW . '</strong>'; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
            </div>
        </div>
      </div>
    </td>
  </tr>
<?php
  } else {
?>
<!-- //################################################################################################################ -->
<!-- //                                           LISTE DES GROUPES EXISTANTS                                           -->
<!-- //################################################################################################################ -->
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="adminTitle">
                <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/group_client.gif', HEADING_TITLE, '40', '40'); ?></span>
                <span class="col-md-7 pageHeading" width="60%"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
                <span class="col-md-2" style="text-align:right;">
                  <div class="form-group">
                    <div class="controls">
<?php
    echo osc_draw_form('search', 'customers_groups.php', '', 'get');
    echo  osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');
?>
                      </form>
                    </div>
                  </div>
                </span>
                <span class="col-md-2" style="text-align:right">
                <span><?php echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . '&action=new') . '">' . osc_image_button('button_new_group.gif', IMAGE_NEW_GROUP) . '</a>' ?></span>
<?php
    if (osc_not_null($_GET['search'])) {
?>
                <span><?php echo '<a href="' . osc_href_link('customers_groups.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></span>
<?php
    }
?>
                </span>
              </div>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
<?php
  switch ($listing) {
    case "id-asc":
      $order = "g.customers_group_id";
      break;
    case "group":
      $order = "g.customers_group_name";
      break;
    case "group-desc":
      $order = "g.customers_group_name DESC";
      break;
    case "discount":
      $order = "g.customers_group_discount";
      break;
    case "discount-desc":
      $order = "g.customers_group_discount DESC";
      break;
    default:
      $order = "g.customers_group_id DESC";
  }
?>
                  <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
                    <tr class="dataTableHeadingRow">
                      <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                      <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_COLOR; ?></td>
                      <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_QUANTITY_DEFAULT; ?></td>
                      <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_DISCOUNT; ?></td>
                      <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                    </tr>
<?php
    $search = '';
    if ( ($_GET['search']) && (osc_not_null($_GET['search'])) ) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "where g.customers_group_name like '%" . $keywords . "%'";
    }
    
    $customers_group_query_raw = "select * 
                                 from customers_groups g
                                      " . $search . " 
                                 order by $order";
    
    $customers_group_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $customers_group_query_raw, $customers_group_query_numrows);
    $customers_group_query = osc_db_query($customers_group_query_raw);

    while ($customers_group = osc_db_fetch_array($customers_group_query)) {

     $Qinfo = $OSCOM_PDO->prepare("select customers_info_date_account_created as date_account_created, 
                                         customers_info_date_account_last_modified as date_account_last_modified, 
                                         customers_info_date_of_last_logon as date_last_logon, 
                                         customers_info_number_of_logons as number_of_logons 
                                  from :table_customers_info
                                  where customers_info_id = :customers_info_id
                                  ");
      $Qinfo->bindInt(':customers_info_id', (int)$customers_group['customers_group_id'] );
      $Qinfo->execute();

      $info = $Qinfo->fetch();

      if (((!$_GET['cID']) || (@$_GET['cID'] == $customers_group['customers_group_id'])) && (!$cInfo)) {


        $cInfo = new objectInfo($customers_group);//$cInfo_array);
      }

      if ( (is_object($cInfo)) && ($customers_group['customers_group_id'] == $cInfo->customers_group_id) ) {
        echo '      <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_group_id . '&action=edit') . '\'">' . "\n";
      } else {
        echo '      <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID')) . 'cID=' . $customers_group['customers_group_id']) . '\'">' . "\n";
      }
?>
                      <td class="dataTableContent"><?php echo $customers_group['customers_group_name']; ?></td>
                      <td class="dataTableContent" align="center"><table width="15" cellspacing="0" cellpadding="0" border="0">                        
                        <td bgcolor="<?php echo $customers_group['color_bar']; ?>">&nbsp;</td>
                      </table></td>
                      <td class="dataTableContent" align="right"><?php echo $customers_group['customers_group_quantity_default']; ?></td>
                      <td class="dataTableContent" align="right"><?php echo $customers_group['customers_group_discount']; ?>%</td>
                      <td class="dataTableContent" align="right">
<?php
    echo '<a href="' . osc_href_link('customers_groups.php', 'page=' . $_GET['page'] . '&cID=' . $customers_group['customers_group_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    if ( (is_object($cInfo)) && ($customers_group['customers_group_id'] == $cInfo->customers_group_id) ) {
      echo '<a href="' . osc_href_link('customers_groups.php', 'page=' . $_GET['page'] . '&cID=' . $customers_group['customers_group_id'] . '&action=updateallprice') . '">' . osc_image(DIR_WS_ICONS . 'actualiser.gif', IMAGE_UPDATEALLPRICE) . '</a>';
    }
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_group['customers_group_id'] . '&action=confirm') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    if ( (is_object($cInfo)) && ($customers_group['customers_group_id'] == $cInfo->customers_group_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID')) . 'cID=' . $customers_group['customers_group_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?></td>
                    </tr>
<?php
    }
?>
                    <tr>
                      <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                        <tr>
                          <td class="smallText" valign="top"><?php echo $customers_group_split->display_count($customers_group_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
                          <td class="smallText" align="right"><?php echo $customers_group_split->display_links($customers_group_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], osc_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
<?php
  $heading = array();
  $contents = array();
  switch ($_GET['action']) {
    case 'confirm':
      if ($_GET['cID'] != 1) {
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CUSTOMER . '</strong>');
        $contents = array('form' => osc_draw_form('customers_group', 'customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_group_id . '&action=deleteconfirm'));
        $contents[] = array('text' => TEXT_DELETE_INTRO . '<br /><br /><strong>' . $cInfo->customers_group_name . ' </strong>');
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('customers_groups.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_group_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      } else {
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CUSTOMER . '</strong>');
        $contents[] = array('text' => TEXT_IMPOSSIBLE_DELETE .'<br /><br /><strong>' . $cInfo->customers_group_name . ' </strong>');
      }
      break;
    default:
    break;
  }

    if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);
      echo '            </td>' . "\n";
    }
?>
                </tr>
              </table></td>
            </div>
<?php
  }
?>
          </table></td>
        </tr>
      </table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
