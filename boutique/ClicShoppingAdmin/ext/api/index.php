<?php
/**
 * index.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: index.php 
 */
// Redirection vers la page d'acceuil si aucune page d'introduction existe
   header("Location: http://".$_SERVER['HTTP_HOST']."/");
?>
