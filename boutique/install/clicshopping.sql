
CREATE TABLE IF NOT EXISTS `action_recorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `success` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_action_recorder_module` (`module`),
  KEY `idx_action_recorder_user_id` (`user_id`),
  KEY `idx_action_recorder_identifier` (`identifier`),
  KEY `idx_action_recorder_date_added` (`date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `address_book` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_gender` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `entry_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_siret` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `entry_ape` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `entry_tva_intracom` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `entry_cf` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `entry_piva` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `entry_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  `entry_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `address_format` (
  `address_format_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_format` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address_summary` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_format_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(1, '$firstname $lastname$cr$streets$cr$city, $postcode$cr$statecomma$country', '$city / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(2, '$firstname $lastname$cr$streets$cr$city, $state    $postcode$cr$country', '$city, $state / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(3, '$firstname $lastname$cr$streets$cr$city$cr$postcode - $statecomma$country', '$state / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(4, '$firstname $lastname$cr$streets$cr$city ($postcode)$cr$country', '$postcode / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(5, '$firstname $lastname$cr$streets$cr$postcode $city$cr$country', '$city / $country');
INSERT INTO `address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES(6, '$firstname $lastname$cr$streets$cr$postcode $city$cr$statecomma$country', '$city / $country');

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `atos_informations` (
  `orders_id` int(11) NOT NULL,
  `entete` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `merchant_id` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `payment_means` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `origin_amount` float NOT NULL,
  `amount` float NOT NULL,
  `currency_code` int(11) NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_time` time NOT NULL,
  `card_validity` int(11) NOT NULL,
  `card_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `response_code` int(11) NOT NULL,
  `cvv_response_code` int(11) NOT NULL,
  `complementary_code` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `certificate` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `authorization_id` int(11) NOT NULL,
  `capture_date` int(11) NOT NULL,
  `transaction_status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `return_context` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `autoresponse_status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `atos_order_id` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `atos_customer_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `customer_ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `account_serial` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_condition` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `cavv_ucaf` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `complementary_info` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `bank_response_code` int(11) NOT NULL,
  PRIMARY KEY (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `banners_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_target` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `banners_html_text` text COLLATE utf8_unicode_ci,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `banners_title_admin` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`banners_id`),
  KEY `idx_banners_group` (`banners_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL DEFAULT '0',
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`banners_history_id`),
  KEY `idx_banners_history_banners_id` (`banners_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `blog_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_categories_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_categories_id`),
  KEY `idx_blog_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_categories_description` (
  `blog_categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `blog_categories_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blog_categories_description` text COLLATE utf8_unicode_ci,
  `blog_categories_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_categories_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_categories_head_keywords_tag` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`blog_categories_id`,`language_id`),
  KEY `idx_blog_categories_name` (`blog_categories_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `blog_content` (
  `blog_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_content_date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `blog_content_last_modified` datetime DEFAULT NULL,
  `blog_content_date_available` datetime DEFAULT NULL,
  `blog_content_status` tinyint(1) NOT NULL DEFAULT '0',
  `blog_content_archive` tinyint(1) NOT NULL DEFAULT '0',
  `admin_user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blog_content_sort_order` int(11) NOT NULL DEFAULT '0',
  `blog_content_author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_content_id`),
  KEY `idx_blog_content_date_added` (`blog_content_date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_content_description` (
  `blog_content_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `blog_content_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blog_content_description` text COLLATE utf8_unicode_ci,
  `blog_content_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_viewed` int(5) DEFAULT '0',
  `blog_content_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_head_keywords_tag` text COLLATE utf8_unicode_ci,
  `blog_content_head_tag_product` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_head_tag_blog` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blog_content_description_summary` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`blog_content_id`,`language_id`),
  KEY `blog_content_name` (`blog_content_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `blog_content_to_categories` (
  `blog_content_id` int(11) NOT NULL DEFAULT '0',
  `blog_categories_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`blog_content_id`,`blog_categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `cache` (
  `cache_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cache_language_id` tinyint(1) NOT NULL DEFAULT '0',
  `cache_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cache_data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cache_global` tinyint(1) NOT NULL DEFAULT '1',
  `cache_gzip` tinyint(1) NOT NULL DEFAULT '1',
  `cache_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'RETURN',
  `cache_date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `cache_expires` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`cache_id`,`cache_language_id`),
  KEY `cache_id` (`cache_id`),
  KEY `cache_language_id` (`cache_language_id`),
  KEY `cache_global` (`cache_global`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `virtual_categories` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`categories_id`),
  KEY `idx_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `categories_description` (
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `categories_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `categories_description` text COLLATE utf8_unicode_ci,
  `categories_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categories_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categories_head_keywords_tag` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`categories_id`,`language_id`),
  KEY `idx_categories_name` (`categories_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `cmcic_reference` (
  `ref_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `cmcic_response` (
  `resp_id` int(11) NOT NULL AUTO_INCREMENT,
  `MAC` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TPE` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `montant` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `texte_libre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_retour` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `retourPLUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`resp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `configuration` (
  `configuration_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration_value` text COLLATE utf8_unicode_ci NOT NULL,
  `configuration_description` text COLLATE utf8_unicode_ci NOT NULL,
  `configuration_group_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(5) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `use_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `set_function` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=695 ;

INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(1, 'Nom de la boutique', 'STORE_NAME', 'ClicShopping', 'Veuillez indiquer le nom de votre boutique.<br><br><font color="#FF0000"><b>Note :</b> Ce nom s''affichera dans le contenu des e-mails</font><br>', 1, 1, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(2, 'Responsable de la boutique', 'STORE_OWNER', '', 'Veuillez indiquer le nom du responsable de la boutique.<br>', 1, 2, '2008-09-15 09:39:27', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(3, 'Adresse  e-Mail de la boutique', 'STORE_OWNER_EMAIL_ADDRESS', '', 'Veuillez indiquer l''adresse email qui sera utilis&eacute;e par la boutique lors des envois des emails.', 1, 3, '2007-06-02 15:40:42', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(4, 'Champ ''From'' d''un email envoy&eacute;', 'EMAIL_FROM', '', 'Champ utilis&eacute; dans les ent&ecirc;tes de mail.<br><br><font color="#FF0000"><b>Note :</b> L''adresse e-mail se construit de la mani&egrave;re suivante mon_service<email> (entre les <>, indiquer votre email) </font>', 1, 4, '2007-05-07 12:31:36', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(5, 'Pays', 'STORE_COUNTRY', '73', 'Le pays de r&eacute;sidence de la boutique.<br><br><font color="#FF0000"><b>Note :</b> N''oubliez pas &eacute;galement de configurer les zones dans l''option suivante<br></font>', 1, 6, '2008-12-05 19:10:24', '2006-04-09 16:13:47', 'osc_get_country_name', 'osc_cfg_pull_down_country_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(6, 'Zone', 'STORE_ZONE', '265', 'Zone de la localisation de la boutique.<br><br><font color="#FF0000"><b>Note :</b> Pour compl&eacute;ter ce menu, vous pouvez aller dans le menu "Lieu/Taxes"<br></font>', 1, 7, '2008-12-05 19:10:33', '2006-04-09 16:13:47', 'osc_cfg_get_zone_name', 'osc_cfg_pull_down_zone_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(9, 'Ajustement automatique des devises', 'USE_DEFAULT_LANGUAGE_CURRENCY', 'false', 'Changement automatique de la devise monetaire selon la langue que s&eacute;l&eacute;ctionne le client.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 10, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(10, 'Envoyer en copie les commandes sur d''autres adresses e-Mails', 'SEND_EXTRA_ORDER_EMAILS_TO', '', 'Envoyer une copie de la commande aux adresses sp&eacute;cifi&eacute;es.<br><br><font color="#FF0000"><b>Note :</b> Vous pouvez en ajouter plusieurs avec une s&eacute;paration par une virgule<br></font>', 1, 11, '2007-06-02 15:44:29', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(12, 'Afficher le panier apr&egrave;s l''ajout d''un produit', 'DISPLAY_CART', 'true', 'En valeur <b><i>true</i></b> la boutique affichera le contenu du panier apr&egrave;s l''ajout d''un produit.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 14, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(13, 'Permettre aux visiteurs la recommandation d''un produit', 'ALLOW_GUEST_TO_TELL_A_FRIEND', 'false', 'En valeur <b><i>true</i></b> les visiteurs trouveront un lien dans les fiches produits afin de lui permettre de recommander ce produit &agrave; un coll&egrave;gue par l''envoi d''un mail.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 15, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(14, 'Mode par d&eacute;faut sur la fonction recherche', 'ADVANCED_SEARCH_DEFAULT_OPERATOR', 'and', 'Veuillez choisir l''op&eacute;rateur de recherche qui sera utilis&eacute; par d&eacute;faut.<br><br><i>(Valeur and = Mot1 <b>et</b> le Mot2 - Valeur or = Mot1 <b>ou</b> le Mot2)</i><br>', 1, 17, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''and'', ''or''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(15, 'Nom, adresse, pays et tel de la boutique', 'STORE_NAME_ADDRESS', 'Adresse\r\nCode Postal - Ville\r\nMon telephone', 'Nom, adresse, pays et t&eacute;l&eacute;phone de la boutique qui seront utilis&eacute;s dans les impressions des documents et l''affichage en ligne.<br>', 1, 18, '2008-09-15 09:39:38', '2006-04-09 16:13:47', NULL, 'osc_cfg_textarea(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(17, 'Taxe d&eacute;cimale', 'TAX_DECIMAL_PLACES', '2', 'Emplacement d&eacute;cimal pour la valeur du montant de la taxe.<br>', 1, 20, '2006-05-31 01:17:46', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(18, 'Afficher les prix avec les taxes', 'DISPLAY_PRICE_WITH_TAX', 'true', 'En valeur <b><i>true</i></b> Le prix Toutes Taxes Comprises est affich&eacute;, en false, le prix affich&eacute; est Hors Taxes.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 21, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(19, 'Pr&eacute;nom', 'ENTRY_FIRST_NAME_MIN_LENGTH', '2', 'Nombre de caract&egrave;res minimum requis pour le pr&eacute;nom.', 16, 1, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(20, 'Nom', 'ENTRY_LAST_NAME_MIN_LENGTH', '2', 'Nombre de caract&egrave;res minimum requis pour le nom de famille.', 16, 2, '2006-10-23 22:49:39', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(21, 'Date de naissance', 'ENTRY_DOB_MIN_LENGTH', '10', 'Nombre de caract&egrave;res minimum requis pour la date de naissance.', 16, 3, '2006-10-23 01:10:08', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(23, 'Adresse', 'ENTRY_STREET_ADDRESS_MIN_LENGTH', '4', 'Nombre de caract&egrave;res minimum requis pour l''adresse postale.', 16, 5, '2006-10-23 01:10:20', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(24, 'Soci&eacute;t&eacute;', 'ENTRY_COMPANY_MIN_LENGTH', '0', 'Nombre de caract&egrave;res minimum requis pour le nom de la soci&eacute;t&eacute;.', 16, 6, '2006-10-23 01:15:51', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(25, 'Code Postal', 'ENTRY_POSTCODE_MIN_LENGTH', '4', 'Nombre de caract&egrave;res minimum requis pour le code postal.', 16, 7, '2006-10-23 01:10:48', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(26, 'Ville', 'ENTRY_CITY_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour la ville.', 16, 8, '2006-10-23 01:10:58', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(27, 'D&eacute;partement ou &eacute;tat', 'ENTRY_STATE_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour le d&eacute;partement ou l''&eacute;tat.', 16, 9, '2006-10-23 01:11:12', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(28, 'T&eacute;l&eacute;phone', 'ENTRY_TELEPHONE_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour le t&eacute;l&eacute;phone.', 16, 10, '2006-10-23 22:49:04', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(29, 'Mot de Passe', 'ENTRY_PASSWORD_MIN_LENGTH', '5', 'Nombre de caract&egrave;res minimum requis pour le mot de passe utilisateur.', 16, 11, '2006-10-23 00:37:27', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(30, 'Propri&eacute;taire de la carte de cr&eacute;dit', 'CC_OWNER_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour le nom du propri&eacute;taire de la carte de cr&eacute;dit.', 2, 12, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(31, 'Num&eacute;ro de la carte de cr&eacute;dit', 'CC_NUMBER_MIN_LENGTH', '10', 'Nombre de caract&egrave;res minimum requis pour le num&eacute;ro de la carte de cr&eacute;dit.', 2, 13, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(32, 'Veuillez indiquer le nombre minimum de mots que le client doit ins&eacute;rer dans les commentaires', 'REVIEW_TEXT_MIN_LENGTH', '50', 'Nombre de caract&egrave;res minimum requis pour le produit comment&eacute;.<br><i> - 10 pour un commentaires de 10 caract&egrave;res minimum</i><br>', 32, 1, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(35, 'Nombre d''entr&eacute;es dans le carnet d''adresses', 'MAX_ADDRESS_BOOK_ENTRIES', '5', 'Nombre maximum d''entr&eacute;es dans le carnet d''adresses pour un client.', 3, 1, '2006-10-14 15:45:07', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(36, 'R&eacute;sultat de recherche', 'MAX_DISPLAY_SEARCH_RESULTS', '20', 'Quantit&eacute; d''articles &agrave; afficher par page.', 3, 2, '2007-04-24 19:02:41', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(37, 'Liens de page', 'MAX_DISPLAY_PAGE_LINKS', '5', 'Nombre de liens utilis&eacute;s.', 3, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(53, 'Affichage Page Historique de commande', 'MAX_DISPLAY_ORDER_HISTORY', '10', 'Nombre maximum d''historiques de commande &agrave; afficher dans la page.', 3, 18, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(54, 'Vignette ou petite image : veuillez indiquer la largeur dans les pages de listing', 'SMALL_IMAGE_WIDTH', '130 ', 'Veuillez indiquer le nombre de pixels pour la largeur des vignettes ou petites images dans les pages de listing', 4, 1, '2007-05-19 16:34:40', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(55, 'Vignette ou petite image : veuillez indiquer la hauteur dans les pages de listing', 'SMALL_IMAGE_HEIGHT', '', 'Veuillez indiquer le nombre de pixels pour la hauteur des vignettes ou petites images dans les pages de listing', 4, 2, '2007-05-19 16:34:35', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(56, 'Image d''ent&ecirc;te, Largeur', 'HEADING_IMAGE_WIDTH', '', 'Nombre de pixels pour la largeur des images d''ent&ecirc;te.', 4, 3, '2007-05-19 16:34:44', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(57, 'Image d''ent&ecirc;te, Hauteur', 'HEADING_IMAGE_HEIGHT', '', 'Nombre de pixels pour la hauteur des images d''ent&ecirc;te.', 4, 4, '2007-05-19 16:34:49', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(58, 'Image de sous-cat&eacute;gorie, Largeur', 'SUBCATEGORY_IMAGE_WIDTH', '', 'Nombre de pixels pour la largeur des images de sous-cat&eacute;gorie.', 4, 5, '2007-05-19 16:34:53', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(59, 'Image de sous-cat&eacute;gorie, Hauteur', 'SUBCATEGORY_IMAGE_HEIGHT', '', 'Nombre de pixels pour la hauteur des images de sous-cat&eacute;gorie.', 4, 6, '2007-05-19 16:34:58', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(60, 'Calcul Auto de la taille des images', 'CONFIG_CALCULATE_IMAGE_SIZE', 'false', 'Calculer automatiquement la taille des images.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 4, 7, '2007-05-18 05:21:07', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(61, 'Image inexistante', 'IMAGE_REQUIRED', 'false', 'Permettre l''affichage des liens bris&eacute;s sur les images (pour le d&eacute;veloppement).<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 4, 8, '2007-05-19 03:31:22', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(62, 'Civilit&eacute; <i>(Homme & Femme)</i>', 'ACCOUNT_GENDER', 'true', 'Afficher deux cases &agrave; cocher sur la civilit&eacute; dans le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 1, '2006-10-23 01:15:03', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(63, 'Date de naissance', 'ACCOUNT_DOB', 'true', 'Afficher la date de naissance dans le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 2, '2006-10-23 01:15:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(64, 'Soci&eacute;t&eacute;', 'ACCOUNT_COMPANY', 'true', 'Afficher le champ soci&eacute;t&eacute; dans la zone "Mon compte" et le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 3, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(65, 'Compl&eacute;ment d''adresse', 'ACCOUNT_SUBURB', 'true', 'Afficher le compl&eacute;ment d''adresse dans le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 4, '2006-10-23 01:16:30', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(66, 'Etat et/ou D&eacute;partement', 'ACCOUNT_STATE', 'true', 'Afficher le champ d&eacute;partement (&eacute;tat) dans le formulaire d\\''inscription<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 5, '2006-10-23 01:17:10', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(84, 'Devise par d&eacute;faut', 'DEFAULT_CURRENCY', 'EUR', 'Devise par d&eacute;faut.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(85, 'Language par d&eacute;faut', 'DEFAULT_LANGUAGE', 'fr', 'Language par d&eacute;faut.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(86, 'Etat par d&eacute;faut pour une nouvelle commande', 'DEFAULT_ORDERS_STATUS_ID', '1', 'Quand une nouvelle commande est cr&eacute;&eacute;e, ce statut de commande lui sera assign&eacute;.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(98, 'Code pays boutique', 'SHIPPING_ORIGIN_COUNTRY', '38', 'Entrer le code "ISO 3166" du pays pour calculer les co&ucirc;ts d''exp&eacute;dition.', 7, 1, '2008-09-13 13:08:41', '2006-04-09 16:13:48', 'osc_get_country_name', 'osc_cfg_pull_down_country_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(99, 'Code postal boutique', 'SHIPPING_ORIGIN_ZIP', 'H2S2N1', 'Entrez le code postal de la boutique pour calculer les frais d''exp&eacute;dition.', 7, 2, '2008-09-13 13:08:47', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(100, 'Poids maximum exp&eacute;diable (en Kg)', 'SHIPPING_MAX_WEIGHT', '50', 'Les transporteurs ont une limite de poids maximum par colis. Cette limite est commune pour tous.', 7, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(101, 'Tare de l''emballage', 'SHIPPING_BOX_WEIGHT', '0', 'Quel est le poids moyen de l''emballage et conditionnement des colis de petite &agrave; moyenne taille ?', 7, 4, '2008-09-13 14:57:27', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(102, 'Gros colis - Pourcentage de suppl&eacute;ment', 'SHIPPING_BOX_PADDING', '10', 'Pour 10% de suppl&eacute;ment, entrer 10.', 7, 5, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(103, 'Souhaitez vous afficher l''image du produit ?', 'PRODUCT_LIST_IMAGE', '1', 'Dans la liste des articles, souhaitez-vous afficher l''image du produit ?<br>Veuillez indiquer un ordre de tri<br><br><i>- 0 pour aucun affichage<br>- 1 par exemple pour la premi&egrave;re position</i><br>', 0, 1, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(104, 'Souhaitez vous afficher l''ordre de tri par marque ?', 'PRODUCT_LIST_MANUFACTURER', '0', 'Dans la liste des articles, Souhaitez-vous afficher l''ordre de tri par la marque  ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br />- 1 pour afficher l''ordre de tri</i><br>', 8, 2, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(105, 'Souhaitez vous afficher l''ordre de tri par le mod&egrave;le du produit ?', 'PRODUCT_LIST_MODEL', '0', 'Dans la liste des articles, souhaitez-vous afficher l''ordre de tri par le mod&egrave;le du produit ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br />- 1 pour afficher l''ordre de tri</i><br />', 8, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(106, 'Souhaitez vous afficher l''ordre de tri par le nom du produit ?', 'PRODUCT_LIST_NAME', '2', 'Dans la liste des articles, souhaitez-vous afficher l''ordre de tri par le nom du produit ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br>- 1 pour afficher l''ordre de tri</i><br />', 8, 4, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(107, 'Souhaitez vous afficher l''ordre de tri par le prix du produit ?', 'PRODUCT_LIST_PRICE', '3', 'Dans la liste des articles, souhaitez-vous afficher l''ordre de tri par le prix du produit ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br />- 1 pour afficher l''ordre de tri</i><br />', 8, 5, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(108, 'Souhaitez vous afficher l''ordre de tri par le stock du produit ?', 'PRODUCT_LIST_QUANTITY', '0', 'Dans la liste des articles, souhaitez-vous afficher l''ordre de tri par le stock du produit ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br />- 1 pour afficher l''ordre de tri</i><br />', 8, 6, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(109, 'Souhaitez vous affiche  l''ordre de tri par le poids du produit ?', 'PRODUCT_LIST_WEIGHT', '0', 'Dans la liste des articles, souhaitez-vous afficher  l''ordre de tri par le poids du produit ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br />- 1 pour afficher l''ordre de tri</i><br />', 8, 7, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(111, 'Souhaitez vous afficher le filtre Cat&eacute;gorie/Marques du produit', 'PRODUCT_LIST_FILTER', '1', 'Dans la liste des articles, souhaitez-vous afficher le filtre Cat&eacute;gorie/Marques du produit ?<br><br><i>- 0 pour NON<br>- 1 pour OUI</i><br>', 8, 9, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(112, 'Ou souhaitez vous positionner la barre de navigation ?', 'PREV_NEXT_BAR_LOCATION', '2', 'Dans la liste des articles, ou souhaitez vous positionner la barre de navigation ?<br><br><i>- 1 en haut<br>- 2 pour le bas<br>- 3 pour le haut et le bas</i><br>', 3, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(113, 'V&eacute;rification du stock', 'STOCK_CHECK', 'true', 'V&eacute;rifier si le niveau de stock est suffisant.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 9, 1, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(114, 'D&eacute;compte du stock', 'STOCK_LIMITED', 'true', 'D&eacute;compte du stock les articles command&eacute;s.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 9, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(115, 'Autorisation achat hors stock', 'STOCK_ALLOW_CHECKOUT', 'false', 'Permet au client de commander m&ecirc;me si un article n''est pas en stock.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 9, 3, '2008-09-14 18:46:31', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(116, 'Affichage des produits hors stock', 'STOCK_MARK_PRODUCT_OUT_OF_STOCK', '***', 'Affiche la remarque suivante si le produit n''est plus en stock.', 9, 4, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(117, 'Stock niveau d''alerte', 'STOCK_REORDER_LEVEL', '5', 'Niveau pour l''alerte de r&eacute;approvisionnement du stock.', 9, 5, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(118, 'Stockage du temps d''ex&eacute;cution', 'STORE_PAGE_PARSE_TIME', 'false', 'Stocke le temps d''ex&eacute;cution d''une page.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 10, 1, '2007-05-20 01:00:47', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(119, 'Emplacement du fichier pour le stocke d''ex&eacute;cution', 'STORE_PAGE_PARSE_TIME_LOG', '/home/www/site/boutique/cache/admin.log', 'Chemin d''acc&egrave;s et nom du fichier des temps d''ex&eacute;cution.', 10, 2, '2008-09-15 10:07:36', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(120, 'Format de date des ex&eacute;cutions', 'STORE_PARSE_DATE_TIME_FORMAT', '%d/%m/%Y %H:%M:%S', 'Format de la date des ex&eacute;cutions.', 10, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(121, 'Affichage du temps d''ex&eacute;cution', 'DISPLAY_PAGE_PARSE_TIME', 'false', 'Affiche le temps d''ex&eacute;cution d''une page (le stockage du temps d''ex&eacute;cution doit &ecirc;tre activ&eacute; et l''emplacement choisi du fichier pour le stockage d''ex&eacute;cution).<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 10, 4, '2007-06-03 16:58:14', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(122, 'Requ&ecirc;tes de base des donn&eacute;es', 'STORE_DB_TRANSACTIONS', 'false', 'Stocke les requ&ecirc;tes de la base des donn&eacute;es avec les temps d''ex&eacute;cution.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 10, 5, '2007-06-03 16:58:22', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(123, 'Utiliser le cache', 'USE_CACHE', 'false', 'Utiliser les fonctionnalit&eacute;s de cache.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 11, 1, '2006-07-18 00:13:26', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(124, 'R&eacute;pertoire de cache', 'DIR_FS_CACHE', '/tmp/', 'R&eacute;pertoire o&ugrave; les fichiers de cache sont stock&eacute;s.', 11, 2, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(125, 'M&eacute;thode de transport d''email', 'EMAIL_TRANSPORT', 'sendmail', 'D&eacute;finit si le serveur utilise une connexion locale &agrave; sendmail ou une connexion SMTP par TCP/IP. Les Serveurs Windows et MacOS devraient codifier SMTP.', 12, 1, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''sendmail'', ''gmail'', ''smtp''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(126, 'Saut de ligne en-t&ecirc;te des emails', 'EMAIL_LINEFEED', 'LF', 'D&eacute;finit les caract&egrave;res utilis&eacute;s pour s&eacute;parer les en-t&ecirc;tes des emails.', 12, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''LF'', ''CRLF''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(127, 'Utiliser MIME HTML pour l''envoi des emails', 'EMAIL_USE_HTML', 'false', 'Envoie les emails au format html ou texte brut.<br><br><i>(Valeur True = HTML - Valeur False = TEXTE)</i>', 12, 3, '2008-09-15 22:57:14', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(128, 'V&eacute;rifier l''adresse email par le DNS', 'ENTRY_EMAIL_ADDRESS_CHECK', 'false', 'V&eacute;rifier l''adresse email au travers un serveur DNS.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 12, 4, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(129, 'Activation des emails', 'SEND_EMAILS', 'true', 'Permettre l''envoi des emails.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 12, 5, '2008-09-16 10:52:38', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(130, 'Permet le t&eacute;l&eacute;chargement', 'DOWNLOAD_ENABLED', 'false', 'Valider la fonction de t&eacute;l&eacute;chargement des produits.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 13, 1, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(131, 'T&eacute;l&eacute;chargement par redirection', 'DOWNLOAD_BY_REDIRECT', 'false', 'Utiliser la redirection pour t&eacute;l&eacute;charger. D&eacute;sactiver sur des syst&egrave;mes non UNIX.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 13, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(132, 'D&eacute;lais d''expiration des t&eacute;l&eacute;chargements', 'DOWNLOAD_MAX_DAYS', '7', 'Nombre de jours avant expiration du lien de t&eacute;l&eacute;chargement.<br>(0 pour pas de limite)', 13, 3, NULL, '2006-04-09 16:13:48', NULL, '');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(133, 'Nombre maximum de t&eacute;l&eacute;chargements', 'DOWNLOAD_MAX_COUNT', '5', 'Nombre maximum de t&eacute;l&eacute;chargements autoris&eacute;s.<br>(0 pour aucun)', 13, 4, NULL, '2006-04-09 16:13:48', NULL, '');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(134, 'Permettre compression GZip', 'GZIP_COMPRESSION', 'true', 'Permettre la compression HTTP GZip.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 14, 1, '2006-09-23 01:42:33', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(135, 'Niveau de Compression', 'GZIP_LEVEL', '5', 'D&eacute;terminer le niveau de compression entre  0 &agrave; 9.<br>(0 = minimum, 9 = maximum)<br>Conseill&eacute; : 5.', 14, 2, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(136, 'R&eacute;pertoire des sessions', 'SESSION_WRITE_DIRECTORY', '/tmp', 'Le chemin o&ugrave; les sessions seront stock&eacute;es.', 15, 1, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(137, 'Utilisation forc&eacute;e des cookies', 'SESSION_FORCE_COOKIE_USE', 'False', 'Forcer l''utilisation des sessions quand les Cookies sont permis.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 15, 2, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(138, 'V&eacute;rifiez l''identification de session', 'SESSION_CHECK_SSL_SESSION_ID', 'False', 'Valider la SSL_SESSION_ID sur chaque demande de page s&eacute;curis&eacute;e en HTTPS.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 15, 3, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(139, 'V&eacute;rifiez l''utilisateur', 'SESSION_CHECK_USER_AGENT', 'False', 'Validez le navigateur du client sur chaque demande de page.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 15, 4, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(140, 'V&eacute;rifiez l''adresse IP', 'SESSION_CHECK_IP_ADDRESS', 'False', 'Validez l''adresse IP du client sur chaque demande de page.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 15, 5, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(141, 'Emp&ecirc;chez les sessions d''araign&eacute;e', 'SESSION_BLOCK_SPIDERS', 'False', 'Emp&ecirc;chez les araign&eacute;es connues de commencer une session.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 15, 6, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(142, 'Recr&eacute;ez une session', 'SESSION_RECREATE', 'True', 'Recr&eacute;ez la session pour produire une nouvelle identification de session quand le client entre ou cr&eacute;e un compte (PHP > = 4.1 n&eacute;cessaire).<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 15, 7, NULL, '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''True'', ''False''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(143, 'Veuillez s&eacute;lectionner votre th&egrave;me graphique du site', 'SITE_THEMA', 'bootstrap', 'Veuillez s&eacute;lectionner le num&eacute;ro du th&egrave;me graphique.<br><br><font color="#FF0000"><b>Note :</b> Pour compl&eacute;ter votre liste de th&egrave;mes graphique, n''h&eacute;sitez pas &agrave; contacter notre support en ligne<br></font>', 43, 1, '2013-12-16 18:12:37', '2006-04-09 18:20:19', NULL, 'osc_cfg_pull_down_all_template_directorylist(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(144, 'Supprimer l''apparition des tarifs aux visiteurs', 'PRICES_LOGGED_IN', 'false', 'Supprimer l\\''apparition des tarifs aux visiteurs non enregistr&eacute;s.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 17, 3, '2006-04-26 19:36:44', '2001-11-17 11:22:55', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(145, 'M&eacute;thode sur la gestion des prix (Remise ou Majoration)', 'B2B', 'false', 'L\\''option <i><b>false</b></i> permet une remise sur les prix et <i><b>true</b></i> permet d\\''effectuer une augmentation sur les prix.', 17, 2, '2006-04-11 18:32:13', '2003-10-06 17:24:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(146, 'Approbation des membres professionnels', 'MEMBER', 'true', 'L\\''option sur <i><b>true</b></i> permet d\\''avoir un contrôle pour approuver les cr&eacute;ations des nouveaux comptes clients professionnels<br><br><font color="FF0000"><b>Note :</b> L\\''approbation s\\''effectue depuis le menu Clients -> Clients en attente</font>', 17, 4, '2006-10-20 00:49:26', '2004-02-26 19:30:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(147, 'Recevez un mail &agrave; l''inscription d''un nouveau client', 'EMAIL_CREAT_ACCOUNT_PRO', 'true', 'Recevez un email lors d\\''une inscription d\\''un nouveau client en compte professionnel.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 17, 5, '2007-05-05 13:23:32', '2006-04-29 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(148, 'Civilit&eacute; <i>(Homme & Femme)</i>', 'ACCOUNT_GENDER_PRO', 'true', 'Afficher deux cases &agrave; cocher sur la civilit&eacute; dans le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 1, '2006-10-29 19:49:55', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(149, 'Date de naissance', 'ACCOUNT_DOB_PRO', 'true', 'Afficher la date de naissance dans le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 2, '2006-10-29 16:07:10', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(150, 'Soci&eacute;t&eacute;', 'ACCOUNT_COMPANY_PRO', 'true', 'Afficher le champ soci&eacute;t&eacute; dans la zone "Mon compte" et le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 3, '2006-10-29 19:49:34', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(151, 'Compl&eacute;ment d''adresse', 'ACCOUNT_SUBURB_PRO', 'true', 'Afficher le compl&eacute;ment d''adresse dans le formulaire d\\''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 4, '2006-10-29 19:50:30', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(152, 'Etat et/ou D&eacute;partement', 'ACCOUNT_STATE_PRO', 'true', 'Afficher le champ d&eacute;partement (&eacute;tat) dans le formulaire d\\''inscription<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 5, '2006-10-29 19:50:57', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(153, 'Num&eacute;ro d''immatriculation de la soci&eacute;t&eacute;', 'ACCOUNT_SIRET_PRO', 'true', 'Demander le num&eacute;ro d''immatriculation de la soci&eacute;t&eacute; dans le formulaire d''inscription des clients <i>(par exemple pour le RCS en France ou le NEQ au Qu&eacute;bec)</i>.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 6, '2006-10-29 16:04:49', '2006-04-26 14:44:07', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(154, 'Num&eacute;ro de la nomenclature de la soci&eacute;t&eacute;', 'ACCOUNT_APE_PRO', 'true', 'Demander le num&eacute;ro de la nomenclature de la soci&eacute;t&eacute; dans le formulaire d''inscription des clients <i>(par exemple 721Z pour le code APE en France)</i>.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 7, '2006-10-29 16:05:35', '2006-04-27 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(155, 'Num&eacute;ro de TVA Intracommunautaire (Europe seulement)', 'ACCOUNT_TVA_INTRACOM_PRO', 'true', 'Demander le num&eacute;ro de TVA Intracommunautaire dans le formulaire d''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i><br><br><font color="#FF0000"><b> Note :</b> Valide uniquement pour l''europe</font>', 18, 8, '2006-10-29 16:05:56', '2006-04-26 14:43:56', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(156, 'Pays par d&eacute;faut &agrave; afficher sur le menu d&eacute;roulant', 'ACCOUNT_COUNTRY_PRO', '73', 'Veuillez s&eacute;l&eacute;ctionner le pays par d&eacute;faut &agrave; afficher dans le menu d&eacute;roulant du formulaire d\\''inscription des clients.<br>', 18, 9, '2007-03-18 21:43:58', '2006-04-29 00:00:00', 'osc_get_country_name', 'osc_cfg_pull_down_country_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(157, 'Groupe clients &agrave; attribuer par d&eacute;faut aux nouveaux clients professionnels', 'ACCOUNT_GROUP_DEFAULT_PRO', '1', 'Veuillez s&eacute;l&eacute;ctionner le groupe clients &agrave; attribuer par d&eacute;faut aux nouveaux clients.', 18, 10, '2008-08-30 11:55:02', '2006-10-16 00:00:00', 'osc_get_customers_group_name', 'osc_cfg_pull_down_customers_group_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(158, 'Autoriser les modifications sur les informations de la soci&eacute;t&eacute;', 'ACCOUNT_MODIFY_PRO', 'true', 'Autoriser par d&eacute;faut les clients &agrave; modifier <b><i>Nom, num&eacute;ro de Siret, code APE, et num&eacute;ro de TVA Intracom</i></b> de sa soci&eacute;t&eacute;.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 11, '2006-10-15 22:50:18', '2006-04-29 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(159, 'Autoriser la modification de l''adresse principale de facturation', 'ACCOUNT_MODIFY_ADRESS_DEFAULT_PRO', 'true', 'Autoriser par d&eacute;faut les clients &agrave; modifier l''adresse principale de son carnet d''adresse.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 12, '2006-10-15 22:43:28', '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(160, 'Autoriser l''ajout dans le carnet d''adresse', 'ACCOUNT_ADRESS_BOOK_PRO', 'true', 'Autoriser par d&eacute;faut les clients &agrave; ajouter des adresses dans son carnet.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 13, '2006-10-15 22:50:21', '2006-05-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(161, 'Nombre minimum de caract&egrave;res pour le pr&eacute;nom', 'ENTRY_FIRST_NAME_PRO_MIN_LENGTH', '2', 'Nombre de caract&egrave;res minimum requis pour le pr&eacute;nom.', 19, 1, '2006-10-29 22:51:30', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(162, 'Nombre minimum de caract&egrave;res pour le nom', 'ENTRY_LAST_NAME_PRO_MIN_LENGTH', '2', 'Nombre de caract&egrave;res minimum requis pour le nom.', 19, 2, '2006-10-29 22:30:18', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(163, 'Nombre minimum de caract&egrave;res pour la date de naissance', 'ENTRY_DOB_PRO_MIN_LENGTH', '10', 'Nombre de caract&egrave;res minimum requis pour la date de naissance.', 19, 3, NULL, '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(689, 'Souhaitez vous afficher l''ordre de tri par la date du produit ?', 'PRODUCT_LIST_DATE', '0', 'Dans la liste des articles, souhaitez-vous afficher l''ordre de tri par le date du produit ?<br />Veuillez indiquer un ordre de tri<br /><br /><i>- 0 pour aucun affichage<br />- 1 pour afficher l''ordre de tri</i><br />', 8, 3, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(165, 'Nombre minimum de caract&egrave;res pour l''adresse', 'ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour l''adresse.', 19, 5, '2006-10-29 22:30:35', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(166, 'Nombre minimum de caract&egrave;res pour la soci&eacute;t&eacute;', 'ENTRY_COMPANY_PRO_MIN_LENGTH', '4', 'Nombre de caract&egrave;res minimum requis pour la soci&eacute;t&eacute;.', 19, 6, '2006-10-29 16:08:26', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(167, 'Nombre minimum de caract&egrave;res pour le num&eacute;ro d''immatriculation de la soci&eacute;t&eacute;', 'ENTRY_SIRET_MIN_LENGTH', '14', 'Nombre de caract&egrave;res minimum requis pour le num&eacute;ro d''immatriculation de la soci&eacute;t&eacute;.<br><br><i>(par exemple pour le RCS en France ou le NEQ au Qu&eacute;bec)</i>', 19, 7, '2006-10-29 16:09:04', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(168, 'Nombre minimum de caract&egrave;res pour le num&eacute;ro de nomenclature de la soci&eacute;t&eacute;', 'ENTRY_CODE_APE_MIN_LENGTH', '4', 'Nombre de caract&egrave;res minimum requis pour la nomenclature de la soci&eacute;t&eacute;.<br><br><i>(par exemple 721Z pour le code APE en France)</i>', 19, 8, '2006-10-29 16:09:50', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(169, 'Nombre minimum de caract&egrave;res pour le num&eacute;ro de TVA Intracommunautaire (Europe seulement)', 'ENTRY_TVA_INTRACOM_MIN_LENGTH', '14', 'Nombre de caract&egrave;res minimum requis pour le num&eacute;ro de TVA Intracommunautaire (hors le code ISO du pays qui est indiqu&eacute; automatiquement).<br><br><font color="#FF0000"><b> Note :</b> Valide uniquement pour l''europe</font>', 19, 9, '2006-10-29 16:10:35', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(170, 'Nombre minimum de caract&egrave;res pour le code postal', 'ENTRY_POSTCODE_PRO_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour le code postal.', 19, 10, '2006-10-29 22:31:16', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(171, 'Nombre minimum de caract&egrave;res pour la ville', 'ENTRY_CITY_PRO_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour la ville.', 19, 11, '2006-10-29 22:31:31', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(172, 'Nombre minimum de caract&egrave;res pour le d&eacute;partement ou &eacute;tat', 'ENTRY_STATE_PRO_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour le d&eacute;partement ou &eacute;tat.', 19, 12, '2006-10-29 22:31:47', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(173, 'Nombre minimum de caract&egrave;res pour le t&eacute;l&eacute;phone', 'ENTRY_TELEPHONE_PRO_MIN_LENGTH', '3', 'Nombre de caract&egrave;res minimum requis pour le t&eacute;l&eacute;phone.', 19, 13, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(174, 'Nombre minimum de caract&egrave;res pour le mot de passe', 'ENTRY_PASSWORD_PRO_MIN_LENGTH', '5', 'Nombre de caract&egrave;res minimum requis pour le mot de passe utilisateur.', 19, 14, NULL, '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(175, 'Nombre maximum de caract&egrave;res pour le nom de la soci&eacute;t&eacute;', 'ENTRY_COMPANY_PRO_MAX_LENGTH', '128', 'Nombre de caract&egrave;res maximum requis pour le nom de la soci&eacute;t&eacute;.', 20, 1, '2006-10-29 15:21:20', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(176, 'Nombre maximum de caract&egrave;res pour l''immatriculation de la soci&eacute;t&eacute;', 'ENTRY_SIRET_MAX_LENGTH', '14', 'Nombre de caract&egrave;res maximum requis pour le num&eacute;ro d''immatriculation de la soci&eacute;t&eacute;.<br><br><i>(par exemple pour le RCS en France ou le NEQ au Qu&eacute;bec)</i>', 20, 2, '2006-10-29 15:21:14', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(177, 'Nombre maximum de caract&egrave;res pour la nomenclature de la soci&eacute;t&eacute;', 'ENTRY_CODE_APE_MAX_LENGTH', '4', 'Nombre de caract&egrave;res maximum requis pour la nomenclature de la soci&eacute;t&eacute;.<br><br><i>(par exemple 721Z pour le code APE en France)</i>', 20, 3, '2006-10-29 15:21:06', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(178, 'Nombre maximum de caract&egrave;res pour la TVA Intracommunautaire (Europe seulement)', 'ENTRY_TVA_INTRACOM_MAX_LENGTH', '14', 'Nombre de caract&egrave;res maximum requis pour le num&eacute;ro de TVA Intracommunautaire (hors le code ISO du pays qui est indiqu&eacute; automatiquement).<br><br><font color="#FF0000"><b> Note :</b> Valide uniquement pour l''europe</font>', 20, 4, '2006-10-29 15:20:57', '2006-10-29 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(209, 'Installed Modules', '', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2006-08-21 23:28:24', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(234, 'Veuillez indiquer le nombre de lignes maximales que vous souhaitez afficher dans les listing de l''administration', 'MAX_DISPLAY_SEARCH_RESULTS_ADMIN', '20', 'Affiche un nombre de ligne maximale dans afficher dans les listing de l''administration', 21, NULL, '2007-06-03 13:09:14', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(235, 'Recevoir les emails d''inscription clients', 'EMAIL_INFORMA_ACCOUNT_ADMIN', 'true', 'Souhaitez vous recevoir les emails d''inscription clients lors de la cr&eacute;ation de son compte ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 22, 1, '2007-05-05 13:33:01', '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(236, 'Last Database Restore', 'DB_LAST_RESTORE', 'ClicShopping_05_05_2007_v2.sql', 'Last database restore file', 6, 0, '1000-01-01 00:00:00', '2007-05-07 10:12:36', '', '');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(248, 'Afficher la r&eacute;f&eacute;rence des produits.', 'DISPLAY_MAJR_MODEL', 'true', 'Est ce que vous souhaitez visualiser la r&eacute;f&eacute;rence des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 1, '2007-05-24 23:05:01', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(249, 'Modifier la r&eacute;f&eacute;rence des produits.', 'MODIFY_MAJR_MODEL', 'true', 'Est ce que vous souhaitez pouvoir modifier la r&eacute;f&eacute;rence des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 2, '2007-05-24 23:05:15', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(250, 'Modifier le nom des produits.', 'MODIFY_MAJR_NAME', 'true', 'Est-ce que vous souhaitez pouvoir modifier le nom des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 3, '2007-05-24 23:05:26', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(251, 'Afficher et modifier le statut des produits.', 'DISPLAY_MAJR_STATUS', 'true', 'Est ce que vous souhaitez visualiser et modifier le statut des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 4, '2007-05-24 23:05:36', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(252, 'Afficher et modifier le poids des produits.', 'DISPLAY_MAJR_WEIGHT', 'true', 'Est ce que vous souhaitez visualiser et modifier le poids des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 5, '2007-05-24 23:06:02', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(253, 'Afficher et modifier la quantit&eacute; en stock des produits.', 'DISPLAY_MAJR_QUANTITY', 'true', 'Est ce que vous souhaitez visualiser et modifier le stock des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 6, '2007-05-24 23:06:12', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(254, 'Afficher et modifier l''image des produits.', 'DISPLAY_MAJR_IMAGE', 'false', 'Est ce que vous souhaitez visualiser et modifier l''image des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 7, '2007-05-24 23:06:24', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(255, 'Afficher le fabricant des produits.', 'DISPLAY_MAJR_MANUFACTURER', 'true', 'Est ce que vous souhaitez visualiser le fabricant des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 8, '2007-05-24 23:06:36', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(256, 'Modifier le fabricant des produits.', 'MODIFY_MAJR_MANUFACTURER', 'true', 'Est ce que vous souhaitez pouvoir modifier le fabricant des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 9, '2007-05-24 23:06:47', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(257, 'Afficher les prix des groupes B2B.', 'DISPLAY_MAJR_B2B', 'true', 'Est ce que vous souhaitez visualiser les tarifs des groupes B2B ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 10, '2007-05-24 23:06:59', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(258, 'Afficher la taxe des produits.', 'DISPLAY_MAJR_TAX', 'true', 'Est ce que vous souhaitez visualiser la taxe des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 11, '2007-05-24 23:07:11', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(259, 'Modifier la classe de taxe des produits.', 'MODIFY_MAJR_TAX', 'true', 'Est ce que vous souhaitez visualiser et modifier la classe de taxe &agrave; laquelle sont soumis les produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 12, '2007-05-24 23:07:26', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(260, 'Afficher le prix toutes taxes lors du survol de la souris.', 'DISPLAY_MAJR_TVA_OVER', 'true', 'Est ce que vous souhaitez visualiser les prix toutes taxes des produits lorsque vous passez la souris dessus ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 13, '2007-05-24 23:07:42', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(261, 'Afficher le prix toutes taxes lors de la saisie de prix.', 'DISPLAY_MAJR_TVA_UP', 'true', 'Est ce que vous souhaitez que le prix toutes taxes des produits s''affiche dynamiquement pendant que vous saisissez le prix ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 14, '2007-05-24 00:00:00', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(262, 'Afficher un lien pour visualiser la fiche produit.', 'DISPLAY_MAJR_PREVIEW', 'true', 'Est ce que vous souhaitez avoir un lien pour visualiser une fiche produit ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 15, '2007-05-24 23:08:16', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(263, 'Afficher un lien pour &eacute;diter une fiche produit.', 'DISPLAY_MAJR_EDIT', 'true', 'Est ce que vous souhaitez avoir un lien pour &eacute;diter une fiche produit ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 16, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(318, 'Mode de gestion de la boutique &agrave; utiliser (B2C, B2B ou B2C/B2B)', 'MODE_MANAGEMENT_B2C_B2B', 'B2C_B2B', 'Veuillez s&eacute;lectionner ci-dessous le mode de gestion &agrave; utiliser pour l''inscription de vos clients et le fonctionnement de la boutique.', 17, 1, '2008-09-14 20:47:30', '2007-06-24 02:52:29', NULL, 'osc_cfg_select_option(array(''B2C'', ''B2B'', ''B2C_B2B''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(319, 'Etes vous soumis &agrave; la l&eacute;gislation de l''Ecotaxe ? (Non fonctionnel)', 'DISPLAY_ECOTAX', 'false', 'En position <i>True</i> le calcul de l''Ecotaxe sera pris en compte &agrave; la facturation, n''oubliez pas de v&eacute;rifier si les tarifs sont correctes.<br><br><b><font color="#FF0000">Non fonctionnel sur cette version de ClicShopping</font></b>', 25, 1, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(320, 'Faites vous partie d''un pays g&eacute;rant une double taxe vente ?', 'DISPLAY_DOUBLE_TAXE', 'false', 'En position <i>True</i> vous pouvez g&eacute;rer 2 taxes (ex : Etats F&eacute;d&eacute;ralistes - Province/F&eacute;d&eacute;rale) sur votre facture. <br><br><b><font color="#FF0000">Non fonctionnel sur cette version de ClicShopping</font></b>', 25, 2, '2008-09-15 18:41:51', '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(321, 'Acceptation des conditions g&eacute;n&eacute;rales', 'DISPLAY_CONDITIONS_ON_CHECKOUT', 'true', 'Voulez-vous afficher les termes et les conditions d''achat durant la proc&eacute;dure de paiement ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 25, 3, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(322, 'Afficher les conditions de confidentialit&eacute; &agrave; la cr&eacute;ation d''un compte', 'DISPLAY_PRIVACY_CONDITIONS', 'true', 'Voulez-vous afficher les conditions de confidentialit&eacute; durant la proc&eacute;dure de cr&eacute;ation du compte ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 0, 4, NULL, '2007-07-15 00:00:00', 'NULL', 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(323, 'URL des conditions de vente', 'SHOP_CODE_URL_CONDITIONS_VENTE', 'page_manager.php?pages_id=4', 'Veuillez indiquer l''URL des conditions de vente.', 0, 5, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(324, 'URL de la politique de confidentialit&eacute;', 'SHOP_CODE_URL_CONFIDENTIALITY', 'page_manager.php?pages_id=5', 'Veuillez indiquer l''URL de la politique de confidentialit&eacute;.', 0, 6, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(325, 'Capital de la soci&eacute;t&eacute;', 'SHOP_CODE_CAPITAL', 'SARL au capital de 4000 Ã©', 'Veuillez indiquer le capital de votre soci&eacute;t&eacute;.', 25, 7, '2008-09-15 11:45:32', '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(326, 'TVA Intracommunautaire de la soci&eacute;t&eacute; (Europe seulement)', 'TVA_SHOP_INTRACOM', 'TVA Intracommunautaire : FR14568557555', 'Veuillez indiquer la TVA intracommunautaire de votre soci&eacute;t&eacute;.*<br><br>*<i>Laisser vide si cela ne s''applique pas votre cas.</i>', 25, 8, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(327, 'Num&eacute;ro de la taxe provinciale', 'TVA_SHOP_PROVINCIAL', 'TPS/GST : R127066546', 'Veuillez indiquer le num&eacute;ro administratif de la taxe provinciale (ex : TPS/GST : R127066546).*<br><br>*<i>Laisser vide si cela ne s''applique pas votre cas.</i>', 25, 9, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(328, 'Num&eacute;ro de la taxe f&eacute;d&eacute;rale', 'TVA_SHOP_FEDERAL', 'TVQ/QST : 1006197104', 'Veuillez indiquer le num&eacute;ro administratif de la taxe f&eacute;d&eacute;rale (ex TVQ/QST : 1006197104).*<br><br>*<i>Laisser vide si cela ne s''applique pas votre cas.</i>', 25, 10, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(330, 'Num&eacute;ro d''immatriculation de la soci&eacute;t&eacute;', 'SHOP_CODE_RCS', 'RCS : 1234567', 'Veuillez indiquer le num&eacute;ro d''immatriculation de votre soci&eacute;t&eacute; (ex RCS : 1234567).*<br><br>*<i>Laisser vide si cela ne s''applique pas votre cas.</i>', 25, 11, '2008-09-15 09:46:19', '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(331, 'Num&eacute;ro nomenclature (ou autre) de la soci&eacute;t&eacute;', 'SHOP_CODE_APE', 'Code APE : 721Z', 'Veuillez indiquer le num&eacute;ro de la nomenclature de votre soci&eacute;t&eacute; (ex : Code APE : 721z).*<br><br>*<i>Laisser vide si cela ne s''applique pas votre cas.</i>', 25, 12, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(332, 'Information juridique &agrave; figurer sur les factures', 'SHOP_DIVERS', '', 'Autres informations juridiques &agrave; faire figurer sur les factures.*<br><br>*<i>Laisser vide si aucune information.</i>', 25, 13, NULL, '2007-07-15 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(333, 'Afficher &agrave; cot&eacute; du prix une indication du type de taxe', 'DISPLAY_PRODUCT_PRICE_VALUE_TAX', 'true', 'Afficher apr&egrave;s le prix une indication si le prix du produit est tax&eacute; (ex : TTC) ou non tax&eacute; (ex: HT).<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 22, 2, NULL, '2008-04-14 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(334, 'Afficher &agrave; cot&eacute; du prix une indiquation du type de taxe (pour les groupes client)', 'DISPLAY_PRODUCT_PRICE_VALUE_TAX_PRO', 'true', 'Afficher &agrave; cot&eacute; du prix une indication si le prix du produit est tax&eacute; (ex : TTC) ou non tax&eacute; (ex: HT) pour les groupes de client.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 17, 6, NULL, '2008-04-14 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(336, 'Souhaitez vous supprimer l''ent&ecirc;te des factures PDF  et des bons de livraison', 'DISPLAY_INVOICE_HEADER', 'false', 'Si oui (True) alors l''ent&ecirc;te des factures sera supprim&eacute;e.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 26, 2, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(337, 'Souhaitez vous supprimer le bas de page des factures PDF  et des bons de livraison', 'DISPLAY_INVOICE_FOOTER', 'false', 'Si oui (True) alors le bas de page des factures sera supprim&eacute;.<br><br><i>(Valeur True = Oui - Valeur False = Non</i>', 26, 3, NULL, '2007-07-15 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(338, 'Nom du fichier du logo &agrave; faire apparaitre sur la facture et des bons de livraison', 'INVOICE_LOGO', 'invoice_logo.jpg', 'Veuillez indiquer le nom de votre logo.<br><br><font color="#FF0000"><b>Note :</b> Votre logo (gif, jpg, png) doit &ecirc;tre imp&eacute;rativement ins&eacute;r&eacute; dans le r&eacute;pertoire image/logo/invoice.<br> Attention &agrave; sa hauteur<font>', 7, 6, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(339, 'Statut commande pour l''affichage des factures', 'INVOICE_DISPLAY_ORDER_STATUT', '1', 'Veuillez s&eacute;l&eacute;ctionner ci-dessous le statut par d&eacute;faut qui permettra d''imprimer les factures.<br><br><font color="#FF0000"><b>Note :</b> Les autres statuts imprimeront un bon de commande.</font><br>', 26, 5, '2008-06-23 23:42:37', '2008-06-23 00:00:00', 'osc_get_orders_status_name', 'osc_cfg_pull_down_order_status_list(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(340, 'M&eacute;thode d''affichage des factures', 'INVOICE_DISPLAY_ORDER', 'true', 'Sur oui (true) vous aurez la possibilit&eacute; d''afficher des bons de commandes et des factures selon le statut configur&eacute; par d&eacute;faut.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 26, 4, '2008-06-23 23:15:21', '2008-06-23 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(341, 'Version ClicShopping', 'PROJECT_VERSION', 'ClicShopping est une marque d&eacute;pos&eacute;e (INPI : 3810384) V2.55 -  All right Reserved - ', 'Nom et Version de ClicShopping', 0, NULL, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(342, 'Indiquer la quantit&eacute; maximale que le client peut commander dans son panier', 'MAX_QTY_IN_CART', '99', 'Ins&eacute;rer une quantit&eacute; maximale que le client peut mettre dans son panier pour une commande.<br><i> - 0 pour aucune limite</i></br>', 3, 19, NULL, '2008-08-30 10:21:58', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(343, 'Gestion de la dur&eacute;e des sessions', 'MYSESSION_LIFETIME', '3600', 'D&eacute;terminer le temps que vous souhaitez avant q''une session sur une page se termine, sans rafraichissement. Mesure en secondes. <br><br><i>(Veuillez choisir une valeur diff&eacute;rente de 0)</i>', 15, 8, '2005-01-19 03:43:31', '2005-01-19 03:30:25', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(356, 'Veuillez ins&eacute;rer vos d&eacute;lais de livraison par d&eacute;faut &agrave; indiquer &agrave; vos clients', 'DISPLAY_SHIPPING_DELAY', '4 jours', 'Veuillez indiquer vos d&eacute;lais de livraison des produits par d&eacute;faut &agrave; vos client<br><br>.<b>Note :</b><i> Pour la France cette information est obligatoire (lois Chatel)</i>', 25, 14, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(357, 'Etat par d&eacute;faut pour une &eacute;dition de facture/commande PDF', 'DEFAULT_ORDERS_STATUS_INVOICE_ID', '1', 'Quand une nouvelle g&eacute;n&eacute;ration d''&eacute;dition de facture/commande PDF est cr&eacute;&eacute;e, ce statut de commande lui sera assign&eacute;.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(358, 'Souhaitez vous mettre la boutique hors ligne pour maintenance ?', 'STORE_OFFLINE', 'false', '<br>Si true, votre site sera mis hors ligne et les clients ne pourront plus commander<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 23, NULL, '1000-01-01 00:00:00', 'NULL', 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(359, 'IP qui sont autoris&eacute;es &agrave; se connecter sur le site pendant une maintenance ', 'STORE_OFFLINE_ALLOW', '', '<br>Veuillez sp&eacute;cifier votre adresse ou vos adresses IP. Si vous avez plusieurs adresses ip, veuillez suivre les instructions entre parenth&egrave;ses (Chaque ip doivent &ecirc;tre s&eacute;par&eacute;es par des virgules <br>ex: 127.0.0.1,222.0.0.5', 1, 24, '2007-03-07 20:36:44', '2007-03-07 20:04:07', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(376, 'Installed Modules', 'MODULE_PAYMENT_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, '2008-12-05 19:09:30', '2008-09-16 16:13:41', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(377, 'Indiquer la quantit&eacute; minimale que le client peut commander dans son panier', 'MAX_MIN_IN_CART', '1', 'Ins&eacute;rer une quantit&eacute; minimale par d&eacute;faut que le client doit mettre dans son panier pour une commande.<br><br><i> - 1 : pour une une quantit&eacute; par d&eacute;faut</i></br><br><i> - 0 : emp&ecirc;che toutes commandes faites sur le site par les clients (non recommand&eacute;)</i>', 3, 19, NULL, '2008-08-30 10:21:58', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(399, 'Installed Modules', 'MODULE_SHIPPING_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, '2008-12-05 19:10:05', '2008-12-05 19:09:42', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(433, 'Installed Modules', 'MODULE_ORDER_TOTAL_INSTALLED', 'ot_subtotal.php;ot_shipping.php;ot_tax.php;ot_total.php', 'This is automatically updated. No need to edit.', 6, 0, '2008-12-05 19:10:58', '2008-12-05 19:10:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(434, 'Affichage livraison', 'MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'true', 'Voulez vous afficher le co&ucirc;t de la livraison sur la commande ?', 6, 1, NULL, '2008-12-05 19:10:50', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(435, 'Ordre de tri', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', '2', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:50', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(436, 'Permettre la livraison gratuite', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'false', 'Voulez vous accepter les livraisons gratuites en fonction du montant ?', 6, 3, NULL, '2008-12-05 19:10:50', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(437, 'Livraison gratuite pour commande au dessus', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', '50', 'Permettre la livraison gratuite pour les commandes au dessus du montant suivant.', 6, 4, NULL, '2008-12-05 19:10:50', 'currencies->format', NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(438, 'Attacher livraison gratuite pour les destinations', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION', 'national', 'Livraison gratuite pour des commandes envoy&eacute;s &agrave; l''ensemble des destinations.<br>(both=tous les deux)', 6, 5, NULL, '2008-12-05 19:10:50', NULL, 'osc_cfg_select_option(array(''national'', ''international'', ''both''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(439, 'Affichage du sous-total', 'MODULE_ORDER_TOTAL_SUBTOTAL_STATUS', 'true', 'Voulez-vous montrer le sous-total de la commande ?', 6, 1, NULL, '2008-12-05 19:10:52', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(440, 'Ordre de tri', 'MODULE_ORDER_TOTAL_SUBTOTAL_SORT_ORDER', '1', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:52', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(441, 'Affichage de la taxe', 'MODULE_ORDER_TOTAL_TAX_STATUS', 'true', 'Voulez-vous montrer la taxe de la commande ?', 6, 1, NULL, '2008-12-05 19:10:55', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(442, 'Ordre de tri', 'MODULE_ORDER_TOTAL_TAX_SORT_ORDER', '3', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:55', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(443, 'Affichage du total', 'MODULE_ORDER_TOTAL_TOTAL_STATUS', 'true', 'Voulez-vous montrer le total de la commande ?', 6, 1, NULL, '2008-12-05 19:10:58', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(444, 'Ordre de tri', 'MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER', '4', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2008-12-05 19:10:58', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(445, 'Num&eacute;ro du coupon attribu&eacute; au client lors de la cr&eacute;ation du compte', 'COUPON_CUSTOMER', '', 'Pour tout changement du num&eacute;ro de coupon, veuillez vous r&eacute;f&eacute;rez à la section Marketing / Coupon de votre menu', 0, NULL, NULL, '1000-01-01 00:00:00', ' ', NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(446, 'Num&eacute;ro du coupon attribu&eacute; au client lors de la cr&eacute;ation du compte B2B', 'COUPON_CUSTOMER_B2B', '', 'Pour tout changement du num&eacute;ro de coupon, veuillez vous r&eacute;f&eacute;rez à la section Marketing / Coupon de votre menu', 0, NULL, NULL, '1000-01-01 00:00:00', ' ', NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(447, 'Afficher et modifier la quantit&eacute; minimale pour une commande', 'DISPLAY_QTY_MIN_ORDER', 'true', 'Est ce que vous souhaitez visualiser et modifier la quantit&eacute; minimale de commande qu\\''un client peut réeacute;aliser ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 7, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(456, 'Souhaitez vous recevoir une alerte email concernant la gestion du stock d''alerte ?', 'STOCK_ALERT_PRODUCT_REORDER_LEVEL', 'false', 'Une alerte email vous sera envoy&eacute; si le stock arrive &agrave; son niveau d''alerte ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 9, 6, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(457, 'Souhaitez vous  recevoir une alerte email concernant les produits &eacute;puis&eacute;s  ?', 'STOCK_ALERT_PRODUCT_EXHAUSTED', 'false', 'Une alerte email vous sera envoy&eacute; si le stock est in&eacute;rieure &agrave; 0 ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 9, 7, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(460, 'Souhaitez vous ins&eacute;rer un code de s&eacute;curit&eacute; pour exporter des fichiers xml, txt, csv g&eacute;n&eacute;r&eacute;s ?', 'EXPORT_CODE', 'Gh87yDE', 'Ce code permet d''acc&eacute;der &agrave; l''import de donn&eacutes; par des personnes ou des robots qui sont autoris&eacute;s &agrave; r&eacute;aliser cette op&eacute;ration. Nous vous conseillons fortement de changer le code d''origine et d''en cr&eacute;er un complexe (lettres et chiffres).', 36, 1, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(461, 'Afficher une autorisation pour les produits destin&eacute;s aux comparateurs de prix.', 'DISPLAY_MAJR_PRICE_COMPARISON', 'true', 'Est ce que vous souhaitez connaitre les produits qui sont autoris&eacute;s &agrave; figurer dans l''export pour les comparateurs de prix ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 18, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(462, 'Indiquez le nombre de jours que vous souhaitez voir apparaitre la petite icone nouveaut&eacute; &agrave; cot&eacute; de vos produits ?', 'DAY_NEW_PRODUCTS_ARRIVAL', '30', 'Veuillez indiquer le nombre de jours que vous souhaitez voir afficher la petite icone nouveaut&eacute;.<br><br> Au terme du nombre de jours donn&eacute;, l''icone ne sera plus affich&eacute;<br>', 3, 9, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(502, 'Souhaitez vous afficher les banni&egrave;res ?', 'STORE_DISPLAY_BANNERS', 'true', 'Est ce que vous souhaitez afficher les banni&egrave;res sur le site ?', 4, 10, '2010-07-21 19:20:37', '2010-07-21 19:20:37', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(503, 'Petite image en Hauteur pour l''administration', 'SMALL_IMAGE_HEIGHT_ADMIN', '50', 'Nombre de pixels pour la hauteur des petites images des articles.', 23, 8, '2007-05-19 16:34:35', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(504, 'Petite Image en Largeur pour l''administration', 'SMALL_IMAGE_WIDTH_ADMIN', '', 'Nombre de pixels pour la largeur des images d''ent&ecirc;te.', 23, 9, '2007-05-19 16:34:44', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(505, 'Modules install&eacute;s', 'MODULE_ACTION_RECORDER_INSTALLED', 'ar_admin_login.php;ar_contact_us.php;ar_tell_a_friend.php', 'liste des actions enregistr&eacute;es concernant les modules (s&eacute;par&eacute;s par des points virgules). Mise &agrave; jour automatique. Ce n''est pas utile de l''editer.', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(506, 'Quel d&eacute;lais minimum souhaitez-vous concernant l''envoi par emails par le module contactez-nous', 'MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES', '15', 'Veuillez indiquer un nombre minimum pour permettre l''envoi des emails.<br><font color="#FF0000"><br><b>Note :</b></font>15 pour l''envoi des emails tous les 15 minutes)', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(507, 'Quel d&eacute;lais minimum souhaitez vous concernant l''envoi par emails par le module envoyez &agrave; un ami ?', 'MODULE_ACTION_RECORDER_TELL_A_FRIEND_EMAIL_MINUTES', '15', 'Veuillez indiquer un nombre minimum pour permettre l''envoi des emails.<br><font color="#FF0000"><br><b>Note :</b></font>15 pour l''envoi des emails tous les 15 minutes)', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(508, 'Veuiller indiquer le temps d''attente pour une erreur de connexion dans la partie administration', 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_MINUTES', '5', 'Veuillez indiquer le nombre de minutes d''attente concernant une nouvelle connexion dans l''administration', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(509, 'Veuiller indiquer le nombre de login permis pour se connecter dans la partie administration ', 'MODULE_ACTION_RECORDER_ADMIN_LOGIN_ATTEMPTS', '3', 'Veuillez indiquer le nombre de login permis concernant une connexion dans l''administration', 6, 0, NULL, '2010-07-21 19:21:01', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(531, 'Installed Template Block Groups', 'TEMPLATE_BLOCK_GROUPS', ';header_tags;modules_account_customers;modules_advanced_search;modules_blog;modules_blog_content;modules_boxes;modules_checkout_success;modules_contact_us;modules_create_account;modules_create_account_pro;modules_footer_suffix;modules_footer;modules_front_page;modules_header;modules_index_categories;modules_login;modules_products_featured;modules_products_heart;modules_products_info;modules_products_listing;modules_products_new;modules_products_special;modules_shopping_cart;modules_sitemap', 'This is automatically updated. No need to edit.', 6, 0, '2014-02-07 20:02:26', '2010-11-12 21:28:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(534, 'Installed Modules', 'MODULE_BOXES_INSTALLED', '', 'List of box module filenames separated by a semi-colon. This is automatically updated. No need to edit.', 6, 0, NULL, '2011-01-17 19:45:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(536, 'Veuillez indiquer le type de pr&eacute;fixe par d&eacute;faut concernant la r&eacute;f&eacute;rence du produit', 'CONFIGURATION_PREFIX_MODEL', 'REF-', 'Veuillez indiquer le type de pr&eacute;fixe par d&eacute;faut que vous souhaitez mettre concernant la r&eacute;f&eacute;rence du produit.<br><br><i>ex : r&eacute;f&eacute;rence produit : <b>REF-</b>Num&eacute;ro_produit ', 7, 7, NULL, '2011-01-17 19:45:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(537, 'Modules install&eacute;es', 'MODULE_SOCIAL_BOOKMARKS_INSTALLED', '', 'Liste des modules des r&eacute;seaux sociaux s&eacute;par&eacute; par un point virgule. Elle est automatiquement mis a jour.', 6, 0, '2014-02-07 20:07:16', '2011-01-17 19:45:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(538, 'Souhaitez vous afficher le t&eacute;l&eacute;phone portable', 'ACCOUNT_CELLULAR_PHONE', 'false', 'Afficher le champ le num&eacute;ro du t&eacute;l&eacute;phone portable dans la zone "Mon compte" et le formulaire d''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 6, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(539, 'Souhaitez vous afficher le fax / T&eacute;l&eacute;copie', 'ACCOUNT_FAX', 'false', 'Afficher le champ le fax dans la zone "Mon compte" et le formulaire d''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 5, 7, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(540, 'Souhaitez vous afficher le  t&eacute;l&eacute;phone portable', 'ACCOUNT_CELLULAR_PHONE_PRO', 'false', 'Afficher le champ le num&eacute;ro du t&eacute;l&eacute;phone portable dans la zone "Mon compte" et le formulaire d''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 161, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(541, 'Souhaitez vous afficher le fax / T&eacute;l&eacute;copie', 'ACCOUNT_FAX_PRO', 'false', 'Afficher le champ le fax dans la zone "Mon compte" et le formulaire d''inscription des clients.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 18, 162, '2006-10-23 01:16:20', '2006-04-09 16:13:48', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(542, 'Veuillez indiquer la couleur en RGB du texte de l''&egrave;dition des factures / commandes', 'INVOICE_RGB', '158,11,14', 'Veuillez indiquer la couleur du texte au format RGB<br><br><font color="#FF0000"><b>Note :</b>Chaque nombre doit etre s&egrave;par&egrave; par un virgule (ex : 0,0,0 pour le noir)</font><br>', 26, 5, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(544, 'Etat par d&eacute;faut pour les commandes par type de quantit&eacute;', 'DEFAULT_PRODUCTS_QUANTITY_UNIT_STATUS_ID', '1', 'Quand un nouveau type de quantit&eacute, est cr&eacute;&eacute;, ce statut lui sera assign&eacute;.', 6, 0, NULL, '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(545, 'Veuillez s&eacute;lectionner le type de cache que vous souhaitez utiliser ?', 'USU5_CACHE_SYSTEM', 'memcache', 'Veuillez choisir une des options concernant la strat&eacute;gie de gestion du cache.', 34, 5, '2011-01-09 21:12:26', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''mysql'', ''file'',''sqlite'',''memcache''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(546, 'Veuillez indiquer le nombre de jour de stokage du cache.', 'USU5_CACHE_DAYS', '7', 'Veuillez indiquer le nombre de jour de stockage du cache, ce d&eacute;lai d&eacute;pass&eacute; le cache sera automatiquement r&eacute;actualis&eacute;.', 34, 6, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(547, 'Veuillez choisir le type d''URL &agrave; afficher', 'USU5_URLS_TYPE', 'rewrite', '<b>Choississez le type d''URL &agrave; afficher</b>', 34, 7, '2011-01-08 21:37:49', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''standard'',''path_standard'',''rewrite'',''path_rewrite'',), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(548, 'Veuillez choisir le mode d''affichage du lien concernant le texte du produit qui sera construit dans l''url', 'USU5_PRODUCTS_LINK_TEXT_ORDER', 'p', 'Veuillez indiquer le type de lien qui sera constuit dans lurl:<br /><br /><b>p</b> = nom des produits (product name)<br /><b>c</b> = nom des cat&eacute;gories (category name)<br /><b>b</b> = mod&egrave;les (brand)<br /><b>m</b> = mod&egrave;le<br /><br /><i>ex : <b>bp</b> (brand/product)</i>', 34, 8, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(549, 'Souhaitez-vous r&eacute;actualiser le cache', 'USU5_RESET_CACHE', 'false', '<span style="color: red;">Note : </span>Cela peut s''av&eacute;rer utile de le faire lorsque vous modifiez un produit par exemple<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 17, '2011-01-09 22:48:06', '2011-01-08 21:22:06', 'osc_reset_cache_data_usu5', 'osc_cfg_select_option(array(''reset'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(550, 'Souhaitez-vous visualiser les variables trait&eacute;s dans le site ?', 'USU5_DEBUG_OUPUT_VARS', 'false', '<span style="color: red;"><strong>Ne pas utiliser cet outil si vous etes dans un mode de production</strong></span><br /><br />Ce syst&egrave;me vous permet de visualiser la contenu des variables trait&eacutees par le gestionnaire de rewriting.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 15, '2011-01-08 21:39:51', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(551, 'Souhaitez-vous visualiser la performance du syst&egrave;me', 'USU5_OUPUT_PERFORMANCE', 'false', '<span style="color: red;"><strong>Ne pas utiliser cet outil si vous etes dans un mode de production.</stong></span><br /><br />Ce syst&egrave;me vous permet de visualiser la performance des requetes qui seront affich&eacutes; au bas de la page du site.<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 14, '2011-01-09 22:15:04', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(552, 'Souhaitez vous supprimer tous les caract&egrave;res non alphanum&eacute;rique ?', 'USU5_REMOVE_ALL_SPEC_CHARS', 'true', 'Ce param&egrave;tre supprime toutes les non lettres et non num&eacute;ros. Le r&eacute;sultat modifie l''&eacute;criture des url.<br /><br /><i>(ex :mon-produit-p-1.html devient monproduit-p-1.html)<br><br>(Valeur True = Oui - Valeur False = Non)</i>', 34, 11, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(553, 'Souhaitez-vous ajouter le cPath &agrave; l''URL de vos produits ?', 'USU5_ADD_CPATH_TO_PRODUCT_URLS', 'false', 'Ce param&egrave;tre ins&egrave;re le cPath &agrave; la fin de l''URL du produit<br /><br /><i>(ex. - mon-produit-p-1.html?cPath=xx).<br /><br />(Valeur True = Oui - Valeur False = Non)</i>', 34, 12, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(554, 'Souhaitez vous convertir des caract&egrave;res sp&eacute;ciaux de conversion ?', 'USU5_CHAR_CONVERT_SET', '', 'Ce param&egrave;tre permet de convertir certains caract&egrave;res.<br><br>le format <b>DOIT</b> etre de la forme : <b>char=>conv, char2=>conv2</b><br />', 34, 13, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(555, 'Souhaitez-vous forcer l''url de la page d''index ?', 'USU5_HOME_PAGE_REDIRECT', 'false', 'Forcer la redirection vers www.mysite.com/ quand www.mysite.com/index.php est affich&eacute; ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 16, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(556, 'Souhaitez-vous filtrer les mots cours ?', 'USU5_FILTER_SHORT_WORDS', '2', 'Ce param&egrave;tre permet de filtrer des mots qui ont un nombre de caract&egrave;res inf&eacute;rieures &agrave la valeur que vous avez indiquer dans l''URL. <br /><br />1 = Supprime les mots de 1 lettre <br />2 = Supprime les mots de 2 lettres ou moins <br />3 = Supprime les mots de 3 lettres ou moins<br>', 34, 9, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''1'',''2'',''3'',), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(557, 'Souhaitez vous ajouter la cat&eacute;gorie parente au d&eacute;but de l''URL ?', 'USU5_ADD_CAT_PARENT', 'true', 'Ce param&egrave;tre ins&eacute;re le nom de la cat&eacute;gorie parente du produit au d&eacute;but de l''URL. <br /><br /><i>(ex : categorie-parente-c-1.html)<br />br />(Valeur True = Oui - Valeur False = Non)</i>', 34, 10, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(558, 'Souhaitez-vous activer le syst&egrave;me de gestion multi-langue ?', 'USU5_MULTI_LANGUAGE_SEO_SUPPORT', 'false', 'Si vous g&eacute;rez plusieurs langues, veuillez activer cette option.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 3, '2011-01-09 21:12:11', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(559, 'Souhaitez-vous des URL en relation avec les normes W3C ?', 'USU5_USE_W3C_VALID', 'true', 'Ce param&egrave;tre (True) permet de cr&eacute;er des URL conforment aux normes &eacute;tablies par le groupe W3C.\r\n<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 4, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(560, 'Souhaitez-vous activer la r&eacute;&eacute;criture des URLs ?', 'USU5_ENABLED', 'false', 'Utiliser des URLs modifi&eacute;es pour tous les liens de la boutique.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 1, '2011-01-09 22:47:43', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(561, 'Souhaitez-vous activer le gestionnaire de cache?', 'USU5_CACHE_ON', 'false', 'Le gestionnaire de cache vous permet d''afficher plus rapidement les pages. Veuillez noter qu''un d&eacute;lai d''attente est n&eacute;cessaire avant l''affichage de page (sinon r&eacute;actuliser le cache).<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 34, 2, '2011-01-08 21:22:06', '2011-01-08 21:22:06', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(562, 'Veuillez indiquer le nombre maximal d''attributs &agrave afficher dans le fichier de gestion des attributs', 'MAX_ROW_LISTS_OPTIONS', '17', 'Veuillez indiquer le nombre maximal d''attributs &agrave afficher', 21, 2, '1000-01-01 00:00:00', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(567, 'Website module installed', 'WEBSITE_MODULE_INSTALLED', '0', 'Verification si le site contient un module installe ou pas', 0, 0, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(569, 'Afficher les produits disponibles seulement sur internet (non dans le magasin) ?', 'DISPLAY_MAJR_PRODUCTS_ONLY_ONLINE', 'false', 'Est ce que vous souhaitez afficher les produits disponibles seulement sur Internet (non dans le magasin) ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 16, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(570, 'Modifier le fournisseur des produits.', 'MODIFY_MAJR_SUPPLIER', 'false', 'Est ce que vous souhaitez pouvoir modifier le fournisseur des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 18, '2007-05-24 23:06:47', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(571, 'Afficher le fournisseur des produits.', 'DISPLAY_MAJR_SUPPLIER', 'false', 'Est ce que vous souhaitez visualiser le fournisseur des produits ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 19, '2007-05-24 23:06:36', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(572, 'Afficher une petite image de pr&eacute;visualisation du produit ?', 'DISPLAY_MAJR_PREVIEW_IMAGE', 'false', 'Est-ce que vous souhaitez afficher une petite image de pr&eacute;visualisation du produit ?<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 24, 17, '2007-05-24 23:08:21', '2007-05-24 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(573, 'Veuillez s&eacute;lectionner le type de code barre', 'BAR_CODE_TYPE', 'EAN', 'Veuillez indiquer votre type de code barre.', 9, 8, '2008-09-14 20:47:30', '2007-06-24 02:52:29', NULL, 'osc_cfg_select_option(array(''C128'', ''C128C'', ''C25'', ''C25I'', ''MSI'', ''EAN'', ''UPC'', ''C39'', ''C11'', ''CODABAR'', ''POSTNET'', ''CMC7'', ''KIX''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(574, 'Veuillez indiquer la taille du code barre', 'BAR_CODE_SIZE', '50', 'Veuillez indiquer la taille de l''image du code barre.', 9, 9, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(575, 'Veuillez indiquer la couleur du code barre', 'BAR_CODE_COLOR', '#254433', 'Veuillez indiquer la couleur du code barre (en hezad&eacute;cimal).', 9, 10, '2006-10-23 22:49:44', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(576, 'Veuillez indiquer l''extansion de l''image qui sera g&eacute;n&eacute;r&eacute;e', 'BAR_CODE_EXTENSION', 'png', 'Veuillez indiquer l''extansion de l''image qui sera g&eacute;n&eacute;r&eacute;e.', 9, 11, '2008-09-14 20:47:30', '2007-06-24 02:52:29', NULL, 'osc_cfg_select_option(array(''png'', ''gif'', ''jpg''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(577, 'Souhaitez-vous afficher la gestion manuelle d''images dans la page de description des produits', 'MANUAL_IMAGE_PRODUCTS_DESCRIPTION', 'false', 'L''affichage de la gestion manuelle d''images vous permettra de g&eacute;rer au mieux vos images dans la partie visuelle de description des produits et d''acc&eacute;der &agrave; une galerie d''images.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 23, 3, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(578, 'Souhaitez-vous afficher la gestion manuelle d''images dans la page de description des produits', 'MANUAL_IMAGE_PRODUCTS_DESCRIPTION', 'false', 'L''affichage de la gestion manuelle d''images vous permettra de g&eacute;rer au mieux vos images dans la partie visuelle de description des produits et d''acc&eacute;der &agrave; une galerie d''images.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 23, 3, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(579, 'Image Moyenne : veuillez indiquer la largeur dans la page de description des produits', 'MEDIUM_IMAGE_WIDTH', '250', 'Veuillez indiquer le nombre de pixels pour la largeur des images moyenne dans la page de description des articles', 4, 11, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(580, 'Image Moyenne : veuillez indiquer la hauteur dans la page de description des produits', 'MEDIUM_IMAGE_HEIGHT', '', 'Veuillez indiquer le nombre de pixels pour la hauteur des images moyenne dans la page de description des articles', 4, 12, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(581, 'Image Zoom : veuillez indiquer la largeur dans la page de description des produits', 'BIG_IMAGE_WIDTH', '640', 'Veuillez indiquer le nombre de pixels pour la largeur des images zoom dans la page de description des articles', 4, 13, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(582, 'Image Zoom : veuillez indiquer la hauteur dans la page de description des produits', 'BIG_IMAGE_HEIGHT', '', 'Veuillez indiquer le nombre de pixels pour la hauteur des images zoom dans la page de description des articles', 4, 14, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(583, 'Image Moyenne : veuillez indiquer la largeur dans la page de description des produits', 'MEDIUM_IMAGE_WIDTH', '250', 'Veuillez indiquer le nombre de pixels pour la largeur des images moyenne dans la page de description des articles', 4, 11, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(584, 'Image Moyenne : veuillez indiquer la hauteur dans la page de description des produits', 'MEDIUM_IMAGE_HEIGHT', '', 'Veuillez indiquer le nombre de pixels pour la hauteur des images moyenne dans la page de description des articles', 4, 12, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(585, 'Image Zoom : veuillez indiquer la largeur dans la page de description des produits', 'BIG_IMAGE_WIDTH', '640', 'Veuillez indiquer le nombre de pixels pour la largeur des images zoom dans la page de description des articles', 4, 13, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(586, 'Image Zoom : veuillez indiquer la hauteur dans la page de description des produits', 'BIG_IMAGE_HEIGHT', '', 'Veuillez indiquer le nombre de pixels pour la hauteur des images zoom dans la page de description des articles', 4, 14, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(587, 'Afficher un d&eacute;partement de l''entreprise dans le formulaire Contactez-nous', 'CONTACT_DEPARTMENT_LIST', '', 'Veuillez indiquer le ou les d&eacute;partements &agrave; contacter qui seront list&eacute;s dans le formulaire Contactez-nous.<br /><br /> <font color="#FF0000"><strong>Notes :</strong></font> <br /><br />- la liste des d&eacute;partements doit &ecirc;tre sous le forme: <strong>D&eacute;partement1<departement1@mondomaine.com>, D&eacute;partement2<departement2@mondomaine.com> s&eacute;par&eacute; par une virgule</strong><br />- Le mail de r&eacute;ception par d&eacute;faut est votre mail de contact de la boutique</font><br /><br />', 1, 5, NULL, '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(588, 'Etat par d&eacute;faut pour le tracking des exp&eacute;ditions', 'DEFAULT_ORDERS_STATUS_TRACKING_ID', '1', 'Quand une nouvelle g&eacute;n&eacute;ration d''&eacute;dition de facture/commande PDF est cr&eacute;&eacute;e, ce statut de tracking lui sera assign&eacute;.', 0, 6, '1000-01-01 00:00:00', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(589, 'Souhaitez-vous autoriser la pr&eacute;-commande des produits si l''autorisation des achats hors stock est sur True (en cours de test)', 'PRE_ORDER_AUTORISATION', 'false', 'Si vous autorisez la pr&eacute;-commande, le bouton Produit Epuis&eacute; n''apparaitra plus et le client pourra passer sa pr&eacute;-commande.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 9, 3, '2007-05-21 14:23:13', '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(590, 'R&eacute;ception d''un email si un client met un commentaire', 'REVIEW_COMMENT_SEND_EMAIL', 'false', 'Si client met un commentaire sur un produit vous pourrez recevoir un email d''alerte.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 14, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(591, 'Souhaitez-vous modifier le prix public d''achat lors du passage d''un t&eacute;l&eacute;phone mobile ou une tablette?', 'PRICE_MOBILE_ACCEPT', 'false', 'Vous pouvez augmenter ou diminuer le prix d''achat des produits si la visite provient d''un t&eacute;l&eacute;phone mobile ou d''une tablette. <br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 22, 3, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(592, 'Montant de l''augmentation / diminution du tarif pour les t&eacute;l&eacute;phones mobiles et les tablettes', 'PRICE_MOBILE_POURCENTAGE', '0', '<br /><br /> <font color="#FF0000"><strong>Notes :</strong></font> <br /><br />- L''augmentation s''applique uniquement sur les prix B2C. <br /><br />- 10 = 10% d''augmentation (Veuillez &agrave; ne pas utiliser le %)<br />- 0 pour un prix normal', 22, 4, '1000-01-01 00:00:00', '1000-01-01 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(593, 'Souhaitez-vous afficher une devise automatique par d&eacute;faut en fonction du continent ?', 'CONFIGURATION_CURRENCIES_GEOLOCALISATION', 'false', 'En fonction de la provenance du client et de son continent, le tarif du produit prend par d&eacute;faut la devise du continent du client.<br /><br /><u><strong>Note :</strong></u><br />- Les devises par d&eacute;faut impl&eacute;ment&eacute;es sont : USD, EUR, CAD <br />- Si le client provient d''un autre continent, ce sera la devise par d&eacute;faut qui s''affichera.<br />- L''ajustement automatique des devises en fonction de la langue ne fonctionne pas dans ce cas.<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', 1, 9, NULL, '1000-01-01 00:00:00', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(594, 'Taille des colonnes du site', 'GRID_CONTAINER_WITH', '12', 'Veuillez indiquer une valeur num&eacute;rique.', 43, 5, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(595, 'Taille des colonnes de contenu du site', 'GRID_CONTENT_WITH', '8', 'Veuillez indiquer une valeur num&eacute;rique.', 43, 6, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(687, 'Souhaitez-vous votre template pleine page ou centr&eacute; ?', 'BOOTSTRAP_CONTAINER', 'container', 'Veuillez indiquer votre pr&eacute;f&eacuterence', 43, 1, NULL, '2015-02-19 21:46:19', NULL, 'osc_cfg_select_option(array(''container-fluid'', ''container''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(599, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_NEW_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2013-09-01 11:31:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(600, 'Installed Modules', 'MODULE_MODULES_BOXES_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2013-09-01 11:31:33', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(601, 'Souhaitez-vous afficher le prix si celui-ci est &eacute;gal &agrave; 0', 'NOT_DISPLAY_PRICE_ZERO', 'true', 'Affiche ou non le tarif du produit si celui-ci est &eacute;gal &agrave; 0.<br><br><i>(Valeur True = Oui - Valeur False = Non)</i>', 22, 2, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(602, 'Veuillez indiquer le nombre maximal de commentaires clients que vous souhaitez afficher', 'MAX_DISPLAY_NEW_REVIEWS', '5', 'Veuillez indiquer une valeur num&eacute;rique.', 3, 8, '2007-06-02 15:39:18', '2006-04-09 16:13:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(603, 'Installed Modules', 'MODULE_ADMIN_DASHBOARD_INSTALLED', 'd_total_month.php;d_total_ca_by_year.php;d_total_revenue.php;d_total_customers.php;d_orders.php;d_twitter.php', 'This is automatically updated. No need to edit.', 6, 0, '2013-12-16 18:12:18', '2013-12-16 18:11:51', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(604, 'Souhaitez-vous afficher les derni&egrave;res commandes ?', 'MODULE_ADMIN_DASHBOARD_ORDERS_STATUS', 'True', 'Module qui affiche les derni&egrave;res commandes pass&eacute;es', 6, 1, NULL, '2013-12-16 18:11:57', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(605, 'Combien de commande souhaitez-vous afficher ?', 'MODULE_ADMIN_DASHBOARD_ORDERS_LIMIT', '10', 'Veuillez indiquer le nombre de commande &agrave; afficher.', 6, 2, NULL, '2013-12-16 18:11:57', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(606, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_ORDERS_SORT_ORDER', '60', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 3, NULL, '2013-12-16 18:11:57', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(607, 'Souhaitez-vous faire apparaitre le graphique sur votre tableau concernant votre chiffre d''affaires total par ann&eacute;e ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS', 'True', 'Le chiffre d''affaires comprend toutes les ventes effectu&eacute;es et liv&eacute;es dans lann&eacute;e calcul&eacute;es en fonction du sous-total ?', 6, 1, NULL, '2013-12-16 18:12:06', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(608, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_SORT_ORDER', '30', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 0, NULL, '2013-12-16 18:12:06', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(609, 'Souhaitez-vous connaitre l&eacute;volution de nombre de clients ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_STATUS', 'True', 'Nombre de Clients inscrit au cours des 30 derniers jours', 6, 1, NULL, '2013-12-16 18:12:10', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(610, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_SORT_ORDER', '50', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 2, NULL, '2013-12-16 18:12:10', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(611, 'Souhaitez-vous faire apparaitre le graphique sur votre tableau concernant l''&eacute;volution du chiffre d''affaires par mois ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_STATUS', 'True', 'Le chiffre d''affaires comprend les ventes qui ont &eacute;t&eacute; r&eacute;alis&eacute;es dans le mois ?', 6, 1, NULL, '2013-12-16 18:12:14', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(612, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_SORT_ORDER', '20', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 3, NULL, '2013-12-16 18:12:14', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(613, 'Souhaitez-vous faire apparaitre le graphique sur votre tableau concernant votre chiffre d''affaires journalier ?', 'MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_STATUS', 'True', 'Le chiffre d''affaires comprend les ventes qui ont les statuts suivants, en attente, en cours, livr&eacute; ?', 6, 1, NULL, '2013-12-16 18:12:18', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(614, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TOTAL_REVENUE_SORT_ORDER', '40', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 0, NULL, '2013-12-16 18:12:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(615, 'Souhaitez-vous utiliser le module twitter pour envoyer des Twits', 'MODULE_ADMIN_DASHBOARD_TWITTER_STATUS', 'True', 'Ce module n&eacute;cessite de cr&eacute;er un compte sur twitter.com pour pouvoir l''utiliser.<br /><br /><b><i>Note :</i></b>Vous devez aussi vous identifier sur cette page <a href="http://dev.twitter.com/apps/" target="_blank">http://dev.twitter.com/apps/</a> (connectez-vous avant sur votre compe) puis remplissez le formulaire et r&eacute;cup&eacute;rez les codes confidentiels.<br /><br /><font color="red"><b>le mode access level doit etre en read / write</b></font>', 6, 1, NULL, '2013-12-16 18:12:22', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(616, 'Veuillez ins&eacute;rer votre code consumerKey', 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_KEY', '', 'Veuillez indiquer le code consumerKey', 6, 2, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(617, 'Veuillez ins&eacute;rer votre code consumerSecret', 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_SECRET', '', 'Veuillez indiquer le code consumerSecret', 6, 3, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(618, 'Veuillez ins&eacute;rer votre code accessToken', 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN', '', 'Veuillez indiquer le code accessToken<br /><br /><b>Note : </b> cliquer sur le bouton My access token ', 6, 3, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(619, 'Veuillez ins&eacute;rer votre code accessTokenSecret', 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN_SECRET', '', 'Veuillez indiquer le code accessTokenSecret<br /><br /> <b>Note : </b> cliquer sur le bouton My access token', 6, 3, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(620, 'Ordre de tri', 'MODULE_ADMIN_DASHBOARD_TWITTER_SORT_ORDER', '70', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 4, NULL, '2013-12-16 18:12:22', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(623, 'Installed Modules', 'MODULE_MODULES_HEADER_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(624, 'Installed Modules', 'MODULE_MODULES_FOOTER_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:20', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(625, 'Installed Modules', 'MODULE_MODULES_ADVANCED_SEARCH_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:23', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(626, 'Installed Modules', 'MODULE_MODULES_SITEMAP_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:25', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(627, 'Installed Modules', 'MODULE_MODULES_CONTACT_US_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:28', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(628, 'Installed Modules', 'MODULE_MODULES_BLOG_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:31', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(629, 'Installed Modules', 'MODULE_MODULES_BLOG_CONTENT_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:34', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(630, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_INFO_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:42', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(631, 'Installed Modules', 'MODULE_MODULES_SHOPPING_CART_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:44', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(632, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_SHIPPING_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:47', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(633, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_PAYMENT_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:01:56', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(634, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_CONFIRMATION_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:03', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(635, 'Installed Modules', 'MODULE_MODULES_CHECKOUT_SUCCESS_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:07', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(636, 'Installed Modules', 'MODULE_MODULES_FRONT_PAGE_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:11', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(637, 'Installed Modules', 'MODULE_MODULES_INDEX_CATEGORIES_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:13', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(638, 'Installed Modules', 'MODULE_MODULES_LOGIN_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:15', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(639, 'Installed Modules', 'MODULE_MODULES_ACCOUNT_CUSTOMERS_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:18', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(640, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_HEART_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:23', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(641, 'Installed Modules', 'MODULE_MODULES_PRODUCTS_SPECIAL_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:02:26', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(642, 'Nombre minimum de caract&egrave;res pour le T&eacute;l&eacute;phone Mobile', 'ENTRY_CELLULAR_PHONE_MIN_LENGTH', '3', 'Indiquer le nombre minimum de caract&egrave;res pour le t&eacute;l&eacute;phone mobile.', 16, 10, '2006-10-23 22:49:04', '2006-04-09 16:13:48', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(643, 'Nombre minimum de caract&egrave;res pour le t&eacute;l&eacute;phone Mobile', 'ENTRY_CELLULAR_PHONE_PRO_MIN_LENGTH', '3', 'Indiquer le nombre de caract&egrave;res minimum requis pour le t&eacute;l&eacute;phone mobile.', 19, 13, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(644, 'Nombre minimum de caract&egrave;res pour la taxe provinviale', 'ENTRY_CODE_TAXE_PROVINCIALE_MIN_LENGTH', '3', 'Indiquer le nombre de caract&egrave;res minimum requis pour la taxe provinciale.', 19, 15, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(645, 'Nombre minimum de caract&egrave;res pour la taxe f&eacute;d&eacute;rale', 'ENTRY_CODE_TAXE_FEDERALE_MIN_LENGTH', '3', 'Indiquer le nombre de caract&egrave;res minimum requis pour la taxe f&eacute;d&eacute;rale.', 19, 16, '2006-10-29 16:21:09', '2006-10-19 00:00:00', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(646, 'Installed Modules', 'MODULE_HEADER_TAGS_INSTALLED', '', 'This is automatically updated. No need to edit.', 6, 0, NULL, '2014-02-07 20:07:32', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(647, 'Souhaitez-vous afficher l''acceptation de la commande par le consommateur (loi Hamon)', 'CONFIGURATION_LAW_HAMON', 'false', 'La loi Hamon (loi francaise) oblige le consommateur &agrave; accepter sa prise de commande. Souhaitez-vous l''activer ?<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', 25, 3, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''true'', ''false''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(648, 'Quel type de menu souhaitez-vous voir apparaitre dans votre administration ?', 'CONFIGURATION_MENU_NAVIGATION', 'computer', 'Affiche soit un menu horizontal, soit un menu vertical dans l''administration.<br /><br /><i>(Valeur True = Menu Vertical - Valeur False = Menu Horizontal)</i>', 1, 400, NULL, '2006-04-09 16:13:47', NULL, 'osc_cfg_select_option(array(''computer'', ''tablet'', ''both''),');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(649, 'Host SMTP', 'EMAIL_SMTP_HOSTS', '', 'Indiquer le SMTP host', 12, 6, NULL, '2014-05-16 11:21:13', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(650, 'Authentication SMTP', 'EMAIL_SMTP_AUTHENTICATION', 'true', 'Souhaitez-vous une authentication SMTP ?', 12, 7, NULL, '2014-05-16 11:21:13', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(651, 'Mot de passe SMTP', 'EMAIL_SMTP_PASSWORD', '', 'Ajouter un mot de passe pour le protocol SMTP', 12, 8, NULL, '2014-05-16 11:21:13', 'osc_cfg_password', 'osc_cfg_input_password(');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(652, 'Nom utilisateur SMTP', 'EMAIL_SMTP_USER', '', 'Ajouter un utilisateur pour le protocol SMTP', 12, 9, NULL, '2014-05-16 11:21:13', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(653, 'SMTP transfert', 'EMAIL_SMTP_REPLYTO', '', 'Ajuter un transfert sur une adresse', 12, 10, NULL, '2014-05-16 11:21:13', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(654, 'Souhatez-vous utiliser le protocol de s&eacute;curisation SMTP', 'EMAIL_SMTP_SECURE', 'no', 'utiliser le protocol de s&eacute;curisation SMTP', 12, 11, NULL, '2014-06-02 11:52:39', NULL, 'osc_cfg_select_option(array(''no'', ''ssl'', ''tls''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(655, 'Souhaitez-vous modifier le Port SMTP', 'EMAIL_SMTP_PORT', '25', ' En fonction des prestataires le port smtp peut etre modifi&eacute;', 12, 12, NULL, '2014-06-02 11:52:39', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(656, 'Souhaitez-vous activer le webservice with Odoo ?', 'ODOO_ACTIVATE_WEB_SERVICE', 'false', 'Activation permettant d''utiliser le webservice Odoo<br /><br/><Strong>Note :</strong><br /><br/> - Veuillez bien faire attention aux nformations ci-dessous.<br />- Veuillez installer les modules Odoo pour un bon fonctionnement du Web service.<br /><br />- En cas d''erreur, fausse manipulation ou demauvais flux de transmission dinformation, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur true = Activation - Valeur False = Non)</i>', 44, 1, NULL, '2014-11-03 13:54:43', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(657, '- Etape 1 Connexion - Veuillez ins&eacute;rer l''adresse du serveur utilisant Odoo', 'ODOO_WEBSERVER_WEB_SERVICE', '', 'Veuillez indiquer  l''adresse du serveur utilisant Odoo(sans http) <br /><br />- exemple : mondomaine.com', 44, 2, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(658, 'Souhaitez-vous mettre votre boutique en gestion mode priv&eacute;e ?', 'MODE_VENTE_PRIVEE', 'false', 'En fonction de votre choix, le client devra cr&eacute;er ou non un compte pour acc&eacuteder au catalogue sur les modes B2B/B2C - B2C- B2C', 22, 2, NULL, '2014-08-20 15:51:50', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(659, '- Etape 2 Connexion - Veuillez ins&eacute;rer le port utilis&eacute; par Postgresql', 'ODOO_PORT_WEB_SERVICE', '', 'Veuillez indiquer le num&eacute;ro de port utilis&eacute; par Odoo <br /><br />- Exemple : 8069', 44, 3, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(660, '- Etape 3 Connexion - Veuillez ins&eacute;rer User de connexion &agrave; Odoo', 'ODOO_USER_WEB_SERVICE', '', 'Veuillez indiquer user Odoo. C''est un login de connexion param&eacute;tr&eacute; dans odoo, Cela peut etre un email par exemple.<br /><br /><strong><u>Note :</u></strong><br /><br /> - Pour assurer une s&eacutecurit&eacute; maximale, veuillez ne pas rentrer le user Admin de Odoo', 44, 4, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(661, '- Etape 4 Connexion - Veuillez ins&eacute;rer le mot de passe de connexion &agrave; Odoo', 'ODOO_PASSWORD_WEB_SERVICE', '', 'Veuillez indiquer le mot de passe de connexion &agrave; Odoo', 44, 5, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(662, '- Etape 5 Connexion - Veuillez ins&eacute;rer le nom de la base de donn&eacute;es Odoo', 'ODOO_DATABASE_NAME_WEB_SERVICE', '', 'Veuillez indiquer le nom de la base de donn&eacute;es Odoo', 44, 6, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(663, 'Souhaitez-vous etre averti par email d''une erreur de connexion de ClicShopping vers odoo ?', 'ODOO_EMAIL_WEB_SERVICE', 'true', 'Une information vous sera envoy&eacute;e en cas de d&eacute;tection d''une erreur.<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', 44, 1, NULL, '2014-11-03 13:54:43', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(664, 'Souhaitez-vous activer le web service pour enregistrer les clients cr&eacute;ant un compte ?', 'ODOO_ACTIVATE_CUSTOMERS_CATALOG', 'false', 'Enregistre tous les clients cr&eacute;ant un compte ou modifiant son compte<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', 44, 8, NULL, '2014-11-03 13:54:43', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(666, 'Souhaitez-vous activer le web service pour enregistrer les modifications et cr&eacute;ations de produits dans l''administration vers Odoo?', 'ODOO_ACTIVATE_PRODUCTS_ADMIN', 'false', 'Enregistre toutes les modifications ou cr&eacute;ations de produits dans l''administration vers Odoo', 44, 11, NULL, '2014-11-03 13:54:43', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(667, 'Souhaitez-vous activer le web service pour enregistrer les mise &agrave; des commandes de l''administration vers Odoo?', 'ODOO_WEB_SERVICE_ORDERS_ADMIN', 'false', 'Enregistre la mises &agrave; jour de la commande de l''administration vers Odoo<br /><br /><strong>Note :</strong><br /><br />- N''oubliez pas de v&eacute;rifier les codes de taxes dans la section ClicShopping Lieux et taxes/Taux fiscaux<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>', 44, 12, NULL, '2014-11-03 13:54:43', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(668, '- Etape 6 Connexion - Veuillez indiquer le nom de votre entreprise cr&eacute;e dans Odoo', 'ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE', '', 'Veuillez indiquer le nom de votre entreprise cr&eacute;&eacute;e dans l''administration Odoo<br />- Veuillez respecter correctement le nom de l''entreprise inscrite dans Odoo', 44, 7, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(669, '- Etape 1 Comptabilit&eacute; - Veuillez indiquer le num&eacutero comptable de compte de vente de marchandises de Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_SELL', '', 'Odoo utilise un compte de comptabilit&eacute; pour enregistrer les factures (vente de marchandises).<br /><br /><strong>Note :</strong><br /><br />- En France, c''est le compte 707100 (comptabilit&eacute; francaise)<br />- Veuillez respecter correctement le num&eacute;ro de compte de Odoo', 44, 14, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(670, '- Etape 2 Comptabilit&eacute; - Veuillez indiquer le num&eacutero comptable de compte client d''achat de marchandises de Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_PURCHASE', '', 'Odoo utilise un compte de comptabilit&eacute; pour enregistrer les factures (achat client).<br /><br /><strong>Note :</strong><br /><br />- En France, c''est le compte 411100 (comptabilit&eacute; francaise)<br />- Veuillez respecter correctement le num&eacute;ro de compte de Odoo', 44, 15, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(671, '- Etape 3 Comptabilit&eacute; - Veuillez indiquer le num&eacutero comptable des frais d''exp&eacute;dition de marchandise de Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_SHIPPING', '', 'Odoo utilise un compte de comptabilit&eacute; pour enregistrer les frais d''exp&eacute;dition de marchandise.<br /><br /><strong>Note :</strong><br /><br />- En France, c''est le compte 708500 (comptabilit&eacute; francaise)<br />- Veuillez respecter correctement le num&eacute;ro de compte de Odoo', 44, 16, NULL, '2014-11-03 13:54:43', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(672, 'Souhaitez-vous permettre le changement d''adresse de facturation par d&eacutefaut dans ClicShopping ?', 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG', 'false', 'Cela permet au client de changer son adresse de facturation par d&eacute;faut.<br /><br /><strong>Note :</strong><br /><br />- Nous vous recommandons fortement de laisser cette option sur false quand vous utilisez l''option facturation (ci-dessous). Accepter le changement d''adresse principal, aura une <u>r&eacutepercution importante dans toute la gestion de vos factures dans Odoo</u><br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>.', 44, 10, NULL, '2014-11-05 14:57:43', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(673, '- Etape 4 Comptabilit&eacute; - Veuillez indiquer len num&eacutero comptable des Rabais/Remises/Ristournes de Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_DISCOUNT', '', 'Odoo utilise un compte de comptabilit&eacute; pour enregistrer les RRR de marchandise.<br /><br /><strong>Note :</strong><br /><br />- En France, c''est le compte 709700 (comptabilit&eacute; francaise)<br />- Veuillez respecter correctement le num&eacute;ro de compte de Odoo', 44, 16, NULL, '2014-12-04 14:26:21', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(674, 'Souhaitez-vous activer le web service pour enregistrer les cr&eacute;ations de commande du catalogue vers Odoo ?', 'ODOO_ACTIVATE_ORDER_CATALOG', 'none', 'Le type de facturation enregistr&eacute; doit etre s&eacute;lectionn&eacute; en fonction de votre process.<br /><br /><strong>Note :</strong><br /><br />- None : Aucun enregistrement dans Odoo ne sera enregistr&eacute; dans odoo.<br/><br />- Order : Enregistrer la commande du site comme commande dans Odoo. Vous devrez traiter et compl&eacute;ter par la suite le traitement en actualisant les statuts de ClicShopping.<br /><br />- Invoice : La commande effectu&eacute;e par le client est directement enregistr&eacute;e comme facture.<br /><br />- Conseil : N''oubliez pas d''adapter votre statut de commande par d&eacute;faut en fonction de vos choix.', 44, 10, NULL, '2014-12-04 14:26:21', NULL, 'osc_cfg_select_option(array(''none'', ''order'', ''invoice''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(675, '- Etape 7 Export - Souhaitez-vous exporter vos produits dans Odoo ?', 'ODOO_EXPORT_PRODUCTS', 'false', 'Exporte tous les produits de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de produits, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes : <strong><br /><br />- 1 - Si vous avez des fournisseurs, veuillez importer vos fournisseurs dans une premier temps.<br />- 2 - Pour commencer le processus d''export, veuillez <strong>s&eacute;lectionner l''option Export</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus d''exportation <br />- 3 - D&eacute;s que le processus d''export est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d''exportation ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des produits - Valeur False = Non)</i>', 44, 87, '2014-12-04 14:26:21', '2014-12-04 14:26:21', 'osc_bulk_product', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(676, '- Etape 3 Export - Souhaitez-vous exporter vos clients dans Odoo ?', 'ODOO_EXPORT_CUSTOMERS', 'false', 'Exporte tous les clients de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de clients, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes :</strong><br /><br />- 1- Pour commencer le processus d''export, veuillez <strong>s&eacute;lectionner l''option Export</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus d''exportation <br />- 2 - D&eacute;s que le processus d''export est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d''exportation ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des clients - Valeur False = Non)</i>', 44, 83, '2014-12-04 14:26:21', '2014-12-04 14:26:21', 'osc_bulk_customer', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(677, 'Souhaitez-vous activer la cr&eacute;ation  ou modification de Fournisseurs dans Odoo ?', 'ODOO_ACTIVATE_SUPPLIERS_ADMIN', 'false', 'Enregistre toutes les modifications ou cr&eacute;ation de fournisseur dans l''administration vers Odoo', 44, 8, NULL, '2014-12-04 14:26:21', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(678, 'Souhaitez vous activer le web service pour cr&eacute;er / modifier les clients de l administration ?', 'ODOO_ACTIVATE_CUSTOMERS_ADMIN', 'false', 'Ernregistre toues modification ou cr&eacute;ation dans ladministration vers Odoo', 44, 9, NULL, '2014-11-03 13:54:17', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(679, '- Etape 4 Export - Souhaitez-vous exporter vos fournisseurs dans Odoo ?', 'ODOO_EXPORT_SUPPLIERS', 'false', 'Exporte tous les fournisseurs de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de fournisseurs, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes :</strong><br /><br />- 1- Pour commencer le processus d''export, veuillez <strong>s&eacute;lectionner l''option Export</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus d''exportation <br />- 2 - D&eacute;s que le processus d''export est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d''exportation ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des clients - Valeur False = Non)</i>', 44, 84, '2015-01-12 20:29:02', '2015-01-12 20:29:02', 'osc_bulk_suppliers', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(680, '- Etape 1 Export - Souhaitez-vous configurer votre entrepot de stockage ?', 'ODOO_WHAREHOUSE_CONFIG', 'false', 'Cr&eacute;e votre entrepot de Stockage ClicShopping.<br /><br /><strong>Note :</strong><br /><br />- Cette configuration va vous permettre de g&eacute;rer vos stocks dans Odoo.<br /><br />- 1- Pour commencer le processus de cr&eacute;ation, veuillez <strong>s&eacute;lectionner l''option Configure</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus de configuration de l''entrepot <br />- 2 - D&eacute;s que le processus de configuration est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus de configuration ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur configure = Configuration Entrepot - Valeur False = Non)</i>', 44, 81, '2015-01-12 20:29:02', '2015-01-12 20:29:02', 'osc_config_create_wharehouse', 'osc_cfg_select_option(array(''configure'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(681, '- Etape 6 Export - Souhaitez-vous exporter vos marques de produit dans Odoo ?', 'ODOO_EXPORT_MANUFACTURER', 'false', 'Exporte toutes les marques de produit de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de clients, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes :</strong><br /><br />- 1- Pour commencer le processus d''export, veuillez <strong>s&eacute;lectionner l''option Export</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus d''exportation <br />- 2 - D&eacute;s que le processus d''export est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d''exportation ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des marques - Valeur False = Non)</i>', 44, 86, '2015-01-12 20:29:02', '2015-01-12 20:29:02', 'osc_bulk_manufacturer', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(682, '- Etape 5 Export - Souhaitez-vous exporter vos cat&eacute;gories de produit dans Odoo ?', 'ODOO_EXPORT_CATEGORIES', 'false', 'Exporte toutes les cat&eacute;gories de produit de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de clients, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes :</strong><br /><br />- 1- Pour commencer le processus d''export, veuillez <strong>s&eacute;lectionner l''option Export</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus d''exportation <br />- 2 - D&eacute;s que le processus d''export est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d''exportation ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des cat&eacute;gories - Valeur False = Non)</i>', 44, 85, '2015-01-12 20:29:02', '2015-01-12 20:29:02', 'osc_bulk_categories', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(683, '- Etape 2 Export - Souhaitez-vous exporter les groupes clients dans Odoo ?', 'ODOO_EXPORT_CUSTOMERS_GROUP', 'false', 'Exporte toutes les groupes client de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de clients, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes :</strong><br /><br />- 1- Pour commencer le processus d''export, veuillez <strong>s&eacute;lectionner l''option Export</strong>, puis valider et r&eacute;-&eacute;diter l''option. Cela d&eacute;clenchera le processus d''exportation <br />- 2 - D&eacute;s que le processus d''export est termin&eacute;, veuillez <strong>s&eacute;lectionner l''option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d''exportation ult&eacute;rieurement.<br /><br />- En cas d''erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des marques - Valeur False = Non)</i>', 44, 82, '2015-01-30 19:58:59', '2015-01-30 19:58:59', 'osc_bulk_customer_group', 'osc_cfg_select_option(array(''export'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(686, 'Souhaitez-vous activer le web service pour enregistrer les groupes clients dans Odoo ?', 'ODOO_ACTIVATE_CUSTOMERS_GROUP', 'false', 'Enregistre tous les groupes clients dans Odoo.<br /><br /><i>(Valeur true = Oui - Valeur False = Non)</i>', 44, 8, NULL, '2015-01-30 19:58:59', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(688, 'Souhaitez-vous activer le web service pour enregistrer les marques dans Odoo ?', 'ODOO_ACTIVATE_MANUFACTURER_ADMIN', 'false', 'Enregistre toutes les marques.<br /><br /><i>(Valeur true = Oui - Valeur False = Non)</i>', 44, 9, NULL, '2015-01-30 19:58:59', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(690, 'Installed Modules', 'MODULE_MODULES_FOOTER_SUFFIX_INSTALLED', 'sf_footer_suffix_copyright.php', 'This is automatically updated. No need to edit.', 6, 0, '2015-04-05 11:16:32', '2015-04-05 11:16:27', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(691, 'Souhaitez-vous activer ce module ?', 'MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS', 'True', 'Souhaitez vous activer ce module &agrave; votre boutique ?', 6, 1, NULL, '2015-04-05 11:16:32', NULL, 'osc_cfg_select_option(array(''True'', ''False''), ');
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(692, 'Ordre de tri d''affichage', 'MODULES_FOOTER_SUFFIX_COPYRIGHT_SORT_ORDER', '100', 'Ordre de tri pour l''affichage (Le plus petit nombre est montr&eacute; en premier).', 6, 3, NULL, '2015-04-05 11:16:32', NULL, NULL);
INSERT INTO `configuration` (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES(684, 'Souhaitez-vous activer le web service pour enregistrer toutes les demandes de support des clients qui sont enregistr&eacute;s sur le site ?', 'ODOO_ACTIVATE_SUPPORT_CONTACT_US_ODOO', 'false', 'Enregistre toutes les demandes de support dans Odoo de tous les clients qui sont enregistr&eacute;s dans ClicShopping.<br /><br /><i>(Valeur true = Oui - Valeur False = Non)</i>', 44, 8, NULL, '2015-02-19 21:46:19', NULL, 'osc_cfg_select_option(array(''true'', ''false''), ');



CREATE TABLE IF NOT EXISTS `configuration_group` (
  `configuration_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `configuration_group_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `configuration_group_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(5) DEFAULT NULL,
  `visible` int(1) DEFAULT '1',
  PRIMARY KEY (`configuration_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(1, 'Gestion de ma boutique', 'Informations g&eacute;n&eacute;rales sur la boutique.', 1, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(2, 'Gestion de la carte de cr&eacute;dit', 'Valeur minimum pour : fonctions / donn&eacute;es', 2, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(3, 'Gestion des valeurs maximales et minimales', 'Valeurs pour : fonctions / donn&eacute;es', 3, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(4, 'Gestion des images', 'Configuration des images', 4, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(5, 'Gestion des d&eacute;tails clients B2C', 'Configuration du compte client.', 5, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(6, 'Gestion des options des Modules', 'Cach&eacute; de la configuration.', 6, 0);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(7, 'Gestion des exp&eacute;dition/Emballage', 'Options de livraison possibles dans cette boutique.', 7, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(8, 'Gestion de la liste des produits', 'Options de configuration des listes de produits.', 8, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(9, 'Gestion des stock', 'Options de configuration du stock.', 9, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(10, 'Gestion des logging', 'Options de configuration du Logging.', 10, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(11, 'Gestion du cache', 'Options de configuration du cache.', 11, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(12, 'Gestion des options de mail', 'Configuration g&eacute;n&eacute;rale pour le ''client mail'' et les emails au format HTML.', 12, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(13, 'Gestion des t&eacute;l&eacute;chargements', 'Options des produits t&eacute;l&eacute;chargeable.', 13, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(14, 'Gestion de la compression GZip', 'Options de compression GZip.', 14, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(15, 'Gestion des sessions', 'Session options', 15, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(16, 'Gestion des valeurs minimum pour les clients B2C', 'Valeur minimum pour les champs des clients B2C', 16, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(17, 'Gestion B2B', 'Gestion g&eacute;n&eacute;ral de la B2B', 17, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(18, 'Gestion des d&eacute;tails clients B2B', 'Configuration sur les d&eacute;tails des inscriptions clients en groupe B2B', 18, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(19, 'Gestion des valeurs minimum pour les clients B2B', 'Valeur minimum pour les champs des clients B2B', 19, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(20, 'Gestion des valeurs maximum pour les clients B2B', 'Valeur maximum pour les champs des clients B2B', 20, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(21, 'Gestion des valeurs maximum', 'Valeur maximum pour : fonctions / donn&eacute;es', 21, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(22, 'Gestion B2C', 'Gestion g&eacute;n&eacute;rale de la B2C', 22, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(23, 'Gestion des images', 'Configuration des images', 23, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(24, 'Gestion  de la mise &agrave; jour rapide', 'Veuillez choisir ce que vous voulez pouvoir modifier dans le module Mises &agrave; jour rapides.', 24, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(25, 'Gestion de la l&eacute;gislation r&eacute;glementaire', 'Options r&eacute;glementaires concernant la gestion de votre site', 25, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(26, 'Gestion de la configuration des factures et des bons exp&eacute;', 'Configuration des factures et des bons exp&eacute;ditions', 26, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(27, 'Gestion du R&eacute;f&eacute;rencement et des statistiques', 'Options concernant la gestion du r&eacute;f&eacute;rencement et des statistiques', 27, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(28, 'Gestion des produits ont &eacute;galement achet&eacute;', 'Affichage des informations dans la fiche de description des produits', 28, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(29, 'Gestion de l''Affichage du listing des promotions', 'Fiche de Description du Listing des promotions', 29, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(30, 'Gestion de la page d''Accueil et des cat&eacute;gories', 'Valeurs concernant la gestion de la page d''accueil et des cat&eacute;gories', 30, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(31, 'Gestion des boxes gauches et droites', 'Valeur concernant la gestion des boxes gauches et droites', 31, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(32, 'Gestion des Commentaires', 'Valeur concernant les commentaires', 32, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(33, 'Gestion du listing des nouveaut&eacute;s', 'Valeur concernant le listing des nouveaut&eacute;s', 33, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(34, 'Gestion des URL du site', 'Options de gestion des URL', 34, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(35, 'Gestion du listing des coups de coeur', 'Valeurs concernant le listing des coups de coeur', 35, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(36, 'Gestion des imports / exports', 'Configuration des imports et des exports.', 36, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(37, 'Gestion des promotions pour la page d''acceuil', 'Gestion des promotions pour la page d''acceuil', 37, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(39, 'Gestion des produits similaires', 'Information concernant les produits similaires', 2, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(40, 'Gestion des produits crois&eacute;s', 'Information concernant les produits crois&eacutes', 2, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(43, 'Configuration g&eacute;n&eacute;rale et diverse sur le design', 'Configuration g&eacute;n&eacute;rale  et diverse sur du design', 43, 1);
INSERT INTO `configuration_group` (`configuration_group_id`, `configuration_group_title`, `configuration_group_description`, `sort_order`, `visible`) VALUES(44, 'Web Service', 'Connect via webservice at new external application', NULL, 1);

CREATE TABLE IF NOT EXISTS `contact_customers` (
  `contact_customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_department` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_enquiry` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_date_added` datetime NOT NULL,
  `languages_id` int(11) NOT NULL,
  `contact_customers_archive` tinyint(1) NOT NULL,
  `contact_customers_status` tinyint(1) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `contact_telephone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`contact_customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `contact_customers_follow` (
  `id_contact_customers_follow` int(11) NOT NULL AUTO_INCREMENT,
  `contact_customers_id` int(11) NOT NULL,
  `administrator_user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_response` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contact_date_sending` datetime NOT NULL,
  PRIMARY KEY (`id_contact_customers_follow`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `counter` (
  `startdate` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `counter` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `counter_history` (
  `month` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `counter` int(12) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_2` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_3` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `address_format_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`countries_id`),
  KEY `IDX_COUNTRIES_NAME` (`countries_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=240 ;

INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(1, 'Afghanistan', 'AF', 'AFG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(2, 'Albania', 'AL', 'ALB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(3, 'Algeria', 'DZ', 'DZA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(4, 'American Samoa', 'AS', 'ASM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(5, 'Andorra', 'AD', 'AND', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(6, 'Angola', 'AO', 'AGO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(7, 'Anguilla', 'AI', 'AIA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(8, 'Antarctica', 'AQ', 'ATA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(9, 'Antigua and Barbuda', 'AG', 'ATG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(10, 'Argentina', 'AR', 'ARG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(11, 'Armenia', 'AM', 'ARM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(12, 'Aruba', 'AW', 'ABW', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(13, 'Australia', 'AU', 'AUS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(14, 'Austria', 'AT', 'AUT', 5, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(15, 'Azerbaijan', 'AZ', 'AZE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(16, 'Bahamas', 'BS', 'BHS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(17, 'Bahrain', 'BH', 'BHR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(18, 'Bangladesh', 'BD', 'BGD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(19, 'Barbados', 'BB', 'BRB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(20, 'Belarus', 'BY', 'BLR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(21, 'Belgium', 'BE', 'BEL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(22, 'Belize', 'BZ', 'BLZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(23, 'Benin', 'BJ', 'BEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(24, 'Bermuda', 'BM', 'BMU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(25, 'Bhutan', 'BT', 'BTN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(26, 'Bolivia', 'BO', 'BOL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(28, 'Botswana', 'BW', 'BWA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(29, 'Bouvet Island', 'BV', 'BVT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(30, 'Brazil', 'BR', 'BRA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(32, 'Brunei Darussalam', 'BN', 'BRN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(33, 'Bulgaria', 'BG', 'BGR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(34, 'Burkina Faso', 'BF', 'BFA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(35, 'Burundi', 'BI', 'BDI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(36, 'Cambodia', 'KH', 'KHM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(37, 'Cameroon', 'CM', 'CMR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(38, 'Canada', 'CA', 'CAN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(39, 'Cape Verde', 'CV', 'CPV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(40, 'Cayman Islands', 'KY', 'CYM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(41, 'Central African Republic', 'CF', 'CAF', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(42, 'Chad', 'TD', 'TCD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(43, 'Chile', 'CL', 'CHL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(44, 'China', 'CN', 'CHN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(45, 'Christmas Island', 'CX', 'CXR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(47, 'Colombia', 'CO', 'COL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(48, 'Comoros', 'KM', 'COM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(49, 'Congo', 'CG', 'COG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(50, 'Cook Islands', 'CK', 'COK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(51, 'Costa Rica', 'CR', 'CRI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(52, 'Cote D''Ivoire', 'CI', 'CIV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(53, 'Croatia', 'HR', 'HRV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(54, 'Cuba', 'CU', 'CUB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(55, 'Cyprus', 'CY', 'CYP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(56, 'Czech Republic', 'CZ', 'CZE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(57, 'Denmark', 'DK', 'DNK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(58, 'Djibouti', 'DJ', 'DJI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(59, 'Dominica', 'DM', 'DMA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(60, 'Dominican Republic', 'DO', 'DOM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(61, 'East Timor', 'TP', 'TMP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(62, 'Ecuador', 'EC', 'ECU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(63, 'Egypt', 'EG', 'EGY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(64, 'El Salvador', 'SV', 'SLV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(66, 'Eritrea', 'ER', 'ERI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(67, 'Estonia', 'EE', 'EST', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(68, 'Ethiopia', 'ET', 'ETH', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(70, 'Faroe Islands', 'FO', 'FRO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(71, 'Fiji', 'FJ', 'FJI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(72, 'Finland', 'FI', 'FIN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(73, 'France', 'FR', 'FRA', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(75, 'French Guiana', 'GF', 'GUF', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(76, 'French Polynesia', 'PF', 'PYF', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(77, 'French Southern Territories', 'TF', 'ATF', 6, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(78, 'Gabon', 'GA', 'GAB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(79, 'Gambia', 'GM', 'GMB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(80, 'Georgia', 'GE', 'GEO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(81, 'Germany', 'DE', 'DEU', 5, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(82, 'Ghana', 'GH', 'GHA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(83, 'Gibraltar', 'GI', 'GIB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(84, 'Greece', 'GR', 'GRC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(85, 'Greenland', 'GL', 'GRL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(86, 'Grenada', 'GD', 'GRD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(87, 'Guadeloupe', 'GP', 'GLP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(88, 'Guam', 'GU', 'GUM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(89, 'Guatemala', 'GT', 'GTM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(90, 'Guinea', 'GN', 'GIN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(91, 'Guinea-bissau', 'GW', 'GNB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(92, 'Guyana', 'GY', 'GUY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(93, 'Haiti', 'HT', 'HTI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(95, 'Honduras', 'HN', 'HND', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(96, 'Hong Kong', 'HK', 'HKG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(97, 'Hungary', 'HU', 'HUN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(98, 'Iceland', 'IS', 'ISL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(99, 'India', 'IN', 'IND', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(100, 'Indonesia', 'ID', 'IDN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(102, 'Iraq', 'IQ', 'IRQ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(103, 'Ireland', 'IE', 'IRL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(104, 'Israel', 'IL', 'ISR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(105, 'Italy', 'IT', 'ITA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(106, 'Jamaica', 'JM', 'JAM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(107, 'Japan', 'JP', 'JPN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(108, 'Jordan', 'JO', 'JOR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(109, 'Kazakhstan', 'KZ', 'KAZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(110, 'Kenya', 'KE', 'KEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(111, 'Kiribati', 'KI', 'KIR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(112, 'Korea, Democratic People''s Republic of', 'KP', 'PRK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(113, 'Korea, Republic of', 'KR', 'KOR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(114, 'Kuwait', 'KW', 'KWT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(115, 'Kyrgyzstan', 'KG', 'KGZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(117, 'Latvia', 'LV', 'LVA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(118, 'Lebanon', 'LB', 'LBN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(119, 'Lesotho', 'LS', 'LSO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(120, 'Liberia', 'LR', 'LBR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(122, 'Liechtenstein', 'LI', 'LIE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(123, 'Lithuania', 'LT', 'LTU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(124, 'Luxembourg', 'LU', 'LUX', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(125, 'Macau', 'MO', 'MAC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(127, 'Madagascar', 'MG', 'MDG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(128, 'Malawi', 'MW', 'MWI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(129, 'Malaysia', 'MY', 'MYS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(130, 'Maldives', 'MV', 'MDV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(131, 'Mali', 'ML', 'MLI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(132, 'Malta', 'MT', 'MLT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(133, 'Marshall Islands', 'MH', 'MHL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(134, 'Martinique', 'MQ', 'MTQ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(135, 'Mauritania', 'MR', 'MRT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(136, 'Mauritius', 'MU', 'MUS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(137, 'Mayotte', 'YT', 'MYT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(138, 'Mexico', 'MX', 'MEX', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(139, 'Micronesia, Federated States of', 'FM', 'FSM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(140, 'Moldova, Republic of', 'MD', 'MDA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(141, 'Monaco', 'MC', 'MCO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(142, 'Mongolia', 'MN', 'MNG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(143, 'Montserrat', 'MS', 'MSR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(144, 'Morocco', 'MA', 'MAR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(145, 'Mozambique', 'MZ', 'MOZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(146, 'Myanmar', 'MM', 'MMR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(147, 'Namibia', 'NA', 'NAM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(148, 'Nauru', 'NR', 'NRU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(149, 'Nepal', 'NP', 'NPL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(150, 'Netherlands', 'NL', 'NLD', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(151, 'Netherlands Antilles', 'AN', 'ANT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(152, 'New Caledonia', 'NC', 'NCL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(153, 'New Zealand', 'NZ', 'NZL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(154, 'Nicaragua', 'NI', 'NIC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(155, 'Niger', 'NE', 'NER', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(156, 'Nigeria', 'NG', 'NGA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(157, 'Niue', 'NU', 'NIU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(158, 'Norfolk Island', 'NF', 'NFK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(159, 'Northern Mariana Islands', 'MP', 'MNP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(160, 'Norway', 'NO', 'NOR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(161, 'Oman', 'OM', 'OMN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(162, 'Pakistan', 'PK', 'PAK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(163, 'Palau', 'PW', 'PLW', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(164, 'Panama', 'PA', 'PAN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(165, 'Papua New Guinea', 'PG', 'PNG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(166, 'Paraguay', 'PY', 'PRY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(167, 'Peru', 'PE', 'PER', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(168, 'Philippines', 'PH', 'PHL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(169, 'Pitcairn', 'PN', 'PCN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(170, 'Poland', 'PL', 'POL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(171, 'Portugal', 'PT', 'PRT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(172, 'Puerto Rico', 'PR', 'PRI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(173, 'Qatar', 'QA', 'QAT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(174, 'Reunion', 'RE', 'REU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(175, 'Romania', 'RO', 'ROM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(176, 'Russian Federation', 'RU', 'RUS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(177, 'Rwanda', 'RW', 'RWA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(179, 'Saint Lucia', 'LC', 'LCA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(181, 'Samoa', 'WS', 'WSM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(182, 'San Marino', 'SM', 'SMR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(183, 'Sao Tome and Principe', 'ST', 'STP', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(184, 'Saudi Arabia', 'SA', 'SAU', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(185, 'Senegal', 'SN', 'SEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(186, 'Seychelles', 'SC', 'SYC', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(187, 'Sierra Leone', 'SL', 'SLE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(188, 'Singapore', 'SG', 'SGP', 4, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(190, 'Slovenia', 'SI', 'SVN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(191, 'Solomon Islands', 'SB', 'SLB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(192, 'Somalia', 'SO', 'SOM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(193, 'South Africa', 'ZA', 'ZAF', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(195, 'Spain', 'ES', 'ESP', 3, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(196, 'Sri Lanka', 'LK', 'LKA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(197, 'St. Helena', 'SH', 'SHN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(199, 'Sudan', 'SD', 'SDN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(200, 'Suriname', 'SR', 'SUR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(202, 'Swaziland', 'SZ', 'SWZ', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(203, 'Sweden', 'SE', 'SWE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(204, 'Switzerland', 'CH', 'CHE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(205, 'Syrian Arab Republic', 'SY', 'SYR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(206, 'Taiwan', 'TW', 'TWN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(207, 'Tajikistan', 'TJ', 'TJK', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(209, 'Thailand', 'TH', 'THA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(210, 'Togo', 'TG', 'TGO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(211, 'Tokelau', 'TK', 'TKL', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(212, 'Tonga', 'TO', 'TON', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(213, 'Trinidad and Tobago', 'TT', 'TTO', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(214, 'Tunisia', 'TN', 'TUN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(215, 'Turkey', 'TR', 'TUR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(216, 'Turkmenistan', 'TM', 'TKM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(218, 'Tuvalu', 'TV', 'TUV', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(219, 'Uganda', 'UG', 'UGA', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(220, 'Ukraine', 'UA', 'UKR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(221, 'United Arab Emirates', 'AE', 'ARE', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(222, 'United Kingdom', 'GB', 'GBR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(223, 'United States', 'US', 'USA', 2, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(225, 'Uruguay', 'UY', 'URY', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(226, 'Uzbekistan', 'UZ', 'UZB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(227, 'Vanuatu', 'VU', 'VUT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(229, 'Venezuela', 'VE', 'VEN', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(230, 'Viet Nam', 'VN', 'VNM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(231, 'Virgin Islands (British)', 'VG', 'VGB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(234, 'Western Sahara', 'EH', 'ESH', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(235, 'Yemen', 'YE', 'YEM', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(236, 'Yugoslavia', 'YU', 'YUG', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(237, 'Zaire', 'ZR', 'ZAR', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(238, 'Zambia', 'ZM', 'ZMB', 1, 1);
INSERT INTO `countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`, `status`) VALUES(239, 'Zimbabwe', 'ZW', 'ZWE', 1, 1);

CREATE TABLE IF NOT EXISTS `currencies` (
  `currencies_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `symbol_left` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol_right` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thousands_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_places` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` float(13,8) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`currencies_id`),
  KEY `idx_currencies_code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES(1, 'Euro', 'EUR', '', 'EUR', '.', ',', '2', 1.00000000, '2008-09-13 18:02:35');
INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES(2, 'Dollard', 'USD', 'USD', '', '.', ',', '2', 1.40750003, '2008-09-13 18:02:36');
INSERT INTO `currencies` (`currencies_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES(3, 'Canada', 'CAD', '', 'CAD', '.', '.', '2', 1.50580001, '2008-09-13 18:02:36');

CREATE TABLE IF NOT EXISTS `customers` (
  `customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_siret` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_ape` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_tva_intracom` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_tva_intracom_code_iso` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `customers_gender` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `customers_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_dob` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `customers_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_default_address_id` int(11) DEFAULT NULL,
  `customers_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `customers_newsletter` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languages_id` int(11) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `member_level` int(5) NOT NULL DEFAULT '0',
  `customers_options_order_taxe` tinyint(1) NOT NULL DEFAULT '0',
  `customers_modify_company` tinyint(1) NOT NULL DEFAULT '1',
  `customers_modify_address_default` tinyint(1) NOT NULL DEFAULT '1',
  `customers_add_address` tinyint(1) NOT NULL DEFAULT '1',
  `customers_cellular_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_email_validation` int(1) NOT NULL DEFAULT '0',
  `customer_discount` decimal(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`customers_id`),
  KEY `idx_customers_email_address` (`customers_email_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customers_basket` (
  `customers_basket_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `products_id` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `customers_basket_quantity` int(2) NOT NULL DEFAULT '0',
  `final_price` decimal(15,4) DEFAULT NULL,
  `customers_basket_date_added` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`customers_basket_id`),
  KEY `idx_customers_basket_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customers_basket_attributes` (
  `customers_basket_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `products_id` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `products_options_value_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_basket_attributes_id`),
  KEY `idx_customers_basket_att_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `customers_groups` (
  `customers_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_group_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `customers_group_discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `color_bar` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `group_order_taxe` tinyint(1) NOT NULL DEFAULT '0',
  `group_payment_unallowed` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cc',
  `group_shipping_unallowed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_tax` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `customers_group_quantity_default` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customers_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

INSERT INTO `customers_groups` (`customers_group_id`, `customers_group_name`, `customers_group_discount`, `color_bar`, `group_order_taxe`, `group_payment_unallowed`, `group_shipping_unallowed`, `group_tax`, `customers_group_quantity_default`) VALUES(1, 'Tarifs 1', 5.00, '#FF0000', 0, 'moneyorder', 'table', 'true', 0);

CREATE TABLE IF NOT EXISTS `customers_info` (
  `customers_info_id` int(11) NOT NULL DEFAULT '0',
  `customers_info_date_of_last_logon` datetime DEFAULT NULL,
  `customers_info_number_of_logons` int(5) DEFAULT NULL,
  `customers_info_date_account_created` datetime DEFAULT NULL,
  `customers_info_date_account_last_modified` datetime DEFAULT NULL,
  `global_product_notifications` int(1) DEFAULT '0',
  `password_reset_key` char(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_date` datetime DEFAULT NULL,
  PRIMARY KEY (`customers_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `customers_notes` (
  `customers_notes_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `customers_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_notes_date` datetime DEFAULT '1000-01-01 00:00:00',
  `user_administrator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`customers_notes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `desjardins_reference` (
  `ref_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `order_id` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `desjardins_response` (
  `resp_id` int(11) NOT NULL AUTO_INCREMENT,
  `MAC` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ref_number` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TPE` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `montant` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `texte_libre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_retour` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `retourPLUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`resp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `discount_coupons` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coupons_description` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `coupons_discount_amount` decimal(15,12) NOT NULL DEFAULT '0.000000000000',
  `coupons_discount_type` enum('fixed','percent','shipping') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'percent',
  `coupons_date_start` datetime DEFAULT NULL,
  `coupons_date_end` datetime DEFAULT NULL,
  `coupons_max_use` int(3) NOT NULL DEFAULT '0',
  `coupons_min_order` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `coupons_min_order_type` enum('price','quantity') COLLATE utf8_unicode_ci DEFAULT 'price',
  `coupons_number_available` int(3) NOT NULL DEFAULT '0',
  `coupons_create_account_b2c` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_create_account_b2b` tinyint(1) NOT NULL DEFAULT '0',
  `coupons_twitter` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_categories` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_customers` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_manufacturers` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `manufacturers_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`manufacturers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_orders` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `orders_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_products` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `discount_coupons_to_zones` (
  `coupons_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupons_id`,`geo_zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `geo_zones` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `geo_zone_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(1, 'France', 'Etat franÃ§ais avec tous les dÃ©partements', '2008-09-15 10:04:04', '2006-04-11 18:48:30');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(2, 'Etats membres de l''UE', 'Etats membres de l''union europÃ©enne', '2008-09-15 10:03:58', '2006-05-04 10:27:42');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(3, 'Etats Zones EuropÃ©ennes', 'Tous les Ã©tats ainsi que les dÃ©partements', '2015-02-09 16:33:22', '2006-05-04 17:10:19');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(7, 'Zone FÃ©dÃ©rale 5% - Canada GST', 'Zone FÃ©dÃ©rale 5% - GST', '2015-02-09 16:08:55', '2008-09-15 21:48:44');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(8, 'Zone fÃ©dÃ©rale 12% HST BC', 'Zone fÃ©dÃ©rale 12% HST BC', '2015-02-09 16:04:08', '2008-09-15 21:49:02');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(9, 'Zone Provinciale', 'Canada (quÃ©bec)', NULL, '2008-09-15 21:51:15');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(15, 'Taxe hamonisÃ©e QuÃ©bec', 'Taxe hamonisÃ©e QuÃ©bec', NULL, '2015-02-09 16:59:28');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(11, 'Zone fÃ©dÃ©rale 13% - NB / NF /', 'Zone fÃ©dÃ©rale 13%', NULL, '2015-02-09 16:05:34');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(12, 'Zone fÃ©dÃ©rale 14%', 'Zone fÃ©dÃ©rale 14%', NULL, '2015-02-09 16:07:02');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(13, 'Zone fÃ©dÃ©rale 15% - NS HST', 'Zone fÃ©dÃ©rale 15% - HST', NULL, '2015-02-09 16:07:53');
INSERT INTO `geo_zones` (`geo_zone_id`, `geo_zone_name`, `geo_zone_description`, `last_modified`, `date_added`) VALUES(14, 'Zone fÃ©dÃ©rale 7%', 'Zone fÃ©dÃ©rale 7%', NULL, '2015-02-09 16:11:43');

CREATE TABLE IF NOT EXISTS `groups_to_categories` (
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  `discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`customers_group_id`,`categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `information_email_customers` (
  `information_email_customers_id` int(11) NOT NULL,
  `information_email_customers_delay_90` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_60` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_30` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_15` tinyint(1) NOT NULL DEFAULT '0',
  `information_email_customers_delay_7` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`information_email_customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `information_email_customers` (`information_email_customers_id`, `information_email_customers_delay_90`, `information_email_customers_delay_60`, `information_email_customers_delay_30`, `information_email_customers_delay_15`, `information_email_customers_delay_7`) VALUES(1, 0, 0, 0, 0, 0);

CREATE TABLE IF NOT EXISTS `languages` (
  `languages_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `directory` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`languages_id`),
  KEY `IDX_LANGUAGES_NAME` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

INSERT INTO `languages` (`languages_id`, `name`, `code`, `image`, `directory`, `sort_order`, `status`) VALUES(1, 'Francais', 'fr', 'french.gif', 'french', 1, 1);
INSERT INTO `languages` (`languages_id`, `name`, `code`, `image`, `directory`, `sort_order`, `status`) VALUES(2, 'Anglais', 'en', 'english.gif', 'english', 2, 1);

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `manufacturers_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturers_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturers_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `manufacturers_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manufacturers_id`),
  KEY `IDX_MANUFACTURERS_NAME` (`manufacturers_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `manufacturers_info` (
  `manufacturers_id` int(11) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `manufacturers_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_clicked` int(5) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL,
  `manufacturer_description` text COLLATE utf8_unicode_ci,
  `manufacturer_seo_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_seo_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer_seo_keyword` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`manufacturers_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `newsletters` (
  `newsletters_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `date_sent` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `locked` int(1) DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `newsletters_accept_file` int(1) NOT NULL DEFAULT '0',
  `newsletters_twitter` tinyint(1) NOT NULL DEFAULT '0',
  `newsletters_customer_no_account` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsletters_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `newsletter_customers_temp` (
  `customers_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `newsletter_no_account` (
  `customers_firstname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_lastname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_email_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `customers_newsletter` tinyint(1) NOT NULL DEFAULT '1',
  `customers_date_added` datetime DEFAULT NULL,
  `languages_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`customers_email_address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_siret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_ape` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_tva_intracom` varchar(14) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customers_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_email_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_address_format_id` int(5) NOT NULL DEFAULT '0',
  `delivery_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address_format_id` int(5) NOT NULL DEFAULT '0',
  `billing_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_cf` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_piva` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_format_id` int(5) NOT NULL DEFAULT '0',
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_owner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_expires` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status` int(5) NOT NULL DEFAULT '0',
  `orders_status_invoice` int(5) NOT NULL DEFAULT '1',
  `orders_date_finished` datetime DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `client_computer_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_name_client` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customers_cellular_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orders_archive` int(1) NOT NULL DEFAULT '0',
  `odoo_invoice` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_id`),
  KEY `idx_orders_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_pages_manager` (
  `orders_page_manager_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `page_manager_general_condition` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_page_manager_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_products` (
  `orders_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `final_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_tax` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `products_quantity` int(2) NOT NULL DEFAULT '0',
  `products_full_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_products_id`),
  KEY `idx_orders_products_orders_id` (`orders_id`),
  KEY `idx_orders_products_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_products_attributes` (
  `orders_products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_id` int(11) NOT NULL DEFAULT '0',
  `products_options` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_values` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `options_values_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `products_attributes_reference` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_products_attributes_id`),
  KEY `idx_orders_products_att_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_products_download` (
  `orders_products_download_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_id` int(11) NOT NULL DEFAULT '0',
  `orders_products_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `download_maxdays` int(2) NOT NULL DEFAULT '0',
  `download_count` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_products_download_id`),
  KEY `idx_orders_products_download_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_status` (
  `orders_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `public_flag` int(11) DEFAULT '1',
  `downloads_flag` int(11) DEFAULT '0',
  `support_orders_flag` int(1) DEFAULT '0',
  PRIMARY KEY (`orders_status_id`,`language_id`),
  KEY `idx_orders_status_name` (`orders_status_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(1, 1, 'En instance', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(1, 2, 'Pending', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(2, 1, 'Traitement en cours', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(2, 2, 'processing', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(3, 1, 'Livr&eacute;', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(3, 2, 'Delivered', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(4, 1, 'Annul&eacute;', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(4, 2, 'Cancelled', 1, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(5, 1, 'Support Client', 0, 0, 0);
INSERT INTO `orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`, `support_orders_flag`) VALUES(5, 2, 'Customers Support', 0, 0, 0);

CREATE TABLE IF NOT EXISTS `orders_status_history` (
  `orders_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) DEFAULT '0',
  `orders_status_id` int(5) DEFAULT '0',
  `orders_status_invoice_id` int(5) NOT NULL DEFAULT '1',
  `admin_user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `customer_notified` int(1) DEFAULT '0',
  `comments` text COLLATE utf8_unicode_ci,
  `orders_status_tracking_id` int(5) NOT NULL DEFAULT '0',
  `orders_tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`orders_status_history_id`),
  KEY `idx_orders_status_history_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `orders_status_invoice` (
  `orders_status_invoice_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_invoice_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_status_invoice_id`,`language_id`),
  KEY `idx_orders_status_invoice_name` (`orders_status_invoice_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(3, 1, 'Annuler');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(4, 1, 'Avoir');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(3, 2, 'Cancelled');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(1, 1, 'Commande');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(2, 1, 'Facture');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(4, 2, 'Have a bill');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(2, 2, 'Invoice');
INSERT INTO `orders_status_invoice` (`orders_status_invoice_id`, `language_id`, `orders_status_invoice_name`) VALUES(1, 2, 'Order');

CREATE TABLE IF NOT EXISTS `orders_status_tracking` (
  `orders_status_tracking_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_tracking_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `orders_status_tracking_link` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`orders_status_tracking_id`,`language_id`),
  KEY `idx_orders_status_tracking_name` (`orders_status_tracking_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(1, 1, '-- Aucune --', '');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(1, 2, '-- Nothing --', '');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(2, 1, 'Colissimo', 'http://www.coliposte.fr/particulier/suivi_particulier.jsp?colispart=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(2, 2, 'Colissimo', 'http://www.coliposte.fr/particulier/suivi_particulier.jsp?colispart=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(3, 1, 'Chronospost', 'http://www.fr.chronopost.com/web/fr/tracking/suivi_inter.jsp?listeNumeros=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(3, 2, 'Chronospost', 'http://www.fr.chronopost.com/web/fr/tracking/suivi_inter.jsp?listeNumeros=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(4, 1, 'FEDEX', 'http://fedex.com/Tracking?ascend_header=1&clienttype=dotcomreg&cntry_code=fr&language=french&tracknumbers=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(4, 2, 'FEDEX', 'http://fedex.com/Tracking?ascend_header=1&clienttype=dotcomreg&cntry_code=fr&language=french&tracknumbers=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(5, 1, 'UPS', 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(5, 2, 'UPS', 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(6, 1, 'Post Canada', 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(6, 2, 'Post Canada', 'http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(7, 1, 'Coliposte Pro', 'https://www.coliposte.net/pro/services/main.jsp?m=12003010&colispro=');
INSERT INTO `orders_status_tracking` (`orders_status_tracking_id`, `language_id`, `orders_status_tracking_name`, `orders_status_tracking_link`) VALUES(7, 2, 'Coliposte Pro', 'https://www.coliposte.net/pro/services/main.jsp?m=12003010&colispro=');

CREATE TABLE IF NOT EXISTS `orders_total` (
  `orders_total_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `class` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`orders_total_id`),
  KEY `idx_orders_total_orders_id` (`orders_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `pages_manager` (
  `pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `links_target` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `page_type` int(1) NOT NULL DEFAULT '0',
  `page_box` int(1) NOT NULL DEFAULT '0',
  `page_time` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `page_date_start` datetime DEFAULT NULL,
  `page_date_closed` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `last_modified` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `page_general_condition` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(1, '', 0, 1, 1, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2008-09-08 15:39:50', '2008-09-03 20:38:09', 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(2, '', 1, 1, 2, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2008-09-02 08:39:21', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(3, '', 2, 1, 3, 0, '', NULL, NULL, '1000-01-01 00:00:00', NULL, NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(4, '_self', 3, 1, 4, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2014-02-07 20:14:00', NULL, 0, 1);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(5, '', 4, 1, 4, 0, '', NULL, NULL, '1000-01-01 00:00:00', '2008-09-16 14:55:26', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(7, '', 5, 1, 4, 0, '', NULL, NULL, '2008-09-02 10:36:53', '2008-09-02 10:37:16', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(8, '', 6, 1, 4, 0, '', NULL, NULL, '2008-09-16 14:48:16', '2008-09-16 14:51:26', NULL, 0, 0);
INSERT INTO `pages_manager` (`pages_id`, `links_target`, `sort_order`, `status`, `page_type`, `page_box`, `page_time`, `page_date_start`, `page_date_closed`, `date_added`, `last_modified`, `date_status_change`, `customers_group_id`, `page_general_condition`) VALUES(9, '_self', 7, 1, 4, 0, '', NULL, NULL, '2009-08-30 15:51:56', '2009-08-30 15:52:32', NULL, 0, 0);

CREATE TABLE IF NOT EXISTS `pages_manager_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pages_id` int(11) DEFAULT NULL,
  `pages_title` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pages_html_text` longtext COLLATE utf8_unicode_ci,
  `externallink` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `page_manager_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_manager_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_manager_head_keywords_tag` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(1, 1, 'page intro', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(2, 1, 'Intro Page', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(3, 2, 'Page Accueil', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(4, 2, 'MainPage', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(5, 3, 'Nous Contacter', '<p><strong>(indiquer le nom de votre site ou votre soci&eacute;t&eacute;) </strong></p>', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(6, 3, 'Contact Us', '<p><strong>(Specify the name of your website or company)</strong></p>', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(7, 4, 'Conditions GÃ©nÃ©rales', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(8, 4, 'General Conditions', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(9, 5, 'Politiques de ConfidentialitÃ©', '', '', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(10, 5, 'Confidential politics', '', '', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(13, 7, 'RSS', '', 'rss.php', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(14, 7, 'RSS', '', 'rss.php', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(17, 8, 'Cartographie du site', '', 'sitemap.php', 1, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(18, 8, 'Sitemap', '', 'sitemap.php', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(19, 9, 'newsletter subscription', '', 'newsletter_no_account.php', 2, NULL, NULL, NULL);
INSERT INTO `pages_manager_description` (`id`, `pages_id`, `pages_title`, `pages_html_text`, `externallink`, `language_id`, `page_manager_head_title_tag`, `page_manager_head_desc_tag`, `page_manager_head_keywords_tag`) VALUES(20, 9, 'Inscription newsletter', '', 'newsletter_no_account.php', 1, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_quantity` int(11) NOT NULL DEFAULT '0',
  `products_model` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_ean` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_sku` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_image_zoom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `products_last_modified` datetime DEFAULT NULL,
  `products_date_available` datetime DEFAULT NULL,
  `products_weight` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `products_price_kilo` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `products_status` tinyint(1) NOT NULL DEFAULT '0',
  `products_tax_class_id` int(11) NOT NULL DEFAULT '0',
  `manufacturers_id` int(11) DEFAULT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  `products_percentage` tinyint(1) NOT NULL DEFAULT '1',
  `products_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `orders_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `suppliers_id` int(11) DEFAULT NULL,
  `products_archive` tinyint(1) NOT NULL DEFAULT '0',
  `products_min_qty_order` int(4) NOT NULL DEFAULT '0',
  `products_price_comparison` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `products_dimension_width` decimal(5,2) NOT NULL DEFAULT '0.00',
  `products_dimension_height` decimal(5,2) NOT NULL DEFAULT '0.00',
  `products_dimension_depth` decimal(5,2) NOT NULL DEFAULT '0.00',
  `products_dimension_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_volume` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `products_quantity_unit_id` int(11) NOT NULL DEFAULT '0',
  `products_only_online` int(1) NOT NULL DEFAULT '0',
  `products_image_medium` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_weight_pounds` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `products_cost` decimal(15,2) NOT NULL DEFAULT '0.00',
  `products_handling` decimal(15,2) NOT NULL DEFAULT '0.00',
  `products_wharehouse_time_replenishment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_wharehouse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_wharehouse_row` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_wharehouse_level_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_packaging` int(1) NOT NULL DEFAULT '0',
  `Products_sort_order` int(11) NOT NULL DEFAULT '0',
  `products_quantity_alert` int(4) NOT NULL DEFAULT '0',
  `products_only_shop` int(1) NOT NULL DEFAULT '0',
  `products_download_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `products_download_public` int(1) NOT NULL DEFAULT '0',
  `products_type` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_id`),
  KEY `idx_products_date_added` (`products_date_added`),
  KEY `idx_products_model` (`products_model`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_attributes` (
  `products_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `options_id` int(11) NOT NULL DEFAULT '0',
  `options_values_id` int(11) NOT NULL DEFAULT '0',
  `options_values_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_prefix` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_sort_order` int(3) NOT NULL DEFAULT '1',
  `products_attributes_reference` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_attributes_id`),
  KEY `idx_products_attributes_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_attributes_download` (
  `products_attributes_id` int(11) NOT NULL DEFAULT '0',
  `products_attributes_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `products_attributes_maxdays` int(2) DEFAULT '0',
  `products_attributes_maxcount` int(2) DEFAULT '0',
  PRIMARY KEY (`products_attributes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_description` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `products_description` text COLLATE utf8_unicode_ci,
  `products_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_viewed` int(5) DEFAULT '0',
  `products_head_title_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_head_desc_tag` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_head_keywords_tag` text COLLATE utf8_unicode_ci,
  `products_head_tag` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_shipping_delay` text COLLATE utf8_unicode_ci NOT NULL,
  `products_description_summary` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_id`,`language_id`),
  KEY `products_name` (`products_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_extra_fields` (
  `products_extra_fields_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_extra_fields_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `products_extra_fields_order` int(3) NOT NULL DEFAULT '0',
  `products_extra_fields_status` tinyint(1) NOT NULL DEFAULT '1',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_id` tinyint(11) NOT NULL,
  `products_extra_fields_type` int(1) NOT NULL,
  PRIMARY KEY (`products_extra_fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_featured` (
  `products_featured_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_featured_date_added` datetime DEFAULT NULL,
  `products_featured_last_modified` datetime DEFAULT NULL,
  `scheduled_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_featured_id`),
  KEY `idx_featured_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_groups` (
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `customers_group_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `price_group_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `products_group_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `orders_group_view` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `products_quantity_unit_id_group` int(5) NOT NULL DEFAULT '0',
  `products_model_group` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_quantity_fixed_group` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_heart` (
  `products_heart_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_heart_date_added` datetime DEFAULT NULL,
  `products_heart_last_modified` datetime DEFAULT NULL,
  `scheduled_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_heart_id`),
  KEY `idx_specials_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_images_prodid` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_notifications` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`products_id`,`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_options` (
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `products_options_sort_order` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`products_options_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_options_values` (
  `products_options_values_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `products_options_values_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_options_values_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_options_values_to_products_options` (
  `products_options_values_to_products_options_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_options_id` int(11) NOT NULL DEFAULT '0',
  `products_options_values_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_options_values_to_products_options_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_quantity_unit` (
  `products_quantity_unit_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `products_quantity_unit_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`products_quantity_unit_id`,`language_id`),
  KEY `idx_products_quantity_unit_title` (`products_quantity_unit_title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(4, 1, 'douzaine');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(4, 2, 'dozen');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(2, 1, 'kg');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(2, 2, 'kg');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(3, 2, 'liter');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(3, 1, 'litre');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(5, 2, 'pcs');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(5, 1, 'pi&egrave;ce');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(1, 2, 'unit');
INSERT INTO `products_quantity_unit` (`products_quantity_unit_id`, `language_id`, `products_quantity_unit_title`) VALUES(1, 1, 'unit&eacute;');

CREATE TABLE IF NOT EXISTS `products_related` (
  `products_related_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_related_id_master` int(11) NOT NULL DEFAULT '0',
  `products_related_id_slave` int(11) NOT NULL DEFAULT '0',
  `products_related_sort_order` smallint(6) NOT NULL DEFAULT '0',
  `products_cross_sell` tinyint(1) NOT NULL DEFAULT '0',
  `products_related` tinyint(1) NOT NULL DEFAULT '0',
  `products_mode_b2b` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `products_to_categories` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `categories_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`products_id`,`categories_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `products_to_products_extra_fields` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `products_extra_fields_id` int(11) NOT NULL DEFAULT '0',
  `products_extra_fields_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`products_id`,`products_extra_fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `reviews` (
  `reviews_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reviews_rating` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reviews_read` int(5) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`reviews_id`),
  KEY `idx_reviews_products_id` (`products_id`),
  KEY `idx_reviews_customers_id` (`customers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `reviews_description` (
  `reviews_id` int(11) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `reviews_text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`reviews_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sec_directory_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(1, 'ClicShoppingAdmin/backups');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(2, 'ClicShoppingAdmin/images');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(3, 'ClicShoppingAdmin/images/graphs');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(4, 'sources');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(5, 'sources/images');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(6, 'pub');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(7, 'includes/work');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(8, 'cache');
INSERT INTO `sec_directory_whitelist` (`id`, `directory`) VALUES(9, 'ClicCpanel/cache');

CREATE TABLE IF NOT EXISTS `sessions` (
  `sesskey` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `expiry` int(11) unsigned NOT NULL DEFAULT '0',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sesskey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `specials` (
  `specials_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL DEFAULT '0',
  `specials_new_products_price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `specials_date_added` datetime DEFAULT NULL,
  `specials_last_modified` datetime DEFAULT NULL,
  `scheduled_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `customers_group_id` int(11) NOT NULL DEFAULT '0',
  `flash_discount` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`specials_id`),
  KEY `idx_specials_products_id` (`products_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `submit_description` (
  `submit_id` int(11) NOT NULL DEFAULT '1',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `submit_defaut_language_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_defaut_language_keywords` text COLLATE utf8_unicode_ci,
  `submit_defaut_language_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_defaut_language_footer` text COLLATE utf8_unicode_ci,
  `submit_language_products_info_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_products_info_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_products_info_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_products_new_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_products_new_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_products_new_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_special_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_special_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_special_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_reviews_title` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submit_language_reviews_keywords` text COLLATE utf8_unicode_ci,
  `submit_language_reviews_description` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`submit_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `submit_description` (`submit_id`, `language_id`, `submit_defaut_language_title`, `submit_defaut_language_keywords`, `submit_defaut_language_description`, `submit_defaut_language_footer`, `submit_language_products_info_title`, `submit_language_products_info_keywords`, `submit_language_products_info_description`, `submit_language_products_new_title`, `submit_language_products_new_keywords`, `submit_language_products_new_description`, `submit_language_special_title`, `submit_language_special_keywords`, `submit_language_special_description`, `submit_language_reviews_title`, `submit_language_reviews_keywords`, `submit_language_reviews_description`) VALUES(1, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `submit_description` (`submit_id`, `language_id`, `submit_defaut_language_title`, `submit_defaut_language_keywords`, `submit_defaut_language_description`, `submit_defaut_language_footer`, `submit_language_products_info_title`, `submit_language_products_info_keywords`, `submit_language_products_info_description`, `submit_language_products_new_title`, `submit_language_products_new_keywords`, `submit_language_products_new_description`, `submit_language_special_title`, `submit_language_special_keywords`, `submit_language_special_description`, `submit_language_reviews_title`, `submit_language_reviews_keywords`, `submit_language_reviews_description`) VALUES(1, 2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

CREATE TABLE IF NOT EXISTS `suppliers` (
  `suppliers_id` int(11) NOT NULL AUTO_INCREMENT,
  `suppliers_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `suppliers_manager` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_phone` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_email_address` varchar(96) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_fax` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_suburb` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_postcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_city` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_states` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suppliers_country_id` int(11) NOT NULL DEFAULT '0',
  `suppliers_notes` text COLLATE utf8_unicode_ci NOT NULL,
  `suppliers_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`suppliers_id`),
  KEY `IDX_SUPPLIERS_NAME` (`suppliers_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `suppliers_info` (
  `suppliers_id` int(11) NOT NULL DEFAULT '0',
  `languages_id` int(11) NOT NULL DEFAULT '0',
  `suppliers_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_clicked` int(5) NOT NULL DEFAULT '0',
  `date_last_click` datetime DEFAULT NULL,
  PRIMARY KEY (`suppliers_id`,`languages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `support_admin` (
  `support_admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `support_subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_support` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `message_support` text COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`support_admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tax_class_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(1, 'TVA 20', 'TVA France 20', '2015-02-09 16:13:40', '2006-04-09 16:13:48');
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(2, 'TVA 5.5', 'TVA France 5.5', '2008-09-03 13:33:35', '2006-04-16 00:30:06');
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(3, 'Biens taxables Canada', 'Biens taxables Canada', NULL, '2008-09-16 15:02:29');
INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES(4, 'Taxe hamonisÃ©e QuÃ©bec', 'Taxe hamonisÃ©e QuÃ©bec', NULL, '2015-01-25 01:23:07');

CREATE TABLE IF NOT EXISTS `tax_rates` (
  `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_zone_id` int(11) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL DEFAULT '0',
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,4) NOT NULL DEFAULT '0.0000',
  `tax_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `code_tax_odoo` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tax_rates_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(1, 1, 1, 1, 20.0000, 'TVA 20%', '2015-02-09 16:14:24', '2006-04-09 16:13:48', '20.0');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(2, 1, 2, 0, 5.5000, 'TVA 5.5%', '2015-02-09 17:01:13', '2006-04-16 00:30:21', '5.5');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(3, 9, 3, 1, 9.9750, 'Zone TVH 9.975', '2015-02-09 16:16:14', '2008-09-16 15:04:49', 'TVQ');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(5, 7, 3, 1, 5.0000, 'Zone TPS 5%', '2015-02-09 16:16:55', '2008-09-16 15:05:48', 'TPS_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(6, 15, 4, 1, 14.9750, 'Taxe hamonisÃ©e QuÃ©bec', '2015-02-09 17:00:14', '2015-01-25 01:25:11', 'TPSTVQ_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(7, 11, 3, 1, 13.0000, 'Zone TVH 13%', '2015-02-09 16:18:27', '2015-02-09 16:15:01', 'TVH13_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(8, 12, 3, 1, 14.0000, 'Zone fÃ©dÃ©rale 14%', '2015-02-09 16:18:38', '2015-02-09 16:17:18', 'TVH14_SALE');
INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`, `code_tax_odoo`) VALUES(9, 8, 3, 1, 12.0000, 'Zone FÃ©dÃ©rale 12%', '2015-02-09 16:18:46', '2015-02-09 16:17:47', 'TVH12_SALE');

CREATE TABLE IF NOT EXISTS `template_email` (
  `template_email_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_email_variable` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `customers_group_id` int(2) NOT NULL DEFAULT '0',
  `template_email_type` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_email_id`),
  KEY `customer_group_id` (`customers_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(1, 'TEMPLATE_EMAIL_WELCOME', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(2, 'TEMPLATE_EMAIL_TEXT_FOOTER', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(3, 'TEMPLATE_EMAIL_WELCOME_ADMIN', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(4, 'TEMPLATE_EMAIL_TEXT_COUPON', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(5, 'TEMPLATE_EMAIL_SIGNATURE', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(6, 'TEMPLATE_EMAIL_INTRO_COMMAND', 0, 0);
INSERT INTO `template_email` (`template_email_id`, `template_email_variable`, `customers_group_id`, `template_email_type`) VALUES(7, 'TEMPLATE_EMAIL_NEWSLETTER_TEXT_FOOTER', 0, 0);

CREATE TABLE IF NOT EXISTS `template_email_description` (
  `template_email_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_email_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `template_email_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_email_short_description` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `template_email_description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`template_email_description_id`),
  KEY `template_id` (`language_id`),
  KEY `template_email_id` (`template_email_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(1, 1, 1, 'Message de bienvenue catalogue', 'Catalogue  -Message de bienvenue lors de l''inscription du client', '<div align="justify">\r\n &nbsp;Nous vous remercions pour la confiance que vous nous t&eacute;moigniez en vous enregistrant comme nouveau client sur le site %%HTTP_CATALOG%%.</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;Comme suite &agrave; votre demande d&#39;ouverture de compte sur %%STORE_NAME%% nous vous confirmons sa validation. Vous pouvez d&egrave;s a pr&eacute;sent vous connecter sur %%STORE_NAME%%.</div>\r\n<br />\r\n<div align="justify">\r\n Nous sommes ravis de vous compter parmi nos nouveaux clients et nous restons &agrave; votre service pour vous faciliter vos achats !</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;&#39;<strong>Vos liens utiles :</strong>&#39;</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;1) Pour acc&eacute;der &agrave; votre compte client, modifier vos coordonn&eacute;es, vos adresses de livraison et/ou facturation, %%HTTP_CATALOG%%boutique/account.php</a></div>\r\n<br />\r\n<br />\r\n<div align="justify">\r\n 2) Pour acc&eacute;der directement au suivi de vos commandes, %%HTTP_CATALOG%%boutique/account_history.php</a></div>\r\n<div align="justify">\r\n 3) Retrouver son mot de passe oubli&eacute; : %%HTTP_CATALOG%%boutique/password_forgotten</a><br />\r\n &nbsp;</div>\r\n<div align="justify">\r\n Pour tous les acc&egrave;s pr&eacute;cit&eacute;s, vous devrez renseigner au pr&eacute;alable vos identifiants.</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n Pour toute aide sur nos services, n&#39;h&eacute;sitez pas &agrave; contacter notre service client&egrave;le :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(2, 1, 2, 'Welcome message on catalogue', 'Catalog - welcome message for a new customer', '<div align="justify">\r\n &nbsp;Thank you for the trust you place in us by registering as a new customer on site %%HTTP_CATALOG%%.</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;Following your request to open an account on %%STORE_NAME%% we confirm its validity. You can now log on %%STORE_NAME%%.</div>\r\n<br />\r\n<div align="justify">\r\n We are delighted to count you among our new clients and we remain at your service to make shopping easy! !</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;&#39;<strong>Your links :</strong>&#39;</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;1) To access your account, change your address, your delivery address and / or billing,%%HTTP_CATALOG%%boutique/account.php</div>\r\n<br />\r\n<br />\r\n<div align="justify">\r\n 2) To go directly to track your orders, %%HTTP_CATALOG%%boutique/account_history.php</div>\r\n<div align="justify">\r\n 3) Recover lost password : %%HTTP_CATALOG%%/boutique/password_forgotten</a><br />\r\n &nbsp;</div>\r\n<div align="justify">\r\n To access all the above, you should check before your login..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n For assistance on our services, please contact our customer service: %%STORE_OWNER_EMAIL_ADDRESS%% :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(3, 2, 1, 'Avis de confidentialitÃ©', 'LÃ©gislation : pied de page pour tous les emails envoyÃ©s sauf newsletter', '<p>\r\n &nbsp;<u>Avis de confidentialit&eacute; :</u><br />\r\n Ce message ainsi que les documents qui seraient joints en annexe sont adress&eacute;s exclusivement &agrave; leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la l&eacute;gislation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans &ecirc;tre le destinataire ou une personne autoris&eacute;e, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re&ccedil;u ce message, nous vous prions de bien vouloir nous le communiquer par courriel :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%% et de proc&eacute;der directement &agrave; sa destruction.</p>\r\n<p>\r\n Conform&eacute;ment &agrave; la Loi dans le pays de r&eacute;sidence de la soci&eacute;t&eacute; exploitant la boutique %%STORE_NAME%%, vous avez droit &agrave; la rectification de vos donn&eacute;es personnelles &agrave; tout moment ou sur simple demande par email. %%STORE_OWNER_EMAIL_ADDRESS%% </p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(4, 2, 2, 'Confidentialities', 'Regulation : footer for all email sending except newsletter', 'This e-mail and any files transmitted with it are confidential and intended solely for the use of the individual to whom it is addressed. If you have received this email in error please send it back to the person that sent it to you. Any views or opinions presented are solely those of its author and do not necessarily represent those of %%STORE_NAME%% or any of its subsidiary companies. Unauthorized publication, use, dissemination, forwarding, printing or copying of this email and its associated attachments is strictly prohibited.<br />\r\n<br />\r\n.Pursuant to the Act in the country of residence of the company operating the store %%STORE_NAME%%, you are entitled to the correction of your personal data at any time or upon request by email  at %%STORE_OWNER_EMAIL_ADDRESS%% .</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(5, 3, 1, 'CrÃ©ation du compte client', 'Admin : Message de bienvenue lors de la crÃ©ation du comte client', '<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n Nous venons de vous cr&eacute;er un compte suite &agrave; une commande de votre part (par t&eacute;l&eacute;phone ou autre) sur le site %%STORE_NAME%%.</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n En vous connectant sur %%STORE_NAME%% (apr&egrave;s avoir r&eacute;cup&eacute;r&eacute; votre mot de passe), nous pourrez b&eacute;n&eacute;ficier d&#39;un ensemble de services.</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n Nous sommes ravis de vous compter parmi nos nouveaux clients et nous restons &agrave; votre service pour vous faciliter vos achats !</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n <strong>Vos liens utiles :</strong></div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div>\r\n 1) Pour acc&eacute;der &agrave; votre compte client, modifier vos coordonn&eacute;es, vos adresses de livraison et/ou facturation : %%HTTP_CATALOG%%account.php<br />\r\n </a></div>\r\n<div>\r\n 2) Pour acc&eacute;der directement au suivi de vos commandes : %%HTTP_CATALOG%%account_history.php</div>\r\n<div>\r\n 3) Retrouver son mot de passe oubli&eacute; : %%HTTP_CATALOG%%password_forgotten.php<br />\r\n 4) Pour contacter notre service apr&egrave;s vente concernant une commande, veuillez &eacute;diter votre commande dans votre espace et cliquer sur support.</div>\r\n<br />\r\n<div align="justify">\r\n Pour tous les acc&egrave;s pr&eacute;cit&eacute;s, vous devrez renseigner au pr&eacute;alable vos identifiants.</div>\r\n<div align="justify">\r\n Pour toute aide sur nos services, n&#39;h&eacute;sitez pas &agrave; contacter notre service client&egrave;le :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(6, 3, 2, 'Customer create account', 'Admin : Welcome message - Customer create account', '<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n We just create an account following an order by you (by phone or otherwise) on site%%STORE_NAME%%..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n By logging on %%STORE_NAME%% (after retrieving your password), we can enjoy a range of services..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\nWe are delighted to count you among our new clients and we remain at your service to make shopping easy! !</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n <strong>Your links :</strong>&#39;</div>\r\n<br />\r\n<div align="justify">\r\n &nbsp;1) To access your account, change your address, your delivery address and / or billing,%%HTTP_CATALOG%%boutique/account.php</a></div>\r\n<br />\r\n<br />\r\n<div align="justify">\r\n 2) To go directly to track your orders, %%HTTP_CATALOG%%boutique/account_history.php</a></div>\r\n<div align="justify">\r\n 3) Recover lost password : %%HTTP_CATALOG%%boutique/password_forgotten</a><br />\r\n &nbsp;</div>\r\n<div align="justify">\r\n To access all the above, you should check before your login..</div>\r\n<div align="justify">\r\n &nbsp;</div>\r\n<div align="justify">\r\n For assistance on our services, please contact our customer service: %%STORE_OWNER_EMAIL_ADDRESS%% :&nbsp; %%STORE_OWNER_EMAIL_ADDRESS%%</div>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(7, 4, 1, 'Coupon Client', 'Offrir un coupon lors de la crÃ©ation d''un compte client', '<p>\r\n %%STORE_NAME%% se fait un plaisir de vous offrir un coupon remise sur votre prochaine commande que vous pourrez utiliser n&#39;importe quand sur la boutique. Pour connaitre les modalit&eacute;s d&#39;application du coupon, veuillez consulter notre aide en ligne.</p>\r\n<p>\r\n Le num&eacute;ro du coupon est :</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(8, 4, 2, 'Customer coupon', 'Give a coupon during the creation a customer account', '<p>\r\n %%STORE_NAME%% is pleased to offer you a discount on your next order coupon that you can use anytime Store. To know the rules for applying the coupon, please visit our online help. .</p>\r\n<p>\r\n The coupon number is: :</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(9, 5, 1, 'Signature', 'Qui sera associÃ© au bas d''un mail envoyÃ© : signature', '<p>\r\n ---------------------</p>\r\n<p>\r\n Cordialement,</p>\r\n<p>\r\n L&#39;&eacute;quipe %%STORE_NAME%%</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(10, 5, 2, 'Signature', 'Signature at the bottom of the message', '<p>\r\n ---------------------</p>\r\n<p>\r\n Regards,</p>\r\n<p>\r\n %%STORE_NAME%% team</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(11, 6, 1, 'Statut commande', 'Mail concernant le statut commande', 'Email envoy&eacute; suite &agrave; une MAJ d&#39;un statut<br />\r\n<p>\r\n &nbsp;Bonjour,<br />\r\n <br />\r\n &nbsp;Le statut de votre commande a &eacute;t&eacute; mis &agrave; jour.<br />\r\n <br />\r\n &nbsp;Pour toutes les demandes suivantes, veuillez vous connecter &agrave; votre espace d&#39;administration.<br />\r\n <br />\r\n &nbsp;- Consulter vos commandes ou votre historique de commande : %%HTTP_CATALOG%%account_history.php<br />\r\n &nbsp;- Nous envoyer un message concernant cette commande (&eacute;diter votre commande et cliquer contacter notre support)<br />\r\n &nbsp;- Imprimer, t&eacute;l&eacute;charger une commande, une facture (&eacute;diter votre commande et cliquer sur l&#39;icone PDF facture)<br />\r\n &nbsp;</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(12, 6, 2, 'Order status', 'Order status email', 'Email sent after an update status<br />\r\n<p>\r\n &nbsp;Hello,<br />\r\n <br />\r\n &nbsp;The status of your order has been updated..<br />\r\n <br />\r\n &nbsp;For all subsequent requests, please log into your administration area..<br />\r\n <br />\r\n &nbsp;- View your orders and your order history : %%HTTP_CATALOG%%account_history.php<br />\r\n &nbsp;- We send a message regarding this order (click edit your order and contact our support)<br />\r\n &nbsp;- Print, download an order, an invoice (edit your order and click on the PDF icon invoice)<br />\r\n &nbsp;</p>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(13, 7, 1, 'LÃ©gislation newsletter', 'text bas de page lÃ©gislation newsletter', '<style="font-weight="6">Conform&eacute;ment aux <strong>Lois</strong> en vigueur dans le pays de la boutique %%STORE_NAME%%, vous avez droit &agrave; la rectification de vos donn&eacute;es personnelles &agrave; tout moment ou sur simple demande par email.<br />\r\nPour se d&eacute;sabonner de notre Newsletter, veuillez recopier&nbsp; sur le lien suivant : %%HTTP_CATALOG%%account_newsletters.php</font>');
INSERT INTO `template_email_description` (`template_email_description_id`, `template_email_id`, `language_id`, `template_email_name`, `template_email_short_description`, `template_email_description`) VALUES(14, 7, 2, 'Newsletter regulation', 'Regulation footer text', '<style="font-weight="6">In accordance with the <strong>Law of the </strong>%%STORE_NAME%%, you are entitled to correction of your personal data or on request by email.<br />\r\nTo unsubscribe from our newsletter, send us an email or just click on the following link: %%HTTP_CATALOG%%account_newsletters.php</font>');

CREATE TABLE IF NOT EXISTS `usu_cache` (
  `cache_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `cache_data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `cache_date` datetime NOT NULL,
  PRIMARY KEY (`cache_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `whos_online` (
  `customer_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_entry` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `time_last_click` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `last_page_url` text COLLATE utf8_unicode_ci NOT NULL,
  `http_referer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL DEFAULT '0',
  `zone_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `idx_zones_to_geo_zones_country_id` (`zone_country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=642 ;

INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(1, 223, 'AL', 'Alabama');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(2, 223, 'AK', 'Alaska');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(3, 223, 'AS', 'American Samoa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(4, 223, 'AZ', 'Arizona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(5, 223, 'AR', 'Arkansas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(6, 223, 'AF', 'Armed Forces Africa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(7, 223, 'AA', 'Armed Forces Americas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(8, 223, 'AC', 'Armed Forces Canada');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(9, 223, 'AE', 'Armed Forces Europe');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(10, 223, 'AM', 'Armed Forces Middle East');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(11, 223, 'AP', 'Armed Forces Pacific');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(12, 223, 'CA', 'California');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(13, 223, 'CO', 'Colorado');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(14, 223, 'CT', 'Connecticut');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(15, 223, 'DE', 'Delaware');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(16, 223, 'DC', 'District of Columbia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(17, 223, 'FM', 'Federated States Of Micronesia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(18, 223, 'FL', 'Florida');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(19, 223, 'GA', 'Georgia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(20, 223, 'GU', 'Guam');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(21, 223, 'HI', 'Hawaii');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(22, 223, 'ID', 'Idaho');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(23, 223, 'IL', 'Illinois');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(24, 223, 'IN', 'Indiana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(25, 223, 'IA', 'Iowa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(26, 223, 'KS', 'Kansas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(27, 223, 'KY', 'Kentucky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(28, 223, 'LA', 'Louisiana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(29, 223, 'ME', 'Maine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(30, 223, 'MH', 'Marshall Islands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(31, 223, 'MD', 'Maryland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(32, 223, 'MA', 'Massachusetts');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(33, 223, 'MI', 'Michigan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(34, 223, 'MN', 'Minnesota');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(35, 223, 'MS', 'Mississippi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(36, 223, 'MO', 'Missouri');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(37, 223, 'MT', 'Montana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(38, 223, 'NE', 'Nebraska');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(39, 223, 'NV', 'Nevada');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(40, 223, 'NH', 'New Hampshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(41, 223, 'NJ', 'New Jersey');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(42, 223, 'NM', 'New Mexico');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(43, 223, 'NY', 'New York');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(44, 223, 'NC', 'North Carolina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(45, 223, 'ND', 'North Dakota');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(46, 223, 'MP', 'Northern Mariana Islands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(47, 223, 'OH', 'Ohio');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(48, 223, 'OK', 'Oklahoma');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(49, 223, 'OR', 'Oregon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(50, 223, 'PW', 'Palau');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(51, 223, 'PA', 'Pennsylvania');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(52, 223, 'PR', 'Puerto Rico');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(53, 223, 'RI', 'Rhode Island');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(54, 223, 'SC', 'South Carolina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(55, 223, 'SD', 'South Dakota');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(56, 223, 'TN', 'Tennessee');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(57, 223, 'TX', 'Texas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(58, 223, 'UT', 'Utah');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(59, 223, 'VT', 'Vermont');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(60, 223, 'VI', 'Virgin Islands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(61, 223, 'VA', 'Virginia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(62, 223, 'WA', 'Washington');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(63, 223, 'WV', 'West Virginia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(64, 223, 'WI', 'Wisconsin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(65, 223, 'WY', 'Wyoming');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(66, 38, 'AB', 'Alberta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(67, 38, 'BC', 'British Columbia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(68, 38, 'MB', 'Manitoba');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(69, 38, 'NF', 'Newfoundland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(70, 38, 'NB', 'New Brunswick');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(71, 38, 'NS', 'Nova Scotia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(72, 38, 'NT', 'Northwest Territories');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(73, 38, 'NU', 'Nunavut');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(74, 38, 'ON', 'Ontario');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(75, 38, 'PE', 'Prince Edward Island');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(76, 38, 'QC', 'Quebec');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(77, 38, 'SK', 'Saskatchewan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(78, 38, 'YT', 'Yukon Territory');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(79, 81, 'NDS', 'Niedersachsen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(80, 81, 'BAW', 'Baden-W&uuml;rttemberg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(81, 81, 'BAY', 'Bayern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(82, 81, 'BER', 'Berlin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(83, 81, 'BRG', 'Brandenburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(84, 81, 'BRE', 'Bremen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(85, 81, 'HAM', 'Hamburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(86, 81, 'HES', 'Hessen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(87, 81, 'MEC', 'Mecklenburg-Vorpommern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(88, 81, 'NRW', 'Nordrhein-Westfalen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(89, 81, 'RHE', 'Rheinland-Pfalz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(90, 81, 'SAR', 'Saarland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(91, 81, 'SAS', 'Sachsen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(92, 81, 'SAC', 'Sachsen-Anhalt');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(93, 81, 'SCN', 'Schleswig-Holstein');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(94, 81, 'THE', 'Th&uuml;ringen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(95, 14, 'WI', 'Wien');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(96, 14, 'NO', 'Nieder&ouml;sterreich');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(97, 14, 'OO', 'Ober&ouml;sterreich');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(98, 14, 'SB', 'Salzburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(99, 14, 'KN', 'K&auml;rnten');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(100, 14, 'ST', 'Steiermark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(101, 14, 'TI', 'Tirol');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(102, 14, 'BL', 'Burgenland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(103, 14, 'VB', 'Voralberg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(104, 204, 'AG', 'Aargau');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(105, 204, 'AI', 'Appenzell Innerrhoden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(106, 204, 'AR', 'Appenzell Ausserrhoden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(107, 204, 'BE', 'Bern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(108, 204, 'BL', 'Basel-Landschaft');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(109, 204, 'BS', 'Basel-Stadt');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(110, 204, 'FR', 'Freiburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(111, 204, 'GE', 'Genf');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(112, 204, 'GL', 'Glarus');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(113, 204, 'JU', 'Graub&uuml;nden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(114, 204, 'JU', 'Jura');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(115, 204, 'LU', 'Luzern');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(116, 204, 'NE', 'Neuenburg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(117, 204, 'NW', 'Nidwalden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(118, 204, 'OW', 'Obwalden');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(119, 204, 'SG', 'St. Gallen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(120, 204, 'SH', 'Schaffhausen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(121, 204, 'SO', 'Solothurn');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(122, 204, 'SZ', 'Schwyz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(123, 204, 'TG', 'Thurgau');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(124, 204, 'TI', 'Tessin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(125, 204, 'UR', 'Uri');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(126, 204, 'VD', 'Waadt');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(127, 204, 'VS', 'Wallis');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(128, 204, 'ZG', 'Zug');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(129, 204, 'ZH', 'Z&uuml;rich');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(130, 195, 'A CoruÃ±a', 'A CoruÃ±a');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(131, 195, 'Alava', 'Alava');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(132, 195, 'Albacete', 'Albacete');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(133, 195, 'Alicante', 'Alicante');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(134, 195, 'Almeria', 'Almeria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(135, 195, 'Asturias', 'Asturias');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(136, 195, 'Avila', 'Avila');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(137, 195, 'Badajoz', 'Badajoz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(138, 195, 'Baleares', 'Baleares');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(139, 195, 'Barcelona', 'Barcelona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(140, 195, 'Burgos', 'Burgos');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(141, 195, 'Caceres', 'Caceres');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(142, 195, 'Cadiz', 'Cadiz');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(143, 195, 'Cantabria', 'Cantabria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(144, 195, 'Castellon', 'Castellon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(145, 195, 'Ceuta', 'Ceuta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(146, 195, 'Ciudad Real', 'Ciudad Real');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(147, 195, 'Cordoba', 'Cordoba');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(148, 195, 'Cuenca', 'Cuenca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(149, 195, 'Girona', 'Girona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(150, 195, 'Granada', 'Granada');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(151, 195, 'Guadalajara', 'Guadalajara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(152, 195, 'Guipuzcoa', 'Guipuzcoa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(153, 195, 'Huelva', 'Huelva');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(154, 195, 'Huesca', 'Huesca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(155, 195, 'Jaen', 'Jaen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(156, 195, 'La Rioja', 'La Rioja');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(157, 195, 'Las Palmas', 'Las Palmas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(158, 195, 'Leon', 'Leon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(159, 195, 'Lleida', 'Lleida');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(160, 195, 'Lugo', 'Lugo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(161, 195, 'Madrid', 'Madrid');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(162, 195, 'Malaga', 'Malaga');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(163, 195, 'Melilla', 'Melilla');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(164, 195, 'Murcia', 'Murcia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(165, 195, 'Navarra', 'Navarra');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(166, 195, 'Ourense', 'Ourense');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(167, 195, 'Palencia', 'Palencia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(168, 195, 'Pontevedra', 'Pontevedra');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(169, 195, 'Salamanca', 'Salamanca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(170, 195, 'Santa Cruz de Tenerife', 'Santa Cruz de Tenerife');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(171, 195, 'Segovia', 'Segovia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(172, 195, 'Sevilla', 'Sevilla');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(173, 195, 'Soria', 'Soria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(174, 195, 'Tarragona', 'Tarragona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(175, 195, 'Teruel', 'Teruel');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(176, 195, 'Toledo', 'Toledo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(177, 195, 'Valencia', 'Valencia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(178, 195, 'Valladolid', 'Valladolid');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(179, 195, 'Vizcaya', 'Vizcaya');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(180, 195, 'Zamora', 'Zamora');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(181, 73, 'Et', 'Etranger');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(182, 73, '01', 'Ain');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(183, 73, '02', 'Aisne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(184, 73, '03', 'Allier');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(185, 73, '04', 'Alpes de Haute Provence');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(186, 73, '05', 'Hautes-Alpes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(187, 73, '06', 'Alpes Maritimes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(188, 73, '07', 'Ard&egrave;che');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(189, 73, '08', 'Ardennes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(190, 73, '09', 'Ari&egrave;ge');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(191, 73, '10', 'Aube');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(192, 73, '11', 'Aude');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(193, 73, '12', 'Aveyron');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(194, 73, '13', 'Bouches du Rh&ocirc;ne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(195, 73, '14', 'Calvados');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(196, 73, '15', 'Cantal');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(197, 73, '16', 'Charente');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(198, 73, '17', 'Charente Maritime');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(199, 73, '18', 'Cher');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(200, 73, '19', 'Corr&egrave;ze');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(201, 73, '2A', 'Corse du Sud');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(202, 73, '2B', 'Haute Corse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(203, 73, '21', 'C&ocirc;te d''or');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(204, 73, '22', 'C&ocirc;tes d''Armor');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(205, 73, '23', 'Creuse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(206, 73, '24', 'Dordogne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(207, 73, '25', 'Doubs');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(208, 73, '26', 'Dr&ocirc;me');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(209, 73, '27', 'Eure');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(210, 73, '28', 'Eure et Loir');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(211, 73, '29', 'Finist&egrave;re');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(212, 73, '30', 'Gard');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(213, 73, '31', 'Haute Garonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(214, 73, '32', 'Gers');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(215, 73, '33', 'Gironde');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(216, 73, '34', 'H&eacute;rault');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(217, 73, '35', 'Ille et Vilaine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(218, 73, '36', 'Indre');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(219, 73, '37', 'Indre et Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(220, 73, '38', 'Is&egrave;re');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(221, 73, '39', 'Jura');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(222, 73, '40', 'Landes');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(223, 73, '41', 'Loir et Cher');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(224, 73, '42', 'Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(225, 73, '43', 'Haute Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(226, 73, '44', 'Loire Atlantique');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(227, 73, '45', 'Loiret');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(228, 73, '46', 'Lot');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(229, 73, '47', 'Lot et Garonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(230, 73, '48', 'Loz&egrave;re');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(231, 73, '49', 'Maine et Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(232, 73, '50', 'Manche');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(233, 73, '51', 'Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(234, 73, '52', 'Haute Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(235, 73, '53', 'Mayenne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(236, 73, '54', 'Meurthe et Moselle');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(237, 73, '55', 'Meuse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(238, 73, '56', 'Morbihan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(239, 73, '57', 'Moselle');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(240, 73, '58', 'Ni&egrave;vre');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(241, 73, '59', 'Nord');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(242, 73, '60', 'Oise');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(243, 73, '61', 'Orne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(244, 73, '62', 'Pas de Calais');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(245, 73, '63', 'Puy de D&ocirc;me');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(246, 73, '64', 'Pyr&eacute;n&eacute;es Atlantiques');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(247, 73, '65', 'Hautes Pyr&eacute;n&eacute;es');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(248, 73, '66', 'Pyr&eacute;n&eacute;es Orientales');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(249, 73, '67', 'Bas Rhin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(250, 73, '68', 'Haut Rhin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(251, 73, '69', 'Rhone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(252, 73, '70', 'Haute Sa&ocirc;ne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(253, 73, '71', 'Sa&ocirc;ne et Loire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(254, 73, '72', 'Sarthe');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(255, 73, '73', 'Savoie');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(256, 73, '74', 'Haute Savoie');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(257, 73, '75', 'Paris');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(258, 73, '76', 'Seine Maritime');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(259, 73, '77', 'Seine et Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(260, 73, '78', 'Yvelines');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(261, 73, '79', 'Deux S&egrave;vres');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(262, 73, '80', 'Somme');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(263, 73, '81', 'Tarn');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(264, 73, '82', 'Tarn et Garonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(265, 73, '83', 'Var');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(266, 73, '84', 'Vaucluse');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(267, 73, '85', 'Vend&eacute;e');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(268, 73, '86', 'Vienne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(269, 73, '87', 'Haute Vienne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(270, 73, '88', 'Vosges');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(271, 73, '89', 'Yonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(272, 73, '90', 'Territoire de Belfort');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(273, 73, '91', 'Essonne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(274, 73, '92', 'Hauts de Seine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(275, 73, '93', 'Seine St-Denis');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(276, 73, '94', 'Val de Marne');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(277, 73, '95', 'Val d''Oise');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(278, 73, '971 (DOM)', 'Guadeloupe');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(279, 73, '972 (DOM)', 'Martinique');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(280, 73, '973 (DOM)', 'Guyane');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(281, 73, '974 (DOM)', 'Saint Denis');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(282, 73, '975 (DOM)', 'St-Pierre de Miquelon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(283, 73, '976 (TOM)', 'Mayotte');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(284, 73, '984 (TOM)', 'Terres australes et Antartiques ');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(285, 73, '985 (TOM)', 'Nouvelle Cal&eacute;donie');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(286, 73, '986 (TOM)', 'Wallis et Futuna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(287, 73, '987 (TOM)', 'Polyn&eacute;sie fran&ccedil;aise');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(288, 21, 'Anvers', 'Anvers');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(289, 21, 'Limbourg', 'Limbourg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(290, 21, 'Hainaut', 'Hainaut');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(291, 21, 'Brabant Flamand', 'Brabant Flamand');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(292, 21, 'Brabant Wallon', 'Brabant Wallon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(293, 21, 'Liege', 'Liege');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(294, 21, 'Namur', 'Namur');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(295, 21, 'Luxembourg', 'Luxembourg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(296, 21, 'Flandre Occidental', 'Namen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(297, 21, 'Flandre Orientale', 'Flandre Orientale');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(298, 105, 'AG', 'Agrigento');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(299, 105, 'AL', 'Alessandria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(300, 105, 'AN', 'Ancona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(301, 105, 'AO', 'Aosta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(302, 105, 'AR', 'Arezzo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(303, 105, 'AP', 'Ascoli Piceno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(304, 105, 'AT', 'Asti');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(305, 105, 'AV', 'Avellino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(306, 105, 'BA', 'Bari');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(307, 105, 'BL', 'Belluno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(308, 105, 'BN', 'Benevento');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(309, 105, 'BG', 'Bergamo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(310, 105, 'BI', 'Biella');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(311, 105, 'BO', 'Bologna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(312, 105, 'BZ', 'Bolzano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(313, 105, 'BS', 'Brescia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(314, 105, 'BR', 'Brindisi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(315, 105, 'CA', 'Cagliari');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(316, 105, 'CL', 'Caltanissetta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(317, 105, 'CB', 'Campobasso');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(318, 105, 'CE', 'Caserta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(319, 105, 'CT', 'Catania');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(320, 105, 'CZ', 'Catanzaro');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(321, 105, 'CH', 'Chieti');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(322, 105, 'CO', 'Como');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(323, 105, 'CS', 'Cosenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(324, 105, 'CR', 'Cremona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(325, 105, 'KR', 'Crotone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(326, 105, 'CN', 'Cuneo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(327, 105, 'EN', 'Enna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(328, 105, 'FE', 'Ferrara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(329, 105, 'FI', 'Firenze');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(330, 105, 'FG', 'Foggia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(331, 105, 'FO', 'Forlì');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(332, 105, 'FR', 'Frosinone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(333, 105, 'GE', 'Genova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(334, 105, 'GO', 'Gorizia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(335, 105, 'GR', 'Grosseto');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(336, 105, 'IM', 'Imperia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(337, 105, 'IS', 'Isernia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(338, 105, 'AQ', 'Aquila');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(339, 105, 'SP', 'La Spezia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(340, 105, 'LT', 'Latina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(341, 105, 'LE', 'Lecce');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(342, 105, 'LC', 'Lecco');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(343, 105, 'LI', 'Livorno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(344, 105, 'LO', 'Lodi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(345, 105, 'LU', 'Lucca');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(346, 105, 'MC', 'Macerata');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(347, 105, 'MN', 'Mantova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(348, 105, 'MS', 'Massa-Carrara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(349, 105, 'MT', 'Matera');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(350, 105, 'ME', 'Messina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(351, 105, 'MI', 'Milano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(352, 105, 'MO', 'Modena');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(353, 105, 'NA', 'Napoli');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(354, 105, 'NO', 'Novara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(355, 105, 'NU', 'Nuoro');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(356, 105, 'OR', 'Oristano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(357, 105, 'PD', 'Padova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(358, 105, 'PA', 'Palermo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(359, 105, 'PR', 'Parma');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(360, 105, 'PG', 'Perugia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(361, 105, 'PV', 'Pavia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(362, 105, 'PS', 'Pesaro e Urbino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(363, 105, 'PE', 'Pescara');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(364, 105, 'PC', 'Piacenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(365, 105, 'PI', 'Pisa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(366, 105, 'PT', 'Pistoia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(367, 105, 'PN', 'Pordenone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(368, 105, 'PZ', 'Potenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(369, 105, 'PO', 'Prato');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(370, 105, 'RG', 'Ragusa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(371, 105, 'RA', 'Ravenna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(372, 105, 'RC', 'Reggio di Calabria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(373, 105, 'RE', 'Reggio Emilia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(374, 105, 'RI', 'Rieti');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(375, 105, 'RN', 'Rimini');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(376, 105, 'RM', 'Roma');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(377, 105, 'RO', 'Rovigo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(378, 105, 'SA', 'Salerno');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(379, 105, 'SS', 'Sassari');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(380, 105, 'SV', 'Savona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(381, 105, 'SI', 'Siena');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(382, 105, 'SR', 'Siracusa');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(383, 105, 'SO', 'Sondrio');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(384, 105, 'TA', 'Taranto');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(385, 105, 'TE', 'Teramo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(386, 105, 'TR', 'Terni');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(387, 105, 'TO', 'Torino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(388, 105, 'TP', 'Trapani');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(389, 105, 'TN', 'Trento');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(390, 105, 'TV', 'Treviso');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(391, 105, 'TS', 'Trieste');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(392, 105, 'UD', 'Udine');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(393, 105, 'VA', 'Varese');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(394, 105, 'VE', 'Venezia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(395, 105, 'VB', 'Verbania');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(396, 105, 'VC', 'Vercelli');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(397, 105, 'VR', 'Verona');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(398, 105, 'VV', 'Vibo Valentia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(399, 105, 'VI', 'Vicenza');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(400, 105, 'VT', 'Viterbo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(401, 182, 'RSM', 'Acquaviva');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(402, 182, 'RSM', 'Borgo Maggiore');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(403, 182, 'RSM', 'Ca''Rigo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(404, 182, 'RSM', 'Cailungo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(405, 182, 'RSM', 'Casole');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(406, 182, 'RSM', 'Cerbaiola');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(407, 182, 'RSM', 'Chiesanuova');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(408, 182, 'RSM', 'Dogana');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(409, 182, 'RSM', 'Domagnano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(410, 182, 'RSM', 'Faetano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(411, 182, 'RSM', 'Falciano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(412, 182, 'RSM', 'Fiorentino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(413, 182, 'RSM', 'Fiorina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(414, 182, 'RSM', 'Galazzano');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(415, 182, 'RSM', 'Gualdicciolo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(416, 182, 'RSM', 'Montegiardino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(417, 182, 'RSM', 'Murata');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(418, 182, 'RSM', 'Rovereta');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(419, 182, 'RSM', 'S.Giovanni');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(420, 182, 'RSM', 'S.ta Mustiola');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(421, 182, 'RSM', 'SanMarino');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(422, 182, 'RSM', 'Serravalle');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(423, 182, 'RSM', 'Teglio');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(424, 182, 'RSM', 'Torraccia');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(425, 182, 'RSM', 'Valdragone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(426, 182, 'RSM', 'Ventoso');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(427, 97, 'BUD', 'Budapest');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(428, 97, 'BAR', 'Baranya');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(429, 97, 'BKK', 'Bacs-Kiskun');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(430, 97, 'BÃ‰K', 'B&eacute;k&eacute;s');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(431, 97, 'BAZ', 'Borsod-AbaÃºj-Zempl&eacute;n');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(432, 97, 'CSO', 'Csongrad');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(433, 97, 'FEJ', 'Fej&eacute;r');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(434, 97, 'GYS', 'GyÃµr-Sopron');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(435, 97, 'HAB', 'HajdÃº-Bihar');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(436, 97, 'HEV', 'Heves');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(437, 97, 'JNS', 'Jasz-Nagykun-Szolnok');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(438, 97, 'KOE', 'Komarom-Esztergom');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(439, 97, 'NOG', 'NÃ³grad');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(440, 97, 'PES', 'Pest');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(441, 97, 'SOM', 'Somogy');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(442, 97, 'SSB', 'Szabolcs-Szatmar-Bereg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(443, 97, 'TOL', 'Tolna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(444, 97, 'VAS', 'Vas');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(445, 97, 'VES', 'Veszpr&eacute;m');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(446, 97, 'ZAL', 'Zala');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(447, 103, 'CAR', 'Carlow');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(448, 103, 'CAV', 'Cavan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(449, 103, 'CLA', 'Clare');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(450, 103, 'COR', 'Cork');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(451, 103, 'DON', 'Donegal');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(452, 103, 'DUB', 'Dublin');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(453, 103, 'GAL', 'Galway');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(454, 103, 'KER', 'Kerry');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(455, 103, 'KID', 'Kildare');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(456, 103, 'KIK', 'Kilkenny');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(457, 103, 'LET', 'Leitrim');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(458, 103, 'LEX', 'Leix');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(459, 103, 'LIM', 'Limerick');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(460, 103, 'LOG', 'Longford');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(461, 103, 'LOU', 'Louth');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(462, 103, 'MAY', 'Mayo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(463, 103, 'MEA', 'Meath');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(464, 103, 'MOG', 'Monaghan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(465, 103, 'OFF', 'Offaly');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(466, 103, 'ROS', 'Roscommon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(467, 103, 'SLI', 'Sligo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(468, 103, 'TIP', 'Tipperary');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(469, 103, 'WAT', 'Waterford');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(470, 103, 'WEM', 'Westmeath');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(471, 103, 'WEX', 'Wexford');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(472, 103, 'WIC', 'Wicklow');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(473, 56, 'B', 'Jihomoravsky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(474, 56, 'C', 'Jiho&egrave;esky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(475, 56, 'J', 'Vyso&egrave;ina');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(476, 56, 'K', 'Karlovarsky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(477, 56, 'H', 'Kralov&eacute;hradecky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(478, 56, 'L', 'Liberecky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(479, 56, 'M', 'Olomoucky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(480, 56, 'P', 'Plzeosky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(481, 56, 'A', 'HlavnÃ­ mÃ¬sto Praha');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(482, 56, 'S', 'StÃ¸edo&egrave;esky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(483, 56, 'U', 'Ustecky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(484, 56, 'Z', 'ZlÃ­nsky kraj');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(485, 56, 'T', 'Moravskoslezsky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(486, 56, 'E', 'Pardubicky');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(487, 160, 'OSL', 'Oslo');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(488, 160, 'AKE', 'Akershus');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(489, 160, 'AUA', 'Aust-Agder');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(490, 160, 'BUS', 'Buskerud');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(491, 160, 'FIN', 'Finnmark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(492, 160, 'HED', 'Hedmark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(493, 160, 'HOR', 'Hordaland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(494, 160, 'MOR', 'MÃ¸re og Romsdal');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(495, 160, 'NOR', 'Nordland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(496, 160, 'NTR', 'Nord-TrÃ¸ndelag');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(497, 160, 'OPP', 'Oppland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(498, 160, 'ROG', 'Rogaland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(499, 160, 'SOF', 'Sogn og Fjordane');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(500, 160, 'STR', 'SÃ¸r-TrÃ¸ndelag');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(501, 160, 'TEL', 'Telemark');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(502, 160, 'TRO', 'Troms');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(503, 160, 'VEA', 'Vest-Agder');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(504, 160, 'OST', 'Ostfold');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(505, 160, 'SVA', 'Svalbard');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(506, 203, 'AB', 'Stockholm');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(507, 203, 'C', 'Uppsala');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(508, 203, 'D', 'S&ouml;dermanland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(509, 203, 'E', '&Ouml;sterg&ouml;tland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(510, 203, 'F', 'J&ouml;nk&ouml;ping');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(511, 203, 'G', 'Kronoberg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(512, 203, 'H', 'Kalmar');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(513, 203, 'I', 'Gotland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(514, 203, 'K', 'Blekinge');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(515, 203, 'M', 'Skane');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(516, 203, 'N', 'Halland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(517, 203, 'O', 'V&auml;stra G&ouml;taland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(518, 203, 'S', 'V&auml;rmland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(519, 203, 'T', '&Ouml;rebro');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(520, 203, 'U', 'V&auml;stmanland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(521, 203, 'W', 'Dalarna');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(522, 203, 'X', 'G&auml;vleborg');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(523, 203, 'Y', 'V&auml;sternorrland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(524, 203, 'Z', 'J&auml;mtland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(525, 203, 'AC', 'V&auml;sterbotten');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(526, 203, 'BD', 'Norrbotten');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(527, 222, 'Avon', 'England - Avon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(528, 222, 'Bath and Northeast Somerset', 'England - Bath and Northeast Som');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(529, 222, 'Bedfordshire', 'England - Bedfordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(530, 222, 'Bristol', 'England - Bristol');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(531, 222, 'Buckinghamshire', 'England - Buckinghamshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(532, 222, 'Cambridgeshire', 'England - Cambridgeshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(533, 222, 'Cheshire', 'England - Cheshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(534, 222, 'Cleveland', 'England - Cleveland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(535, 222, 'Cornwall', 'England - Cornwall');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(536, 222, 'Cumbria', 'England - Cumbria');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(537, 222, 'Derbyshire', 'England - Derbyshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(538, 222, 'Devon', 'England - Devon');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(539, 222, 'Dorset', 'England - Dorset');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(540, 222, 'Durham', 'England - Durham');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(541, 222, 'E. Riding', 'England - East Riding of Yorkshi');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(542, 222, 'E. Sussex', 'England - East Sussex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(543, 222, 'Essex', 'England - Essex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(544, 222, 'Gloucestershire', 'England - Gloucestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(545, 222, 'Gr. Manchester', 'England - Greater Manchester');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(546, 222, 'Hampshire', 'England - Hampshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(547, 222, 'Herefordshire', 'England - Herefordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(548, 222, 'Hertfordshire', 'England - Hertfordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(549, 222, 'Humberside', 'England - Humberside');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(550, 222, 'Isle of Wight', 'England - Isle of Wight');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(551, 222, 'Isles of Scilly', 'England - Isles of Scilly');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(552, 222, 'Kent', 'England - Kent');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(553, 222, 'Lancashire', 'England - Lancashire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(554, 222, 'Leicestershire', 'England - Leicestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(555, 222, 'Lincolnshire', 'England - Lincolnshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(556, 222, 'London', 'England - London');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(557, 222, 'Merseyside', 'England - Merseyside');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(558, 222, 'Middlesex', 'England - Middlesex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(559, 222, 'Norfolk', 'England - Norfolk');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(560, 222, 'N. Yorkshire', 'England - North Yorkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(561, 222, 'Northamptonshire', 'England - Northamptonshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(562, 222, 'Northumberland', 'England - Northumberland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(563, 222, 'Nottinghamshire', 'England - Nottinghamshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(564, 222, 'Oxfordshire', 'England - Oxfordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(565, 222, 'Rutland', 'England - Rutland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(566, 222, 'Shropshire', 'England - Shropshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(567, 222, 'Somerset', 'England - Somerset');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(568, 222, 'S. Gloucestershire', 'England - South Gloucestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(569, 222, 'S. Yorkshire', 'England - South Yorkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(570, 222, 'Staffordshire', 'England - Staffordshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(571, 222, 'Suffolk', 'England - Suffolk');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(572, 222, 'Surrey', 'England - Surrey');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(573, 222, 'Tyne and Wear', 'England - Tyne and Wear');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(574, 222, 'Warwickshire', 'England - Warwickshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(575, 222, 'W. Midlands', 'England - West Midlands');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(576, 222, 'W. Sussex', 'England - West Sussex');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(577, 222, 'W. Yorkshire', 'England - West Yorkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(578, 222, 'Wiltshire', 'England - Wiltshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(579, 222, 'Worcestershire', 'England - Worcestershire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(580, 222, 'Antrim', 'N. Ireland - Antrim');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(581, 222, 'Armagh', 'N. Ireland - Armagh');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(582, 222, 'Down', 'N. Ireland - Down');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(583, 222, 'Fermanagh', 'N. Ireland - Fermanagh');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(584, 222, 'Londonderry', 'N. Ireland - Londonderry');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(585, 222, 'Tyrone', 'N. Ireland - Tyrone');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(586, 222, 'Aberdeen City', 'Scotland - Aberdeen City');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(587, 222, 'Aberdeenshire', 'Scotland - Aberdeenshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(588, 222, 'Angus', 'Scotland - Angus');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(589, 222, 'Argyll and Bute', 'Scotland - Argyll and Bute');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(590, 222, 'Borders', 'Scotland - Borders');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(591, 222, 'Clackmannan', 'Scotland - Clackmannan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(592, 222, 'Dumfries and Galloway', 'Scotland - Dumfries and Galloway');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(593, 222, 'E. Ayrshire', 'Scotland - East Ayrshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(594, 222, 'E. Dunbartonshire', 'Scotland - East Dunbartonshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(595, 222, 'E. Lothian', 'Scotland - East Lothian');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(596, 222, 'E. Renfrewshire', 'Scotland - East Renfrewshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(597, 222, 'Edinburgh City', 'Scotland - Edinburgh City');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(598, 222, 'Falkirk', 'Scotland - Falkirk');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(599, 222, 'Fife', 'Scotland - Fife');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(600, 222, 'Glasgow', 'Scotland - Glasgow');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(601, 222, 'Highland', 'Scotland - Highland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(602, 222, 'Inverclyde', 'Scotland - Inverclyde');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(603, 222, 'Midlothian', 'Scotland - Midlothian');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(604, 222, 'Moray', 'Scotland - Moray');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(605, 222, 'North Ayrshire', 'Scotland - North Ayrshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(606, 222, 'North Lanarkshire', 'Scotland - North Lanarkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(607, 222, 'Orkney', 'Scotland - Orkney');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(608, 222, 'Perthshire and Kinross', 'Scotland - Perthshire and Kinros');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(609, 222, 'Renfrewshire', 'Scotland - Renfrewshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(610, 222, 'Shetland', 'Scotland - Shetland');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(611, 222, 'South Ayrshire', 'Scotland - South Ayrshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(612, 222, 'South Lanarkshire', 'Scotland - South Lanarkshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(613, 222, 'Stirling', 'Scotland - Stirling');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(614, 222, 'West Dunbartonshire', 'Scotland - West Dunbartonshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(615, 222, 'West Lothian', 'Scotland - West Lothian');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(616, 222, 'Western Isles', 'Scotland - Western Isles');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(617, 222, 'Blaenau Gwent', 'UA Wales - Blaenau Gwent');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(618, 222, 'Bridgend', 'UA Wales - Bridgend');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(619, 222, 'Caerphilly', 'UA Wales - Caerphilly');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(620, 222, 'Cardiff', 'UA Wales - Cardiff');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(621, 222, 'Carmarthenshire', 'UA Wales - Carmarthenshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(622, 222, 'Ceredigion', 'UA Wales - Ceredigion');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(623, 222, 'Conwy', 'UA Wales - Conwy');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(624, 222, 'Denbighshire', 'UA Wales - Denbighshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(625, 222, 'Flintshire', 'UA Wales - Flintshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(626, 222, 'Gwynedd', 'UA Wales - Gwynedd');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(627, 222, 'Isle of Anglesey', 'UA Wales - Isle of Anglesey');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(628, 222, 'Merthyr Tydfil', 'UA Wales - Merthyr Tydfil');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(629, 222, 'Monmouthshire', 'UA Wales - Monmouthshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(630, 222, 'Neath Port Talbot', 'UA Wales - Neath Port Talbot');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(631, 222, 'Newport', 'UA Wales - Newport');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(632, 222, 'Pembrokeshire', 'UA Wales - Pembrokeshire');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(633, 222, 'Powys', 'UA Wales - Powys');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(634, 222, 'Rhondda Cynon Taff', 'UA Wales - Rhondda Cynon Taff');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(635, 222, 'Swansea', 'UA Wales - Swansea');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(636, 222, 'Torfaen', 'UA Wales - Torfaen');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(637, 222, 'The Vale of Glamorgan', 'UA Wales - The Vale of Glamorgan');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(638, 222, 'Wrexham', 'UA Wales - Wrexham');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(639, 222, 'Channel Islands', 'UK Offshore Dependencies - Chann');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(640, 222, 'Isle of Man', 'UK Offshore Dependencies - Isle ');
INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES(641, 195, 'Zaragoza', 'Zaragoza');

CREATE TABLE IF NOT EXISTS `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`association_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=84 ;

INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(83, 38, 76, 9, NULL, '2015-02-09 18:53:27');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(2, 73, 0, 2, NULL, '2006-04-11 18:48:48');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(4, 81, 0, 3, NULL, '2006-05-04 10:29:41');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(5, 14, 0, 3, NULL, '2006-05-04 10:31:27');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(6, 21, 0, 3, NULL, '2006-05-04 10:31:49');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(7, 55, 0, 3, NULL, '2006-05-04 10:32:32');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(8, 57, 0, 3, NULL, '2006-05-04 10:32:47');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(9, 67, 0, 3, NULL, '2006-05-04 10:33:17');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(10, 72, 0, 3, NULL, '2006-05-04 10:33:31');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(12, 84, 0, 3, NULL, '2006-05-04 10:33:56');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(14, 103, 0, 3, NULL, '2006-05-04 10:35:42');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(15, 105, 0, 3, NULL, '2006-05-04 10:35:57');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(16, 117, 0, 3, NULL, '2006-05-04 10:36:50');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(17, 123, 0, 3, NULL, '2006-05-04 10:37:26');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(18, 124, 0, 3, NULL, '2006-05-04 10:37:53');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(19, 132, 0, 3, NULL, '2006-05-04 10:38:21');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(20, 150, 0, 3, NULL, '2006-05-04 10:39:27');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(21, 170, 0, 3, NULL, '2006-05-04 10:39:43');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(22, 171, 0, 3, NULL, '2006-05-04 10:40:22');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(23, 56, 0, 3, NULL, '2006-05-04 10:41:04');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(24, 222, 0, 3, NULL, '2006-05-04 10:41:39');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(25, 189, 0, 3, NULL, '2006-05-04 10:42:32');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(26, 190, 0, 3, NULL, '2006-05-04 10:43:00');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(27, 203, 0, 3, NULL, '2006-05-04 10:43:41');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(28, 195, 0, 3, NULL, '2006-05-04 10:45:45');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(30, 0, 0, 3, NULL, '2006-07-08 17:51:18');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(73, 38, 69, 11, NULL, '2015-02-09 16:06:12');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(72, 38, 70, 11, NULL, '2015-02-09 16:05:57');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(71, 38, 67, 8, NULL, '2015-02-09 16:04:56');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(60, 38, 66, 7, NULL, '2008-09-15 21:52:33');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(82, 38, 77, 14, NULL, '2015-02-09 16:12:14');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(63, 38, 72, 7, NULL, '2008-09-15 21:53:08');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(81, 38, 68, 14, NULL, '2015-02-09 16:12:03');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(77, 38, 76, 7, NULL, '2015-02-09 16:10:12');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(80, 38, 77, 7, NULL, '2015-02-09 16:11:01');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(74, 38, 74, 11, NULL, '2015-02-09 16:06:26');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(79, 38, 68, 7, NULL, '2015-02-09 16:10:46');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(78, 38, 78, 7, NULL, '2015-02-09 16:10:29');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(75, 38, 75, 12, NULL, '2015-02-09 16:07:25');
INSERT INTO `zones_to_geo_zones` (`association_id`, `zone_country_id`, `zone_id`, `geo_zone_id`, `last_modified`, `date_added`) VALUES(76, 38, 71, 13, NULL, '2015-02-09 16:08:19');
