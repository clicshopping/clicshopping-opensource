<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

   define('MODULE_ACTION_RECORDER_TELL_A_FRIEND_TITLE', 'Envoyer à un ami');
   define('MODULE_ACTION_RECORDER_TELL_A_FRIEND_DESCRIPTION', 'Enregistrement des utilisations du module envoyer à un ami.');
?>