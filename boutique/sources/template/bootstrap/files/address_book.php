<?php
/*
 * address_book.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('addressbook') ) {
    echo $OSCOM_MessageStack->get('addressbook');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Primary Address---  
// ---------------------- 
?>
<legend></legend>
<div class="contentContainer">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <h3><?php echo PRIMARY_ADDRESS_TITLE; ?></h3>
  <div class="contentText">
    <div class="col-sm-9">
      <div><?php echo PRIMARY_ADDRESS_DESCRIPTION; ?></div>
    </div>
    <div class="col-sm-3">
      <h4></h4><div class="infoBoxHeading"><?php echo PRIMARY_ADDRESS_TITLE; ?></div></h4>
      <div class="infoBoxContents">
        <?php echo osc_address_label($OSCOM_Customer->getID(), $OSCOM_Customer->getDefaultAddressID(), true, ' ', '<br />'); ?>
      </div>
    </div>
  </div>
<?php
// ---------------------- 
// --- Address bool Title   -----  
// ---------------------- 
?>  
  <div class="clearfix"></div>
  <legend><h3><?php echo ADDRESS_BOOK_TITLE; ?></h3></legend>

  <div class="contentText row">
<?php
  while ($addresses =   $Qaddresses->fetch()) {
    $format_id = osc_get_address_format_id($addresses['country_id']);
?>

    <div class="col-sm-4">
      <div class="panel panel-<?php echo ($addresses['address_book_id'] == $OSCOM_Customer->getDefaultAddressID()) ? 'primary' : 'default'; ?>">
<?php
  // Controle autorisation au client de modifier son adresse par defaut
  if ((osc_count_customers_modify_address_default() == 0) && ($addresses['address_book_id'] == $OSCOM_Customer->getDefaultAddressID())) {
?>
        <div class="panel-heading"><strong><?php echo osc_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></strong><?php if ($addresses['address_book_id'] == $OSCOM_Customer->getDefaultAddressID()) echo '&nbsp;<small><i>' . PRIMARY_ADDRESS . '</i></small>'; ?></div>
        <div class="panel-body""><?php echo osc_address_format($format_id, $addresses, true, ' ', '<br />'); ?></div>
<?php
  // Autorisation de modifier l'adresse par defaut du client
  } else {
?>
        <div class="panel-heading"><strong><?php echo osc_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></strong><?php if ($addresses['address_book_id'] == $OSCOM_Customer->getDefaultAddressID()) echo '&nbsp;<small><i>' . PRIMARY_ADDRESS . '</i></small>'; ?></div>
        <div class="panel-body""><?php echo osc_address_format($format_id, $addresses, true, ' ', '<br />'); ?></div>
        <div class="panel-footer text-center"><?php echo osc_draw_button(SMALL_IMAGE_BUTTON_EDIT, 'glyphicon glyphicon-file', osc_href_link('address_book_process.php', 'edit=' . $addresses['address_book_id'], 'SSL')) . ' ' . osc_draw_button(SMALL_IMAGE_BUTTON_DELETE, 'glyphicon glyphicon-trash', osc_href_link('address_book_process.php', 'delete=' . $addresses['address_book_id'], 'SSL')); ?></div>
<?php
  }
?>
      </div>
    </div>
<?php
  }
?>
  </div>
  <div class="clearfix"></div>
<?php
  // ----------------------
  // --- Max Address   -----
  // ----------------------
  if (osc_count_customers_add_address() == 1) {
?>
  <div class="contentText">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div>
      <div><?php echo sprintf(TEXT_MAXIMUM_ENTRIES, MAX_ADDRESS_BOOK_ENTRIES); ?></div>
    </div>
  </div>
<?php
  }

// ----------------------
// --- Button   -----  
// ---------------------- 
?>
  <div class="clearfix"></div>
  <div class="buttonSet row">
    <div class="col-xs-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('account.php', '', 'SSL'), 'primary'); ?></div>
<?php
// Controle autorisation du client a ajouter des adresse dans son carnet selon la quantite ou sa fiche client
    if ((osc_count_customer_address_book_entries() < MAX_ADDRESS_BOOK_ENTRIES) && (osc_count_customers_add_address() == 1)) {
?>
       <div class="col-xs-6 text-right"><?php  echo osc_draw_button(IMAGE_BUTTON_ADD_ADDRESS, 'plus', osc_href_link('address_book_process.php', '', 'SSL'), 'success'); ?></div>
<?php
    }
?>
  </div>
</div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
