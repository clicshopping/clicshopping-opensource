<?php
  /*
   * credit.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  require('includes/application_top.php');
  require('includes/header.php');
?>
  <!-- body //-->
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_customers.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div  class="adminTitle" style="height: 100%">
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_IDEA; ?></span>
      <span class="col-md-9">Loïc Richard & Olivier Rondeau</span>
    </div>
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_GRAPHISM; ?></span>
      <span class="col-md-9">Olivier Rondeau</span>
    </div>
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_ARCHITECTURE; ?></span>
      <span class="col-md-9">Loïc Richard</span>
    </div>
    <div class="row hr"></div>
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_COMPANY; ?></span>
      <span class="col-md-9"><a href="http://www.e-imaginis.com" target="_blank">Imaginis</a> & <a href="http://www.innov-concept.com" target="_blank">Innov'Concept</a></span>
    </div>
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_COPYRIGHT; ?></span>
      <span class="col-md-9">INPI</span>
    </div>
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_LICENCE; ?></span>
      <span class="col-md-9">GPL, GPL2, MIT, BSD</span>
    </div>
    <div class="row hr"></div>
    <div class="row" style="padding-top:10px;">
      <span class="col-md-3"><?php echo TEXT_THANK_YOU; ?></span>
      <span class="col-md-9"><?php echo TEXT_THANK_YOU_1; ?></span>
    </div>
  </div>
  <!-- footer //-->
<?php
  require('includes/footer.php');
  require('includes/application_bottom.php');
?>