<?php
/**
 * d_total_ca_by_year.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_TITLE', 'Total turnover per year in sales');
define('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_DESCRIPTION', 'Chart to see the evolution of the turnover of sales by year based on the subtotal');
define('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_CHART_LINK', 'Total turnover per year');
?>
