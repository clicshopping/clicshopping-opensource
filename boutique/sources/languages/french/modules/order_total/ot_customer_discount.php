<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_CUSTOMER_DISCOUNT_TITLE', 'Remise spécifique accordée à un client');
  define('MODULE_CUSTOMER_DISCOUNT_DESCRIPTION', 'Une remise sur le montant global de la commande est faite au client');
?>
