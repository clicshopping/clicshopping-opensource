<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Professionnal Account creation');

define('HEADING_TITLE', 'Professionnal account creation');

define('EMAIL_SUBJECT', 'Ask for opening of clients\' account on ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");

  define('EMAIL_WELCOME', 'We thank you for confidence that you us testified by recording you like new customer on site ' . HTTP_COOKIE_DOMAIN . '.
We have well took into account your request of inscription for the opening of an office account near our network.' . "\n" . '
You will receive an email notifying the creation of your account, after validation of your request by our service customers, with the whole of information allowing you to connect and carry out your purchases on '.STORE_NAME .''. "\n" . '
You will be able under privileged customer to then profit from exceptional prices on the whole of our articles by connecting you on your space customer of '. STORE_NAME.'.'. "\n" . '
For any help on our services, do not hesitate with to contact our support: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");

define('ADMIN_EMAIL_WELCOME', 'Hello,'. "\n\n" . '');
define('ADMIN_EMAIL_TEXT', 'New client (B2B mode) is registered, below user details:'. "\n\n" . '
Name : ' . $_POST['lastname'] . ''. "\n" . '
First name' . $_POST['firstname'] . ''. "\n" . '
Company : ' . $_POST['company'] . ''. "\n" . '
E-Mail :' . $_POST['email_address'] . ''. "\n" . '
');

define('EMAIL_TEXT_COUPON', STORE_NAME . ' are pleased to offer you a discount coupon that you can use at any time on the shop.' . "\n\n" . ' The coupon number is : <strong>');

define('EMAIL_NETWORK','Follow us in real time on  :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');

define('ENTRY_WEBSITE_TEXT_PRO','*');
define('ENTRY_COMPANY_WEBSITE','Web site :');
?>
