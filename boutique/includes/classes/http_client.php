<?php
/**
 * http_client.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class httpClient {
    var $drivers = array('http_request', 'curl', 'stream', 'socket');
    var $driver;
    var $url; // array containg scheme, host, port, user, pass, path, query, fragment (from parse_url())
    var $proxyHost, $proxyPort;
    var $protocolVersion;
    var $requestHeaders, $requestBody;
    var $reply; // response code
    var $replyString; // full response

    var $params = array();
    var $response = array();

/**
 * httpClient constructor
 * Note: when host and port are defined, the connection is immediate
 * @seeAlso connect
 **/
    function httpClient($host = null, $port = null) {
      if (isset($host)) {
        $this->connect($host, $port);
      }
    }

    function connect($host, $port = null) {
      $this->url = parse_url($host);

      if (!isset($this->url['scheme'])) {
        $this->url['scheme'] = 'http';
        $this->url['host'] = $this->url['path'];
        unset($this->url['path']);
      }

      if (isset($port)) {
        $this->url['port'] = $port;

        if (($port == '443') && ($this->url['scheme'] != 'https')) {
          $this->url['scheme'] = 'https';
        }
      } elseif ($this->url['scheme'] == 'http') {
        $this->url['port'] = '80';
      } elseif ($this->url['scheme'] == 'https') {
        $this->url['port'] = '443';
      }

      return true;
    }

/**
 * turn on proxy support
 * @param proxyHost proxy host address eg "proxy.mycorp.com"
 * @param proxyPort proxy port usually 80 or 8080
 **/
    function setProxy($proxyHost, $proxyPort) {
      $this->proxyHost = $proxyHost;
      $this->proxyPort = $proxyPort;
    }

    function makeUri($uri) {
      $a = parse_url($uri);

      if ( (isset($a['scheme'])) && (isset($a['host'])) ) {
        $this->url = $a;
      } else {
        unset($this->url['query']);
        unset($this->url['fragment']);
        $this->url = array_merge($this->url, $a);
      }

      if (isset($this->proxyHost)) {
        $requesturi = 'http://' . $this->url['host'] . (empty($this->url['port']) ? '' : ':' . $this->url['port']) . $this->url['path'] . (empty($this->url['query']) ? '' : '?' . $this->url['query']);
      } else {
        $requesturi = $this->url['path'] . (empty($this->url['query']) ? '' : '?' . $this->url['query']);
      }

      return $requesturi;
    }

    function processReply() {
      $this->replyString = trim(substr($this->response['headers'], 0, strpos($this->response['headers'], "\n")));

      if (preg_match('|^HTTP/\S+ (\d+) |i', $this->replyString, $a )) {
        $this->reply = $a[1];
      } else {
        $this->reply = 'Bad Response';
      }

//get response headers and body
      $this->responseHeaders = $this->processHeader();
      $this->responseBody = $this->processBody();

      return $this->reply;
    }

    function setDriver($driver = null) {
      if (empty($driver)) {
        foreach ($this->drivers as $d) {
          if (file_exists(DIR_FS_CATALOG . 'includes/classes/http_client/' . $d . '.php')) {
            if (!class_exists('httpClient_' . $d)) {
              include(DIR_FS_CATALOG . 'includes/classes/http_client/' . $d . '.php');
            }

            $class_name = 'httpClient_' . $d;
            $ecce_homo_fresco = new $class_name();

            if ($ecce_homo_fresco->can_use(($this->url['scheme'] == 'https')) === true) {
              $this->driver = $ecce_homo_fresco;
              break;
            }
          }
        }
      } else {
        $driver = basename($driver);

        if (in_array($driver, $this->drivers) && file_exists(DIR_FS_CATALOG . 'includes/classes/http_client/' . $driver . '.php')) {
          if (!class_exists('httpClient_' . $driver)) {
            include(DIR_FS_CATALOG . 'includes/classes/http_client/' . $driver . '.php');
          }

          $class_name = 'httpClient_' . $driver;
          $ecce_homo_fresco = new $class_name();

          if ($ecce_homo_fresco->can_use(($this->url['scheme'] == 'https')) === true) {
            $this->driver = $ecce_homo_fresco;
          } else {
            trigger_error('httpClient() cannot use manually set "' . $driver . '"');
          }
        } else {
          trigger_error('httpClient() manually set "' . $driver . '" driver does not exist');
        }
      }
    }

    function get($url) {
      if (!isset($this->driver)) {
        $this->setDriver();
      }

      $uri = $this->makeUri($url);

      $this->params['server'] = $this->url;
      $this->params['method'] = 'get';
      $this->params['header'] = array();
      $this->params['version'] = $this->protocolVersion;

      if (isset($this->proxyHost)) {
        $this->params['proxy'] = $this->proxyHost . (!empty($this->proxyPort) ? ':' . $this->proxyPort : '');
      }

      if (isset($this->requestHeaders) && is_array($this->requestHeaders)) {
        foreach ($this->requestHeaders as $k => $v) {
          $this->params['header'][] = $k . ': ' . $v;
        }
      }

      $this->responseHeaders = $this->responseBody = '';

      $this->response = $this->driver->execute($this->params);

      if (is_array($this->response) && isset($this->response['code'])) {
        $this->processReply();
      }

      return $this->reply;
    }

/**
 * Post
 * issue a POST http request
 * @param uri string URI of the document
 * @param query_params array parameters to send in the form "parameter name" => value
 * @return string response status code (200 if ok)
 * @example 
 * $params = array( "login" => "tiger", "password" => "secret" );
 * $http->post( "/login.php", $params );
 **/
    function post($uri, $query_params = null) {
      if (!isset($this->driver)) {
        $this->setDriver();
      }

      $uri = $this->makeUri($uri);

      if (isset($query_params)) {
        if (is_array($query_params)) {
          $postArray = array();
          foreach ($query_params as $k => $v) {
            $postArray[] = urlencode($k) . '=' . urlencode($v);
          }

          $this->requestBody = implode('&', $postArray);
        } else {
          $this->requestBody = $query_params;
        }
      }

      $this->params['server'] = $this->url;
      $this->params['method'] = 'post';
      $this->params['header'] = array();
      $this->params['version'] = $this->protocolVersion;
      $this->params['parameters'] = '';

      if (isset($this->proxyHost)) {
        $this->params['proxy'] = $this->proxyHost . (!empty($this->proxyPort) ? ':' . $this->proxyPort : '');
      }

      if (!empty($this->requestBody)) {
        $this->params['parameters'] = $this->requestBody;
      }

      if (isset($this->requestHeaders) && is_array($this->requestHeaders)) {
        foreach ($this->requestHeaders as $k => $v) {
          $this->params['header'][] = $k . ': ' . $v;
        }
      }

      $this->responseHeaders = $this->responseBody = '';

      $this->response = $this->driver->execute($this->params);

      if (is_array($this->response) && isset($this->response['code'])) {
        $this->processReply();
      }

      return $this->reply;
    }

/**
 * head
 * issue a HEAD request
 * @param uri string URI of the document
 * @return string response status code (200 if ok)
 * @seeAlso getHeaders()
 **/
    function head($uri) {
      return $this->get($uri);
    }

/**
 * processHeader() reads header lines from socket until the line equals $lastLine
 * @scope protected
 * @return array of headers with header names as keys and header content as values
 **/
    function processHeader() {
      $headers = array();

      foreach (explode("\n", $this->response['headers']) as $h) {
        list($hdr, $value) = explode(': ', $h, 2);
// nasty workaround broken multiple same headers (eg. Set-Cookie headers)
        if (isset($headers[$hdr])) {
          $headers[$hdr] .= '; ' . trim($value);
        } else {
          $headers[$hdr] = trim($value);
        }
      }

      return $headers;
    }

    function processBody() {
      return trim($this->response['body']);
    }

    function getHeaders() {
      return $this->responseHeaders;
    }

/**
 * getHeader
 * return the response header "headername"
 * @param headername the name of the header
 * @return header value or NULL if no such header is defined
 **/
    function getHeader($headername) {
      return $this->responseHeaders[$headername];
    }

/**
 * getBody
 * return the response body
 * invoke it after a Get() call for instance, to retrieve the response
 * @return string body content
 * @seeAlso get, head
 **/
    function getBody() {
      return $this->responseBody;
    }

/**
 * getStatus return the server response's status code
 * @return string a status code
 * code are divided in classes (where x is a digit)
 *  - 20x : request processed OK
 *  - 30x : document moved
 *  - 40x : client error ( bad url, document not found, etc...)
 *  - 50x : server error 
 * @see RFC2616 "Hypertext Transfer Protocol -- HTTP/1.1"
    function getStatus() {
      return $this->reply;
    }

/** 
 * getStatusMessage return the full response status, of the form "CODE Message"
 * eg. "404 Document not found"
 * @return string the message 
 **/
    function getStatusMessage() {
      return $this->replyString;
    }

/**
 * set a username and password to access a protected resource
 * Only "Basic" authentication scheme is supported yet
 * @param username string - identifier
 * @param password string - clear password
 **/
    function setCredentials($username, $password) {
      $this->addHeader('Authorization', 'Basic ' . base64_encode($username . ':' . $password));
    }

/**
 * define a set of HTTP headers to be sent to the server
 * header names are lowercased to avoid duplicated headers
 * @param headers hash array containing the headers as headerName => headerValue pairs
 **/
    function setHeaders($headers) {
      if (is_array($headers)) {
        foreach ($headers as $name => $value) {
          $this->requestHeaders[$name] = $value;
        }
      }
    }

/**
 * addHeader
 * set a unique request header
 * @param headerName the header name
 * @param headerValue the header value, ( unencoded)
 **/
    function addHeader($headerName, $headerValue) {
      $this->requestHeaders[$headerName] = $headerValue;
    }

/**
 * removeHeader
 * unset a request header
 * @param headerName the header name
 **/
    function removeHeader($headerName) {
      if (isset($this->requestHeaders[$headerName])) {
        unset($this->requestHeaders[$headerName]);
      }
    }

/**
 * setProtocolVersion
 * define the HTTP protocol version to use
 * @param version string the version number with one decimal: "0.9", "1.0", "1.1"
 * when using 1.1, you MUST set the mandatory headers "Host"
 * @return boolean false if the version number is bad, true if ok
 **/
    function setProtocolVersion($version) {
      $this->protocolVersion = $version;
    }

    function put($uri, $filecontent) {
      if (!isset($this->driver)) {
        $this->setDriver();
      }

      $uri = $this->makeUri($uri);

      $this->params['server'] = $this->url;
      $this->params['method'] = 'put';
      $this->params['header'] = array();
      $this->params['version'] = $this->protocolVersion;
      $this->params['parameters'] = $filecontent;

      if (isset($this->proxyHost)) {
        $this->params['proxy'] = $this->proxyHost . (!empty($this->proxyPort) ? ':' . $this->proxyPort : '');
      }

      if (isset($this->requestHeaders) && is_array($this->requestHeaders)) {
        foreach ($this->requestHeaders as $k => $v) {
          $this->params['header'][] = $k . ': ' . $v;
        }
      }

      $this->responseHeaders = $this->responseBody = '';

      $this->response = $this->driver->execute($this->params);

      if (is_array($this->response) && isset($this->response['code'])) {
        $this->processReply();
      }

      return $this->reply;
    }

    function addParameter($key, $value) {
      $this->params[$key] = $value;
    }

    function savePublicCertificate($server = null, $port = null) {
      if (extension_loaded('openssl') && is_writable(DIR_FS_CACHE)) {
        if (!isset($server)) {
          $server = $this->url['host'];
        }

        if (!isset($port)) {
          $port = $this->url['port'];
        }

        $public_cert = null;

        $context = stream_context_create(array('ssl' => array('capture_peer_cert' => true)));

        $socket = stream_socket_client('ssl://' . $server . ':' . $port, $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $context);

        $options = stream_context_get_options($socket);

        openssl_x509_export($options['ssl']['peer_certificate'], $public_cert);

        if (!empty($public_cert)) {
          return file_put_contents(DIR_FS_CACHE . $server . '.crt', $public_cert);
        }
      }

      return false;
    }

// Deprecated
    function sendCommand($command) {
      trigger_error('httpClient::sendCommand is deprecated.');
    }

// Deprecated
    function disconnect() { }
  }
