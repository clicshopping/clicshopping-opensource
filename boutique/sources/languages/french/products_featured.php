<?php
/**
 * featured_products.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Produits Sélectionnés');
define('HEADING_TITLE', 'Produits Sélectionnés');

define('TEXT_DATE_ADDED', 'En vente depuis le :');
define('TEXT_MANUFACTURER', 'Marque :');
define('TEXT_PRICE', 'Prix :');
define('TEXT_PRODUCT_OPTIONS','Options possibles ');
define('MIN_QTY_ORDER_PRODUCT','Qté min : ');
define('TEXT_NO_PRODUCTS','Aucun Produits Sélectionnés en ce moment');
?>