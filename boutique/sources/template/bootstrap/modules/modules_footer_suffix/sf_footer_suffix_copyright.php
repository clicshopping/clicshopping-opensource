<?php
/**
 * sf_footer_suffix_copyright.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  class sf_footer_suffix_copyright {
    var $code;
    var $group;
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function sf_footer_suffix_copyright() {
      $this->code = get_class($this);
      $this->group = basename(__DIR__);
      $this->title = MODULES_FOOTER_SUFFIX_COPYRIGHT_TITLE;
      $this->description = MODULES_FOOTER_SUFFIX_COPYRIGHT_DESCRIPTION;

      if ( defined('MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS') ) {
        $this->sort_order = MODULES_FOOTER_SUFFIX_COPYRIGHT_SORT_ORDER;
        $this->enabled = (MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS == 'True');
      }
    }

    function execute() {
      global $OSCOM_Template;

      $logo = '<img width="24" height="24" alt="www.imaginis.com" src="images/clicshopping.gif">';

      $footer_copyright = '<!-- footer copyright start -->' . "\n";

      ob_start();
      require($OSCOM_Template->getTemplateModules($this->group . '/content/suffix_footer_copyright'));

      $footer_copyright .= ob_get_clean();

      $footer_copyright .= '<!-- footer copyright end -->' . "\n";

      $OSCOM_Template->addBlock($footer_copyright, $this->group);
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS');
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Souhaitez-vous activer ce module ?', 'MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS', 'True', 'Souhaitez vous activer ce module &agrave; votre boutique ?', '6', '1', 'osc_cfg_select_option(array(\'True\', \'False\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ordre de tri d\'affichage', 'MODULES_FOOTER_SUFFIX_COPYRIGHT_SORT_ORDER', '100', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier).', '6', '3', now())");
      osc_db_query("update configuration set configuration_value = '1' where configuration_key = 'WEBSITE_MODULE_INSTALLED'");

    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULES_FOOTER_SUFFIX_COPYRIGHT_STATUS',
                   'MODULES_FOOTER_SUFFIX_COPYRIGHT_SORT_ORDER'
                  );
    }
  }
