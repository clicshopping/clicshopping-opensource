<?php
/*
   * modules_hooks.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/
  require('includes/application_top.php');
  $directory = DIR_FS_CATALOG . 'includes/modules/hooks/';
  require('includes/header.php');
?>

  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/products_unit.png', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

  <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
      <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">

<?php
      if ( $dir = @dir($directory) ) {
        while ( $file = $dir->read() ) {
          if ( is_dir($directory . '/' . $file) && !in_array($file, array('.', '..')) ) {
?>
            <tr class="dataTableHeadingRow">
              <td class="dataTableHeadingContent" colspan="2"><?php echo $file; ?></td>
            </tr>

<?php
            if ( $dir2 = @dir($directory . '/' . $file) ) {
              while ( $file2 = $dir2->read() ) {
                if ( is_dir($directory . '/' . $file . '/' . $file2) && !in_array($file2, array('.', '..')) ) {
                  if ( $dir3 = @dir($directory . '/' . $file . '/' . $file2) ) {
                    while ( $file3 = $dir3->read() ) {
                      if ( !is_dir($directory . '/' . $file . '/' . $file2 . '/' . $file3) ) {
                        if ( substr($file3, strrpos($file3, '.')) == '.php' ) {
                          $code = substr($file3, 0, strrpos($file3, '.'));
                          $class = 'hook_' . $file . '_' . $file2 . '_' . $code;
                          if ( !class_exists($class) ) {
                            include($directory . '/' . $file . '/' . $file2 . '/' . $file3);
                          }
                          $obj = new $class();
                          foreach ( get_class_methods($obj) as $method ) {
                            if ( substr($method, 0, 7) == 'listen_' ) {
?>

                              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                                <td class="dataTableContent"><?php echo $file2 . '/' . $file3; ?></td>
                                <td class="dataTableContent"><?php echo substr($method, 7); ?></td>
                              </tr>

<?php
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
?>
            </table></td>
         </tr>
      </table></td>
    </tr>
    <tr>
      <td><p class="smallText"><?php echo TEXT_HOOKS_DIRECTORY . ' ' . DIR_FS_CATALOG . 'includes/modules/hooks/'; ?></p></td>
    </tr>
  </table>
<?php
  require('includes/footer.php');
  require('includes/application_bottom.php');
?>