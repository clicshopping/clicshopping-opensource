<?php
  /*
   * xml_rpc_admin_suppliers.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

// **********************************
// search iso code ClicShopping
// **********************************

  $QcountryIdCustomer = $OSCOM_PDO->prepare("select suppliers_country_id
                                             from :table_suppliers
                                             where suppliers_id = :suppliers_id
                                            ");
  $QcountryIdCustomer->bindInt(':suppliers_id', (int)$suppliers_id);
  $QcountryIdCustomer->execute();

  $country_id_supplier = $QcountryIdCustomer->fetch();

  $country_id_supplier = $country_id_supplier['suppliers_country_id'];


  $QcountryCode = $OSCOM_PDO->prepare("select countries_iso_code_2
                                       from :table_countries
                                       where countries_id = :countries_id
                                      ");
  $QcountryCode->bindInt(':countries_id',(int)$country_id_supplier);
  $QcountryCode->execute();

  $country_code = $QcountryCode->fetch();
  $country_code = $country_code['countries_iso_code_2'];


// **********************************
// search id country odoo
// **********************************
  $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
  $field_list = array('country_id',
                      'name',
                    );

  $Qcountry_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
  $country_id_odoo = $Qcountry_id_odoo[0][id];


// **********************************
// Search odoo customer id
// **********************************
  $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore Suppliers - ' . $suppliers_id, 'res.partner');

// **********************************
// read id customer odoo
// **********************************
  $field_list = array('ref',
                      'id'
                    );

  $id_odoo_suppplier_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
  $id_odoo_supplier = $id_odoo_suppplier_array[0][id];

  if  (empty($id_odoo_supplier)) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
    $values = array(
                    "ref" => new xmlrpcval('WebStore Suppliers - ' . $suppliers_id, "string"),
                    "name"    => new xmlrpcval($suppliers_name . ' ' . $suppliers_manager, "string"),
                    "email"  => new xmlrpcval($suppliers_email_address, "string"),
                    'phone' => new xmlrpcval($suppliers_phone,"string"),
                    'fax' => new xmlrpcval($suppliers_fax,"string"),
                    'street' => new xmlrpcval($suppliers_address,"string"),
                    'street2'=>new xmlrpcval($suppliers_suburb,"string"),
                    'zip' => new xmlrpcval($suppliers_postcode,"string"),
                    'city' => new xmlrpcval($suppliers_city,"string"),
                    'country_id' => new xmlrpcval($country_id_odoo,"int"),
                    "supplier" => new xmlrpcval(1,"int"),
                    "tz"      => new xmlrpcval("Europe/Paris", "string"),
		    'supplier' => new xmlrpcval(1, "int"),
                    "clicshopping_suppliers_id" => new xmlrpcval($suppliers_id, "int"),
                    "clicshopping_suppliers_notes" => new xmlrpcval($suppliers_notes, "string"),
                   );

    $OSCOM_ODOO->createOdoo($values, "res.partner");

  } else {

// **********************************
// update Customer if exist
// **********************************

    $values = array(
                    "ref" => new xmlrpcval('WebStore Suppliers - ' . $suppliers_id, "string"),
                    "name"    => new xmlrpcval($suppliers_name .' ' . $suppliers_manager, "string"),
                    "email"  => new xmlrpcval($suppliers_email_address, "string"),
                    'phone' => new xmlrpcval($suppliers_phone,"string"),
                    'fax' => new xmlrpcval($suppliers_fax,"string"),
                    'street' => new xmlrpcval($suppliers_address,"string"),
                    'street2'=>new xmlrpcval($suppliers_suburb,"string"),
                    'zip' => new xmlrpcval($suppliers_postcode,"string"),
                    'city' => new xmlrpcval($suppliers_city,"string"),
                    'country_id' => new xmlrpcval($country_id_odoo,"int"),
                    "clicshopping_suppliers_id" => new xmlrpcval($suppliers_id, "int"),
                    "clicshopping_suppliers_notes" => new xmlrpcval($suppliers_notes, "string"),
                   );

    $OSCOM_ODOO->updateOdoo($id_odoo_supplier, $values, 'res.partner');
  }
?>