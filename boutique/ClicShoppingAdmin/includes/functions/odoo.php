<?php
/**
 * odoo.php
 * @copyright Copyright 2008 - ClicShopping http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/



  function osc_reset_config_odoo() {
    global $OSCOM_PDO;
/*
    $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value
                                        where configuration_key = :configuration_key
                                      ');
    $Qupdate->bindValue(':configuration_value', 'false');
    $Qupdate->bindValue(':configuration_key', 'ODOO_ACTIVATE_WEB_SERVICE');
    $Qupdate->execute();



    $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                      set configuration_value = :configuration_value
                                      where configuration_key = :configuration_key
                                    ');
    $Qupdate->bindValue(':configuration_value', 'false');
    $Qupdate->bindValue(':configuration_key', 'ODOO_RESET_WEBSERVICE');
    $Qupdate->execute();
*/
  }


/**
 * Export all products inside odoo
 *
 * @param string
 * @return string
 * @access public
 */

  function osc_bulk_product() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_EXPORT_PRODUCTS == 'export' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {
      set_time_limit(0);
      ini_set('memory_limit', '64M');

      $Qproducts = $OSCOM_PDO->prepare('select  p.*,
                                               pd.*
                                       from :table_products p,
                                            :table_products_description pd
                                       where p.products_id = pd.products_id
                                       and pd.products_id = p.products_id
                                       and pd.language_id = :language_id
                                      ');
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qproducts->execute();


      $date = date("Y-m-d H:i:s");
// search company
      $ids = $OSCOM_ODOO->odooSearch('name', '=', ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE, 'res.company');
      $field_list = array('id');

      $company_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.company');
      $company_id = $company_id[0][id];

// customers group
      $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id
                                             from :table_customers_groups
                                             where customers_group_id >  0
                                            ');
      $QcustomersGroup->execute();

      while($product = $Qproducts->fetch() ) {

        if (!empty($product['products_id']) && !empty($product['products_model'])) {

          $products_description = osc_strip_html_tags($product['products_description']);
          $products_id_clicshopping = $product['products_id'];

          if ($product['products_packaging'] == 1) {
            $product_packaging = 'New product';
          } elseif ($product['products_packaging'] == 2) {
            $product_packaging = 'Product repackaged';
          } else {
            $product_packaging = 'Product used';
          }

// products_image export
          if (file_exists(DIR_FS_CATALOG_IMAGES . $product['products_image'])) {
            $products_image = DIR_FS_CATALOG_IMAGES . $product['products_image'];
            $data_small_image = file_get_contents($products_image);
            $products_image_odoo = base64_encode($data_small_image);
          }

          if (file_exists(DIR_FS_CATALOG_IMAGES . $product['products_image_zoom'])) {
            $products_image_zoom = DIR_FS_CATALOG_IMAGES . $product['products_image_zoom'];
            $data_zoom_image = file_get_contents($products_image_zoom);
            $products_image_zoom_odoo = base64_encode($data_zoom_image);
          }

          $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping, 'product.template', 'int');

          $field_list = array('id');

          $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
          $products_id_odoo = $Qproducts_id[0][id];

// **************************************************************************
// Search odoo clicshopping_catgories_id
// **************************************************************************

          $Qcategories_products = $OSCOM_PDO->prepare('select products_id,
                                                               categories_id
                                                        from :table_products_to_categories
                                                        where products_id = :products_id
                                                       ');
          $Qcategories_products->bindInt(':products_id', (int)$products_id_clicshopping);

          $Qcategories_products->execute();

          $categories_products = $Qcategories_products->fetch();
          $categories_products_id = $categories_products['categories_id'];

          $ids = $OSCOM_ODOO->odooSearch('clicshopping_categories_id', '=', $categories_products_id, 'product.category', 'int');

// **********************************
// read id clicshopping_categories_id odoo
// **********************************

          $field_list = array('id');

          $Qcategories_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.category');
          $categories_id_odoo = $Qcategories_id_odoo[0][id];

          if (empty($categories_id_odoo)) {
            $categories_id_odoo = 1;
          }

// **************************************************************************
// Search odoo products tax
// **************************************************************************

          $QProductsCodeTax = $OSCOM_PDO->prepare('select distinct tax_class_id,
                                                                     code_tax_odoo
                                                         from :table_tax_rates
                                                         where tax_class_id = :tax_class_id
                                                       ');

          $QProductsCodeTax->bindValue(':tax_class_id', $product['products_tax_class_id']);

          $QProductsCodeTax->execute();

          $Qproducts_code_tax = $QProductsCodeTax->fetch();
          $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];

//******************************************
// research id tax by description
//***************************************

          if ($products_code_tax != null) {
            $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
            $odoo_products_tax_id = $ids;
            $odoo_products_tax_id = $odoo_products_tax_id[0];

            if (!empty($odoo_products_tax_id)) {

              $type_tax_string = 'array';

              $tax = array(new xmlrpcval(
                array(
                  new xmlrpcval(6, "int"),// 6 : id link
                  new xmlrpcval(0, "int"),
                  new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                ), "array"
              )
              );
            } else {
              $tax = 0;
              $type_tax_string = 'int';
            }

          } else {
            $tax = 0;
            $type_tax_string = 'int';
          }


// **************************************************************************************************
// Manufacturer
// **************************************************************************************************


          if (!empty($product['manufacturers_id'])) {
// **************************
// Search odoo manufacturers id in clicshopping.manufacturer
// ***************************
            $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $product['manufacturers_id'], 'clicshopping.manufacturer', 'int');
            $field_list = array('id');
            $Qmanufacturers_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
            $manufacturer_id_odoo = $Qmanufacturers_id_odoo[0][id];
          }

          if (empty($products_id_odoo)) {

// **********************************
// Create products if doesn't exist in odoo
// **********************************

            $values = array("default_code" => new xmlrpcval($product['products_model'], "string"),
                            "name" => new xmlrpcval($product['products_name'], "string"),
                            "categ_id" => new xmlrpcval($categories_id_odoo, "int"),
                            "description" => new xmlrpcval($products_description, "string"),
                            "description_purchase" => new xmlrpcval($products_description, "string"),
                            "description_sale" => new xmlrpcval($product['products_name'], "string"),
                            "list_price" => new xmlrpcval($product['products_price'], "double"),
                            "standard_price" => new xmlrpcval($product['products_cost'], "double"),
                            "image" => new xmlrpcval($products_image_zoom_odoo, "string"),
                            "image_medium" => new xmlrpcval($products_image_zoom_odoo, "string"),
                            "image_small" => new xmlrpcval($products_image_odoo, "string"),
                            "ean13" => new xmlrpcval($product['products_ean'], "string"),
                            "loc_case" => new xmlrpcval($product['products_wharehouse_level_location'], "string"),
                            "loc_rack" => new xmlrpcval($product['products_wharehouse'], "string"),
                            "loc_row" => new xmlrpcval($product['products_wharehouse_row'], "string"),
                            "weight" => new xmlrpcval($product['products_weight'], "double"),
                            "website_meta_title" => new xmlrpcval($product['products_head_title_tag'], "string"),
                            "website_meta_description" => new xmlrpcval($product['products_head_desc_tag'], "string"),
                            "website_meta_keywords" => new xmlrpcval($product['products_head_keywords_tag'], "string"),
                            "taxes_id" => new xmlrpcval($tax, $type_tax_string),
                            "type" => new xmlrpcval($product['products_type'], "string"),
                            "clicshopping_products_id" => new xmlrpcval($product['products_id'], "int"),
                            "clicshopping_products_sku" => new xmlrpcval($product['products_sku'], "string"),
                            "clicshopping_products_weight_pounds" => new xmlrpcval($product['products_weight_pounds'], "double"),
                            "clicshopping_products_dimension_width" => new xmlrpcval($product['products_dimension_width'], "double"),
                            "clicshopping_products_dimension_height" => new xmlrpcval($product['products_dimension_height'], "double"),
                            "clicshopping_products_dimension_depth" => new xmlrpcval($product['products_dimension_depth'], "double"),
                            "clicshopping_products_dimension_type" => new xmlrpcval($product['products_dimension_type'], "string"),
                            "clicshopping_products_wharehouse_time_replenishment" => new xmlrpcval($product['products_wharehouse_time_replenishment'], "string"),
                            "clicshopping_products_description" => new xmlrpcval($product['products_description'], "string"),
                            "clicshopping_products_price_comparison" => new xmlrpcval($product['products_price_comparison'], "double"),
                            "clicshopping_products_only_online" => new xmlrpcval($product['products_only_online'], "double"),
                            "clicshopping_products_only_shop" => new xmlrpcval($product['products_only_shop'], "double"),
                            "clicshopping_products_stock_status" => new xmlrpcval($product['products_status'], "double"),
                            "clicshopping_products_quantity_alert" => new xmlrpcval($product['products_quantity_alert'], "double"),
                            "clicshopping_products_min_qty_order" => new xmlrpcval($product['products_min_qty_order'], "double"),
                            "clicshopping_products_price_kilo" => new xmlrpcval($product['products_price_kilo'], "double"),
                            "clicshopping_products_view" => new xmlrpcval($product['products_view'], "double"),
                            "clicshopping_products_handling" => new xmlrpcval($product['products_handling'], "double"),
                            "clicshopping_products_orders_view" => new xmlrpcval($product['orders_view'], "double"),
                            "clicshopping_admin_user_name" => new xmlrpcval($product['admin_user_name'], "string"),
                            "clicshopping_products_date_available" => new xmlrpcval($product['products_date_available'], "string"),
                            "clicshopping_products_packaging" => new xmlrpcval($product_packaging, "string"),
                            "clicshopping_products_percentage" => new xmlrpcval($product['products_percentage'], "double"),
                            "clicshopping_product_manufacturer_id" => new xmlrpcval($manufacturer_id_odoo, "int"),
                          );

            $OSCOM_ODOO->createOdoo($values, "product.template");

          } else {

// **********************************
// update products if exist
// **********************************

            $id_list = array();
            $id_list[] = new xmlrpcval($products_id_odoo, 'int');

            $values = array("default_code" => new xmlrpcval($product['products_model'], "string"),
                            "name" => new xmlrpcval($product['products_name'], "string"),
                            "categ_id" => new xmlrpcval($categories_id_odoo, "int"),
                            "description" => new xmlrpcval($products_description, "string"),
                            "description_purchase" => new xmlrpcval($products_description, "string"),
                            "description_sale" => new xmlrpcval($product['products_name'], "string"),
                            "list_price" => new xmlrpcval($product['products_price'], "double"),
                            "standard_price" => new xmlrpcval($product['products_cost'], "double"),
                            "image" => new xmlrpcval($products_image_odoo, "string"),
                            "image_medium" => new xmlrpcval($products_image_zoom_odoo, "string"),
                            "image_small" => new xmlrpcval($products_image_odoo, "string"),
                            "ean13" => new xmlrpcval($product['products_ean'], "string"),
                            "loc_case" => new xmlrpcval($product['products_wharehouse_level_location'], "string"),
                            "loc_rack" => new xmlrpcval($product['products_wharehouse'], "string"),
                            "loc_row" => new xmlrpcval($product['products_wharehouse_row'], "string"),
                            "weight" => new xmlrpcval($product['products_weight'], "double"),
                            "website_meta_title" => new xmlrpcval($product['products_head_title_tag'], "string"),
                            "website_meta_description" => new xmlrpcval($product['products_head_desc_tag'], "string"),
                            "website_meta_keywords" => new xmlrpcval($product['products_head_keywords_tag'], "string"),
                            "taxes_id" => new xmlrpcval($tax, $type_tax_string),
                            "type" => new xmlrpcval($product['products_type'], "string"),
                            "clicshopping_products_id" => new xmlrpcval($product['products_id'], "int"),
                            "clicshopping_products_sku" => new xmlrpcval($product['products_sku'], "string"),
                            "clicshopping_products_weight_pounds" => new xmlrpcval($product['products_weight_pounds'], "double"),
                            "clicshopping_products_dimension_width" => new xmlrpcval($product['products_dimension_width'], "double"),
                            "clicshopping_products_dimension_height" => new xmlrpcval($product['products_dimension_height'], "double"),
                            "clicshopping_products_dimension_depth" => new xmlrpcval($product['products_dimension_depth'], "double"),
                            "clicshopping_products_dimension_type" => new xmlrpcval($product['products_dimension_type'], "string"),
                            "clicshopping_products_wharehouse_time_replenishment" => new xmlrpcval($product['products_wharehouse_time_replenishment'], "string"),
                            "clicshopping_products_description" => new xmlrpcval($product['products_description'], "string"),
                            "clicshopping_products_price_comparison" => new xmlrpcval($product['products_price_comparison'], "double"),
                            "clicshopping_products_only_online" => new xmlrpcval($product['products_only_online'], "double"),
                            "clicshopping_products_only_shop" => new xmlrpcval($product['products_only_shop'], "double"),
                            "clicshopping_products_stock_status" => new xmlrpcval($product['products_status'], "double"),
                            "clicshopping_products_quantity_alert" => new xmlrpcval($product['products_quantity_alert'], "double"),
                            "clicshopping_products_min_qty_order" => new xmlrpcval($product['products_min_qty_order'], "double"),
                            "clicshopping_products_price_kilo" => new xmlrpcval($product['products_price_kilo'], "double"),
                            "clicshopping_products_view" => new xmlrpcval($product['products_view'], "double"),
                            "clicshopping_products_handling" => new xmlrpcval($product['products_handling'], "double"),
                            "clicshopping_products_orders_view" => new xmlrpcval($product['orders_view'], "double"),
                            "clicshopping_admin_user_name" => new xmlrpcval($product['admin_user_name'], "string"),
                            "clicshopping_products_date_available" => new xmlrpcval($product['products_date_available'], "string"),
                            "clicshopping_products_packaging" => new xmlrpcval($product_packaging, "string"),
                            "clicshopping_products_percentage" => new xmlrpcval($product['products_percentage'], "double"),
                            "clicshopping_product_manufacturer_id" => new xmlrpcval($manufacturer_id_odoo, "int"),
                          );

            $OSCOM_ODOO->updateOdoo($products_id_odoo, $values, 'product.template');
          }


// **************************************************************************************************
// Supplier
// **************************************************************************************************

          if (!empty($product['suppliers_id'])) {

// **************************
// Search odoo suppliers id in res.partner
// ***************************

            $ids = $OSCOM_ODOO->odooSearch('clicshopping_suppliers_id', '=', $product['suppliers_id'], 'res.partner');

// **********************************
// read id suppliers_id odoo
// **********************************

            $field_list = array('id',
                                'name',
                              );

            $Qsuppliers_id_name_res_partner_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
            $suppliers_id_name_res_partner_odoo = $Qsuppliers_id_name_res_partner_odoo[0][id];

// search id of product
            $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping, 'product.template', 'int');
            $field_list = array('id');

            $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
            $products_id_odoo = $Qproducts_id[0][id];

            if (!empty($suppliers_id_name_res_partner_odoo)) {

// **********************************
// Search odoo suppliers Info id
// **********************************

              $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('product_code', '=', $product['products_model'], 'product.supplierinfo', 'string',
                                                          'product_tmpl_id', '=', $products_id_odoo, 'int');

// **********************************
// read id suppliers Info  Id odoo
// **********************************

              $field_list = array('id',
                                  'name',
                                );

              $Qsuppliers_id_name_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.supplierinfo');
              $suppliers_id_name_odoo = $Qsuppliers_id_name_odoo[0][id];

              if (empty($suppliers_id_name_odoo)) {

                $values = array("name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                                "product_name" => new xmlrpcval($product['products_name'], "string"),
                                "product_code" => new xmlrpcval($product['products_model'], "string"),
                                "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                              );

                $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

              } else {

                $id_list = array();
                $id_list[] = new xmlrpcval($suppliers_id_name_odoo, 'int');

                $values = array("name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                                "product_name" => new xmlrpcval($product['products_name'], "string"),
                                "product_code" => new xmlrpcval($product['products_model'], "string"),
                                "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                              );

                $OSCOM_ODOO->updateOdoo($suppliers_id_name_odoo, $values, 'product.supplierinfo');
              }

            } else {

              $values = array("name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                              "product_name" => new xmlrpcval($product['products_name'], "string"),
                              "product_code" => new xmlrpcval($product['products_model'], "string"),
                              "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                            );

              $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");
            }
          }

// **************************************************************************************************
// Stock
// **************************************************************************************************

          if (!empty($product['products_quantity'])) {


// product name logistic
            $products_name_odoo = '[' . $product['products_model'] . '] ' . $product['products_name'];


// stock warehouse search id and code concerning ClicShopping Wharehouse
            $ids = $OSCOM_ODOO->odooSearch('name', '=', 'ClicShopping', 'stock.warehouse');

            $field_list = array('id',
                                'code',
                                'name',
                              );

            $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');
            $stock_wharehouse_id = $Qstock_wharehouse[0][id];
            $stock_wharehouse_code = $Qstock_wharehouse[0][code];

// search location name and id in stock location
            $ids = $OSCOM_ODOO->odooSearch('name', '=', $stock_wharehouse_code, 'stock.location');

            $field_list = array('id',
                                'location_id',
                                'name',
                              );

            $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');
            $stock_location_id = $Qstock_location[0][id];
            $stock_location_name = $Qstock_location[0][name];


// **********************************
// Search odoo products id & stock available
// **********************************


            $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping, 'product.template', 'int');
            $field_list = array('id');

            $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
            $products_id_odoo = $Qproducts_id[0][id];

// **********************************
// read id products odoo
// **********************************

            $field_list = array('qty_available');

            $Qproducts_stock_available = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
            $products_stock_available = $Qproducts_stock_available[0][qty_available];

            if (!empty($stock_location_id)) {

              if ($product['products_quantity'] != $products_stock_available) {

// search qty in stock.quant
                $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_name_odoo, 'stock.quant', 'string',
                                                               'location_id', 'like', $stock_location_name . '%', 'string');

                $field_list = array('qty');

                $QSearchStock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.quant');

                $stock_odoo_qty_total = '0';

// Stock calcul inside wharehouse
                foreach ($QSearchStock as $key) {
                  $products_stock_qty = $key[qty];
                  $stock_odoo_qty_total = $stock_odoo_qty_total + $products_stock_qty;
                }

// difference between ClicShopping and Odoo
                $new_stock = $product['products_quantity'] - $stock_odoo_qty_total;

// hack, don't find the good value in location.stock. Add +1 on id because it's create just after
                $stock_location_id = $stock_location_id + 1;

// move stock

                if ($new_stock > 0) {
                  $values = array(
                                  "product_id" => new xmlrpcval($products_id_odoo, "int"),
                                  "product_uom_qty" => new xmlrpcval($new_stock, "double"), // qty of my product
                                  "name" => new xmlrpcval($products_name_odoo, "string"),
                                  "invoice_state" => new xmlrpcval('none', "string"),
                                  "date" => new xmlrpcval($date, "string"),
                                  "date_expected" => new xmlrpcval($date, "string"),
                                  "company_id" => new xmlrpcval($company_id, "int"),
                                  "procure_method" => new xmlrpcval('make_to_stock', "string"),
                                  "location_dest_id" => new xmlrpcval($stock_location_id, "string"),
                                  "location_id" => new xmlrpcval(5, "int"), // Virtual Locations/Inventory loss
                                  "product_uom" => new xmlrpcval(1, "int"),
                                  "origin" => new xmlrpcval("ClicShopping Webstore", "string"),
                                );

                } else {

// Adapted for odoo on the calcul
                  $new_stock = $new_stock * (-1);

                  $values = array(
                                  "product_id" => new xmlrpcval($products_id_odoo, "int"),
                                  "product_uom_qty" => new xmlrpcval($new_stock, "double"), // qty of my product
                                  "name" => new xmlrpcval($products_name_odoo, "string"),
                                  "invoice_state" => new xmlrpcval('none', "string"),
                                  "date" => new xmlrpcval($date, "string"),
                                  "date_expected" => new xmlrpcval($date, "string"),
                                  "company_id" => new xmlrpcval($company_id, "int"),
                                  "procure_method" => new xmlrpcval('make_to_stock', "string"),
                                  "location_dest_id" => new xmlrpcval(5, "string"),
                                  "location_id" => new xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                                  "product_uom" => new xmlrpcval(1, "int"),
                                  "origin" => new xmlrpcval("ClicShopping Webstore", "string"),
                                 );
                }

                $OSCOM_ODOO->createOdoo($values, "stock.move");

// search id for stock.move concerning the product
                $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_id_odoo, 'stock.move', 'int',
                                                              'date', '=', $date, 'string');

                $field_list = array('id');

                $Qodoo_move_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
                $odoo_picking_id = $Qodoo_move_read[0][id];

                $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);
              }
            } // end !empty($stock_location_id)
          } // !empty($product['products_quantity'])

//*******************************************************************************************
//             Stock minimum
//*******************************************************************************************
          $ids = $OSCOM_ODOO->odooSearch('product_id', '=', $products_id_odoo, 'stock.warehouse.orderpoint', 'int' );

// **********************************
// read id stock minimum in odoo
// **********************************

          $field_list = array ('id');

          $Qodoo_min_stock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse.orderpoint');
          $odoo_min_stock_id = $Qodoo_min_stock[0][id];

          if ($product['products_quantity_alert'] == 0) {
            $stock_alert = STOCK_REORDER_LEVEL;
          } else {
            $stock_alert = $product['products_quantity_alert'];
          }

          if (!empty($odoo_min_stock_id)) {

            $id_list = array();
            $id_list[]= new xmlrpcval($odoo_min_stock_id, 'int');

            $values = array("product_min_qty" => new xmlrpcval($stock_alert, "double") );

            $OSCOM_ODOO->updateOdoo($odoo_min_stock_id, $values, 'stock.warehouse.orderpoint');

          } else {

            $stock_location_id = $stock_location_id +1;

            $values = array(
                              "name" => new xmlrpcval($product['products_model'] . ' / ' . $product['products_id'], "string"), // qty of my product
                              "product_id" => new xmlrpcval($products_id_odoo, "int"), //
                              "warehouse_id" => new xmlrpcval($stock_wharehouse_id, "int"),
                              "location_id" => new xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                              "product_min_qty" => new xmlrpcval($stock_alert, "double"),
                              "product_max_qty" => new xmlrpcval(0, "double"),
                              "qty_multiple" => new xmlrpcval(1, "double"),
                              "company_id" => new xmlrpcval($company_id, "int"),
                              "active" => new xmlrpcval(1, "double"),
                            );

            $OSCOM_ODOO->createOdoo($values, "stock.warehouse.orderpoint");
          }
        } // end while

        $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value
                                        where configuration_key = :configuration_key
                                      ');
        $Qupdate->bindValue(':configuration_value', 'false');
        $Qupdate->bindValue(':configuration_key', 'ODOO_EXPORT_PRODUCTS');
        $Qupdate->execute();
      }

//*****************************************************
//          Update Products groups
// ***************************************************


// Gets all of the customers groups
      while($customers_group = $QcustomersGroup->fetch() ) {

        $QproductsGroup = $OSCOM_PDO->prepare('select products_id,
                                                     customers_group_id,
                                                     customers_group_price,
                                                     price_group_view,
                                                     products_group_view,
                                                     orders_group_view,
                                                     products_quantity_unit_id_group,
                                                     products_model_group,
                                                     products_quantity_fixed_group
                                             from :table_products_groups
                                             where products_id = :products_id
                                             and customers_group_id = :customers_group_id
                                        ');
        $QproductsGroup->bindInt(':products_id', (int)$products_id_clicshopping);
        $QproductsGroup->bindInt(':customers_group_id', (int)$customers_group['customers_group_id']);

        $QproductsGroup->execute();

        $ids = $OSCOM_ODOO->odooSearch('clicshopping_products_id', '=', $products_id_clicshopping, 'product.template', 'int');
        $field_list = array('id');

        $Qproducts_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
        $products_id_odoo = $Qproducts_id[0][id];

        $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('clicshopping_customers_group_id', '=', $customers_group['customers_group_id'], 'clicshopping.products.group', 'int',
                                                    'clicshopping_products_id', '=', $products_id_odoo, 'int');

// **********************************
// Search odoo groups id
// **********************************

        $field_list = array('id');

        $QodooCustomersGroupId = $OSCOM_ODOO->readOdoo($ids, $field_list, "clicshopping.products.group");
        $odoo_customers_group_id = $QodooCustomersGroupId[0][id];

        if (!empty($odoo_customers_group_id)) {

          $values = array("clicshopping_customers_group_id" => new xmlrpcval($customers_group['customers_group_id'], "int"),
                           "clicshopping_products_id" => new xmlrpcval($products_id_clicshopping, "int"),
                            "clicshopping_customers_group_price" => new xmlrpcval($customers_group['customers_group_price'], "double"),
                            "clicshopping_product_group_view" => new xmlrpcval($customers_group['products_group_view'], "int"),
                            "clicshopping_orders_group_view" => new xmlrpcval($customers_group['orders_group_view'], "int"),
                            "clicshopping_price_group_view" => new xmlrpcval($customers_group['price_group_view'], "double"),
                            "clicshopping_products_model_group" => new xmlrpcval($customers_group['products_model_group'], "string"),
                            "clicshopping_products_quantity_unit_id_group" => new xmlrpcval($customers_group['products_quantity_unit_id_group'], "int"),
                            "clicshopping_products_quantity_fixed_group" => new xmlrpcval($customers_group['products_quantity_fixed_group'], "int"),
                          );

          $OSCOM_ODOO->updateOdoo($odoo_customers_group_id, $values, "clicshopping.products.group");
        }
      }
    }
  }



  /**
   * Export all customers inside odoo
   *
   * @param string
   * @return string
   * @access public
   */

  function osc_bulk_customer() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_EXPORT_CUSTOMERS == 'export' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {
      set_time_limit(0);
      ini_set ( 'memory_limit', '64M' );

      $Qcustomers = $OSCOM_PDO->prepare('select customers_id,
                                                customers_company,
                                                customers_siret,
                                                customers_tva_intracom_code_iso,
                                                customers_tva_intracom,
                                                customers_lastname,
                                                customers_firstname,
                                                customers_group_id,
                                                customers_email_address,
                                                customers_telephone,
                                                customers_cellular_phone,
                                                customers_fax,
                                                customers_default_address_id,
                                                customers_newsletter
                                          from :table_customers
                                        ');
      $Qcustomers->execute();

      while($customer = $Qcustomers->fetch() ) {

        $customers_id = $customer['customers_id'];
        $customers_default_address_id = $customer['customers_default_address_id'];
        $customers_lastname = $customer['customers_lastname'];
        $customers_firstname = $customer['customers_firstname'];
        $customers_email_address = $customer['customers_email_address'];
        $customers_telephone = $customer['customers_telephone'];
        $customers_cellular_phone = $customer['customers_cellular_phone'];
        $customers_fax = $customer['customers_fax'];
        $customers_group_id = $customer['customers_group_id'];
        $customers_newsletter= $customer['customers_newsletter'];

        if ($customers_newsletter == 1) $customers_newsletter = 0;

// select address
        $QcountryCustomer = $OSCOM_PDO->prepare("select entry_country_id,
                                                        entry_street_address,
                                                        entry_suburb,
                                                        entry_postcode,
                                                        entry_city,
                                                        entry_state
                                                     from :table_address_book
                                                     where customers_id = :customers_id
                                                     and address_book_id = :address_book_id
                                                    ");
        $QcountryCustomer->bindInt(':customers_id', (int)$customers_id);
        $QcountryCustomer->bindInt(':address_book_id', (int)$customers_default_address_id);
        $QcountryCustomer->execute();

        $Qcountry_customer = $QcountryCustomer->fetch();

        $country_id_customer = $Qcountry_customer['entry_country_id'];
        $entry_street_address = $Qcountry_customer['entry_street_address'];
        $entry_suburb = $Qcountry_customer['entry_suburb'];
        $entry_postcode = $Qcountry_customer['entry_postcode'];
        $entry_city = $Qcountry_customer['entry_city'];

// select country code
        $QcountryCode = $OSCOM_PDO->prepare('select countries_iso_code_2
                                             from :table_countries
                                             where countries_id = :countries_id
                                            ');
        $QcountryCode->bindInt(':countries_id', (int)$country_id_customer);
        $QcountryCode->execute();

        $Qcountry_code = $QcountryCode->fetch();
        $country_code = $Qcountry_code['countries_iso_code_2'];


// **********************************
// search id country odoo
// **********************************
        $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
        $field_list = array('country_id',
                            'name'
                          );
        $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
        $country_id_odoo = $country_id_odoo[0][id];

// **********************************
// Search odoo customer id
// **********************************
        $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $customers_id, 'res.partner');

// **********************************
// read id customer odoo
// **********************************
        $field_list = array('ref',
                            'id'
                          );

        $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
        $id_odoo_customer = $id_odoo_customer_array[0][id];


// **********************************
// Search odoo customer group id
// **********************************
        $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_group_id', '=', $customers_group_id, 'clicshopping.customers.group', 'int');

// **********************************
// read id customer odoo
// **********************************
        $field_list = array('id');

        $id_odoo_customer_group__array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.customers.group');
        $id_odoo_customer_group = $id_odoo_customer_group__array[0][id];

        if (empty($id_odoo_customer)) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
          $values = array("ref" => new xmlrpcval('WebStore - ' . $customers_id, "string"),
                          "phone" => new xmlrpcval($customers_telephone, "string"),
                          "mobile" => new xmlrpcval($customers_cellular_phone, "string"),
                          "fax" => new xmlrpcval($customers_fax, "string"),
                          "street" => new xmlrpcval($entry_street_address, "string"),
                          "street2" => new xmlrpcval($entry_suburb, "string"),
                          "zip" => new xmlrpcval($entry_postcode, "string"),
                          "city" => new xmlrpcval($entry_city, "string"),
                          "comment" => new xmlrpcval('Website Registration - Admin Creation', "string"),
                          "name" => new xmlrpcval($customers_lastname . ' ' . $customers_firstname, "string"),
                          "email" => new xmlrpcval($customers_email_address, "string"),
                          "country_id" => new xmlrpcval($country_id_odoo, "int"),
                          "tz" => new xmlrpcval("Europe/Paris", "string"),
                          "clicshopping_customers_id" => new xmlrpcval($customers_id, "int"),
                          "clicshopping_partner_customer_group_id"  => new xmlrpcval($id_odoo_customer_group, "int"),
                          "opt_out"  => new xmlrpcval($customers_newsletter, "double"),
                        );

          $OSCOM_ODOO->createOdoo($values, "res.partner");

        } else {

// **********************************
// update Customer if exist
// **********************************

          $values = array("name" => new xmlrpcval($customers_lastname . ' ' . $customers_firstname, "string"),
                          "email" => new xmlrpcval($customers_email_address, "string"),
                          'phone' => new xmlrpcval($customers_telephone, "string"),
                          'mobile' => new xmlrpcval($customers_cellular_phone, "string"),
                          'fax' => new xmlrpcval($customers_fax, "string"),
                          'street' => new xmlrpcval($entry_street_address, "string"),
                          'street2' => new xmlrpcval($entry_suburb, "string"),
                          'fax' => new xmlrpcval($customers_fax, "string"),
                          'zip' => new xmlrpcval($entry_postcode, "string"),
                          'city' => new xmlrpcval($entry_city, "string"),
                          'country_id' => new xmlrpcval($country_id_odoo, "int"),
                          "ref" => new xmlrpcval('WebStore - ' . $customers_id, "string"),
                          "clicshopping_customers_id" => new xmlrpcval($customers_id, "int"),
                          "clicshopping_partner_customer_group_id"  => new xmlrpcval($id_odoo_customer_group, "int"),
                          "opt_out"  => new xmlrpcval($customers_newsletter, "double"),
                        );

          $OSCOM_ODOO->updateOdoo($id_odoo_customer, $values, 'res.partner');

        }
      }

      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value
                                        where configuration_key = :configuration_key
                                      ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'ODOO_EXPORT_CUSTOMERS');
      $Qupdate->execute();
    }
  }

  /**
   * Export all suppliers inside odoo
   *
   * @param string
   * @return string
   * @access public
   */

  function osc_bulk_suppliers() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_EXPORT_SUPPLIERS == 'export' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {
      set_time_limit(0);
      ini_set ( 'memory_limit', '64M' );

      $Qsuppliers = $OSCOM_PDO->prepare('select suppliers_id,
                                               suppliers_name,
                                               suppliers_manager,
                                               suppliers_phone,
                                               suppliers_email_address,
                                               suppliers_fax,
                                               suppliers_address,
                                               suppliers_suburb,
                                               suppliers_postcode,
                                               suppliers_city,
                                               suppliers_states,
                                               suppliers_country_id,
                                               suppliers_notes
                                          from :table_suppliers
                                        ');
      $Qsuppliers->execute();


      while($suppliers = $Qsuppliers->fetch() ) {

        $suppliers_id = $suppliers['suppliers_id'];
        $suppliers_name = $suppliers['suppliers_name'];
        $suppliers_manager = $suppliers['suppliers_manager'];
        $suppliers_phone = $suppliers['suppliers_phone'];
        $suppliers_email_address = $suppliers['suppliers_email_address'];
        $suppliers_fax = $suppliers['suppliers_fax'];
        $suppliers_address = $suppliers['suppliers_address'];
        $suppliers_suburb = $suppliers['suppliers_suburb'];
        $suppliers_postcode = $suppliers['suppliers_postcode'];
        $suppliers_city = $suppliers['suppliers_city'];
        $suppliers_notes = $suppliers['suppliers_notes'];

// **********************************
// search iso code ClicShopping
// **********************************

        $QcountryIdCustomer = $OSCOM_PDO->prepare("select suppliers_country_id
                                                   from :table_suppliers
                                                   where suppliers_id = :suppliers_id
                                                  ");
        $QcountryIdCustomer->bindInt(':suppliers_id', (int)$suppliers_id);
        $QcountryIdCustomer->execute();

        $country_id_supplier = $QcountryIdCustomer->fetch();

        $country_id_supplier = $country_id_supplier['suppliers_country_id'];


        $QcountryCode = $OSCOM_PDO->prepare("select countries_iso_code_2
                                       from :table_countries
                                       where countries_id = :countries_id
                                      ");
        $QcountryCode->bindInt(':countries_id', (int)$country_id_supplier);
        $QcountryCode->execute();

        $country_code = $QcountryCode->fetch();
        $country_code = $country_code['countries_iso_code_2'];


// **********************************
// search id country odoo
// **********************************
        $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
        $field_list = array('country_id',
                            'name',
                          );
        $Qcountry_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
        $country_id_odoo = $Qcountry_id_odoo[0][id];


// **********************************
// Search odoo customer id
// **********************************
        $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore Suppliers - ' . $suppliers_id, 'res.partner');

// **********************************
// read id customer odoo
// **********************************
        $field_list = array('ref',
                            'id'
                          );

        $id_odoo_suppplier_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
        $id_odoo_supplier = $id_odoo_suppplier_array[0][id];


        if (empty($id_odoo_supplier)) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
          $values = array("ref" => new xmlrpcval('WebStore Suppliers - ' . $suppliers_id, "string"),
                          "name" => new xmlrpcval($suppliers_name . ' ' . $suppliers_manager, "string"),
                          "email" => new xmlrpcval($suppliers_email_address, "string"),
                          'phone' => new xmlrpcval($suppliers_phone, "string"),
                          'fax' => new xmlrpcval($suppliers_fax, "string"),
                          'street' => new xmlrpcval($suppliers_address, "string"),
                          'street2' => new xmlrpcval($suppliers_suburb, "string"),
                          'zip' => new xmlrpcval($suppliers_postcode, "string"),
                          'city' => new xmlrpcval($suppliers_city, "string"),
                          'country_id' => new xmlrpcval($country_id_odoo, "int"),
                          "supplier" => new xmlrpcval(1, "int"),
                          "tz" => new xmlrpcval("Europe/Paris", "string"),
                          "supplier" => new xmlrpcval(1, "int"),
                          "clicshopping_suppliers_id" => new xmlrpcval($suppliers_id, "int"),
                          "clicshopping_suppliers_notes" => new xmlrpcval($suppliers_notes, "string"),
                        );

          $OSCOM_ODOO->createOdoo($values, "res.partner");

        } else {

// **********************************
// update Customer if exist
// **********************************

          $values = array(
                          "ref" => new xmlrpcval('WebStore Suppliers - ' . $suppliers_id, "string"),
                          "name" => new xmlrpcval($suppliers_name . ' ' . $suppliers_manager, "string"),
                          "email" => new xmlrpcval($suppliers_email_address, "string"),
                          'phone' => new xmlrpcval($suppliers_phone, "string"),
                          'fax' => new xmlrpcval($suppliers_fax, "string"),
                          'street' => new xmlrpcval($suppliers_address, "string"),
                          'street2' => new xmlrpcval($suppliers_suburb, "string"),
                          'zip' => new xmlrpcval($suppliers_postcode, "string"),
                          'city' => new xmlrpcval($suppliers_city, "string"),
                          'country_id' => new xmlrpcval($country_id_odoo, "int"),
                          "clicshopping_suppliers_id" => new xmlrpcval($suppliers_id, "int"),
                          "clicshopping_suppliers_notes" => new xmlrpcval($suppliers_notes, "string"),
                        );

          $OSCOM_ODOO->updateOdoo($id_odoo_supplier, $values, 'res.partner');
        }
      }

      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value
                                        where configuration_key = :configuration_key
                                      ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'ODOO_EXPORT_CUSTOMERS');
      $Qupdate->execute();
    }
  }


/**
 * Export all manufacturer inside odoo
 *
 * @param string
 * @return string
 * @access public
 */

  function osc_bulk_manufacturer() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_EXPORT_MANUFACTURER == 'export' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {
      set_time_limit(0);
      ini_set ( 'memory_limit', '64M' );

      $Qmanufacturers = $OSCOM_PDO->prepare('select m.manufacturers_id,
                                                   m.manufacturers_name,
                                                   m.manufacturers_image,
                                                   md.manufacturer_description,
                                                   md.manufacturers_url,
                                                   md.manufacturer_seo_title,
                                                   md.manufacturer_seo_description,
                                                   md.manufacturer_seo_keyword
                                            from :table_manufacturers m,
                                                 :table_manufacturers_info md
                                            where m.manufacturers_id = md.manufacturers_id
                                            and md.languages_id =  :languages_id
                                        ');
      $Qmanufacturers->bindInt(':languages_id', (int)$_SESSION['languages_id']);
      $Qmanufacturers->execute();

      while($manufacturers = $Qmanufacturers->fetch() ) {

        $manufacturers_id = $manufacturers['manufacturers_id'];
        $manufacturers_name = $manufacturers['manufacturers_name'];
        $manufacturer_description = $manufacturers['manufacturer_description'];
        $manufacturers_image = $manufacturers['manufacturers_image'];
        $manufacturer_seo_title = $manufacturers['manufacturer_seo_title'];
        $manufacturer_seo_description = $manufacturers['manufacturer_seo_description'];
        $manufacturer_seo_keyword = $manufacturers['manufacturer_seo_keyword'];

// **********************************
// Search odoo manufacturer id
// **********************************
        $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $manufacturers_id, 'clicshopping.manufacturer', 'int');

// **********************************
// read id manufacturer odoo
// **********************************
        $field_list = array('clicshopping_manufacturers_id');

        $id_odoo_manufacturer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
        $id_odoo_manufacturer = $id_odoo_manufacturer_array[0][id];

        if (file_exists(DIR_FS_CATALOG_IMAGES . $manufacturers_image ) ) {
          $manufacturers_image =  DIR_FS_CATALOG_IMAGES . $manufacturers_image;
          $manufacturers_image = file_get_contents( $manufacturers_image);
          $manufacturers_image = base64_encode($manufacturers_image);
        }

        if  (empty($id_odoo_manufacturer)) {

// **********************************
// Create manufacturer if doesn't exist in oddo
// **********************************
          $values = array(
                          "clicshopping_manufacturers_id" => new xmlrpcval($manufacturers_id, "int"),
                          "clicshopping_manufacturers_name" => new xmlrpcval($manufacturers_name, "string"),
                          "clicshopping_manufacturer_description" => new xmlrpcval($manufacturer_description, "string"),
                          "clicshopping_manufacturers_image" => new xmlrpcval($manufacturers_image, "string"),
                          "clicshopping_manufacturer_seo_title" => new xmlrpcval($manufacturer_seo_title, "string"),
                          "clicshopping_manufacturer_seo_description" => new xmlrpcval($manufacturer_seo_description, "string"),
                          "clicshopping_manufacturer_seo_keyword" => new xmlrpcval($manufacturer_seo_keyword, "string"),
                          );

          $OSCOM_ODOO->createOdoo($values, "clicshopping.manufacturer");

        } else {

// **********************************
// update manufacturer if exist
// **********************************

          $values = array(
                            "clicshopping_manufacturers_name" => new xmlrpcval($manufacturers_name, "string"),
                            "clicshopping_manufacturer_description" => new xmlrpcval($manufacturer_description, "string"),
                            "clicshopping_manufacturers_image" => new xmlrpcval($manufacturers_image, "string"),
                            "clicshopping_manufacturer_seo_title" => new xmlrpcval($manufacturer_seo_title, "string"),
                            "clicshopping_manufacturer_seo_description" => new xmlrpcval($manufacturer_seo_description, "string"),
                            "clicshopping_manufacturer_seo_keyword" => new xmlrpcval($manufacturer_seo_keyword, "string"),
                          );

          $OSCOM_ODOO->updateOdoo($id_odoo_manufacturer, $values, 'clicshopping.manufacturer');
        }
      }

      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value
                                        where configuration_key = :configuration_key
                                      ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'ODOO_EXPORT_MANUFACTURER');
      $Qupdate->execute();
    }
  }


/**
 * Export all categories inside odoo
 *
 * @param string
 * @return string
 * @access public
 */

  function osc_bulk_categories() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_EXPORT_CATEGORIES == 'export' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {
      set_time_limit(0);
      ini_set ( 'memory_limit', '64M' );

      $Qcategories = $OSCOM_PDO->prepare('select cd.categories_name,
                                                     c.categories_id,
                                                     c.parent_id,
                                                     c.categories_image,
                                                     cd.categories_description,
                                                     cd.categories_head_title_tag,
                                                     cd.categories_head_desc_tag,
                                                     cd.categories_head_keywords_tag
                                           from :table_categories c,
                                                :table_categories_description cd
                                           where c.categories_id = cd.categories_id
                                           and cd.language_id =  :languages_id
                                        ');
      $Qcategories->bindInt(':languages_id', (int)$_SESSION['languages_id']);
      $Qcategories->execute();

      while($category = $Qcategories->fetch() ) {

        if (file_exists(DIR_FS_CATALOG_IMAGES . $category['categories_image']) ) {
          $categories_image = DIR_FS_CATALOG_IMAGES . $category['categories_image'];
          $categories_image = file_get_contents($categories_image);
          $categories_image = base64_encode($categories_image);
        }

// **********************************
// Search  Categories odoo
// **********************************
        $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('clicshopping_categories_id', '=', $category['categories_id'], 'product.category', 'int',
                                                    'clicshopping_categories_parent_id', '=', $category['parent_id'], 'int');

// **********************************
// read id Categories odoo
// **********************************

        $field_list = array('id');

        $Qcategories_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.category');
        $categories_id = $Qcategories_id[0][id];


        if (empty($categories_id)) {

// **********************************
// Create categories if doesn't exist in odoo
// **********************************

          $values = array("name" => new xmlrpcval($category['categories_name'], "string"),
                          "type" => new xmlrpcval('normal', "string"),
                          "complete_name" => new xmlrpcval($category['categories_name'], "string"),
                          "clicshopping_categories_id" => new xmlrpcval($category['categories_id'], "int"),
                          "clicshopping_categories_parent_id" => new xmlrpcval($category['parent_id'], "int"),
                          "clicshopping_categories_image" => new xmlrpcval($categories_image, "string"),
                          "clicshopping_categories_description" => new xmlrpcval($category['categories_description'], "string"),
                          "clicshopping_categories_head_title_tag" => new xmlrpcval($category['categories_head_title_tag'], "string"),
                          "clicshopping_categories_head_desc_tag" => new xmlrpcval($category['categories_head_desc_tag'], "string"),
                          "clicshopping_categories_head_keywords_tag" => new xmlrpcval($category['categories_head_keywords_tag'], "string"),
                        );

          $OSCOM_ODOO->createOdoo($values, "product.category");

        } else {

// **********************************
// update categories if exist
// **********************************

          $id_list = array();
          $id_list[] = new xmlrpcval($categories_id, 'int');

          $values = array("name" => new xmlrpcval($category['categories_name'], "string"),
                          "complete_name" => new xmlrpcval($category['categories_name'], "string"),
                          "clicshopping_categories_id" => new xmlrpcval($category['categories_id'], "int"),
                          "clicshopping_categories_parent_id" => new xmlrpcval($category['parent_id'], "int"),
                          "clicshopping_categories_description" => new xmlrpcval($category['categories_description'], "string"),
                          "clicshopping_categories_head_title_tag" => new xmlrpcval($category['categories_head_title_tag'], "string"),
                          "clicshopping_categories_head_desc_tag" => new xmlrpcval($category['categories_head_desc_tag'], "string"),
                          "clicshopping_categories_head_keywords_tag" => new xmlrpcval($category['categories_head_keywords_tag'], "string"),
                          "clicshopping_categories_image" => new xmlrpcval($categories_image, "string"),
                        );

          $OSCOM_ODOO->updateOdoo($categories_id, $values, 'product.category');
        }
      }

      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                      set configuration_value = :configuration_value
                                      where configuration_key = :configuration_key
                                    ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'ODOO_EXPORT_CATEGORIES');
      $Qupdate->execute();

    }
  }

/**
* Wharehouse and Inventory creation
*
* @param string
* @return string
* @access public
*/
  function osc_config_create_wharehouse() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_WHAREHOUSE_CONFIG == 'configure' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {

      $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();
      $date = date("Y-m-d H:i:s");

// Stock Management
      $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('company_id', '=', $company_id, 'stock.warehouse', 'int',
                                                  'name', '=',  'ClicShopping', 'string');
// read id company odoo
      $field_list = array('id');

      $company_id_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');
      $company_id_wharehouse = $company_id_wharehouse[0][id];

      if (empty($company_id_wharehouse)) {

        $values = array("name" => new xmlrpcval('ClicShopping', "string"),
                        "code" => new xmlrpcval('CL', "string"),
                        "partner_id" => new xmlrpcval($company_id, "int"),
                      );

        $OSCOM_ODOO->createOdoo($values, "stock.warehouse");

      } else {

        $values = array("name" => new xmlrpcval('ClicShopping', "string"),
                        "code" => new xmlrpcval('CL', "string"),
                        "partner_id" => new xmlrpcval($company_id, "int"),
                      );

        $OSCOM_ODOO->updateOdoo($company_id_wharehouse, $values, "stock.warehouse");

      }

// Inventory creation
// search location name and id in stock location
      $ids = $OSCOM_ODOO->odooSearch('name', '=', "CL", 'stock.location');

      $field_list = array('id',
                          'location_id',
                          'name',
                        );

      $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');
      $stock_location_id = $Qstock_location[0][id];
      $stock_location_name = $Qstock_location[0][name];

      $stock_location_id = $stock_location_id + 1;

      $values = array("name" => new xmlrpcval("ClicShopping", "string"),
                      "date" => new xmlrpcval($date, "string"),
                      "company_id" => new xmlrpcval($company_id, "int"),
                      "location_id" => new xmlrpcval($stock_location_id, "int"),
                      "filter" => new xmlrpcval('none', "string"),
                    );

      $OSCOM_ODOO->createOdoo($values, "stock.inventory");


      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                      set configuration_value = :configuration_value
                                      where configuration_key = :configuration_key
                                    ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'ODOO_WHAREHOUSE_CONFIG');
      $Qupdate->execute();
    }
  }


/**
 * Export Customers group
 *
 * @param string
 * @return string
 * @access public
 */
  function osc_bulk_customer_group() {
    global $OSCOM_ODOO, $OSCOM_PDO;

    if (ODOO_EXPORT_CUSTOMERS_GROUP == 'export' && ODOO_ACTIVATE_WEB_SERVICE == 'true') {

      set_time_limit(0);
      ini_set ( 'memory_limit', '64M' );

      $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                             customers_group_name,
                                                             customers_group_discount,
                                                             customers_group_quantity_default,
                                                             color_bar,
                                                             group_order_taxe,
                                                             group_tax
                                              from :table_customers_groups
                                            ');
      $QcustomersGroup->execute();


      while($customers_group = $QcustomersGroup->fetch() ) {

        $customers_groups_name = $customers_group['customers_group_name'];
        $customers_groups_id = $customers_group['customers_group_id'];
        $customers_groups_discount = $customers_group['customers_group_discount'];
        $color_bar = $customers_group['color_bar'];
        $customers_group_quantity_default = $customers_group['customers_group_quantity_default'];
        $group_order_taxe = $customers_group['group_order_taxe'];
        $group_tax = $customers_group['group_tax'];


// **********************************
// Search odoo Categories id
// **********************************

        $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_group_id', '=', $customers_groups_id, 'clicshopping.customers.group','int');

// **********************************
// read id Categories odoo
// **********************************

        $field_list = array('id');

        $QodooCustomersGroupId = $OSCOM_ODOO->readOdoo($ids, $field_list, "clicshopping.customers.group");
        $odoo_customers_group_id = $QodooCustomersGroupId[0][id];

        if (empty($odoo_customers_group_id)) {

// **********************************
// Create products if doesn't exist in odoo
// **********************************

          $values = array("clicshopping_customers_group_name" => new xmlrpcval($customers_groups_name, "string"),
                          "clicshopping_customers_group_id" => new xmlrpcval($customers_groups_id, "int"),
                          "clicshopping_customers_group_discount" => new xmlrpcval($customers_groups_discount, "double"),
                          "clicshopping_customers_group_color_bar" => new xmlrpcval($color_bar, "string"),
                          "clicshopping_customers_group_quantity_default" => new xmlrpcval($customers_group_quantity_default, "int"),
                          "clicshopping_customers_group_order_taxe" => new xmlrpcval($group_order_taxe, "double"),
                          "clicshopping_customers_group_tax" => new xmlrpcval($group_tax, "double"),
                        );

          $OSCOM_ODOO->createOdoo($values, "clicshopping.customers.group");


        } else {

// **********************************
// update products if exist
// **********************************

          $id_list = array();
          $id_list[] = new xmlrpcval($odoo_customers_group_id, 'int');

          $values = array("clicshopping_customers_group_name" => new xmlrpcval($customers_groups_name, "string"),
                          "clicshopping_customers_group_id" => new xmlrpcval($customers_groups_id, "int"),
                          "clicshopping_customers_group_discount" => new xmlrpcval($customers_groups_discount, "double"),
                          "clicshopping_customers_group_color_bar" => new xmlrpcval($color_bar, "string"),
                          "clicshopping_customers_group_quantity_default" => new xmlrpcval($customers_group_quantity_default, "int"),
                          "clicshopping_customers_group_group_order_taxe" => new xmlrpcval($group_order_taxe, "double"),
                          "clicshopping_customers_group_tax" => new xmlrpcval($group_tax, "double"),
                        );
          $OSCOM_ODOO->updateOdoo($odoo_customers_group_id, $values, "clicshopping.customers.group");
        }
      }

      $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value
                                        where configuration_key = :configuration_key
                                      ');
      $Qupdate->bindValue(':configuration_value', 'false');
      $Qupdate->bindValue(':configuration_key', 'ODOO_EXPORT_CUSTOMERS_GROUP');
      $Qupdate->execute();
    }
  }
