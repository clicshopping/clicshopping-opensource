<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

   $languages_array = array(array('id' => 'english', 'text' => 'English'),
                            array('id' => 'french', 'text' => 'Francais'),
                    );
                           
                            
  require ('includes/languages/' . $language . '/' . basename($_SERVER['PHP_SELF']));

 require ('includes/languages/' . $language . '.php');

  $template = 'main_page';
?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="robots" content="noindex,nofollow" />
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon" />

<title>ClicShopping On Demand, Open Source E-Commerce Solutions</title>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />

    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <meta name="robots" content="noindex,nofollow">

     <link rel="stylesheet" href="templates/main_page/stylesheet.css">


</head>

<body>


<div class="container">
  <div class=""contenText">
    <header>
      <div style="float: right; padding-top: 40px; padding-right: 15px; color: #000000; font-weight: bold;"><a href="http://www.clicshopping.org" target="_blank">ClicShopping</a></div>
      <a href="index.php"><img src="images/logo_clicshopping.png" border="0" width="200" height="90" title="CliCshopping" style="margin: 10px 10px 0px 10px;" /></a>
    </header>

    <div class="row" style="padding-top:20px;">
      <div class="col-xs-2 pull-left">
        <?php $step = (isset($_REQUEST['step']) && is_numeric($_REQUEST['step'])) ? $_REQUEST['step'] : 1;?>
        <ul>
          <li class="title">Steps</li>
          <li class="<?php echo ($step == 1) ? 'current' : ''; ?>">License agreement</li>
          <li class="<?php echo ($step == 2) ? 'current' : ''; ?>">Pre installation Check</li>
          <li class="<?php echo ($step == 3) ? 'current' : ''; ?>">Database setup</li>
          <li class="<?php echo ($step == 4) ? 'current' : ''; ?>">Web server setup</li>
          <li class="<?php echo ($step == 5) ? 'current' : ''; ?>">Online store setting</li>
          <li class="<?php echo ($step == 6) ? 'last' : ''; ?>">Finished</li>
        </ul>
        <div id="mBox">
          <div id="mBoxContents"></div>
        </div>
      </div>
      <div class="col-xs-10 pull-right">
        <div id="mainBlock">
          <?php require('templates/pages/' . $page_contents); ?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <footer>
      <div class="text-center well well-sm"><p>
          Copyright &copy; 2008-2015 <a href="http://www.innov-concept.com" target="_blank">ClicShopping (TM) by Innov-concept</a> - Brand deposed INPI by innov Concept
        </p>
      </div>
    </footer>
  </div>
</div>


</body>
</html>
