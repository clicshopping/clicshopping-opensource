<?php
/**
 * d_twitter.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TWITTER_TITLE', 'Do you want install the twitter module to send twits ?');
define('MODULE_ADMIN_DASHBOARD_TWITTER_DESCRIPTION', 'Do you want install the twitter module to send twits ?');
define('TEXT_TITLE_TWITTER','Send a tweet');
?>
