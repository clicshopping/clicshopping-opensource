<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SECURITY_CHECK_EXTENDED_ADMIN_BACKUP_FILE_HTTP_200', 'Je suis capable d\'accéder au répertoire ' . DIR_FS_BACKUP . '  - veuiller éviter que le l\accès public soit activé.');
?>
