<?php
/**
 * page_submit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE','Gestion des métadonnées');

define('TAB_SUBMIT_DEFAULT','Défaut');
define('TAB_SUBMIT_CATEGORIES','Catégories');
define('TAB_SUBMIT_PRODUCTS_INFO','Description produits');
define('TAB_SUBMIT_PRODUCTS_NEW','Nouveautés');
define('TAB_SUBMIT_SPECIAL','Promotions');
define('TAB_SUBMIT_REVIEWS','Commentaires');

define('KEYWORDS_GOOGLE_TREND','Tendance des mots clefs selon Google');
define('ANALYSIS_GOOGLE_TOOL','Outil d\'analyse selon Google');
define('POPULARITY_OVERTURE_KEYWORDS','Popularité des mots clefs selon Yahoo Search Marketing');
define('SUCCESS_PAGE_UPDATED','Succès');

define('TITLE_AIDE_IMAGE','Note');
define('TEXT_PAGES_SUBMIT_INFORMATION_DEFAULT','Gestion des Métadonnées du site par défaut');
define('DISPLAY_TITLE_DEFAULT','Afficher le titre par défaut');
define('DISPLAY_KEYWORDS_DEFAULT','Afficher les mots clefs par défaut');
define('DISPLAY_DESCRIPTION_DEFAULT','Afficher la description par défaut');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_TITLE','Titre');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_KEYWORDS','Mots clefs');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_DESCRIPTION','Desription');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_FOOTER','Pied de page');

define('TEXT_PAGES_SUBMIT_INFORMATION_CATEGORIES','Gestion des Métadonnées du site par défaut de la section catégories');
define('DISPLAY_TITLE_CATEGORIES','Afficher le titre de la catégorie par défaut');
define('DISPLAY_KEYWORDS_CATEGORIES','Afficher les mots clefs des catégories par défaut');
define('DISPLAY_DESCRIPTION_CATEGORIES','Afficher la description des catégories par défaut');
define('TEXT_SUBMIT_LANGUAGE_CATEGORIES_TITLE','Titre des catégories');
define('TEXT_SUBMIT_LANGUAGE_CATEGORIES_KEYWORDS','Mots clefs des catégories');
define('TEXT_SUBMIT_LANGUAGE_CATEGORIES_DESCRIPTION','Description des catégories');

define('TEXT_PAGES_SUBMIT_INFORMATION_PRODUCT_INFO','Gestion des Métadonnées du site par défaut de la section description des produits');
define('DISPLAY_TITLE_PRODUCT_INFO','Afficher le titre de la description des produits par défaut');
define('DISPLAY_KEYWORDS_PRODUCT_INFO','Afficher les mots clefs de la description des produits par défaut');
define('DISPLAY_DESCRIPTION_PRODUCT_INFO','Afficher la description  des produits par défaut');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_TITLE','Titre de la description');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_KEYWORDS','Mots clefs de description');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_DESCRIPTION','Description');

define('TEXT_PAGES_SUBMIT_INFORMATION_PRODUCTS_NEW','Gestion des Métadonnées du site par défaut de la section nouveautés');
define('DISPLAY_TITLE_PRODUCTS_NEW','Afficher le titre de la description des nouveautés par défaut');
define('DISPLAY_KEYWORDS_PRODUCTS_NEW','Afficher les mots clefs de la description des nouveautés par défaut');
define('DISPLAY_DESCRIPTION_PRODUCTS_NEW1','Afficher la description  des nouveautés par défaut');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_TITLE','Titre des nouveautés');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_KEYWORDS','Mots clefs des nouveautés');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_DESCRIPTION','Description des nouveautés');

define('TEXT_PAGES_SUBMIT_INFORMATION_SPECIAL','Gestion des Métadonnées du site par défaut de la section promotions');
define('DISPLAY_TITLE_SPECIAL','Afficher le titre de la description des promotions par défaut');
define('DISPLAY_KEYWORDS_SPECIAL','Afficher les mots clefs de la description des promotions par défaut');
define('DISPLAY_DESCRIPTION_SPECIAL','Afficher la description  des promotions par défaut');
define('TEXT_SUBMIT_LANGUAGE_SPECIAL_TITLE','Titre des promotions');
define('TEXT_SUBMIT_LANGUAGE_SPECIAL_KEYWORDS','Mots clefs des promotions');
define('TEXT_SUBMIT_LANGUAGE_SPECIAL_DESCRIPTION','Description des promotions');

define('TEXT_PAGES_SUBMIT_INFORMATION_REVIEWS','Gestion des Métadonnées du site par défaut de la section commentaires');
define('DISPLAY_TITLE_REVIEWS','Afficher le titre de la description des commentaires par défaut');
define('DISPLAY_KEYWORDS_REVIEWS','Afficher les mots clefs de la description des commentaires par défaut');
define('DISPLAY_DESCRIPTION_REVIEWS','Afficher la description  des commentaires par défaut');
define('TEXT_SUBMIT_LANGUAGE_REVIEWS_TITLE','Titre des commentaires');
define('TEXT_SUBMIT_LANGUAGE_REVIEWS_KEYWORDS','Mots clefs des commentaires');
define('TEXT_SUBMIT_LANGUAGE_REVIEWS_DESCRIPTION','Description des commentaires');

define('TITLE_HELP_SUBMIT', 'Notes sur la gestion des métadonnées');
define('HELP_SUBMIT', '<li>Le système gère automatiquement la gestion des métadonnées.<br />
<blockquote>Il inscrit automatiquement les éléments dans l\'ordre suivant si les champs ne sont pas renseignées:<br />
Titre : le nom de votre produit (ex blouson cuir)<br />
Description : le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie,)<br />
Keywords : le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
categorie : la catégorie du produit<br />
</blockquote></li>
<li>Si vous renseignez les champs, les balises apparaitront de la facon suivante : <br />
<blockquote>
Titre : le titre du champs, le nom de votre produit (ex blouson cuir)<br />
Description : le champs description, le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
Mots clefs : le champs mots clefs, le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
</blockquote></li>
<li><strong>Veuillez à ne pas mettre trop de mots clefs (maximum 10)</strong> et établissez une cohérence entre chaque champs afin de retourver les mots les plus importants en premier</li>
');


?>
