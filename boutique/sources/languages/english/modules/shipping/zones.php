<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('MODULE_SHIPPING_ZONES_TEXT_TITLE', 'Zone Rates');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION', 'Zone Rates<br /><br /><font color="#FF0000"><strong>Becarefull !</strong> Uninstalling this module will make all eventual modifications lost');
define('MODULE_SHIPPING_ZONES_TEXT_WAY', 'Shipping to');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS', 'lb(s)');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE', 'This zone is not covered');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE', 'The shipping rate for that zone cannot be determined at this time');
?>
