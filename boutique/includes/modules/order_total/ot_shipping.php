<?php
/**
 * ot_shipping.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class ot_shipping {
    var $title, $output;

    function ot_shipping() {
      $this->code = 'ot_shipping';
      $this->title = MODULE_ORDER_TOTAL_SHIPPING_TITLE;
      $this->description = MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION;
      $this->enabled = ((MODULE_ORDER_TOTAL_SHIPPING_STATUS == 'true') ? true : false);
      $this->sort_order = MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER;

      $this->output = array();
    }

    function process() {
      global $order, $currencies;

      if (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') {
        $pass = false;
        switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
          case 'national':
            if ($order->delivery['country_id'] == STORE_COUNTRY) $pass = true; break;
          case 'international':
            if ($order->delivery['country_id'] != STORE_COUNTRY) $pass = true; break;
          case 'both':
            $pass = true; break;
          default:
            $pass = false; break;
        }

        if ( ($pass == true) && ( ($order->info['total'] - $order->info['shipping_cost']) >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
          $order->info['shipping_method'] = FREE_SHIPPING_TITLE;
          $order->info['total'] -= $order->info['shipping_cost'];
          $order->info['shipping_cost'] = 0;
        }
      }

      $module = substr($_SESSION['shipping']['id'], 0, strpos($_SESSION['shipping']['id'], '_'));

      if (osc_not_null($order->info['shipping_method'])) {
        if ($GLOBALS[$module]->tax_class > 0) {
          $shipping_tax = osc_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
          $shipping_tax_description = osc_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);

          $order->info['tax'] += osc_calculate_tax($order->info['shipping_cost'], $shipping_tax);

//          $order->info['tax_groups']["$shipping_tax_description"] += osc_calculate_tax($order->info['shipping_cost'], $shipping_tax);
          if (isset($order->info['tax_groups']["$shipping_tax_description"])) {
            $order->info['tax_groups']["$shipping_tax_description"] += osc_calculate_tax($order->info['shipping_cost'], $shipping_tax);
          } else {
            $order->info['tax_groups']["$shipping_tax_description"] = osc_calculate_tax($order->info['shipping_cost'], $shipping_tax);
          }

         $order->info['total'] += osc_calculate_tax($order->info['shipping_cost'], $shipping_tax);

          if (DISPLAY_PRICE_WITH_TAX == 'true') $order->info['shipping_cost'] += osc_calculate_tax($order->info['shipping_cost'], $shipping_tax);
        }

        $this->output[] = array('title' => $order->info['shipping_method'] . ':',
                                'text' => $currencies->format($order->info['shipping_cost'], true, $order->info['currency'], $order->info['currency_value']),
                                'value' => $order->info['shipping_cost']);
      }
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = osc_db_query("select configuration_value from configuration where configuration_key = 'MODULE_ORDER_TOTAL_SHIPPING_STATUS'");
        $this->_check = osc_db_num_rows($check_query);
      }

      return $this->_check;
    }

    function keys() {
      return array('MODULE_ORDER_TOTAL_SHIPPING_STATUS', 
                   'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING',
                   'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', 
                   'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION',
                   'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER'
                  );
    }

    function install() {
      if ($_SESSION['languages_id'] =='1') {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Affichage livraison', 'MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'true', 'Voulez vous afficher le co&ucirc;t de la livraison sur la commande ?', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Permettre la livraison gratuite', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'false', 'Voulez vous accepter les livraisons gratuites en fonction du montant ?<br />Note : Si vous avez activ&eacute; le module d\'exp&eacute;dition gratuite (module / livraison), veuillez ne pas utiliser celui-ci.', '6', '3', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, date_added) values ('Livraison gratuite pour commande au dessus', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', '50', 'Permettre la livraison gratuite pour les commandes au dessus du montant suivant.', '6', '4', 'currencies->format', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Attacher livraison gratuite pour les destinations', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION', 'national', 'Livraison gratuite pour des commandes envoy&eacute;s &agrave; l\'ensemble des destinations.<br />(both=tous les deux)', '6', '5', 'osc_cfg_select_option(array(\'national\', \'international\', \'both\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ordre de tri', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', '2', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier).', '6', '6', now())");
      } else {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Display Shipping', 'MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'true', 'Do you want to display the order shipping cost?', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Allow Free Shipping', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'false', 'Do you want to allow free shipping?', '6', '3', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, date_added) values ('Free Shipping For Orders Over', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', '50', 'Provide free shipping for orders over the set amount.', '6', '4', 'currencies->format', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Provide Free Shipping For Orders Made', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION', 'national', 'Provide free shipping for orders sent to the set destination.', '6', '5', 'osc_cfg_select_option(array(\'national\', \'international\', \'both\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', '2', 'Sort order of display.', '6', '6', now())");
      }
    }


    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>
