<?php
  /*
   * xml_rpc_admin_manufacturers.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

// **********************************
// Search odoo manufacturer id
// **********************************
  $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $manufacturers_id, 'clicshopping.manufacturer', 'int');

// **********************************
// read id manufacturer odoo
// **********************************
  $field_list = array('clicshopping_manufacturers_id');

  $id_odoo_manufacturer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
  $id_odoo_manufacturer = $id_odoo_manufacturer_array[0][id];

  if (file_exists(DIR_FS_CATALOG_IMAGES . $manufacturers_image ) ) {
    $manufacturers_image = DIR_FS_CATALOG_IMAGES . $manufacturers_image;
    $manufacturers_image = file_get_contents($manufacturers_image);
    $manufacturers_image = base64_encode($manufacturers_image);
  }

  if  (empty($id_odoo_manufacturer)) {

// **********************************
// Create manufacturer if doesn't exist in oddo
// **********************************
    $values = array(
                    "clicshopping_manufacturers_id" => new xmlrpcval($manufacturers_id, "int"),
                    "clicshopping_manufacturers_name" => new xmlrpcval($manufacturers_name, "string"),
                    "clicshopping_manufacturer_description" => new xmlrpcval($manufacturer_description, "string"),
                    "clicshopping_manufacturers_image" => new xmlrpcval($manufacturers_image, "string"),
                    "clicshopping_manufacturer_seo_title" => new xmlrpcval($manufacturer_seo_title, "string"),
                    "clicshopping_manufacturer_seo_description" => new xmlrpcval($manufacturer_seo_description, "string"),
                    "clicshopping_manufacturer_seo_keyword" => new xmlrpcval($manufacturer_seo_keyword, "string"),
                   );

    $OSCOM_ODOO->createOdoo($values, "clicshopping.manufacturer");

  } else {

// **********************************
// update manufacturer if exist
// **********************************
    $values = array(
                    "clicshopping_manufacturers_name" => new xmlrpcval($manufacturers_name, "string"),
                    "clicshopping_manufacturer_description" => new xmlrpcval($manufacturer_description, "string"),
                    "clicshopping_manufacturers_image" => new xmlrpcval($manufacturers_image, "string"),
                    "clicshopping_manufacturer_seo_title" => new xmlrpcval($manufacturer_seo_title, "string"),
                    "clicshopping_manufacturer_seo_description" => new xmlrpcval($manufacturer_seo_description, "string"),
                    "clicshopping_manufacturer_seo_keyword" => new xmlrpcval($manufacturer_seo_keyword, "string"),
                   );

    $OSCOM_ODOO->updateOdoo($id_odoo_manufacturer, $values, 'clicshopping.manufacturer');

  }
?>