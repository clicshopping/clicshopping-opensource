<?php
/*
 * account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ($OSCOM_MessageStack->size('account') > 0) {
    echo $OSCOM_MessageStack->output('account');
  }
  
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
  <legend></legend>
  <div class="contentContainer"><?php echo $OSCOM_Template->getBlocks('modules_account_customers'); ?></div>
<?php  require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>