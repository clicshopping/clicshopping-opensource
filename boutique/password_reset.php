<?php
/*
 * password_reset.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('password_reset'));

  $error = false;

  if ( !isset($_GET['account']) || !isset($_GET['key']) ) {
    $error = true;

    $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_RESET_LINK_FOUND);
  }

  if ($error == false) {
    $email_address = isset($_GET['account']) ? trim($_GET['account']) : null;
    $password_key = isset($_GET['key']) ? trim($_GET['key']) : null;

    if ( (osc_validate_email($email_address) == false) ) {
      $error = true;

      $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);

    } elseif (strlen($password_key) != 40) {
      $error = true;

      $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_RESET_LINK_FOUND);

    } else {
      $Qc = $OSCOM_PDO->prepare('select c.customers_id, 
                                        c.customers_email_address, 
                                        ci.password_reset_key, 
                                        ci.password_reset_date 
                                 from :table_customers c, 
                                      :table_customers_info ci 
                                 where c.customers_email_address = :customers_email_address
                                 and c.customers_id = ci.customers_info_id 
                                 limit 1
                               ');
      $Qc->bindValue(':customers_email_address', $email_address);
      $Qc->execute();

      if ( $Qc->fetch() !== false ) {

        if ( (strlen($Qc->value('password_reset_key')) != 40) || ($Qc->value('password_reset_key') != $password_key) || (strtotime($Qc->value('password_reset_date') . ' +1 day') <= time()) ) {

          $error = true;

          $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_RESET_LINK_FOUND);
        }
      } else {
        $error = true;

        $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
      }
    }
  }

  if ($error == true) {
    osc_redirect(osc_href_link('password_forgotten.php'));
  }

  if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {

    $password_new = isset($_POST['password']) ? trim($_POST['password']) : null;
    $password_confirmation = isset($_POST['confirmation']) ? trim($_POST['confirmation']) : null;

    if (strlen($password_new) < ENTRY_PASSWORD_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('password_reset', ENTRY_PASSWORD_NEW_ERROR);
    } elseif ($password_new != $password_confirmation) {
      $error = true;

      $OSCOM_MessageStack->addError('password_reset', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
    }

    if ($error == false) {

      $OSCOM_PDO->save('customers', array('customers_password' => osc_encrypt_password($password_new)),
                                     array('customers_id' => $Qc->valueInt('customers_id'))
                         );

      $OSCOM_PDO->save('customers_info', array('customers_info_date_account_last_modified' => 'now()',
                                                'password_reset_key' => 'null',
                                                'password_reset_date' => 'null'),
                                          array('customers_info_id' => $Qc->valueInt('customers_id'))
                         );


      $OSCOM_MessageStack->addSuccess('login', SUCCESS_PASSWORD_RESET);

      osc_mail($Qc->value('customers_firstname') . ' ' . $Qc->value('customers_lastname'), $email_address, TEXT_EMAIL_SUBJECT, TEXT_EMAIL_BODY, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

      osc_redirect(osc_href_link('login.php', '', 'SSL'));
    }
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('login.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2);

  require($OSCOM_Template->getTemplateFiles('password_reset'));
