<?php
/**
 * ext_directory_listing.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_EXT_DIRECTORY_LISTING_HTTP_200', 'I am able to access the ' . DIR_WS_CATALOG . 'ext/ directory - please disable directory listing for this directory in your web server configuration.');
?>
