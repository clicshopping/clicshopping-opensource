<?php
/*
 * google_sitemap_categories.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/
  $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

  include ('includes/application_top.php');

  $category_array = array();

  $Qcategorie = $OSCOM_PDO->prepare('select categories_id,
                                     coalesce(NULLIF(last_modified, :last_modified),
                                                     date_added) as last_modified
                                      from :table_categories
                                      where virtual_categories = :virtual_categories
                                      group by categories_id
                                      order by last_modified DESC
                                      ');

  $Qcategorie->bindValue(':last_modified', '');
  $Qcategorie->bindValue(':virtual_categories', '0');
  $Qcategorie->execute();

  while ($categories = $Qcategorie->fetch() ) {
    $category_array[$categories['categories_id']]['loc'] = osc_href_link('index.php', 'cPath=' . (int)$categories['categories_id']);
    $category_array[$categories['categories_id']]['lastmod'] = $categories['last_modified'];
    $category_array[$categories['categories_id']]['changefreq'] = 'weekly';
    $category_array[$categories['categories_id']]['priority'] = '0.5';
  }

  foreach ($category_array as $k => $v) {
    $url = $xml->addChild('url');
    $url->addChild('loc', $v['loc']);
    $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');
  }

  header('Content-type: text/xml');
  echo $xml->asXML();