<?php
/*
 * xml_rpc_admin_customers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// **********************************
// Search odoo Categories id
// **********************************

     $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_group_id', '=', $customers_groups_id, 'clicshopping.customers.group', 'int');

// **********************************
// read id Categories odoo
// **********************************

    $field_list = array('id');

    $QodooCustomersGroupId = $OSCOM_ODOO->readOdoo($ids, $field_list, "clicshopping.customers.group");
    $odoo_customers_group_id = $QodooCustomersGroupId[0][id];

    if  (empty($odoo_customers_group_id)) {

// **********************************
// Create products if doesn't exist in odoo
// **********************************
      $QcustomersGroups = $OSCOM_PDO->prepare("select customers_group_id
                                               from :table_customers_groups
                                               where customers_group_name = :customers_group_name
                                              ");
      $QcustomersGroups->bindValue(':customers_group_name', $customers_groups_name);

      $QcustomersGroups->execute();

      $customers_groups = $QcustomersGroups->fetch();
      $customers_group_id = $customers_groups['customers_group_id'];

      $values = array ("clicshopping_customers_group_name" => new xmlrpcval($customers_groups_name, "string"),
                       "clicshopping_customers_group_id" => new xmlrpcval($customers_group_id, "int"),
                       "clicshopping_customers_group_discount" => new xmlrpcval($customers_groups_discount, "double"),
                       "clicshopping_customers_group_color_bar" => new xmlrpcval($color_bar, "string"),
                       "clicshopping_customers_group_quantity_default" => new xmlrpcval($customers_group_quantity_default, "int"),
                       "clicshopping_customers_group_order_taxe" => new xmlrpcval($group_order_taxe, "double"),
                       "clicshopping_customers_group_tax" => new xmlrpcval($group_tax, "double"),
                      );

      $OSCOM_ODOO->createOdoo($values, "clicshopping.customers.group");


    }  else {

// **********************************
// update products if exist
// **********************************

      $id_list = array();
      $id_list[]= new xmlrpcval($odoo_customers_group_id, 'int');

      $values = array ("clicshopping_customers_group_name" => new xmlrpcval($customers_groups_name, "string"),
                      "clicshopping_customers_group_id" => new xmlrpcval($customers_groups_id, "int"),
                      "clicshopping_customers_group_discount" => new xmlrpcval($customers_groups_discount, "double"),
                      "clicshopping_customers_group_color_bar" => new xmlrpcval($color_bar, "string"),
                      "clicshopping_customers_group_quantity_default" => new xmlrpcval($customers_group_quantity_default, "int"),
                      "clicshopping_customers_group_group_order_taxe" => new xmlrpcval($group_order_taxe, "double"),
                      "clicshopping_customers_group_tax" => new xmlrpcval($group_tax, "double"),
                    );
      $OSCOM_ODOO->updateOdoo($odoo_customers_group_id, $values, "clicshopping.customers.group");
    }

?>