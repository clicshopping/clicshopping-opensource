<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TITLE', 'Discount Coupon');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TAX_NOT_APPLIED', 'Tax applied');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DESCRIPTION', 'Discount Coupon<br /><br /><p style="color:#FF0000;"> <strong> Warning! </strong>  Disabling this module will restore the default values</p>');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_FILE', 'Discount Coupon [code] applied');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TEXT_SHIPPING_DISCOUNT', 'off shipping');

/*
Use this to define how the order total line will display on the order confirmation, invoice, etc.
You can insert variables to have dynamic data display.
Variables:
[code]
[coupon_desc]
[discount_amount]
[min_order]
[number_available]
[tax_desc]
*/
?>
