<?php
/**
 * modules_upload.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Modules Installation');
define('HEADING_TITLE_STEP1', 'Module installation choixe');
define('HEADING_TITLE_STEP2', 'template choice');
define('HEADING_TITLE_STEP3', 'Module Download');
define('HEADING_TITLE_STEP4', 'Others informations');
define('HEADING_TITLE', 'Others informations');

// step1

define('WELCOME_HEADING_TITLE','
<br /><br />Welcome to the installation system modules from the ClicShopping market place.<br /><br />
				<ul>Installation process</ul>
				<blockquote>
				 <li>Etape 1 - Download the module from the marketplace</li>
				 <li>Etape 2 - Choose the type of module to install in the dialog box (all information  is on the market place concernning the module)</li>
				 <li>Etape 3 - Module Installation</li>
				 <li>Etape 4 - Configure the module into the appropriate section of the administration</li>
				</blockquote>
	  			 <br /><br />
	   			<p align="center">Click on the button to begin the intallation process</p>
');

// step1

define('WELCOME_MESSAGE_STEP1','
<ul>Installation process</ul>
				<blockquote>
				 <li>Etape 2 - Choose the module to install (all information concerning this choice are on the marketplace)</li>
				 <li>Etape 3 - Module Installation</li>
				 <li>Etape 4 - Configure the module in the appropriate section Configurer in the administration</li>
				</blockquote>
	  			 <br /><br />
	   			<p align="center">Please, select from the following options</p>
');

define('TEXT_MODULE_TEMPLATE','Template_module');
define('TEXT_MODULE_FIXE','Fix module');
define('TEXT_MODULE_TEMPLATE','Template');

//step2
define('WELCOME_MESSAGE_STEP2','
				<ul>Installation process</ul>
				<blockquote>
				 <li>Etape 3 - Select the template or the module must be installed</li>
				 <li>Etape 4 - Configure the module into the appropriate section of the administration</li>
				</blockquote>
	  			 <br /><br />
	   			<p align="center">Please select the template or the module must be installed</p>
');

//STEP3
define('WELCOME_MESSAGE_STEP3','	
					<ul>Installation process</ul>
				<blockquote>
				 <li>Etape 4 - Installation module</li>
				 <li>Etape 5 - Configure the module into the appropriate section of the administration</li>
				</blockquote>
	  			 <br /><br />
	   			<p align="center">Please download the module to be installed in the directory : '. $template_install .'</p>
');

//step4
define('WELCOME_MESSAGE_INTRO_STEP4','
				<blockquote>
				 <li>Etape 5 - Configure the module into the appropriate section of the administration</li>
				</blockquote>
	  			 <br /><br />
	   			<p align="center">The installation process is finished</p>
');



define('WELCOME_MESSAGE_STEP4','
				<ul>Where can I find your installation ?</ul>
				<blockquote><blockquote>
				 <li>Boxe module : Menu Design / Boxes left and right</li>
				 <li>Index module : Menu Design / Maon page</li>
				 <li>listing module : Menu Design / Listing</li>
				 <li>Meta tag module : menu Configuration / search engine / Meta data activation </li>
				 <li>Social network module : Menu Configuration / search engine / social network activation</li>
				 <li>Categories module : Menu Design /categories</li>
				 <li>Products desctiption module : Menu Design / products description</li>
				</blockquote></blockquote>
');


?>