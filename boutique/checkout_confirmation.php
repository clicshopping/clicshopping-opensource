<?php
/*
 * checkout_confirmation.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');


// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($_SESSION['cart']->count_contents() < 1) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }
  
// if the customer is not logged on, redirect them to the login page
  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot(array('mode' => 'SSL', 'page' => 'checkout_payment.php'));
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

// avoid hack attempts during the checkout procedure by checking the internal cartID
  if (isset($_SESSION['cart']->cartID) && isset($_SESSION['cartID'])) {
    if ($_SESSION['cart']->cartID != $_SESSION['cartID']) {
      osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
    }
  }

// if no shipping method has been selected, redirect the customer to the shipping method selection page
  if (!isset($_SESSION['shipping'])) {
    osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
  }

  if (isset($_POST['payment'])) $_SESSION['payment'] = $_POST['payment'];

  if (isset($_POST['comments'])) {
    $_SESSION['comments'] = '';
    if (osc_not_null($_POST['comments'])) {
      $_SESSION['comments'] = osc_db_prepare_input($_POST['comments']);
    }    
  }

 if (!isset($_SESSION['coupon'])) $_SESSION['coupon'] = $_POST['coupon'];

  //this needs to be set before the order object is created, but we must process it after
 // $coupon = osc_db_prepare_input($_POST['coupon']);
  if (isset($_SESSION['coupon'])) {
    $coupon = '';
    if (osc_not_null($_SESSION['coupon'])) {
      $_SESSION['coupon'] = osc_db_prepare_input($_SESSION['coupon']);
    }
  }

// Confirmation des conditions des vente
  if (DISPLAY_CONDITIONS_ON_CHECKOUT == 'true') {
    if (!isset($_POST['conditions']) || ($_POST['conditions'] != '1')) {
      osc_redirect(osc_href_link('checkout_payment.php', 'error_message=' . urlencode(ERROR_CONDITIONS_NOT_ACCEPTED), 'SSL'));
    }
  }

// load the selected payment module
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment($_SESSION['payment']);

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

  $payment_modules->update_status();

  if ( ($payment_modules->selected_module != $_SESSION['payment']) || ( is_array($payment_modules->modules) && (sizeof($payment_modules->modules) > 1) && !is_object($GLOBALS[$_SESSION['payment']]) ) || (is_object($GLOBALS[$_SESSION['payment']]) && ($GLOBALS[$_SESSION['payment']]->enabled == false)) ) {
    osc_redirect(osc_href_link('checkout_payment.php', 'error_message=' . urlencode(ERROR_NO_PAYMENT_MODULE_SELECTED), 'SSL'));
  }

  if (is_array($payment_modules->modules)) {
    $payment_modules->pre_confirmation_check();
  }

// discount coupons
  if (isset( $_SESSION['coupon']) && is_object($order->coupon) ) {
    $order->coupon->verify_code();
// erreur quand le nbr de coupon permis = 0 et debug = false
    if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DEBUG != 'true' ) {
      if( !$order->coupon->is_errors() ) {
//     if( $order->coupon->is_recalc_shipping() ) osc_redirect( osc_href_link( 'checkout_shipping.php', 'error_message=' . urlencode( ENTRY_DISCOUNT_COUPON_SHIPPING_CALC_ERROR ), 'SSL' ) ); //redirect to the shipping page to reselect the shipping method
      } else {
        if( isset($_SESSION['coupon']) ) unset($_SESSION['coupon']);
        osc_redirect( osc_href_link( 'checkout_payment.php', 'error_message=' . urlencode( implode( ' ', $order->coupon->get_messages() ) ), 'SSL' ) );
      }
    }
  } else {
     if(isset($_SESSION['coupon']) ) {
       unset($_SESSION['coupon']);
       require_once( DIR_WS_CLASSES.'discount_coupon.php' );
       if( discount_coupon::is_recalc_shipping() ) osc_redirect( osc_href_link( 'checkout_shipping.php', 'error_message=' . urlencode( ENTRY_DISCOUNT_COUPON_SHIPPING_CALC_ERROR ), 'SSL' ) );
     }
  }

// load the selected shipping module
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping($_SESSION['shipping']);

  require(DIR_WS_CLASSES . 'order_total.php');
  $order_total_modules = new order_total;
  $order_total_modules->process();

// Stock Check
  $any_out_of_stock = false;
  if (STOCK_CHECK == 'true') {
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
      if (osc_check_stock($order->products[$i]['id'], $order->products[$i]['qty'])) {
        $any_out_of_stock = true;
      }
    }
    // Out of Stock
    if ( (STOCK_ALLOW_CHECKOUT != 'true') && ($any_out_of_stock == true) ) {
      osc_redirect(osc_href_link('shopping_cart.php'));
    }
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_confirmation'));


// Payment Management Url redirection
  if (isset($GLOBALS[$_SESSION['payment']]->form_action_url)) {
    $form_action_url = $GLOBALS[$_SESSION['payment']]->form_action_url;
  } else {
    $form_action_url = osc_href_link('checkout_process.php', '', 'SSL');
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('checkout_shipping.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2);


// Modification suite atos 
  if (is_array($payment_modules->modules)) {
    $confirmation = $payment_modules->confirmation();
  }

// Detection si atos est installe verifier si le code a la ligne 341 est Ok : Atos module
  if ( substr($payment, 0, 4) == 'atos' ) {
    if (isset($GLOBALS[$_SESSION['payment']]->form_submit)) {
      $button_payment_module = $GLOBALS[$_SESSION['payment']]->form_submit;
    } else {
      $button_payment_module =  osc_draw_button(IMAGE_BUTTON_CONFIRM_ORDER, null, null,  'primary');
    }
     $button_payment_module . '</form>' . "\n";
  }

  require($OSCOM_Template->getTemplateFiles('checkout_confirmation'));
