<?php
/*
 * languages.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 



Tables Impactees
TABLE_CATEGORIES_DESCRIPTION
TABLE_PRODUCTS_DESCRIPTION
TABLE_PRODUCTS_OPTIONS
TABLE_PRODUCTS_OPTIONS_VALUES
TABLE_PRODUCTS_EXTRA_FIELDS
TABLE_MANUFACTURERS_INFO
TABLE_SUPPLIERS_INFO
TABLE_ORDERS_STATUS
TABLE_ORDERS_STATUS INVOICE
TABLE_LANGUAGES
TABLE_PAGES_MANAGER_DESCRIPTION
TABLE_SUBMIT_DESCRIPTION
TABLE_BANNERS
TABLE_EMAIL_DESCRIPTION
TABLE_NEWSLETTER_NO_ACCOUNT =======> langue non supprimee (pour garder les contacts)
TABLE_PRODUCTS_QUANTITY_UNIT
TABLE_BLOG_CATEGORIES_DESCRIPTION
TABLE_BLOG_CONTENT_DESCRIPTION
TABLE_ORDERS_TRACKING
*/

  require('includes/application_top.php');
  require('includes/functions/languages.php');


  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {

     case 'setflag':
        osc_set_language_status($_GET['lid'], $_GET['flag']);

// Verifie si les status ne sont pas tous en off

        $QcountLanguages = $OSCOM_PDO->prepare('select count(status) as status
                                                from :table_languages
                                                where status = :status
                                              ');
        $QcountLanguages->bindInt(':status', 1);
        $QcountLanguages->execute();

        $count_languages = $QcountLanguages->fetch();

        if ($count_languages['status'] == '0')  {

          $Qupdate = $OSCOM_PDO->prepare('update :table_languages
                                          set status = :status
                                          where languages_id = :languages_id
                                        ');
          $Qupdate->bindInt(':status', '1' );
          $Qupdate->bindInt(':languages_id', (int)$_GET['lid']);
          $Qupdate->execute();
        }

        osc_redirect(osc_href_link('languages.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'lID=' . $_GET['lid']));
      break;

      case 'insert':
        $name = osc_db_prepare_input($_POST['name']);
        $code = osc_db_prepare_input(substr($_POST['code'], 0, 2));
        $image = osc_db_prepare_input($_POST['image']);
        $directory = osc_db_prepare_input($_POST['directory']);
        $sort_order = (int)osc_db_prepare_input($_POST['sort_order']);
        $status = (int)osc_db_prepare_input($_POST['status']);

        if (isset($_POST['create_language']) && ($_POST['create_language'] == 'on')) {

          $QlngDefaultDirectory = $OSCOM_PDO->prepare('select directory
                                                       from :table_languages
                                                       where code = :code
                                                      ');

          $QlngDefaultDirectory->bindValue(':code', DEFAULT_LANGUAGE);
          $QlngDefaultDirectory->execute();

          $lng_default_directory = $QlngDefaultDirectory->fetch();

          $lng_default_directory = $lng_default_directory['directory'];

// ---------------------------------------------
// -- Copy the new language in admin directory
// ---------------------------------------------
          $source = DIR_FS_ADMIN . DIR_WS_LANGUAGES . $lng_default_directory;
          $dest = DIR_FS_ADMIN . DIR_WS_LANGUAGES . $directory;

          if (is_dir($source)) {
            osc_smartCopy($source, $dest);
          }

// ---------------------------------------------
// copy the files in the language admin directory
// ---------------------------------------------
          $source_admin = DIR_FS_ADMIN . DIR_WS_LANGUAGES . $lng_default_directory.'.php';
          $dest_admin = DIR_FS_ADMIN . DIR_WS_LANGUAGES . $directory.'.php';


          if (file_exists($source_admin)) {
            copy($source_admin, $dest_admin);
            chmod($dest_admin, 0644);
          }

// ---------------------------------------------
// -- Copy the new language in template catalog directory : original language
// ---------------------------------------------

          $source = DIR_FS_CATALOG_LANGUAGES. $lng_default_directory;
          $dest = DIR_FS_CATALOG_LANGUAGES . $directory;

          if (is_dir($source)) {
            osc_smartCopy($source, $dest);
          }

// copy the files in the template language catalogue directory
          $source_catalogue = DIR_FS_CATALOG_LANGUAGES . $lng_default_directory.'.php';
          $dest_catalogue = DIR_FS_CATALOG_LANGUAGES . $directory.'.php';

          if (file_exists($source_catalogue)) {
            copy($source_catalogue, $dest_catalogue);
            chmod($dest_catalogue, 0644);
          }

// ---------------------------------------------
// -- Copy the new language in template catalog directory for the add on module
// ---------------------------------------------
/*
          $source = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/languages/' . $lng_default_directory;
          $dest = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/languages/' .  $directory;

          if (is_dir($source)) {
            osc_smartCopy($source, $dest);
          }

          $source_catalogue = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/languages/' . $lng_default_directory.'.php';
          $dest_catalogue = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/languages/' . $directory.'.php';

          if (file_exists($source_catalogue)) {
            copy($source_catalogue, $dest_catalogue);
            chmod($dest_catalogue, 0644);
          }
*/

// ---------------------------------------------------------
// -- Copy the new language in the template design directory
// ---------------------------------------------------------
          $source = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/graphism/' . $lng_default_directory;
          $dest = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/graphism/' .  $directory;

          if (is_dir($source)) {
            osc_smartCopy($source, $dest);
          }

// ---------------------------------------------------------
// -- Copy the new language in the template image /template directory for the image
// ---------------------------------------------------------
          $source =DIR_FS_CATALOG_IMAGES . 'template/' . SITE_THEMA . '/' . $lng_default_directory;
          $dest = DIR_FS_CATALOG_IMAGES . 'template/' . SITE_THEMA . '/' .  $directory;

          if (is_dir($source)) {
            osc_smartCopy($source, $dest);
          }
        } // end checkbox

// ---------------------------------------------------------
// -- insert datas
// ---------------------------------------------------------

        $OSCOM_PDO->save('languages', [
                                      'name' =>  $name,
                                      'code' => $code,
                                      'image' =>  $image,
                                      'directory' => $directory,
                                      'sort_order' => $sort_order,
                                      'status' => 0
                                    ]
                        );

        $insert_id = $OSCOM_PDO->lastInsertId();

// create additional categories_description records
        $Qcategories = $OSCOM_PDO->prepare('select c.categories_id,
                                                   cd.categories_name,
                                                   cd.categories_description
                                          from :table_categories c left join :table_categories_description cd on c.categories_id = cd.categories_id
                                          where cd.language_id = :language_id
                                          ');

        $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $Qcategories->execute();

        while ($categories = $Qcategories->fetch() ) {

          $OSCOM_PDO->save('categories_description', [
                                                        'categories_id' =>  (int)$categories['categories_id'],
                                                        'language_id' => (int)$insert_id ,
                                                        'categories_name' =>  $categories['categories_name'],
                                                        'categories_description' => $categories['categories_description']
                                                      ]
                          );
        }

// create additional products_description records
        $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                                 pd.products_name,
                                                 pd.products_description,
                                                 pd.products_url
                                          from :table_products p left join :table_products_description pd on p.products_id = pd.products_id
                                          where pd.language_id = :language_id
                                          ');

        $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $Qproducts->execute();

        while ($products = $Qproducts->fetch() ) {

          $OSCOM_PDO->save('products_description', [
                                                    'products_id' =>  (int)$products['products_id'],
                                                    'language_id' => (int)$insert_id ,
                                                    'products_name' =>  $products['products_name'],
                                                    'products_description' => $products['products_description'],
                                                    'products_url' => $products['products_url']
                                                   ]
                          );
        }

// create additional products_options records
        $QproductsOptions = $OSCOM_PDO->prepare('select products_options_id,
                                                        products_options_name
                                                 from :table_products_options
                                                 where language_id = :language_id
                                                ');

        $QproductsOptions->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QproductsOptions->execute();

        while ($products_options = $QproductsOptions->fetch() ) {

          $OSCOM_PDO->save('products_options', [
                                                  'products_options_id' =>  (int)$products_options['products_options_id'],
                                                  'language_id' =>  (int)$products_options['products_options_id'],
                                                  'products_options_name' => $products_options['products_options_name']
                                                ]
                          );
        }

// create additional products_options_values records
        $QproductsOptionsValues = $OSCOM_PDO->prepare('select products_options_values_id,
                                                              products_options_values_name
                                                       from :table_products_options_values
                                                       where language_id = :language_id
                                                      ');

        $QproductsOptionsValues->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QproductsOptionsValues->execute();

        while ($products_options_values = $QproductsOptionsValues->fetch() ) {

          $OSCOM_PDO->save('products_options_values', [
                                                        'products_options_values_id' =>  (int)$products_options_values['products_options_values_id'],
                                                        'language_id' =>  (int)$products_options_values['$insert_id'],
                                                        'products_options_values_name' => $products_options_values['products_options_values_name']
                                                      ]
                          );
        }

// create additional manufacturers_info records
        $Qmanufacturers = $OSCOM_PDO->prepare('select m.manufacturers_id,
                                                    mi.manufacturers_url
                                              from :table_manufacturers m left join :table_manufacturers_info mi on m.manufacturers_id = mi.manufacturers_id
                                              where mi.languages_id = :language_id
                                              ');

        $Qmanufacturers->bindInt(':languages_id', (int)$_SESSION['languages_id']);
        $Qmanufacturers->execute();


        while ($manufacturers = $Qmanufacturers->fetch() ) {

          $OSCOM_PDO->save('manufacturers_info', [
                                                  'manufacturers_id' =>  (int)$manufacturers['manufacturers_id'],
                                                  'language_id' =>  (int)$insert_id,
                                                  'manufacturers_url' => $manufacturers['manufacturers_url']
                                                ]
                          );
        }

// create additional suppliers_info records
        $Qsuppliers = $OSCOM_PDO->prepare('select m.suppliers_id,
                                                  mi.suppliers_url
                                              from :table_suppliers m left join :table_suppliers_info mi on m.suppliers_id = mi.suppliers_idcturers_id
                                              where mi.languages_id = :languages_id
                                             ');

        $Qsuppliers->bindInt(':languages_id', (int)$_SESSION['languages_id']);
        $Qsuppliers->execute();

        while ($suppliers = $Qsuppliers->fetch() ) {

          $OSCOM_PDO->save('suppliers_info', [
                                              'suppliers_id' =>  (int)$suppliers['suppliers_id'],
                                              'language_id' =>  (int)$insert_id,
                                              'suppliers_url' => $suppliers['suppliers_url']
                                              ]
                          );
        }

// create additional orders_status records
        $QordersStatus = $OSCOM_PDO->prepare('select orders_status_id,
                                                    orders_status_name
                                              from :table_orders_status
                                              where language_id = :language_id
                                             ');

        $QordersStatus->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QordersStatus->execute();

        while ($orders_status = $QordersStatus->fetch() ) {

          $OSCOM_PDO->save('orders_status', [
                                              'orders_status_id' =>  (int)$orders_status['orders_status_id'],
                                              'language_id' =>  (int)$insert_id,
                                              'orders_status_name' => $orders_status['orders_status_name']
                                             ]
                          );
        }

// create additional orders_status invoice records
        $QordersStatusInvoice = $OSCOM_PDO->prepare('select orders_status_invoice_id,
                                                            orders_status_invoice_name
                                                     from :table_orders_status_invoice
                                                     where language_id = :language_id
                                                    ');

        $QordersStatusInvoice->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QordersStatusInvoice->execute();

        while ($orders_status_invoice = $QordersStatusInvoice->fetch() ) {


          $OSCOM_PDO->save('orders_status_invoice', [
                                                      'orders_status_invoice_id' =>  (int)$orders_status_invoice['orders_status_invoice_id'],
                                                      'language_id' =>  (int)$insert_id,
                                                      'orders_status_invoice_name' => $orders_status_invoice['orders_status_invoice_name']
                                                     ]
                          );

        }


// create additional orders_status records
        $QordersStatusTracking = $OSCOM_PDO->prepare('select orders_status_tracking_id,
                                                              orders_status_tracking_name,
                                                              orders_status_tracking_link
                                                      from :table_orders_status_tracking
                                                      where language_id = :language_id
                                                    ');

        $QordersStatusTracking->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QordersStatusTracking->execute();

        while ($orders_status_tracking = $QordersStatusTracking->fetch() ) {

          $OSCOM_PDO->save('orders_status_tracking', [
                                                      'orders_status_tracking_id' =>  (int)$orders_status_tracking['orders_status_tracking_id'],
                                                      'language_id' =>  (int)$insert_id,
                                                      'orders_status_tracking_name' => $orders_status_tracking['orders_status_tracking_name'],
                                                      'orders_status_tracking_link' => $orders_status_tracking['orders_status_tracking_link']
                                                     ]
                          );
        }

// create additional products quantity records
        $QproductsQuantityUnit = $OSCOM_PDO->prepare('select products_quantity_unit_id,
                                                             products_quantity_unit_title
                                                      from :table_products_quantity_unit
                                                      where language_id = :language_id
                                                    ');

        $QproductsQuantityUnit->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QproductsQuantityUnit->execute();

        while ($products_quantity_unit = $QproductsQuantityUnit->fetch() ) {

          $OSCOM_PDO->save('products_quantity_unit', [
                                                      'products_quantity_unit_id' =>  (int)$products_quantity_unit['products_quantity_unit_id'],
                                                      'language_id' =>  (int)$insert_id,
                                                      'products_quantity_unit_title' => $products_quantity_unit['products_quantity_unit_title']
                                                     ]
                          );

        }

        if (isset($_POST['default']) && ($_POST['default'] == 'on')) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                           set configuration_value = :configuration_value
                                           where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', $code);
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_LANGUAGE');

          $Qupdate->execute();

        }

// create additional page manager records
        $QpageManagerDescription = $OSCOM_PDO->prepare('select pm.pages_id,
                                                               pmd.pages_title,
                                                               pmd.externallink
                                                        from :table_pages_manager pm left join :table_pages_manager_description pmd on pm.pages_id = pmd.pages_id
                                                        where pmd.language_id = :language_id
                                                      ');

        $QpageManagerDescription->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QpageManagerDescription->execute();

        while ($page_manager_description = $QpageManagerDescription->fetch() ) {

          $OSCOM_PDO->save('pages_manager_description', [
                                                          'id' =>  (int)$page_manager_description['id'],
                                                          'pages_id' =>  (int)$page_manager_description['pages_id'],
                                                          'pages_title' => $page_manager_description['pages_title'],
                                                          'externallink' => $page_manager_description['externallink'],
                                                          'language_id' => (int)$insert_id
                                                        ]
                            );
        }

// create additionnal email_description records
        $QtemplateEmailDescription = $OSCOM_PDO->prepare('select t.template_email_id,
                                                                 te.template_email_name ,
                                                                 te.template_email_short_description,
                                                                 te.template_email_description
                                                          from :table_template_email t left join :table_template_email_description te on t.template_email_id = te.template_email_id
                                                           where te.language_id = :language_id
                                                        ');

        $QtemplateEmailDescription->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QtemplateEmailDescription->execute();

        while ($template_email_description = $QtemplateEmailDescription->fetch() ) {

          $OSCOM_PDO->save('template_email_description', [
                                                            'template_email_description_id' =>  (int)$template_email_description['template_email_description_id'],
                                                            'template_email_id' =>  (int)$template_email_description['template_email_id'],
                                                            'language_id' => (int)$insert_id,
                                                            'template_email_name' => $template_email_description['template_email_name'],
                                                            'template_email_short_description' => $template_email_description['template_email_short_description'],
                                                            'template_email_description' => $template_email_description['template_email_description']
                                                          ]
                          );

        }

// create additional page table submit_description
        $QsubmitDescription = $OSCOM_PDO->prepare('select submit_id
                                                   from table_submit_description
                                                   where language_id = :language_id
                                                  ');

        $QsubmitDescription->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QsubmitDescription->execute();

        while ($submit_description = $QsubmitDescription->fetch() ) {

          $OSCOM_PDO->save('submit_description', [
                                                  'submit_id' => $submit_description['submit_id'],
                                                  'submit_defaut_language_title' => '',
                                                  'submit_defaut_language_keywords' => '',
                                                  'submit_defaut_language_description' => '',
                                                  'submit_defaut_language_footer' => '',
                                                  'submit_language_products_info_title' => '',
                                                  'submit_language_products_info_keywords' => '',
                                                  'submit_language_products_info_description' => '',
                                                  'submit_language_products_new_title' => '',
                                                  'submit_language_products_new_keywords' => '',
                                                  'submit_language_products_new_description' => '',
                                                  'submit_language_special_title' => '',
                                                  'submit_language_special_keywords' => '',
                                                  'submit_language_special_description' => '',
                                                  'submit_language_reviews_title' => '',
                                                  'submit_language_reviews_keywords' => '',
                                                  'submit_language_reviews_description' => '',
                                                  'language_id' => (int)$insert_id
                                                ]
                          );

        }

// create additional blog_categories_description records
        $QblogCategories = $OSCOM_PDO->prepare('select c.blog_categories_id,
                                                       cd.blog_categories_name,
                                                       cd.blog_categories_description
                                                from :table_blog_categories c left join :table_blog_categories_description cd on c.blog_categories_id = cd.blog_categories_id
                                                where cd.language_id = :language_id
                                             ');

        $QblogCategories->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QblogCategories->execute();

        while ($blog_categories = $QblogCategories->fetch() ) {

          $OSCOM_PDO->save('blog_categories_description', [
                                                            'blog_categories_id' =>  (int)$blog_categories['blog_categories_id'],
                                                            'language_id' =>  (int)$insert_id,
                                                            'blog_categories_name' => $blog_categories['blog_categories_name'],
                                                            'blog_categories_description' => $blog_categories['blog_categories_description']
                                                          ]
                            );
        }

// create additional blog_content_description records
        $QblogContent = $OSCOM_PDO->prepare('select p.blog_content_id,
                                                    pd.blog_content_name,
                                                    pd.blog_content_description
                                              from :table_blog_content p left join :table_blog_content_description pd on p.blog_content_id = pd.blog_content_id
                                              where pd.language_id = language_id
                                             ');

        $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id']);
        $QblogContent->execute();

        while ($blog_content = $QblogContent->fetch() ) {

          $OSCOM_PDO->save('blog_content_description', [
                                                        'blog_content_id' =>  (int)$blog_content['blog_content_id'],
                                                        'language_id' =>  (int)$insert_id,
                                                        'blog_content_name' => $blog_content['blog_content_name'],
                                                        'blog_content_description' => $blog_content['blog_content_description']
                                                        ]
                          );
        }


        osc_redirect(osc_href_link('languages.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'lID=' . $insert_id));
        break;

      case 'save':
        $lID = osc_db_prepare_input($_GET['lID']);
        $name = osc_db_prepare_input($_POST['name']);
        $code = osc_db_prepare_input(substr($_POST['code'], 0, 2));
        $image = osc_db_prepare_input($_POST['image']);
        $directory = osc_db_prepare_input($_POST['directory']);
        $sort_order = (int)osc_db_prepare_input($_POST['sort_order']);
        $status = (int)osc_db_prepare_input($_POST['status']);

          $Qupdate = $OSCOM_PDO->prepare('update :table_languages
                                          set name = :name,
                                              code = :code,
                                              image = :image,
                                              directory = :directory,
                                              sort_order = :sort_order,
                                              status = :status
                                          where languages_id = :languages_id
                                        ');
          $Qupdate->bindValue(':name', osc_db_input($name));
          $Qupdate->bindValue(':code', osc_db_input($code));
          $Qupdate->bindValue(':image', osc_db_input($image));
          $Qupdate->bindValue(':directory', osc_db_input($directory));
          $Qupdate->bindInt(':sort_order', osc_db_input($sort_order));
          $Qupdate->bindInt(':status', osc_db_input($status));
          $Qupdate->bindInt(':languages_id',  (int)$lID);

          $Qupdate->execute();


        if ($_POST['default'] == 'on') {
          $Qupdate = $OSCOM_PDO->prepare('update configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value',  osc_db_input($code));
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_LANGUAGE');
          $Qupdate->execute();
        }

        osc_redirect(osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $_GET['lID']));
        break;
      case 'deleteconfirm':
        $lID = osc_db_prepare_input($_GET['lID']);

        $Qlng = $OSCOM_PDO->prepare('select languages_id
                                     from  :table_languages
                                     where code = :code
                                   ');
        $Qlng->bindValue(':code', 'DEFAULT_CURRENCY');
        $Qlng->execute();

        $lng = $Qlng->fetch();
          $Qlng = $OSCOM_PDO->prepare('select languages_id
                                       from :table_languages
                                       where code = :code
                                     ');
          $Qlng->bindValue(':code', 'DEFAULT_CURRENCY');
          $Qlng->execute();

          $lng = $Qlng->fetch();

        if ($lng['languages_id'] == $lID) {

          $Qupdate = $OSCOM_PDO->prepare('update table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', '');
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_CURRENCY');
          $Qupdate->execute();
        }

// Delete all table for the language deleted
        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_categories_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_options
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_options_values
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_manufacturers_info
                                        where languages_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_suppliers_info
                                        where languages_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();


        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_orders_status
                                         where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_orders_status_invoice
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_languages
                                        where languages_id = :languages_id
                                      ');
        $Qdelete->bindInt(':languages_id',  (int)$lID);
        $Qdelete->execute();


        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_pages_manager_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_submit_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_extra_fields
                                        where languages_id = :languages_id
                                      ');
        $Qdelete->bindInt(':languages_id',  (int)$lID);
        $Qdelete->execute();


        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_banners
                                        where languages_id = :languages_id
                                      ');
        $Qdelete->bindInt(':languages_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_newsletters
                                        where languages_id = :languages_id
                                      ');
        $Qdelete->bindInt(':languages_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_template_email_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_quantity_unit
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_blog_categories_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_blog_content_description
                                        where language_id = :language_id
                                      ');
        $Qdelete->bindInt(':language_id',  (int)$lID);
        $Qdelete->execute();

        osc_redirect(osc_href_link('languages.php', 'page=' . $_GET['page']));
        break;

      case 'delete':
        $lID = osc_db_prepare_input($_GET['lID']);

        $Qlng = $OSCOM_PDO->prepare('select code
                                     from  :table_languages
                                     where languages_id = :languages_id
                                   ');
        $Qlng->bindInt(':languages_id', (int)$lID);
        $Qlng->execute();

        $lng = $Qlng->fetch();

        $remove_language = true;
        if ($lng['code'] == DEFAULT_LANGUAGE) {
          $remove_language = false;
          $OSCOM_MessageStack->add(ERROR_REMOVE_DEFAULT_LANGUAGE, 'error');
        }
        break;
    }
  }

  require('includes/header.php');
?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/languages.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
<?php
  if (empty($action)) {
?>
            <td class="pageHeading" align="right"><?php echo '<a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $lInfo->languages_id . '&action=new') . '">' . osc_image_button('button_new_language.gif', IMAGE_NEW_LANGUAGE) . '</a>'; ?></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LANGUAGE_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LANGUAGE_CODE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LANGUAGE_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $languages_query_raw = "select languages_id,
                                 name,
                                 code,
                                 image,
                                 directory,
                                 sort_order,
                                 status
                          from languages
                          order by sort_order";
  $languages_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $languages_query_raw, $languages_query_numrows);
  $languages_query = osc_db_query($languages_query_raw);

  while ($languages = osc_db_fetch_array($languages_query)) {
    if ((!isset($_GET['lID']) || (isset($_GET['lID']) && ($_GET['lID'] == $languages['languages_id']))) && !isset($lInfo) && (substr($action, 0, 3) != 'new')) {
      $lInfo = new objectInfo($languages);
    }

    if (isset($lInfo) && is_object($lInfo) && ($languages['languages_id'] == $lInfo->languages_id) ) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $lInfo->languages_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $languages['languages_id']) . '\'">' . "\n";
    }

    if (DEFAULT_LANGUAGE == $languages['code']) {
      echo '                <td class="dataTableContent"><strong>' . $languages['name'] . ' (' . TEXT_DEFAULT . ')</strong></td>' . "\n";
    } else {
      echo '                <td class="dataTableContent">' . $languages['name'] . '</td>' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $languages['code']; ?></td>
                <td class="dataTableContent" align="center">
<?php
      if ($languages['status'] == '1') {
        echo '<a href="' . osc_href_link('languages.php', 'action=setflag&flag=0&page=' . $_GET['page'] . '&lid=' . $languages['languages_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
        } else {
        echo '<a href="' . osc_href_link('languages.php', 'action=setflag&flag=1&page=' . $_GET['page'] . '&lid=' . $languages['languages_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $languages['languages_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $languages['languages_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($lInfo) && is_object($lInfo) && ($languages['languages_id'] == $lInfo->languages_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $languages['languages_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $languages_split->display_count($languages_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_LANGUAGES); ?></td>
                    <td class="smallText" align="right"><?php echo $languages_split->display_links($languages_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_LANGUAGE . '</strong>');

      $contents = array('form' => osc_draw_form('languages', 'languages.php', 'action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_NAME . '<br />' . osc_draw_input_field('name'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_CODE . '<br />' . osc_draw_input_field('code'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_IMAGE . '<br />' . osc_cfg_pull_down_language($filename, $value = ''));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_DIRECTORY . '<br />' . osc_draw_input_field('directory'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_SORT_ORDER . '<br />' . osc_draw_input_field('sort_order'));
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('text' => '<br />' . TEXT_INFO_CREATE_LANGUAGE . '<br />' . osc_draw_checkbox_field('create_language') . ' ' . TEXT_CREATE_LANGUAGE . '<br />' . TEXT_NOTE_CREATE_LANGUAGE);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $_GET['lID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');

    break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_LANGUAGE . '</strong>');

      $contents = array('form' => osc_draw_form('languages', 'languages.php', 'page=' . $_GET['page'] . '&lID=' . $lInfo->languages_id . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_NAME . '<br />' . osc_draw_input_field('name', $lInfo->name));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_CODE . '<br />' . osc_draw_input_field('code', $lInfo->code));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_IMAGE . '<br />' . osc_cfg_pull_down_language($filename, $value = ''));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_DIRECTORY . '<br />' . osc_draw_input_field('directory', $lInfo->directory));
      $contents[] = array('text' => '<br />' . TEXT_INFO_LANGUAGE_SORT_ORDER . '<br />' . osc_draw_input_field('sort_order', $lInfo->sort_order));
      if (DEFAULT_LANGUAGE != $lInfo->code) $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $lInfo->languages_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_LANGUAGE . '</strong>');

      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $lInfo->name . '</strong>');
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . (($remove_language) ? '<a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $lInfo->languages_id . '&action=deleteconfirm') . '">' . osc_image_button('button_delete.gif', IMAGE_DELETE) . '</a>' : '') . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('languages.php', 'page=' . $_GET['page'] . '&lID=' . $lInfo->languages_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    default:
     break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>