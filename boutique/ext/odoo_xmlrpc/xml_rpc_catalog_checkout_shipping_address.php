<?php
/*
 * xml_rpc_catalog_checkout_shipping_address.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/
// **********************************
// Customer select another address during the payment process checkout_payment_address.php
// **********************************

    if ($_POST['action'] == 'submit' ) {
    
      $QnewAddress = $OSCOM_PDO->prepare('select entry_lastname,
                                                  entry_firstname,
                                                  entry_street_address,
                                                  entry_suburb,
                                                  entry_city,
                                                  entry_postcode
                                         from :table_address_book
                                         where address_book_id = :address_book_id
                                         and customers_id = :customers_id
                                       ');
      $QnewAddress->bindInt(':address_book_id', (int)$_SESSION['sendto'] );
      $QnewAddress->bindInt(':customers_id', $OSCOM_Customer->getID());
      $QnewAddress->execute();
      $new_address = $QnewAddress->fetch();


      $lastname  = $new_address['entry_lastname'];
      $firstname = $new_address['entry_firstname'];
      $street_address = $new_address['entry_street_address'];
      $suburb = $new_address['entry_suburb'];
      $city = $new_address['entry_city'];
      $postcode =  $new_address['entry_postcode'];

// **********************************
// search iso code ClicShopping
// **********************************

    $QcountryIdCustomer = $OSCOM_PDO->prepare("select entry_country_id
                                               from :table_address_book
                                               where customers_id = :customers_id
                                               and address_book_id = :address_book_id
                                              ");
    $QcountryIdCustomer->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );
    $QcountryIdCustomer->bindInt('address_book_id', (int)$_SESSION['sendto'] );
    $QcountryIdCustomer->execute();

    $country_id_customer = $QcountryIdCustomer->fetch();
    $country_id_customer = $country_id_customer['entry_country_id'];


    $QcountryCode = $OSCOM_PDO->prepare("select countries_iso_code_2
                                         from :table_countries
                                         where countries_id = :countries_id
                                        ");
    $QcountryCode->bindInt(':countries_id',(int)$country_id_customer);
    $QcountryCode->execute();

    $country_code = $QcountryCode->fetch();
    $country_code = $country_code['countries_iso_code_2'];

// **********************************
// search id country odoo
// **********************************
    $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
    $field_list = array('country_id',
                        'name'
                       );

    $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
    $country_id_odoo = $country_id_odoo[0][id];

    if ($_POST['odoo_create_shipping_address'] == '1') {

      if  ($_SESSION['sendto'] == $OSCOM_Customer->getDefaultAddressID()) {
        $search_in_odoo = 'WebStore - ' . $OSCOM_Customer->getId();
      } else {
        $search_in_odoo = 'WebStore New address - ' . $_SESSION['sendto'];
      }

// **********************************
// Search odoo customer id
// **********************************
      $ids = $OSCOM_ODOO->odooSearch('ref', '=', $search_in_odoo, 'res.partner');

// **********************************
// read id customer odoo
// **********************************
      $field_list = array('ref',
                          'id'
                          );

      $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $id_odoo_customer = $id_odoo_customer_array[0][id];

    } else {

// **********************************
// write data
// **********************************

      $values = array (
                      "name" => new xmlrpcval( $lastname . ' ' . $firstname, "string"),
                      "name" => new xmlrpcval( $lastname . ' ' . $firstname, "string"),
                      'street'=>new xmlrpcval($street_address,"string"),
                      'street2'=>new xmlrpcval($suburb,"string"),
                      'city'=>new xmlrpcval($city,"string"),
                      'zip'=>new xmlrpcval($postcode,"string"),
                      'country_id'=>new xmlrpcval($country_id_odoo,"int"),
                      "comment" => new xmlrpcval('Web store New address book creation - relation with customer_id : ' . $OSCOM_Customer->getID() , "string"),
                      "ref" => new xmlrpcval('WebStore New address - ' . $_SESSION['sendto'], "string"),

      );

      $OSCOM_ODOO->createOdoo($values, 'res.partner');

    }
  }
?>