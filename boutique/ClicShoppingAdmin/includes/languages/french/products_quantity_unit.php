<?php
/**
 * products_quantity_unit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Statut du type de quantité des produits');

define('TABLE_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS', 'Statut du type de quantité des produits');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_PRODUCTS_QUANTITY_UNIT_INTRO', 'Merci de compléter ce nouveau type de quantité de commande');
define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_DELETE', 'Etes vous sur de vouloir supprimer ce statut ?');
define('TEXT_INFO_HEADING_PRODUCTS_QUANTITY_UNIT', 'Nouveau type de quantité de commande');
define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_DELETE', 'Editer type de quantité de commande');
define('TEXT_INFO_EDIT_INTRO','Veuillez compléter ce nouveau type de quantité de commande');
define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_NAME','Type de quantité de commande');

define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_DELETE', 'Supprimer ce statut');
define('TEXT_INFO_DELETE_INTRO','Souhaitez-vous supprimer ce type de quantité ?');
define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS', 'Nouveau statut ');


define('ERROR_REMOVE_DEFAULT_PRODUCTS_UNIT_QUANTITY_STATUS', 'Erreur : Le statut par défaut ne peut pas être supprimé. Merci de choisir un autre statut par défaut et de réessayer');
define('ERROR_STATUS_USED_IN_PRODUCTS_UNIT_QUANTITY', 'Erreur : Ce statut est actuellement utilisé.');

?>