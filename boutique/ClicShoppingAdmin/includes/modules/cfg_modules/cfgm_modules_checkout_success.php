<?php
/**
 * cfm_modules_checkout_success.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class cfgm_modules_checkout_success {
    var $code = 'modules_checkout_success';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_CHECKOUT_SUCCESS_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_checkout_success() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_checkout_success/';
      $this->title = MODULE_CFG_MODULE_CHECKOUT_SUCCESS_TITLE;
    }
  }
?>
