<?php
/*
 * shopping_cart.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require("includes/application_top.php");
  if ($_SESSION['cart']->count_contents() > 0) {
    include(DIR_WS_CLASSES . 'payment.php');
    $payment_modules = new payment;
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('shopping_cart'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('shopping_cart.php'));

  require($OSCOM_Template->getTemplateFiles('shopping_cart'));
