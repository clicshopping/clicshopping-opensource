<?php
/*
   * application_bottom.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
*/
  // close session (store variables)
    session_write_close();

    if (STORE_PAGE_PARSE_TIME == 'true') {
      $time_start = explode(' ', PAGE_PARSE_START_TIME);
      $time_end = explode(' ', microtime());
      $parse_time = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);
      error_log(strftime(STORE_PARSE_DATE_TIME_FORMAT) . ' - ' . $_SERVER['REQUEST_URI'] . ' (' . $parse_time . 's)' . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);

      if (DISPLAY_PAGE_PARSE_TIME == 'true') {
        echo '<span class="smallText">Parse Time: ' . $parse_time . 's</span>';
      }
    }
?>
