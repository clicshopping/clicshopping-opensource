<?php
/*
 * stats_low_stock.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Produits Hors stocks');
define('TABLE_HEADING_PRODUCTS', 'Nom du produit');
define('TABLE_HEADING_MODEL', 'Référence');
define('TABLE_HEADING_QTY_LEFT', 'Quantiés restantes');
define('TABLE_HEADING_ACTION','Action');
define('TABLE_HEADING_WAHREHOUSE_TIME_REPLENISHMENT','Délais de réaprovisionnement');
define('TABLE_HEADING_WHAREHOUSE','Nom de l\'entrepot');
define('TABLE_HEADING_WHAREHOUSE_ROW','Rangée de l\'entreprot');
define('TABLE_HEADING_WHAREHOUSE_LEVEL','Niveau de localisation');

?>