<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Fermer la connexion');
define('NAVBAR_TITLE', 'Fermer la connexion');
define('TEXT_MAIN', 'Vous avez fermé votre session. Vous pouvez laisser votre ordinateur allumé sans risque d\'utilisation de votre compte.<br /><br />Votre panier en cours a été sauvegardé, ses produits vous seront proposés lors de votre prochaine ouverture de session.');
?>