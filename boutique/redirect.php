<?php
/*
 * rediect.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  switch ($_GET['action']) {

    case 'banner':

      $Qbanner = $OSCOM_PDO->get('banners', 'banners_url', ['banners_id' => $_GET['goto'] ]);
    
      if ($Qbanner->fetch() !== false) {
        osc_update_banner_click_count($_GET['goto']);

        osc_redirect($Qbanner->value('banners_url'));
      }
    break;

    case 'url':
    
      if (isset($_GET['goto']) && osc_not_null($_GET['goto'])) {

        $Qcheck = $OSCOM_PDO->get('products_description', 
                                  'products_url', 
                                  ['products_url' => HTML::sanitize($_GET['goto'])], 
                                  null, 
                                  1);

        if ($Qcheck->fetch() !== false) {
          HTTP::redirect('http://' . $Qcheck->value('products_url'));
        }
      }

    break;

    case 'manufacturer':

      if (isset($_GET['manufacturers_id']) && is_numeric($_GET['manufacturers_id'])) {

       $Qmanufacturer = $OSCOM_PDO->get('manufacturers_info', 
                                        'manufacturers_url', ['manufacturers_id' => $_GET['manufacturers_id'], 
                                                             'languages_id' => $_SESSION['languages_id']
                                                             ]
                                       );

        if ($Qmanufacturer->fetch() !== false) {

// url exists in selected language
          if (!empty($Qmanufacturer->value('manufacturers_url'))) {
            $Qupdate = $OSCOM_PDO->prepare('update :table_manufacturers_info 
                                            set url_clicked = url_clicked+1, 
                                                date_last_click = now() 
                                             where manufacturers_id = :manufacturers_id 
                                             and languages_id = :languages_id
                                          ');
            $Qupdate->bindInt(':manufacturers_id', $_GET['manufacturers_id']);
            $Qupdate->bindInt(':languages_id', $_SESSION['languages_id']);
            $Qupdate->execute();

            osc_redirect($Qmanufacturer->value('manufacturers_url'));
          }

        } else {

// no url exists for the selected language, lets use the default language then
          $Qmanufacturer = $OSCOM_PDO->prepare('select mi.languages_id, 
                                                       mi.manufacturers_url 
                                                from :table_manufacturers_info mi, 
                                                     :table_languages l 
                                                where mi.manufacturers_id = :manufacturers_id 
                                                and mi.languages_id = l.languages_id 
                                                and l.code = :default_language
                                              ');
          $Qmanufacturer->bindInt(':manufacturers_id', $_GET['manufacturers_id']);
          $Qmanufacturer->bindValue(':default_language', DEFAULT_LANGUAGE);
          $Qmanufacturer->execute();


          if ($Qmanufacturer->fetch() !== false) {
            if (!empty($Qmanufacturer->value('manufacturers_url'))) {

              $Qupdate = $OSCOM_PDO->prepare('update :table_manufactuers_info 
                                              set url_clicked = url_clicked+1, 
                                                  date_last_click = now()
                                              where manufacturers_id = :manufacturers_id 
                                              and languages_id = :languages_id
                                            ');
              $Qupdate->bindInt(':manufacturers_id', $_GET['manufacturers_id']);
              $Qupdate->bindInt(':languages_id', $Qmanufacturer->valueInt('languages_id'));
              $Qupdate->execute();

              osc_redirect($Qmanufacturer->value('manufacturers_url'));
            }
          } // end $Qmanufacturer->fetch()
        } // end else
      } // is numeric

      break;
  }

  osc_redirect(osc_href_link('index.php'));