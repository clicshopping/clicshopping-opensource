<?php
/*
 * blog.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

// Base sql de categorie description
  require('includes/functions/blog.php');
  
  $action = (isset($_GET['action']) ? $_GET['action'] : '');


// ------------------------------------------------------------
// Action
// ------------------------------------------------------------

  if (osc_not_null($action)) {
    if ( $action == 'insert' || $action == 'update' || $action == 'setflag' || $action == 'archive' || $action == 'delete' ) { 
       if ( preg_match("/(insert|update|setflag|archive|delete)/i", $action) ){
// ULTIMATE Seo Urls 5
// If the action will affect the cache entries
 //        osc_reset_cache_data_usu5( 'reset' );

       }
    }

    switch ($action) {
      case 'setflag':
        if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
          if (isset($_GET['pID'])) {
            osc_set_blog_content_status($_GET['pID'], $_GET['flag']);
          }

          if (USE_CACHE == 'true') {
            osc_cache_reset('blog_tree-');
          }
        }

        osc_redirect(osc_href_link('blog.php', 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID']));
        break;
// ----------------------------------------
// Gestion de la categorie
// ----------------------------------------
      case 'insert_category':
      case 'update_category':
          if (isset($_POST['blog_categories_id'])) $blog_categories_id = osc_db_prepare_input($_POST['blog_categories_id']);

          if ($blog_categories_id == '') {
            $blog_categories_id = osc_db_prepare_input($_GET['cID']);
          }

          $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);
          $sort_order = osc_db_prepare_input($_POST['sort_order']);
          $sql_data_array = array('sort_order' => (int)$sort_order);

          if ($action == 'insert_category') {
            $insert_sql_data = array('parent_id' => $current_category_id,
                                     'date_added' => 'now()',
                                     'customers_group_id' => (int)$customers_group_id
                                     );

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            osc_db_perform('blog_categories', $sql_data_array);

            $blog_categories_id = osc_db_insert_id();

          } elseif ($action == 'update_category') {
            $update_sql_data = array('last_modified' => 'now()',
                                     'customers_group_id' => (int)$customers_group_id
                                     );

            $sql_data_array = array_merge($sql_data_array, $update_sql_data);

            $OSCOM_PDO->save('blog_categories', $sql_data_array, ['blog_categories_id' => (int)$blog_categories_id ] );
          }

          $languages = osc_get_languages();

          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {

            $blog_categories_name_array = $_POST['blog_categories_name'];

            $language_id = $languages[$i]['id'];

            $sql_data_array = array('blog_categories_name' => osc_db_prepare_input($blog_categories_name_array[$language_id]),
                                    'blog_categories_description' => osc_db_prepare_input($_POST['blog_categories_description'][$language_id]),
                                    'blog_categories_head_title_tag' => osc_db_prepare_input($_POST['blog_categories_head_title_tag'][$language_id]),
                                    'blog_categories_head_desc_tag' => osc_db_prepare_input($_POST['blog_categories_head_desc_tag'][$language_id]),
                                    'blog_categories_head_keywords_tag' => osc_db_prepare_input($_POST['blog_categories_head_keywords_tag'][$language_id])
                                   );

            if ($action == 'insert_category') {

              $insert_sql_data = array('blog_categories_id' => $blog_categories_id,
                                       'language_id' => $languages[$i]['id']);

              $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

              $OSCOM_PDO->save('blog_categories_description', $sql_data_array);

            } elseif ($action == 'update_category') {

              $OSCOM_PDO->save('blog_categories_description', $sql_data_array, ['blog_categories_id' => (int)$blog_categories_id,
                                                                                'language_id' => (int)$languages[$i]['id']
                                                                               ]
                              );
            }
          }


// Ajoute ou efface l'image dans la base de donees
          if ($_POST['delete_image'] == 'yes') {
            $blog_categories_image = '';
       
            $Qupdate = $OSCOM_PDO->prepare('update :table_blog_categories
                                            set blog_categories_image = :blog_categories_image 
                                            where blog_categories_id = :blog_categories_id
                                          ');
            $Qupdate->bindValue(':blog_categories_image', osc_db_input($blog_categories_image));
            $Qupdate->bindInt(':blog_categories_id', (int)$blog_categories_id);
            $Qupdate->execute();


          } elseif (isset($_POST['blog_categories_image']) && osc_not_null($_POST['blog_categories_image']) && ($_POST['blog_categories_image'] != 'none')) {
            $blog_categories_image = osc_db_prepare_input($_POST['blog_categories_image']);


// Insertion images des produits via l'editeur FCKeditor (fonctionne sur les nouveaux produits et editions produits)
            if (isset($_POST['blog_categories_image']) && osc_not_null($_POST['blog_categories_image']) && ($_POST['blog_categories_image'] != 'none')) {
              $blog_categories_image_name = $_POST['blog_categories_image'];
              $blog_categories_image_name = htmlspecialchars($blog_categories_image_name);
              $blog_categories_image_name = strstr($blog_categories_image_name, DIR_WS_CATALOG_IMAGES);
              $blog_categories_image_name = str_replace(DIR_WS_CATALOG_IMAGES, '', $blog_categories_image_name);
              $blog_categories_image_name_end = strstr($blog_categories_image_name, '&quot;');
              $blog_categories_image_name = str_replace($blog_categories_image_name_end, '', $blog_categories_image_name);
              $blog_categories_image_name = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $blog_categories_image_name);
            } else {
              $blog_categories_image_name = (isset($_POST['categories_previous_image']) ? $_POST['categories_previous_image'] : '');
            }

              $blog_categories_image = $blog_categories_image_name;

              $Qupdate = $OSCOM_PDO->prepare('update :table_blog_categories
                                              set blog_categories_image = :blog_categories_image 
                                              where blog_categories_id = :blog_categories_id
                                            ');
              $Qupdate->bindValue(':blog_categories_image', $blog_categories_image);
              $Qupdate->bindInt(':blog_categories_id', (int)$blog_categories_id);
              $Qupdate->execute();

          }

          if (USE_CACHE == 'true') {
            osc_cache_reset('blog_tree-');
          }

          osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $blog_categories_id));
          
        break;

      case 'delete_category_confirm':

        if (isset($_POST['blog_categories_id'])) {
          $blog_categories_id = osc_db_prepare_input($_POST['blog_categories_id']);
          $categories = osc_get_blog_category_tree($blog_categories_id, '', '0', '', true);

          $blog_content = array();
          $blog_content_delete = array();

          for ($i=0, $n=sizeof($categories); $i<$n; $i++) {

            $QcustomersGroup = $OSCOM_PDO->prepare('select blog_content_id
                                                    from :table_blog_content_to_categories
                                                    where blog_categories_id = :blog_categories_id
                                                  ');
            $QcustomersGroup->bindint(':blog_categories_id', (int)$categories[$i]['id']);
            $QcustomersGroup->execute();

            while ($blog_content_ids = $QcustomersGroup->fetch() ) {
              $blog_content[$blog_content_ids['blog_content_id']]['categories'][] = $categories[$i]['id'];
            }
          }

          foreach ( $blog_content as $key => $value ) {
            $category_ids = '';

            for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
              $category_ids .= "'" . (int)$value['categories'][$i] . "', ";
            }

            $category_ids = substr($category_ids, 0, -2);

            $Qcheck = $OSCOM_PDO->prepare('select count(*) as total
                                               from :table_blog_content_to_categories
                                               where blog_content_id = :blog_content_id
                                               and blog_categories_id not in (:blog_categories_id)
                                              ');
            $Qcheck->bindInt(':blog_content_id', (int)$key);
            $Qcheck->bindInt(':blog_categories_id', $category_ids);
            $Qcheck->execute();

            if ($Qcheck->value('total') < '1') {
              $blog_content_delete[$key] = $key;
            }
          }

// removing categories can be a lengthy process
          osc_set_time_limit(0);
          for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
           osc_remove_blog_category($categories[$i]['id']);
          }

          foreach  ( array_keys ($blog_content_delete) as $key ) {
            osc_remove_blog_content($key);
          }
        }
/*
        if (USE_CACHE == 'true') {
          osc_cache_reset('blog_tree-');
        }
*/
        osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath));

        break;

      case 'delete_product_confirm':

        if (isset($_POST['blog_content_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories'])) {
          $blog_content_id = osc_db_prepare_input($_POST['blog_content_id']);

          $blog_content_categories = $_POST['product_categories'];

          for ($i=0, $n=sizeof($blog_content_categories); $i<$n; $i++) {

// delete product of categorie
            $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_blog_content_to_categories
                                            where blog_content_id = :blog_content_id
                                            and blog_categories_id = :blog_categories_id
                                           ');
            $Qdelete->bindInt(':blog_content_id', (int)$blog_content_id );
            $Qdelete->bindInt(':blog_categories_id', (int)$blog_content_categories[$i]);
            $Qdelete->execute();
          }

          $QblogContentCategories = $OSCOM_PDO->prepare('select count(*)
                                                         from :table_blog_content_to_categories
                                                         where blog_content_id = :blog_content_id
                                                        ');
          $QblogContentCategories->bindInt(':blog_content_id', (int)$blog_content_id);
          $QblogContentCategories->execute();

          if ($QblogContentCategories->rowCount() == 0 ) {
            osc_remove_blog_content($blog_content_id);
          }
        }
/*
        if (USE_CACHE == 'true') {
          osc_cache_reset('blog_tree-');
        }
*/
        osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath));
        break;

      case 'move_category_confirm':
        if (isset($_POST['blog_categories_id']) && ($_POST['blog_categories_id'] != $_POST['move_to_category_id'])) {
          $blog_categories_id = osc_db_prepare_input($_POST['blog_categories_id']);
          $new_parent_id = osc_db_prepare_input($_POST['move_to_category_id']);

          $path = explode('_', osc_get_generated_blog_category_path_ids($new_parent_id));

          if (in_array($blog_categories_id, $path)) {
            $OSCOM_MessageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');

            osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $blog_categories_id));

          } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_blog_categories
                                            set parent_id = :parent_id, 
                                            last_modified = now()
                                            where blog_categories_id = :blog_categories_id
                                          ');
            $Qupdate->bindInt(':parent_id', (int)$new_parent_id);
            $Qupdate->bindInt(':blog_categories_id',(int)$blog_categories_id);
            $Qupdate->execute();

            if (USE_CACHE == 'true') {
              osc_cache_reset('blog_tree-');
            }

//            osc_redirect(osc_href_link('blog.php','cPath=' . $cPath));
            osc_redirect(osc_href_link('blog.php', 'cPath=' . $new_parent_id . '&cID=' . $blog_categories_id));
          }
        }

        break;

      case 'move_product_confirm':

        $blog_content_id = osc_db_prepare_input($_POST['blog_content_id']);
        $new_parent_id = osc_db_prepare_input($_POST['move_to_category_id']);

        $QduplicateCheck = $OSCOM_PDO->prepare('select count(*)
                                                 from :table_blog_content_to_categories
                                                 where blog_content_id = :blog_content_id
                                                 and blog_categories_id  not in ( :blog_categories_id )
                                                ');
        $QduplicateCheck->bindInt(':blog_content_id', (int)$blog_content_id);
        $QduplicateCheck->bindInt(':blog_categories_id', (int)$new_parent_id);
        $QduplicateCheck->execute();

        if ($QduplicateCheck->rowCount()  < 1.01) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_blog_content_to_categories
                                          set blog_categories_id = :blog_categories_id
                                          where blog_content_id = :blog_content_id
                                          and blog_categories_id = :blog_categories_id1
                                        ');
          $Qupdate->bindInt(':blog_categories_id',(int)$new_parent_id);
          $Qupdate->bindInt(':blog_content_id',(int)$blog_content_id);
          $Qupdate->bindInt(':blog_categories_id1',(int)$current_category_id);

          $Qupdate->execute();
        }

        if (USE_CACHE == 'true') {
          osc_cache_reset('blog_tree-');
        }

        osc_redirect(osc_href_link('blog.php', 'cPath=' . $new_parent_id . '&pID=' . $blog_content_id));
        break;
// -----------------------------------------
// Gestion des produits
// -----------------------------------------
      case 'insert_product':
      case 'update_product':

        $blog_twitter_status = osc_db_prepare_input($_POST['blog_status_twitter']);

        if (isset($_GET['pID'])) $blog_content_id = osc_db_prepare_input($_GET['pID']);

          $blog_content_date_available = osc_db_prepare_input($_POST['blog_content_date_available']);
          $blog_content_date_available = (date('Y-m-d') < $blog_content_date_available) ? $blog_content_date_available : 'null';

          $sql_data_array = array('blog_content_date_available' => $blog_content_date_available,
                                  'blog_content_status' => osc_db_prepare_input($_POST['blog_content_status']),
                                  'admin_user_name'	 => osc_user_admin($user_administrator),
                                  'blog_content_sort_order' => osc_db_prepare_input($_POST['blog_content_sort_order']),
                                  'blog_content_author' => osc_db_prepare_input($_POST['blog_content_author']),
                                  'customers_group_id' => osc_db_prepare_input($_POST['customers_group_id'])
                                 );

//----------------------------------------------
// insert blog_content
//----------------------------------------------

          if ($action == 'insert_product') {
            $insert_sql_data = array('blog_content_date_added' => 'now()');

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            $OSCOM_PDO->save('blog_content', $sql_data_array );
            $blog_content_id = $OSCOM_PDO->lastInsertId();

            $OSCOM_PDO->save('blog_content_to_categories', ['blog_content_id' => (int)$blog_content_id,
                                                            'blog_categories_id' => (int)$current_category_id
                                                           ]
                              );

//----------------------------------------------
// update blog_content
//----------------------------------------------
          } elseif ($action == 'update_product') {
            $update_sql_data = array('blog_content_last_modified' => 'now()');
            $sql_data_array = array_merge($sql_data_array, $update_sql_data);

            $OSCOM_PDO->save('blog_content', $sql_data_array, ['blog_content_id' => (int)$blog_content_id ] );
          }

          $languages = osc_get_languages();

          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];

// Referencement

            $sql_data_array = array('blog_content_name' => osc_db_prepare_input($_POST['blog_content_name'][$language_id]),
                                    'blog_content_description' => osc_db_prepare_input($_POST['blog_content_description'][$language_id]),
                                    'blog_content_head_title_tag' => osc_db_prepare_input($_POST['blog_content_head_title_tag'][$language_id]),
                                    'blog_content_head_desc_tag' => osc_db_prepare_input($_POST['blog_content_head_desc_tag'][$language_id]),
                                    'blog_content_head_keywords_tag' => osc_db_prepare_input($_POST['blog_content_head_keywords_tag'][$language_id]),
                                    'blog_content_url' => osc_db_prepare_input($_POST['blog_content_url'][$language_id]),
                                    'blog_content_head_tag_product' => osc_db_prepare_input($_POST['blog_content_head_tag_product'][$language_id]),
                                    'blog_content_head_tag_blog' => osc_db_prepare_input($_POST['blog_content_head_tag_blog'][$language_id]),
                                    'blog_content_description_summary' => osc_db_prepare_input($_POST['blog_content_description_summary'][$language_id])
                                  );
                                   

            if ($action == 'insert_product') {
              $insert_sql_data = array('blog_content_id' => $blog_content_id,
                                       'language_id' => $language_id);

              $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

              $OSCOM_PDO->save('blog_content_description', $sql_data_array );
 
//update blog_content
            } elseif ($action == 'update_product') {

              $OSCOM_PDO->save('blog_content_description', $sql_data_array, ['blog_content_id' => (int)$products_id,
                                                                             'language_id' => (int)$language_id
                                                                            ]
                              );

            }
          } // end for

          $QblogContentTwitter = $OSCOM_PDO->prepare('select  pd.blog_content_name,
                                                                     p.blog_content_id,
                                                                     p.blog_content_status,
                                                                     p.customers_group_id
                                                             from :table_blog_content p,
                                                                  :table_blog_content_description pd
                                                             where p.blog_content_id = :blog_content_id
                                                             and p.blog_content_id = pd.blog_content_id
                                                             and pd.language_id = :language_id
                                                            ');
          $QblogContentTwitter->bindInt(':blog_content_id', (int)$blog_content_id);
          $QblogContentTwitter->bindInt(':language_id', (int)$_SESSION['languages_id']);
          $QblogContentTwitter->execute();

          $blog_twitter = $QblogContentTwitter->fetch();

          if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
            if ($blog_twitter_status == '1' || $blog_twitter['customers_group_id'] !='0' ) {

              $text_blog =  TEXT_NEW_BLOG_TWITTER . $blog_twitter['blog_content_name'] . ' ' . HTTP_SERVER . DIR_WS_CATALOG . 'blog_content.php?blog_content_id='. $blog_content_id;

              $_POST['twitter_msg'] =  $text_blog;

              echo osc_send_twitter($twitter_authentificate_administrator);
            }
          }

         osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' . $blog_content_id));
        break;

      case 'copy_to_confirm':
        if (isset($_POST['blog_content_id']) && isset($_POST['blog_categories_id'])) {
          $blog_content_id = osc_db_prepare_input($_POST['blog_content_id']);
          $blog_categories_id = osc_db_prepare_input($_POST['blog_categories_id']);

          if ($_POST['copy_as'] == 'link') {
            if ($blog_categories_id != $current_category_id) {

              $Qcheck = $OSCOM_PDO->prepare("select count(*) as total 
                                             from :table_blog_content_to_categories
                                             where blog_content_id = :blog_content_id
                                             and blog_categories_id = :blog_categories_id
                                            ");
              $Qcheck->bindInt(':blog_content_id', (int)$blog_content_id );
              $Qcheck->bindInt(':blog_categories_id', (int)$blog_categories_id );
              $Qcheck->execute();

              $check = $Qcheck->fetch();

              if ($check['total'] < '1') {

                $OSCOM_PDO->save('blog_content_to_categories', [
                                                    'blog_content_id' => (int)$blog_content_id,
                                                    'blog_categories_id' => (int)$blog_categories_id
                                                  ]
                              );

              }
            } else {
              $OSCOM_MessageStack->add_session(ERROR_CANNOT_LINK_TO_SAME_CATEGORY, 'error');
            }
          } elseif ($_POST['copy_as'] == 'duplicate') {
//Duplication des champs ou copie de la table product

            $QblogContent = $OSCOM_PDO->prepare('select blog_content_date_available, 
                                                             blog_content_status, 
                                                             admin_user_name,
                                                             blog_content_sort_order,
                                                             blog_content_author,
                                                             customers_group_id
                                           from :table_blog_content
                                           where blog_content_id = :blog_content_id
                                          ');
            $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id);
            $QblogContent->execute();

            $blog_content = $QblogContent->fetch();

            $OSCOM_PDO->save('blog_content', [
                                                'blog_content_date_added' => 'now()',
                                                'blog_content_date_available' => (empty($blog_content['blog_content_date_available']) ? "null" : "'" . $blog_content['blog_content_date_available']) . "'" ,
                                                'blog_content_status' =>  $blog_content['blog_content_status'],
                                                'admin_user_name' => osc_user_admin($user_administrator) ,
                                                'blog_content_sort_order' => $blog_content['blog_content_sort_order'],
                                                'blog_content_author' => $blog_content['blog_content_author'],
                                                'customers_group_id' => $blog_content['customers_group_id']
                                              ]
                           );


            $dup_blog_content_id = $OSCOM_PDO->lastInsertId();

// ---------------------
// referencement
// ----------------------
            $Qdescription = $OSCOM_PDO->prepare('select language_id,
                                                        blog_content_name,
                                                        blog_content_description,
                                                        blog_content_head_title_tag,
                                                        blog_content_head_desc_tag,
                                                        blog_content_head_keywords_tag,
                                                        blog_content_url,
                                                        blog_content_head_tag_product,
                                                        blog_content_head_tag_blog,
                                                        blog_content_description_summary
                                                 from :table_blog_content_description
                                                 where blog_content_id = :blog_content_id
                                               ');
            $Qdescription->bindint(':blog_content_id', (int)$blog_content_id);
            $Qdescription->execute();

            while ($description = $Qdescription->fetch() ) {

              $OSCOM_PDO->save('blog_content_description', [
                                                            'blog_content_id' => (int)$dup_blog_content_id,
                                                            'language_id' => (int)$description['language_id'],
                                                            'blog_content_name' => $description['blog_content_name'],
                                                            'blog_content_description' => $description['blog_content_description'],
                                                            'blog_content_head_title_tag' => $description['blog_content_head_title_tag'],
                                                            'blog_content_head_desc_tag' => $description['blog_content_head_desc_tag'],
                                                            'blog_content_head_keywords_tag' => $description['blog_content_head_keywords_tag'],
                                                            'blog_content_url' => $description['blog_content_url'],
                                                            'blog_content_viewed' => 0,
                                                            'blog_content_head_tag_product' =>  $description['blog_content_head_tag_product'],
                                                            'blog_content_head_tag_blog' => $description['blog_content_head_tag_blog'],
                                                            'blog_content_description_summary' => $description['blog_content_description_summary']
                                                          ]
                              );
            }

            $OSCOM_PDO->save('blog_content_to_categories', [
                                                            'blog_content_id' => (int)$dup_blog_content_id,
                                                            'blog_categories_id' => (int)$blog_categories_id
                                                          ]
                             );

            $blog_content_id = $dup_blog_content_id;
          }

          if (USE_CACHE == 'true') {
            osc_cache_reset('blog_tree-');
          }
        }

        osc_redirect(osc_href_link('blog.php', 'cPath=' . $blog_categories_id . '&pID=' . $blog_content_id));

        break;

// archive a product
      case 'archive_to_confirm':
          $blog_content_id = osc_db_prepare_input($_POST['blog_content_id']);
          $blog_categories_id = osc_db_prepare_input($_POST['blog_categories_id']);

          $Qupdate = $OSCOM_PDO->prepare('update :table_blog_content
                                          set blog_content_archive = :blog_content_archive
                                          where blog_content_id = :blog_content_id
                                        ');
          $Qupdate->bindInt(':blog_content_archive','1');
          $Qupdate->bindInt(':blog_content_id',(int)$blog_content_id);

          $Qupdate->execute();

// Mise a zero des stats
          $Qupdate = $OSCOM_PDO->prepare('update :table_blog_content_description
                                          set blog_content_viewed = :blog_content_viewed
                                          where blog_content_id = :blog_content_id
                                        ');
          $Qupdate->bindInt(':blog_content_viewed','0');
          $Qupdate->bindInt(':blog_content_id',(int)$blog_content_id);

          $Qupdate->execute();
/*
        if (USE_CACHE == 'true') {
          osc_cache_reset('blog_tree-');
        }
*/
        osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' . $blog_content_id));
     break;


// the images are not deleted in this case
// delete all product selected
      case 'delete_all':

       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $blog_contents['blog_content_id'] ) {
// Pour la suppression des produits voir la fonction osc_remove
//        if (isset($_POST['blog_content_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories'])) {

           if (isset($blog_contents['blog_content_id'])) {
             $blog_content_id = $blog_contents['blog_content_id'];

             $Qcheck = $OSCOM_PDO->prepare('select count(*) as total
                                               from :table_blog_content_to_categories
                                               where blog_content_id = :blog_content_id
                                              ');
             $Qcheck->bindInt(':blog_content_id', (int)$blog_content_id);

             $Qcheck->execute();

            if ($Qcheck->value('total') > 0 ) {
              osc_remove_blog_content($blog_content_id);
            }
          }
        } // end for each
      } // end post
/*
        if (USE_CACHE == 'true') {
          osc_cache_reset('blog_tree-');
        }
*/
      osc_redirect(osc_href_link('blog.php', 'cPath=' . $cPath));
      break;
    }
  }

// check if the catalog image directory exists
  if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!osc_is_writable(DIR_FS_CATALOG_IMAGES)) $OSCOM_MessageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
  } else {
    $OSCOM_MessageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
  }

  require('includes/header.php');
?>
<script type="text/javascript"><!--
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<script type="text/javascript"><!--
function popupFilename(url) {
  window.open(url,'popupFilename','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=900,height=600,screenX=550,screenY=25,top=150,left=500')
}
//--></script>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>

<?php
  //######################################################################################################## 
  //                                            EDITION CATEGORIE                                           
  //########################################################################################################
?>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top">
<!-- //######################################################################################################## //-->
<!-- //                                            EDITION CATEGORIE                                            //-->
<!-- //######################################################################################################## //-->
<?php 
  if ($_GET['action'] == 'new_category' || $_GET['action'] == 'edit_category') {
    if ( ($_GET['cID']) && (!$_POST) ) {

      $Qcategory = $OSCOM_PDO->prepare('select c.blog_categories_id, 
                                               cd.blog_categories_name, 
                                               cd.blog_categories_description, 
                                               cd.blog_categories_head_title_tag,
                                               cd.blog_categories_head_desc_tag,
                                               cd.blog_categories_head_keywords_tag,
                                               c.blog_categories_image, 
                                               c.parent_id, 
                                               c.sort_order, 
                                               c.date_added, 
                                               c.last_modified,
                                               c.customers_group_id
                                       from :table_blog_categories c, 
                                            :table_blog_categories_description cd
                                       where c.blog_categories_id = :blog_categories_id
                                       and c.blog_categories_id = cd.blog_categories_id 
                                       and cd.language_id = :language_id
                                       order by c.sort_order, 
                                                cd.blog_categories_name
                                     ');
      $Qcategory->bindInt(':blog_categories_id', (int)$_GET['cID']);
      $Qcategory->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qcategory->execute();

      $category = $Qcategory->fetch();

      $cInfo = new objectInfo($category);
    } else {
      $cInfo = new objectInfo(array());
    } 

    $languages = osc_get_languages();
    $form_action = (isset($_GET['cID'])) ? 'update_category' : 'insert_category'; 
    echo osc_draw_form('new_category', 'blog.php', 'cPath=' . $cPath . '&cID=' . $_GET['cID'] . '&action='.$form_action, 'post', 'enctype="multipart/form-data"');
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
          <div class="adminTitle">
            <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/blog.png', HEADING_TITLE, '40', '40'); ?></span>
            <span class="col-md-4 pageHeading pull-left"><?php echo '&nbsp;' . TABLE_HEADING_CATEGORIES; ?></span>
            <span class="pull-right"><?php echo osc_draw_hidden_field('categories_date_added', (($cInfo->date_added) ? $cInfo->date_added : date('Y-m-d'))) . osc_draw_hidden_field('parent_id', $cInfo->parent_id) . osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $_GET['cID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
          </div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        </div>
        <div class="clearfix"></div>
      </tr>
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' .  TAB_GENERAL . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' .  TAB_DESC . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' .  TAB_REF . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab4" role="tab" data-toggle="tab">' .  TAB_IMG . '</a>'; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
<?php
// -------------------------------------------------------------------
//          ONGLET General sur la description de la categorie
// -------------------------------------------------------------------
?>
                <div class="tab-pane active" id="tab1">
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo TEXT_CATEGORIES_NAME_TITLE; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                        <tr>
                          <td class="main" valign="middle"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                          <td class="main"><?php echo osc_draw_input_field('blog_categories_name[' . $languages[$i]['id'] . ']', (($blog_categories_name[$languages[$i]['id']]) ? $blog_categories_name[$languages[$i]['id']] : osc_get_blog_category_name($cInfo->blog_categories_id, $languages[$i]['id'])), 'class="form-control" required aria-required="true" required="" id="categories_blog_name" placeholder="' . TEXT_CATEGORIES_NAME . '"',  true) . '&nbsp;'; ?></td>
                        </tr>
<?php
    }
?>
                      </table></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                    </tr>
                    <tr>
                      <td class="mainTitle"><?php echo TEXT_DIVERS_TITLE; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
// Activation du module B2B
    if (MODE_B2B_B2C == 'true') {

    $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name
                                           from :table_customers_groups  
                                           where customers_group_id = :customers_group_id
                                           order by customers_group_id
                                           ');
    $QcustomersGroup->bindInt(':customers_group_id', (int)$category['customers_group_id']);
    $QcustomersGroup->execute();

    $customers_group = $QcustomersGroup->fetch();

    $customers_groups_id = $customers_group['customers_group_id'];
    $customers_groups_name = $customers_group['customers_group_name'];
?>
                      <tr>
                        <td class="main"><?php echo ENTRY_CUSTOMERS_GROUP_NAME; ?></td>
                        <td class="main"><?php echo osc_draw_pull_down_menu('customers_group_id', osc_get_customers_group(''. VISITOR_NAME .''), $cInfo->customers_group_id); ?></td>
                      </tr>

<?php
	}
?>
                      <tr>
                        <td class="main"><?php echo TEXT_EDIT_SORT_ORDER; ?></td>
                        <td class="main"><?php echo osc_draw_input_field('sort_order',$cInfo->sort_order, 'size="2"'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<?php
// ----------------------------------------------------------- //-->
//          ONGLET sur la designation de la categorie          //-->
// ----------------------------------------------------------- //-->
?>
              <div class="tab-pane" id="tab2">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TEXT_DESCRIPTION_CATEGORIES; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    for ($i=0; $i<sizeof($languages); $i++) {
?>
                      <tr>
                        <td class="main" valign="top"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                        <td class="main"><div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('blog_categories_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300', (isset($blog_categories_description[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($blog_categories_description[$languages[$i]['id']])) : osc_get_blog_category_description($cInfo->blog_categories_id, $languages[$i]['id'])));?></div></td>
                      </tr>
<?php
    }
?>
                      <tr>
                        <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                  <td><table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_DESCRIPTION); ?></td>
                      <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_DESCRIPTION; ?></strong></td>
                    </tr>
                  </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main">
                          <?php echo HELP_DESCRIPTION; ?>
                          <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg1"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                          <div class="modal fade" id="myModalWysiwyg1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                                </div>
                                <div class="modal-body" style="text-align:center;">
                                  <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<?php
  // -----------------------------------------------------//-->
  //          ONGLET sur le référencement categories      //-->
  // ---------------------------------------------------- //-->
?>
<!-- decompte caracteres -->
<script type="text/javascript">
  $(document).ready(function(){
    <?php
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
    ?>
    //default title
    $("#default_title_<?php echo $i?>").charCount({
      allowed: 70,
      warning: 20,
      counterText: ' Max : '
    });

    //default_description
    $("#default_description_<?php echo $i?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

    //default tag
    $("#default_tag_<?php echo $i?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '
    });
    <?php
       }
    ?>
  });
</script>
              <div class="tab-pane" id="tab3">
                <div class="col-md-12 mainTitle">
                  <span><?php echo TEXT_PRODUCTS_PAGE_REFEFRENCEMENT; ?></span>
                </div>


                <div class="adminformTitle">
                  <div class="spaceRow"></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-3"></span>
                      <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                      <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                    </div>
                  </div>


<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                  <div class="row">
                    <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_PAGE_TITLE; ?></span>
                      <span  class="col-md-6"><?php echo  '&nbsp;' . osc_draw_input_field('blog_categories_head_title_tag[' . $languages[$i]['id'] . ']', (($blog_categories_head_title_tag[$languages[$i]['id']]) ? $blog_categories_head_title_tag[$languages[$i]['id']] : osc_get_blog_categories_head_title_tag($cInfo->blog_categories_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?></span>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_HEADER_DESCRIPTION; ?></span>
                      <span  class="col-md-6"><?php echo '&nbsp;' .  osc_draw_textarea_field('blog_categories_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '2', (isset($blog_categories_head_desc_tag[$languages[$i]['id']]) ? $blog_categories_head_desc_tag[$languages[$i]['id']] : osc_get_blog_categories_head_desc_tag($cInfo->blog_categories_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></span>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <div class="col-md-12">
                      <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_KEYWORDS; ?></span>
                      <span  class="col-md-6"><?php echo  '&nbsp;' .  osc_draw_textarea_field('blog_categories_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '5', (isset($blog_categories_head_keywords_tag[$languages[$i]['id']]) ? $blog_categories_head_keywords_tag[$languages[$i]['id']] : osc_get_blog_categories_head_keywords_tag($cInfo->blog_categories_id, $languages[$i]['id']))); ?></span>
                    </div>
                  </div>
<?php
  }
?>
                </div>
                <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                <div class="adminformAide">
                  <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                        <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                      </span>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                  </div>
                </div>
              </div>
<?php
  // -----------------------------------------------------//-->
  //          ONGLET sur l'image de la categorie          //-->
  // ---------------------------------------------------- //-->
?>
              <div class="tab-pane" id="tab4">
                <div class="mainTitle">
                  <span><?php echo TEXT_CATEGORIES_IMAGE_TITLE; ?></span>
                </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_CATEGORIES_IMAGE_VIGNETTE, '40', '40'); ?></td>
                            <td class="main"><?php echo TEXT_CATEGORIES_IMAGE_VIGNETTE; ?></td>
                          </tr>
                          <tr>
                            <td class="main" colspan="2"><?php echo osc_draw_file_field_image_ckeditor('blog_categories_image', '300', '300', ''); ?></td>
                          </tr>
                        </table></td>
                        <td valign="top" width="80%"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td><table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                  <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_categories.gif', TEXT_CATEGORIES_IMAGE_VISUEL, '40', '40'); ?></td>
                                  <td class="main" align="left"><?php echo TEXT_CATEGORIES_IMAGE_VISUEL; ?></td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr align="center">
                            <td width="100%" align="center" valign="top"><table width="100%" border="0" class="adminformAide">
                                <tr>
                                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                </tr>
                                <tr>
                                  <td align="center"><?php echo osc_info_image($cInfo->blog_categories_image, TEXT_CATEGORIES_IMAGE_VIGNETTE); ?></td>
                                <tr>
                                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                </tr>
                                <tr>
                                  <td class="main" align="right" colspan="2"><?php echo TEXT_CATEGORIES_DELETE_IMAGE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_GENERAL); ?></td>
                            <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_GENERAL; ?></strong></td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td><table border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                            <td class="main"><?php echo HELP_IMAGE_CATEGORIES; ?></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table>
                  </tr>
                </table></td>
              </div>
           </div>
        </td>
      </tr>
  </table></form>
<?php
     } else if ($action == 'new_product') {
 // ################################################################################################################ -->
 //                                           EDITION OU NOUVEAU PRODUIT                                            -->
 //################################################################################################################ -->

    $parameters = array('blog_content_name' => '',
                        'blog_content_description' => '',
                        'blog_content_id' => '',
                        'blog_content_date_added' => '',
                        'blog_content_last_modified' => '',
                        'blog_content_date_available' => '',
                        'blog_content_status' => '',
                        'blog_content_sort_order' => '',
                        'blog_content_author' => '',
                        'customers_group_id' => '',
                        'blog_content_description_summary' => ''
                       );

    $pInfo = new objectInfo($parameters);

    if (isset($_GET['pID']) && empty($_POST)) {
// blog_content_view : Affichage Produit Grand Public - orders_view : Autorisation Commande - Referencement

      $QblogContent = $OSCOM_PDO->prepare('select pd.blog_content_name,
                                                   pd.blog_content_description,
                                                   pd.blog_content_head_title_tag,
                                                   pd.blog_content_head_desc_tag,
                                                   pd.blog_content_head_keywords_tag,
                                                   pd.blog_content_head_tag_product,
                                                   pd.blog_content_head_tag_blog,
                                                   p.blog_content_id,
                                                   p.blog_content_date_added,
                                                   p.blog_content_last_modified,
                                                   date_format(p.blog_content_date_available, "%d-%m-%Y") as blog_content_date_available,
                                                   p.blog_content_status,
                                                   p.blog_content_sort_order,
                                                   p.blog_content_author,
                                                   p.customers_group_id,
                                                  pd.blog_content_description_summary
                                           from :table_blog_content p,
                                                :table_blog_content_description pd
                                           where p.blog_content_id = :blog_content_id
                                           and p.blog_content_id = pd.blog_content_id
                                           and pd.language_id = :language_id
                                            ');
      $QblogContent->bindInt(':blog_content_id',  (int)$_GET['pID']);
      $QblogContent->bindInt(':language_id',  (int)$_SESSION['languages_id']);
      $QblogContent->execute();

      $blog_content = $QblogContent->fetch();

      $pInfo->objectInfo($blog_content);

    }

    $languages = osc_get_languages();

/*
    switch ($blog_content['blog_content_status']) {
      case '0': $in_status = false; $out_status = true; break;
      case '1':
      default: $in_status = true; $out_status = false;
    }		
*/

// Put languages information into an array for drop-down boxes
  $customers_group = osc_get_customers_group();

  $values_customers_group_id[0] = array ('id' =>'0', 'text' => TEXT_ALL_CUSTOMERS);

  for ($i=0, $n=sizeof($customers_group); $i<$n; $i++) {
    $values_customers_group_id[$i+1] = array ('id' =>$customers_group[$i]['id'], 'text' =>$customers_group[$i]['text']);
  }
  

    $form_action = (isset($_GET['pID'])) ? 'update_product' : 'insert_product';
?>
<!-- // Script de lancement du menu onglet tab-view //-->
<!-- // END - Script de lancement du menu onglet tab-view //-->

  <script type="text/javascript">
  $(function() {
    $('#blog_content_date_available').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd'
    });
  });
  </script>

    <?php echo osc_draw_form('new_product', 'blog.php', 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
          <div class="adminTitle">
            <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/blog_edit.png', HEADING_TITLE, '40', '40'); ?></span>
            <span class="col-md-6 pageHeading"><?php echo '&nbsp;' . sprintf(TEXT_NEW_PRODUCT, osc_output_generated_blog_category_path($current_category_id)); ?></span>
            <span class="pull-right"><?php echo osc_draw_hidden_field('blog_content_date_added', (osc_not_null($pInfo->blog_content_date_added) ? $pInfo->blog_content_date_added : date('Y-m-d'))) . osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
            <span class="pull-right"><?php echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
          </div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        </div>
        <div class="clearfix"></div>
      </tr>
      <tr>
        <td>
        <div>
          <ul class="nav nav-tabs" role="tablist"  id="myTab">
            <li class="active"><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' . TAB_GENERAL . '</a>'; ?></a></li>
            <li><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' . TAB_DESC; ?></a></li>
            <li><?php echo '<a href="' . substr(osc_href_link('blog.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' . TAB_REF; ?></a></li>
          </ul>
          <div class="tabsClicShopping">
            <div class="tab-content">
<?php
        // ---------------------------------------------------------------//-->
        //          ONGLET General sur les informations produits          //-->
        // -------------------------------------------------------------- //-->
?>
              <div class="tab-pane active" id="tab1">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TEXT_PRODUCTS_NAME; ?></td>
                    <td class="mainTitle" align="right"><?php echo TEXT_USER_NAME . osc_user_admin($user_administrator); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <tr>
                        <td class="main" valign="middle"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</td>
                        <td class="main"><?php echo  osc_draw_input_field('blog_content_name[' . $languages[$i]['id'] . ']', (isset($blog_content_name[$languages[$i]['id']]) ? $blog_content_name[$languages[$i]['id']] : osc_get_blog_content_name($pInfo->blog_content_id, $languages[$i]['id'])), 'class="form-control" required aria-required="true" required="" id="blog_name" placeholder="' . TEXT_PRODUCTS_NAME . '"',  true) . '&nbsp;'; ?></td>
                      </tr>
<?php
    }
?>

                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                  <tr>
                    <td class="mainTitle"><?php echo TEXT_PRODUCTS_PRESENTATION; ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo TEXT_EDIT_SORT_ORDER; ?></td>
                        <td class="main"><?php echo osc_draw_input_field('blog_content_sort_order',$pInfo->blog_content_sort_order, 'size="2"'); ?></td>
                      </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_AUTHOR; ?></td>
                        <td class="main"><?php echo osc_draw_input_field('blog_content_author', $pInfo->blog_content_author,'size="30"'); ?></td>
                      </tr>
<?php
// status
        if($blog_content['blog_content_status'] == '')  {
          $blog_content_status = '1';
          echo osc_draw_hidden_field('blog_content_status', $blog_content_status);
        } else {
          echo osc_draw_hidden_field('blog_content_status', $blog_content['blog_content_status']);
        }


// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?>
                      <tr>
                        <td class="main"><?php echo TEXT_CUSTOMERS_GROUP; ?></td>
                        <td class="main"><?php echo osc_draw_pull_down_menu('customers_group_id', $values_customers_group_id,  $pInfo->customers_group_id); ?></td>
                      </tr>
<?php
    }
    if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
?>
                      <tr>
                         <td class="main"><?php echo TEXT_PRODUCTS_TWITTER; ?></td>
                         <td class="main"><?php echo  osc_draw_radio_field('blog_status_twitter', '1', $pInfo->in_accept_twitter) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('blog_status_twitter', '0', $pInfo->out_accept_twitter) . '&nbsp;' . TEXT_NO; ?></td>
                      </tr>
<?php
    }
/*
                    <tr>
                      <td class="main"><?php echo TEXT_DATE_AVAILABLE; ?><br /><small>(YYYY-MM-DD)</small></td>
                      <td class="main"><?php echo osc_draw_input_field('blog_content_date_available', $pInfo->blog_content_date_available, 'id="blog_content_date_available"'); ?></td>
                    </tr>	
*/
?>
                    </table></td>
                  </tr>
                </table>
              </div>

<?php
// ------------------------------------ //-->
//          ONGLET Description          //-->
// ------------------------------------ //-->
?>
              <div class="tab-pane" id="tab2">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TEXT_PRODUCTS_DESCRIPTION; ?></td>
                  </tr>
                </table>
                <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
<script type='text/javascript' src='ext/ckeditor/ckeditor.js'></script>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <tr>
                        <div class="row">
                          <span class="col-md-2"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                          <span class="col-md-10">
                            <div style="visibility:visible; display:block;">
                              <?php echo osc_draw_textarea_ckeditor('blog_content_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300', (isset($blog_content_description[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($blog_content_description[$languages[$i]['id']])) : osc_get_blog_content_description($pInfo->blog_content_id, $languages[$i]['id']))); ?>
                            </div>
                          </span>
                        </div>
                      </tr>
                      <tr>
                        <div class="row"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                        <div>
                          <div>
                            <span class="col-md-12"><?php echo TEXT_PRODUCTS_DESCRIPTION_SUMMARY; ?></span>
                          </div>
                          <div class="row">
                            <span class="col-md-2"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                            <span class="col-md-7">
                              <?php echo osc_draw_textarea_field('blog_content_description_summary[' . $languages[$i]['id'] . ']', 'soft', '2', '3', (isset($blog_content_description_summary[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($blog_content_description_summary[$languages[$i]['id']])) : osc_get_blog_content_description_summary($pInfo->blog_content_id, $languages[$i]['id']))); ?>
                            </span>
                          </div>
                        </div>
                        <div class="row"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                      </tr>
<?php
    }
?>
                      <tr>
                        <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image(DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_DESCRIPTION); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_DESCRIPTION; ?></strong></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main">
                          <?php echo HELP_DESCRIPTION; ?><br />
                          <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg2"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                          <div class="modal fade" id="myModalWysiwyg2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                                </div>
                                <div class="modal-body" style="text-align:center;">
                                  <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<?php
// ----------------------------- //-->
//          ONGLET Referencement //-->
// ----------------------------- //-->
?>
              <div class="tab-pane" id="tab3">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td class="mainTitle"><?php echo TEXT_PRODUCTS_PAGE_REFEFRENCEMENT; ?></td>
                  </tr>
                </table>
                <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                  <tr>
                     <td colspan="3"><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                  </tr>
                  <tr>
                    <td class="text" align="right" ><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></td>
                    <td class="text" align="center"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td colspan="2">
<!-- decompte caracteres -->
<script type="text/javascript">
  $(document).ready(function(){
<?php
       for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>	
    //default title
    $("#default_title_<?php echo $i?>").charCount({
      allowed: 70,
      warning: 20,
      counterText: ' Max : '
    });

    //default_description
    $("#default_description_<?php echo $i?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

    //default tag product
    $("#default_tag_product_<?php echo $i?>").charCount({
      allowed: 100,
      warning: 70,
      counterText: ' Max : '
    });

    //default tag
    $("#default_tag_blog_<?php echo $i?>").charCount({
      allowed: 100,
      warning: 70,
      counterText: ' Max : '
    });
<?php
       }
?>	
  });
</script>

<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <tr>
                        <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                      </tr>
                      <tr>
                        <td class="main" valign="top"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']). '&nbsp '. TEXT_PRODUCTS_PAGE_TITLE; ?></td>
                        <td class="main"><?php echo  '&nbsp;' . osc_draw_input_field('blog_content_head_title_tag[' . $languages[$i]['id'] . ']', (($blog_content_head_title_tag[$languages[$i]['id']]) ? $blog_content_head_title_tag[$languages[$i]['id']] : osc_get_blog_content_head_title_tag($pInfo->blog_content_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?>&nbsp;
                      </tr>
                      <tr>
                        <td class="main" valign="top"><?php echo TEXT_PRODUCTS_KEYWORDS; ?></td>
                        <td class="main"><?php echo osc_draw_textarea_field('blog_content_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '5', (isset($blog_content_head_keywords_tag[$languages[$i]['id']]) ? $blog_content_head_keywords_tag[$languages[$i]['id']] : osc_get_blog_content_head_keywords_tag($pInfo->blog_content_id, $languages[$i]['id']))); ?>&nbsp;
                        </td>
                      </tr>
                      <tr>
                        <td class="main" valign="top"><?php echo TEXT_PRODUCTS_HEADER_DESCRIPTION; ?></td>
                        <td class="main"><?php echo osc_draw_textarea_field('blog_content_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '2', (isset($blog_content_head_desc_tag[$languages[$i]['id']]) ? $blog_content_head_desc_tag[$languages[$i]['id']] : osc_get_blog_content_head_desc_tag($pInfo->blog_content_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></td>
                      </tr>
                      <tr>
                       <td class="main" valign="top"><?php echo TEXT_PRODUCTS_TAG_PRODUCT; ?></td>
                       <td class="main"><?php echo  '&nbsp;' . osc_draw_input_field('blog_content_head_tag_product[' . $languages[$i]['id'] . ']', (($blog_content_head_tag_product[$languages[$i]['id']]) ? $blog_content_head_tag_product[$languages[$i]['id']] : osc_get_blog_content_tag_product($pInfo->blog_content_id, $languages[$i]['id'])),'maxlength="100" size="77" id="default_tag_product_'.$i.'"', false); ?>&nbsp;
                      </tr>
                      <tr>
                       <td class="main" valign="top"><?php echo TEXT_PRODUCTS_TAG_BLOG; ?></td>
                       <td class="main"><?php echo  '&nbsp;' . osc_draw_input_field('blog_content_head_tag_blog[' . $languages[$i]['id'] . ']', (($blog_content_head_tag_blog[$languages[$i]['id']]) ? $blog_content_head_tag_blog[$languages[$i]['id']] : osc_get_blog_content_tag_blog($pInfo->blog_content_id, $languages[$i]['id'])),'maxlength="100" size="77" id="default_tag_blog_'.$i.'"', false); ?>&nbsp;
                      </tr>

<?php
    }
?>
                    </td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                  </tr>
                </table>
                <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?></td>
                        <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                        <td class="main"><?php echo HELP_SUBMIT; ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<!-- fin produit -->
            </div>
          </div>
        </div>
      </td>
    </tr>
  </table></form>

<?php
    } else {
 // ################################################################################################################ -->
 //                                         LISTING PRODUITS ET CATEGORIES                                          -->
 // ################################################################################################################ -->
?>

    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/blog.png', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right">
                      <div class="form-group">
                        <div class="controls">
<?php
  echo osc_draw_form('search', 'blog.php', '', 'get');
  echo  osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');
  echo osc_hide_session_id();
?>
                          </form>
                        </div>
                      </div>
                     </td>
                  </tr>
                  <tr>
                    <td align="right" class="smallText"><?php echo osc_draw_form('goto', 'blog.php', '', 'get') . HEADING_TITLE_GOTO . ' ' . osc_draw_pull_down_menu('cPath', osc_get_blog_category_tree(), $current_category_id, 'onchange="this.form.submit();"'); ?>
                      <?php echo osc_hide_session_id() . '</form>'; ?>
                    </td>
                  </tr>
                </table></td>
                <td align="right" class="pageHeading"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
<!-- Deplacement du bouton retour -->
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
                    <td align="right" class="smallText">  
<?php			  
  echo '<a href="' . osc_href_link('blog.php', $cPath_back . 'cID=' . $current_category_id) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>&nbsp;';

  if (!isset($_GET['search'])) {
    echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&action=new_category') . '">' . osc_image_button('button_new_category.gif', IMAGE_NEW_CATEGORY) . '</a>&nbsp;';
    echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&action=new_product') . '">' . osc_image_button('button_new_blog.gif', IMAGE_NEW_BLOG) . '</a>';
  }
?>
                     <form name="delete_all" action="<?php echo  osc_href_link('blog.php', 'cPath=' . $cPath . '&action=delete_all'); ?>" method="post"><a onClick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
                    </td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
           <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
<!-- // select all the product to delete -->
                <td width="1" style="text-align: center;"><input type="checkbox" onClick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="dataTableHeadingContent" colspan="4">&nbsp;</td>
                <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_CATEGORIES_PRODUCTS; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?> 
                <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_CUSTOMERS_GROUPS; ?></td>
<?php
  }
?> 
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAST_MODIFIED; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CREATED; ?>&nbsp;</td>	
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SORT_ORDER; ?>&nbsp;</td>	
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $blog_categories_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $search = osc_db_prepare_input($_GET['search']);
// Recherche dans les categories
      $Qcategories= $OSCOM_PDO->prepare('select c.blog_categories_id,
                                               cd.blog_categories_name,
                                               c.blog_categories_image,
                                               c.parent_id,
                                               c.sort_order,
                                               c.date_added,
                                               c.last_modified,
                                               c.customers_group_id
                                        from :table_blog_categories c,
                                             :table_blog_categories_description cd
                                        where c.blog_categories_id = cd.blog_categories_id
                                        and cd.language_id = :language_id
                                        and cd.blog_categories_name like :search
                                        order by c.sort_order,
                                                 cd.blog_categories_name
                                        ');
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qcategories->bindValue(':search', '%'.$search.'%');
      $Qcategories->execute();

    } else {

      $Qcategories= $OSCOM_PDO->prepare('select c.blog_categories_id,
                                               cd.blog_categories_name,
                                               c.blog_categories_image,
                                               c.parent_id,
                                               c.sort_order,
                                               c.date_added,
                                               c.last_modified,
                                               c.customers_group_id
                                        from :table_blog_categories c,
                                             :table_blog_categories_description cd
                                        where c.parent_id = :parent_id
                                        and c.blog_categories_id = cd.blog_categories_id
                                        and cd.language_id = :language_id
                                        order by c.sort_order,
                                                 cd.blog_categories_name
                                        ');

      $Qcategories->bindInt(':parent_id', (int)$current_category_id );
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qcategories->execute();

    }

    while ($categories = $Qcategories->fetch() ) {
      $blog_categories_count++;
      $rows++;

// Get parent_id for subcategories if search
      if (isset($_GET['search'])) $cPath= $categories['parent_id'];

      if ((!isset($_GET['cID']) && !isset($_GET['pID']) || (isset($_GET['cID']) && ($_GET['cID'] == $categories['blog_categories_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
        $category_childs = array('childs_count' => osc_childs_in_blog_category_count($categories['blog_categories_id']));
        $category_blog_content = array('blog_content_count' => osc_get_blog_content_in_category_count($categories['blog_categories_id']));

        $cInfo_array = array_merge($categories, $category_childs, $category_blog_content);
        $cInfo = new objectInfo($cInfo_array);
      }

      if (isset($cInfo) && is_object($cInfo) && ($categories['blog_categories_id'] == $cInfo->blog_categories_id) ) {
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('blog.php', osc_get_path($categories['blog_categories_id'])) . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $categories['blog_categories_id']) . '\'">' . "\n";
      }

// Permettre l'affichage des groupes en mode B2B
     if (MODE_B2B_B2C == 'true') {

       $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name
                                               from :table_customers_groups
                                               where customers_group_id = :customers_group_id
                                             ');
       $QcustomersGroup->bindint(':customers_group_id',(int)$categories['customers_group_id']);
       $QcustomersGroup->execute();

       $customers_group = $QcustomersGroup->fetch();

       if ($customers_group['customers_group_name'] == '') {
          $customers_group['customers_group_name'] =  NORMAL_CUSTOMER;
       }
     }

?>
                   <td class="dataTableContent" align="center">&nbsp;</td> 
                   <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('blog.php', osc_get_path($categories['blog_categories_id'])) . '">' . osc_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER); ?></td>
                   <td class="dataTableContent" colspan="3">&nbsp;</td>
                   <td class="dataTableContent" align="left"><?php echo '<strong>' . $categories['blog_categories_name'] . '</strong>'; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?> 

                 <td class="dataTableContent" align="left"><?php echo $customers_group['customers_group_name']; ?></td>
<?php
  }
?>
                 <td class="dataTableContent" align="center">&nbsp;</td>
                 <td class="dataTableContent" align="center">&nbsp;</td>
                 <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($categories['last_modified']); ?></td>
                 <td class="dataTableContent" align="center">&nbsp;</td>
                 <td class="dataTableContent" align="center"><?php echo $categories['sort_order']; ?></td>
                 <td class="dataTableContent" align="right">
<?php
    echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $categories['blog_categories_id'] . '&action=edit_category') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>' ;
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $categories['blog_categories_id'] . '&action=move_category') . '">' . osc_image(DIR_WS_ICONS . 'move.gif', IMAGE_MOVE) . '</a>' ;
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $categories['blog_categories_id'] . '&action=delete_category') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>' ;
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    if (isset($cInfo) && is_object($cInfo) && ($categories['blog_categories_id'] == $cInfo->blog_categories_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $categories['blog_categories_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                  </td>
              </tr>
<?php
  }
 // ################################################################################################################ -->
 //                                         LISTING PRODUITS                                          -->
 //################################################################################################################ -->
 
    $blog_content_count = 0;

// Recherche des produits
    if (isset($_GET['search'])) {

      $QblogContent = $OSCOM_PDO->prepare('select p.blog_content_id,
                                             pd.blog_content_name,
                                             p.blog_content_date_added,
                                             p.blog_content_last_modified,
                                             p.blog_content_date_available,
                                             p.blog_content_status,
                                             p.admin_user_name,
                                             p2c.blog_categories_id,
                                             p.blog_content_sort_order,
                                             p.blog_content_author
                                       from :table_blog_content p,
                                            :table_blog_content_description pd,
                                            :table_blog_content_to_categories p2c
                                       where p.blog_content_id = pd.blog_content_id
                                       and pd.language_id = :language_id
                                       and p.blog_content_id = p2c.blog_content_id
                                       and p.blog_content_archive = 0
                                       and pd.blog_content_name like :search
                                       order by pd.blog_content_name
                                    ');
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindValue(':search', '%'.$search.'%');
      $QblogContent->execute();


    } else {

      $QblogContent = $OSCOM_PDO->prepare('select p.blog_content_id,
                                                   pd.blog_content_name,
                                                   p.blog_content_date_added,
                                                   p.blog_content_last_modified,
                                                   p.blog_content_date_available,
                                                   p.blog_content_status,
                                                   p.admin_user_name,
                                                   p.blog_content_sort_order,
                                                   p.blog_content_author,
                                                   p.customers_group_id
                                             from blog_content p,
                                                  blog_content_description pd,
                                                  blog_content_to_categories p2c
                                             where p.blog_content_id = pd.blog_content_id
                                             and pd.language_id = :language_id
                                             and p.blog_content_id = p2c.blog_content_id
                                             and p2c.blog_categories_id = :blog_categories_id
                                             and p.blog_content_archive = 0
                                             order by pd.blog_content_name
                                        ');
      $QblogContent->bindInt(':blog_categories_id', (int)$current_category_id  );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $QblogContent->execute();

    }

    while ($blog_content = $QblogContent->fetch() ) {
    
      $blog_content_count++;
      $rows++;

// Permettre l'affichage des groupes en mode B2B
     if (MODE_B2B_B2C == 'true') {

       $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name
                                               from :table_customers_groups
                                               where customers_group_id = :customers_group_id
                                             ');
       $QcustomersGroup->bindint(':customers_group_id', (int)$blog_content['customers_group_id']);
       $QcustomersGroup->execute();

       $customers_group = $QcustomersGroup->fetch();

       if ($customers_group['customers_group_name'] == '') {
          $customers_group['customers_group_name'] =  NORMAL_CUSTOMER;
       }
     }


// Get blog_categories_id for product if search
      if (isset($_GET['search'])) $cPath = $blog_content['blog_categories_id'];

      if (isset($pInfo) && is_object($pInfo) && ($blog_content['blog_content_id'] == $pInfo->blog_content_id) ) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }


?>
               <td>
<?php // select all the product to delete 
        if ($blog_content['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $blog_content['blog_content_id']; ?>" checked="checked" />
<?php 
        } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $blog_content['blog_content_id']; ?>" />
<?php 
        }
?>
                </td>
<?php
      if ((isset($blog_content['blog_content_id'])) && $blog_content['blog_content_status'] == '1') {
?>
                     <td class="dataTableContent" width="20px;"><?php echo '<a href="' . DIR_WS_CATALOG . 'blog_content.php?blog_content_id=' . $blog_content['blog_content_id'], '" target="_blank">' . osc_image(DIR_WS_ICONS . 'preview_catalog.png', ICON_PREVIEW_CATALOG) . '</a>'; ?></td>
<?php
        } else {
?>
                      <td class="dataTableContent"></td>
<?php
        }
?>
                <td class="dataTableContent"></td>
                <td class="dataTableContent" width="20px;"><?php //echo '<a href="' . osc_href_link('products_preview.php' . '?pID=' . $blog_content['blog_content_id']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>'; ?></td>
                <td class="dataTableContent"><?php //echo  osc_image(DIR_WS_CATALOG_IMAGES . $blog_content['blog_content_image'], $blog_content['blog_content_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                <td class="dataTableContent"><?php echo $blog_content['blog_content_name']; ?></td>
<?php
// Permettre l'affichage des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
?> 
                <td class="dataTableContent"><?php echo $customers_group['customers_group_name']; ?></td>
<?php
  }
?>
                <td class="dataTableContent" align="center">
<?php
      if ($blog_content['blog_content_status'] == '1') {
        echo '<a href="' . osc_href_link('blog.php', 'action=setflag&flag=0&pID=' . $blog_content['blog_content_id'] . '&cPath=' . $cPath) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('blog.php', 'action=setflag&flag=1&pID=' . $blog_content['blog_content_id'] . '&cPath=' . $cPath) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="center"></td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($blog_content['blog_content_last_modified']); ?></td>
                <td class="dataTableContent" align="center"><?php echo $blog_content['admin_user_name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $blog_content['blog_content_sort_order']; ?></td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' .  $blog_content['blog_content_id'] . '&action=new_product') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' .  $blog_content['blog_content_id'] . '&action=move_product') . '">' . osc_image(DIR_WS_ICONS . 'move.gif', IMAGE_MOVE) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' .  $blog_content['blog_content_id'] . '&action=copy_to') . '">' . osc_image(DIR_WS_ICONS . 'copy.gif', IMAGE_COPY_TO) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' .  $blog_content['blog_content_id'] . '&action=archive') . '">' . osc_image(DIR_WS_ICONS . 'archive.gif', IMAGE_ARCHIVE_TO) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' .  $blog_content['blog_content_id'] . '&action=delete_product') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($pInfo) && is_object($pInfo) && ($blog_content['blog_content_id'] == $pInfo->blog_content_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' . $blog_content['blog_content_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    }

    $cPath_back = '';
    if (isset($cPath_array) && sizeof($cPath_array) > 0) {
      for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
        if (empty($cPath_back)) {
          $cPath_back .= $cPath_array[$i];
        } else {
          $cPath_back .= '_' . $cPath_array[$i];
        }
      }
    }

    $cPath_back = (osc_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
             <tr> 
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo TEXT_CATEGORIES . '&nbsp;' . $blog_categories_count . '<br />' . TEXT_PRODUCTS . '&nbsp;' . $blog_content_count; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
</form>
<?php
    $heading = array();
    $contents = array();

    switch ($action) {

      case 'delete_category':

        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CATEGORY . '</strong>');

        $contents = array('form' => osc_draw_form('categories', 'blog.php', 'action=delete_category_confirm&cPath=' . $cPath) . osc_draw_hidden_field('blog_categories_id', $cInfo->blog_categories_id));
        $contents[] = array('text' => TEXT_DELETE_CATEGORY_INTRO);
        $contents[] = array('text' => '<br /><strong>' . $cInfo->blog_categories_name . '</strong>');
        if ($cInfo->childs_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_CHILDS, $cInfo->childs_count));
        if ($cInfo->blog_content_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_PRODUCTS, $cInfo->blog_content_count));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $cInfo->blog_categories_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');;
      break;

      case 'move_category':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_CATEGORY . '</strong>');

        $contents = array('form' => osc_draw_form('categories', 'blog.php', 'action=move_category_confirm&cPath=' . $cPath) . osc_draw_hidden_field('blog_categories_id', $cInfo->blog_categories_id));
        $contents[] = array('text' => sprintf(TEXT_MOVE_CATEGORIES_INTRO, $cInfo->blog_categories_name));
        $contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $cInfo->blog_categories_name) . '<br />' . osc_draw_pull_down_menu('move_to_category_id', osc_get_blog_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_move.gif', IMAGE_MOVE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&cID=' . $cInfo->blog_categories_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'delete_product':

        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</strong>');

        $contents = array('form' => osc_draw_form('products', 'blog.php', 'action=delete_product_confirm&cPath=' . $cPath) . osc_draw_hidden_field('blog_content_id', $_GET['pID']));
        $contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);

        $blog_content_categories_string = '';
        $blog_content_categories = osc_generate_blog_category_path($_GET['pID'], 'product');

        for ($i = 0, $n = sizeof($blog_content_categories); $i < $n; $i++) {
          $category_path = '';
          for ($j = 0, $k = sizeof($blog_content_categories[$i]); $j < $k; $j++) {
            $category_path .= $blog_content_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
          }
          $category_path = substr($category_path, 0, -16);
          $blog_content_categories_string .= osc_draw_checkbox_field('product_categories[]', $blog_content_categories[$i][sizeof($blog_content_categories[$i])-1]['id'], true) . '&nbsp;' . $category_path . '<br />';
        }
        $blog_content_categories_string = substr($blog_content_categories_string, 0, -4);

        $contents[] = array('text' => '<br />' . $blog_content_categories_string);

        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' . $_GET['pID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'move_product':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_PRODUCT . '</strong>');

        $contents = array('form' => osc_draw_form('blog_content', 'blog.php', 'action=move_product_confirm&cPath=' . $cPath) . osc_draw_hidden_field('blog_content_id', $_GET['pID']));
//        $contents[] = array('text' => sprintf(TEXT_MOVE_PRODUCTS_INTRO, $pInfo->blog_content_name));
        $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . osc_output_generated_blog_category_path($_GET['pID'], 'product') . '</strong>');
        $contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $pInfo->blog_content_name) . '<br />' . osc_draw_pull_down_menu('move_to_category_id', osc_get_blog_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_move.gif', IMAGE_MOVE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' . $_GET['pID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
        break;

      case 'copy_to':

        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');
        $contents = array('form' => osc_draw_form('copy_to', 'blog.php', 'action=copy_to_confirm&cPath=' . $cPath) . osc_draw_hidden_field('blog_content_id',$_GET['pID']));
        $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
        $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . osc_output_generated_blog_category_path($_GET['pID'], 'product') . '</strong>');
        $contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . osc_draw_pull_down_menu('blog_categories_id', osc_get_blog_category_tree(), $current_category_id));
        $contents[] = array('text' => '<br />' . TEXT_HOW_TO_COPY . '<br />' . osc_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_COPY_AS_LINK . '<br />' . osc_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE);
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_copy.gif', IMAGE_COPY) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' .$_GET['pID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'archive':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_ARCHIVE . '</strong>');
        $contents = array('form' => osc_draw_form('archive', 'blog.php', 'action=archive_to_confirm&cPath=' . $cPath) . osc_draw_hidden_field('blog_content_id', $_GET['pID']));
        $contents[] = array('text' => TEXT_INFO_ARCHIVE_INTRO);
//         $contents[] = array('text' => '<br /><strong>' . $pInfo->blog_content_name . '</strong><br />');
        $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . osc_output_generated_blog_category_path($_GET['pID'], 'product') . '</strong>');
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_archive.gif', IMAGE_ARCHIVE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('blog.php', 'cPath=' . $cPath . '&pID=' . $_GET['pID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

        default:
        break;
    }

    if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
    </table>
<?php
  }
?>
    </td>
  </tr>
</table>
<!-- body_eof //-->
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
