<?php
  /*
  * edit_template_products.php
  * @copyright Copyright 2008 - http://www.e-imaginis.com
  * @copyright Portions Copyright 2003 osCommerce
  * @license GNU Public License V2.0
  * @version $Id:
  */


  define('HEADING_TITLE','Editeur Template produits');

  define('TEXT_SELECT_FILES','Sélection votre template : ');
  define('TITLE_HELP_EDIT_TEMPLATE_PRODUCTS_IMAGE','Aide sur l\'éditeur du Template produits');
  define('TITLE_HELP_EDIT_TEMPLATE_PRODUCTS', 'Aide sur l\'éditeur du Template produits');
  define('TEXT_HELP_EDIT_TEMPLATE_PRODUCTS','
Les répertoires qui ont un template:<br /><br />
- modules_frontpage<br />
- modules_categories_Index<br />
- modules_products_listing<br />
- modules_products_info<br />
- modules_products_heart<br />
- modules_products_new<br />
- modules_specials<br /><br />

  Pour des raisons de sécurit&eacute, les permissions sont d\'origine non permises dans l\'éditeur de Template. Il est important de comprendre
  que changer les permissions des fichiers peut entrainer un problème de sécurité de votre application. Il est préférable de télécharger votre fichier par FTP et de le changer sur votre ordinateur<br />
  Dans le cas ou vous souhaiteriez réaliser des opérations de modification des fichiers à partir de l\'éditeur du css, vous devez suivre cette procédure.<br />
  - Droits sur les répertoires : chmod 755<br />
  - Droits sur les fichiers : chmod 666</br />
  - Après vos modifications, veuillez changer vos droits sur les fichiers en chmod 644.
  - Veuillez noter que le configuration de votre serveur aura une influence sur les autorisations que vous aurez droit de faire ou pas. Nous vous invitons à consulter dans <a href="http://clicshopping.org/marketplace/blog.php" target="_blank">notre guide</a>
  pour de plus amples explications.');

?>