<?php
  /*
   * edit_template_products.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */
  set_time_limit(0);


  require('includes/application_top.php');


  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {

      case 'directory':

        if (isset($_POST['directory_html'])) {
          $directory_selected = $_POST['directory_html'];
        } else {
          $directory_selected = $_GET['directory_html'];
        }

        if (isset($_POST['filename'])) {
          $filename_selected = $_POST['filename'];
        } else {
          $filename_selected = $_GET['filename'];
        }

        $file = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/'.$directory_selected.'/template_html/'.$filename_selected;

        if(file_exists($file)) {
           $code = file_get_contents($file);
/*
           $code = preg_replace('@<script[^>]*?>.*?</script>@si', '', $code);
//          $code = preg_replace("/>/","&gt;",$code);
           $code = preg_replace("/</","&lt;",$code);
*/
        }

       break;

      case 'update':

        $directory_selected =  $_POST['directory_html'];
        $filename_selected = $_POST['filename'];
        $code = $_POST['code'];

        if (file_exists(DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/'. $directory_selected .'/template_html/'. $filename_selected)) {
          $filename = DIR_FS_DOCUMENT_ROOT . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/'. $directory_selected .'/template_html/'. $filename_selected;
        } else {
          $OSCOM_MessageStack->add_session(ERROR_FILE_DOES_NOT_EXIST, 'error');
        }

        if  (osc_is_writable($filename)) {
          $file = new SplFileObject($filename, "w");
          $written = $file->fwrite($code);
          $OSCOM_MessageStack->add_session(SUCCESS_FILE_SAVED_SUCCESSFULLY, 'success');
        } else {
          $OSCOM_MessageStack->add_session(ERROR_FILE_NOT_WRITEABLE, 'error');
        }

        osc_redirect(osc_href_link('edit_template_products.php', 'action=directory&directory_html=' . $directory_selected .'&filename=' . $filename_selected));
        break;
    }
  }

  require('includes/header.php');
?>
<!-- body //-->
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
            <tr>
              <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/edit_template.png', HEADING_TITLE, '40', '40'); ?></td>
              <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
<?php
  if (empty($action)) {
    $form_action = 'directory';
?>
              <td align="center"><?php echo  osc_draw_form('directory', 'edit_template_products.php', 'action=' . $form_action, 'post', 'enctype="multipart/form-data"') . osc_draw_pull_down_menu('directory_html', osc_directory_template_products(),  $directory_selected, 'onchange="this.form.submit();"');?></form></td>
<?php
  } else {
?>

              <td align="center">
<?php
  echo  osc_draw_form('edit_file_html', 'edit_template_products.php', 'action=directory', 'post') . osc_draw_pull_down_menu('directory_html', osc_directory_template_products(),  $directory_selected, 'onchange="this.form.submit();"') . '      ' .osc_draw_pull_down_menu('filename', osc_filename_template_products(), $filename_selected, 'onchange="this.form.submit();"');
?>
              </form></td>
              <td align="right">
<?php
  echo osc_draw_form('area_html', 'edit_template_products.php', 'action=update', 'post', 'enctype="multipart/form-data"');
  echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>

              </td>
<?php
  }
?>
           </tr>
          </table></td>
        </tr>
<?php
   if ($action == 'directory') {
?>
        <tr>
          <td>
  <style>.CodeMirror {background: #f8f8f8;}</style>
  <link rel="stylesheet" href="ext/codemirror/lib/codemirror.css">
  <script src="ext/codemirror/lib/codemirror-compressed.js"></script>
  <script src="ext/codemirror/mode/css/css-compressed.js"></script>
  <script src="ext/codemirror/addon/selection/active-line-compressed.js"></script>
  <style type="text/css">
    .CodeMirror {
      border-top: 1px solid #eee;
      border-bottom: 1px solid #eee;
      height: auto;
    }
    .CodeMirror-scroll {
      overflow-y: hidden;
      overflow-x: auto;
    }
  </style>
<?php
                echo osc_draw_hidden_field('directory_html', $directory_selected);
                echo osc_draw_hidden_field('filename', $filename_selected);
                echo  osc_draw_textarea_field('code', '', '', '', $code,'id="code"');
?>


  <script>
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
      styleActiveLine: true,
      lineNumbers: true,
      lineWrapping: true,
      viewportMargin: Infinity
    });
  </script>
          </td>
        </tr>
<?php
  }
?>
        <tr>
          <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
          </table>
          <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
            <tr>
              <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_EDIT_TEMPLATE_PRODUCTS_IMAGE); ?></td>
                    <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_EDIT_TEMPLATE_PRODUCTS; ?></strong></td>
                  </tr>
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                    <td class="main"><?php echo TEXT_HELP_EDIT_TEMPLATE_PRODUCTS; ?></td>
                  </tr>
                </table></td>
            </tr>
          </table>
        </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php
  require('includes/footer.php');
  require('includes/application_bottom.php');
?>
