<?php
/**
 * countires.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {

     case 'setflag':
        osc_set_countries_status($_GET['id'], $_GET['flag']);
        osc_redirect(osc_href_link('countries.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'cID=' . $_GET['id']));
        break;	

      case 'insert':
        $countries_name = osc_db_prepare_input($_POST['countries_name']);
        $countries_iso_code_2 = osc_db_prepare_input($_POST['countries_iso_code_2']);
        $countries_iso_code_3 = osc_db_prepare_input($_POST['countries_iso_code_3']);
        $address_format_id = osc_db_prepare_input($_POST['address_format_id']);

        osc_db_query("insert into countries (countries_name,
        $OSCOM_PDO->save('countries', [
                                        'countries_name' => $countries_name,
                                        'countries_iso_code_2' => $countries_iso_code_2,
                                        'countries_iso_code_3' => $countries_iso_code_3,
                                        'address_format_id' => (int)$address_format_id,
                                        'status' => 1
                                      ]
                          );

        osc_redirect(osc_href_link('countries.php'));
        break;
      case 'save':
        $countries_id = osc_db_prepare_input($_GET['cID']);
        $countries_name = osc_db_prepare_input($_POST['countries_name']);
        $countries_iso_code_2 = osc_db_prepare_input($_POST['countries_iso_code_2']);
        $countries_iso_code_3 = osc_db_prepare_input($_POST['countries_iso_code_3']);
        $address_format_id = osc_db_prepare_input($_POST['address_format_id']);
        $countries_status = osc_db_prepare_input($_POST['status']);

        $Qupdate = $OSCOM_PDO->prepare('update :table_countries 
                                        set countries_name = :countries_name, 
                                        countries_iso_code_2 = :countries_iso_code_2,
                                        countries_iso_code_3 = :countries_iso_code_3,
                                        address_format_id = :address_format_id,
                                        status = :status
                                        where countries_id = :countries_id
                                      ');
        $Qupdate->bindValue(':countries_name', $countries_name);
        $Qupdate->bindValue(':countries_iso_code_2', $countries_iso_code_2);
        $Qupdate->bindValue(':countries_iso_code_3', $countries_iso_code_3);
        $Qupdate->bindInt(':address_format_id',(int)$address_format_id);
        $Qupdate->bindInt(':status', $countries_status);
        $Qupdate->bindInt(':countries_id', (int)$countries_id);
        $Qupdate->execute();

        osc_redirect(osc_href_link('countries.php', 'page=' . $_GET['page'] . '&cID=' . $countries_id));
      break;

      case 'deleteconfirm':
        $countries_id = osc_db_prepare_input($_GET['cID']);

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_countries
                                        where countries_id = :countries_id
                                      ');
        $Qdelete->bindInt(':countries_id', (int)$countries_id);
        $Qdelete->execute();

        osc_redirect(osc_href_link('countries.php', 'page=' . $_GET['page']));
      break;

      case 'update_all':

       if ($_POST['selected'] != '') { 
          foreach ($_POST['selected'] as $countries['countries_id']) {

            $Qupdate = $OSCOM_PDO->prepare('update :table_countries  
                                           set status = :status
                                           where countries_id = :countries_id
                                          ');
            $Qupdate->bindInt(':status', '0');
            $Qupdate->bindInt(':countries_id', (int)$countries['countries_id']);
            $Qupdate->execute();
          }
       }
        osc_redirect(osc_href_link('countries.php', 'page=' . $_GET['page']));
      break;
    }
  }

  require('includes/header.php');

  $countries_query_raw = "select countries_id,
                                 countries_name,
                                 countries_iso_code_2,
                                 countries_iso_code_3,
                                 status,
                                 address_format_id
                          from countries
                          order by countries_name
                         ";
  $countries_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $countries_query_raw, $countries_query_numrows);
  $countries_query = osc_db_query($countries_query_raw);

?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/countries.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
<?php
  if (empty($action)) {
?>

            <td class="smallText" valign="middle" align="center">
              <?php echo $countries_split->display_count($countries_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_COUNTRIES); ?>
              <?php echo $countries_split->display_links($countries_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
            </td>
            <td class="pageHeading" align="right">
              <?php echo '<a href="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_country.gif', IMAGE_NEW_COUNTRY) . '</a>'; ?>
              <form name="update_all" <?php echo 'action="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&action=update_all') . '"'; ?> method="post">
              <a onclick="$('update').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_countries_deactivate.gif', IMAGE_UPDATE); ?></span></a>&nbsp;
            </td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>              
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_COUNTRY_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center" colspan="2"><?php echo TABLE_HEADING_COUNTRY_CODES; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php

  
  while ($countries = osc_db_fetch_array($countries_query)) {
    if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $countries['countries_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
      $cInfo = new objectInfo($countries);
    }

    if (isset($cInfo) && is_object($cInfo) && ($countries['countries_id'] == $cInfo->countries_id)) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    }
?>
                <td>
<?php 
      if ($countries['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $countries['countries_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $countries['countries_id']; ?>" />
<?php 
      } 
?>
                </td>
                <td class="dataTableContent"><?php echo $countries['countries_name']; ?></td>
                <td class="dataTableContent" align="center">
<?php
      if ($countries['status'] == '1') {
        echo '<a href="' . osc_href_link('countries.php', 'action=setflag&flag=0&id=' . $countries['countries_id'] . '&page='. $_GET['page']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('countries.php', 'action=setflag&flag=1&id=' . $countries['countries_id'] . '&page='. $_GET['page']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="center" width="40"><?php echo $countries['countries_iso_code_2']; ?></td>
                <td class="dataTableContent" align="center" width="40"><?php echo $countries['countries_iso_code_3']; ?></td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&cID=' . $countries['countries_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&cID=' . $countries['countries_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($cInfo) && is_object($cInfo) && ($countries['countries_id'] == $cInfo->countries_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&cID=' . $countries['countries_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?></td>
              </tr>
<?php
  } // end while
?>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_COUNTRY . '</strong>');

      $contents = array('form' => osc_draw_form('countries', 'countries.php', 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_NAME . '<br />' . osc_draw_input_field('countries_name'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_CODE_2 . '<br />' . osc_draw_input_field('countries_iso_code_2'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_CODE_3 . '<br />' . osc_draw_input_field('countries_iso_code_3'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_ADDRESS_FORMAT . '<br />' . osc_draw_pull_down_menu('address_format_id', osc_get_address_formats()));
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('countries.php', 'page=' . $_GET['page']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_COUNTRY . '</strong>');

      $contents = array('form' => osc_draw_form('countries', 'countries.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_NAME . '<br />' . osc_draw_input_field('countries_name', $cInfo->countries_name));
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_CODE_2 . '<br />' . osc_draw_input_field('countries_iso_code_2', $cInfo->countries_iso_code_2));
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_CODE_3 . '<br />' . osc_draw_input_field('countries_iso_code_3', $cInfo->countries_iso_code_3));
      $contents[] = array('text' => '<br />' . TEXT_INFO_ADDRESS_FORMAT . '<br />' . osc_draw_pull_down_menu('address_format_id', osc_get_address_formats(), $cInfo->address_format_id));
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_COUNTRY . '</strong>');

      $contents = array('form' => osc_draw_form('countries', 'countries.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $cInfo->countries_name . '</strong>');
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_UPDATE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('countries.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->countries_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    default:
    break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>