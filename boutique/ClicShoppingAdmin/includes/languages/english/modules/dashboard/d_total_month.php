<?php
/**
 * d_total_month.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_TITLE', 'Evolution of cumulative sales per month on  1 year');
define('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_DESCRIPTION', 'Chart to see the evolution of sales per month based on the basis of subtotal');
define('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_CHART_LINK', 'Evolution of cumulative sales on 1 year');
?>
