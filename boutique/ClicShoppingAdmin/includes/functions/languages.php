<?php
/*
 * languages.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  function osc_get_languages_directory($code) {
    global $OSCOM_PDO;

    $Qlanguage = $OSCOM_PDO->prepare('select languages_id,
                                                   directory
                                            from :table_languages
                                            where code = $code
                                           ');
    $Qlanguage->bindValue(':code', $code);
    $Qlanguage->execute();

    if ($Qlanguage->fetch() === false) {
      $_SESSION['languages_id'] = $Qlanguage->value('languages_id');
      return $Qlanguage->value('directory');
    } else {
      return false;
    }
  }
  
/**
* Copy file or folder from source to destination, it can do
* recursive copy as well and is very smart
* It recursively creates the dest file or directory path if there weren't exists
* @param $source //file or folder
* @param $dest ///file or folder
* @param $options //folderPermission,filePermission
* @return boolean
*/
  function osc_smartCopy($source, $dest) {
    mkdir($dest, 0755);
    foreach (
    $iterator = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
    RecursiveIteratorIterator::SELF_FIRST) as $item) {
      if ($item->isDir()) {
        mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
      } else {
        copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
      }
    }
  }


/**
 * Directory list file with a drop down for languages
 * 
 * @param string $filename, $value
 * @return string $filename_array,  $filename, $name, the file name of directory
 * @access public
 */

  function osc_cfg_pull_down_language($filename, $value = '') {
    global $OSCOM_PDO;

    $template_directory =  DIR_FS_CATALOG_IMAGES .'icons/languages/';
 
    if ($contents = @scandir($template_directory)) {
      $found = array(); //initialize an array for matching files
      $fileTypes = array('gif'); // Create an array of file types
      $found = array(); // Traverse the folder, and add filename to $found array if type matches

      $name = 'image';

      foreach ($contents as $item) {
        $fileInfo = pathinfo($item);
        if(array_key_exists('extension', $fileInfo) && in_array($fileInfo['extension'],$fileTypes)) {
          $found[] = $item;
        }
      }

      if ($found) { // Check the $found array is not empty 
        natcasesort($found); // Sort in natural, case-insensitive order, and populate menu
        $filename_array = array();
        foreach ($found as $filename){
          $filename_array[] = array('id' => $filename,
                                    'text' => $filename);
        }
      } 
    } // end if contents


    $QlanguageImage = $OSCOM_PDO->prepare('select image
                                           from :table_languages
                                           where languages_id = :languages_id
                                          ');
    $QlanguageImage->bindValue(':languages_id', (int)$_GET['lID']);
    $QlanguageImage->execute();

    $language_image = $QlanguageImage->fetch();

    if ( $language_image == '') {
      $image_name  = '';
    } else {
      $image_name  = $language_image['image'];
    }

    $filename = $image_name;
    return osc_draw_pull_down_menu($name, $filename_array,  $filename);
  }
