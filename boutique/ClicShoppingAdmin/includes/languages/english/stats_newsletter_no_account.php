<?php
/*
 * stats_newsletter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */


define('HEADING_TITLE', 'Newsletter subscription with no account');

define('TABLE_HEADING_FIRST_NAME', 'First name');
define('TABLE_HEADING_LAST_NAME', 'Last name');
define('TABLE_HEADING_ADDRESS_EMAIL', 'Address email');
define('TABLE_HEADING_DATE_ADDED', 'date added');
define('TABLE_HEADING_CLEAR','Clear');
define('HEADING_TITLE_SEARCH','Search');

?>