<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ACTION_RECORDER_RESET_PASSWORD_TITLE', 'Customer Password Reset');
  define('MODULE_ACTION_RECORDER_RESET_PASSWORD_DESCRIPTION', 'Record usage of customer password resets.');
?>
