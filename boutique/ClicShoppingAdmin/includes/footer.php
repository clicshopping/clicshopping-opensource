<?php
/**
 * footer.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */


  if (isset($_SESSION['admin'])) {
    if ($menu_navigation == 'tablet' || $menu_navigation == 'both') {
      include ('ext/javascript/MultiLevelPushMenu/configure_menu_footer.php');
    }
  }
?>

  </div> <!-- id="content" -->
</div> <!-- id="container" -->

<footer>
  <div class="clearfix"></div>
  <div id="footer">
    <div class="footerCadre">
      <div class="navbar navbar-inverse navbar-fixed-bottom">
        <div class="navbar-collapse collapse pull-left" id="footer-body">
          <ul class="nav navbar-nav">
            <li><?php echo PROJECT_VERSION; ?> &copy; 2008 - <?php echo date("Y"); ?></li>
          </ul>
        </div>
        <div class="navbar-collapse collapse pull-right" id="footer-body">
          <ul class="nav navbar-nav">
            <li>
<?php
  if (isset($_SESSION['admin'])) {
?>
              <span>
<?php
    echo TEXT_LEGEND;
    echo osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) .' ' . IMAGE_EDIT .' - ';
    echo osc_image(DIR_WS_ICONS . 'copy.gif', IMAGE_COPY_TO) .' ' . IMAGE_COPY_TO .' - ';
    echo osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) .' ' . IMAGE_DELETE .' - ';
    echo osc_image(DIR_WS_ICONS . 'preview.gif', IMAGE_PREVIEW) .' ' . IMAGE_PREVIEW . ' - ' ;
?>

              </span>
              <span><a href="#topPage"><?php echo osc_image(DIR_WS_IMAGES . 'footer/top.gif', 'Retour en haut de la page', '16', '16'); ?></a></span>
<?php
  }
  if (!isset($_SESSION['admin'])) {
?>
                <span style="font-size:small;">
<?php
    echo 'Optimized for';
    echo osc_draw_separator('pixel_trans.gif', '5', '1');
    echo osc_image(DIR_WS_IMAGES . 'chrome.png', 'Google Chrome', '16', '16');
    echo osc_draw_separator('pixel_trans.gif', '5', '1');
    echo osc_image(DIR_WS_IMAGES . 'firefox.png', 'Firefox', '16', '16');
    echo osc_draw_separator('pixel_trans.gif', '10', '1');
?>
                </span>
<?php
  }
?>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--// go to the top of the page -->
  <script>
    $(function() { $('a[href=#topPage]').click(function(){ $('html, body').animate({scrollTop:0}, 'slow'); return false; }); });
  </script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- autocompletion -->
<script  src="ext/javascript/jquery/autocompletion/jquery.tokeninput.min.js" /></script>
<!-- seo meta -->
<script src="ext/javascript/jquery/charcount/charCount.js" /></script>

<!-- date picker -->
<script src="ext/javascript/bootstrap/js/bootstrap-datepicker.js" /></script>
<script src="ext/javascript/clicshopping/datepicker.js" /></script>

<!-- Tab bootstrap -->
<script type="text/javascript" src="ext/javascript/bootstrap/js/bootstrap_tab.js" /></script>

<!--fixe footer -->
<script src="ext/javascript/jquery/footer/footer.js" /></script>

<?php
  if ($menu_navigation == 'tablet' || $menu_navigation == 'both') {
    echo '<script src="ext/javascript/MultiLevelPushMenu/jquery.multilevelpushmenu.min.js" /></script>';
    echo '<script type="text/javascript" src="ext/javascript/MultiLevelPushMenu/clicshopping/clicshopping.js" /></script>';
  }
?>
  </body>
</html>