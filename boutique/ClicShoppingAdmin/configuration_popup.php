<?php
/**
 * configuration_popup.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */

  require('includes/application_top.php');

  $gID = (isset($_GET['gID'])) ? $_GET['gID'] : 1;

  $Qconfiguration = $OSCOM_PDO->prepare('select configuration_id,
                                                configuration_title,
                                                configuration_value,
                                                use_function
                                       from :table_configuration
                                       where configuration_group_id = :configuration_group_id
                                       order by sort_order
                                       ');
  $Qconfiguration->bindInt(':configuration_group_id', (int)$gID);
  $Qconfiguration->execute();

  while ($configuration = $Qconfiguration->fetch() ) {
    if (osc_not_null($configuration['use_function'])) {
      $use_function = $configuration['use_function'];
      if (preg_match('/->/', $use_function)) {
        $class_method = explode('->', $use_function);
        if (!is_object(${$class_method[0]})) {
          include('includes/classes/'. $class_method[0] . '.php');
          ${$class_method[0]} = new $class_method[0]();
        }
        $cfgValue = osc_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
      } else {
        $cfgValue = osc_call_function($use_function, $configuration['configuration_value']);
      }
    } else {
      $cfgValue = $configuration['configuration_value'];
    }

    if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {

      $QcfgExtra = $OSCOM_PDO->prepare("select configuration_key,
                                                configuration_description,
                                                date_added,
                                                last_modified,
                                                use_function,
                                                set_function
                                         from configuration
                                         where configuration_id = :configuration_id
                                        ");

      $QcfgExtra->bindInt(':configuration_id', (int)(int)$configuration['configuration_id']);
      $QcfgExtra->execute();

      $cfg_extra = $QcfgExtra->fetch();

      $cInfo_array = array_merge($configuration, $cfg_extra);
      $cInfo = new objectInfo($cInfo_array);
    }

  }
  if ($cInfo->set_function) {
    eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
  } else {
    $value_field = osc_draw_input_field('configuration_value', $cInfo->configuration_value);
  }

  echo osc_draw_form('configuration', 'configuration.php', 'gID=' . $_GET['gID'] . '&cID=' . $cInfo->configuration_id . '&action=save');
?>

  <div class="adminTitle">
    <div class="row">
      <span class="col-md-1"  style="padding-bottom:10px;"><?php echo osc_image(DIR_WS_IMAGES . 'categories/configuration_' . $gID . '.gif', '', '40', '40'); ?></span>
      <span class="pageHeading col-md-8"  style="padding-bottom:10px;"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
      <span class="col-md-3" style="text-align:right; padding-top:5px;">&nbsp;<?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) ; ?></span>
    </div>
  </div>
  <div style="padding:20px 10px 30px 10px; text-align:left;">
    <div  style="font-weight: bold; font-size:12px;"><?php echo '&nbsp;' . $cInfo->configuration_title; ?></div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <div><?php echo $cInfo->configuration_description; ?></div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <div><?php echo $value_field; ?></div>
  </div>

</form>
  <!-- footer //-->
<?php
 require('includes/application_bottom.php');
?>