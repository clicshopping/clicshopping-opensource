<?php
/*
 * stats_suppliers_orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('PRINT_INVOICE_HEADING', 'Rapport Fournisseur');
define('PRINT_SUPPLIERS_TITLE', 'Bon de commande fournisseur ');
define('PRINT_SUPPLIERS', 'Fournisseur');

//Customer information
define('ENTRY_EMAIL','E-mail : ');
define('ENTRY_MANAGER','Responsable : ');
define('ENTRY_PHONE','Telephone : ');
define('ENTRY_FAX','Fax : ');
define('ENTRY_SHIP_TO', 'Adresse :');
define('ENTRY_SUPPLIER_INFORMATION','Informations fournisseur : ');
define('ENTRY_SUPPLIERS_NUMBER','Reference Fournisseur : ');

define('START_ANALYSE','Date debut analyse : ');
define('END_ANALYSE','Date fin analyse : ');

define('ENTRY_STATUS','Statut de la commande traitée');

define('TABLE_HEADING_QTE', 'Qte');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Reference');
define('TABLE_HEADING_PRODUCTS', 'Produit');
define('TABLE_HEADING_OPTIONS','Option');
define('TABLE_HEADING_VALUE','Valeur');



define('ENTRY_TVA_SHOP_INTRACOM ','Numéro intracommunautaire : ');
define('ENTRY_SHOP_SIRET', 'Numéro de nomenclature : ');
define('ENTRY_SHOP_CODE_APE', 'No Enregistrement : ');

define('ENTRY_HTTP_SITE','Site Internet : ');
define('PRINT_INVOICE_URL', HTTP_CATALOG_SERVER );


define('THANK_YOU_CUSTOMER', '');
define('RESERVE_PROPRIETE', '');
define('RESERVE_PROPRIETE_NEXT', '');
define('RESERVE_PROPRIETE_NEXT1', '');
define('RESERVE_PROPRIETE_NEXT2', '');

// gestion de la double taxe ou non 
if (DISPLAY_DOUBLE_TAXE == 'false') {
  define('ENTRY_INFO_SOCIETE', ' ');
  define('ENTRY_INFO_SOCIETE_NEXT', '');
} else {
  define('ENTRY_INFO_SOCIETE', '');
  define('ENTRY_INFO_SOCIETE_NEXT', '');
}

?>