<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Address Book');

define('HEADING_TITLE','Address management');

define('NAVBAR_TITLE_ADD_ENTRY', 'New Entry');
define('NAVBAR_TITLE_MODIFY_ENTRY', 'Update Entry');
define('NAVBAR_TITLE_DELETE_ENTRY', 'Delete Entry');

define('HEADING_TITLE_ADD_ENTRY', 'New Address Book Entry');
define('HEADING_TITLE_MODIFY_ENTRY', 'Update Address Book Entry');
define('HEADING_TITLE_DELETE_ENTRY', 'Delete Address Book Entry');

define('DELETE_ADDRESS_TITLE', 'Delete Address');
define('DELETE_ADDRESS_DESCRIPTION', 'Are you sure you would like to delete the selected address from your address book?');

define('NEW_ADDRESS_TITLE', 'New Address Book Entry');

define('SELECTED_ADDRESS', 'Selected Address');
define('SET_AS_PRIMARY', 'Set as primary address.');

define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'The selected address has been successfully removed from your address book.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Your address book has been successfully updated.');

define('WARNING_PRIMARY_ADDRESS_DELETION', 'The primary address cannot be deleted. Please set another address as the primary address and try again.');

define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'The address book entry does not exist.');
define('ERROR_ADDRESS_BOOK_FULL', 'Your address book is full. Please delete an unneeded address to save a new one.');
define('ERROR_ADDRESS_BOOK_NO_ADD', 'You are not authorized to add a new address in your personal notebook. Do not hesitate to contact us to receive more information over this impossibility.');
define('ERROR_ADDRESS_BOOK_NO_MODIFY_DEFAULT', 'You are not authorized to modify the principal address of your personal notebook. Do not hesitate to contact us to receive more information over this impossibility.');
?>