<?php
/*
 * export_data.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Export Management ');
define('TEXT_TITLE_EXPORT','Form of export configuration ');
define('COMPARATEUR_SELECT', 'Format: ');
define('COMPARATEUR_LNG', 'Language: ');
define('COMPARATEUR_CODE', 'Secure code: ');
define('COMPARATEUR_CACHE', 'Use cache file ? ');
define('COMPARATEUR_OUI', 'Yes');
define('COMPARATEUR_NON', 'No');
define('COMPARATEUR_SECU', 'Put cache file in secure dir ? ');
define('COMPARATEUR_FICHIER', 'Cache filename: ');
define('COMPARATEUR_OBLIG', 'You have to fill this input if you use cache file ');

define('COMPARATEUR_CHAMP', 'Free field, save for example <strong><i>"?id=1"</i></strong> to add this parameter in urls exported. No verification is done ');
define('COMPARATEUR_URL', 'Url to copy in the address bar of your browser to generate the file or see the script or to give your integrator');
define('COMPARATEUR_SELECT_EXPORT','Select your export ');

// Aide
define('TITLE_AIDE_EXPORT_IMAGE', 'Help');
define('TITLE_AIDE_EXPORT', 'Informations on export');


define('TEXT_AIDE_EXPORT', 'This system will allow you to make different types of exports, price comparison, products .....<br /><br /><strong><u>Our advices </u></strong><br />
<blockquote>
<li><font color="#FF0000"> * </font> are our tips to follow for proper use of this tool. They apply to most configurations. </li> <li> We advise you save the file on the server and well remember his name if you have a large quantity of product.
<li> If you have a product range over 100 products, it will generate a file. Some options will be removed from this quota reached </li>
<blockquote>	
- The creation of the file involves a mounted server load important. <br /> - The ideal practice is to have the same name as the export selected. <br /> (eg price comparison guide.com xml ===> filename: le_guidecom.xml or <br /> Kelkoo price comparison xml ===> filename: kelkoo.xml). <br /> The syntax will make it easy to generate a new file overwriting the old and update your data. <br /> - This procedure will be to achieve each time you change your data so that your file is always updated <br /> - Saving the file on the server is important because it will allow the server to retrieve information more easily without requiring resources to the server. <br />
- The options and fields are not taken into account now. <br />
</blockquote>
<li>	
If you do not create a file and you generate the file directly (onset of flow xml in your browser). The record must be in xml. (eg export_data.php in export_data.xml) <br />
<br />
<li>How to access your file from a URL and download</li>
<blockquote>	- 	
Address unsecured accessible via the browser : '. HTTP_SERVER . DIR_WS_ADMIN .'ext/export/myfile.xml<br />
<strong>- This URLs to access the files do not disclose to anyone !</strong>
</blockquote>
');




?>
