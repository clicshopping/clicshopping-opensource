<?php
/**
 * newsletters.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

define('HEADING_TITLE', 'Bulletin d\'informations');

define('TABLE_HEADING_NEWSLETTERS', 'Bulletin d\'informations');
define('TABLE_HEADING_SIZE', 'Taille');
define('TABLE_HEADING_MODULE', 'Module');
define('TABLE_HEADING_SENT', 'Envoyer');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_LANGUAGE','Langue');
define('TABLE_HEADING_B2B','Groupe client');

define('TEXT_ALL_LANGUAGES','Toutes les langues');
define('TEXT_ALL_CUSTOMERS','Clients normaux'); 
define('TEXT_NEWSLETTER_CUSTOMERS_GROUP',' Groupes Client');
define('TEXT_NEWSLETTER_LANGUAGE','Type de client visé');
define('TEXT_NEWSLETTER_CREATE_FILE_HTML','Souhaitez vous créer un fichier html ? *');
define('TEXT_NEWSLETTER_TWITTER','Souhaitez vous publier cette newsletter sur Twitter ? ');
define('TEXT_NEWSLETTER_CUSTOMER_NO_ACCOUNT','Souhaitez vous inclure les clients sans compte ? ');
define('TEXT_YES','Oui');
define('TEXT_NO','non');

define('TEXT_NEWSLETTER_MODULE', 'Module :');
define('TEXT_NEWSLETTER_TITLE', 'Titre du bulletin d\'informations :');
define('TEXT_NEWSLETTER_CONTENT', 'Contenu :');

define('TEXT_NEWSLETTER_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_NEWSLETTER_DATE_SENT', 'Date d\'envoi :');

define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer ce bulletin d\'informations ?');

define('TEXT_PLEASE_WAIT', 'Veuillez patienter et ne pas interrompre ce traitement !<br /><br />Envois en cours...<br /><br />Le traitement peut être plus ou moins long en fonction du nombre de mail à traiter');
define('TEXT_FINISHED_SENDING_EMAILS', 'Fini.. Tous les courriers électroniques sont envoyés !');

define('ERROR_NEWSLETTER_TITLE', 'Erreur : Titre du bulletin d\'informations requis');
define('ERROR_NEWSLETTER_MODULE', 'Erreur : Module bulletin d\'informations requis');
define('ERROR_REMOVE_UNLOCKED_NEWSLETTER', 'Erreur : Merci de fermer le bulletin d\'informations avant de le supprimer.');
define('ERROR_EDIT_UNLOCKED_NEWSLETTER', 'Erreur : Merci de fermer le bulletin d\'informations avant de l\'éditer.');
define('ERROR_SEND_UNLOCKED_NEWSLETTER', 'Erreur : Merci de fermer le bulletin d\'informations avant de l\'envoyer.');

define('TITLE_AIDE_IMAGE', 'Note sur l\'utilisation des images');
define('TITLE_HELP_DESCRIPTION', 'Note sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>
<li>Nous vous conseillons de créer un fichier uniquement pour les clients normaux. Ce fichier sera visible par toutes les personnes ayant un accès au répertoire boutique/pub.</li><li>La publication sur Twitter ne fonctionne que si vous créez un fichier html.</li>
');
define('IMAGE_BUTTON_CONTINUE','Envoyer');
?>
