<?php
/*
 * create_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

    $error = false;   
    $process = false;
    if (isset($_POST['action']) && ($_POST['action'] == 'process')) {
      if (isset($_POST['gender'])) {
        $customers_gender = osc_db_prepare_input($_POST['customers_gender']);
      } else {
        $customers_gender = false;
      }

// Informations client
        $customers_firstname = osc_db_prepare_input($_POST['customers_firstname']);
        $customers_lastname = osc_db_prepare_input($_POST['customers_lastname']);
        $customers_email_address = osc_db_prepare_input($_POST['customers_email_address']);
        $customers_telephone = osc_db_prepare_input($_POST['customers_telephone']);
        $customers_fax = osc_db_prepare_input($_POST['customers_fax']);
        $customers_cellular_phone = osc_db_prepare_input($_POST['customers_cellular_phone']);
        $customers_languages_id = osc_db_prepare_input($_POST['customers_languages_id']);
        $customers_group_name = osc_db_prepare_input($_POST['customers_group_name']);
        $customers_gender = osc_db_prepare_input($_POST['customers_gender']);
        $customers_dob = osc_db_prepare_input($_POST['customers_dob']);
        $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);

// Informations sur la societe
        $customers_company = osc_db_prepare_input($_POST['customers_company']);
        $customers_siret = osc_db_prepare_input($_POST['customers_siret']);
        $customers_ape = osc_db_prepare_input($_POST['customers_ape']);

// Informations numero de TVA avec transformation de code ISO en majuscule
        $customers_tva_intracom_code_iso = osc_db_prepare_input($_POST['customers_tva_intracom_code_iso']);
        $customers_tva_intracom_code_iso = strtoupper($customers_tva_intracom_code_iso);
        $customers_tva_intracom = osc_db_prepare_input($_POST['customers_tva_intracom']);

// Informations sur le type de facturation
        $customers_options_order_taxe = osc_db_prepare_input($_POST['customers_options_order_taxe']);
        $default_address_id = osc_db_prepare_input($_POST['default_address_id']);
        $customers_street_address = osc_db_prepare_input($_POST['customers_street_address']);
        $customers_suburb = osc_db_prepare_input($_POST['customers_suburb']);
        $customers_postcode = osc_db_prepare_input($_POST['postcode']);
        $customers_city = osc_db_prepare_input($_POST['city']);
        $customers_country_id = osc_db_prepare_input($_POST['customers_country_id']);
        $customers_company = osc_db_prepare_input($_POST['customers_company']);
        $customers_state = osc_db_prepare_input($_POST['customers_state']);

// Autorisation aux clients de modifier Les informations de la societe
        $customers_modify_company = osc_db_prepare_input($_POST['customers_modify_company']);

// Autorisation aux clients de modifier adresse principal
        $customers_modify_address_default = osc_db_prepare_input($_POST['customers_modify_address_default']);
        $customers_add_address = osc_db_prepare_input($_POST['customers_add_address']);

// Coupon - Email: 
        $customers_coupons = $_POST['customers_coupons'];
        $customers_email = $_POST['customers_email'];

// Information sur la zone geographique

        $customers_zone_id = false;
        if (ACCOUNT_STATE_PRO == 'true' || ACCOUNT_STATE == 'true') {
          $customers_state = osc_db_prepare_input($_POST['customers_state']);
          if (isset($_POST['zone_id'])) {
            $customers_zone_id = osc_db_prepare_input($_POST['customers_zone_id']);
          }
        }

        $QmultipleGroups = $OSCOM_PDO->prepare('select distinct customers_group_id
                                                 from :table_products_groups
                                              ');

        $QmultipleGroups->execute();

        while ($group_ids = $QmultipleGroups->fetch() ) {

          $Qmultiplecustomers = $OSCOM_PDO->prepare('select distinct customers_group_id
                                                    from :table_customers_groups
                                                    where customers_group_id = :customers_group_id
                                                   ');

          $Qmultiplecustomers->bindInt(':customers_group_id', (int)$group_ids['customers_group_id'] );
          $Qmultiplecustomers->execute();

          
          if (!($multiple_groups = $Qmultiplecustomers->fetch() )) {

            $Qdelete = $OSCOM_PDO->prepare('select from :table_products_groups
                                            where customers_group_id = :customers_group_id
                                           ');
            $Qdelete->bindInt(':customers_group_id',  (int)$group_ids['customers_group_id']);
            $Qdelete->execute();

          }
        } // end while

        $QgroupNameCheck = $OSCOM_PDO->prepare("select distinct customers_group_name, 
                                                                customers_group_id 
                                                from :table_customers_groups 
                                                order by customers_group_id 
                                      ");
        $QgroupNameCheck->execute();

        $group_name_check = $QgroupNameCheck->fetch();

// Controle des saisies faites sur les champs TVA Intracom
        if ((strlen($customers_tva_intracom_code_iso) > '0') || (strlen($customers_tva_intracom) > '0')) {

          $QcustomersTva = $OSCOM_PDO->prepare('select countries_iso_code_2
                                                 from countries
                                                 where countries_iso_code_2 = :countries_iso_code_2
                                               ');

          $QcustomersTva->bindValue(':countries_iso_code_2',  $customers_tva_intracom_code_iso );

          $QcustomersTva->execute();

          if ($QcustomersTva->rowCount() > 0 ) {
            $customers_tva_intracom_code_iso_error = false;
          } else {
            $error = true;
            $customers_tva_intracom_code_iso_error = true;
          }
        }

        if (strlen($customers_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
          $error = true;
          $entry_firstname_error = true;
        } else {
          $entry_firstname_error = false;
        }

        if (strlen($customers_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
          $error = true;
          $entry_lastname_error = true;
        } else {
          $entry_lastname_error = false;
        }

        $entry_email_address_error = false;

        if (!osc_validate_email($customers_email_address)) {
          $error = true;
          $entry_email_address_check_error = true;
        } else {
          $entry_email_address_check_error = false;
        }

        if (strlen($customers_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
          $error = true;
          $entry_street_address_error = true;
        } else {
          $entry_street_address_error = false;
        }

        if (strlen($customers_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
          $error = true;
          $entry_post_code_error = true;
        } else {
          $entry_post_code_error = false;
        }


        if (strlen($customers_city) < ENTRY_CITY_MIN_LENGTH) {
          $error = true;
          $entry_city_error = true;
        } else {
          $entry_city_error = false;
        }

        if ($customers_country_id == false) {
          $error = true;
          $entry_country_error = true;
        } else {
          $entry_country_error = false;
        }


        if (ACCOUNT_STATE == 'true') {
          if ($entry_country_error == true) {
            $entry_state_error = true;
          } else {
            $zone_id = 0;
            $entry_state_error = false;

            $Qcheck = $OSCOM_PDO->prepare("select count(*) as total 
                                           from :table_zones 
                                           where zone_country_id = :zone_country_id
                                          ");
            $Qcheck->bindInt(':zone_country_id', (int)$customers_country_id);
            $Qcheck->execute();

            $check_value = $Qcheck->fetch();

            $entry_state_has_zones = ($check_value['total'] > 0);
            
            if ($entry_state_has_zones == true) {
              $Qzone = $OSCOM_PDO->prepare('select zone_id
                                             from :table_zones
                                             where zone_country_id = :zone_country_id
                                             and (zone_name = :zone_name or zone_code = :zone_code)
                                           ');
              $Qzone->bindInt(':zone_country_id', (int)$customers_country_id);
              $Qzone->bindValue(':zone_name',  osc_db_input($customers_state));
              $Qzone->bindValue(':zone_code',  osc_db_input($customers_state));

              $Qzone->execute();

              if ($Qzone->rowCount() == 1) {
                $zone_values = $Qzone->fetch();

                $entry_zone_id = $zone_values['zone_id'];

              } else {

                $error = true;
                $entry_state_error = true;
              }
            } else {
              if (strlen($customers_state) < ENTRY_STATE_MIN_LENGTH) {
                $error = true;
                $entry_state_error = true;
              }
            }
         }
      }

      if (strlen($customers_telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;
        $entry_telephone_error = true;
      } else {
        $entry_telephone_error = false;
      }

      $Qcheck = $OSCOM_PDO->prepare("select customers_email_address
                                   from customers
                                   where customers_email_address = :customers_email_address
                                   and customers_id <> :customers_id
                                                    ");
      $Qcheck->bindValue(':customers_email_address', $customers_email_address);
      $Qcheck->bindInt(':customers_id', (int)$customers_id);
      $Qcheck->execute();

      if ($Qcheck->fetch() !== false) {
        $error = true;
        $entry_email_address_exists = true;
      } else {
        $entry_email_address_exists = false;
      }

      if ($error == false) {

// check if mail on newsletter_no_account	  

        $QcheckEmailNoAccount = $OSCOM_PDO->prepare("select count(*) as total 
                                                     from :table_newsletter_no_account
                                                     where customers_email_address = :customers_email_address
                                                    ");
        $QcheckEmailNoAccount->bindValue(':customers_email_address', osc_db_input($customers_email_address));
        $QcheckEmailNoAccount->execute();

        $check_email_no_account = $Qcheck->fetch();

        if ($check_email_no_account['total'] > 0) {
          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_newsletter_no_account
                                          where customers_email_address = :email_address 
                                        ');
          $Qdelete->bindValue(':email_address', $email_address);
          $Qdelete->execute();
        }

        $customers_password = 'clicshopping';

// Autorisation aux clients de modifier informations societe et adresse principal + Ajout adresse
        if ($customers_modify_company != '1') $customers_modify_company = '0';
        if ($customers_modify_address_default != '1') $customers_modify_address_default = '0';
        if ($customers_add_address != '1') $customers_add_address = '0';

        $sql_data_array = array('customers_company' => $customers_company,
                                'customers_siret' => $customers_siret,
                                'customers_ape' => $customers_ape,
                                'customers_tva_intracom' => $customers_tva_intracom,
                                'customers_tva_intracom_code_iso' => $customers_tva_intracom_code_iso,
                                'customers_gender' => $customers_gender,			 
                                'customers_firstname' => $customers_firstname,
                                'customers_lastname' => $customers_lastname,
                                'customers_dob' => osc_date_raw($customers_dob),
                                'customers_email_address' => $customers_email_address,
                                'customers_telephone' => $customers_telephone,
                                'customers_fax' => $customers_fax,
//                                'customers_password' => osc_encrypt_password($customers_password),
                                'customers_password' => $customers_password,
                                'customers_newsletter' => '1',
                                'languages_id' => $customers_languages_id,
                                'customers_group_id' => $customers_group_id,
                                'member_level' => '1',
                                'customers_modify_company' => $customers_modify_company,
                                'customers_modify_address_default' => $customers_modify_address_default,
                                'customers_add_address' => $customers_add_address,
                                'customers_cellular_phone' => $customers_cellular_phone
                                );

        $OSCOM_PDO->save('customers', $sql_data_array);

        $customer_id = $OSCOM_PDO->lastInsertId();

//zone
        if ($customers_zone_id > 0) $customers_state = '';

        $sql_data_array = array('customers_id' => $customer_id,
                                'entry_gender' => $customers_gender,
                                'entry_company' => $customers_company,
                                'entry_firstname' => $customers_firstname,
                                'entry_lastname' => $customers_lastname,
                                'entry_street_address' => $customers_street_address,
                                'entry_suburb' => $customers_suburb,
                                'entry_postcode' => $customers_postcode,
                                'entry_city' => $customers_city,
                                'entry_country_id' => $customers_country_id,
                                'entry_siret' => $customers_siret,
                                'entry_ape' => $customers_ape,
                                'entry_tva_intracom' => $customers_tva_intracom,
                                'entry_firstname' => $customers_firstname,
                                'entry_lastname' => $customers_lastname,
                                'entry_street_address' => $customers_street_address,
                                'entry_suburb' => $customers_suburb,
                                'entry_postcode' => $customers_postcode,
                                'entry_city' => $customers_city
                                );

        if (ACCOUNT_STATE == 'true') {
          if ($entry_zone_id > 0) {
            $sql_data_array['entry_zone_id'] = $customers_zone_id;
            $sql_data_array['entry_state'] = $customers_state;
          } else {
            $sql_data_array['entry_zone_id'] = '0';
            $sql_data_array['entry_state'] = $customers_state;

          }
        }

        $OSCOM_PDO->save('address_book', $sql_data_array);

        $address_id = $OSCOM_PDO->lastInsertId();

        $sql_data_array['customers_info_id'] = (int)$customers_id;


        $Qupdate = $OSCOM_PDO->prepare('update :table_customers
                                        set customers_default_address_id = :customers_default_address_id
                                        where customers_id = :customers_id
                                      ');
        $Qupdate->bindInt(':customers_default_address_id', (int)$address_id);
        $Qupdate->bindInt(':customers_id', (int)$customer_id );
        $Qupdate->execute();


        $OSCOM_PDO->save('customers_info', [
                                          'customers_info_id' =>  (int)$customer_id,
                                          'customers_info_number_of_logons' => 0,
                                          'customers_info_date_account_created' => 'now()'
                                        ]
                        );

// email template
        require ('includes/functions/template_email.php');

        $template_email_welcome_admin = osc_get_template_email_welcome_admin($template_email_welcome_admin);
        $template_email_coupon_admin = osc_get_template_email_coupon_admin($template_email_coupon_admin);
        $template_email_signature =osc_get_template_email_signature($template_email_signature);
        $template_email_footer = osc_get_template_email_text_footer($template_email_footer);
        $email_welcome = html_entity_decode(EMAIL_WELCOME);
        $email_subject = html_entity_decode(EMAIL_SUBJECT);


        if (ACCOUNT_GENDER == 'true') {
          if ($customers_gender == 'm') {
            $email_gender = sprintf(EMAIL_GREET_MR, $customers_lastname);
          } else {
            $email_gender = sprintf(EMAIL_GREET_MS, $custmers_lastname);
          }
        } else {
          $email_gender = sprintf(EMAIL_GREET_NONE, $customers_firstname);
        }


// coupon
        if (COUPON_CUSTOMER != '' && $customers_coupons == '1') {
          $email_coupon = $template_email_coupon_admin . COUPON_CUSTOMER .'</ strong>';
        }


        $email_text = $email_gender .'<br /><br />'. $template_email_welcome_admin .'<br /><br />'. $email_coupon .'<br /><br />' .   $template_email_signature . '<br /><br />' . $template_email_footer;
        $email_text = $email_text;

// Envoi du mail avec gestion des images pour Fckeditor et Imanager.
        if ($customers_email == '1') {
           $mimemessage = new email(array('X-Mailer: ClicShopping'));
           $message = html_entity_decode($email_text);
           $message = str_replace('src="/', 'src="' . HTTP_CATALOG_SERVER . '/', $message);
           $mimemessage->add_html_fckeditor($message);
           $mimemessage->build_message();
           $from = STORE_OWNER_EMAIL_ADDRESS;
           $mimemessage->send($name, $customers_email_address, '', $from, $email_subject);
        }

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_ADMIN == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_create_account.php');
        }
//***************************************
// End odoo web service
//***************************************
        osc_redirect(osc_href_link('customers.php'));
      }
    }

  require('includes/header.php');

?>
<script type="text/javascript"><!--

function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

<?php
  if (ACCOUNT_GENDER == 'true') {
?>
  if (document.customers.customers_gender[0].checked || document.customers.customers_gender[1].checked) {
  } else {
    error_message = error_message + "<?php echo JS_GENDER; ?>";
    error = 1;
  }
<?php
  }
?>

  if (customers_firstname.length < <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_FIRST_NAME; ?>";
    error = 1;
  }

  if (customers_lastname.length < <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_LAST_NAME; ?>";
    error = 1;
  }
/* dob verification deleted

    if (ACCOUNT_DOB == 'true') {
      if ((strlen($dob) < ENTRY_DOB_MIN_LENGTH) || (!empty($dob) && (!is_numeric(osc_date_raw($dob)) || !@checkdate(substr(osc_date_raw($dob), 4, 2), substr(osc_date_raw($dob), 6, 2), substr(osc_date_raw($dob), 0, 4))))) {
         $error = true;

        $messageStack->add('create_account', ENTRY_DATE_OF_BIRTH_ERROR);
      }
    }
*/

  if (entry_street_address.length < <?php echo ENTRY_STREET_ADDRESS_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_ADDRESS; ?>";
    error = 1;
  }

  if (entry_postcode.length < <?php echo ENTRY_POSTCODE_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_POST_CODE; ?>";
    error = 1;
  }

  if (entry_city.length < <?php echo ENTRY_CITY_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_CITY; ?>";
    error = 1;
  }

<?php
  if (ACCOUNT_STATE == 'true') {
?>
  if (document.customers.elements['customers_state'].type != "hidden") {
    if (document.customers.customers_state.value.length < <?php echo ENTRY_STATE_MIN_LENGTH; ?>) {
       error_message = error_message + "<?php echo JS_STATE; ?>";
       error = 1;
    }
  }
<?php
  }
?>

  if (document.customers.elements['entry_country_id'].type != "hidden") {
    if (document.customers.entry_country_id.value == 0) {
      error_message = error_message + "<?php echo JS_COUNTRY; ?>";
      error = 1;
    }
  }

  if (customers_telephone.length < <?php echo ENTRY_TELEPHONE_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_TELEPHONE; ?>";
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}
//--></script>

<!-- body //-->
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>



  <div class="adminTitle";">
    <span class="col-md-1"><?php echo  osc_image(DIR_WS_IMAGES . 'categories/client_editer.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-3 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
    <?php echo osc_draw_form('create_account', 'create_account.php', 'post') . osc_draw_hidden_field('action', 'process'); ?>
    <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
    <span class="pull-right"><?php echo '<a href="' . osc_href_link('customers.php') .'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
    </span>
  </div>
<?php
    if ($error == true) {
?>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <table border="0" width="100%" cellspacing="3" cellpadding="0" class="messageStackError">
      <tr>
        <td class="messageStackError" height="20" colspan="2"><table width="100%">
            <tr>
              <td width="27" align="center"><?php echo osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING); ?></td>
              <td><?php echo WARNING_EDIT_CUSTOMERS; ?></td>
            </tr>
          </table></td>
      </tr>
    </table>
 <?php
    }
?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="90%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<!-- //################################################################################################################ -->
<!-- //                                               FICHE CLIENT                                                      -->
<!-- //################################################################################################################ -->
    <tr>
      <td>
       <div  style="padding-left: 10px";>

<?php
  if (MODE_B2B_B2C == 'true') {
?>
         <ul class="nav nav-tabs" role="tablist"  id="myTab">
           <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
           <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_SOCIETE; ?></a></li>
           <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_ORDERS; ?></a></li>
           <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_OPTIONS; ?></a></li>
         </ul>
<?php
  } else {
?>
        <ul class="nav nav-tabs" role="tablist"  id="myTab">
          <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
          <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_SOCIETE; ?></a></li>
          <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_OPTIONS; ?></a></li>
        </ul>
<?php
  }
?>
        <div class="tabsClicShopping">
          <div class="tab-content">
<!-- -------------------------------------- //-->
<!--          ONGLET NOM & ADRESSE          //-->
<!-- -------------------------------------- //-->
            <div class="tab-pane active" id="tab1">
              <div class="col-md-12 mainTitle">
                <div class="pull-left"><?php echo CATEGORY_PERSONAL; ?></div>
              </div>
              <div class="adminformTitle">
                <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                <div class="row">
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
                  <div class="form-group has-feedback">
                    <label for="firstname" class="control-label col-md-2"><?php echo ENTRY_GENDER; ?></label>
                    <div class="col-md-6">
                      <span class="text-warning pull-left"><?php echo osc_draw_radio_field('customers_gender', 'm') . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . osc_draw_radio_field('customers_gender', 'f') . '&nbsp;&nbsp;' . FEMALE; ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
                  <div class="form-group has-feedback">
                    <label for="firstname" class="control-label col-md-2"><?php echo ENTRY_FIRST_NAME; ?></label>
                    <div class="col-md-6">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_firstname', NULL, 'required aria-required="true" id="firstname" placeholder="' . ENTRY_FIRST_NAME . '" minlength="'. ENTRY_FIRST_NAME_MIN_LENGTH .'"'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group has-feedback" style="padding-top:10px;">
                    <label for="lastname" class="control-label col-md-2"><?php echo ENTRY_LAST_NAME; ?></label>
                    <div class="col-md-6">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_lastname', NULL, 'required aria-required="true" id="lastname" placeholder="' . ENTRY_LAST_NAME . '" minlength="'. ENTRY_LAST_NAME_MIN_LENGTH .'"'); ?></span>
                    </div>
                  </div>
                </div>

<script type="text/javascript">
  $(function() {
    $('#customers_dob').datepicker({
      /*dateFormat: '<?php echo JQUERY_DATEPICKER_FORMAT; ?>', */
      dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+0'
    });
  });
</script>
                <div class="spaceRow"></div>
                <div class="row">
                  <label for="customers_dob" class="control-label col-md-2"><?php echo ENTRY_DATE_OF_BIRTH; ?></label>
                  <span class="col-md-2">
<?php
  if ($error == true) {
    if ($entry_date_of_birth_error == true) {
      echo osc_draw_input_field('customers_dob', $OSCOM_Date->getShort($cInfo->customers_dob), 'id ="customers_dob" maxlength="10" style="border: 2px solid #FF0000"') . '&nbsp;' . ENTRY_DATE_OF_BIRTH_ERROR;
    } else {
      echo $cInfo->customers_dob . osc_draw_hidden_field('customers_dob');
    }
  } else {
    echo osc_draw_input_field('customers_dob', $OSCOM_Date->getShort($cInfo->customers_dob), 'id ="customers_dob"', false);
  }
?>
                  </span>
                </div>

                <div class="row">
                  <div class="form-group has-feedback" style="padding-top:10px;">
                    <label for="email" class="control-label col-md-2"><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
                    <div class="col-md-2">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_email_address', NULL, 'required aria-required="true" id="email" placeholder="' . ENTRY_EMAIL_ADDRESS . '"', 'email'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group has-feedback" style="padding-top:10px;">
                    <label for="phone" class="control-label col-md-2"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label>
                    <div class="col-md-2">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_telephone', NULL, 'required aria-required="true" id="phone" placeholder="' . ENTRY_TELEPHONE_NUMBER . '" minlength="'. ENTRY_TELEPHONE_MIN_LENGTH .'"', 'phone'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group" style="padding-top:10px;">
                    <label for="cellular" class="control-label col-md-2"><?php echo ENTRY_CELLULAR_PHONE_NUMBER; ?></label>
                    <div class="col-md-2">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_cellular_phone', NULL, 'id="cellular" placeholder="' . ENTRY_CELLULAR_PHONE_NUMBER . '"'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group" style="padding-top:10px;">
                    <label for="fax" class="control-label col-md-2"><?php echo ENTRY_FAX_NUMBER; ?></label>
                    <div class="col-md-2">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_fax', NULL, 'id="fax" placeholder="' . ENTRY_FAX_NUMBER . '"'); ?></span>
                    </div>
                  </div>
                </div>


              </div>
              <div class="spaceRow"></div>
              <div class="col-md-12 mainTitle">
                <div class="pull-left"><?php echo CATEGORY_ADDRESS_DEFAULT; ?></div>
              </div>
              <div class="adminformTitle">
                 <div class="spaceRow"></div>
                  <div class="row">
                    <div class="form-group has-feedback" style="padding-top:10px;">
                      <label for="street" class="control-label col-md-2"><?php echo ENTRY_STREET_ADDRESS; ?></label>
                      <div class="col-md-2">
                        <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_street_address', NULL, 'required aria-required="true" id="street" placeholder="' . ENTRY_STREET_ADDRESS . '" minlength="'. ENTRY_STREET_ADDRESS_MIN_LENGTH .'"', 'street'); ?></span>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group" style="padding-top:10px;">
                      <label for="suburb" class="control-label col-md-2"><?php echo ENTRY_SUBURB; ?></label>
                      <div class="col-md-3">
                        <span class="text-warning pull-left"><?php echo osc_draw_input_field('customers_suburb', NULL, 'id="suburb" placeholder="' . ENTRY_SUBURB . '"'); ?></span>
                      </div>
                    </div>
                  </div>
                  <div class="spaceRow"></div>
                  <div class="row">
                    <label for="country" class="control-label col-md-2"><?php echo ENTRY_COUNTRY; ?></label>
                    <div class="col-md-2">
                      <span><?php echo  osc_draw_pull_down_menu('customers_country_id', osc_get_countries()) . '&nbsp;' . (osc_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="inputRequirement">' . TEXT_FIELD_REQUIRED . '</span>': ''); ?></span>
                    </div>
                  </div>
<?php
  if (ACCOUNT_STATE == 'true') {
?>
                  <div class="spaceRow"></div>
                  <div class="row">

                    <label for="state" class="control-label col-md-2"><?php echo ENTRY_STATE; ?></label>
                    <div class="col-md-2">
                      <span>
<?php

    if ($process == false) {

      if ($entry_state_has_zones == true) {
        $zones_array = array();
        $Qzone = $OSCOM_PDO->prepare('select zone_name
                                     from :table_zones
                                     where zone_country_id = :zone_country_id
                                     order by zone_name
                                     ');
        $Qzone->bindInt(':zone_country_id', (int)$customers_country_id);

        $Qzone->execute();

        while ($zones_values = $Qzone->fetch() ) {
          $zones_array[] = array('id' => $zones_values['zone_name'],
                                 'text' => $zones_values['zone_name']);
        }

        echo osc_draw_pull_down_menu('customers_state', $zones_array);
      } else {
        echo osc_draw_input_field('customers_state');
      }
    } else {
      echo osc_draw_input_field('customers_state');
    }

    if (osc_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="inputRequirement">' . TEXT_FIELD_REQUIRED;
?>
                      </span>
                    </div>
                  </div>
<?php
  }
?>
                <div class="row">
                  <div class="form-group has-feedback" style="padding-top:10px;">
                    <label for="postcode" class="control-label col-md-2"><?php echo ENTRY_POST_CODE; ?></label>
                    <div class="col-md-2">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('postcode', NULL, 'required aria-required="true" id="postcode" placeholder="' . ENTRY_POST_CODE . '" minlength="'. ENTRY_POSTCODE_MIN_LENGTH .'"', 'postcode'); ?></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group has-feedback" style="padding-top:10px;">
                    <label for="city" class="control-label col-md-2"><?php echo ENTRY_CITY; ?></label>
                    <div class="col-md-2">
                      <span class="text-warning pull-left"><?php echo osc_draw_input_field('city', NULL, 'required aria-required="true" id="city" placeholder="' . ENTRY_CITY . '" minlength="'. ENTRY_CITY_MIN_LENGTH .'"', 'city'); ?></span>
                    </div>
                  </div>
                </div>
             </div>
           </div>


<!-- -------------------------------------- //-->
<!--          ONGLET Infos Societe          //-->
<!-- -------------------------------------- //-->
          <div class="tab-pane" id="tab2">
            <div class="col-md-12 mainTitle" style="height:27px;">
              <div class="pull-left"><?php echo CATEGORY_COMPANY; ?></div>
              <div class="pull-right">
<?php
  if (MODE_B2B_B2C == 'true') {
?>
                <span class="mainTitleTexteSeul"><?php echo '&nbsp;' . ENTRY_CUSTOMERS_MODIFY_COMPANY . '&nbsp;'; ?></span
                <span class="mainTitleTexteSeul"><?php echo osc_draw_checkbox_field('customers_modify_company', '1', true); ?></span>
<?php
  }
?>

              </div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2 centerInputFields"><?php echo ENTRY_COMPANY; ?></span>
                  <span class="col-md-4"><?php echo  osc_draw_input_field('customers_company', $customers_company, 'placeholder="' . ENTRY_COMPANY . '" maxlength="32"'); ?></span>
                </div>
              </div>



<?php
  if (MODE_B2B_B2C == 'true') {
?>
              <div class="spaceRow"></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2 centerInputFields"><?php echo ENTRY_SIRET; ?></span>
                  <span class="col-md-4"><?php echo  osc_draw_input_field('customers_siret', $customers_siret, 'placeholder="' . ENTRY_SIRET . '" maxlength="14"') . '&nbsp;<span class="fieldRequired">' . ENTRY_SIRET_EXEMPLE . '</span>'; ?></span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2 centerInputFields"><?php echo ENTRY_APE; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_input_field('customers_ape', $customers_ape, 'placeholder="' . ENTRY_APE . '" maxlength="4"') . '&nbsp;<span class="fieldRequired">' . ENTRY_APE_EXEMPLE . '</span>'; ?></span>
                </div>
              </div>
<?php
    if (ACCOUNT_TVA_INTRACOM_PRO == 'true') {
?>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2 centerInputFields"><?php echo ENTRY_TVA; ?></span>
                  <span class="col-md-4">
<?php
  echo osc_draw_input_field('customers_tva_intracom_code_iso', $customers_tva_intracom_code_iso, 'maxlength="2" size="2"');
  echo '&nbsp;' . osc_draw_input_field('customers_tva_intracom', $customers_tva_intracom, 'maxlength="14"');
?>
                  </span>
                  <span class="col-md-4">
<!-- lien pointant sur le site de verification -->
                    <a href="<?php echo 'http://ec.europa.eu/taxation_customs/vies/vieshome.do?ms=' . $customers_tva_intracom_code_iso . '&iso='.$customers_tva_intracom_code_iso.'&vat=' . $customers_tva_intracom; ?>" target="_blank"><?php echo TVA_INTRACOM_VERIFY; ?></a>
                   <span>
                </div>
              </div>
<?php
    }
  }
// Activation du module B2B
    if (MODE_B2B_B2C == 'true') {
?>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="adminformAide">
                <div class="row">
                  <span class="col-md-12">
                    <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_CUSTOMERS_TVA); ?>
                    <strong><?php echo '&nbsp;' . TITLE_AIDE_CUSTOMERS_TVA; ?></strong>
                  </span>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . TITLE_AIDE_TVA_CUSTOMERS; ?></span>
                </div>
              </div>
<?php
     }
?>
            </div>
          </div>
<?php
// Activation du module B2B
    if (MODE_B2B_B2C == 'true') {
?>	
<!-- ------------------------------------ //-->
<!--          ONGLET Facturation          //-->
<!-- ------------------------------------ //-->
          <div class="tab-pane" id="tab3">
            <div class="col-md-12 mainTitle" style="height:27px;">
              <div class="pull-left"><?php echo CATEGORY_COMPANY; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-2 centerInputFields"><?php echo ENTRY_CUSTOMERS_GROUP_NAME; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_pull_down_menu('customers_group_id', osc_get_customers_group(''. VISITOR_NAME .'')); ?></span>
                </div>
              </div>
            </div>
          </div>
<?php
    }
?>
<!-- ------------------------------------ //-->
<!--          ONGLET Option          //-->
<!-- ------------------------------------ //-->
          <div class="tab-pane" id="tab4">
            <div class="col-md-12 mainTitle" style="height:27px;">
              <div class="pull-left"><?php echo CATEGORY_COMPANY; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-4"><?php echo ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_checkbox_field('customers_modify_address_default', '1', true); ?></span>
                </div>
              </div>
              <div class="spaceRow"></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-4"><?php echo ENTRY_CUSTOMERS_ADD_ADDRESS; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_checkbox_field('customers_add_address', '1', true); ?></span>
                </div>
              </div>
<?php
// Activation du module B2B
    if (MODE_B2B_B2C == 'true') {
?>
              <div class="spaceRow"></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-4"><?php echo ENTRY_CUSTOMERS_COUPONS; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_checkbox_field('customers_coupons', '1', false); ?></span>
                </div>
              </div>
<?php
    }
?>
              <div class="spaceRow"></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-4"><?php echo ENTRY_CUSTOMERS_EMAIL; ?></span>
                  <span class="col-md-4"><?php echo osc_draw_checkbox_field('customers_email', '1', false); ?></span>
                </div>
              </div>
              <div class="spaceRow"></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-4"><?php echo ENTRY_NEWSLETTER_LANGUAGE; ?></span>
                  <span class="col-md-4">
<?php
    $languages = osc_get_languages();
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
      $values_languages_id[$i]=array ('id' =>$languages[$i]['id'],
        'text' =>$languages[$i]['name']);
    }

    echo osc_draw_pull_down_menu('customers_languages_id', $values_languages_id);
?>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </td>
  </tr>
</form>
</table></td>
</tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
