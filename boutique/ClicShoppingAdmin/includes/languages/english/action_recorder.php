<?php
/**
 * action_recorder.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE','Action recorder');
define('TABLE_HEADING_MODULE','Module');
define('TABLE_HEADING_CUSTOMER','Customers'); 
define('TEXT_FILTER_SEARCH', 'Search:');
define('TEXT_ALL_MODULES', '-- All Modules --'); 	
define('TABLE_HEADING_DATE_ADDED','Date added');  	
define('TABLE_HEADING_ACTION','Action'); 
define('TEXT_INFO_IDENTIFIER','Information customer : ');
define('TEXT_INFO_DATE_ADDED','Date added : ');
define('SUCCESS_EXPIRED_ENTRIES','Success');
?>