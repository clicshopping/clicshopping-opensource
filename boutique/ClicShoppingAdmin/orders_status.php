<?php
/*
 * orders_status.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':
      case 'save':
        if (isset($_GET['oID'])) $orders_status_id = osc_db_prepare_input($_GET['oID']);

        $languages = osc_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $orders_status_name_array = $_POST['orders_status_name'];
          $language_id = $languages[$i]['id'];

          $sql_data_array = array('orders_status_name' => osc_db_prepare_input($orders_status_name_array[$language_id]),
                                  'public_flag' => ((isset($_POST['public_flag']) && ($_POST['public_flag'] == '1')) ? '1' : '0'),
                                  'downloads_flag' => ((isset($_POST['downloads_flag']) && ($_POST['downloads_flag'] == '1')) ? '1' : '0'),
                                  'support_orders_flag' => ((isset($_POST['support_orders_flag']) && ($_POST['support_orders_flag'] == '1')) ? '1' : '0')		  
                                 );

          if ($action == 'insert') {
            if (empty($orders_status_id)) {

              $QnextId = $OSCOM_PDO->prepare('select max(orders_status_id) as orders_status_id 
                                              from :table_orders_status
                                            ');
              $QnextId->execute();

              $next_id = $QnextId->fetch();

              $orders_status_id = $next_id['orders_status_id'] + 1;
            }

            $insert_sql_data = array('orders_status_id' => $orders_status_id,
                                     'language_id' => $language_id);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            $OSCOM_PDO->save('orders_status', $sql_data_array);

          } elseif ($action == 'save') {

            $OSCOM_PDO->save('orders_status', $sql_data_array, ['orders_status_id' => (int)$orders_status_id,
                                                                'language_id' => (int)$language_id
                                                               ]
                            );
          }
        }

        if (isset($_POST['default']) && ($_POST['default'] == 'on')) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', $orders_status_id);
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_ORDERS_STATUS_ID');
          $Qupdate->execute();
        }

        osc_redirect(osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status_id));
        break;
      case 'deleteconfirm':
        $oID = osc_db_prepare_input($_GET['oID']);


        $QordersStatus = $OSCOM_PDO->prepare('select configuration_value 
                                               from :table_configuration
                                               where configuration_key = :configuration_key
                                              ');
        $QordersStatus->bindValue(':configuration_key', 'DEFAULT_ORDERS_STATUS_ID');
        $QordersStatus->execute();

        $orders_status = $QordersStatus->fetch();

        if ($orders_status['configuration_value'] == $oID) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                          set configuration_value = :configuration_value
                                          where configuration_key = :configuration_key
                                        ');
          $Qupdate->bindValue(':configuration_value', '' );
          $Qupdate->bindValue(':configuration_key', 'DEFAULT_ORDERS_STATUS_ID');
          $Qupdate->execute();
        }

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_orders_status
                                        where orders_status_id = :orders_status_id
                                        ');
        $Qdelete->bindInt(':orders_status_id', $oID);
        $Qdelete->execute();

        osc_redirect(osc_href_link('orders_status.php', 'page=' . $_GET['page']));
        break;

      case 'delete':
        $oID = osc_db_prepare_input($_GET['oID']);


        $Qstatus = $OSCOM_PDO->prepare('select count(*) as count  
                                        from :table_orders
                                        where orders_status = :orders_status
                                      ');
        $Qstatus->bindInt(':orders_status', (int)$oID );
        $Qstatus->execute();

        $status = $Qstatus->fetch();

        $remove_status = true;
        if ($oID == DEFAULT_ORDERS_STATUS_ID) {
          $remove_status = false;
          $OSCOM_MessageStack->add(ERROR_REMOVE_DEFAULT_ORDER_STATUS, 'error');
        } elseif ($status['count'] > 0) {
          $remove_status = false;
          $OSCOM_MessageStack->add(ERROR_STATUS_USED_IN_ORDERS, 'error');
        } else {

          $Qhistory = $OSCOM_PDO->prepare('select count(*) as count  
                                          from :table_orders_status_history
                                          where orders_status_id = :orders_status_id
                                        ');
          $Qhistory->bindInt(':orders_status_id', (int)$oID );
          $Qhistory->execute();

          $history = $Qhistory->fetch();

          if ($history['count'] > 0) {

            $remove_status = false;
            $OSCOM_MessageStack->add(ERROR_STATUS_USED_IN_HISTORY, 'error');
          }
        }
        break;
    }
  }

  require('includes/header.php');
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/order_status.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
<?php
  if (empty($action)) {
?>
            <td class="pageHeading" align="right"><?php echo '<a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_order_status.gif', IMAGE_INSERT) . '</a>'; ?></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDERS_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $orders_status_query_raw = "select * 
                              from orders_status
                              where language_id = '" . (int)$_SESSION['languages_id'] . "' 
                              order by orders_status_id
                            ";

  $orders_status_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $orders_status_query_raw, $orders_status_query_numrows);
  $orders_status_query = osc_db_query($orders_status_query_raw);

  while ($orders_status = osc_db_fetch_array($orders_status_query)) {

    if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders_status['orders_status_id']))) && !isset($oInfo) && (substr($action, 0, 3) != 'new')) {
      $oInfo = new objectInfo($orders_status);
    }
        
    if (isset($oInfo) && is_object($oInfo) && ($orders_status['orders_status_id'] == $oInfo->orders_status_id)) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status['orders_status_id']) . '\'">' . "\n";
    }

    if (DEFAULT_ORDERS_STATUS_ID == $orders_status['orders_status_id']) {
      echo '                <td class="dataTableContent"><strong>' . $orders_status['orders_status_name'] . ' (' . TEXT_DEFAULT . ')</strong></td>' . "\n";
    } else {
      echo '                <td class="dataTableContent">' . $orders_status['orders_status_name'] . '</td>' . "\n";
    }
?>
                <td class="dataTableContent" align="right">
<?php
    if ($orders_status['orders_status_id'] > 5) {
                 echo '<a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status['orders_status_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
    }
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status['orders_status_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($oInfo) && is_object($oInfo) && ($orders_status['orders_status_id'] == $oInfo->orders_status_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $orders_status['orders_status_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $orders_status_split->display_count($orders_status_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS); ?></td>
                    <td class="smallText" align="right"><?php echo $orders_status_split->display_links($orders_status_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_ORDERS_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status', 'orders_status.php', 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);

      $orders_status_inputs_string = '';
      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']');
      }

      $contents[] = array('text' => '<br />' . TEXT_INFO_ORDERS_STATUS_NAME . $orders_status_inputs_string);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('public_flag', '1') . ' ' . TEXT_SET_PUBLIC_STATUS);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('downloads_flag', '1') . ' ' . TEXT_SET_DOWNLOADS_STATUS);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('support_orders_flag','1') . ' ' . TEXT_SET_SUPPORT_ORDERS_STATUS);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<br />' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_ORDERS_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status', 'orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id  . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);

      $orders_status_inputs_string = '';
      $languages = osc_get_languages();
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $orders_status_inputs_string .= '<br />' . osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . osc_draw_input_field('orders_status_name[' . $languages[$i]['id'] . ']', osc_get_orders_status_name($oInfo->orders_status_id, $languages[$i]['id']));
      }

      $contents[] = array('text' => '<br />' . TEXT_INFO_ORDERS_STATUS_NAME . $orders_status_inputs_string);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('public_flag', '1', $oInfo->public_flag) . ' ' . TEXT_SET_PUBLIC_STATUS);
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('downloads_flag', '1', $oInfo->downloads_flag) . ' ' . TEXT_SET_DOWNLOADS_STATUS);
	  $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('support_orders_flag', '1', $oInfo->support_orders_flag) . ' ' . TEXT_SET_SUPPORT_ORDERS_STATUS);
      if (DEFAULT_ORDERS_STATUS_ID != $oInfo->orders_status_id) $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('default') . ' ' . TEXT_SET_DEFAULT);
      $contents[] = array('align' => 'center', 'text' => '<br />' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . ' <a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_ORDERS_STATUS . '</strong>');

      $contents = array('form' => osc_draw_form('status', 'orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id  . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $oInfo->orders_status_name . '</strong>');
      if ($remove_status) {
	    $contents[] = array('align' => 'center', 'text' => '<br />' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a>');
	  } else {
	    $contents[] = array('align' => 'center', 'text' => '<br />' . '<a href="' . osc_href_link('orders_status.php', 'page=' . $_GET['page'] . '&oID=' . $oInfo->orders_status_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a>');
	  }
      break;
    default:

      break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
