<?php
/*
 * tell_a_friend.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn() && (ALLOW_GUEST_TO_TELL_A_FRIEND == 'false')) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  if ( isset($_GET['products_id']) && !empty($_GET['products_id']) ) {

    $Qp = $OSCOM_PDO->prepare('select pd.products_name
                               from :table_products p, 
                                    :table_products_description pd 
                              where p.products_id = :products_id 
                              and p.products_status = 1 
                              and p.products_id = pd.products_id 
                              and pd.language_id = :language_id
                              ');
    $Qp->bindInt(':products_id', osc_get_prid($OSCOM_Products->getId() ));
    $Qp->bindInt(':language_id', $_SESSION['languages_id']);
    $Qp->execute();

    if ( $Qp->fetch() === false ) {
      osc_redirect(osc_href_link('products', 'id=' . $OSCOM_Products->getId()));
    }
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('tell_a_friend'));

  if (isset($_GET['action']) && ($_GET['action'] == 'process')  && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $error = false;

    $to_email_address = osc_db_prepare_input($_POST['to_email_address']);
    $to_name = osc_db_prepare_input($_POST['to_name']);
    $from_email_address = osc_db_prepare_input($_POST['from_email_address']);
    $from_name = osc_db_prepare_input($_POST['from_name']);
    $message = osc_db_prepare_input($_POST['message']);
    $number_email_confirmation = osc_db_prepare_input($_POST['number_email_confirmation']);

    if (empty($from_name)) {
      $error = true;

      $OSCOM_MessageStack->addError('friend', ERROR_FROM_NAME);
    }

    if (!osc_validate_email($from_email_address)) {
      $error = true;

      $OSCOM_MessageStack->addError('friend', ERROR_FROM_ADDRESS);
    }

    if (empty($to_name)) {
      $error = true;

      $OSCOM_MessageStack->addError('friend', ERROR_TO_NAME);
    }

    if (!osc_validate_email($to_email_address)) {
      $error = true;

      $OSCOM_MessageStack->addError('friend', ERROR_TO_ADDRESS);
    }

    $actionRecorder = new actionRecorder('ar_tell_a_friend', ($OSCOM_Customer->isLoggedOn() ? $OSCOM_Customer->getID() : null), $from_name);
    if (!$actionRecorder->canPerform()) {
      $error = true;

      $actionRecorder->record(false);

      $OSCOM_MessageStack->addError('friend', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_TELL_A_FRIEND_EMAIL_MINUTES') ? (int)MODULE_ACTION_RECORDER_TELL_A_FRIEND_EMAIL_MINUTES : 15)));
    }

    if (!osc_validate_email($to_email_address)) {
      $error = true;
      $OSCOM_MessageStack->addError('friend', ERROR_TO_ADDRESS);
    } elseif (!osc_validate_number_email($number_email_confirmation)) {
      $error = true;
      $OSCOM_MessageStack->addError('friend', ENTRY_EMAIL_ADDRESS_CHECK_ERROR_NUMBER);
    }  

    if ($error == false) {
      $email_subject = sprintf(TEXT_EMAIL_SUBJECT, $from_name, STORE_NAME);
      $email_body = sprintf(TEXT_EMAIL_INTRO, $to_name, $from_name, $OSCOM_Products->getProductsName(), STORE_NAME) . "\n\n";
      
      if (osc_not_null($message)) {
        $email_body .= $message . "\n\n";
      }

      $email_body .= sprintf(TEXT_EMAIL_LINK, osc_href_link('product_info.php', 'products_id=' . (int)$OSCOM_Products->getId(), 'NONSSL', false)) . "\n\n" .
                     sprintf(TEXT_EMAIL_SIGNATURE, STORE_NAME . "\n" . HTTP_SERVER . DIR_WS_CATALOG . "\n");

      $email_body = utf8_encode(html_entity_decode($email_body));
  
      osc_mail($to_name, $to_email_address, $email_subject, $email_body, $from_name, $from_email_address);

      $actionRecorder->record();

      $OSCOM_MessageStack->addSuccess('header', sprintf(TEXT_EMAIL_SUCCESSFUL_SENT, $OSCOM_Products->getProductsName(), osc_output_string_protected($to_name)));
      osc_redirect(osc_href_link('product_info.php', 'products_id=' . (int)$OSCOM_Products->getId() ));
    }
    
 // revoir cette partie   
  } elseif ($OSCOM_Customer->isLoggedOn()) {

    $Qaccount = $OSCOM_PDO->prepare('select customers_firstname, 
                                            customers_lastname, 
                                            customers_email_address 
                                      from :table_customers 
                                      where customers_id = :customers_id
                                    ');
    $Qaccount->bindInt(':customers_id', $OSCOM_Customer->getID());
    $Qaccount->execute();

    $from_name = $Qaccount->value('customers_firstname') . ' ' . $Qaccount->value('customers_lastname');
    $from_email_address = $Qaccount->value('customers_email_address');

  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('tell_a_friend.php', 'products_id=' . (int)$OSCOM_Products->getId() ));

// number for the antispam
  $number_confirmation = '(2 + trois)x1';
 
  require($OSCOM_Template->getTemplateFiles('tell_a_friend'));

