<?php
/**
 * manufacturers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Brands');

define('TABLE_HEADING_MANUFACTURERS', 'brands');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_HEADING_NEW_MANUFACTURER', 'New brand');
define('TEXT_HEADING_EDIT_MANUFACTURER', 'Edit brand');
define('TEXT_HEADING_DELETE_MANUFACTURER', 'Delete brand');
define('TABLE_HEADING_STATUS','Status');

define('TITLE_MANUFACTURER_GENERAL','General informations');
define('TEXT_MANUFACTURERS_DESCRIPTION','Description');
define('TITLE_MANUFACTURER_IMAGE','Image');

define('TEXT_MANUFACTURERS', 'brands:');
define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_PRODUCTS', 'Products:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');
define('TEXT_MANUFACTURERS_DESCRIPTION ','Description');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');
define('TEXT_PRODUCTS_IMAGE_VISUEL','Image preview');

define('TAB_DESCRIPTION','Description');
define('TAB_VISUEL','Image');
define('TAB_SEO','Metadatas');

define('TEXT_NEW_INTRO', 'Please fill out the following information for the new brand');
define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_MANUFACTURERS_NAME', 'Brands name:');
define('TEXT_MANUFACTURERS_NEW_IMAGE', 'New brands Image:');
define('TEXT_MANUFACTURERS_IMAGE', 'Brands Image:');
define('TEXT_MANUFACTURERS_DELETE_IMAGE', 'Not display image brand');
define('HELP_IMAGE_MANUFACTURERS', '* Please not insert more than one image in the section New image.<br /><br />* Note : For the Firefox navigator users, you must click twice on the button source to use the wisiwyg correctly.<br />If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock +)');

define('TEXT_MANUFACTURERS_IMAGE_DELETE','Delete image');
define('TEXT_MANUFACTURERS_URL', 'Brands URL:');

define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this brand?');
define('TEXT_DELETE_IMAGE', 'Delete brand image?');
define('TEXT_DELETE_PRODUCTS', 'Delete products from this brand? (including product reviews, products on special, upcoming products)');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>WARNING:</strong> There are %s products still linked to this brand!');

define('TITLE_AIDE_IMAGE', 'Note on the use of the images');
define('HELP_IMAGE_MANUFACTURERS', '<li>Please not insert more than one image in the section Categorie image label.</li><li>If you use the Firefox navigator, you must click twice on the button source to use the wisiwyg correctly.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock +)</li>');

define('TITLE_HELP_DESCRIPTION', 'Note on the use of the wysiwyg');
define('HELP_DESCRIPTION', '<li>To activate wysiwyg, you must click twice on Source button for Firefox users.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock more)</li>');

define('TITLE_MANUFACTURER_SEO','Metadatas for the brand');
define('TEXT_MANUFACTURER_SEO_TITLE','Title');
define('TEXT_MANUFACTURER_SEO_DESCRIPTION','Description');
define('TEXT_MANUFACTURER_SEO_KEYWORDS','keywords');

define('TITLE_HELP_SUBMIT', 'Note on the search engine optimization management');
define('HELP_SUBMIT', '<li>The system automatically manages the search engine optimization.<br />
<blockquote>It automatically included elements in the following order if the fields are not filled:<br />
Title : the name of your product (ex leather jacket)<br />
Description: The product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Category: Category Product<br />
</blockquote></li>
<li>If you fill in the fields, the tags appear in the following way:<br />
<blockquote>
Title: The title of fields, the name of your product (ex leather jacket)<br />
Description: The field description, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the fields keywords, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
</blockquote></li>
<li><strong>Please do not put too keywords (maximum 10)</strong> and establish consistency between each field to find the words the most important first</li>
');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: I can not write to this directory. Please set the right user permissions on: %s');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Error: Directory does not exist: %s');
?>