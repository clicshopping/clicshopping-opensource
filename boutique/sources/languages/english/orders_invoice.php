<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
// Tableau produits
define('TABLE_HEADING_QTE','Qty');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Price (ex)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Price (inc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ex)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (inc)');

// Cadre adresses facturation et livraison
define('ENTRY_SOLD_TO', 'SOLD TO:');
define('ENTRY_SHIP_TO', 'SHIP TO:');

// M�thode de paiement
define('ENTRY_PAYMENT_METHOD', 'Payment Method:');

// Affichage selon la m�thode d'affichage et le statut une Facture ou une Commande
define('ENTRY_NUMERO_INVOICE', 'Invoice number :');
define('ENTRY_NUMERO_ORDER', 'Order number :');
define('ENTRY_DATE_INVOICE', 'Invoice date :');
define('ENTRY_DATE_ORDER', 'Order date :');
define('ENTRY_INVOICE', 'INVOICE');
define('ENTRY_ORDER', 'ORDER');
define('PRINT_ORDER_DATE','Date : ');
// Email et URL de la soci�t�
define('ENTRY_HTTP_SITE','Web site : ');
define('PRINT_INVOICE_URL', HTTP_CATALOG_SERVER );

// Customer information
define('ENTRY_PHONE','Phone : ');
define('ENTRY_EMAIL','E-mail : ');
define('ENTRY_CUSTOMER_INFORMATION','Customer informations');
define('ENTRY_CUSTOMER_NUMBER','Customer number : ');

// Remerciement
define('THANK_YOU_CUSTOMER', 'We thank you for having placed an order with us. We hope to see you again soon.');

// Reserve de propri�t�
define('RESERVE_PROPRIETE', 'Legal proprieties : According with the laws ' . STORE_NAME . ' reserves the property of the goods delivered until the integral payment of the price.');
define('RESERVE_PROPRIETE_NEXT', 'For more informations, please consult our general conditions and terms on the website');
define('RESERVE_PROPRIETE_NEXT1', 'at the following address : '. HTTP_SERVER . '/boutique/' . SHOP_CODE_URL_CONDITIONS_VENTE);

// gestion de la double taxe ou non 
if (DISPLAY_DOUBLE_TAXE == 'false') {
  define('ENTRY_INFO_SOCIETE', '' . SHOP_CODE_CAPITAL . ' - ' . SHOP_CODE_RCS . ' - ' . SHOP_CODE_APE . ' ');
  define('ENTRY_INFO_SOCIETE_NEXT', ' '. TVA_SHOP_INTRACOM);
} else {
  define('ENTRY_INFO_SOCIETE', '' . SHOP_CODE_CAPITAL . ' - ' . SHOP_CODE_RCS . ' - ' . SHOP_CODE_APE . ' ');
  define('ENTRY_INFO_SOCIETE_NEXT', ' '. TVA_SHOP_PROVINCIAL . ' - ' .TVA_SHOP_FEDERAL . ' - ' . SHOP_DIVERS . ' ');
}
?>