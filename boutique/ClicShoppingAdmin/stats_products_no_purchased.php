<?php
/*
 * stats_products_no_purchased.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: stats_products_no_purchased.php
 */

  require('includes/application_top.php');

// Langues
  $languages = osc_get_languages();
  $languages_array = array();
  $languages_selected = DEFAULT_LANGUAGE;
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    $languages_array[] = array('id' => $languages[$i]['code'],
                               'text' => $languages[$i]['name']);
    if ($languages[$i]['directory'] == $_SESSION['language']) {
      $languages_selected = $languages[$i]['code'];
    }
  }

// initialisation form
  $form_action = 'update';

  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN - MAX_DISPLAY_SEARCH_RESULTS_ADMIN;

  $rows = 0;
  $products_query_raw = "select p.products_id, 
                                p.products_ordered,
                                p.products_image,
                                pd.products_name 
                        from products p,
                             products_description pd
                        where pd.products_id = p.products_id 
                       and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                       and p.products_archive = '0'
                       and p.products_ordered = 0 
                       group by pd.products_id 
                       order by p.products_ordered DESC, 
                                pd.products_name
                       ";
  $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_query_raw, $products_query_numrows);

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
           <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_products_purchased.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading" width="350"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>	
            <td class="smallText" valign="middle" align="center">
<?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?><br />
<?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
            </td>
<?php
  if (sizeof($languages_array) > 1) {
?>
            <td align="right"><?php echo osc_draw_form('adminlanguage', 'stats_products_no_purchased.php', '', 'get') .  osc_draw_pull_down_menu('language', $languages_array, $languages_selected, 'onchange="this.form.submit();"')  . osc_hide_session_id(); ?></form></td>
<?php
  }
?>                        
            <form name="stats_products_purchased" <?php echo 'action="' . osc_href_link('stats_products_no_purchased.php', 'action=' . $form_action . '&resetPurchased=0') . '"'; ?> method="post">
              <td align="right"><?php echo '<a href="' . osc_href_link('stats_products_no_purchased.php', 'resetPurchased=0&page=' . $page .'&action=update') . '"><strong>' . osc_image_submit('button_delete_stats_products_purchased.gif', IMAGE_DELETE) . '</strong></a>'; ?>&nbsp;</td>
            </form>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="20"></td> 
                <td width="50"></td>             
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NUMBER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_NO_PURCHASED; ?>&nbsp;</td>
              </tr>
<?php

  $products_query = osc_db_query($products_query_raw);
  while ($products = osc_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                <td class="dataTableContent" width="50"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id'] . '&origin=' . 'stats_products_viewed.php' . '?page=' . $_GET['page']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                <td class="dataTableContent"><?php echo $rows; ?>.</td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id'] . '&origin=' . 'stats_products_no_purchased.php' . '?page=' . $_GET['page']) . '">' . $products['products_name'] . '</a>'; ?></td>
                <td class="dataTableContent" align="center"><?php echo $products['products_ordered']; ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
