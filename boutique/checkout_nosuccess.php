<?php
/*
 * checkout_nosuccess.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
  if (!$OSCOM_Customer->isLoggedOn()) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_nosuccess'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1);
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2);

  require($OSCOM_Template->getTemplateFiles('checkout_nosuccess'));
