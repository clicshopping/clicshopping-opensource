<?php
/**
 * address_book_details.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license Licensed under GPL and MIT
 * @version $Id: 
*/

  if (!isset($process)) $process = false;
?>
  <div class="hr"></div>
  <div class="contentText">
      <h3><span class="text-warning pull-right"><?php echo FORM_REQUIRED_INFORMATION; ?></span></h3>
      <h3><?php echo ENTRY_COMPANY; ?></h3>
  </div>
  <div class="contentText">
<?php
// Clients B2C et B2B
// Nouvelle adresse : Affichage du nom societe par defaut si il existe dans la table customers.
// Edition adresse :  Affiche le nom de la societe present dans le carnet d'adresse table address_book.
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ACCOUNT_COMPANY == 'true')) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ACCOUNT_COMPANY_PRO == 'true'))) {

     $QaccountGroup = $OSCOM_PDO->prepare('select customers_company
                                           from :table_customers
                                           where customers_id = :customers_id
                                         ');
     $QaccountGroup->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
     $QaccountGroup->execute();

     $account_group = $QaccountGroup->fetch();
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <label class="control-label col-sm-4" for="company"><?php echo ENTRY_COMPANY; ?></label>
        <div class="controls col-sm-6">
<?php
    if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
?>
            <?php echo osc_draw_input_field('company', (isset($entry['entry_company']) ? $entry['entry_company'] : ''), 'id="company" placeholder="'.ENTRY_COMPANY.'"'); ?>
<?php
    } else {
?>
            <?php echo osc_draw_input_field('company', $account_group['customers_company'], 'id="company" placeholder="'.ENTRY_COMPANY.'"'); ?>
<?php
    }
    if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_COMPANY_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_COMPANY_PRO_MIN_LENGTH > 0))) {
      echo '&nbsp;' . (osc_not_null(ENTRY_COMPANY_TEXT) ? '<span class="text-warning">' . ENTRY_COMPANY_TEXT . '</span>': ''); 
    } 
?>
      </div>
    </div>
<?php
  }
?>

      <h3><?php echo CATEGORY_PERSONAL; ?></h3>
      <div class="hr"></div>
<?php
// Clients B2C et B2B : Informations general - Nom, prenom, date de naissance, email, telephone et fax
  if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
    $male = $female = false;
    if (isset($gender)) {
      $male = ($gender == 'm') ? true : false;
      $female = !$male;
    } elseif (isset($entry['entry_gender'])) {
      $male = ($entry['entry_gender'] == 'm') ? true : false;
      $female = !$male;
    }
?>

    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-2" for="gender"><?php echo ENTRY_GENDER; ?></label>
        <span class="text-warning pull-left col-sm-8">
          <?php echo osc_draw_radio_field('gender', 'm', $male) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . osc_draw_radio_field('gender', 'f', $female) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (osc_not_null(ENTRY_GENDER_TEXT) ? '<span class="text-warning">' . ENTRY_GENDER_TEXT . '</span>': '');?>
      </div>
    </div>

<?php
  }
?>

    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="firstname"><?php echo ENTRY_FIRST_NAME; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('firstname',  (isset($entry['entry_firstname']) ? $entry['entry_firstname'] : ''), 'required aria-required="true"  id="entry_first_name" placeholder="'.ENTRY_FIRST_NAME.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;
<?php
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_FIRST_NAME_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_FIRST_NAME_PRO_MIN_LENGTH > 0))) {
     echo '&nbsp;' . (osc_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="text-warning">' . ENTRY_FIRST_NAME_TEXT . '</span>': '');
  }
?>
          </span>
        </div>
      </div>
    </div>

    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_LAST_NAME; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo  osc_draw_input_field('lastname', (isset($entry['entry_lastname']) ? $entry['entry_lastname'] : ''), 'required aria-required="true"  id="entry_first_name" placeholder="'.ENTRY_LAST_NAME.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;
<?php

  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_LAST_NAME_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_LAST_NAME_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="text-warning">' . ENTRY_LAST_NAME_TEXT . '</span>': '');
  }
?>
          </span>
        </div>
      </div>
    </div>

<?php
  if ($_GET['newcustomer'] == 1) {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="telephone"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('telephone', null, 'required aria-required="true"  id="telephone" placeholder="'.ENTRY_TELEPHONE_NUMBER.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;<?php echo (osc_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '&nbsp;<span class="text-warning">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></span>
        </div>
      </div>
    </div>

<?php
    if (ACCOUNT_CELLULAR_PHONE == 'true') {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="cellular_phone"><?php echo ENTRY_CELLULAR_PHONE_NUMBER; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('cellular_phone', null, 'id="cellular_phone" placeholder="'.ENTRY_CELLULAR_PHONE_NUMBER.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;<?php echo (osc_not_null(ENTRY_CELLULAR_PHONE_NUMBER) ? '&nbsp;<span class="text-warning">' . ENTRY_CELLULAR_PHONE_NUMBER_TEXT  . '</span>': ''); ?></span>
        </div>
      </div>
    </div>
<?php
    }
    if (ACCOUNT_FAX == 'true') {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="fax"><?php echo ENTRY_FAX_NUMBER; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('fax', null, 'id="fax" placeholder="'.ENTRY_FAX_NUMBER.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;<?php echo (osc_not_null(ENTRY_FAX_NUMBER) ? '&nbsp;<span class="text-warning">' . ENTRY_FAX_NUMBER_TEXT  . '</span>': ''); ?></span>
        </div>
      </div>
    </div>
<?php
    }
  } // end $_GET['newcustomer']
?>
   <div class="contentText">
      <legend>
        <h3><?php echo NEW_ADDRESS_TITLE; ?></h3>
      </legend>
   </div>
   <div class="hr"></div>
   <div class="row">
     <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
     <div class="form-group has-feedback">
       <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_STREET_ADDRESS; ?></label>
       <div class="controls col-sm-6">
         <span class="text-warning pull-left"><?php echo osc_draw_input_field('street_address', (isset($entry['entry_street_address']) ? $entry['entry_street_address'] : ''), 'id="street_address" placeholder="'.ENTRY_STREET_ADDRESS.'"'); ?></span>
         <span class="text-warning pull-left">&nbsp;
<?php
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_STREET_ADDRESS_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="text-warning">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': '');
  }
?>
         </span>
       </div>
     </div>
   </div>

<?php
  if (((ACCOUNT_SUBURB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_SUBURB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_SUBURB; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('suburb', (isset($entry['entry_suburb']) ? $entry['entry_suburb'] : ''), ' id="suburb" placeholder="'.ENTRY_SUBURB.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;
            <?php echo (osc_not_null(ENTRY_SUBURB_TEXT) ? '<span class="text-warning">' . ENTRY_SUBURB_TEXT . '</span>': ''); ?>
          </span>
        </div>
      </div>
    </div>
<?php
  }
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_POST_CODE; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('postcode', (isset($entry['entry_postcode']) ? $entry['entry_postcode'] : ''), 'required aria-required="true" id="postcode" placeholder="'.ENTRY_POST_CODE.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;
<?php
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_POSTCODE_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_POSTCODE_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="text-warning">' . ENTRY_POST_CODE_TEXT . '</span>': '');
  }
?>
          </span>
        </div>
      </div>
    </div>


    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_CITY; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('city', (isset($entry['entry_city']) ? $entry['entry_city'] : ''), 'required aria-required="true" id="city" placeholder="'.ENTRY_CITY.'"'); ?></span>
          <span class="text-warning pull-left">&nbsp;
<?php
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_CITY_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_CITY_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_CITY_TEXT) ? '<span class="text-warning">' . ENTRY_CITY_TEXT . '</span>': '');
  }
?>
          </span>
        </div>
      </div>
    </div>


    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-4" for="lastname"><?php echo ENTRY_COUNTRY; ?></label>
        <div class="controls col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_get_country_list('country', (isset($entry['entry_country_id']) ? $entry['entry_country_id'] : STORE_COUNTRY)); ?></span>
          <span class="text-warning pull-left">&nbsp;
            <?php echo (osc_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="text-warning">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?>
          </span>
        </div>
      </div>
    </div>

<?php
  if (((ACCOUNT_STATE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_STATE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="control-group">
        <label class="control-label col-sm-4" for="suburb"><?php echo ENTRY_STATE; ?></label>
        <div class="controls col-sm-4">
<?php
    if ($process == true) {
      if ($entry_state_has_zones == true) {
        $zones_array = array();
        $zones_query = osc_db_query("select zone_name from zones
                                     where zone_country_id = '" . (int)$country . "' 
                                     order by zone_name
                                  ");
        while ($zones_values = osc_db_fetch_array($zones_query)) {
          $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
        }
        echo osc_draw_pull_down_menu('state', $zones_array);
      } else {
        echo osc_draw_input_field('state', null, 'id="state" placeholder="'.ENTRY_STATE.'"');
      }
    } else {
      echo osc_draw_input_field('state', (isset($entry['entry_country_id']) ? osc_get_zone_name($entry['entry_country_id'], $entry['entry_zone_id'], $entry['entry_state']) : ''), 'id="state" placeholder="'.ENTRY_STATE.'"');
    }

    if (((osc_not_null(ENTRY_STATE_TEXT)) && (ENTRY_STATE_MIN_LENGTH > 0) && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((osc_not_null(ENTRY_STATE_TEXT)) && (ENTRY_STATE_PRO_MIN_LENGTH > 0) && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
       echo '&nbsp;<span class="text-warning">' . ENTRY_STATE_TEXT .'</span>';
    }
?>
        </div>
      </div>
    </div>

<?php
  }

  if ($_GET['newcustomer'] != 1) {
//   Allow or not to customer change this address ou to change the default address if oddo is activated.
    if ((isset($_GET['edit']) && ($OSCOM_Customer->getDefaultAddressID() != $_GET['edit']) && (osc_count_customers_modify_address_default() == 1)) || (isset($_GET['edit']) == false) && (osc_count_customers_modify_address_default() == 1)) {
      if ( ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG == 'true' || ODOO_ACTIVATE_WEB_SERVICE == 'false') {
?>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="checkbox">
        <label>
          <?php echo osc_draw_checkbox_field('primary', 'on', false, 'id="primary"') . ' ' . SET_AS_PRIMARY; ?>
        </label>
      </div>
<?php
      }
    }
  } else {
    echo osc_draw_hidden_field('primary', 'on', true, 'id="primary"');
  }
?>

  </div>