<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Reviews');
define('HEADING_TITLE', 'Customers Comments ');
define('TEXT_CLICK_TO_ENLARGE', 'Click to enlarge');

define('TEXT_OF_5_STARS', '%s of 5 Stars!');
define('TABLE_HEADING_NEW_PRODUCTS','&nbsp;');
?>