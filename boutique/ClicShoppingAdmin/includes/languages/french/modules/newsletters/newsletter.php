<?php
/**
 * newsletters.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('TEXT_COUNT_CUSTOMERS', 'Clients recevant le bulletin d\'informations : %s');
define('TEXT_COUNT_CUSTOMERS_NO_ACCOUNT', 'Clients sans compte recevant le bulletin d\'informations : %s');

define('TEXT_UNSUBSCRIBE','Se désabonner');

define('TEXT_SEND_NEWSLETTER_EMAIL','<p style="font-size: 10px; text-align: center;">Pour être sur de recevoir tous nos emails, veuillez ajouter <strong>'.STORE_OWNER_EMAIL_ADDRESS.'</strong> à votre carnet d\'adresses.</p><br/>');

define('TEXT_SEND_NEWSLETTER','<p style="font-size: 10px; text-align: center;">Si vous ne visualisez pas la newsletter de '.STORE_NAME.', copiez l\'URL suivante dans votre navigateur Internet pour la visualiser correctement.</p><br/>');

define('TEXT_FILE_NEWSLETTER','Nom du fichier newsletter généré : ');
define('TEXT_FILE_DIRECTORIES','Répertoire : ');
define('TEXT_TWITTER','Retrouvez notre dernière newsletter parue sur '.STORE_NAME.' : ');
?>
