<?php
/*
 * account_history_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Order   -----  
// ---------------------- 
?>
<div class="clearfix"></div>
<div class="contentContainer">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <div class="contentText">
      <strong><?php echo sprintf(HEADING_ORDER_NUMBER, $_GET['order_id']) . ' <small>(' . $order->info['orders_status'] . ')</small>'; ?></strong>
  </div>
<?php
// ---------------------- 
// --- Summary order   -----  
// ---------------------- 
?>

  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <div class="hr"></div>
<?php
// ----------------------------- 
// --- Blling information   -----  
// ---------------------- ------
?>
  <h3><?php echo HEADING_BILLING_INFORMATION; ?></h3>
  <div class="contentText">

    <div class="panel panel-default">
      <div class="panel-heading"><strong><?php echo sprintf(HEADING_ORDER_NUMBER, $_GET['order_id']) . ' <span class="badge pull-right">' . $order->info['orders_status'] . '</span>'; ?></strong></div>
      <div class="panel-body">

        <table border="0" width="100%" cellspacing="0" cellpadding="2" class="table-hover order_confirmation">
<?php
  if (sizeof($order->info['tax_groups']) > 1) {
?>
          <tr>
            <th colspan="2"><strong><?php echo HEADING_PRODUCTS; ?></strong></th>
            <th align="right"><strong><?php echo HEADING_TAX; ?></strong></th>
            <th align="right"><strong><?php echo HEADING_TOTAL; ?></strong></th>
          </tr>
<?php
  } else {
?>
          <tr>
            <th colspan="2"><strong><?php echo HEADING_PRODUCTS; ?></strong></th>
            <th align="right"><strong><?php echo HEADING_TOTAL; ?></strong></th>
          </tr>
<?php
  }

  for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
    echo '          <tr>' . "\n" .
         '            <td align="right" valign="top" width="30">' . $order->products[$i]['qty'] . '&nbsp;x&nbsp;</td>' . "\n" .
         '            <td valign="top">' . $order->products[$i]['name'];

    if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        echo '<br /><nobr><small>&nbsp;<i> - <strong>'. $order->products[$i]['attributes'][$j]['reference'] . '</strong> ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . '</i></small></nobr>';
      }
    }

    echo '</td>' . "\n";

    if (sizeof($order->info['tax_groups']) > 1) {
      echo '            <td valign="top" align="right">' . osc_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n";
    }

    echo '            <td align="right" valign="top">' . $currencies->format(osc_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</td>' . "\n" .
         '          </tr>' . "\n";
  }
?>
         
    </table>
    <div style="height:10px;"></div>
    <div class="hr"></div>
<?php
// ---------------------- 
// --- Total order   -----  
// ---------------------- 
?>
        <table width="100%" class="pull-right">
<?php
  for ($i=0, $n=sizeof($order->totals); $i<$n; $i++) {
    echo '              <tr>' . "\n" .
         '                <td align="right"  width="80%">' . $order->totals[$i]['title'] . '&nbsp;</td>' . "\n" .
         '                <td align="right" width=20%">' . $order->totals[$i]['text'] . '</td>' . "\n" .
         '              </tr>' . "\n";
  }
?>
        </table>
      </div>
      <div class="panel-footer">
        <?php echo HEADING_ORDER_DATE . ' ' .  $OSCOM_Date->getLong($order->info['date_purchased']); ?>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>

<?php
// --------------------------- 
// --- Delivery Adress   -----  
// --------------------------- 
?>
    <div class="row">
<?php
  if ($order->delivery != false) {
?>
      <div class="col-sm-6">
        <div class="panel-default">
          <div class="panel-heading"><?php echo '<strong>' . HEADING_DELIVERY_ADDRESS . '</strong>'; ?></div>
          <div class="panel-body">
            <?php echo osc_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />'); ?>
          </div>
       </div>
      </div> 
      <div class="col-sm-6">          
<?php
    if (osc_not_null($order->info['shipping_method'])) {
?>
        <div class="panel-default">
          <div class="panel-heading"><strong><?php echo HEADING_SHIPPING_METHOD; ?></strong></div>
          <div class="panel-body">
            <?php echo $order->info['shipping_method']; ?>
          </div>
        </div>
<?php
    }
  }
?>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-sm-6">
        <div class="panel-default">
          <div class="panel-heading"><strong><?php echo HEADING_BILLING_ADDRESS; ?></strong></div>
          <div class="panel-body">
            <?php echo osc_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></td>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel-default">
          <div class="panel-heading"><strong><?php echo HEADING_PAYMENT_METHOD; ?></strong></div>
          <div class="panel-body">
            <?php echo $order->info['payment_method']; ?>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="hr"></div>



<script type="text/javascript"><!--
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=600,height=600,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
      <h3><?php echo HEADING_ORDER_CONDITIONS_OF_SALES; ?></h3>
      <div class="contentText">
        <div class="glyphicon glyphicon-arrow-right">&nbsp;<?php echo '<a href="javascript:popupImageWindow(\'' . 'popup_page_manager_account_history.php' .'?order_id='. (int)$_GET['order_id'].'&customer_id='.$OSCOM_Customer->getID().'\')">' . TEXT_CONDITION_GENERAL_OF_SALES . '</a>'; ?></div>
      </div>
<?php
// ---------------------- 
// --- order history   -----  
// ---------------------- 
?>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="hr"></div>
      <h3><?php echo HEADING_ORDER_HISTORY; ?></h3>

    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<?php
  while ($statuses = $Qstatuse->fetch()) {
        echo '<div class="col-sm-12">';
        echo '<span class="text-muted"><i class="glyphicon glyphicon-time"></i> ' . $OSCOM_Date->getShort($statuses['date_added']) . '</span>';
        echo '<span style="padding-left:20px;">' . $statuses['orders_status_name'] . '</span>';
        echo '<div>';
        echo '<p>' . (empty($statuses['comments']) ? '&nbsp;' : '<blockquote>' . nl2br(osc_output_string_protected($statuses['comments'])) . '</blockquote>') . '</p>';
        echo '</div>';
        echo '</div>';
  }
?>        
      <div class="clearfix"></div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="hr"></div>
      <h3><?php echo HEADING_FOLLOW_TRACKING; ?></h3>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div>
<?php
  while ($statuses_shipping = $Qtracking->fetch()) {
   if (!empty($statuses_shipping['orders_tracking_number']) ) {
     $orders_tracking .= ' <div class="row">';
     $orders_tracking .= '<span class="pull-left col-sm-5">'. TEXT_TRACKING_SERVICES .'<br /> '. osc_tracking_link($tracking_url) .'</span>';
     $orders_tracking .= '<span class="col-sm-5">' . $statuses_shipping['orders_status_tracking_name'] . ' : <a href=" '.$statuses_shipping['orders_status_tracking_link']. $statuses_shipping['orders_tracking_number'].'" target="_blank">' . $statuses_shipping['orders_tracking_number'] .'</a></span>';
     $orders_tracking .= '</div>';
   }
    echo ' <div><span>' . $orders_tracking . '</span></div>';
  }
?>
      </div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="clearfix"></div>
      <div><?php echo HEADING_FOLLOW_TRACKING_TEXT; ?></div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="glyphicon glyphicon-arrow-right">&nbsp;<?php echo '<a href="' . osc_href_link('contact_us.php',  'order_id=' . $_GET['order_id'], 'SSL') . '">' . TEXT_MY_SUPPORT . '</a>'; ?></div>
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="clearfix"></div>
      <div class="hr"></div>
      <h3><?php echo TEXT_INVOICE_DOWNLOAD; ?></h3>
      <div style="text-align:center;"><?php echo $print_invoice_pdf; ?></div>
      <div class="hr"></div>
      <div style="padding-top:10px;">
<?php
    while ($DonwloadProductsFiles = $QdonwloadProductsFiles->fetch()) {
      $download_files_agree = $DonwloadProductsFiles['products_download_filename'];
      if (!empty($download_files_agree)) {
        $download_files .= TEXT_DOWNLOAD_FILES . ': <a href="' . osc_href_link(DIR_WS_SOURCES . 'public/download/' . $download_files_agree, '', 'SSL') . '" target="_blank">' . $download_files_agree . '</a>';
      }
?>
        <div><?php echo $download_files; ?></div>
<?php 
    }
?>
  </div>
<?php
// ---------------------- 
// --- Download   -----
// ---------------------- 
  if (DOWNLOAD_ENABLED == 'true') require($OSCOM_Template->getTemplateModules('download/downloads'));

// ----------------------
// --- Button   -----  
// ---------------------- 
?>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="hr"></div>
    <div class="clearfix"></div>   
    <div class="buttonSet">
      <?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('account_history.php', osc_get_all_get_params(array('order_id')), 'SSL'), 'primary', null, null); ?>
    </div>

 </div>
<?php  require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>