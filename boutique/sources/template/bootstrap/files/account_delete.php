<?php
/*
 * account_customers_delete_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  echo osc_draw_form('delete_account', osc_href_link('account_delete.php', '', 'SSL'), 'post', '', true) . osc_draw_hidden_field('action', 'process');

?>
  <div class="row">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  </div>
  <div class="contentContainer">
    <div class="contentText">
      <div class="form-group">
        <label class="control-label"><strong><?php echo TEXT_MODULE_ACCOUNT_CUSTOMERS_DELETE_ACCOUNT_INTRO; ?></strong></label>
        <blockquote><?php echo TEXT_MODULE_ACCOUNT_CUSTOMERS_DELETE_ACCOUNT_CHECKBOX; ?>
          <label class="checkbox-inline" style="padding-bottom:40px;">
            <?php echo osc_draw_checkbox_field('delete_customers_account_checkbox','1'); ?>
          </label>
        </blockquote>
      </div>
    </div>
  </div>
<?php
// ----------------------
// --- Button   -----
// ----------------------
?>
  <div class="buttonSet row">
    <div class="col-xs-6"><?php  echo osc_draw_button(IMAGE_BUTTON_BACK,null, osc_href_link('account.php', '', 'SSL'), 'primary', null, null);  ?></div>
    <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null);  ?></div>
  </div>
</form>

<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>