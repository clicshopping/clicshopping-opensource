<?php
/*
 * account_customers_delete_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_delete'));

  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {


    if (isset($_POST['delete_customers_account_checkbox']) && is_numeric($_POST['delete_customers_account_checkbox'])) {
      $delete_customers_account_checkbox = osc_db_prepare_input($_POST['delete_customers_account_checkbox']);
    } else {
      $delete_customers_account_checkbox = '0';
    }

    if ($delete_customers_account_checkbox == '1') {


      $QcustomerEmail = $OSCOM_PDO->prepare('select customers_email_address,
                                                    customers_firstname,
                                                    customers_lastname
                                             from :table_customers
                                             where customers_id = :customers_id
                                            ');
      $QcustomerEmail->bindValue(':customers_id', $OSCOM_Customer->getID());
      $QcustomerEmail->execute();

      $customerEmail = $QcustomerEmail->fetch();

      $text_email .= html_entity_decode(EMAIL_TEXT_MESSAGE) . "\n" .

      osc_mail($customer['customers_firstname'] . ' ' . $customer['customers_lastname'], $customer['customers_email_address'], EMAIL_TEXT_SUBJECT, $text_email, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);


      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_customers
                                      where customers_id = :customers_id
                                    ');
      $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qdelete->execute();

      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_customers_basket
                                      where customers_id = :customers_id
                                    ');
      $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qdelete->execute();


      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_customers_basket_attributes
                                      where customers_id = :customers_id
                                    ');
      $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qdelete->execute();


      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_customers_info
                                      where customers_info_id = :customers_id
                                    ');
      $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qdelete->execute();


      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_address_book
                                      where customers_id = :customers_id
                                    ');
      $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qdelete->execute();

      $OSCOM_Customer->reset();
      $_SESSION['cart']->reset();

      osc_redirect(osc_href_link('index.php', '', 'SSL'));

    } else {

      osc_redirect(osc_href_link('account.php', '', 'SSL'));
    }
  }


  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_delete.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('account_delete'));
