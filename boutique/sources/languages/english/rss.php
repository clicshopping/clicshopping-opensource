<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'RSS');
define('HEADING_TITLE', 'RSS');
define('BOX_INFORMATION_RSS','The latest news on '.STORE_NAME.' ');
define('IMAGE_BUTTON_MORE_INFO','More informations');
//define('BOX_INFORMATION_RSS','The latest news on '.STORE_NAME.' ');
?>
