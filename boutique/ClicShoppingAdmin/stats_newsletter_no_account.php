<?php
/*
 * stats_newsletter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

  require('includes/application_top.php');

  $resetViewed = (isset($_GET['resetViewed']) ? $_GET['resetViewed'] : '');
  
// print_r($_GET); 
  
  if (osc_not_null($_GET['resetViewed'])) {
    if ($_GET['resetViewed'] == '0') {

// Reset ALL counts
        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_newsletter_no_account
                                      ');
        $Qdelete->execute();

    } else {
// Reset selected product count

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_newsletter_no_account
                                        where customers_email_address = :customers_email_address
                                      ');
        $Qdelete->bindInt(':customers_email_address',  (int)$_GET['resetViewed']);
        $Qdelete->execute();
    }
  }

// Langues
  $languages = osc_get_languages();
  $languages_array = array();
  $languages_selected = DEFAULT_LANGUAGE;
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    $languages_array[] = array('id' => $languages[$i]['code'],
                               'text' => $languages[$i]['name']);
    if ($languages[$i]['directory'] == $_SESSION['language']) {
      $languages_selected = $languages[$i]['code'];
    }
  }

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
           <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/newsletters.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
            <td align="right" width="40%"><?php echo osc_draw_form('adminlanguage', 'stats_newsletter_no_account.php', '', 'get') .  osc_draw_pull_down_menu('language', $languages_array, $languages_selected, 'onchange="this.form.submit();"')  . osc_hide_session_id(); ?></td>
          </form>
<?php echo osc_hide_session_id(); ?>
             <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>          
            <td align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right">
                  <div class="form-group">
                    <div class="controls">
<?php
  echo osc_draw_form('search', 'stats_newsletter_no_account.php', '', 'get');
  echo osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'" size="12"');
?>
                    </div>
                  </div>
                </td>
<?php
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
?>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
                <td><?php echo '<a href="' . osc_href_link('stats_newsletter_no_account.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></td>
<?php
    }
?>
              </tr>
            </table></td>
<?php echo osc_hide_session_id(); ?>
          </form>          
          
          
             <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
            <td align="right"><?php echo '<a href="' . osc_href_link('stats_newsletter_no_account.php', 'resetViewed=0&page=' . $page) . '"><strong>' . osc_image_submit('button_delete_big.gif', IMAGE_DELETE) . '</strong></a>'; ?>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIRST_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LAST_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ADDRESS_EMAIL; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_ADDED; ?>&nbsp;</td>                
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_CLEAR; ?>&nbsp;</td>
              </tr>
<?php

  if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN - MAX_DISPLAY_SEARCH_RESULTS_ADMIN;
  $rows = 0;

// Recherche
    $search = '';
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "and (customers_email_address like '%" . $keywords . "%' or  customers_lastname like '%" . $keywords . "%' or customers_date_added like '%" . $keywords . "%') ";
    }
	

  $products_query_raw = "select *
                        from newsletter_no_account
                        where languages_id = ".(int)$_SESSION['languages_id'] ."
                        " . $search . " 
                        order by customers_date_added ASC
                       ";
  $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_query_raw, $products_query_numrows);
  $products_query = osc_db_query($products_query_raw);
  while ($products = osc_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href=''">
                <td class="dataTableContent"><?php echo $products['customers_firstname'] ; ?></td>
                <td class="dataTableContent"><?php echo $products['customers_lastname'] ; ?></td>
                <td class="dataTableContent"><?php echo $products['customers_email_address'] ; ?></td>                
                <td class="dataTableContent" align="center"><?php echo $products['customers_date_added']; ?>&nbsp;</td>
                <td class="dataTableContent" align="right"><?php echo '<a href="' . osc_href_link('stats_newsletter_no_account.php', 'resetViewed=' . $products['customers_email_address'] . '&page=' . $page) . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>'; ?>&nbsp;</td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

