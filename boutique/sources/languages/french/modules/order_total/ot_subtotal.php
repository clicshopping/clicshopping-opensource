<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


  define('MODULE_ORDER_TOTAL_SUBTOTAL_TITLE', 'Sous-Total');
  define('MODULE_ORDER_TOTAL_SUBTOTAL_DESCRIPTION', 'Commande Sous-Total<br /><br /><font color="#FF0000"><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut</font>');
?>