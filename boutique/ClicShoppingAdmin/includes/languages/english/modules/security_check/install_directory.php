<?php
/**
 * install_directory.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Installation directory exists at: ' . DIR_FS_CATALOG . 'install. Please remove this directory for security reasons.');
?>
