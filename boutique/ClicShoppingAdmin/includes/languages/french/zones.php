<?php
/**
 * zone.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Départements ou Etats');

define('TABLE_HEADING_COUNTRY_NAME', 'Pays');
define('TABLE_HEADING_ZONE_NAME', 'Zones');
define('TABLE_HEADING_ZONE_CODE', 'Code');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_ZONES_NAME', 'Nom de la zone :');
define('TEXT_INFO_ZONES_CODE', 'Code de la zone :');
define('TEXT_INFO_COUNTRY_NAME', 'Pays :');
define('TEXT_INFO_INSERT_INTRO', 'Merci d\'entrer la nouvelle zone avec ses données liées');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer cette zone ?');
define('TEXT_INFO_HEADING_NEW_ZONE', 'Nouvelle zone');
define('TEXT_INFO_HEADING_EDIT_ZONE', 'Editer zone');
define('TEXT_INFO_HEADING_DELETE_ZONE', 'Supprimer zone');
?>