<?php
/**
 * specials.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Promotions produits');

define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Prix du Produits');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_PRODUCTS_GROUP','Groupe client');
define('TABLE_HEADING_START_DATE','Date Début');
define('TABLE_HEADING_EXPIRES_DATE','Date expiration');
define('TABLE_HEADING_ARCHIVE','Produits archivés');
define('TABLE_HEADING_MODEL','Réf');
define('TABLE_HEADING_PERCENTAGE','Pourcentage');
define('TABLE_HEADING_FLASH_DISCOUNT','Flash');

define('TABLE_HEADING_SCHEDULED_DATE','Date début');
define('TEXT_SPECIALS_PRODUCT', 'Produit :');
define('TEXT_SPECIALS_SPECIAL_PRICE', 'Prix promotionnel hors taxes :');
define('TEXT_SPECIALS_EXPIRES_DATE', 'Date d\'expiration :');
define('TEXT_SPECIALS_GROUPS', 'Groupe Client :');
define('TEXT_SPECIALS_FLASH_DISCOUNT', 'S\'agit-il d\'une promotion flash ?');

define('TEXT_NEW_SPECIALS_TWITTER','Nouvelle promotion sur '. STORE_NAME  .' ! ');
define('TEXT_FLASH_DISCOUNT_TWITTER','ATTENTION : Vente Flash maintenant et pas après ! ');

define('TEXT_SPECIALS_TWITTER','Souhaitez vous publier cette promotion  ou cette remise flash sur Twitter ? ');
define('TEXT_YES','Oui');
define('TEXT_NO','non');
define('TEXT_STATUS','Statut du produit');

define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'dernière modification :');
define('TEXT_INFO_NEW_PRICE', 'Nouveau Prix :');
define('TEXT_INFO_ORIGINAL_PRICE', 'Prix original :');
define('TEXT_INFO_PERCENTAGE', 'Pourcentage :');
define('TEXT_INFO_START_DATE', 'Commence le :');
define('TEXT_INFO_EXPIRES_DATE', 'Expire le :');
define('TEXT_INFO_STATUS_CHANGE', 'Changement de statut :');

define('TEXT_INFO_HEADING_DELETE_SPECIALS', 'Supprimer promotion');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer cette promotion ?');
define('TEXT_SPECIALS_START_DATE', 'Date de début');

define('TITLE_SPECIALS_GENERAL', 'Informations sur le produit');
define('TITLE_SPECIALS_GROUPE', 'Prix et Groupe client');
define('TITLE_SPECIALS_DATE', 'Date d\'affichage');
define('TITLE_AIDE_SPECIALS_PRICE', 'Note sur les promotions');

define('TEXT_AIDE_SPECIALS_PRICE', '<ul><li>Vous pouvez entrer un pourcentage dans le champ prix remisé, par exemple : <strong>20%</strong></li>
<li>Si vous entrez un prix, le séparateur décimal doit être un \'.\' (point décimal), exemple: <strong>49.99</strong></li>
<li>Laissez la date d\'expiration vide pour aucune expiration.</li>
<li>La date de début a des droits supérieurs sur la date d\'expiration.</li>
<li>Si vous souhaitez réinitialiser les dates, veuillez changer de statut dans la page générale.</li>
<li>La publication d\'une information sur Twitter ne fonctionne qu\'à la création du produit (avec un statut sur on et avec un groupe client normal). Si le nom de votre boutique + URL de votre produit fait plus de 140 caractères, la promotion ne sera pas publiée (contrainte forte)</li>
<li>Les promotions sont automatiquement mises à jour (la partie catalogue de la boutique gère cet élément), il est inutile de changer le statut de la promotion après son enregistrement.</li>
<li>Les promotions flash sont liées à la date d\'expiration du produit et sa date de mise en ligne.</li>
</ul>');
?>
