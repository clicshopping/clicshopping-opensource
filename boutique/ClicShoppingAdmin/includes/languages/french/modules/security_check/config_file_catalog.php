<?php
/**
 * config_file_catalog.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_CONFIG_FILE_WRITEABLE', 'Je suis capable d\'écrire dans le fichier de configuration du système : ' . DIR_FS_CATALOG . 'includes/configure.php. C\'est une vulnérabilité potentielle importante - veuillez mettre des droits 444 sur ce fichier.');
?>
