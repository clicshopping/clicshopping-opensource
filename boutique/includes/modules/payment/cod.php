<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class cod {
    var $code, $title, $description, $enabled;

// class constructor
    function cod() {
      global $order;

      $this->code = 'cod';
      if (MODULE_PAYMENT_COD_NAME_TITLE != 'MODULE_PAYMENT_COD_NAME_TITLE') {
        $this->title = MODULE_PAYMENT_COD_NAME_TITLE;
      } else {
        $this->title = MODULE_PAYMENT_COD_TEXT_TITLE;
      }
      $this->description = MODULE_PAYMENT_COD_TEXT_DESCRIPTION;
      $this->sort_order = defined('MODULE_PAYMENT_COD_SORT_ORDER') ? MODULE_PAYMENT_COD_SORT_ORDER : 0;

// Activation module de paiement selon les groupes B2B
      if ( osc_get_payment_unallowed($this->code) ) {
        $this->enabled = defined('MODULE_PAYMENT_COD_STATUS') && (MODULE_PAYMENT_COD_STATUS == 'True') ? true : false;
      }

      if ((int)MODULE_PAYMENT_COD_ORDER_STATUS_ID > 0) {
        $this->order_status = defined('MODULE_PAYMENT_COD_ORDER_STATUS_ID') && ((int)MODULE_PAYMENT_COD_ORDER_STATUS_ID > 0) ? (int)MODULE_PAYMENT_COD_ORDER_STATUS_ID : 0;
      }

      if ( $this->enabled === true ) {
        if ( isset($order) && is_object($order) ) {
          $this->update_status();
        }
      }
    }

// class methods
    function update_status() {
      global $order;

      if ( ($this->enabled == true) && ((int)MODULE_PAYMENT_COD_ZONE > 0) ) {
        $check_flag = false;
        $check_query = osc_db_query("select zone_id 
                                    from zones_to_geo_zones
                                    where geo_zone_id = '" . MODULE_PAYMENT_COD_ZONE . "' 
                                    and zone_country_id = '" . $order->delivery['country']['id'] . "' 
                                    order by zone_id
                                  ");
        while ($check = osc_db_fetch_array($check_query)) {
          if ($check['zone_id'] < 1) {
            $check_flag = true;
            break;
          } elseif ($check['zone_id'] == $order->delivery['zone_id']) {
            $check_flag = true;
            break;
          }
        }

        if ($check_flag == false) {
          $this->enabled = false;
        }
      }

// disable the module if the order only contains virtual products
      if ($this->enabled == true) {
        if ($order->content_type == 'virtual') {
          $this->enabled = false;
        }
      }
    }

    function javascript_validation() {
      return false;
    }

    function selection() {
      if (MODULE_PAYMENT_COD_IMAGE) {
        $this->title_selection = $this->title . osc_draw_separator('pixel_trans.gif', '10', '1') . osc_image(DIR_WS_IMAGES . 'logos/payment/' . MODULE_PAYMENT_COD_IMAGE);
      } else {
        $this->title_selection = $this->title;
      }

      return array('id' => $this->code,
                   'module' => $this->title_selection);
      }


    function pre_confirmation_check() {
      return false;
    }

    function confirmation() {
      return false;
    }

    function process_button() {
      return false;
    }

    function before_process() {
      return false;
    }

    function after_process() {
      return false;
    }

    function get_error() {
      return false;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = osc_db_query("select configuration_value from configuration where configuration_key = 'MODULE_PAYMENT_COD_STATUS'");
        $this->_check = osc_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Paiement &agrave; la livraison', 'MODULE_PAYMENT_COD_STATUS', 'True', 'Voulez-vous accepter le paiement &agrave la livraison ?', '6', '1', 'osc_cfg_select_option(array(\'True\', \'False\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Titre', 'MODULE_PAYMENT_COD_NAME_TITLE', 'Paiement &agrave; la livraison de votre commande', 'Permet de modifier le titre du module qui s\'affichera dans la page paiement de la boutique.', '6', '11', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Logo', 'MODULE_PAYMENT_COD_IMAGE', 'paiement_livraison.jpg', 'Veuillez indiquer le nom de l\'image avec son extension (.gif, .jpg, etc...) &agrave; afficher dans la page paiement de la boutique.<br /><br /><font color=\"#FF0000\"><strong>Note :</strong></font> Le logo doit ce trouver dans le dossier images/logos/payment/', '6', '12', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, use_function, date_added) values ('Etat de la commande', 'MODULE_PAYMENT_COD_ORDER_STATUS_ID', '0', 'S&eacute;lectionner l\'&eacute;tat de la commande que vous voulez par d&eacute;fault.', '6', '0', 'osc_cfg_pull_down_order_statuses(', 'osc_get_order_status_name', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone de paiement', 'MODULE_PAYMENT_COD_ZONE', '0', 'Permettre seulement cette m&eacute;thode de paiement pour la zone choisie.', '6', '2', 'osc_get_zone_class_title', 'osc_cfg_pull_down_zone_classes(', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ordre de tri d\'affichage', 'MODULE_PAYMENT_COD_SORT_ORDER', '0', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier).', '6', '0', now())");
      osc_db_query("update configuration set configuration_value = '1' where configuration_key = 'WEBSITE_MODULE_INSTALLED'");

   }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_PAYMENT_COD_STATUS',
                    'MODULE_PAYMENT_COD_NAME_TITLE',
                   'MODULE_PAYMENT_COD_IMAGE',
                   'MODULE_PAYMENT_COD_SORT_ORDER',
                   'MODULE_PAYMENT_COD_ZONE',
                   'MODULE_PAYMENT_COD_ORDER_STATUS_ID'
                  );
    }
  }
