<?php
/*
 * xml_rpc_admin_customers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

    $Qproduct = $OSCOM_PDO->prepare('select p.*,
                                            pd.*
                                     from :table_products p,
                                          :table_products_description pd
                                     where p.products_id = :products_id
                                     and pd.products_id = p.products_id
                                     and pd.language_id = :language_id
                                    ');
    $Qproduct->bindInt(':products_id', (int)$products_id );
    $Qproduct->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
    $Qproduct->execute();

    $product = $Qproduct->fetch();

    $products_description  = osc_strip_html_tags($product['products_description']);
    $products_id_clicshopping  = $product['products_id'];

    $products_id_odoo = $OSCOM_ODOO->getSearchProductIdOdoo();

// products_image export
    if (file_exists(DIR_FS_CATALOG_IMAGES . $product['products_image']) ) {
      $products_image = DIR_FS_CATALOG_IMAGES . $product['products_image'];
      $data_small_image = file_get_contents($products_image);
      $products_image_odoo = base64_encode($data_small_image);
    }

    if (file_exists(DIR_FS_CATALOG_IMAGES . $product['products_image_zoom']) ) {
      $products_image_zoom = DIR_FS_CATALOG_IMAGES . $product['products_image_zoom'];
      $data_zoom_image = file_get_contents($products_image_zoom);
      $products_image_zoom_odoo = base64_encode($data_zoom_image);
    }

// **************************************************************************
// Search odoo clicshopping_catgories_id
// **************************************************************************

  $Qcategories_products  = $OSCOM_PDO->prepare('select products_id,
                                                        categories_id
                                                  from :table_products_to_categories
                                                  where products_id = :products_id
                                                 ');
  $Qcategories_products->bindInt(':products_id', (int)$products_id_clicshopping );

  $Qcategories_products->execute();

  $categories_products = $Qcategories_products->fetch();
  $categories_products_id = $categories_products['categories_id'];

  $ids = $OSCOM_ODOO->odooSearch('clicshopping_categories_id', '=', $categories_products_id , 'product.category', 'int');

// **********************************
// read id clicshopping_categories_id odoo
// **********************************

  $field_list = array('id');

  $Qcategories_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.category');
  $categories_id_odoo =   $Qcategories_id_odoo[0][id];

  if (empty($categories_id_odoo)) {
    $categories_id_odoo = 1;
  }

// **************************************************************************
// Search odoo products tax
// **************************************************************************

      $QProductsCodeTax = $OSCOM_PDO->prepare('select distinct tax_class_id,
                                                                code_tax_odoo
                                                 from :table_tax_rates
                                                 where tax_class_id = :tax_class_id
                                               ');

      $QProductsCodeTax->bindValue(':tax_class_id', $product['products_tax_class_id']);

      $QProductsCodeTax->execute();

      $Qproducts_code_tax = $QProductsCodeTax->fetch();
      $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];

//******************************************
// research id tax by description
//***************************************

      if ($products_code_tax != null) {
        $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
        $odoo_products_tax_id = $ids;
        $odoo_products_tax_id = $odoo_products_tax_id[0];

        if (!empty($odoo_products_tax_id)) {

          $type_tax_string = 'array';

          $tax = array(new xmlrpcval(
                                      array(
                                            new xmlrpcval(6, "int"),// 6 : id link
                                            new xmlrpcval(0, "int"),
                                            new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                      ), "array"
                                  )
                      );
        } else  {
          $tax = 0;
          $type_tax_string = 'int';
        }

      } else {
        $tax = 0;
        $type_tax_string = 'int';
      }


// **************************************************************************************************
// Manufacturer
// **************************************************************************************************


  if (!empty($product['manufacturers_id'])) {
// **************************
// Search odoo manufacturers id in clicshopping.manufacturer
// ***************************
    $ids = $OSCOM_ODOO->odooSearch('clicshopping_manufacturers_id', '=', $product['manufacturers_id'], 'clicshopping.manufacturer', 'int');
    $field_list = array('id');
    $Qmanufacturers_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.manufacturer');
    $manufacturer_id_odoo = $Qmanufacturers_id_odoo[0][id];
  }


  if  (empty($products_id_odoo)) {

      $product_packaging = osc_product_packaging();

// **********************************
// Create products if doesn't exist in odoo
// **********************************

      $values = array (  "default_code" => new xmlrpcval($product['products_model'], "string"),
                          "name" => new xmlrpcval($product['products_name'],"string"),
                          "categ_id" => new xmlrpcval($categories_id_odoo, "int"),
                          "description"  => new xmlrpcval($products_description,"string"),
                          "description_purchase"  => new xmlrpcval($products_description,"string"),
                          "description_sale" => new xmlrpcval($product['products_name'],"string"),
                          "list_price" => new xmlrpcval($product['products_price'],"double"),
                          "standard_price" => new xmlrpcval($product['products_cost'],"double"),
                          "image" => new xmlrpcval($products_image_zoom_odoo,"string"),
                          "image_medium" => new xmlrpcval($products_image_zoom_odoo,"string"),
                          "image_small" => new xmlrpcval($products_image_odoo,"string"),
                          "ean13" => new xmlrpcval($product['products_ean'],"string"),
                          "loc_case" => new xmlrpcval($product['products_wharehouse_level_location'],"string"),
                          "loc_rack" => new xmlrpcval($product['products_wharehouse'],"string"),
                          "loc_row" => new xmlrpcval($product['products_wharehouse_row'],"string"),
                          "weight" => new xmlrpcval($product['products_weight'],"double"),
                          "website_meta_title" => new xmlrpcval($product['products_head_title_tag'],"string"),
                          "website_meta_description" => new xmlrpcval($product['products_head_desc_tag'],"string"),
                          "website_meta_keywords" => new xmlrpcval($product['products_head_keywords_tag'],"string"),
                          "taxes_id" =>  new xmlrpcval($tax, $type_tax_string),
                          "type" =>  new xmlrpcval($product['products_type'], "string"),
                          "clicshopping_products_id" =>  new xmlrpcval($product['products_id'], "int"),
                          "clicshopping_products_sku" => new xmlrpcval($product['products_sku'],"string"),
                          "clicshopping_products_weight_pounds" => new xmlrpcval($product['products_weight_pounds'],"double"),
                          "clicshopping_products_dimension_width" => new xmlrpcval($product['products_dimension_width'],"double"),
                          "clicshopping_products_dimension_height" => new xmlrpcval($product['products_dimension_height'],"double"),
                          "clicshopping_products_dimension_depth" => new xmlrpcval($product['products_dimension_depth'],"double"),
                          "clicshopping_products_dimension_type" => new xmlrpcval($product['products_dimension_type'],"string"),
                          "clicshopping_products_wharehouse_time_replenishment" => new xmlrpcval($product['products_wharehouse_time_replenishment'],"string"),
                          "clicshopping_products_description" => new xmlrpcval($product['products_description'],"string"),
                          "clicshopping_products_price_comparison" => new xmlrpcval($product['products_price_comparison'],"double"),
                          "clicshopping_products_only_online" => new xmlrpcval($product['products_only_online'],"double"),
                          "clicshopping_products_only_shop" => new xmlrpcval($product['products_only_shop'],"double"),
                          "clicshopping_products_stock_status" => new xmlrpcval($product['products_status'],"double"),
                          "clicshopping_products_quantity_alert" => new xmlrpcval($product['products_quantity_alert'],"double"),
                          "clicshopping_products_min_qty_order" => new xmlrpcval($product['products_min_qty_order'],"double"),
                          "clicshopping_products_price_kilo" => new xmlrpcval($product['products_price_kilo'],"double"),
                          "clicshopping_products_view" => new xmlrpcval($product['products_view'],"double"),
                          "clicshopping_products_handling" => new xmlrpcval($product['products_handling'],"double"),
                          "clicshopping_products_orders_view" => new xmlrpcval($product['orders_view'],"double"),
                          "clicshopping_admin_user_name" => new xmlrpcval($product['admin_user_name'],"string"),
                          "clicshopping_products_date_available" => new xmlrpcval($product['products_date_available'],"string"),
                          "clicshopping_products_packaging" => new xmlrpcval($product_packaging,"string"),
                          "clicshopping_products_percentage" => new xmlrpcval($product['products_percentage'],"double"),
                          "clicshopping_product_manufacturer_id" => new xmlrpcval($manufacturer_id_odoo,"int"),
                       );

      $OSCOM_ODOO->createOdoo($values, "product.template");

    }  else {

// **********************************
// update products if exist
// **********************************
      $product_packaging = osc_product_packaging();

      $id_list = array();
      $id_list[]= new xmlrpcval($products_id_odoo, 'int');

      $values = array ( "default_code" => new xmlrpcval($product['products_model'], "string"),
                        "name" => new xmlrpcval($product['products_name'],"string"),
                        "categ_id" => new xmlrpcval($categories_id_odoo, "int"),
                        "description"  => new xmlrpcval($products_description,"string"),
                        "description_purchase"  => new xmlrpcval($products_description,"string"),
                        "description_sale" => new xmlrpcval($product['products_name'],"string"),
                        "list_price" => new xmlrpcval($product['products_price'],"double"),
                        "standard_price" => new xmlrpcval($product['products_cost'],"double"),
                        "image" => new xmlrpcval($products_image_odoo,"string"),
                        "image_medium" => new xmlrpcval($products_image_zoom_odoo,"string"),
                        "image_small" => new xmlrpcval($products_image_odoo,"string"),
                        "ean13" => new xmlrpcval($product['products_ean'],"string"),
                        "loc_case" => new xmlrpcval($product['products_wharehouse_level_location'],"string"),
                        "loc_rack" => new xmlrpcval($product['products_wharehouse'],"string"),
                        "loc_row" => new xmlrpcval($product['products_wharehouse_row'],"string"),
                        "weight" => new xmlrpcval($product['products_weight'],"double"),
                        "website_meta_title" => new xmlrpcval($product['products_head_title_tag'],"string"),
                        "website_meta_description" => new xmlrpcval($product['products_head_desc_tag'],"string"),
                        "website_meta_keywords" => new xmlrpcval($product['products_head_keywords_tag'],"string"),
                        "taxes_id" =>  new xmlrpcval($tax, $type_tax_string),
                        "type" =>  new xmlrpcval($product['products_type'], "string"),                       
                        "clicshopping_products_id" =>  new xmlrpcval($product['products_id'], "int"),
                        "clicshopping_products_sku" => new xmlrpcval($product['products_sku'],"string"),
                        "clicshopping_products_weight_pounds" => new xmlrpcval($product['products_weight_pounds'],"double"),
                        "clicshopping_products_dimension_width" => new xmlrpcval($product['products_dimension_width'],"double"),
                        "clicshopping_products_dimension_height" => new xmlrpcval($product['products_dimension_height'],"double"),
                        "clicshopping_products_dimension_depth" => new xmlrpcval($product['products_dimension_depth'],"double"),
                        "clicshopping_products_dimension_type" => new xmlrpcval($product['products_dimension_type'],"string"),
                        "clicshopping_products_wharehouse_time_replenishment" => new xmlrpcval($product['products_wharehouse_time_replenishment'],"string"),
                        "clicshopping_products_description" => new xmlrpcval($product['products_description'],"string"),
                        "clicshopping_products_price_comparison" => new xmlrpcval($product['products_price_comparison'],"double"),
                        "clicshopping_products_only_online" => new xmlrpcval($product['products_only_online'],"double"),
                        "clicshopping_products_only_shop" => new xmlrpcval($product['products_only_shop'],"double"),
                        "clicshopping_products_stock_status" => new xmlrpcval($product['products_status'],"double"),
                        "clicshopping_products_quantity_alert" => new xmlrpcval($product['products_quantity_alert'],"double"),
                        "clicshopping_products_min_qty_order" => new xmlrpcval($product['products_min_qty_order'],"double"),
                        "clicshopping_products_price_kilo" => new xmlrpcval($product['products_price_kilo'],"double"),
                        "clicshopping_products_view" => new xmlrpcval($product['products_view'],"double"),
                        "clicshopping_products_handling" => new xmlrpcval($product['products_handling'],"double"),
                        "clicshopping_products_orders_view" => new xmlrpcval($product['orders_view'],"double"),
                        "clicshopping_admin_user_name" => new xmlrpcval($product['admin_user_name'],"string"),
                        "clicshopping_products_date_available" => new xmlrpcval($product['products_date_available'],"string"),
                        "clicshopping_products_packaging" => new xmlrpcval($product_packaging,"string"),
                        "clicshopping_products_percentage" => new xmlrpcval($product['products_percentage'],"double"),
                        "clicshopping_product_manufacturer_id" => new xmlrpcval($manufacturer_id_odoo,"int"),
                      );

      $OSCOM_ODOO->updateOdoo($products_id_odoo, $values, 'product.template');
    }


// **************************************************************************************************
// Supplier
// **************************************************************************************************


    if (!empty($product['suppliers_id'])) {

// **************************
// Search odoo suppliers id in res.partner
// ***************************

     $ids = $OSCOM_ODOO->odooSearch('clicshopping_suppliers_id', '=', $product['suppliers_id'], 'res.partner');

// **********************************
// read id suppliers_id odoo
// **********************************

      $field_list = array('id',
                          'name',
                        );

      $Qsuppliers_id_name_res_partner_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
      $suppliers_id_name_res_partner_odoo = $Qsuppliers_id_name_res_partner_odoo[0][id];
      $products_id_odoo = $OSCOM_ODOO->getSearchProductIdOdoo();

      if  (!empty($suppliers_id_name_res_partner_odoo)) {

// **********************************
// Search odoo suppliers Info id
// **********************************

       $ids =  $OSCOM_ODOO->odooSearchByTwoCriteria('product_code', '=', $product['products_model'], 'product.supplierinfo', 'string',
                                                    'product_tmpl_id', '=', $products_id_odoo, 'int');

// **********************************
// read id suppliers Info  Id odoo
// **********************************

        $field_list = array('id',
                            'name',
                           );

        $Qsuppliers_id_name_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.supplierinfo');
        $suppliers_id_name_odoo = $Qsuppliers_id_name_odoo[0][id];

        if  (empty($suppliers_id_name_odoo) ) {

          $values = array (  "name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new xmlrpcval($product['products_name'], "string"),
                            "product_code" => new xmlrpcval($product['products_model'], "string"),
                            "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                          );

          $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");

        } else {

          $id_list = array();
          $id_list[] = new xmlrpcval($suppliers_id_name_odoo, 'int');

          $values = array ( "name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                            "product_name" => new xmlrpcval($product['products_name'], "string"),
                            "product_code" => new xmlrpcval($product['products_model'],"string"),
                            "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                          );

          $OSCOM_ODOO->updateOdoo($suppliers_id_name_odoo, $values, 'product.supplierinfo');
        }

      } else {

        $values = array("name" => new xmlrpcval($suppliers_id_name_res_partner_odoo, "int"),
                        "product_name" => new xmlrpcval($product['products_name'], "string"),
                        "product_code" => new xmlrpcval($product['products_model'], "string"),
                        "product_tmpl_id" => new xmlrpcval($products_id_odoo, "int"),
                      );

        $OSCOM_ODOO->createOdoo($values, "product.supplierinfo");
      }
    }

// **************************************************************************************************
// Stock
// **************************************************************************************************

    $date = date("Y-m-d H:i:s");

    $company_id = $OSCOM_ODOO->getSearchCompanyIdOdoo();
    $products_name_odoo = '[' . $product['products_model'] . '] ' . $product['products_name'];


// stock warehouse search id and code concerning ClicShopping Wharehouse
    $ids = $OSCOM_ODOO->odooSearch('name', '=', 'ClicShopping', 'stock.warehouse');

    $field_list = array('id',
                        'code',
                        'name',
                      );

    $Qstock_wharehouse = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse');
    $stock_wharehouse_id = $Qstock_wharehouse[0][id];
    $stock_wharehouse_code = $Qstock_wharehouse[0][code];


// search location name and id in stock location
    $ids = $OSCOM_ODOO->odooSearch('name', '=', $stock_wharehouse_code, 'stock.location');

    $field_list = array('id',
                        'location_id',
                        'name',
                      );

    $Qstock_location = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.location');
    $stock_location_id = $Qstock_location[0][id];
    $stock_location_name = $Qstock_location[0][name];

  if (!empty($product['products_quantity'])) {

// **********************************
// Search odoo products id & stock available
// **********************************

      $products_id_odoo = $OSCOM_ODOO->getSearchProductIdOdoo();

// **********************************
// read id products odoo
// **********************************

    $field_list = array('qty_available');

    $Qproducts_stock_available = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
    $products_stock_available = $Qproducts_stock_available[0][qty_available];

    if (!empty($stock_location_id)) {

      if ($product['products_quantity'] != $products_stock_available) {

// search qty in stock.quant
        $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_name_odoo, 'stock.quant', 'string',
                                                       'location_id', 'like', $stock_location_name . '%', 'string');

        $field_list = array('qty');

        $QSearchStock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.quant');

        $stock_odoo_qty_total = '0';

// Stock calcul inside wharehouse
        foreach ($QSearchStock as $key) {
          $products_stock_qty = $key[qty];
          $stock_odoo_qty_total = $stock_odoo_qty_total + $products_stock_qty;
        }

// difference between ClicShopping and Odoo
        $new_stock = $product['products_quantity'] - $stock_odoo_qty_total;

// hack, don't find the good value in location.stock. Add +1 on id because it's create just after
        $stock_location_id = $stock_location_id + 1;

// move stock

        if ($new_stock > 0) {
          $values = array(
                          "product_id" => new xmlrpcval($products_id_odoo, "int"),
                          "product_uom_qty" => new xmlrpcval($new_stock, "double"), // qty of my product
                          "name" => new xmlrpcval($products_name_odoo, "string"),
                          "invoice_state" => new xmlrpcval('none', "string"),
                          "date" => new xmlrpcval($date, "string"),
                          "date_expected" => new xmlrpcval($date, "string"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "procure_method" => new xmlrpcval('make_to_stock', "string"),
                          "location_dest_id" => new xmlrpcval($stock_location_id, "string"),
                          "location_id" => new xmlrpcval(5, "int"), // Virtual Locations/Inventory loss
                          "product_uom" => new xmlrpcval(1, "int"),
                          "origin" => new xmlrpcval("ClicShopping Webstore", "string"),
                        );
        } else {

// Adapted for odoo on the calcul
          $new_stock = $new_stock * (-1);

          $values = array(
                          "product_id" => new xmlrpcval($products_id_odoo, "int"),
                          "product_uom_qty" => new xmlrpcval($new_stock, "double"), // qty of my product
                          "name" => new xmlrpcval($products_name_odoo, "string"),
                          "invoice_state" => new xmlrpcval('none', "string"),
                          "date" => new xmlrpcval($date, "string"),
                          "date_expected" => new xmlrpcval($date, "string"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "procure_method" => new xmlrpcval('make_to_stock', "string"),
                          "location_dest_id" => new xmlrpcval(5, "string"),
                          "location_id" => new xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                          "product_uom" => new xmlrpcval(1, "int"),
                          "origin" => new xmlrpcval("ClicShopping Webstore", "string"),
                        );
        }

        $OSCOM_ODOO->createOdoo($values, "stock.move");


// search id for stock.move concerning the product
        $ids = $OSCOM_ODOO->odooSearchAllByTwoCriteria('product_id', '=', $products_id_odoo, 'stock.move', 'int',
                                                       'date', '=', $date, 'string');

        $field_list = array('id');

        $Qodoo_move_read = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.move');
        $odoo_picking_id = $Qodoo_move_read[0][id];

        $OSCOM_ODOO->buttonClickOdoo('stock.move', 'action_done', $odoo_picking_id);
      }
    } // end !empty($stock_location_id)
  } //!empty($product['products_quantity'])

//*******************************************************************************************
//             Stock minimum
//*******************************************************************************************
    $ids = $OSCOM_ODOO->odooSearch('product_id', '=', $products_id_odoo, 'stock.warehouse.orderpoint', 'int' );

// **********************************
// read id stock minimum in odoo
// **********************************

    $field_list = array ('id');

    $Qodoo_min_stock = $OSCOM_ODOO->readOdoo($ids, $field_list, 'stock.warehouse.orderpoint');
    $odoo_min_stock_id = $Qodoo_min_stock[0][id];

    if ($product['products_quantity_alert'] == 0) {
      $stock_alert = STOCK_REORDER_LEVEL;
    } else {
      $stock_alert = $product['products_quantity_alert'];
    }

    if (!empty($odoo_min_stock_id)) {

      $id_list = array();
      $id_list[]= new xmlrpcval($odoo_min_stock_id, 'int');

      $values = array("product_min_qty" => new xmlrpcval($stock_alert, "double") );

      $OSCOM_ODOO->updateOdoo($odoo_min_stock_id, $values, 'stock.warehouse.orderpoint');

    } else {
      $values = array(
                      "name" => new xmlrpcval($product['products_model'] . ' / ' . $product['products_id'], "string"), // qty of my product
                      "product_id" => new xmlrpcval($products_id_odoo, "int"), //
                      "warehouse_id" => new xmlrpcval($stock_wharehouse_id, "int"),
                      "location_id" => new xmlrpcval($stock_location_id, "int"), // Virtual Locations/Inventory loss
                      "product_min_qty" => new xmlrpcval($stock_alert, "double"),
                      "product_max_qty" => new xmlrpcval(0, "double"),
                      "qty_multiple" => new xmlrpcval(1, "double"),
                      "company_id" => new xmlrpcval($company_id, "int"),
                      "active" => new xmlrpcval(1, "double"),
                    );

      $OSCOM_ODOO->createOdoo($values, "stock.warehouse.orderpoint");
    }

?>