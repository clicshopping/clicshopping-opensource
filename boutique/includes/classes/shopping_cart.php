<?php
/**
 * shopping_cart.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  class shoppingCart {
    var $contents, $total, $weight, $content_type;

    function shoppingCart() {
      $this->reset();
    }

/*
 *  Restore the good qty in function B2BC / B2C or not and qty minimal or maximal defined
 *  string $qty
*/

    function restore_qty() {
      global $qty, $products_id;

// Maximum to take an order
      if (defined('MAX_QTY_IN_CART') && (MAX_QTY_IN_CART > 0) && ((int)$qty > MAX_QTY_IN_CART)) {
        $qty = MAX_QTY_IN_CART;
      }

      if ((osc_get_products_min_order_qty_shopping_cart($products_id) > 1) & ($qty < osc_get_products_min_order_qty_shopping_cart($products_id))) {
        $qty =  osc_get_products_min_order_qty_shopping_cart($products_id);
      }

     if (osc_get_products_min_order_qty_shopping_cart($products_id) == 0) {
       if (defined('MAX_MIN_IN_CART') && (MAX_MIN_IN_CART > 0) && ((int)$qty < MAX_MIN_IN_CART)) {
        $qty = MAX_MIN_IN_CART;
       }
     }

     return $qty;
    }

    function restore_contents() {
      global $OSCOM_Customer, $OSCOM_PDO;

      if (!$OSCOM_Customer->isLoggedOn()) return false;

// insert current cart contents in database
      if (is_array($this->contents)) {

        foreach ( array_keys($this->contents) as $products_id ) {

// B2B / B2C Choose the good qty
          $qty = $this->contents[$products_id]['qty'];
          $qty1 = $this->restore_qty();

          if ($qty < $qty1) $qty = $this->restore_qty();
          if ($qty > $qty1) $qty = $this->contents[$products_id]['qty'];

          $Qcheck = $OSCOM_PDO->prepare('select products_id
                                         from :table_customers_basket 
                                         where customers_id = :customers_id
                                         and products_id = :products_id'
                                        );
          $Qcheck->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
          $Qcheck->bindValue(':products_id', (int)$products_id);
          $Qcheck->execute();

          if ($Qcheck->fetch() === false) {
          
            $OSCOM_PDO->save('customers_basket', ['customers_id' => (int)$OSCOM_Customer->getID(), 
                                                  'products_id' => (int)$products_id, 
                                                  'customers_basket_quantity' => (int)$qty, 
                                                  'customers_basket_date_added' => date('Ymd')
                                                ]
                             );

            if (isset($this->contents[$products_id]['attributes'])) {

              foreach ($this->contents[$products_id]['attributes'] as $option => $value) {

                $OSCOM_PDO->save('customers_basket_attributes', ['customers_id' =>  (int)$OSCOM_Customer->getID(), 
                                                                 'products_id' => (int)$products_id, 
                                                                 'products_options_id' => (int)$option, 
                                                                 'products_options_value_id' => (int)$value
                                                                ]
                                );
              }
            }
          } else {

            $OSCOM_PDO->save('customers_basket', ['customers_basket_quantity' => (int)$qty], 
                                                 ['customers_id' => (int)$OSCOM_Customer->getID(), 
                                                  'products_id' => (int)$products_id
                                                ]
                             );
          }
        }
      }

// reset per-session cart contents, but not the database contents
      $this->reset(false);

      $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                              customers_basket_quantity 
                                      from :table_customers_basket 
                                      where customers_id = :customers_id
                                      ');
      $Qproducts->bindInt(':customers_id',  (int)$OSCOM_Customer->getID());

      $Qproducts->execute();

       while ($Qproducts->fetch()) {

        $this->contents[$Qproducts->value('products_id')] = array('qty' => $Qproducts->valueInt('customers_basket_quantity'));

// attributes
        $Qattributes = $OSCOM_PDO->prepare('select products_options_id, 
                                                  products_options_value_id 
                                                  from :table_customers_basket_attributes 
                                            where customers_id = :customers_id 
                                            and products_id = :products_id
                                            ');
        $Qattributes->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
        $Qattributes->bindValue(':products_id', $Qproducts->value('products_id'));
        $Qattributes->execute();

        while ($Qattributes->fetch()) {
          $this->contents[$Qproducts->value('products_id')]['attributes'][$Qattributes->valueInt('products_options_id')] = $Qattributes->valueInt('products_options_value_id');
        }
      }

      $this->cleanup();

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
      $this->cartID = $this->generate_cart_id();
    }

    function reset($reset_database = false) {
      global $OSCOM_Customer, $OSCOM_PDO;

      $this->contents = array();
      $this->total = 0;
      $this->weight = 0;
      $this->content_type = false;

      if ($OSCOM_Customer->isLoggedOn() && ($reset_database == true)) {
        $OSCOM_PDO->delete('customers_basket', ['customers_id' => $OSCOM_Customer->getID() ]);

        $OSCOM_PDO->delete('customers_basket_attributes', ['customers_id' =>$OSCOM_Customer->getID() ]);
      }

      unset($this->cartID);
      if (isset($_SESSION['cartID'])) unset($_SESSION['cartID']);
    }

    function add_cart($products_id, $qty = '1', $attributes = '', $notify = true) {
      global $OSCOM_Customer, $OSCOM_PDO, $OSCOM_ProductsListing;

      $products_id_string = osc_get_uprid($products_id, $attributes);
      $products_id = osc_get_prid($products_id_string);

// B2B / B2C Choose the good qty
//      $qty = $this->restore_qty();
      if (defined('MAX_QTY_IN_CART') && (MAX_QTY_IN_CART > 0) && ((int)$qty > MAX_QTY_IN_CART)) {
        $qty = MAX_QTY_IN_CART;
      }

      if ((osc_get_products_min_order_qty_shopping_cart($products_id) > 1) & ($qty < osc_get_products_min_order_qty_shopping_cart($products_id))) {
        $qty =  osc_get_products_min_order_qty_shopping_cart($products_id);
      }

     if (osc_get_products_min_order_qty_shopping_cart($products_id) == 0) {
       if (defined('MAX_MIN_IN_CART') && (MAX_MIN_IN_CART > 0) && ((int)$qty < MAX_MIN_IN_CART)) {
        $qty = MAX_MIN_IN_CART;
       }
     }

      $attributes_pass_check = true;

      if (is_array($attributes) && !empty($attributes)) {
        foreach ($attributes as $option => $value) {
          if (!is_numeric($option) || !is_numeric($value)) {
            $attributes_pass_check = false;
            break;
          } else {
          
            $Qcheck = $OSCOM_PDO->prepare('select products_attributes_id 
                                          from :table_products_attributes 
                                          where products_id = :products_id 
                                          and options_id = :options_id 
                                          and options_values_id = :options_values_id 
                                          limit 1
                                         ');

           $Qcheck->bindInt(':options_values_id', (int)$value);
           $Qcheck->bindInt(':products_id', (int)$products_id );
           $Qcheck->bindInt(':options_id', (int)$option);
           $Qcheck->execute();

            if ($Qcheck->fetch() === false) {
              $attributes_pass_check = false;
              break;
            }
          }
        }
      } elseif (osc_has_product_attributes($products_id)) {
        $attributes_pass_check = false;
      }

      if (is_numeric($products_id) && is_numeric($qty) && ($attributes_pass_check == true)) {

        $Qcheck = $OSCOM_PDO->prepare('select products_id 
                                      from :table_products 
                                      where products_id = :products_id 
                                      and products_status = 1
                                      ');
        $Qcheck->bindInt(':products_id', (int)$products_id);
        $Qcheck->execute();

        if (($Qcheck->fetch() !== false) ) {
          if ($notify == true) {
            $_SESSION['new_products_id_in_cart'] = $products_id;
          }

          if ($this->in_cart($products_id_string)) {
            $this->update_quantity($products_id_string, $qty, $attributes);
          } else {
            $this->contents[$products_id_string] = array('qty' => (int)$qty);

// insert into database
            if ($OSCOM_Customer->isLoggedOn()) {
            
              $OSCOM_PDO->save('customers_basket', ['customers_id' => (int)$OSCOM_Customer->getID(),
                                                   'products_id' => (int)$products_id_string, 
                                                   'customers_basket_quantity' => (int)$qty, 
                                                   'customers_basket_date_added' => date('Ymd')
                                                  ]
                              );
            
            }
            
            if (is_array($attributes)) {
              foreach ($attributes as $option => $value) {
                $this->contents[$products_id_string]['attributes'][$option] = $value;
// insert into database
                if ($OSCOM_Customer->isLoggedOn()) {
                
                  $OSCOM_PDO->save('customers_basket_attributes', ['customers_id' => (int)$OSCOM_Customer->getID(),
                                                                  'products_id' => (int)$products_id_string, 
                                                                  'products_options_id' => (int)$option, 
                                                                  'products_options_value_id' => (int)$value
                                                                 ]
                  );
                }
              }
            }
          }

          $this->cleanup();

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
          $this->cartID = $this->generate_cart_id();
        }
      }
    }

    function update_quantity($products_id, $quantity = '', $attributes = '') {
      global $OSCOM_Customer, $OSCOM_PDO;

      $products_id_string = osc_get_uprid($products_id, $attributes);
      $products_id = osc_get_prid($products_id_string);


// Maximum to take an order
      if (defined('MAX_QTY_IN_CART') && (MAX_QTY_IN_CART > 0) && ((int)$quantity > MAX_QTY_IN_CART)) {
        $quantity = MAX_QTY_IN_CART;
      }

// Define the minimum in basket if the qty min order is not define in product
     if (osc_get_products_min_order_qty_shopping_cart($products_id) == 0) {
       if (defined('MAX_MIN_IN_CART') && (MAX_MIN_IN_CART > 0) && ((int)$quantity < MAX_MIN_IN_CART)) {
        $quantity = MAX_MIN_IN_CART;
       }
     }

      $attributes_pass_check = true;

      if (is_array($attributes)) {
        foreach ($attributes as $option => $value) {
          if (!is_numeric($option) || !is_numeric($value)) {
            $attributes_pass_check = false;
            break;
            
          } else {
          
             $Qcheck = $OSCOM_PDO->prepare('select products_attributes_id
                                           from products_attributes
                                           where products_id = :products_id
                                           and options_id = :option
                                           and options_values_id = :value
                                           limit 1
                                        ');
            $Qcheck->bindInt(':products_id', (int)$products_id);
            $Qcheck->bindInt(':option', (int)$option);
            $Qcheck->bindInt(':value', (int)$value);
            $Qcheck->execute();

            if ($Qcheck->fetch() !== false) {
              $attributes_pass_check = false;
              break;
            }
          }
        }
      }

      if (is_numeric($products_id) && isset($this->contents[$products_id_string]) && is_numeric($quantity) && ($attributes_pass_check == true)) {
        $this->contents[$products_id_string] = array('qty' => (int)$quantity);

// update database
        if ($OSCOM_Customer->isLoggedOn()) {

          $OSCOM_PDO->save('customers_basket', ['customers_basket_quantity' => (int)$quantity],
                                               ['customers_id' => (int)$OSCOM_Customer->getID(), 
                                                'products_id' => (int)$products_id_string
                                              ]
                         );

        }

        if (is_array($attributes)) {
          foreach ($attributes as $option => $value) {
            $this->contents[$products_id_string]['attributes'][$option] = $value;

// update database
            if ($OSCOM_Customer->isLoggedOn()) {
            
              $OSCOM_PDO->save('customers_basket_attributes', ['products_options_value_id' => (int)$value], 
                                                              ['customers_id' => (int)$OSCOM_Customer->getID(), 
                                                              'products_id' => $products_id_string, 
                                                              'products_options_id' => (int)$option
                                                             ]
                              );
            
            }
          }
        }

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
        $this->cartID = $this->generate_cart_id();
      }
    }

    function cleanup() {
      global $OSCOM_Customer, $OSCOM_PDO;

      foreach ( array_keys($this->contents) as $key ) {
        if ($this->contents[$key]['qty'] < 1) {
          unset($this->contents[$key]);
// remove from database
          if ($OSCOM_Customer->isLoggedOn()) {

            $OSCOM_PDO->delete('customers_basket', ['customers_id' => $OSCOM_Customer->getID(),
                                                   'products_id' => (int)$key
                                                   ]
                               );
            $OSCOM_PDO->delete('customers_basket_attributes', ['customers_id' => $OSCOM_Customer->getID(),
                                                              'products_id' => (int)$key
                                                             ]
                             );

          }
        }
      }
    }

    function count_contents() {  // get total number of items in cart
      $total_items = 0;
      if (is_array($this->contents)) {
        foreach ( array_keys($this->contents) as $products_id ) {
          $total_items += $this->get_quantity($products_id);
        }
      }

      return $total_items;
    }

    function get_quantity($products_id) {
      if (isset($this->contents[$products_id])) {
        return $this->contents[$products_id]['qty'];
      } else {
        return 0;
      }
    }

    function in_cart($products_id) {
      if (isset($this->contents[$products_id])) {
        return true;
      } else {
        return false;
      }
    }

    function remove($products_id) {
      global $OSCOM_Customer, $OSCOM_PDO;

      unset($this->contents[$products_id]);
// remove from database
      if ($OSCOM_Customer->isLoggedOn()) {

        $OSCOM_PDO->delete('customers_basket', ['customers_id' => $OSCOM_Customer->getID(),
                                                'products_id' => (int)$products_id
                                               ]
                          );

        $OSCOM_PDO->delete('customers_basket_attributes', ['customers_id' => $OSCOM_Customer->getID(),
                                                           'products_id' => (int)$products_id
                                                          ]
                          );
      }

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
      $this->cartID = $this->generate_cart_id();
    }

    function remove_all() {
      $this->reset();
    }

    function get_product_id_list() {
      $product_id_list = '';
      if (is_array($this->contents)) {
       foreach ( array_keys($this->contents) as $products_id ) {
          $product_id_list .= ', ' . $products_id;
        }
      }

      return substr($product_id_list, 2);
    }

    function calculate() {
      global $OSCOM_Customer, $OSCOM_PDO, $OSCOM_ProductsListing, $OSCOM_DetectMobile;

      $this->total = 0;
      $this->weight = 0;
      if (!is_array($this->contents)) return 0;

      foreach ( array_keys($this->contents) as $products_id ) {
        $qty = $this->contents[$products_id]['qty'];

// Requete SQL pour avoir le prix du produit
         if ($OSCOM_Customer->getCustomersGroupID() != 0) {

           $Qproduct = $OSCOM_PDO->prepare('select p.products_id,
                                                  p.products_price,
                                                  p.products_tax_class_id,
                                                  p.products_weight,
                                                  g.price_group_view,
                                                  g.customers_group_price
                                           from :table_products p left join :table_products_groups g on p.products_id = g.products_id
                                           where p.products_id = :products_id
                                           and g.customers_group_id = :customers_group_id
                                           and g.products_group_view = :products_group_view
                                          ');
           $Qproduct->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
           $Qproduct->bindInt(':products_id', (int)$products_id );
           $Qproduct->bindInt(':products_group_view', 1 );
           $Qproduct->execute();

         } else {

           $Qproduct = $OSCOM_PDO->prepare('select products_id,
                                                  products_price,
                                                  products_tax_class_id,
                                                  products_weight
                                           from :table_products
                                           where products_id = :products_id
                                          ');
           $Qproduct->bindInt(':products_id', (int)$products_id );
           $Qproduct->execute();

         }


        if ($Qproduct->fetch() !== false) {

          $prid = $Qproduct->valueInt('products_id');
          $products_tax = osc_get_tax_rate($Qproduct->valueInt('products_tax_class_id'));
          $products_price = $Qproduct->valueDecimal('products_price');
          $products_weight = $Qproduct->valueDecimal('products_weight');

// Prix B2B ou client normal
           if (($OSCOM_Customer->getCustomersGroupID() != 0) && ($Qproduct->valueInt('price_group_view') == '1')) {
             $products_price = $Qproduct->valueDecimal('customers_group_price');
           } else {

// Marketing : increase the price on the mobile phone and tablet
// see shopping_cart cart class - currencies.php - order.php
            if (PRICE_MOBILE_ACCEPT == 'true') {
              if ($OSCOM_Customer->getCustomersGroupID()  == 0) {
                if ($OSCOM_DetectMobile->isMobile() || $OSCOM_DetectMobile->isTablet()) {
                  if ( (PRICE_MOBILE_POURCENTAGE != 0) || (PRICE_MOBILE_POURCENTAGE != '') ) {
                    $products_price = $products_price * (1+(PRICE_MOBILE_POURCENTAGE/100));
                  }
                }
              }
            } // end PRICE_MOBILE_ACCEPT
          }

// Prix promotionel B2B ou normal
          if (($OSCOM_Customer->getCustomersGroupID() != 0) && ($Qproduct->valueInt('price_group_view') == '1')) {

            $Qspecial = $OSCOM_PDO->prepare('select specials_new_products_price
                                              from :table_specials
                                              where products_id = :products_id
                                              and status = :status
                                              and customers_group_id = :customers_group_id
                                            ');
            $Qspecial->bindInt(':products_id', (int)$prid );
            $Qspecial->bindInt(':status', 1 );
            $Qspecial->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());

          } else {

            $Qspecial = $OSCOM_PDO->prepare('select specials_new_products_price
                                              from :table_specials
                                              where products_id = :products_id
                                              and status = :status
                                            ');
            $Qspecial->bindInt(':products_id', (int)$prid );
            $Qspecial->bindInt(':status', 1 );
          }
          
          $Qspecial->execute();

          if ($Qspecial->fetch() !== false) {

            $products_price = $Qspecial->valueDecimal('specials_new_products_price');

// Marketing : increase the price on the mobile phone and tablet
// see shopping_cart cart class - currencies.php - order.php
            if (PRICE_MOBILE_ACCEPT == 'true') {
              if ($OSCOM_Customer->getCustomersGroupID()  == 0) {
                if ($OSCOM_DetectMobile->isMobile() || $OSCOM_DetectMobile->isTablet()) {
                  if ( (PRICE_MOBILE_POURCENTAGE != 0) || (PRICE_MOBILE_POURCENTAGE != '') ) {
                    $products_price = $products_price * (1+(PRICE_MOBILE_POURCENTAGE/100));
                  }
                }
              }
            } // end PRICE_MOBILE_ACCEPT
          }
// Total calculation
          if ($qty < (int)$min_quantity) $qty =  (int)$min_quantity;

// Probleme avec calculate price qui ne converti pas correctement les informations voir la classe shopping_cart
//          $this->total += $currencies->calculate_price($products_price, $products_tax, $qty);
          $this->total += osc_add_tax($products_price, $products_tax) * $qty;
          $this->weight += ($qty * $products_weight);
        }

// Prix des attribues
        if (isset($this->contents[$products_id]['attributes'])) {
          foreach ($this->contents[$products_id]['attributes'] as $option => $value) {

              $Qattributes = $OSCOM_PDO->prepare('select options_values_price, 
                                                        price_prefix 
                                                from :table_products_attributes 
                                                where products_id = :products_id 
                                                and options_id = :options_id 
                                                and options_values_id = :options_values_id'
                                                );
              $Qattributes->bindInt(':products_id', (int)$prid);
              $Qattributes->bindInt(':options_id', (int)$option);
              $Qattributes->bindInt(':options_values_id', (int)$value);
              $Qattributes->execute();

              if ($Qattributes->fetch() !== false) {

// La prix et l'attribut ((additionne) sont en rapport avec la quantite - total dela commande
                if ($Qattributes->value('price_prefix') == '+') {
                  $this->total += $qty * osc_add_tax($Qattributes->valueDecimal('options_values_price'), $products_tax);
                }

// La prix et l'attribut (soustraction) sont en rapport avec la quantite - total dela commande
                if ($Qattributes->value('price_prefix') == '-') {
                  $this->total -= $qty * osc_add_tax($Qattributes->valueDecimal('options_values_price'), $products_tax);
                }

// Prix forfaitaire : l'attribut est independant de la quantite commande - total dela commande
// pb au niveau du calcul du total au niveau des arrondis -arret du developpement de la fonctionnalite
              if ($Qattributes->value('price_prefix') == '=') {
                $this->total +=  osc_add_tax($Qattributes->valueDecimal('options_values_price'), $products_tax);
              }
            }
          }
        }
      }
    }

    function attributes_price($products_id) {
      global $OSCOM_PDO;

      $attributes_price = 0;

      if (isset($this->contents[$products_id]['attributes'])) {
        foreach ($this->contents[$products_id]['attributes'] as $option => $value) {

          $Qattributes = $OSCOM_PDO->prepare('select options_values_price, 
                                                    price_prefix 
                                                    from :table_products_attributes 
                                            where products_id = :products_id 
                                            and options_id = :options_id 
                                            and options_values_id = :options_values_id
                                           ');
          $Qattributes->bindInt(':products_id', (int)$products_id);
          $Qattributes->bindInt(':options_id', (int)$option);
          $Qattributes->bindInt(':options_values_id', (int)$value);
          $Qattributes->execute();

          if ($Qattributes->fetch() !== false) {

// La prix et l'attribut ((additionne) sont en rapport avec la quantite
           if ($Qattributes->value('price_prefix') == '+') {
             $attributes_price += $Qattributes->valueDecimal('options_values_price');
           }
// La prix et l'attribut (soustraction) sont en rapport avec la quantite
           if ($Qattributes->value('price_prefix') == '-') {
             $attributes_price -= $Qattributes->valueDecimal('options_values_price');
           }
// Prix forfaitaire : l'attribut est independant de la quantite commandee
           if ($Qattributes->value('price_prefix') == '=') {
             $attributes_price += round( ($Qattributes->valueDecimal('options_values_price') / $this->contents[$products_id]['qty']) ,2);
           }
/* test suite bug identifie avec = suite au virgule flottante
// Prix forfaitaire : l'attribut est independant de la quantite commandee
           if $Qattributes->value('price_prefix') == '=' && !forfait ) {
             $attributes_price += $Qattributes->valueDecimal('options_values_price') / $this->contents[$products_id]['qty'] ;
             $forfait = true;
           }
*/
          }
        }
      }

      return $attributes_price;
    }

    function get_products() {
      global $OSCOM_Customer, $OSCOM_PDO;

      if (!is_array($this->contents)) return false;

      $products_array = array();
      foreach ( array_keys($this->contents) as $products_id ) {

// Requete SQL pour avoir le prix du produit
       if ($OSCOM_Customer->getCustomersGroupID() != 0) {

         $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                                 pd.products_name,
                                                 p.products_model,
                                                 g.products_model_group,
                                                 p.products_image,
                                                 p.products_price,
                                                 p.products_weight,
                                                 p.products_tax_class_id,
                                                 g.price_group_view,
                                                 g.customers_group_price
                                          from :table_products p left join :table_products_groups g on p.products_id = g.products_id,
                                               :table_products_description pd
                                          where p.products_id = :products_id
                                          and pd.products_id = p.products_id
                                          and g.customers_group_id = :customers_group_id
                                          and g.products_group_view = :products_group_view
                                          and pd.language_id = :language_id
                                    ');

         $Qproducts->bindInt(':products_id', (int)$products_id );
         $Qproducts->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
         $Qproducts->bindInt(':products_group_view', 1 );
         $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
         $Qproducts->execute();

       } else {

         $Qproducts = $OSCOM_PDO->prepare('select p.products_id,
                                                 pd.products_name,
                                                 p.products_model,
                                                 p.products_image,
                                                 p.products_price,
                                                 p.products_weight,
                                                 p.products_tax_class_id
                                          from :table_products p,
                                               :table_products_description pd
                                          where p.products_id = :products_id
                                          and pd.products_id = p.products_id
                                          and pd.language_id = :language_id
                                    ');
         $Qproducts->bindInt(':products_id', (int)$products_id);
         $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
         $Qproducts->execute();
       }

        if ($Qproducts->fetch() !== false) {

// Prix B2B ou client normal
          if (($OSCOM_Customer->getCustomersGroupID() != 0) && ($Qproducts->valueInt('price_group_view') == '1')) {
            $products_price = $Qproducts->valueDecimal('customers_group_price');
          } else {
            $products_price = $Qproducts->valueDecimal('products_price');
          }

// Prix promotionel B2B ou normal
          if (($OSCOM_Customer->getCustomersGroupID() != 0) && ($Qproducts->valueInt('price_group_view') == '1')) {

            $Qspecial = $OSCOM_PDO->prepare('select specials_new_products_price
                                              from :table_specials
                                              where products_id = :products_id
                                              and status = :status
                                              and customers_group_id = :customers_group_id
                                             ');
            $Qspecial->bindInt(':customers_group_id', $OSCOM_Customer->getCustomersGroupID() );
            $Qspecial->bindInt(':products_id', $Qproducts->valueInt('products_id'));
            $Qspecial->bindInt(':status', 1 );

          } else {

            $Qspecial = $OSCOM_PDO->prepare('select specials_new_products_price
                                              from :table_specials
                                              where products_id = :products_id
                                              and status = :status
                                             ');
            $Qspecial->bindInt(':products_id', $Qproducts->valueInt('products_id'));
            $Qspecial->bindInt(':status', 1 );
          }

          $Qspecial->execute();
          
          if ($Qspecial->fetch() !== false) {
            $products_price = $Qspecial->valueDecimal('specials_new_products_price');
          }

          if ($Qproducts->value('products_model_group') != '' && $OSCOM_Customer->getCustomersGroupID() != 0) {
            $model = $Qproducts->value('products_model_group');
          } else {
            $model = $Qproducts->value('products_model');
          }

          $products_array[] = array('id' => $products_id,
                                    'name' => $Qproducts->value('products_name'),
                                    'model' => $Qproducts->value('products_model'),
                                    'image' => $Qproducts->value('products_image'),
                                    'price' => $products_price,
                                    'quantity' => $this->contents[$products_id]['qty'],
                                    'weight' => $Qproducts->valueDecimal('products_weight'),
                                    'final_price' => ($products_price + $this->attributes_price($products_id)),
                                    'tax_class_id' => $Qproducts->valueInt('products_tax_class_id'),
                                    'attributes' => (isset($this->contents[$products_id]['attributes']) ? $this->contents[$products_id]['attributes'] : ''));
        }
      }

      return $products_array;
    }

    function show_total() {
      $this->calculate();

      return $this->total;
    }

    function show_weight() {
      $this->calculate();

      return $this->weight;
    }

    function generate_cart_id($length = 5) {
      return osc_create_random_value($length, 'digits');
    }

    function get_content_type() {
      global $OSCOM_PDO;

      $this->content_type = false;

      if ( (DOWNLOAD_ENABLED == 'true') && ($this->count_contents() > 0) ) {
        foreach ( array_keys($this->contents) as $products_id ) {
          if (isset($this->contents[$products_id]['attributes'])) {
            foreach ($this->contents[$products_id]['attributes'] as $value) {

              $Qcheck = $OSCOM_PDO->prepare('select pa.products_attributes_id 
                                            from :table_products_attributes pa,
                                                 :table_products_attributes_download pad
                                            where pa.products_id = :products_id
                                            and pa.options_values_id = :options_values_id
                                            and pa.products_attributes_id = pad.products_attributes_id
                                            limit 1
                                           ');
              $Qcheck->bindInt(':products_id', (int)$products_id  );
              $Qcheck->bindInt(':options_values_id', (int)$value );

              $Qcheck->execute();

              if ($Qcheck->fetch() !== false) {
                switch ($this->content_type) {
                  case 'physical':
                    $this->content_type = 'mixed';

                    return $this->content_type;
                    break;
                  default:
                    $this->content_type = 'virtual';
                    break;
                }
              } else {
                switch ($this->content_type) {
                  case 'virtual':
                    $this->content_type = 'mixed';

                    return $this->content_type;
                    break;
                  default:
                    $this->content_type = 'physical';
                    break;
                }
              }
            }
          } else {
            switch ($this->content_type) {
              case 'virtual':
                $this->content_type = 'mixed';

                return $this->content_type;
                break;
              default:
                $this->content_type = 'physical';
                break;
            }
          }
        }
      } else {
        $this->content_type = 'physical';
      }

      return $this->content_type;
    }

    function unserialize($broken) {
      for(reset($broken);$kv=each($broken);) {
        $key=$kv['key'];
        if (gettype($this->$key)!="user function")
        $this->$key=$kv['value'];
      }
    }
  }