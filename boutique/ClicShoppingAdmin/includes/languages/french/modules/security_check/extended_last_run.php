<?php
/**
 * extended_last_run.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_LAST_RUN_OLD', 'Cela fait plus de 30 jours depuis la derni�re vérification a été faite. Veuillez effectuer une vérification de votre système (Outils / Informations sécurité / Information ClicShopping.');
?>
