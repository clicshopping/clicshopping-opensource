<?php
/*
 * login.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
 require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('login') ) {
    echo $OSCOM_MessageStack->get('login');
  }

// ----------------------
// --- Title   -----  
// ---------------------- 
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
<div class="contentContainer">
  <div class="contentText" style="width: 100%; float: left; margin-left:5px;">
  <p><h3 class="loginText"><?php echo HEADING_NEW_CUSTOMER; ?></h3></p>
  <div class="contentText">
    <p class="mainLogin"><?php echo TEXT_NEW_CUSTOMER; ?></p>
    <p class="mainLogin"><?php echo TEXT_NEW_CUSTOMER_INTRODUCTION; ?></p>
  </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="contentContainer"><?php echo $OSCOM_Template->getBlocks('modules_login'); ?></div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>