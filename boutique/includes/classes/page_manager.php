<?php
/*
 * page_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 1.0
*/

////
// Affichage des pages d'introduction, accueil, contact et informations spécifier par groupe et le ID ($identifier)

  class osc_display_page_manager {
    var $pages_string,
        $pages_introduction,
        $pages_accueil,
        $pages_contact,
        $pages_liste_info_box,
        $pages_liste_info_box_secondary,
        $pages_liste_info_box_footer,
        $pages_informations;

    function osc_display_page_manager($groupe = '', $id = '') {
      global $OSCOM_Customer, $OSCOM_PDO;


      if ($groupe == 'introduction') {

//***********************************
// -------- Introduction -----------
//***********************************

        $this->pages_introduction = array();

        $Qpages = $OSCOM_PDO->prepare('select count(*) as count 
                                       from :table_pages_manager
                                       where status = :status 
                                       and page_type = :page_type
                                      ');
        $Qpages->bindValue(':status', '1' );
        $Qpages->bindValue(':page_type', '1' );
        $Qpages->execute();

        $pages = $Qpages->fetch();

        if ($pages['count'] > 0) {

          $Qpages = $OSCOM_PDO->prepare('select p.pages_id,
                                             p.page_time,
                                             s.pages_html_text
                                        from :table_pages_manager p,
                                             :table_pages_manager_description s
                                        where p.status = :status
                                        and p.pages_id = s.pages_id
                                        and p.page_type = :page_type
                                        and s.language_id= :language_id
                                        order by rand()
                                        limit 1
                                      ');
          $Qpages->bindValue(':status', '1' );
          $Qpages->bindValue(':language_id', (int)$_SESSION['languages_id'] );
          $Qpages->bindValue(':page_type', '1' );
          $Qpages->execute();

          $pages = $Qpages->fetch();

          $this->pages_introduction = array('pages_id' => $pages['pages_id'],
                                            'page_time' => $pages['page_time'],
                                            'pages_html_text' => $pages['pages_html_text']);
        } else {
          $this->pages_introduction = array('pages_id' => '0',
                                            'page_time' => '0',
                                            'pages_html_text' => '');
        }


//***********************************
// -------- Index Page -----------
//***********************************


      } else if ($groupe == 'accueil') {
        $this->pages_accueil = array();

        $Qpages = $OSCOM_PDO->prepare('select count(*) as count 
                                       from :table_pages_manager
                                       where status = :status 
                                       and page_type = :page_type
                                      ');
        $Qpages->bindValue(':status', '1' );
        $Qpages->bindValue(':page_type', '2' );
        $Qpages->execute();

        $pages = $Qpages->fetch();

        if ($pages['count'] > 0) {

          $Qpages = $OSCOM_PDO->prepare('select p.pages_id,
                                                s.pages_html_text
                                        from :table_pages_manager p LEFT JOIN :table_pages_manager_description s on p.pages_id = s.pages_id
                                        where p.status = :status
                                        and p.page_type = :page_type
                                        and (s.language_id  = :language_id or
                                            s.language_id = 0)
                                        and (p.customers_group_id = :customers_group_id or
                                             p.customers_group_id = 99
                                            )
                                        order by rand()
                                        limit 1
                                        ');
          $Qpages->bindInt(':status', '1' );
          $Qpages->bindInt(':page_type', '2' );
          $Qpages->bindInt(':language_id', (int)$_SESSION['languages_id'] );
          $Qpages->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID()  );
          $Qpages->execute();

          $pages = $Qpages->fetch();

          if (isset($pages['pages_id'])) {
            $this->pages_accueil = array('pages_id' => $pages['pages_id'],
                                         'pages_html_text' => $pages['pages_html_text']);
          }
        } else {
          $this->pages_accueil = array('pages_id' => '0',
                                       'pages_html_text' => '');
        }

//***********************************
// -------- Contact page -----------
//***********************************

      } else if ($groupe == 'contact') {
        $this->pages_contact = array();

        $QPage = $OSCOM_PDO->prepare("select  p.pages_id,
                                               s.pages_html_text
                                       from :table_pages_manager p,
                                            :table_pages_manager_description s
                                       where p.pages_id = s.pages_id
                                       and p.status = :status
                                       and p.page_type = :page_type
                                       and (s.language_id  = :language_id or s.language_id  = 0)
                                       and (p.customers_group_id = :customers_group_id  or p.customers_group_id = 99)
                                    ");
        $QPage->bindInt(':status', '1' );
        $QPage->bindInt(':page_type', '3' );
        $QPage->bindInt(':language_id', (int)$_SESSION['languages_id'] );
        $QPage->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );

        $QPage->execute();
        $pages = $QPage->fetch();

        if ( $QPage->fetch() !== true ) {

          $this->pages_contact = array('pages_id' => $pages['pages_id'],
                                       'pages_html_text' => $pages['pages_html_text']);
        } else {
          $this->pages_contact = array('pages_id' => '0',
                                       'pages_html_text' => '');
        }
      } else if ($groupe == 'liste_info_box') {
        $this->pages_liste_info_box = array();

        $QPage = $OSCOM_PDO->prepare("select  p.pages_id,
                                              p.sort_order,
                                              p.status,
                                              p.page_box,
                                              s.pages_title,
                                              s.pages_html_text,
                                              s.externallink,
                                              p.links_target
                                       from :table_pages_manager p,
                                            :table_pages_manager_description s
                                       where p.pages_id = s.pages_id
                                       and p.status = :status
                                       and (p.page_type = 4 or p.page_type = 3)
                                       and (s.language_id  = :language_id or s.language_id  = '0')
                                       and p.page_box = 0
                                       and (p.customers_group_id = :customers_group_id  or p.customers_group_id = 99)
                                       order by p.sort_order, s.pages_title
                                          ");
        $QPage->bindInt(':status', '1' );
        $QPage->bindInt(':language_id', (int)$_SESSION['languages_id'] );
        $QPage->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );


        $QPage->execute();

        $rows = 0;

//***********************************
// -------- boxe et footer -----------
//***********************************
        $page_liste_footer = '<div class="footerPageManager">';
        $page_liste_box = '<div>';

        while ($page = $QPage->fetch()) {

          $rows++;

          if($rows != '1')  {
            $separation = " | ";
          } else {
            $separation = "";
          }

          if ($page['externallink'] != '')  {
           $page_liste_box .= '<div><span class="ListManager"><a href="' . $page['externallink'] . '" target="' .$page['links_target'] . '">' . $page['pages_title'] . '</a></span></div>';
           $page_liste_footer .= '<span class="footerPageManager">' . $separation . '<a href="' . $page['externallink'] . '" target="' .$page['links_target'] . '" class="footer">' . $page['pages_title'] . '</a></span>';
          } else {
            $link = osc_href_link('page_manager.php', 'pages_id=' . (int)$page['pages_id']);
            $page_liste_box .= '<div><span class="ListManager"><a href="' . $link . '" target="' .$page['links_target'] . '">' . $page['pages_title'] . '</a></span></div>';	
            $page_liste_footer .= '<span class="footerPageManager">' . $separation . '<a href="' . $link . '" target="' .$page['links_target'] . '" class="footer">' . $page['pages_title'] . '</a></span>';
          }
        }

        $page_liste_box .= '</div>';
        $this->pages_liste_info_box = array('text' => $page_liste_box);

        $page_liste_footer .= '</div>';
        $this->pages_liste_info_box_footer = array('text' => $page_liste_footer);

//***********************************
// -------- Deuxième Boxe -----------
//***********************************

      } else if ($groupe == 'liste_info_box_secondary') {

        $Qcheck = $OSCOM_PDO->prepare('select count(*) as count
                                       from :table_pages_manager
                                       where status = :status
                                       and page_box = :page_box
                                      ');
        $Qcheck->bindValue(':status', '1' );
        $Qcheck->bindValue(':page_box', '1' );
        $Qcheck->execute();

        $pages_secondary = $Qcheck->fetch();

        $this->pages_liste_info_box_secondary = array();

        $QPageSecondary = $OSCOM_PDO->prepare("select  p.pages_id,
                                                      p.sort_order,
                                                      p.status,
                                                      p.page_box,
                                                      s.pages_title,
                                                      s.pages_html_text,
                                                      s.externallink,
                                                      p.links_target
                                                 from :table_pages_manager p,
                                                      :table_pages_manager_description s
                                                 where p.pages_id = s.pages_id
                                                 and p.status = :status
                                                 and (p.page_type = 4 or p.page_type = 3)
                                                 and (s.language_id  = :language_id or s.language_id  = '0')
                                                 and p.page_box = 1
                                                 and (p.customers_group_id = :customers_group_id  or p.customers_group_id = 99)
                                                 order by p.sort_order, s.pages_title
                                              ");
        $QPageSecondary->bindInt(':status', '1' );
        $QPageSecondary->bindInt(':language_id', (int)$_SESSION['languages_id'] );
        $QPageSecondary->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );

        $QPageSecondary->execute();

        $rows = 0;
        $page_liste_box_secondary = '<div>';

        while ($pages_secondary = $QPageSecondary->fetch()) {
          $rows++;

          if($rows != '1')  {
            $separation = " | ";
          } else {
            $separation = "";
          }

          if ($pages_secondary['externallink'] != '')  {
            $page_liste_box_secondary .= '<div><span class="ListManagerCustomize"><a href="' . $pages_secondary['externallink'] . '" target="' .$pages_secondary['links_target'] . '">' . $pages_secondary['pages_title'] . '</a></span></div>';				
          } else {
             $link_secondary = osc_href_link('page_manager.php', 'pages_id=' . $pages_secondary['pages_id']);
             $page_liste_box_secondary .= '<div><span class="ListManagerCustomize"><a href="' .$link_secondary . '" target="' .$pages_secondary['links_target'] . '">' . $pages_secondary['pages_title'] . '</a></span></div>';				
          }
        }
        $page_liste_box_secondary .= '</div>';
        $this->pages_liste_info_box_secondary = array('text' => $page_liste_box_secondary);

      } elseif ($groupe == 'informations') {

//***********************************
// -------- Information page -----------
//***********************************

        $this->pages_informations = array();

        $QPage = $OSCOM_PDO->prepare("select p.pages_id,
                                            p.page_type,
                                            s.pages_title,
                                            s.pages_html_text
                                       from :table_pages_manager p,
                                            :table_pages_manager_description s
                                       where p.pages_id = s.pages_id
                                       and p.status = :status
                                       and (p.page_type = 3 or p.page_type = 4)
                                       and p.pages_id = :pages_id
                                       and (s.language_id  = :language_id or s.language_id  = 0)
                                       and (p.customers_group_id = :customers_group_id  or p.customers_group_id = 99)
                                    ");
        $QPage->bindInt(':status', '1' );
        $QPage->bindInt(':pages_id', (int)$id  );
        $QPage->bindInt(':language_id', (int)$_SESSION['languages_id'] );
        $QPage->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );

        $QPage->execute();
        $pages = $QPage->fetch();

        if ( $QPage->fetch() !== true ) {

          if ($pages['page_type'] == '3') {
          $url = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'contact_us.php';
          header('Location: ' . $url);
          } else {
            $this->pages_informations = array('pages_id' => $pages['pages_id'],
                                              'pages_title' => $pages['pages_title'],
                                              'pages_html_text' => $pages['pages_html_text']);
          }
        } else {
          $this->pages_informations = array('pages_id' => '0',
                                            'pages_title' => '',
                                            'pages_html_text' => '');
        }
      }
    }
  }
