<?php
/*
 * checkout_shipping.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  echo osc_draw_form('checkout_address', osc_href_link('checkout_shipping.php', '', 'SSL'), 'post', '', true) . osc_draw_hidden_field('action', 'process');
?>

<div class="contentContainer">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <legend><?php echo TABLE_HEADING_SHIPPING_ADDRESS; ?></legend>
  <div class="contentText">
    <div class="infoBoxContainer" style="float: right;">
      <div class="infoBoxHeading"><?php echo TITLE_SHIPPING_ADDRESS; ?></div>
      <div class="infoBoxContents">
        <?php echo osc_address_label($OSCOM_Customer->getID(), $_SESSION['sendto'], true, ' ', '<br />'); ?>
      </div>
    </div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <?php echo TEXT_CHOOSE_SHIPPING_DESTINATION; ?><br /><br />

<?php
// Autorise l'ajout dans le carnet d'adresse des clients B2B ou clients normaux
  if (osc_count_customers_modify_address_default() == 1) {
?>
     <span class="smallText"><?php echo osc_draw_button(IMAGE_BUTTON_CHANGE_ADDRESS, null, osc_href_link('checkout_shipping_address.php', '', 'SSL'), 'primary'); ?></span>
<?php
  }
?>
  </div>
  <div class="clearfix"></div>
<?php
  if (osc_count_shipping_modules() > 0) {
?>
  <h3><?php echo TABLE_HEADING_SHIPPING_METHOD; ?></h3>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<?php
    if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
?>
  <div class="contentText">
    <div style="pull-right;"><?php echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?> </div>
    <?php echo TEXT_CHOOSE_SHIPPING_METHOD; ?>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<?php
    } elseif ($free_shipping == false) {
?>
  <div class="contentText">
    <div><?php echo TEXT_ENTER_SHIPPING_INFORMATION; ?></div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  </div>
<?php
    }
?>
  <div class="contentText">
    <table class="table table-condensed table-hover">
      <tbody>

<?php
    if ($free_shipping == true) {
?>
    <div class="contentText">
        <div><strong><?php echo FREE_SHIPPING_TITLE; ?></strong>&nbsp;<?php echo $quotes[$i]['icon']; ?></div>
        <div style="padding-left: 15px;"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . osc_draw_hidden_field('shipping', 'free_free'); ?></div>
    </div>
<?php
    } else {

      for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
        for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
// set the radio button to be checked if it is the method chosen
          $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $shipping['id']) ? true : false);

          echo '      <tr>' . "\n";
?>

        <td>
          <strong><?php echo $quotes[$i]['module']; ?></strong>
<?php
          if (isset($quotes[$i]['icon']) && osc_not_null($quotes[$i]['icon'])) echo '&nbsp;' . $quotes[$i]['icon'];
?>

<?php
          if (isset($quotes[$i]['error'])) {
            echo '<div class="help-block">' . $quotes[$i]['error'] . '</div>';
          }
?>

<?php
          if (osc_not_null($quotes[$i]['methods'][$j]['title'])) echo '<div class="help-block">' . $quotes[$i]['methods'][$j]['title'] . '</div>';
?>
        </td>

<?php
          if ( ($n > 1) || ($n2 > 1) ) {
?>

        <td align="right" valign="middle">
<?php
          if (isset($quotes[$i]['error'])) {
// nothing
            echo '&nbsp;';
          } else {
             echo '<p style="float:right;">'  . osc_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked, 'required aria-required="true"') .'</p>';
             echo '<p style="padding-top:10px; padding-right:30px; float:right">'. $currencies->format(osc_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . '</p>&nbsp;&nbsp';
          }
?>
        </td>
<?php
          } else {
?>

         <td align="right">
            <?php echo $currencies->format(osc_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . osc_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']); ?>
         </td>

<?php
          }
?>
      </tr>

<?php

        }
      }
    }
?>

      </tbody>
    </table>
  </div>

<?php
  }
?>
  <div class="hr"></div>

  <div class="contentText">
    <div class="form-group">
      <label for="inputComments">
         <h3><?php echo TABLE_HEADING_COMMENTS; ?></h3>
       </label>
       <div><?php echo osc_draw_textarea_field('comments', 'soft', 60, 5, $_SESSION['comments'], 'id="inputComments" placeholder="' . TABLE_HEADING_COMMENTS . '"'); ?></div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="contentText">
    <div class="pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null); ?></div>
  </div>
</div>
</form>
<div class="contentContainer"><?php echo $OSCOM_Template->getBlocks('modules_checkout_shipping'); ?></div>
<?php   require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
