<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Commentaires');
define('HEADING_TITLE', 'Voulez vous nous faire part de vos impressions ?');
define('SUB_TITLE_PRODUCT', 'Article :');
define('SUB_TITLE_FROM', 'De :');
define('SUB_TITLE_REVIEW', 'Commentaires :');
define('SUB_TITLE_RATING', 'Classement :');

define('TEXT_NO_HTML', '<p class="x-small"><style="color:#ff0000;"><strong>NOTE:</strong>&nbsp;Le HTML n\'est pas converti !</p>');
define('TEXT_BAD', '<p class="x-small"><style="color:#ff0000;"><strong>MAUVAIS</strong></p>');
define('TEXT_GOOD', '<p class="x-small"><style="color:#ff0000;"><strong>BON</strong></p>');

define('TEXT_CLICK_TO_ENLARGE', 'Cliquer pour agrandir');

define('EMAIL_SUBJECT','Nouveau commentaire proposé par un client sur ' . STORE_NAME);
define('EMAIL_TEXT','Un nouveau commentaire a été inséré par un client concernant un produit. Veuillez consulter votre administration pour valider ou non le commentaire du client');
?>
