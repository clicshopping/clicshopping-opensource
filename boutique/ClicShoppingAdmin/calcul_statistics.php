<?php
/**
 * calcul_statistics.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: calcul_statistics.php 
 */

// Calcul concernant les statistiques generales
    $customers_total = stat_gen1();
    $products_total =  stat_gen2();
    $reviews_total =  stat_gen3();
    $customers_total_newsletter = stat_customers_newsletter();
    $customers_total_notification = stat_customers_notification();
    $customers_recover_shopping_cart = stat_recover_shopping_cart();
    $customers_total_newsletter_no_account = stat_customers_newsletter_no_account();
    $stats_order_total_delivery = stat_order_total_delivery();
    $contact_customers_total = stat_contact_customers();

    
// Calcul CA annuel
    $ca_total =  stat_total($total); // sur toutes les annees
    $annuel3 = stat3($annuel3); // annee en cours n-3
    $annuel2 =  stat2($annuel2); // annee en cours n-2
    $annuel1 =  stat1($annuel1); // annee en cours n-1
    $annuel =   stat0($annuel); // annee en cours

// Calcul Taux de croissance sur l'annee precedente
    if (($annuel == 0) or ($annuel1 == 0)) {
        $taux_croissance_annee1 = 0;
      } else {
        $taux_croissance_annee1 = (round(($annuel/$annuel1)-1,2)*100);
      }
      if (($annuel1 == 0) or ($annuel2 == 0)) {
        $taux_croissance_annee2 = 0 ;
      } else {
        $taux_croissance_annee2 =  (round(($annuel1/$annuel2)-1,2)*100);
      }
      if (($annuel2 == 0) or ($annuel3 == 0)) {
        $taux_croissance_annee3 = 0 ;
      } else {
        $taux_croissance_annee3 =  (round(($annuel2/$annuel3)-1,2)*100);
      }

// Calcul CA Mensuel
  $month_janvier1 = round(stat1_month(),0);
  $month_fevrier1 = round(stat2_month(),0);
  $month_mars1 = round(stat3_month(),0);
  $month_avril1 = round(stat4_month(),0);
  $month_mai1 = round(stat5_month(),0);
  $month_juin1 =  round(stat6_month(),0);
  $month_juillet1 = round(stat7_month(),0);
  $month_aout1 = round(stat8_month(),0);
  $month_septembre1 = round(stat9_month(),0);
  $month_octobre1 = round(stat10_month(),0);
  $month_novembre1 = round(stat11_month(),0);
  $month_decembre1 = round(stat12_month(),0);


// Calcul CA Mensuel cumule
  $month_janvier = round(stat1_month(),0);
  $month_fevrier = round(stat2_month() +$month_janvier,0);
  $month_mars = round(stat3_month() + $month_fevrier,0);
  $month_avril = round(stat4_month() + $month_mars,0);
  $month_mai = round(stat5_month() +  $month_avril,0);
  $month_juin =  round(stat6_month() +  $month_mai,0);
  $month_juillet = round(stat7_month() +  $month_juin,0);
  $month_aout = round(stat8_month() +  $month_juillet,0);
  $month_septembre = round(stat9_month() +  $month_aout,0);
  $month_octobre = round(stat10_month() +  $month_septembre,0);
  $month_novembre = round(stat11_month() +  $month_octobre,0);
  $month_decembre = round(stat12_month() +  $month_novembre,0);

// Calcul du nbr de client annuel
  $customers_total_annee = stat_customers_annee();
  $customers_total_annee1 = stat_customers_annee();
  $customers_total_annee2 = stat_customers_annee();
  $customers_total_annee3 = stat_customers_annee();


// calcul achat moyen sur le total des commandes statut livre
    if (($ca_total == 0) || ($stats_order_total_delivery == 0)) {
      $purchase_average_order_total_delivery = 0;
    } else {
      $purchase_average_order_total_delivery = (round(($ca_total/$stats_order_total_delivery),2));
    }



// Calcul du panier Moyen par annee
// Sur toutes les annees
// All the years
    if (($annuel == 0) || ($customers_total_annee == 0)) {
      $panier_client_total = 0;
    } else {
      $panier_client_total = (round(($ca_total/$customers_total),2));
    }

    if (($annuel == 0) || ($customers_total_annee == 0)) {
      $panier_client_annee = 0;
    } else {
      $panier_client_annee = (round(($annuel/$customers_total_annee),2));
    }

    if (($annuel1 == 0) || ($customers_total_annee1 == 0)) {
      $panier_client_annee1 = 0 ;
    } else {
      $panier_client_annee1 =  (round(($annuel1/$customers_total_annee1),2));
    }

    if (($annuel2 == 0) || ($customers_total_annee2 == 0)) {
      $panier_client_annee2 = 0 ;
    } else {
      $panier_client_annee2 =  (round(($annuel2/$customers_total_annee2),2));
    }

    if (($annuel3 == 0) || ($customers_total_annee3 == 0)) {
      $panier_client_annee3 = 0 ;
    } else {
      $panier_client_annee3 =  (round(($annuel3/$customers_total_annee3),2));
    }



// Calcul du panier Moyen par annee
    if (($panier_client_annee == 0) or ($panier_client_annee1 == 0)) {
      $croissance_panier_client_annee = 0;
    } else {
      $croissance_panier_client_annee = (round(($panier_client_annee / $panier_client_annee1)-1,2)*100);
    }

    if (($panier_client_annee1 == 0) or ($panier_client_annee2 == 0)) {
      $croissance_panier_client_annee1 = 0;
    } else {
      $croissance_panier_client_annee1 = (round(($panier_client_annee1 / $panier_client_annee2)-1,2)*100);
    }

    if (($panier_client_annee2 == 0) or ($panier_client_annee3 == 0)) {
      $croissance_panier_client_annee2 = 0;
    } else {
      $croissance_panier_client_annee2 = (round(($panier_client_annee2 / $panier_client_annee3)-1,2)*100);
    }

    if (($panier_client_annee3 == 0) or ($panier_client_annee4 == 0)) {
      $croissance_panier_client_annee3 = 0;
    } else {
      $croissance_panier_client_annee3 = (round(($panier_client_annee3 / $panier_client_annee4)-1,2)*100);
    }
