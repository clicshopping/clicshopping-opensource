<?php
  /**
   * cfm_modules_blog.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_blog {
    var $code = 'modules_blog';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_BLOG_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_blog() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA .  '/modules/modules_blog/';
      $this->title = MODULE_CFG_MODULE_BLOG_TITLE;
    }
  }
?>
