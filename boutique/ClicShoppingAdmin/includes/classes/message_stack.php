<?php
/*
 * message_stack.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

  Example usage:

  $OSCOM_MessageStack = new messageStack();
  $OSCOM_MessageStack->add('Error: Error 1', 'error');
  $OSCOM_MessageStack->add('Error: Error 2', 'warning');
  if ($OSCOM_MessageStack->size > 0) echo $OSCOM_MessageStack->output();
*/
/**
 * The MessageStack class manages information messages to be displayed.
 * Messages that are shown are automatically removed from the stack.
 */

  class messageStack extends tableBlock {
    var $size = 0;

    function messageStack() {
      $this->errors = array();

      if (isset($_SESSION['messageToStack'])) {
        for ($i = 0, $n = sizeof($_SESSION['messageToStack']); $i < $n; $i++) {
          $this->add($_SESSION['messageToStack'][$i]['text'], $_SESSION['messageToStack'][$i]['type']);
        }
        unset($_SESSION['messageToStack']);
      }
    }

/**
 * Add a message to the stack
 *
 * @param string $group The group the message belongs to
 * @param string $message The message information text
 * @param string $type The type of message: error, warning, success
 * @access public
 */
    function add($message, $type = 'error') {
      if ($type == 'error') {
        $this->errors[] = array('params' => 'class="messageStackError"', 'text' => '<div>' . osc_image(DIR_WS_ICONS . 'warning.gif', ICON_ERROR) . '<span style="padding-left:5px; font-weight:bold;">' . $message . '</span></div>');
      } elseif ($type == 'warning') {
        $this->errors[] = array('params' => 'class="messageStackWarning"', 'text' => '<div>' . osc_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING) . '<span style="padding-left:5px; font-weight:bold;">' . $message . '</span></div>');
      } elseif ($type == 'success') {
        $this->errors[] = array('params' => 'class="messageStackSuccess"', 'text' => '<div>' . osc_image(DIR_WS_ICONS . 'success.gif', ICON_SUCCESS) . '<span style="padding-left:5px; font-weight:bold;">' . $message . '</span></div>');
      } else {
        $this->errors[] = array('params' => 'class="messageStackError"', 'text' => $message);
      }

      $this->size++;
    }

    function add_session($message, $type = 'error') {
      if (!isset($_SESSION['messageToStack'])) {
        $_SESSION['messageToStack'] = array();
      }

      $_SESSION['messageToStack'][] = array('text' => $message, 'type' => $type);
    }

    function reset() {
      $this->errors = array();
      $this->size = 0;
    }

    function output() {
      $this->table_data_parameters = 'class="messageBox"';
      return $this->tableBlock($this->errors);
    }
  }
?>
