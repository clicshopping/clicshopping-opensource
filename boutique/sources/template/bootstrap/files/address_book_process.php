<?php
/*
 * address_book_process.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('addressbook') ) {
    echo $OSCOM_MessageStack->get('addressbook');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  if (!isset($_GET['delete'])) echo osc_draw_form('addressbook', osc_href_link('address_book_process.php', (isset($_GET['edit']) ? 'edit=' . $_GET['edit'] : ''), 'SSL'), 'post', '', true);
?>

<div class="contentContainer">
  <div class="contentText">
    <h2><?php if (isset($_GET['edit'])) { echo HEADING_TITLE_MODIFY_ENTRY; } elseif (isset($_GET['delete'])) { echo HEADING_TITLE_DELETE_ENTRY; } else { echo HEADING_TITLE_ADD_ENTRY; } ?></h2>
  </div>
</div>
<?php
// ------------------------------- 
// --- Delete Adresse Title   -----  
// ------------------------------
  
  if (isset($_GET['delete'])) {
?>
<div class="contentContainer">
  <h3><?php echo DELETE_ADDRESS_TITLE; ?></h3>
  <div class="contentText">
    <strong><?php echo DELETE_ADDRESS_DESCRIPTION; ?></strong>
     <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td align="right" width="50%" valign="top"><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main" align="center" valign="top"><strong><?php echo SELECTED_ADDRESS; ?></strong></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
            <td class="main" valign="top"><?php echo osc_address_label($OSCOM_Customer->getID(), $_GET['delete'], true, ' ', '<br />'); ?></td>
          </tr>
        </table></td>
      </tr>
    </table>
  </div>
<?php
// ---------------------- 
// --- Button   -----  
// ---------------------- 
?>

    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="buttonSet">
      <div class="col-xs-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('address_book.php', '', 'SSL'), 'info'); ?></div>
      <div class="col-xs-6 text-right"><span class="buttonAction"><?php echo osc_draw_button(IMAGE_BUTTON_CONFIRM_DELETE, null, osc_href_link('address_book_process.php', 'delete=' . $_GET['delete'] . '&action=deleteconfirm&formid=' . md5($_SESSION['sessiontoken']), 'SSL'), 'success'); ?></span></div>
    </div>
  </div>
<?php
  } else {
// ---------------------- 
//  Address Detail   
// ----------------------
?>
  <div class="contentContainer">
<?php
  require($OSCOM_Template->getTemplateModules('customers_address/address_book_details'));
?>
  </div>
<?php
    if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
?>
  <div class="contentText">
<?php
     if ($_GET['newcustomer'] != 1) {
?>
      <div class="col-sm-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('address_book.php', '', 'SSL'),'primary'); ?></div>
<?php
     }
?>
     <div class="col-sm-6 pull-right" align="right"><?php echo osc_draw_hidden_field('action', 'update') . osc_draw_hidden_field('edit', $_GET['edit']) . osc_draw_hidden_field('shopping', $_GET['shopping']) . osc_draw_button(IMAGE_BUTTON_UPDATE, 'refresh', null, 'success'); ?></div>
  </div>
  <div class="clearfix"></div>
<?php
    } else {
      if (sizeof($_SESSION['navigation']->snapshot) > 0) {
        $back_link = osc_href_link($_SESSION['navigation']->snapshot['page'], osc_array_to_string($_SESSION['navigation']->snapshot['get'], array(session_name())), $_SESSION['navigation']->snapshot['mode']);
      } else {
        $back_link = osc_href_link('address_book.php', '', 'SSL');
      }
// ----------------------
// --- Button   -----
// ----------------------
?>
    <div class="buttonSet row">
      <div class="col-xs-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, $back_link, 'primary'); ?></div>
      <div class="col-xs-6 text-right"><span class="buttonAction"><?php echo osc_draw_hidden_field('action', 'process') . osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success'); ?></span></div>
    </div>
<?php
    }
?>

</form>

<?php
  }
?>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
