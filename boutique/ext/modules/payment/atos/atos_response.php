<?php
/*
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  chdir('../../../../');
  require('includes/application_top.php');

  include(DIR_WS_MODULES . 'payment/atos.php');
//  include(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/payment/atos.php');

  if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . '/languages/'. $_SESSION['language']  . '/modules/payment/atos.php') ) {
    require(DIR_WS_TEMPLATE . SITE_THEMA . '/languages/'. $_SESSION['language']  . '/modules/payment/atos.php');
    require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/payment/atos.php');
  } else {
    require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/modules/payment/atos.php');
  }


  $atos = new atos();
  $hash = $atos->decodeResponse($_POST['DATA']);

  /* check response data and code and update session information */
  if ($hash['code'] != 0) {
    /* An internal error occurs: inform the customer and invite him to contact
         our service and team */
    // FIXME
    $atos->sendNotification('ATOS Response Error', $hash);
    osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
  }

  $atos_reason = '';

  /* Check the response code */
  switch ($hash['response_code'])
  {
  case '00':
    /* Transaction approved */
    /* Check whether the merchant id is the expected one */
    osc_redirect(MODULE_PAYMENT_ATOS_PRODUCTION_MODE == 'false' ? osc_href_link('checkout_payment.php', 'info_message=' . urlencode(MODULE_PAYMENT_ATOS_TEXT_CHECKOUT_DEMO),'SSL') : osc_href_link('checkout_success.php', '', 'SSL'));
    break;
  case '02':
    /* Referral, autorization required by phone
       PROBLEM LOCATION: CUSTOMER */
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_02;
    break;
  case '03':
    /* MERCHANT_ID invalid
       PROBLEM LOCATION: MERCHANT */
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_03;
    break;
  case '05':
    /* Transaction cancelled. No detail about security
       PROBLEM LOCATION: BANK */
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_05;
    break;
  case '12':
    /* Invalid amount
       PROBLEM LOCATION: MERCHANT */
    $atos->sendNotification('ATOS Transaction Rejected - Invalid Amount', $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_12;
    break;
  case '13':
    /* Invalid transaction, fields invalid: (used of AMEX, without any AMEX contract)
       PROBLEM LOCATION: MERCHANT */
    $atos->sendNotification('ATOS Transaction Rejected - Invalid Transaction', $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_13;
    break;
  case '17':
    /* Cancelled by CUSTOMER
       PROBLEM LOCATION: CUSTOMER */
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_17;
    break;
  case '30':
    /* format error: contact CyberPlus hotline for more details
       PROBLEM LOCATION: MERCHANT/BANK */
    $atos->sendNotification('ATOS Transaction Rejected - Format Error', $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_30;
    break;
  case '63':
    /* HIGH PROBLEM OF SECURITY: must log out the customer and remove its session id
       PROBLEM LOCATION: CUSTOMER */
    $atos->sendNotification('ATOS Transaction Rejected - Security Problem', $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_63;
    break;
  case '75':
    /* More than 3 tries failed
       PROBLEM LOCATION: CUSTOMER */
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_75;
    break;
  case '90':
    /* Service unavailable
       PROBLEM LOCATION: MERCHANT */
    $atos->sendNotification('ATOS Transaction Rejected - Service unavailable', $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_90;
    break;
  case '94':
    /* Transaction already played
       PROBLEM LOCATION: MERCHANT */
    $atos->sendNotification('ATOS Transaction Rejected - Transaction already played', $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_94;
    break;
  default:
    /* Any other code are not recognized yet. Probably a security problem
       Assume we can remove the session id and log out the customer */
    $atos->sendNotification('ATOS Transaction Rejected - Invalid Code ' . $hash['response_code'], $hash);
    $atos_reason = MODULE_PAYMENT_ATOS_ERROR_INVALID_CODE;
  }

  osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
?>
