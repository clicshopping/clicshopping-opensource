<?php
/*
 * tax_classes.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Classes fiscales');

define('TABLE_HEADING_TAX_CLASSES', 'Classes fiscales');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_TAX_DESCRIPTION','Description');

define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_CLASS_TITLE', 'Titre de la classe fiscale :');
define('TEXT_INFO_CLASS_DESCRIPTION', 'Description :');
define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_INFO_INSERT_INTRO', 'Merci d\'entrer la nouvelle classe fiscale avec ses données liées');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer cette classe fiscale ?');
define('TEXT_INFO_HEADING_NEW_TAX_CLASS', 'Nouvelle classe fiscale');
define('TEXT_INFO_HEADING_EDIT_TAX_CLASS', 'Editer classe fiscale');
define('TEXT_INFO_HEADING_DELETE_TAX_CLASS', 'Supprimer classe fiscale');
?>