<?php
/*
 * stats_suppliers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();

// initialisation des dates et recuperation
  $expires_date = osc_db_prepare_input($_POST['expires_date']);
  $date_scheduled = osc_db_prepare_input($_POST['date_scheduled']);
  $orders_status = osc_db_prepare_input($_POST['orders_status']);

  if ($expires_date == 0) {
    $expires_date = date("Y-m-d 23:59:59");   
    $end_date = $expires_date;
    $end_date = date("d/m/Y H:i:s");
  } else {
    $end_date = $expires_date;
  } 
  
  if ($date_scheduled == 0) {
    $date_scheduled = date("Y-m-d 00:00:00");
    $start_date = $date_scheduled;
    $start_date = date("d/m/Y");
  } else {
    $start_date = $date_scheduled;  
  }


  if (osc_not_null($expires_date)) {
  list($day, $month, $year) = explode('/', $expires_date);
  $expires_date = $year .
          ((strlen($month) == 1) ? '0' . $month : $month) .
          ((strlen($day) == 1) ? '0' . $day : $day);
  }
  
  if (osc_not_null($date_scheduled)) {
  list($day, $month, $year) = explode('/', $date_scheduled);
  $date_scheduled = $year .
            ((strlen($month) == 1) ? '0' . $month : $month) .
            ((strlen($day) == 1) ? '0' . $day : $day);
  }


// Date management  
    $parameters = array('expires_date' => '',
                        'date_scheduled' => $start_date
                       );

    $bInfo = new objectInfo($parameters);

// orders_status
  $orders_statuses = array();
  $orders_status_array = array();

  $QordersStatus = $OSCOM_PDO->prepare('select orders_status_id,
                                               orders_status_name
                                        from :table_orders_status
                                        where language_id = :language_id
                                        order by orders_status_id
                                       ');

  $QordersStatus->bindInt(':language_id', (int)$_SESSION['languages_id'] );
  $QordersStatus->execute();

  while ($orders_status = $QordersStatus->fetch() ) {
    $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                               'text' => $orders_status['orders_status_name']);
    $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $Qstatus = $OSCOM_PDO->prepare("select orders_status_id, 
                                         orders_status_name 
                                  from :table_orders_status
                                  where language_id = :language_id
                                  and orders_status_id = :orders_status_id
                                ");
  $Qstatus->bindInt(':language_id',  (int)$_SESSION['languages_id']);
  $Qstatus->bindInt(':orders_status_id', (int)$_GET['bOS']);
  $Qstatus->execute();

  $status = $Qstatus->fetch();

  if (osc_not_null($action)) {
    switch ($action) {
     case 'edit':

     $suppliers_query_model_raw = "select distinct p.products_model,
                                                  s.suppliers_id, 
                                                  sum(op.products_quantity) as sum_qty,	 
                                                  s.suppliers_name,
                                                  op.products_name, 
                                                  opa.products_options, 
                                                  opa.products_options_values   
                                  from orders_products  op
                                     left join products  p ON op.products_id = p.products_id
                                     left join suppliers s on p.suppliers_id = s.suppliers_id
                                     left join orders  o ON op.orders_id = o.orders_id
                                     left join orders_products_attributes opa ON op.orders_products_id = opa.orders_products_id
                                  where o.date_purchased between '". $_GET['bDS']."'  and '".$_GET['bED']."'
                                  and s.suppliers_id = '" . (int)$_GET['bID'] . "'
                                  and o.orders_status = '" .(int)$_GET['bOS'] ."'  
                                  group by op.products_name, 
                                           opa.products_options, 
                                           opa.products_options_values
                                  order by p.products_model, 
                                            op.products_name
                          ";
      break;

    case 'customer':
      $customers_query_raw = "select distinct o.customers_id,
                                              o.customers_name,
                                            s.suppliers_id,
                                              s.suppliers_name,
                                              p.products_model,
                                              op.products_name,  
                                              sum(op.products_quantity) as sum_qty,
                                              opa.products_options,
                                              opa.products_options_values
                                      from orders_products  op
                                         left join products  p ON op.products_id = p.products_id
                                         left join suppliers s on p.suppliers_id = s.suppliers_id
                                         left join orders  o ON op.orders_id = o.orders_id
                                         left join orders_products_attributes opa ON op.orders_products_id = opa.orders_products_id
                                      where o.date_purchased between '". $_GET['bDS']."' and '".$_GET['bED']."'
                                  and s.suppliers_id = '" . (int)$_GET['bID'] . "'
                                  and o.orders_status = '" .(int) $_GET['bOS'] ."'
                                  group by op.products_name, 
                                           opa.products_options, 
                                           opa.products_options_values
                                  order by o.customers_name
                          "; 
    break;
    }
  }  

 require('includes/header.php');
?>


  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
<!-- body_text //-->
<?php
  if ($action == 'edit') {
?>
<!-- //################################################################################################-->
<!-- //                                      Page des fournisseurs	                                   -->
<!-- //################################################################################################ -->
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/suppliers.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_PRODUCTS_BY_SUPPLIERS; ?></td>
            <td align="left">
              <?php echo  ENTRY_START_DATE;  ?> <br />
              <?php echo '<br />'.PERIOD . $_GET['bDS']; ?>
            </td>
            <td>
              <?php echo ENTRY_TO_DATE; ?> <br />   
              <?php echo '<br />au ' . $_GET['bED']; ?>
           </td>
            <td align="center">
              <?php echo ENTRY_STATUS .'<br /> <br />' .  $status['orders_status_name']; ?>
            </td>  
            <td class="smallText" align="right">
<?php 
              echo osc_draw_form('print_pdf', 'stats_suppliers_orders.php', '&bID=' . (int)$_GET['bID'] . '&bDS=' .$_GET['bDS'] .'&bED=' .osc_replace_string($_GET['bED']  . '&bOS=' . (int)$_GET['bOS']));
              echo osc_image_submit('button_print.gif', IMAGE_UPDATE); 
?>
             </form>
             <?php echo osc_draw_form('cancel', 'stats_suppliers.php');  ?>
             <?php echo osc_image_submit('button_back.gif', IMAGE_UPDATE); ?>
             <?php echo osc_hide_session_id(); ?>
             </form>
           </td>
         </tr>
       </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MODEL; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_QUANTITY; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SUPPLIERS_ID; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SUPPLIERS_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_OPTIONS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_OPTIONS_VALUES; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php

    $suppliers_model_query = osc_db_query($suppliers_query_model_raw);

    while ($suppliers_model = osc_db_fetch_array($suppliers_model_query)) {

      $suppliers_model_query1 = osc_db_query("select  distinct p.products_model,
                                                              sum(op.products_quantity) as sum_qty,	  
                                                              s.suppliers_id, 
                                                              s.suppliers_name,
                                                              op.products_name,
                                                              p.products_id, 
                                                              opa.products_options, 
                                                              opa.products_options_values   
                                              from orders_products  op
                                                 left join products  p ON op.products_id = p.products_id
                                                 left join suppliers s on p.suppliers_id = s.suppliers_id
                                                 left join orders  o ON op.orders_id = o.orders_id
                                                 left join orders_products_attributes opa ON op.orders_products_id = opa.orders_products_id
                                              where o.date_purchased between '".$date_scheduled."' and '".$expires_date."'
                                              and s.suppliers_id = '" . (int)$_GET['bID'] . "'
                                              and o.orders_status = '" .(int)$_GET['bOS'] ."'  	
                                              group by op.products_name, 
                                                       opa.products_options, 
                                                       opa.products_options_values
                                              order by p.products_model, 
                                                       op.products_name
                                            ");
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)">
                <td class="dataTableContent" align="left"><?php echo  $suppliers_model['products_model']; ?></td>
                <td class="dataTableContent" align="left"><?php echo  $suppliers_model['sum_qty']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers_model['suppliers_id']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers_model['suppliers_name']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $suppliers_model['products_name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $suppliers_model['products_options']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $suppliers_model['products_options_values']; ?></td>
                <td class="dataTableContent" align="right">
<?php
                   echo '<a href="' . osc_href_link('categories.php', 'search=' .  osc_replace_string($suppliers_model['products_name'])) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', ICON_EDIT_PRODUCTS) . '</a>';
                   echo osc_draw_separator('pixel_trans.gif', '6', '16');
                   echo osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO); 
?>
                </td>
              </tr>
<?php
  } 
?>
              <tr>
                <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php //echo $suppliers_split->display_count($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SUPPLIERS); ?></td>
                    <td class="smallText" align="right"><?php //echo $suppliers_split->display_links($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<?php
  } elseif ($action == 'customer'){
?>
<!-- //################################################################################################-->
<!-- //                                      Clients	                                       -->
<!-- //################################################################################################ -->
   <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/suppliers.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE_PRODUCTS_BY_CUSTOMERS; ?></td>
            <td align="left">
              <?php echo  ENTRY_START_DATE;  ?> <br />
              <?php echo '<br />'. PERIOD . $_GET['bDS']; ?>
            </td>
            <td>
              <?php echo ENTRY_TO_DATE; ?> <br />
              <?php echo '<br />au ' . $_GET['bED']; ?>
            </td>
            <td align="center"><?php echo ENTRY_STATUS .'<br /> <br />' .  $status['orders_status_name']; ?></td>
            <td class="smallText" align="right">
<?php
              echo osc_draw_form('print_pdf', 'stats_suppliers_customers.php', '&bID=' . (int)$_GET['bID'] . '&bDS=' .$_GET['bDS'] .'&bED=' .osc_replace_string($_GET['bED'] . '&bOS=' .  (int)$_GET['bOS']));
              echo osc_image_submit('button_print.gif', IMAGE_UPDATE);
?>
              </form>
              <?php echo osc_draw_form('cancel', 'stats_suppliers.php');  ?>
              <?php echo osc_image_submit('button_back.gif', IMAGE_UPDATE); ?>
              <?php echo osc_hide_session_id(); ?>
              </form>
            </td>
           </tr>
         </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SUPPLIERS_ID; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SUPPLIERS_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_QUANTITY; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS_id; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS_MANE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MODEL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_OPTIONS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_OPTIONS_VALUES; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php
    $suppliers_customer_query = osc_db_query($customers_query_raw);

    while ($suppliers_customer = osc_db_fetch_array($suppliers_customer_query)) {


      $QsuppliersCustomer = $OSCOM_PDO->prepare('select distinct o.customers_id,
                                                                o.customers_name,
                                                                s.suppliers_id,
                                                                s.suppliers_name,
                                                                p.products_model,
                                                                op.products_name,
                                                                sum(op.products_quantity) as sum_qty,
                                                                opa.products_options,
                                                                opa.products_options_values
                                                from :table_orders_products  op
                                                   left join :table_products  p ON op.products_id = p.products_id
                                                   left join :table_suppliers s on p.suppliers_id = s.suppliers_id
                                                   left join :table_orders  o ON op.orders_id = o.orders_id
                                                   left join :table_orders_products_attributes opa ON op.orders_products_id = opa.orders_products_id
                                                where o.date_purchased between :date_scheduled and :expires_date
                                                and s.suppliers_id = :suppliers_id
                                                and o.orders_status = :orders_status
                                                group by o.customers_name
                                                order by o.customers_name
                                            ');
      $QsuppliersCustomer->bindValue(':date_scheduled',  $date_scheduled);
      $QsuppliersCustomer->bindValue(':expires_date',  $expires_date);
      $QsuppliersCustomer->bindValue(':orders_status',  (int)$_GET['bOS']);
      $QsuppliersCustomer->bindInt(':suppliers_id',   (int)$_GET['bID']);

      $QsuppliersCustomer->execute();

      $suppliers_customer = $QsuppliersCustomer->fetch();

 ?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)">
                <td class="dataTableContent"><?php echo $suppliers_customer['suppliers_id']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers_customer['suppliers_name']; ?></td>
                <td class="dataTableContent"><?php echo $suppliers_customer['sum_qty']; ?></td>
                <td class="dataTableContent" align="left"><?php echo  $suppliers_customer['customers_id']; ?></td>
                <td class="dataTableContent" align="left"><?php echo  $suppliers_customer['customers_name']; ?></td>
                <td class="dataTableContent" align="left"><?php echo  $suppliers_customer['products_model']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $suppliers_customer['products_name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $suppliers_customer['products_options']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $suppliers_customer['products_options_values']; ?></td>
                <td class="dataTableContent" align="right">
<?php
                   echo '<a href="' . osc_href_link('orders.php', 'cID=' . $suppliers_customer['customers_id']) . '">' . osc_image(DIR_WS_ICONS . 'order.gif', ICON_EDIT_ORDERS) . '</a>';
                   echo osc_draw_separator('pixel_trans.gif', '6', '16');
                   echo osc_draw_separator('pixel_trans.gif', '6', '16');
                   echo osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO);
?>
                </td>

              </tr>
<?php
  }
?>
              <tr>
                <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php //echo $suppliers_split->display_count($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SUPPLIERS); ?></td>
                    <td class="smallText" align="right"><?php //echo $suppliers_split->display_links($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
           </tr>
         </table></td>
       </tr>
    </table></td>
  </tr>
<?php
  } else {
?>
<!-- //################################################################################################-->
<!-- //                                      Page Principale	                                       -->
<!-- //################################################################################################ -->
      <tr>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
              <tr>
                <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/suppliers.gif', HEADING_TITLE, '40', '40'); ?></td>
                <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
                <td align="left">
<?php
  echo osc_draw_form('date_range', 'stats_suppliers.php', 'post');
  echo  ENTRY_START_DATE;
?>
                  <br />
<?php echo osc_draw_input_field('date_scheduled', $sInfo->date_scheduled, 'id="schdate"'); ?>
                </td>
              <td>
<?php echo ENTRY_TO_DATE; ?> <br />
<?php echo osc_draw_input_field('expires_date', $sInfo->expires_date, 'id="expdate"'); ?>
               </td>
               <td class="main" align="center" valign="top"><?php echo ENTRY_STATUS; ?><br /><?php echo osc_draw_pull_down_menu('orders_status', $orders_statuses, $order->info['orders_status']); ?></td>
               <td align="right"><?php echo osc_image_submit('button_reset.gif', IMAGE_UPDATE); ?>
                 <?php echo osc_hide_session_id(); ?>
               </td></form>
             </tr>
           </table></td>
         </tr>
         <tr>
           <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
         </tr>
         <tr>
           <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
             <tr>
               <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
                 <tr class="dataTableHeadingRow">
                   <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SUPPLIERS_ID; ?></td>
                   <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SUPPLIERS; ?></td>
                   <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MANAGER; ?></td>
                   <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PHONE; ?></td>
                   <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_EMAIL; ?></td>
                   <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
                   <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?></td>
                 </tr>
<?php
    $suppliers_query_raw = "select distinct s.suppliers_id, 
                                           s.suppliers_name,
                                           s.suppliers_manager,
                                           s.suppliers_phone,
                                           s.suppliers_email_address,
                                            sum(op.products_quantity) as sum_qty
                            from orders_products  op
                              left join products  p ON op.products_id = p.products_id
                              left join suppliers s on p.suppliers_id = s.suppliers_id
                              left join orders  o ON op.orders_id = o.orders_id
                            where o.date_purchased between '" . $date_scheduled . "'
                            and '" . $expires_date . "'
                            and o.orders_status = '" . $_POST['orders_status'] ."' 
                            and s.suppliers_id is not null 
                            and s.suppliers_id <> ''
                            group by  s.suppliers_id
                          ";

    $suppliers_query = osc_db_query($suppliers_query_raw);
    $suppliers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $suppliers_query_raw, $suppliers_query_numrows);

    while ($suppliers = osc_db_fetch_array($suppliers_query)) {

      $Qsuppliers = $OSCOM_PDO->prepare('select distinct s.suppliers_id,
                                                        s.suppliers_name,
                                                        s.suppliers_manager,
                                                        s.suppliers_phone,
                                                        s.suppliers_email_address,
                                                        sum(op.products_quantity) as sum_qty
                                        from :table_orders_products  op
                                           left join :table_products  p on op.products_id = p.products_id
                                           left join :table_suppliers s on p.suppliers_id = s.suppliers_id
                                           left join :table_orders  o on op.orders_id = o.orders_id
                                        where o.date_purchased between :date_scheduled  and :expires_date
                                        and o.orders_status = :orders_status
                                        and s.suppliers_id is not null
                                        and s.suppliers_id <> :suppliers_id
                                        group by  s.suppliers_id
                                        order by  s.suppliers_name
                                        ');
      $Qsuppliers->bindValue(':date_scheduled',  $date_scheduled);
      $Qsuppliers->bindValue(':expires_date',  $expires_date);
      $Qsuppliers->bindValue(':orders_status',  $_POST['orders_status']);
      $Qsuppliers->bindValue(':suppliers_id', '');

      $Qsuppliers->execute();

      $suppliers = $Qsuppliers->fetch();

      $suppliers_qty = $suppliers['sum_qty'];
?> 
                 <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)">
                   <td class="dataTableContent"><?php echo $suppliers['suppliers_id']; ?></td>
                   <td class="dataTableContent" align="left"><?php echo $suppliers['suppliers_name']; ?></td>
                   <td class="dataTableContent" align="left"><?php echo $suppliers['suppliers_manager']; ?></td>
                   <td class="dataTableContent" align="center"><?php echo $suppliers['suppliers_phone']; ?></td>
                   <td class="dataTableContent" align="left"><?php echo $suppliers['suppliers_email_address']; ?></td>
                   <td class="dataTableContent" align="center"><?php echo $suppliers_qty; ?></td>
                   <td class="dataTableContent" align="right">
<?php
      echo '<a href="mailto:'. $suppliers['suppliers_email_address'] .'">' . osc_image(DIR_WS_ICONS . 'email.gif', IMAGE_EMAIL) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('stats_suppliers.php', 'page=' . $_GET['page'] . '&bID=' . (int)$suppliers['suppliers_id'] . '&bDS=' .osc_replace_string($date_scheduled) .'&bED=' .osc_replace_string($expires_date) . '&bOS=' .  $_POST['orders_status'] .'&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_STATS_SUPPLIERS_EDIT) . '</a>';

      echo osc_draw_separator('pixel_trans.gif', '6', '16');

      echo '<a href="' . osc_href_link('stats_suppliers_orders.php', 'page=' . $_GET['page'] . '&bID=' . (int)$suppliers['suppliers_id'] . '&bDS=' .osc_replace_string($date_scheduled) .'&bED=' .osc_replace_string($expires_date)).'&bOS=' .  $_POST['orders_status'] .'" target="_blank">' . osc_image(DIR_WS_ICONS . 'invoice.gif', IMAGE_STATS_SUPPLIERS_PDF) . '</a>';

      echo osc_draw_separator('pixel_trans.gif', '6', '16');

      echo '<a href="' . osc_href_link('stats_suppliers.php', 'page=' . $_GET['page'] . '&bID=' . (int)$suppliers['suppliers_id'] . '&bDS=' .osc_replace_string($date_scheduled) .'&bED=' .osc_replace_string($expires_date) .'&bOS=' .  $_POST['orders_status'] .'&action=customer') . '">' . osc_image(DIR_WS_ICONS . 'client_b2c.gif', ICON_STATS_SUPPLIERS_EDIT_CUSTOMERS) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO);  
?>
                   </td>
                 </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="smallText" valign="top"><?php //echo $suppliers_split->display_count($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SUPPLIERS); ?></td>
                        <td class="smallText" align="right"><?php echo $suppliers_split->display_links($suppliers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
<?php
  }
?>
        </table></td>
      </tr>
    </table></td>

<!-- body_eof //-->

<!-- footer //-->
<?php require('includes/footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php 
  require('includes/application_bottom.php');