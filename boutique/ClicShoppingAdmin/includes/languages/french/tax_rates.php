<?php
/*
 * tax_rates.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Taux fiscaux');

define('TABLE_HEADING_TAX_RATE_PRIORITY', 'Priorité');
define('TABLE_HEADING_TAX_CLASS_TITLE', 'Classe fiscale');
define('TABLE_HEADING_COUNTRIES_NAME', 'Pays');
define('TABLE_HEADING_ZONE', 'Zone');
define('TABLE_HEADING_TAX_RATE', 'Taux fiscaux');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_TAX_DESCRIPTION','Description');
define('TABLE_HEADING_CODE_TAX_ODOO','Code taxe Odoo');

define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_INFO_CLASS_TITLE', 'Titre du taux fiscal :');
define('TEXT_INFO_COUNTRY_NAME', 'Pays :');
define('TEXT_INFO_ZONE_NAME', 'Zone :');
define('TEXT_INFO_TAX_RATE', 'Taux fiscal (%) :');
define('TEXT_INFO_TAX_RATE_PRIORITY', 'Des taux fiscaux ayant la même priorité sont ajoutés, d\'autres sont combinés.<br /><br />Priorité :');
define('TEXT_INFO_RATE_DESCRIPTION', 'Description :');
define('TEXT_INFO_INSERT_INTRO', 'Merci d\'entrer le nouveau taux fiscal avec ses données liées');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer ce taux fiscal ?');
define('TEXT_INFO_HEADING_NEW_TAX_RATE', 'Nouveau taux fiscal');
define('TEXT_INFO_HEADING_EDIT_TAX_RATE', 'Editer taux fiscal');
define('TEXT_INFO_HEADING_DELETE_TAX_RATE', 'Supprimer taux fiscal');
define('TEXT_INFO_TAX_ODOO','Code de Taxe Odoo<br />Note : Veuillez consulter Odoo dans la section Comptabilité/taxes/taxes');
?>