<?php
/**
* Title Name of the submit
*
* @param string  $pages_id, $language_id
* @return string product['products_head_title_tag'], description name
* @access public
*/



  function osc_get_page_manager_head_title_tag($pages_id, $language_id) {
    global $OSCOM_PDO;

    if ($language_id == 0) $language_id = $_SESSION['languages_id'];

    $QpageManager = $OSCOM_PDO->prepare('select page_manager_head_title_tag
                                        from :table_pages_manager_description
                                        where pages_id = :pages_id
                                        and language_id = :language_id
                                       ');
    $QpageManager->bindInt(':pages_id', (int)$pages_id);
    $QpageManager->bindInt(':language_id', (int)$language_id);

    $QpageManager->execute();

    return $QpageManager->value('page_manager_head_title_tag');
  }

/**
* Description Name
*
* @param string  $pages_id, $language_id
* @return string $page_manager['products_head_desc_tag'], description name
* @access public
*/
  function osc_get_page_manager_head_desc_tag($pages_id, $language_id) {
    global $OSCOM_PDO;

    if ($language_id == 0) $language_id = $_SESSION['languages_id'];

    $QpageManager = $OSCOM_PDO->prepare('select page_manager_head_desc_tag
                                          from :table_pages_manager_description
                                          where pages_id = :pages_id
                                          and language_id = :language_id
                                         ');
    $QpageManager->bindInt(':pages_id', (int)$pages_id);
    $QpageManager->bindInt(':language_id', (int)$language_id);

    $QpageManager->execute();

    return $QpageManager->value('page_manager_head_desc_tag');
  }

/**
* keywords Name
*
* @param string  $pages_id, $language_id
* @return string $page_manager['products_head_keywords_tag'], keywords name
* @access public
*/
  function osc_get_page_manager_head_keywords_tag($pages_id, $language_id) {
    global $OSCOM_PDO;

    if ($language_id == 0) $language_id = $_SESSION['languages_id'];

    $QpageManager = $OSCOM_PDO->prepare('select page_manager_head_keywords_tag
                                          from :table_pages_manager_description
                                          where pages_id = :pages_id
                                          and language_id = :language_id
                                         ');
    $QpageManager->bindInt(':pages_id', (int)$pages_id);
    $QpageManager->bindInt(':language_id', (int)$language_id);

    $QpageManager->execute();

    return $QpageManager->value('page_manager_head_keywords_tag');
  }
