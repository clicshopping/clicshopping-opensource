<?php
  /**
   * cfm_modules_shopping_cart.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_shopping_cart {
    var $code = 'modules_shopping_cart';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_SHOPPING_CART_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_shopping_cart() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_shopping_cart/';
      $this->title = MODULE_CFG_MODULE_SHOPPING_CART_TITLE;
    }
  }
?>
