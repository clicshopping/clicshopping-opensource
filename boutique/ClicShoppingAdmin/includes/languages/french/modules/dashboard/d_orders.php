<?php
/**
 * d_orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_ORDERS_TITLE', 'Commandes');
define('MODULE_ADMIN_DASHBOARD_ORDERS_DESCRIPTION', 'Voir les dernières commandes');
define('MODULE_ADMIN_DASHBOARD_ORDERS_TOTAL', 'Total');
define('MODULE_ADMIN_DASHBOARD_ORDERS_DATE', 'Date');
define('MODULE_ADMIN_DASHBOARD_ORDERS_ORDER_STATUS', 'Statut');
define('MODULE_ADMIN_DASHBOARD_ORDERS_ORDER_ACTION','Actions');
define('MODULE_ADMIN_DASHBOARD_ORDERS_ODOO_STATUS','Odoo');
?>
