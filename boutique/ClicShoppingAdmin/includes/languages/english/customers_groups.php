<?php
/**
 * customers_group.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Groups');
define('HEADING_TITLE_EDIT', 'Edition of the Customers Group');
define('HEADING_TITLE_NEW', 'New Customers Group');

if (B2B == 'true'){
define('HEADING_TITLE_CATEGORIE', 'Category Markup');
define('ENTRY_CATEGORIES_DISCOUNT', 'Category Markup:');
define('TITLE_DEFAULT_DISCOUNT', 'Markup by default on the products');
define('TITLE_CATEGORIES_DISCOUNT', 'Markup by default on the category');
} else {
define('HEADING_TITLE_CATEGORIE', 'Category Discount');
define('ENTRY_CATEGORIES_DISCOUNT', 'Category Discount:');
define('TITLE_DEFAULT_DISCOUNT', 'Discount by default on the products');
define('TITLE_CATEGORIES_DISCOUNT', 'Discount by default on the category');
}

define('HEADING_TITLE_SEARCH', 'Find');

define('TABLE_HEADING_NAME', 'Name');

if (B2B == 'true'){
  define('TABLE_HEADING_DISCOUNT', 'Default Markup');
  define('TABLE_HEADING_CATEGORIES_DISCOUNT', 'Markup on category');
} else {
  define('TABLE_HEADING_DISCOUNT', 'Default Discount');
  define('TABLE_HEADING_CATEGORIES_DISCOUNT', 'Discount on category');
}

define('TABLE_HEADING_QUANTITY_DEFAULT','Order quantity group');
define('TABLE_HEADING_COLOR', 'color Code ');
define('TABLE_HEADING_ACTION', 'Action');
define('ENTRY_GROUP_CATEGORIES', 'Categories :');
define('SELECT_CATEGORY', '1- Select Categories');

define('TEXT_CATEGORIES', 'Categories');
define('TEXT_CATEGORIES_NO_NEW', 'After recording of this new customers group you will have the possibility of personalizing different handing-over on the categories of your choice');
define('TEXT_PRODUCTS_UPDATED', '');
define('TEXT_QTY_UPDATED', 'price(s) updated concerning the group');

define('TITLE_DEFAULT_QUANTITY','Order quantity minimal of this group');
define('ENTRY_TEXT_QUANTITY_DEFAULT','Order quantity minimal of this group');
define('ENTRY_TEXT_QUANTITY_NOTE', '&nbsp;<span class="errorText">* See note</span>');

define('ENTRY_GROUPS_NAME_ERROR', 'Becarefull all fields are required');
define('ENTRY_GROUPS_NAME_ERROR_ZERO', 'Becarefull % field is required and must be diffenet than zero');
define('ENTRY_GROUPS_CATEGORIE_ERROR', 'No recording ! because you did not select a category');

define('TEXT_DELETE_INTRO', 'Are you sure to delete this Group?');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Delete Group');
define('TEXT_IMPOSSIBLE_DELETE', 'you cannot delete:');
define('TYPE_BELOW', 'Type');
define('PLEASE_SELECT', 'Select');
define('ENTRY_COLOR_BAR', 'Code color:');

define('TITLE_GROUP_TAX', 'View of the products');
define('ENTRY_GROUP_TAX', 'View the taxe (price) :');
define('TEXT_GROUP_TAX_INC', 'Inc.');
define('TEXT_GROUP_TAX_EX', 'Ex.');
define('ENTRY_GROUP_TAX_NOTE', '&nbsp;<span class="errorText">* Please read the note below</span>');

define('TITLE_GROUP_NAME', 'Name and code color of the customers group');
define('ENTRY_VALEUR_DISCOUNT', 'Value <i>(%)</i> :');
define('ENTRY_VALEUR_DISCOUNT_NOTE', '&nbsp;<span class="errorText">* Please not add the signs according to : + - or %</span>');

define('TITLE_ORDER_CUSTOMER_DEFAULT', 'Mode of invoicing by default');
define('TITLE_GROUP_PAIEMENT_DEFAULT', 'Payment to be authorized by default');
define('TITLE_GROUP_SHIPPING_DEFAULT', 'Shipping to be authorized by default');
define('TITLE_LIST_CATEGORIES_DISCOUNT', 'List categories');

define('AIDE_TITLE_ONGLET_GENERAL', 'Note on the management of groups B2B');
define('AIDE_GROUP_TAX', '<li>While selecting " <i>Ex. the tax</i> " billing in the tab, select the mode display products prices, this position automatically Excluding taxes registration.</li>
<li>If the minimum order quantity is equal to 0, it will be the default amount will be generally taken into account. The minimum product does not apply in the B2B mode</li>');

define('AIDE_TITLE_ONGLET_FACTURATION', 'Note on the management of groups B2B');
define('AIDE_ORDER_TAX', '<li>While selecting " <i>Ex. the tax</i> ", 	
mode selection display price products in the General tab this position automatically Excluding taxes registration. </li>');
?>