<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
	define('MODULE_PAYMENT_COD_TEXT_TITLE', 'Paiement a la livraison');
	define('MODULE_PAYMENT_COD_TEXT_DESCRIPTION', 'Paiement à la livraison<br /><br /><font color="#FF0000"><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut');
        define('TEXT_INFO_ONLINE_STATUS','Plus d\'informations');
	define('TEXT_INFO_VERSION','Version : ');
?>
