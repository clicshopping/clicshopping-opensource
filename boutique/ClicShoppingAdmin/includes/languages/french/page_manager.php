<?php
/**
 * page_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version 
*/

// Titre principal
define('HEADING_TITLE', 'Page d\'accueil et informations');
define('HEADING_TITLE_EDITION', 'Edition page');
define('HEADING_TITLE_NEW', 'Nouvelle page');
define('TABLE_HEADING_PAGE_TYPE','Type boxe');

// Texte tableau de la liste des pages
define('TABLE_HEADING_PAGES', 'Titre de la page');
define('TABLE_HEADING_TYPE_PAGE', 'Type de page');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_SORT_ORDER', 'Ordre de tri');
define('TABLE_HEADING_ACTION', 'Actions');
define('TEXT_DISPLAY_NUMBER_OF_PAGES', 'Affichage <strong>%d</strong> à <strong>%d</strong> (de <strong>%d</strong> Pages)');
define('TABLE_HEADING_LINKS_TARGET','Type de liens');
define('TABLE_HEADING_CUSTOMERS_GROUP','Groupes client');

// Menu des onglets
define('TAB_GENERAL', 'Général');
define('TAB_PAGE_LINK', 'Liens de la page');
define('TAB_PAGE_DESCRIPTION', 'Description');
define('TAB_PAGE_META_TAG', 'Méta Tag');

// Nouvelle page
define('TITLE_PAGES_CHOOSE', 'Type de page');
define('TEXT_PAGES_CHOOSE', 'Veuillez sélectionner le type de page que vous souhaitez insérer :');

// Nom de la page
define('TITLE_NAME_PAGE', 'Nom de la page');

// Date
define('TITLE_PAGES_DATE', 'Date');
define('TEXT_PAGES_DATE_START', 'Planifié le :');
define('TEXT_PAGES_DATE_CLOSED', 'Expire le :');
define('TEXT_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_LAST_MODIFIED',  'Dernière modification :');
define('TEXT_STATUS_CHANGE', 'Changement du statut :');
define('TEXT_PAGE_MANAGER_CUSTOMERS_GROUP','Groupe de client pour affichage : ');
define('TEXT_START_DATE','Date début : ');
define('TEXT_CLOSE_DATE','Date d\'expiration : ');
define('TEXT_ALL_CUSTOMERS','Clients Normaux');
define('NORMAL_CUSTOMER','Clients Normaux');

// Type de la page
define('TITLE_PAGES_TYPE', 'Type de la page' );
define('TEXT_PAGES_BOX','Choix boxe d\'affichage : ');
define('TEXT_PAGES_TYPE', 'Type de page :');
define('PAGE_MANAGER_INTRODUCTION_PAGE','Introduction');
define('PAGE_MANAGER_MAIN_PAGE', 'Accueil');
define('PAGE_MANAGER_CONTACT_US', 'Nous contacter');
define('PAGE_MANAGER_INFORMATIONS', 'Informations');
define('PAGE_MANAGER_MAIN_BOX','Boxe principale');
define('PAGE_MANAGER_SECONDARY_BOX','Boxe secondaire');
define('PAGE_MANAGER_NO_APPEAR','Ne pas afficher dans les boxes');


// Conditions generales
define('TEXT_PAGES_GENERAL_CONDITIONS','Est une page Conditions Générales de Vente ?');
define('PAGE_MANAGER_TEXT_YES','Oui');
define('PAGE_MANAGER_TEXT_NO','Non');

// Divers
define('TITLE_DIVERS', 'Divers');
define('TEXT_PAGES_TIME', 'Temps d\'affichage de la page d\'introduction :');
define('TEXT_PAGES_TIME_SECONDE', '<i>Secondes</i>');
define('TEXT_PAGES_SORT_ORDER', 'Ordre de tri :');

// Lien sur la page
define('TITLE_LINK', 'Type de lien sur la page');
define('TEXT_PAGES_INTEXT', 'Type de lien :');
define('TEXT_LINKS_SAME_WINDOWS','Même fenêtre');
define('TEXT_LINKS_NEW_WINDOWS','Nouvelle fenêtre');
define('TEXT_TARGET_INTERNAL', 'Interne');
define('TEXT_TARGET_EXTERNAL', 'Externe');
define('TEXT_PAGES_EXTERNAL_LINK', 'URL Externe de la page :');
define('TEXT_TARGET', 'Affichage cible de la page :');
define('TEXT_TARGET_SAMEWINDOW', 'Meme onglet');
define('TEXT_TARGET_NEWWINDOW', 'Nouvel onglet');

// Description
define('TEXT_PAGES_INFORMATION_DESCRIPTION', 'Description de la page');

// Boxe information
define('TEXT_INFO_DELETE_INTRO', '&Euml;tes-vous certain que vous voulez supprimer cette page ?');

//seo
define('KEYWORDS_GOOGLE_TREND','Tendance des mots clefs selon Google');
define('ANALYSIS_GOOGLE_TOOL','Outil d\'analyse selon Google');

define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Métadonnées');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Titre');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_KEYWORDS', 'Mots clefs');
define('TEXT_PRODUCTS_TAG_PRODUCT','tags Produit');
define('TEXT_PRODUCTS_TAG_BLOG','Blog tag');

define('TITLE_HELP_SUBMIT', 'Notes sur la gestion des métadonnées');
define('HELP_SUBMIT', '<li>Le système gère automatiquement la gestion des métadonnées.<br />
<blockquote>Il inscrit automatiquement les éléments dans l\'ordre suivant si les champs ne sont pas renseignées:<br />
Titre : le nom de votre article (ex blouson cuir)<br />
Description : le nom de l\'article (blouson cuir), le nom du article découpé (blouson, cuir), le nom de la catégorie,)<br />
Keywords : le nom du l\'article (blouson cuir), le nom du l\'article découpé (blouson, cuir), le nom de la catégorie<br />
categorie : la catégorie de l\'article<br />
</blockquote></li>
<li>Si vous renseignez les champs, les balises apparaitront de la facon suivante : <br />
<blockquote>
Titre : le titre du champs, le nom de votre article (ex blouson cuir)<br />
Description : le champs description, le nom de l\'article (blouson cuir), le nom du article découpé (blouson, cuir), le nom de la catégorie<br />
Mots clefs : le champs mots clefs, le nom de l\'article (blouson cuir), le nom de l\'article découpé (blouson, cuir), le nom de la catégorie<br />
</blockquote></li>
<li><strong>Veuillez à ne pas mettre trop de mots clefs (maximum 10)</strong> et établissez une cohérence entre chaque champs afin de retourver les mots les plus importants en premier</li>
');

// Aide
define('TITLE_AIDE_IMAGE', 'Aide');
define('TITLE_AIDE_PAGE_MANAGER', 'Note sur la création des pages d\'introduction');
define('TEXT_PAGES_TYPE_INFORMATION', '<li>Concernant la page d\'introduction, nous vous suggérons de la laisser afficher au <u>minimum 10 secondes</u>. Si vous souhaitez un temps illimité, laisser le champs vide');
define('TITLE_HELP_DESCRIPTION', 'Note sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

// Message flash
define('SUCCESS_PAGE_INSERTED', 'Succès : La page a été insérée.');
define('SUCCESS_PAGE_UPDATED', 'Succès : La page a été mise à jour.');
define('SUCCESS_PAGE_REMOVED', 'Succès : La page a été supprimée.');
define('SUCCESS_PAGE_MANAGER_STATUS_UPDATED', 'Succès : Le statut de la page a été mis à jour.');
define('ERROR_PAGE_TITLE_REQUIRED', 'Erreur : Le titre de page est requis.');
define('ERROR_PAGE_TITLE_REQUIRED_CURSEUR', '<span class="errorText">Veuillez informer le titre de la page.</span>');

// TAB des boutons
define('IMAGE_NEW_PAGE', 'Nouvelle page');
define('ICON_EDIT', 'Editer la page');
?>