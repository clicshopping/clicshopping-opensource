<?php
  chdir('../../');
  require('includes/application_top.php');
// Demarrage de la bufferisation de sortie
    ob_start();


// Redirection vers la page d'acceuil si aucune page d'introduction existe
    osc_redirect(osc_href_link('index.php', '', 'NONSSL'));

// Afficher le contenu du buffer
    ob_end_flush();

