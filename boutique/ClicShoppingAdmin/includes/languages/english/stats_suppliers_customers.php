<?php
/**
 * stats_suppliers_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('PRINT_INVOICE_HEADING', 'Supplier report');
define('PRINT_SUPPLIERS_TITLE', 'Supplier orders analyse ');
define('PRINT_SUPPLIERS', 'Supplier');

//Customer information
define('ENTRY_EMAIL','E-mail : ');
define('ENTRY_MANAGER','Manager : ');
define('ENTRY_PHONE','Telephone : ');
define('ENTRY_FAX','Fax : ');
define('ENTRY_SHIP_TO', 'Address :');
define('ENTRY_SUPPLIER_INFORMATION','Supplier informations : ');
define('ENTRY_SUPPLIERS_NUMBER','Supplier model : ');

define('START_ANALYSE','Start analyse : ');
define('END_ANALYSE','End analyse : ');

define('ENTRY_STATUS','Order Status treated');

define('TABLE_HEADING_QTE', 'Qty');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Product');
define('TABLE_HEADING_OPTIONS','Option');
define('TABLE_HEADING_VALUE','Value');
define('TABLE_HEADING_CUSTOMERS_ID','Id client');
define('TABLE_HEADING_CUSTOMERS_NAME','Customer name');

define('ENTRY_HTTP_SITE','Internet site : ');
define('PRINT_INVOICE_URL', HTTP_CATALOG_SERVER );


define('THANK_YOU_CUSTOMER', '');
define('RESERVE_PROPRIETE', '');
define('RESERVE_PROPRIETE_NEXT', '');
define('RESERVE_PROPRIETE_NEXT1', '');
define('RESERVE_PROPRIETE_NEXT2', '');

// gestion de la double taxe ou non 
if (DISPLAY_DOUBLE_TAXE == 'false') {
  define('ENTRY_INFO_SOCIETE', ' ');
  define('ENTRY_INFO_SOCIETE_NEXT', '');
} else {
  define('ENTRY_INFO_SOCIETE', '');
  define('ENTRY_INFO_SOCIETE_NEXT', '');
}

?>