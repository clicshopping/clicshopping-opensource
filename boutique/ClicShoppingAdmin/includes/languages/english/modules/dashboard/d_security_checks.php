<?php
/**
 * d_security_checkr.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_TITLE', 'Security Checks');
define('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_DESCRIPTION', 'Run security checks');
define('MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_SUCCESS', 'This is a properly configured installation of osCommerce Online Merchant!');
?>
