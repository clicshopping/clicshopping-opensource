<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Checkout');
define('NAVBAR_TITLE_2', 'Confirmation');

define('HEADING_TITLE', 'Order Confirmation');

define('HEADING_DELIVERY_ADDRESS', 'Delivery Address');
define('HEADING_SHIPPING_INFORMATION','Customer Informations');
define('HEADING_SHIPPING_METHOD', 'Shipping Method');
define('HEADING_PRODUCTS', 'Products');
define('HEADING_TAX', 'Tax');
define('HEADING_TOTAL', 'Total');
define('HEADING_BILLING_INFORMATION', 'Billing Information');
define('HEADING_BILLING_ADDRESS', 'Billing Address');
define('HEADING_PAYMENT_METHOD', 'Payment Method');
define('HEADING_PAYMENT_INFORMATION', 'Payment Information');
define('HEADING_ORDER_COMMENTS', 'Comments About Your Order');

define('TEXT_EDIT', 'Edit');
define('TEXT_CONDITIONS_CONFIRMATION','By validating this check box and pressing the button confirmed, I agree to confirm the order of the ordered products');
define('TEXT_CONDITION_CONFIRMATION_ERROR_AGREEMENT','Error Agreement : Please check the box to continue');

?>
