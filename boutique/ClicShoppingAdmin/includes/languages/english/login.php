<?php
/*
 * login.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

define('HEADING_TITLE', 'Administrator Login');

define('TEXT_USERNAME', 'Username:');
define('TEXT_PASSWORD', 'Password:');
define('TEXT_NAME','Administrator name:');
define('TEXT_FIRSTNAME','Administrator First name:');

define('TEXT_CREATE_FIRST_ADMINISTRATOR', 'No administrators exist in the database table. Please fill in the following information to create the first administrator. (A manual login is still required after this step)');

define('HEADING_TITLE_SENT_PASSWORD', 'Recovery lost password');
define('TEXT_SENT_PASSWORD', 'If you\'ve forgotten your password, enter your e-mail address below and we\'ll send you an e-mail message containing your new password.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND','Error: The E-Mail Address  or pseudo was not found in our records, please try again.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' (Administrator Control Panel) - New Password');
define('EMAIL_PASSWORD_REMINDER_BODY', 'A new password was requested from ' . $_SERVER['REMOTE_ADDR'] . '.' . "\n\n" . 'Your new password to \'' . STORE_NAME . '\' (Administrator Control Panel) is:' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'Success: A new password has been sent to your e-mail address.');
define('BUTTON_SUBMIT', 'Submit');
define('TEXT_NEW_TEXT_PASSWORD', 'New password');
define('TEXT_ADMINISTRATION_PANEL','Administration panel');
define('TEXT_EMAIL_LOST_PASSWORD','Your email');


define('ERROR_INVALID_ADMINISTRATOR', 'Error: Invalid administrator login attempt.');

define('BUTTON_LOGIN', 'Login');
define('BUTTON_CREATE_ADMINISTRATOR', 'Create Administrator');

define('ERROR_ACTION_RECORDER', 'Error: The maximum number of login attempts has been reached. Please try again in %s minutes. An email has been sent to the administrator with your contact information (geolocation). You can try to connect again one time after the administration panel before being permanently blocked');

define('REPORT_ACCESS_LOGIN','Someone try to connect at your admin, below the information. You can also see in you administration panel the details in tool section : ');
define ('REPORT_SENDER_IP_ADRESS','Senders ip address ');
define ('REPORT_SENDER_HOST_NAME','Senders host name : ');
define ('REPORT_SUBJECT_EMAIL','[Report] Admin-Login connexion error');
define ('REPORT_SENDER_USERNAME','Username : ');
?>
