<?php
/*
 * stats_newsletter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

define('HEADING_TITLE', 'Souscription Newsletter avec aucun compte');

define('TABLE_HEADING_FIRST_NAME', 'Prénom');
define('TABLE_HEADING_LAST_NAME', 'Nom');
define('TABLE_HEADING_ADDRESS_EMAIL', 'Adresse email');
define('TABLE_HEADING_DATE_ADDED', 'date ajout');
define('TABLE_HEADING_CLEAR','Supprimer');
define('HEADING_TITLE_SEARCH','Rechercher');
?>