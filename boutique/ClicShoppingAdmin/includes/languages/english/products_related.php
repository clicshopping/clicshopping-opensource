<?php
/**
 * products_related.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Cross sell and products related managemet');
define('HEADING_TITLE_SEARCH','Search');

define('TABLE_HEADING_ID', 'Id');
define('TABLE_HEADING_PRODUCT_FROM','Master products (of)');
define('TABLE_HEADING_PRODUCT_TO', 'Slave products (to)');
define('TABLE_HEADING_STATUS','Status');
define('TABLE_HEADING_CUSTOMERS_GROUP','B2B Mode');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_ORDER','Sort order');
define('TABLE_HEADING_RELATED_PRODUCTS','Related products');
define('TABLE_HEADING_CROSS_SELL_PRODUCTS','Cross sell products');
define('TABLE_HEADING_MODEL','Model');


define('TEXT_CONFIRM_DELETE_ATTRIBUTE','Please, confirm the delete');

define('SHOW_ALL_PRODUCTS','All the products');
define('EDIT','Edit');
define('DELETE','Delete');
define('INHERIT','Hinerit');
define('RECIPROCICATE','Reciprocate');
define('INSERT','Insert');
define('TITLE_AIDE_RELATED_PRODUCTS','Products related and cross sell notes');
define('TEXT_AIDE_RELATED_PRODUCTS_CONTENT', '	
<ul> <li> We advise you to choose a product option is cross sell option or related product option. </ li>
<li> <u> B2B mode </ u> (if you have the option) to display only products for B2B customers groups. Otherwise, the client part of a particular group will not see this product if the box is not checked. This rule applies to all client groups.
<li> <u> Insert mode </ u> enables products to be linked. In this case, you create one relationship between the products (slave \ master in display).
<li> <u> mode Reprocicate </ u> will create a link between the 2 products (master and slave). </ li>
<li> <u> Legacy mode </ u> enables you to create multiple recordings with the same master and slave products. To use this feature, you must have created several products. The display is made only in one direction master - slave (the slave master will appear in). </ Li>
<li> Click to return to update the general page to display all registered products. </ li>
');	   
define('TITLE_PRODUCTS_RELATED_IMAGE','Help on products cross sell or related');
?>
