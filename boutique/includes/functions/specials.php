<?php
/**
 * specials.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: specials.php 
*/
////
// Sets the status of a special product
  function osc_set_specials_status($specials_id, $status) {
    global $OSCOM_PDO;
      
      if ($status == '1') {

        return $OSCOM_PDO->save('specials', ['status' => 1,
                                            'date_status_change' => 'now()',
                                            'scheduled_date' => 'null'
                                            ],
                                            ['specials_id' => (int)$specials_id]
                               );

      } elseif ($status == '0') {

        return $OSCOM_PDO->save('specials', ['status' => 0,
                                            'date_status_change' => 'now()',
                                            'scheduled_date' => 'null',
                                            'flash_discount' => 0
                                            ],
                                            ['specials_id' => (int)$specials_id]
                               );
      } else {
        return -1;
      }
    }

// Auto activate scheduled products on special
  function osc_scheduled_specials() {
    global $OSCOM_PDO;

      $Qspecials = $OSCOM_PDO->query('select specials_id
                                      from :table_specials
                                      where scheduled_date is not null
                                      and scheduled_date <= now()
                                      and status != 1
                                     ');

      $Qspecials->execute;
      $Qspecials->execute;

      if ($Qspecials->fetch() !== false) {
        do {
          static::setSpecialsStatus($Qspecials->valueInt('specials_id'), 1);
        } while($Qspecials->fetch());
      }
    }

// Auto expire products on special
  function osc_expire_specials() {
    global $OSCOM_PDO;

    $Qspecials = $OSCOM_PDO->query('select specials_id
                                    from :table_specials
                                    where status = 1
                                    and expires_date is not null
                                    and now() >= expires_date
                                  ');

    $Qspecials->execute;

    if ($Qspecials->fetch() !== false) {
      do {
        static::setSpecialsStatus($Qspecials->valueInt('specials_id'), 0);
      } while ($Qspecials->fetch());
    }
  }
