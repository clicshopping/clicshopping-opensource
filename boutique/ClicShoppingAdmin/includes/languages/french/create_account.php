<?php
/*
 * create_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Créer un compte client');

define('EMAIL_SUBJECT', 'Création de compte client sur ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Bonjour Mr ' . $_POST['customers_firstname'] . ' ' . $_POST['customers_lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Bonjour Mme ' . $_POST['customers_firstname'] . ' ' . $_POST['customers_lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Bonjour ' . $_POST['customers_firstname'] . ' ' . $_POST['customers_lastname'] . ',' . "\n\n");

define('ENTRY_CUSTOMERS_COUPONS','Souhaitez-vous offrir un coupon à votre client ?');
define('ENTRY_CUSTOMERS_EMAIL',' Souhaitez-vous avertir votre client de la création d\'un compte ?');

// Autorisation de modification pour les clients
define('ENTRY_CUSTOMERS_MODIFY_COMPANY', 'Autoriser le client à modifier ses informations');
define('ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT', 'Autoriser le client à modifier l\'adresse principale');
define('ENTRY_CUSTOMERS_ADD_ADDRESS', 'Autoriser le client à ajouter d\'autres adresses');
define('ENTRY_POST_CODE_TEXT','Requis');
define('ENTRY_CITY_TEXT', 'Requis');

// TVA Intracom
define('TVA_INTRACOM_VERIFY','Vérifier');
define('TITLE_AIDE_CUSTOMERS_TVA','Note sur la TVA intracommunautaire');
define('TITLE_AIDE_TVA_CUSTOMERS','Veuillez valider la fiche avant de vérifier le numéro de TVA Intracommunautaire de la société');

// Social Network
define('EMAIL_NETWORK',''. "\n\n" . 'Suivez nous en temps reel sur :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');
?>
