<?php
  /**
   * cfm_modules_action_recorder.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_action_recorder {
    var $code = 'action_recorder';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_ACTION_RECORDER_INSTALLED';
    var $title;
    var $template_integration = false;

    function cfgm_action_recorder() {
      $this->directory = DIR_FS_CATALOG_MODULES . 'action_recorder/';
      $this->title = MODULE_CFG_MODULE_ACTION_RECORDER_TITLE;
    }
  }
?>
