<?php
/*
  * header.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  if (isset($_SESSION['admin'])) {
    $whos_online_query = osc_db_query("select customer_id,
                                              full_name,
                                              ip_address,
                                              time_entry,
                                              time_last_click,
                                              last_page_url,
                                              session_id
                                       from whos_online
                                     ");

  // Temps des sessions des clients en ligne sur la boutique avant expiration
    $xx_mins_ago = (time() - 900);

  // Supprime dans la base de donnees les clients qui ne sont plus en ligne
  // Note : Cette ligne ne peut s'executer seulement si le fichier application_top.php est appelle- Exemple : require('includes/application_top.php');
  // En regle general le fichier application_top.php est deja appele dans toutes les pages.

    $Qdelete = $OSCOM_PDO->prepare('delete
                                    from :table_whos_online
                                    where  time_last_click <= :time_last_click
                                  ');
    $Qdelete->bindValue(':time_last_click', $xx_mins_ago);
    $Qdelete->execute();

  }


  if ($OSCOM_MessageStack->size > 0) {
    $ClassMessageStack = "messageStackSuccess";
    if ($ClassMessageStackError == 1) {
      $ClassMessageStack = "messageStackError";
    }
  }
?>
<!DOCTYPE html>
<html <?php echo HTML_PARAMS; ?>>
<head>
  <meta charset="<?php echo CHARSET; ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="robots" content="noindex,nofollow" />
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <title><?php echo TITLE; ?></title>
  <base href="<?php echo ($request_type == 'SSL') ? HTTPS_SERVER . DIR_WS_HTTPS_ADMIN : HTTP_SERVER . DIR_WS_ADMIN; ?>" />

  <link rel="icon" type="image/png" href="<?php echo HTTP_SERVER . DIR_WS_CATALOG .'images/logo_clicshopping.png' ?>" />

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="ext/javascript/bootstrap/less/bootstrap.less" />
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

  <link rel="stylesheet" type="text/css" href="includes/stylesheet.css" />
  <link rel="stylesheet" type="text/css" href="ext/javascript/bootstrap/less/datepicker.less" />

  <script type="text/javascript" src="ext/javascript/clicshopping/general.js"></script>

<?php
// ********************************************
// JSCookMenu
// *******************************************

 if ($OSCOM_DetectMobile->isTablet() == true && CONFIGURATION_MENU_NAVIGATION == 'tablet' )  {
   $menu_navigation = 'tablet';
 } elseif (CONFIGURATION_MENU_NAVIGATION == 'computer' || CONFIGURATION_MENU_NAVIGATION == 'tablet' ){
   $menu_navigation = CONFIGURATION_MENU_NAVIGATION;
 } else {
   $menu_navigation = 'both';
 }

  if ($menu_navigation == 'computer' || $menu_navigation == 'both') {
?>
  <script language="javascript" src="ext/javascript/JSCookMenu/JSCookMenu.js"></script>
  <link  rel="stylesheet" href="ext/javascript/JSCookMenu/ThemeOffice2003Noir/theme.css" />
  <script language="javascript" src="ext/javascript/JSCookMenu/ThemeOffice2003Noir/theme.js"></script>

  <link rel="stylesheet" type="text/css" href="includes/stylesheet_responsive.css" />
<?php
  }
?>

  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

</head>
<body onload="SetFocus();">
<div id="container">
  <div id="content">
    <div class="headerFond"><?php echo osc_image(DIR_WS_IMAGES . 'header/logo_clicshopping.png', PROJECT_VERSION, '166', '55'); ?></div>
    <div class="headerLine"></div>
<?php
  if (isset($_SESSION['admin'])) {
?>
    <div class="menuJSCookMenu">
      <span class="pull-left MenuHeaderAdmin">
<?php
         if ($menu_navigation == 'computer' || $menu_navigation == 'both') {
             include ('ext/javascript/JSCookMenu/JSCookMenu_config.php');
         } else {
           echo '<span style="padding-left:5px; padding-top: 2px;"><a href="' . osc_href_link('index.php') . '" class="headerLink">' . MENU_HOME . '</a></span>';
           echo '  / <span style="padding-left:5px; padding-top: 2px;"><a href="' . HTTP_CATALOG_SERVER . DIR_WS_CATALOG .'" target="_blank" class="headerLink">Catalog</a></span>';
         }
?>
      </span>
      <span class="pull-right">
        <div class="InfosHeaderAdmin">
          <span><?php echo osc_image(DIR_WS_IMAGES . 'header/administrateur.gif', TEXT_HEADER_USER_ADMINISTRATOR, '16', '16'); ?></span>
          <span class="menuJSCookTexte"><?php echo (isset($_SESSION['admin'])? '&nbsp;' . osc_user_admin()  .  '&nbsp;-&nbsp;(<a href="' . osc_href_link('login.php', 'action=logoff') . '" class="headerLink">' . TEXT_HEADER_LOGOFF_ADMINISTRATOR . '</a>)' : ''); ?> &nbsp;&nbsp;</span>
          <span><?php echo (isset($_SESSION['admin']) ?  '<a href="' . osc_href_link('whos_online.php') . '">' . osc_image(DIR_WS_IMAGES . '/header/clients.gif', TEXT_HEADER_ICON_NUMBER_OF_CUSTOMERS, '16', '16') . '</a>': ''); ?></span>
          <span class="menuJSCookTexte"><?php  echo (isset($_SESSION['admin']) ? '&nbsp;' . sprintf(TEXT_HEADER_NUMBER_OF_CUSTOMERS, osc_db_num_rows($whos_online_query)) . '&nbsp;&nbsp;' : ''); ?></span>
        </div>
      </span>
    </div>
<?php
  }
?>
    <div class="clearfix"></div>
<?php
  if ($OSCOM_MessageStack->size > 0) {
?>
    <div class="<?php echo $ClassMessageStack; ?>"><?php echo $OSCOM_MessageStack->output(); ?></div>
<?php
  } // end $OSCOM_MessageStack
?>

    <div>
      <noscript>
        <div class="no-script">
          <div class="no-script-inner"><?php echo NO_SCRIPT_TEXT; ?></div>
        </div>
      </noscript>
    </div>
<?php
 if (isset($_SESSION['admin'])) {
   if ($menu_navigation == 'tablet' || $menu_navigation == 'both') {
     include ('ext/javascript/MultiLevelPushMenu/configure_menu.php');
   }
 }
