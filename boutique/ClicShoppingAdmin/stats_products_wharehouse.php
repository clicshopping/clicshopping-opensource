<?php
/**
 * stats_products_wharehouse.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  require('includes/header.php');

  // get list of order status
  $orders_statuses = array();
  $orders_status_array = array();

  $QordersStatus = $OSCOM_PDO->prepare('select orders_status_id,
                                               orders_status_name
                                        from :table_orders_status
                                        where language_id = :language_id
                                        order by orders_status_id
                                       ');

  $QordersStatus->bindInt(':language_id', (int)$_SESSION['languages_id'] );
  $QordersStatus->execute();

    while ($orders_status = $QordersStatus->fetch() ) {
      $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                                 'text' => $orders_status['orders_status_name']);
      $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
    }

    $status = osc_db_prepare_input($_GET['status']);

    if ($status > 0) {
    $products_query_raw = "select op.products_id, 
                                    op.orders_products_id,
                                    op.orders_id,
                                    op.products_model,
                                    op.products_name,
                                    op.products_quantity as customers_order_quantity,
                                    o.orders_id,
                                    o.customers_id,
                                    o.customers_name,
                                    o.customers_telephone,
                                    o.orders_status,
                                    p.products_quantity,
                                    p.products_wharehouse_time_replenishment,
                                    p.products_wharehouse,
                                    p.products_wharehouse_row,
                                    p.products_wharehouse_level_location,
                                    p.products_packaging,
                                    p.products_image
                            from  orders_products op left join products p on (op.products_id = p.products_id),
                                 orders o
                            where  op.orders_id = o.orders_id
                            and o.orders_status = '" . (int)$status ."'
                            order by o.orders_id, o.date_purchased DESC
                          ";
    } else {
    $products_query_raw = "select op.products_id, 
                                    op.orders_products_id,
                                    op.orders_id,
                                    op.products_model,
                                    op.products_name,
                                    op.products_quantity as customers_order_quantity,
                                    o.orders_id,
                                    o.customers_id,
                                    o.customers_name,
                                    o.customers_telephone,
                                    o.orders_status,
                                    p.products_quantity,
                                    p.products_wharehouse_time_replenishment,
                                    p.products_wharehouse,
                                    p.products_wharehouse_row,
                                    p.products_wharehouse_level_location,
                                    p.products_packaging,
                                    p.products_image
                            from  orders_products op left join products p on (op.products_id = p.products_id),
                                  orders o
                            where  op.orders_id = o.orders_id
                            and o.orders_status = 1             
                            order by o.orders_id, o.date_purchased DESC
                          ";  
    }
  
    $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $products_query_raw, $products_query_numrows);
?>

  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_customers.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
    <span style="text-align:center;">
<?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?><br />
<?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
    </span>
    <span class="col-md-4 pull-right">
<?php
  echo osc_draw_form('status', 'stats_products_wharehouse.php', '', 'get');
  echo TITLE_STATUS . ' ' . osc_draw_pull_down_menu('status', $orders_statuses, '', 'onChange="this.form.submit();"');
  echo osc_hide_session_id();
?>
      </form>
    </span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

      <table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"></td>
                <td class="dataTableHeadingContent" width="130"></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_ORDERS_QUANTITY; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WHAREHOUSE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WHAREHOUSE_ROW; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WHAREHOUSE_LEVEL; ?></td>  
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MODEL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRODUCTS_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PACKAGING; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER_ID; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_ID; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_PHONE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_QTY_LEFT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WAHREHOUSE_TIME_REPLENISHMENT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TEXT_ACTION; ?></td>
              </tr>
<?php

    $products_query = osc_db_query($products_query_raw);

    while ($products = osc_db_fetch_array($products_query)) {
    $rows++;

    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }

  if ($products['products_packaging'] == 0) $products_packaging = '';
  if ($products['products_packaging'] == 1) $products_packaging = TEXT_PRODUCTS_PACKAGING_NEW;
  if ($products['products_packaging'] == 2) $products_packaging = TEXT_PRODUCTS_PACKAGING_REPACKAGED;
  if ($products['products_packaging'] == 3) $products_packaging = TEXT_PRODUCTS_PACKAGING_USED;
  
?>              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', '&pID=' . $products['products_id'] . '&origin=' . 'stats_products_viewed.php' . '?page=' . $_GET['page']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                <td class="dataTableContent" align="right"><?php echo $products['customers_order_quantity']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['products_wharehouse']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['products_wharehouse_row']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['products_wharehouse_level_location']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['products_model']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['products_name']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products_packaging; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['orders_id']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['customers_id']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $products['customers_name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $products['customers_telephone']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $products['products_quantity']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $products['products_wharehouse_time_replenishment']; ?></td>
                <td class="dataTableContent" align="right" width="75">
<?php 
      echo '<a href="' . osc_href_link('customers.php', '&cID='.$products['customers_id'].'&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'client_b2b.gif', ICON_EDIT_CUSTOMER) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href=' . osc_href_link('orders.php', '&oID=' . $products['orders_id'] . '&action=edit') . '>' . osc_image(DIR_WS_ICONS . 'order.gif', ICON_EDIT_ORDER) .'</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');	  
      echo '<a href=' . osc_href_link('categories.php', '&pID=' . $products['products_id'] . '&action=new_product') . '>' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) .'</a>';
?>
</td>


             </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
              </tr>
            </table></td>
          </tr>
        </table>
      </tr>
  </table></td>
   </tr>
 </table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
