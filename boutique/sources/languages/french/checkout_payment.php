<?php

define('NAVBAR_TITLE_1', 'Commande');
define('NAVBAR_TITLE_2', 'Méthode de paiement');

define('HEADING_TITLE', 'Information paiement');

define('TEXT_SELECTED_BILLING_DESTINATION', 'Veuillez choisir dans votre carnet d\'adresses l\'adresse o&ugrave; vous voudriez que la facture soit expédiée');
define('TITLE_BILLING_ADDRESS', 'Adresse de facturation :');

define('TABLE_HEADING_BILLING_ADDRESS', 'Adresse de facturation');
define('TABLE_HEADING_PAYMENT_METHOD', 'Méthode de paiement');
define('TABLE_HEADING_COUPON', 'Insérer le code de votre coupon de remise' );
define('TABLE_HEADING_CONDITIONS', 'Information sur les conditions de vente');

define('TEXT_COUPON', 'Le coupon vous permet de bénéficier d\'une remise sur votre commande. En fonction de vos éléments commandés, le coupon peut s\'appliquer ou non.');

define('TEXT_SELECT_PAYMENT_METHOD', 'Choisissez votre mode de paiement pour cette commande.');
define('TITLE_PLEASE_SELECT', 'Veuillez choisir');
define('TEXT_ENTER_PAYMENT_INFORMATION', 'C\'est actuellement la seule méthode de paiement disponible pour cette commande.');

define('TABLE_HEADING_COMMENTS', 'Ajoutez des commentaires au sujet de votre commande');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continuer la procédure de commande');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'Confirmer cette commande.');

define('TEXT_CONDITIONS_DESCRIPTION', 'Veuillez approuver les conditions de vente liées à cette commande en cochant cette case. Les conditions de vente peuvent être consultées <a target="_blank" href="' . SHOP_CODE_URL_CONDITIONS_VENTE . ' "><u>ici</u></a>.');
define('TEXT_CONDITIONS_CONFIRM', '<strong>J\'ai lu et j\'accepte les conditions de vente liées à cette commande.</strong>');
?>
