<?php
/*
 * modules.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE_MODULES_PAYMENT', 'Modules de paiement');
define('HEADING_TITLE_MODULES_SHIPPING', 'Modules de livraison');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modules total commande');
define('HEADING_TITLE_MODULES_ACTION_RECORDER', 'Modules enregistrement des actions');
define('HEADING_TITLE_MODULES_SOCIAL_BOOKMARKS', 'Modules réseaux sociaux');
define('HEADING_TITLE_MODULES_HEADER_TAGS', 'Modules référencement');
define('HEADING_TITLE_MODULES_ADMIN_DASHBOARD', 'Modules Tableau de bord');

define('TABLE_HEADING_MODULES', 'Modules');
define('TABLE_HEADING_SORT_ORDER', 'Ordre d\'affichage');
define('TABLE_HEADING_ACTION', 'Action');
define('TEXT_INFO_API_VERSION', 'Version API : ');

define('TEXT_MODULE_DIRECTORY', 'Répertoire contenant les modules :');
?>