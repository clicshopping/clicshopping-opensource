<?php
/*
 * products_reviews_info.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (isset($_GET['reviews_id']) && osc_not_null($_GET['reviews_id']) && isset($_GET['products_id']) && osc_not_null($_GET['products_id'])) {
    if ($OSCOM_ProductsReviews->getReviewsCount() < 1) {
      osc_redirect(osc_href_link('product_reviews.php', osc_get_all_get_params(array('reviews_id'))));
    }
  } else {
    osc_redirect(osc_href_link('product_reviews.php', osc_get_all_get_params(array('reviews_id'))));
  }

    if ( $OSCOM_Products->getProductsGroupView() == '1' ||  $OSCOM_Products->getProductsView() == '1') {

      $Qcheck = $OSCOM_PDO->prepare('select count(r.reviews_id) as total
                                     from :table_reviews r,
                                          :table_reviews_description rd
                                     where  r.reviews_id = :reviews_id
                                     and r.status = :status
                                     and r.reviews_id = rd.reviews_id
                                     and rd.languages_id = :languages_id
                                    ');

      $Qcheck->bindInt(':reviews_id', $_GET['reviews_id']);
      $Qcheck->bindInt(':languages_id',  $_SESSION['languages_id']);
      $Qcheck->bindValue(':status', '1');

      $Qcheck->execute();

      $check = $Qcheck->fetch();

      if ( $check['total'] == 0 ) {
        osc_redirect(osc_href_link('reviews.php'));
      } else {

        $Qupdate = $OSCOM_PDO->prepare('update :table_reviews
                                        set reviews_read = reviews_read+1
                                        where reviews_id = :reviews_id
                                      ');
        $Qupdate->bindInt(':reviews_id', (int)$_GET['reviews_id']);
        $Qupdate->execute();

        $Qproducts = $OSCOM_PDO->prepare('select rd.reviews_text,
                                               r.reviews_rating,
                                               r.reviews_id,
                                               r.customers_name,
                                               r.date_added,
                                               r.reviews_read,
                                               p.products_id,
                                               p.products_image
                                        from :table_reviews r,
                                             :table_reviews_description rd,
                                             :table_products p
                                        where r.reviews_id = :reviews_id
                                        and r.reviews_id = rd.reviews_id
                                        and r.products_id = p.products_id
                                        and r.status = :status
                                        and rd.languages_id = :languages_id
                                      ');
        $Qproducts->bindInt(':reviews_id', $_GET['reviews_id']);
        $Qproducts->bindInt(':languages_id', $_SESSION['languages_id']);
        $Qproducts->bindInt(':status', '1');

        $Qproducts->execute();

        $products = $Qproducts->fetch();
      }


     $customers_name = $products['customers_name'];
     $date_added = $OSCOM_Date->getLong($products['date_added']);
     $reviews_text = $products['reviews_text'];
     $reviews_rating = $products['reviews_rating'];

     $products_id = (int)$OSCOM_Products->getId();

// **************************
//    product name
// **************************
      $products_name = '<a href="'.osc_href_link('product_info.php', 'products_id=' . $products_id) . '">' . $OSCOM_Products->getProductsName() .'</a>';
      $products_name_image =  $OSCOM_Products->getProductsName();

// *************************
// display the differents prices before button
// **************************
      $product_price = $OSCOM_Products->getCustomersPrice();

// **************************
// See the button more view details
// **************************
        $button_small_view_details = osc_draw_button(SMALL_IMAGE_BUTTON_DETAILS, '', osc_href_link('product_info.php', 'products_id='. $products_id), 'info', null,'small');
// **************************
// 10 - Display the image
// **************************
        $products_image = '<a href="' . osc_href_link('product_info.php', 'products_id=' . $products_id, 'NONSSL', false) . '">' . osc_image(DIR_WS_IMAGES . $OSCOM_Products->getProductsImage(), $OSCOM_Products->getProductsName(), SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, null, true) . '</a>';



// Doit etre a la fin du fichier
// archive management
      if ($OSCOM_Products->getProductsArchive() == '1' &&  $products['products_id'] == (int)$OSCOM_Products->getId() ) {
          $product_price = '';
          $product_not_sell = PRODUCTS_NOT_SELL;
      }
    }


    require($OSCOM_Template->GeTemplatetLanguageFiles('product_reviews_info'));

    $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('product_reviews_info.php', osc_get_all_get_params()));

    require($OSCOM_Template->getTemplateFiles('product_reviews_info'));

