<?php
/**
 * page_submit_history.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE_SUBMIT_HISTORY','Meta data History');

define('TAB_KEYWORDS_GOOGLE','Google History');
define('TAB_SUBMIT_CATEGORIES','Categories');
define('TAB_SUBMIT_PRODUCTS_INFO','Products Description');
define('TAB_SUBMIT_PRODUCTS_NEW','New');
define('TAB_SUBMIT_SPECIAL','Specials');
define('TAB_SUBMIT_REVIEWS','Comments');
define('TAB_SUBMIT_SEO','Position');
define('TAB_SUBMIT_RANKING','Page Rank & Density');


define('TEXT_DELETE','Delete Search History');
define('TEXT_DATE','Search Date');
define('TEXT_URL','Web Site Url');
define('TEXT_RANK','Rank');
define('TEXT_WORD','Keywords');

?>
