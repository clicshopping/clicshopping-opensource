<?php
/**
 * customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Customers');
define('HEADING_TITLE_EDIT', 'Customer No');
define('HEADING_TITLE_SEARCH', 'Search');

define('TABLE_HEADING_CUSTOMERS_ID', 'Customers No');
define('TABLE_HEADING_FIRSTNAME', 'First Name');
define('TABLE_HEADING_LASTNAME', 'Last Name');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Account Created');
define('TABLE_HEADING_ACTION', 'Actions');
define('TABLE_HEADING_COUNTRY','From');
define('TABLE_HEADING_NUMBER_OF_REVIEWS','Comments Number');
define('TABLE_HEADING_ENTRY_EMAIL_VALIDATION','Email valide');

define('TABLE_ENTRY_GROUPS_NAME','Customers Group');

define('TEXT_DATE_ACCOUNT_CREATED', 'Account Created:');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_DATE_LAST_LOGON', 'Last Logon:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Number of Logons:');
define('TEXT_INFO_COUNTRY', 'Country:');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Number of Reviews:');
define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this customer?');
define('TEXT_DELETE_REVIEWS', 'Delete %s review(s)');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Delete Customer');
define('TYPE_BELOW', 'Type below');
define('PLEASE_SELECT', 'Select One');
define('TEXT_CUSTOMER_DISCOUNT', 'Customer Discount Rate :');


define('TEXT_INFO_NUMBER_OF_ORDERS', 'orders:');

// Approbation du compte client
define('TABLE_ENTRY_VALIDATE','Approved Customers');
define('APPROVED_CLIENT','Customer B2B not approved');
define('TABLE_HEADING_ENTRY_COMPANY_B2B','Company(B2B)');
define('TABLE_HEADING_ENTRY_COMPANY','Company');


// Customer mail
define('EMAIL_SUBJECT', 'Opening account clients on ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Hello ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");

define('IMAGE_CUSTOMER','New customer');
define('IMAGE_EMAIL_APPROVED','Customer email valide');
define('IMAGE_EMAIL_NOT_APPROVED',' Customer email not valide');

define('EMAIL_PASS','Your Password : ');
define('EMAIL_NETWORK','Follow us in real time on  :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');

// Notes
// notes
define('CUSTOMERS_NOTE','Specifics notes concerning the customer');
define('CUSTOMERS_NOTE_SUMMARY','Notes');
define('TAB_NOTES','Customer Notes');

// envoi email
define('ENTRY_CONTACT_CUSTOMER','Would you inform the client by email for this account creation ?');
define('ENTRY_COUPON_CUSTOMER','Would you offer a discount coupon to the customer ?');
define('ENTRY_CUSTOMERS_YES','Yes');
define('ENTRY_CUSTOMERS_NO','No');
define('TEXT_CONTACT_EMAIL','Contact client');

// Autorisation de modification pour les clients
define('ENTRY_CUSTOMERS_MODIFY_COMPANY', 'To authorize the customer to modify this information');
define('ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT', 'To authorize the customer to modify the address by default');
define('ENTRY_CUSTOMERS_ADD_ADDRESS', 'To authorize the customer to add other addresses');
define('ENTRY_CUSTOMER_LOCATION','Location ');
// Message pour les erreurs de saisie
define('ERROR_ENTRY_CUSTOMERS_MODIFY_YES', 'Authorized');
define('ERROR_ENTRY_CUSTOMERS_MODIFY_NO', 'Refused');

// TVA Intracom
define('TVA_INTRACOM_VERIFY','Verify');
define('TITLE_AIDE_CUSTOMERS_TVA','Note on VAT Intracom');
define('TITLE_AIDE_TVA_CUSTOMERS','Please, valid the document before to verify the society VAT');

// Aide
define('TITLE_AIDE_CUSTOMERS_IMAGE', 'Help');
define('TITLE_AIDE_CUSTOMERS_DEFAULT_ADRESSE', 'Note on the authorizations of modifications of the customers');
define('TEXT_AIDE_CUSTOMERS_DEFAULT_ADRESSE', '<u>To authorize the customer to add others addresses</u> :<ul><li>Not activated it prohibits the customer to add new addresses in its notebook.</li><li>Active option in the zone <i><strong>My Account</strong></i> of the customer and  during <i><strong>the order process</strong></i>.</li></ul><br /><u>To authorize the customer to modify the address by default</u> :<ul><li>Not activated it prohibits the customer to modify or remove the address by defect in its notebook.</li><li>Not activated it also prohibits <i><strong>the possibility of choosing another bill-to-address</i></strong> during the process order.</li></ul>');

// Raccourcis
define('TEXT_EDIT_GROUP_CUSTOMER', 'Modify the group');
define('ICON_EDIT_CUSTOMER', 'Edit customer');
define('ICON_EDIT_ORDERS','Search client Orders');
define('ICON_EDIT_CUSTOMERS_GROUP','Edit customer group');
define('ICON_EDIT_NEW_PASSWORD','To send a new password');
?>