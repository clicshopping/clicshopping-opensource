<?php
  /**
   * cfm_modules_products_new.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_products_new {
    var $code = 'modules_products_new';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_PRODUCTS_NEW_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_products_new() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_products_new/';
      $this->title = MODULE_CFG_MODULE_PRODUCTS_NEW_MODULES_TITLE;
    }
  }
?>
