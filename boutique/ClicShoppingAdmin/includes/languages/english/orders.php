<?php
/**
 * orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Orders');
define('HEADING_TITLE_SEARCH', 'Order ID');
define('HEADING_TITLE_STATUS', 'Status:');

define('TITLE_ORDERS_ADRESSE', 'Addresses');
define('TITLE_ORDERS_CUSTOMERS', 'Information on the customer');
define('TITLE_ORDERS_PAIEMENT', 'Payment');
define('TITLE_ORDERS_HISTORY', 'History');

define('TABLE_HEADING_COMMENTS', 'Comments');
define('TABLE_HEADING_CUSTOMERS', 'Customers');
define('TABLE_HEADING_ORDER_TOTAL', 'Order Total');
define('TABLE_HEADING_DATE_PURCHASED', 'Date Purchased');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Actions');
define('TABLE_HEADING_QUANTITY', 'Qty.');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_REALISED_BY','Modified by');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Price (ex)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Price (inc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ex)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (inc)');
define('TABLE_HEADING_COLOR_GROUP','Group B2B');
define('TABLE_HEADING_ORDERS','Orders No ');
define('TABLE_HEADING_ODOO', 'Odoo');

define('TABLE_HEADING_CUSTOMER_NOTIFIED', 'Customer Notified');
define('TABLE_HEADING_DATE_ADDED', 'Date Added');

define('ENTRY_CUSTOMER', 'Customer:');
define('ENTRY_SOLD_TO', 'SOLD TO:');
define('ENTRY_DELIVERY_TO', 'Delivery To:');
define('ENTRY_SHIP_TO', 'SHIP TO:');
define('ENTRY_SHIPPING_ADDRESS', 'Shipping Address:');
define('ENTRY_BILLING_ADDRESS', 'Billing Address:');
define('ENTRY_PAYMENT_METHOD', 'Payment Method:');
define('ENTRY_CREDIT_CARD_TYPE', 'Credit Card Type:');
define('ENTRY_CREDIT_CARD_OWNER', 'Credit Card Owner:');
define('ENTRY_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('ENTRY_CREDIT_CARD_EXPIRES', 'Credit Card Expires:');
define('ENTRY_SUB_TOTAL', 'Sub-Total:');
define('ENTRY_TAX', 'Tax:');
define('ENTRY_SHIPPING', 'Shipping:');
define('ENTRY_TOTAL', 'Total:');
define('ENTRY_DATE_PURCHASED', 'Date Purchased:');
define('ENTRY_STATUS', 'Status:');
define('ENTRY_ATOS_BOARD_INFORMATIONS','Transaction Informations');
define('ENTRY_STATUS_ORDERS_TRACKING_NAME','Tracking company (shipping) : ');
define('ENTRY_STATUS_ORDERS_TRACKING_NUMBER','Tracking number : ');

define('ENTRY_STATUS_INVOICE', 'PDF to create : '); 
define('ENTRY_STATUS_COMMENT_INVOICE','PDF  created; : ');
define('ENTRY_STATUS_INVOICE_REALISED', 'Realised by : ');
define('ENTRY_STATUS_INVOICE_NOTE','<u>Others Informations</u> :<br /> '); 

define('ENTRY_CLIENT_COMPUTER_IP','Client address ip : ');
define('ENTRY_PROVIDER_NAME_CLIENT','Client provider : ');
define('TEXT_CONDITION_GENERAL_OF_SALES',' Contract of general conditions sales concerning this order');

define('ENTRY_DATE_LAST_UPDATED', 'Date Last Updated:');
define('ENTRY_NOTIFY_CUSTOMER', 'Notify Customer:');
define('ENTRY_NOTIFY_COMMENTS', 'Append Comments:');
define('ENTRY_PRINTABLE', 'Print Invoice');

define('TEXT_INFO_HEADING_DELETE_ORDER', 'Delete Order');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this order?');
define('TEXT_INFO_RESTOCK_PRODUCT_QUANTITY', 'Restock product quantity');
define('TEXT_DATE_ORDER_CREATED', 'Date Created:');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_PAYMENT_METHOD', 'Payment Method:');
define('ENTRY_CUSTOMER_LOCATION','Location');

define('TEXT_ALL_ORDERS', 'All Orders');
define('TEXT_NO_ORDER_HISTORY', 'No Order History Available');

define('TEXT_INFO_HEADING_ARCHIVE','Archive this order');
define('TEXT_INFO_ARCHIVE_INTRO','Do you wish archive this order ?');

define('EMAIL_SEPARATOR', '------------------------------------------------------');

define('EMAIL_TEXT_SUBJECT', 'Order Update on '.STORE_NAME);
define('EMAIL_INTRO_COMMAND','Hello,' . "\n\n" . 'An actualization of your order was carried out. Please find the whole of information below.
'. "\n\n" . ''.STORE_NAME.' '. "\n" . ' '.STORE_NAME_ADDRESS.'');
define('EMAIL_TEXT_ORDER_NUMBER', 'Order Number:');
define('EMAIL_TEXT_INVOICE_URL', 'Detailed Invoice:');
define('EMAIL_TEXT_DATE_ORDERED', 'Date Ordered:');
define('EMAIL_TEXT_STATUS_UPDATE', 'Your order has been updated to the following status.' . "\n\n" . 'New status: %s' . "\n\n" . 'If you want know the comments of this order, please, connect you in your account. '. "\n\n" . 'If you have questions concerning the evolution of the procedure of order, you can answer this email '. "\n\n" . '
<U><font size="2">Confidentiality Note :</font></U><font size="2">'. "\n" . '
This e-mail message is intended only for the named recipient(s) above and may contain information that is privileged, confidential and/or exempt from disclosure under applicable law.  If you have received this message in error, or are not the named recipient(s), please immediately notify the sender and delete this e-mail message and send an email at ' . STORE_OWNER_EMAIL_ADDRESS . '. '. "\n" . '
<p><font size="2">In accordance with the <strong>Law</strong> on Data-processing Law and Freedom, you are entitled to correction of your personal data or on request by email</font>
</div>');
define('EMAIL_TEXT_COMMENTS_UPDATE', 'The comments for your order are' . "\n\n%s\n\n");

define('ERROR_ORDER_DOES_NOT_EXIST', 'Error: Order does not exist.');
define('SUCCESS_ORDER_UPDATED', 'Success: Order has been successfully updated.');
define('WARNING_ORDER_NOT_UPDATED', 'Warning: Nothing to change. The order was not updated.');

define('ENTRY_ORDER_SIRET', 'Registration Code (ex: RCS):');
define('ENTRY_TVA_INTRACOM', 'N&deg; of VAT intracom:');
define('ENTRY_ORDER_CODE_APE', 'Nomenclatur Number (ex: APE):');

define('ICON_EDIT_CUSTOMER', 'Edit customer');
define('ICON_EDIT_ORDER', 'Edit order');

define('TEXT_ODOO_INVOICE', '<p style="color:red;">Order saved in Odoo</p>');
define('TEXT_ODOO_INVOICE_REGISTRED', '<p style="color:red;">Invoice saved in Odoo</p>');
define('TEXT_ODOO_INVOICE_CANCELLED', '<p style="color:red;">Invoice cancelled in Odoo</p>');
define('TEXT_ODOO_INVOICE_CONFIRM', '<p style="color:red;">Invoice registred in Odoo</p>');
?>