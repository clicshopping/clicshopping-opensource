<?php
/*
 * blog_content.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

    require('includes/application_top.php');

    if (!isset($_GET['blog_content_id'])) {
      osc_redirect(osc_href_link('index.php') );
    }

    require($OSCOM_Template->GeTemplatetLanguageFiles('blog_content'));

    $id = (int)$OSCOM_Blog->getId();

    $OSCOM_Breadcrumb->add($OSCOM_Blog->getBlogContentName(), osc_href_link('blog_content.php','blog_content_id='.(int)$id));

    require($OSCOM_Template->getTemplateFiles('blog_content'));
