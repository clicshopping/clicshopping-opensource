<?php
/**
 * blog.php 
 * @copyright Copyright 2008 - ClicShopping http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

/**
 * the blog category name
 * 
 * @param string  $category_id, $language_id
 * @return string $category['blog_categories_name'],  name of the blog categorie
 * @access public
 */
  function osc_get_blog_category_name($blog_category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QcategoryBlog = $OSCOM_PDO->prepare('select blog_categories_name
                                          from :table_blog_categories_description
                                          where blog_categories_id = :blog_categories_id
                                          and language_id = :language_id
                                        ');
    $QcategoryBlog->bindInt(':blog_categories_id', (int)$blog_category_id);
    $QcategoryBlog->bindInt(':language_id', (int)$language_id);

    $QcategoryBlog->execute();

    $blog_category = $QcategoryBlog->fetch();

    return $blog_category['blog_categories_name'];
  }
  
/**
 * the category description
 * 
 * @param string  $blog_category_id, $language_id
 * @return string $category['blog_categories_name'],  description of the blog categorie
 * @access public
 */
  function osc_get_blog_category_description($blog_category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QcategoryBlog = $OSCOM_PDO->prepare('select blog_categories_description
                                          from :table_blog_categories_description
                                          where blog_categories_id = :blog_categories_id
                                          and language_id = :language_id
                                        ');
    $QcategoryBlog->bindInt(':blog_categories_id', (int)$blog_category_id);
    $QcategoryBlog->bindInt(':language_id', (int)$language_id);

    $QcategoryBlog->execute();

    $blog_category = $QcategoryBlog->fetch();

    return $blog_category['blog_categories_description'];
  }


/**
* the category meta title title
*
* @param string  $blog_category_id, $language_id
* @return string $category['categories_head_title_tag'],  meta tile of the blog categorie
* @access public
*/

  function osc_get_blog_categories_head_title_tag($blog_category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

     $QcategoryBlog = $OSCOM_PDO->prepare('select blog_categories_head_title_tag
                                            from :table_blog_categories_description
                                            where blog_categories_id = :blog_categories_id
                                            and language_id = :language_id
                                          ');
     $QcategoryBlog->bindInt(':blog_categories_id', (int)$blog_category_id);
     $QcategoryBlog->bindInt(':language_id', (int)$language_id);

     $QcategoryBlog->execute();

     $blog_category = $QcategoryBlog->fetch();

     return $blog_category['blog_categories_head_title_tag'];
  }


/**
* the category meta description 
*
* @param string  $blog_category_id, $language_id
* @return string $category['categories_head_title_tag'],  meta description of the blog categorie
* @access public
*/

  function osc_get_blog_categories_head_desc_tag($blog_category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QcategoryBlog = $OSCOM_PDO->prepare('select blog_categories_head_desc_tag
                                          from :table_blog_categories_description
                                          where blog_categories_id = :language_id
                                          and language_id = :language_id
                                        ');
    $QcategoryBlog->bindInt(':blog_categories_id', (int)$blog_category_id);
    $QcategoryBlog->bindInt(':language_id', (int)$language_id);

    $QcategoryBlog->execute();

    return $QcategoryBlog->value('blog_categories_head_desc_tag');
  }

  /**
   * the category meta keywords title
   *
   * @param string  $blog_category_id, $language_id
   * @return string $category['categories_head_title_tag'],  meta keywords of the blog categorie
   * @access public
   */
  function osc_get_blog_categories_head_keywords_tag($blog_category_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QcategoryBlog = $OSCOM_PDO->prepare('select blog_categories_head_keywords_tag
                                          from :table_blog_categories_description
                                          where blog_categories_id = :language_id
                                          and language_id = :language_id
                                        ');
    $QcategoryBlog->bindInt(':blog_categories_id', (int)$blog_category_id);
    $QcategoryBlog->bindInt(':language_id', (int)$language_id);

    $QcategoryBlog->execute();

    return $QcategoryBlog->value('blog_categories_head_keywords_tag');
  }


/**
 * osc_get_blog_content_in_category_count
 * Count how many products exist in a category
 * @param string $blog_category_id, $include_deactivated (FALSE OR TRUE)
 * @return string $blog_content_countt, the Count how many products exist in a category
 * @access public
 */
  function osc_get_blog_content_in_category_count($blog_category_id, $include_deactivated = false) {
    global $OSCOM_PDO;
    $blog_content_count = 0;

    if ($include_deactivated) {
      $QblogContent = $OSCOM_PDO->prepare('select count(*) as total 
                                            from :table_blog_content p, 
                                                 :table_blog_content_to_categories p2c 
                                            where p.blog_content_id = p2c.blog_content_id 
                                            and p2c.blog_categories_id = :blog_categories_id
                                          ');
      $QblogContent->bindInt(':blog_categories_id', (int)$blog_category_id);

    } else {
      $QblogContent = $OSCOM_PDO->prepare('select count(*) as total 
                                            from :table_blog_content p, 
                                                 :table_blog_content_to_categories p2c 
                                            where p.blog_content_id = p2c.blog_content_id 
                                            and p.blog_content_status = :blog_content_status
                                            and p2c.blog_categories_id = :blog_categories_id
                                       ');
      $QblogContent->bindInt(':blog_content_status', '1' );
      $QblogContent->bindInt(':blog_categories_id', (int)$blog_category_id);

    }

    $QblogContent->execute();
    $blog_content = $QblogContent->fetch();

    $blog_content_count += $blog_content['total'];

    $Qcategories = $OSCOM_PDO->prepare('select blog_categories_id
                                        from :table_blog_categories
                                        where parent_id = :parent_id
                                       ');

    $Qcategories->bindInt(':parent_id', (int)$blog_category_id);
    $Qcategories->execute();


    if ($Qcategories->fetch() !== false) {
      while ($childs = $Qcategories->fetch() ) {
        $blog_content_count += osc_get_blog_content_in_category_count($childs['blog_categories_id'], $include_deactivated);
      }
    }

    return $blog_content_count;
  }


/**
 * osc_childs_in_blog_category_count
 * Count how many subcategories exist in a category
 * @param string $blog_category_id
 * @return string $blog_categories_count, the tchilds_in_blog_category_count
 * @access public
 */
  function osc_childs_in_blog_category_count($blog_category_id) {
    global $OSCOM_PDO;
    
    $blog_categories_count = 0;

    $Qcategories = $OSCOM_PDO->prepare('select blog_categories_id
                                        from :table_blog_categories
                                        where parent_id = :parent_id
                                       ');

    $Qcategories->bindInt(':parent_id', (int)$blog_category_id);
    $Qcategories->execute();

    while ($categories = $Qcategories->fetch()  ) {
      $blog_categories_count++;
      $blog_categories_count += osc_childs_in_category_count($categories['blog_categories_id']);
    }

    return $blog_categories_count;
  }


/**
 * blog category tree
 * 
 * @param string $parent_id, $spacing, $exclude, $category_tree_array , $include_itself
 * @return string $category_tree_array, the tree of category
 * @access public
 */
  function osc_get_blog_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $OSCOM_PDO;

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {

      $Qcategory = $OSCOM_PDO->prepare('select cd.blog_categories_name
                                        from :table_blog_categories_description cd
                                        where cd.language_id = :language_id
                                        and cd.blog_categories_id = :parent_id
                                       ');

      $Qcategory->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qcategory->bindInt(':parent_id', (int)$parent_id);
      $Qcategory->execute();

      $category = $Qcategory->fetch();

      $category_tree_array[] = array('id' => $parent_id, 'text' => $category['blog_categories_name']);
    }

    $Qcategory = $OSCOM_PDO->prepare('select c.blog_categories_id,
                                             cd.blog_categories_name,
                                             c.parent_id
                                      from :table_blog_categories c,
                                           :table_blog_categories_description cd
                                      where c.blog_categories_id = cd.blog_categories_id
                                      and cd.language_id = :language_id
                                      and c.parent_id = :parent_id
                                      order by c.sort_order,
                                               cd.blog_categories_name
                                    ');

    $Qcategory->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qcategory->bindInt(':parent_id', (int)$parent_id);
    $Qcategory->execute();

    while ($categories = $Qcategory->fetch()) {

      if ($exclude != $categories['blog_categories_id']) $category_tree_array[] = array('id' => $categories['blog_categories_id'],
                                                                                        'text' => $spacing . $categories['blog_categories_name']);
      $category_tree_array = osc_get_blog_category_tree($categories['blog_categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
  }

/**
 * blog remove blog categories
 * 
 * @param string $blog_categories_id
 * @return string
 * @access public
 */
  function osc_remove_blog_category($blog_categories_id) {
    global $OSCOM_PDO;

    $QcategoryBlogImage = $OSCOM_PDO->prepare('select blog_categories_image 
                                                from :table_blog_categories
                                                where blog_categories_id = :blog_categories_id
                                               ');
    $QcategoryBlogImage->bindInt(':blog_categories_id', (int)$blog_categories_id);
    $QcategoryBlogImage->execute();

    $category_blog_image = $QcategoryBlogImage->fetch();

// Controle si l'image est utilise sur une autre categorie du blog
    $QduplicateBlogImage = $OSCOM_PDO->prepare('select count(*) as total  
                                                from :table_blog_categories
                                                where blog_categories_image = :blog_categories_image
                                               ');
    $QduplicateBlogImage->bindValue(':blog_categories_image', osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateBlogImage->execute();

    $duplicate_blog_image = $QduplicateBlogImage->fetch();


// Controle si l'image est utilise sur les descriptions d'un blog
    $QduplicateImageBlogCategoriesDescription = $OSCOM_PDO->prepare('select count(*) as total 
                                                                     from :table_blog_categories_description
                                                                     where blog_categories_description like "%":blog_categories_image"%"
                                                                   ');
    $QduplicateImageBlogCategoriesDescription->bindValue(':blog_categories_image', osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageBlogCategoriesDescription->execute();

    $duplicate_image_blog_categories_description = $QduplicateImageBlogCategoriesDescription->fetch();


// Controle si l'image est utilise sur une autre categorie
    $QduplicateImageCategories = $OSCOM_PDO->prepare('select count(*) as total  
                                                      from :table_categories
                                                      where categories_image = :categories_image
                                                     ');
    $QduplicateImageCategories->bindValue(':categories_image',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageCategories->execute();

    $duplicate_image_categories = $QduplicateImageCategories->fetch();

// Controle si l'image est utilise sur les descriptions d'une catégorie

    $QduplicateImageCategoriesDescription = $OSCOM_PDO->prepare('select count(*) as total  
                                                                  from :table_categories_description
                                                                  where categories_description like "%":categories_description"%"
                                                                 ');
    $QduplicateImageCategoriesDescription->bindValue(':categories_description',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageCategoriesDescription->execute();

    $duplicate_image_categories_description = $QduplicateImageCategoriesDescription->fetch();

// Controle si l'image est utilise le visuel d'un produit


    $QduplicateImageProduct = $OSCOM_PDO->prepare('select count(*) as total  
                                                  from :table_products
                                                   where products_image = :products_image 
                                                   or products_image_zoom = :products_image_zoom
                                                   or products_image_medium = :products_image_medium
                                                 ');
    $QduplicateImageProduct->bindValue(':products_image',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageProduct->bindValue(':products_image_zoom',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageProduct->bindValue(':products_image_medium',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageProduct->execute();

    $duplicate_image_product = $QduplicateImageProduct->fetch();


// Controle si l'image est utilise sur les descriptions d'un produit
    $QduplicateImageProductDescription = $OSCOM_PDO->prepare('select count(*) as total  
                                                              from :table_products_description
                                                              where products_description like "%":products_description"%"
                                                             ');
    $QduplicateImageProductDescription->bindValue(':products_description',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageProductDescription->execute();

    $duplicate_image_product_description = $QduplicateImageProductDescription->fetch();

// Controle si l'image est utilisee sur une banniere
    $QduplicateImageBanners = $OSCOM_PDO->prepare('select count(*) as total  
                                                  from :table_banners
                                                  where banners_image = :banners_image
                                                 ');
    $QduplicateImageBanners->bindValue(':banners_image',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageBanners->execute();

    $duplicate_image_banners = $QduplicateImageBanners->fetch();

// Controle si l'image est utilisee sur les fabricants
    $QduplicateImageManufacturers = $OSCOM_PDO->prepare('select count(*) as total  
                                                        from :table_manufacturers
                                                        where manufacturers_image = :manufacturers_image
                                                      ');
    $QduplicateImageManufacturers->bindValue(':manufacturers_image',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageManufacturers->execute();

    $duplicate_image_manufacturers = $QduplicateImageManufacturers->fetch();

// Controle si l'image est utilisee sur les fournisseurs
    $QduplicateImageSuppliers = $OSCOM_PDO->prepare('select count(*) as total  
                                                     from :table_suppliers
                                                     where suppliers_image = :suppliers_image
                                                   ');
    $QduplicateImageSuppliers->bindValue(':suppliers_image',  osc_db_input($category_blog_image['blog_categories_image']) );
    $QduplicateImageSuppliers->execute();

    $duplicate_image_suppliers = $QduplicateImageSuppliers->fetch();
	
    if (($duplicate_blog_image['total'] < 2) && 
        ($duplicate_image_blog_categories_description['total'] == 0) && 
        ($duplicate_image_categories['total'] == 0) && 
        ($duplicate_image_categories_description['total'] == 0) && 
        ($duplicate_image_product['total'] == 0) &&
        ($duplicate_image_product_description['total'] == 0) && 
        ($duplicate_image_banners['total'] == 0) && 
        ($duplicate_image_manufacturers['total'] == 0) && 
        ($duplicate_image_suppliers['total'] == 0)) {

// delete categorie image
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_blog_image['blog_categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_blog_image['blog_categories_image']);
      }
    }

    $Qdelete = $OSCOM_PDO->prepare('delete 
                                    from :table_blog_categories
                                    where blog_categories_id = :blog_categories_id 
                                  ');
    $Qdelete->bindInt(':blog_categories_id',  (int)$blog_categories_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete 
                                    from :table_blog_categories_description
                                    where blog_categories_id = :blog_categories_id 
                                  ');
    $Qdelete->bindInt(':blog_categories_id',  (int)$blog_categories_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete 
                                    from :table_blog_content_to_categories
                                    where blog_categories_id = :blog_categories_id 
                                  ');
    $Qdelete->bindInt(':blog_categories_id',  (int)$blog_categories_id);
    $Qdelete->execute();

    if (USE_CACHE == 'true') {
      osc_cache_reset('blog_tree-');
    }
  }


/**
 * blog path on categories generated
 * 
 * @param string $id, $categories_array, $from, $index
 * @return string $$categories_array, an array on categories
 * @access public
 */
   function osc_generate_blog_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $OSCOM_PDO;

    if (!is_array($categories_array)) $categories_array = array();

    if ($from == 'product') {

      $Qcategories = $OSCOM_PDO->prepare('select blog_categories_id
                                          from :table_blog_content_to_categories
                                          where blog_content_id = :blog_content_id
                                          ');
      $Qcategories->bindInt(':blog_content_id',  (int)$id);
      $Qcategories->execute();


      while ($categories = $Qcategories->fetch() ) {

        if ($categories['blog_categories_id'] == '0') {
          $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
        } else {

          $Qcategories = $OSCOM_PDO->prepare('select cd.blog_categories_name,
                                                     c.parent_id
                                              from :table_blog_categories c,
                                                   :table_blog_categories_description cd
                                             where c.blog_categories_id = :blog_categories_id
                                             and c.blog_categories_id = cd.blog_categories_id
                                             and cd.language_id = :language_id
                                          ');
          $Qcategories->bindInt(':blog_categories_id', (int)$categories['blog_categories_id'] );
          $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id']);

          $Qcategories->execute();

          $category = $Qcategories->fetch();

          $categories_array[$index][] = array('id' => $categories['blog_categories_id'], 'text' => $category['blog_categories_name']);



          if ( (osc_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = osc_generate_blog_category_path($category['parent_id'], 'category', $categories_array, $index);
          $categories_array[$index] = array_reverse($categories_array[$index]);
        }
        $index++;
      }
    } elseif ($from == 'category') {

      $Qcategories = $OSCOM_PDO->prepare('select cd.blog_categories_name,
                                                 c.parent_id
                                          from :table_blog_categories c,
                                               :table_blog_categories_description cd
                                          where c.blog_categories_id = :blog_categories_id
                                          and c.blog_categories_id = cd.blog_categories_id
                                          and cd.language_id = :language_id
                                         ');
      $Qcategories->bindInt(':blog_categories_id', (int)$id );
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id']);

      $Qcategories->execute();

      $category = $Qcategories->fetch();

      $categories_array[$index][] = array('id' => $id, 'text' => $category['blog_categories_name']);
      if ( (osc_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = osc_generate_blog_category_path($category['parent_id'], 'category', $categories_array, $index);
    }

    return $categories_array;
  }

/**
 * output generated blog category path
 * 
 * @param string $id, $from, 
 * @return string $calculated_category_path_string
 * @access public
 */
  function osc_output_generated_blog_category_path($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = osc_generate_blog_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -16) . '<br />';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -6);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }




/**
 * get generated blog category path
 * 
 * @param string $id, $from, 
 * @return string $calculated_category_path_string
 * @access public
 */


  function osc_get_generated_blog_category_path_ids($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = osc_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '_';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br />';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -6);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }




// ******************************************* 
// Blog Content                                
// *******************************************/


/**
 * Name of the blog content
 * 
 * @param string  $blog_content_id, $language_id
 * @return string $blog_content['blog_content_name'], name of the blog_content
 * @access public
 */
  function osc_get_blog_content_name($blog_content_id, $language_id = 0) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_name
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_name'];
  }


/**
 * Description Name
 * 
 * @param string  $blog_content_id, $language_id
 * @return string $blog_content['products_description'], description name
 * @access public
 */
  function osc_get_blog_content_description($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_description
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_description'];
  }


/**
 * Title Name of the submit
 * 
 * @param string  $blog_content_id, $language_id
 * @return string product['products_head_title_tag'], description name
 * @access public
 */
  function osc_get_blog_content_head_title_tag($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_head_title_tag
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_head_title_tag'];
  }

/**
 * Description Name
 * 
 * @param string  $blog_content_id, $language_id
 * @return string $blog_content['products_head_desc_tag'], description name
 * @access public
 */
  function osc_get_blog_content_head_desc_tag($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_head_desc_tag
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_head_desc_tag'];
  }

/**
 * keywords Name
 * 
 * @param string  $blog_content_id, $language_id
 * @return string $blog_content['products_head_keywords_tag'], keywords name
 * @access public
 */
  function osc_get_blog_content_head_keywords_tag($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_head_keywords_tag
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_head_keywords_tag'];
  }


/**
 * Product Tag Name
 * 
 * @param string  $blog_content_id, $language_id
 * @return string $blog_content['blog_content_head_tag_product'], keywords name
 * @access public
 */
  function osc_get_blog_content_tag_product($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_head_tag_product
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_head_tag_product'];
  }


/**
 * blog Tag Name
 * 
 * @param string  $blog_content_id, $language_id
 * @return string $blog_content['blog_content_head_tag_blog'], keywords name
 * @access public
 */
  function osc_get_blog_content_tag_blog($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $QblogContent = $OSCOM_PDO->prepare('select blog_content_head_tag_blog
                                        from :table_blog_content_description
                                        where blog_content_id = :blog_content_id
                                        and language_id = :language_id
                                      ');
    $QblogContent->bindInt(':blog_content_id', (int)$blog_content_id  );
    $QblogContent->bindInt(':language_id', (int)$language_id  );
    $QblogContent->execute();

    $blog_content = $QblogContent->fetch();

    return $blog_content['blog_content_head_tag_blog'];
  }
  
  

/**
 * Status products - Sets the status of a product
 * 
 * @param string products_id, status
 * @return string status on or off
 * @access public 
 */
  function osc_set_blog_content_status($blog_content_id, $status) {
    global $OSCOM_PDO;
    if ($status == '1') {

      return $OSCOM_PDO->save('blog_content', ['blog_content_status' => 1,
                                              'blog_content_last_modified' => 'now()',
                                              ],
                                              ['blog_content_id' => (int)$blog_content_id]
                            );

      if (USE_CACHE == 'true') {
        osc_cache_reset('blog_tree-');
      }
    } elseif ($status == '0') {

      return $OSCOM_PDO->save('blog_content', ['blog_content_status' => 0,
                                                'blog_content_last_modified' => 'now()',
                                              ],
                                              ['blog_content_id' => (int)$blog_content_id]
                              );

      if (USE_CACHE == 'true') {
        osc_cache_reset('blog_tree-');
      }

    } else {
      return -1;
    }
  }


/**
 * Blog content : remove blog
 * 
 * @param string blog_content_id
 * @return 
 * @access public 
 */

  function osc_remove_blog_content($blog_content_id) {
    global $OSCOM_PDO;

    $Qdelete = $OSCOM_PDO->prepare('delete 
                                    from :table_blog_content
                                    where blog_content_id = :blog_content_id 
                                  ');
    $Qdelete->bindInt(':blog_content_id',  (int)$blog_content_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete 
                                    from :table_blog_content_to_categories
                                    where blog_content_id = :blog_content_id 
                                  ');
    $Qdelete->bindInt(':blog_content_id',  (int)$blog_content_id);
    $Qdelete->execute();

    $Qdelete = $OSCOM_PDO->prepare('delete 
                                    from :table_blog_content_description
                                    where blog_content_id = :blog_content_id 
                                  ');
    $Qdelete->bindInt(':blog_content_id',  (int)$blog_content_id);
    $Qdelete->execute();

    if (USE_CACHE == 'true') {
      osc_cache_reset('blog_tree-');
    }
  }


/**
 *  Blog content Description summary
 *
 * @param string  $product_id, $language_id
 * @return string $product['products_description'], description name
 * @access public
 */
  function osc_get_blog_content_description_summary($blog_content_id, $language_id) {
    global $OSCOM_PDO;

    if (!$language_id) $language_id = $_SESSION['languages_id'];

    $Qblog = $OSCOM_PDO->prepare('select blog_content_description_summary
                                  from :table_blog_content_description
                                  where blog_content_id = :blog_content_id
                                  and language_id = :language_id
                                ');
    $Qblog->bindInt(':blog_content_id', (int)$blog_content_id  );
    $Qblog->bindInt(':language_id', (int)$language_id  );
    $Qblog->execute();

    $blog = $Qblog->fetch();

    return $blog['blog_content_description_summary'];
  }
