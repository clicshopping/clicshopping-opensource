<?php
/**
 * session_auto_start.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('WARNING_SESSION_AUTO_START', 'session.auto_start is enabled - please disable this php feature in php.ini and restart the web server.');
?>
