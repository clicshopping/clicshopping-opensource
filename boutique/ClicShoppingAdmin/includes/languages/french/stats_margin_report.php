<?php
/*
 * stats_margin_report.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Rapport de Marges');
define('TABLE_HEADING_PRODUCT', 'Produits');
define('TABLE_HEADING_COST', 'Prix d\'achat');
define('TABLE_HEADING_PRICE', 'Prix');
define('TABLE_HEADING_SPECIAL_PRICE', 'Prix promotionnelle');
define('TABLE_HEADING_MARGIN_DOLLARS', 'Marge (�)');
define('TABLE_HEADING_MARGIN_PERCENTAGE', 'Marge (%)');
define('TEXT_ACTION','Action');
define('TEXT_FILE_DOES_NOT_EXIST', 'Ce fichier n\'existe pas');
define('TEXT_CACHE_DIRECTORY', 'Répertoire Cache :');
define('TEXT_SHOW', 'Afficher :');
define('TEXT_ALL_MANUFACTURERS', 'Tous les fabricants');
define('TEXT_ALL_CATEGORIES_BY_MANUFACTURER', 'Toutes les catégories par Fabricants');
define('TEXT_ALL_CATEGORIES', 'Toutes les catégories');
define('ERROR_CACHE_DIRECTORY_DOES_NOT_EXIST', 'Erreur: Le répertoire Cache n\'existe pas. Merci de régler cela Configuration->Cache.');
define('ERROR_CACHE_DIRECTORY_NOT_WRITEABLE', 'Erreur: Le répertoire Cache n\'est inscriptible.');
define('TEXT_SORT_PRODUCTS', 'Tri des produits ');
define('TEXT_DESCENDINGLY', 'desc');
define('TEXT_ASCENDINGLY', 'asc');
define('TEXT_BY', ' par ');
define('TEXT_HANDLING','Autres Couts associés');
define('TEXT_NO_PRODUCTS', 'Il n\'y a pas de produit dans la base.');
define('TEXT_RESULT_PAGES', 'Résultat : ');
define('BUTTON_BACK_TO_MAIN', 'Retour Menu Principal');
define('TEXT_SELECT_REPORT', 'Choisissez un rapport');
define('TEXT_SELECT_REPORT_DAILY', 'Aujourd\'hui');
define('TEXT_SELECT_REPORT_YESTERDAY', 'Hier');
define('TEXT_SELECT_REPORT_WEEKLY', 'Cette semaine');
define('TEXT_SELECT_REPORT_LASTWEEK', 'Semaine derniére');
define('TEXT_SELECT_REPORT_MONTHLY', 'Ce mois');
define('TEXT_SELECT_REPORT_LASTMONTH', 'Mois dernier');
define('TEXT_SELECT_REPORT_QUARTERLY', 'Ce trimestre ');
define('TEXT_SELECT_REPORT_SEMIANNUALLY', ' Ce semestre');
define('TEXT_SELECT_REPORT_ANNUALLY', 'Annuelle');
define('TEXT_REPORT_HEADER', 'Rapport pour toutes les commandes. ');
define('TEXT_REPORT_HEADER_FROM_DAY', 'Rapport du jour : ');
define('TEXT_REPORT_HEADER_FROM_YESTERDAY', 'Rapport d\'hier : ');
define('TEXT_REPORT_HEADER_FROM_WEEK', 'Rapport de la semaine débutant le : ');
define('TEXT_REPORT_HEADER_FROM_LASTWEEK', 'Rapport de la semaine dernière. ');
define('TEXT_REPORT_HEADER_FROM_MONTH', 'Rapport du mois débutant le : ');
define('TEXT_REPORT_HEADER_FROM_LASTMONTH', 'Rapport du mois dernier');
define('TEXT_REPORT_HEADER_FROM_QUARTER', 'Rapport du trimestre débutant le : ');
define('TEXT_REPORT_HEADER_FROM_SEMIYEAR', 'Rapport du semestre débutant le : ');
define('TEXT_REPORT_HEADER_FROM_YEAR', 'Rapport de l\'année débutant le : ');
define('TEXT_REPORT_BETWEEN_DAYS', 'Rapport compris entre le ');
define('TEXT_AND', ' et le ');
define('TEXT_ORDER_ID', 'Ref commande');
define('TEXT_PRODUCTS_NAME', 'Nom du Produit');
define('TEXT_PRODUCTS_COST', 'Prix d\'achat');
define('TEXT_PRODUCTS_PRICE', 'Prix de vente');
define('TEXT_REDUC', 'Réductions');
define('TEXT_TOTAL_REDUC', 'Montant des réductions');
define('TEXT_SPECIAL_PRICE', 'Prix Promo');
define('TEXT_MARGIN_MONEY', 'Marge en Euros');
define('TEXT_MARGIN_PERCENTAGE', 'Marge en %');
define('TEXT_TOTAL_ITEMS_SOLD', 'Total Articles Vendus : ');
define('TEXT_TOTAL_MARGIN', 'Marge brute totale : ');
define('TEXT_ITEMS_SOLD', 'Articles Vendus');
define('TEXT_SALES_AMOUNT', 'Montant brut des commandes');
define('TEXT_SALES_NET', 'Montant net des commandes ');
define('TEXT_COST', 'Prix d\'achat');
define('TEXT_TOTAL_COST', 'Prix d\'achat Total : ');
define('TEXT_TOTAL_GROSS_PROFIT', 'Marge brute totale : ');
define('TEXT_GROSS_PROFIT', 'Bénéfice Brut');
define('TEXT_PROFIT_NET', 'Bénéfice Net');
define('TEXT_TOTAL_MARGIN_NET', 'Marge nette totale : ');
define('TEXT_TOTAL', 'Total ');
define('TEXT_QUERY_DATES', 'Choisissez les dates :');
define('TEXT_START_DATE', 'Date du Début');
define('TEXT_END_DATE', 'Date de Fin');
define('TITLE_STATUS', 'Etat des commandes : ');
define('TEXT_ALL_ORDERS', 'Toutes les commandes');

define('TITLE_AIDE_MARGIN_IMAGE','Notes');
define('TITLE_AIDE_MARGIN',' Notes Informatives');
define('TITLE_AIDE_MARGIN_DESCRIPTION','<li>Le calcul de marge se fait uniquement sur le prix du produit. Les options ne sont pas incluses dans le calcul');
?>