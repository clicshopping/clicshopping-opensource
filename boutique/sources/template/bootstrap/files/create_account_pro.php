<?php
/*
 * create_account_pro.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
 require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('create_account') ) {
    echo $OSCOM_MessageStack->get('create_account');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>

<div class="clearfix"></div>
<div class="contentContainer">
	<div class="contentText"><?php echo $OSCOM_Template->getBlocks('modules_create_account_pro'); ?></div>
</div>

<?php
  require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>
