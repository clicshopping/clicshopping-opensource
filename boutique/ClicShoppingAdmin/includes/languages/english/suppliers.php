<?php
/**
 * suppliers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Suppliers management');

define('TABLE_HEADING_SUPPLIERS', 'Supplier name');

define('TABLE_HEADING_MANAGER','Manager name');
define('TABLE_HEADING_PHONE','Telephone');
define('TABLE_HEADING_FAX','FAX');
define('TABLE_HEADING_EMAIL_ADDRESS','Email');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS','Statut');

define('TEXT_HEADING_NEW_SUPPLIER', 'New Supplier');
define('TEXT_HEADING_EDIT_SUPPLIERS', 'Edit supplier');
define('TEXT_HEADING_DELETE_SUPPLIERS', 'Delete supplier');
define('TAB_SUPPLIERS_NOTE','Notes');
define('TAB_VISUEL','Image');
define('TITLE_SUPPLIERS_GENERAL','General');
define('TITLE_AIDE_IMAGE', 'Note on the use of the images');

define('TEXT_SUPPLIERS', 'Suppliers:');
define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_PRODUCTS', 'Products:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');
define('TITLE_SUPPLIERS_IMAGE','Supplier image');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');
define('TEXT_PRODUCTS_IMAGE_VISUEL','Image preview');


define('TEXT_NEW_INTRO', 'Please fill out the following information for the new supplier');
define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_SUPPLIERS_NAME', 'Suppliers Name:');
define('TEXT_SUPPLIERS_NEW_IMAGE', 'New suppliers Image:');
define('TEXT_SUPPLIERS_IMAGE', 'Suppliers Image:');
define('TEXT_SUPPLIERS_DELETE_IMAGE', 'Delete image');
define('HELP_IMAGE_SUPPLIERS', '* Please do not insert more than one image in the section New image.<br /><br />* Note : For the Firefox navigator users, you must click twice on the button source to use the wisiwyg correctly.<br />If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock +)');
define('TEXT_SUPPLIERS_URL', 'Suppliers URL:');
define('TEXT_SUPPLIERS_MANAGER', 'Manager name :');
define('TEXT_SUPPLIERS_PHONE', 'Telephone :');
define('TEXT_SUPPLIERS_FAX', 'Fax :');
define('TEXT_SUPPLIERS_EMAIL_ADDRESS','Email address :');
define('TEXT_SUPPLIERS_ADDRESS', 'Address :');
define('TEXT_SUPPLIERS_SUBURB', 'Suburb :');
define('TEXT_SUPPLIERS_POSTCODE', 'Postcode : ');
define('TEXT_SUPPLIERS_CITY', 'City :');
define('TEXT_SUPPLIERS_STATES', 'States :');
define('TEXT_SUPPLIERS_COUNTRY', 'Country :');
define('TEXT_SUPPLIERS_NOTES','Others informations');

define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this supplier?');
define('TEXT_DELETE_IMAGE', 'Delete suppliers image?');
define('TEXT_DELETE_PRODUCTS', 'Delete products from this supplier? (including product reviews, products on special, upcoming products)');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>WARNING:</strong> There are %s products still linked to this supplier !');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: I can not write to this directory. Please set the right user permissions on: %s');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Error: Directory does not exist: %s');
?>