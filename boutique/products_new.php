<?php
/*
 * products_new.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('products_new'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('products_new.php'));

// create column list
  $define_list = array(
                      'MODULE_PRODUCTS_NEW_LIST_DATE_ADDED' => MODULE_PRODUCTS_NEW_LIST_DATE_ADDED,
                      'MODULE_PRODUCTS_NEW_LIST_PRICE' => MODULE_PRODUCTS_NEW_LIST_PRICE,
                      'MODULE_PRODUCTS_NEW_LIST_MODEL' => MODULE_PRODUCTS_NEW_LIST_MODEL,
                      'MODULE_PRODUCTS_NEW_LIST_WEIGHT' => MODULE_PRODUCTS_NEW_LIST_WEIGHT,
                      'MODULE_PRODUCTS_NEW_LIST_QUANTITY' => MODULE_PRODUCTS_NEW_LIST_QUANTITY,
                    );

  asort($define_list);

  $column_list = array();
  reset($define_list);
  while (list($key, $value) = each($define_list)) {
    if ($value > 0) $column_list[] = $key;
  }

  $select_column_list = '';

  for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
    switch ($column_list[$i]) {
      case 'MODULE_PRODUCTS_NEW_LIST_DATE_ADDED':
        $select_column_list .= 'p.products_date_added, ';
        break;
      case 'MODULE_PRODUCTS_NEW_LIST_PRICE':
        $select_column_list .= 'p.products_price, ';
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_MODEL':
        $select_column_list .= 'p.products_model, ';
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_WEIGHT':
        $select_column_list .= 'p.products_weight, ';
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_QUANTITY':
        $select_column_list .= 'p.products_quantity, ';
      break;
    }
  }

  if ( (!isset($_GET['sort'])) || (!preg_match('/^[1-8][ad]$/', $_GET['sort'])) || (substr($_GET['sort'], 0, 1) > sizeof($column_list)) ) {
    for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
      if ($column_list[$i] == 'MODULE_PRODUCTS_NEW_LIST_DATE_ADDED') {
        $_GET['sort'] = $i + 1 . 'a';
        $listing_sql .= "order by p.products_date_added DESC";
        break;
      }
    }
  } else {
    $sort_col = substr($_GET['sort'], 0 , 1);
    $sort_order = substr($_GET['sort'], 1);
    switch ($column_list[$sort_col-1]) {
      case 'MODULE_PRODUCTS_NEW_LIST_DATE_ADDED':
        $listing_sql .= " order by p.products_date_added " . ($sort_order == 'd' ? 'desc' : '');
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_PRICE':
        $listing_sql .= " order by p.products_price " . ($sort_order == 'd' ? 'desc' : '') . ", p.products_date_added DESC";
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_MODEL':
        $listing_sql .= " order by p.products_model " . ($sort_order == 'd' ? 'desc' : '') . ", p.products_date_added DESC";
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_QUANTITY':
        $listing_sql .= " order by p.products_quantity " . ($sort_order == 'd' ? 'desc' : '') . ", p.products_date_added DESC";
      break;
      case 'MODULE_PRODUCTS_NEW_LIST_WEIGHT':
        $listing_sql .= " order by p.products_weight " . ($sort_order == 'd' ? 'desc' : '') . ", p.products_date_added DESC";
      break;

    }
  }

  if ($OSCOM_Customer->getCustomersGroupID() != 0) {
// Mode B2B
    $products_query_raw = "select p.products_id,
                                  g.customers_group_price,
                                  g.price_group_view,
                                  g.orders_group_view
                           from products p  left join products_groups g on p.products_id = g.products_id
                           where p.products_status = '1'
                           and g.customers_group_id = '".(int)$OSCOM_Customer->getCustomersGroupID() ."'
                           and g.products_group_view = '1'
                           and p.products_archive = '0'
                           ". $listing_sql;
  } else {
// Mode normal

    $products_query_raw = "select p.products_id
                          from products p
                          where p.products_status = '1'
                          and p.products_view = '1'
                          and p.products_archive = '0'
                          ". $listing_sql;
  }

  require($OSCOM_Template->getTemplateFiles('products_new'));
