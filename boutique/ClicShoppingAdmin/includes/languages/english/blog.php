<?php
/**
 * blog.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Blog');

define('HEADING_TITLE_SEARCH', 'Search');
define('HEADING_TITLE_GOTO', 'Go To:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'Categories / Articles');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_CATEGORIES', 'Categories');
define('TABLE_HEADING_SORT_ORDER','Sort Order');

define('TABLE_HEADING_LAST_MODIFIED','last modification'); 
define('TABLE_HEADING_CREATED','Created / modified');
define('TABLE_HEADING_CUSTOMERS_GROUPS','Customer group');

define('TEXT_PRODUCTS_SEO_URL', 'Products SEO URL:');
define('TEXT_EDIT_CATEGORIES_SEO_URL', 'Category SEO URL:');
define('TEXT_CATEGORIES_SEO_URL', 'Category SEO URL:');

define('TEXT_CUSTOMERS_GROUP','Customer group :');

define('TEXT_NEW_PRODUCT', 'New Product in &quot;%s&quot;');
define('TEXT_CATEGORIES', 'Categories:');
define('TEXT_SUBCATEGORIES', 'Subcategories:');
define('TEXT_DIVERS_TITLE', 'Others');
define('TEXT_DESCRIPTION_CATEGORIES','Categories Description');

define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Product Meta Data');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Title');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_DESCRIPTION_SUMMARY','Summary');
define('TEXT_PRODUCTS_KEYWORDS', 'Keywords');
define('TEXT_PRODUCTS_TAG_PRODUCT','Products tag');
define('TEXT_PRODUCTS_TAG_BLOG','Blog tag');

define('TEXT_CREATE','create');
define('TEXT_PRODUCTS_AUTHOR','Article Author : ');

define('KEYWORDS_GOOGLE_TREND','Keywords Google trend');
define('ANALYSIS_GOOGLE_TOOL','Analysis Google Tool');


define('TEXT_PRODUCTS', 'Articles :');

define('TEXT_DATE_ADDED','Date Added     :');
define('TEXT_DATE_AVAILABLE','Date Available :');
define('TEXT_LAST_MODIFIED',  'Last Modified  :');

define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Please insert a new category or product in this level.');

define('TEXT_PRODUCT_DATE_ADDED', 'Product added our catalog on:');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'Product in stock on:');

define('TEXT_PRODUCTS_SORT_ORDER','Sort order');

define('TEXT_ALL_CUSTOMERS','Normal customers');
define('NORMAL_CUSTOMER','Normal customer');

define('TEXT_YES','yes');
define('TEXT_NO','no');
define('TEXT_CHOOSE','Select');
define('TEXT_PRODUCTS_TWITTER','Do you whish send this article on twitter ? ');
define('TEXT_NEW_BLOG_TWITTER','New: ');

define('TEXT_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_EDIT_CATEGORIES_ID', 'Category ID:');
define('TEXT_EDIT_CATEGORIES_NAME', 'Category Name:');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Category Image:');
define('TEXT_EDIT_SORT_ORDER', 'Sort Order:');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new category you wish to copy this product to');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Current Categories:');

define('TEXT_INFO_HEADING_NEW_CATEGORY', 'New Category');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Edit Category');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Delete Category');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Move Category');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Delete Product');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Move Product');
define('TEXT_INFO_HEADING_COPY_TO', 'Copy To');
define('TEXT_INFO_HEADING_ARCHIVE','Archiving Product');
define('TEXT_INFO_ARCHIVE_INTRO','Do you want to put this product in archive? <br /> <br /> This product can be reactivated in the archive section. <br /> <br /> <strong> Note: </strong> Archiving allows to remove your product catalog, but it does not deleted. Customers will have access to the product via search engines but they can not order it. This system is useful for referencing your site.');

define('TEXT_DELETE_CATEGORY_INTRO', 'Are you sure you want to delete this category?');
define('TEXT_DELETE_PRODUCT_INTRO', 'Are you sure you want to permanently delete this product in this category ?');

define('TEXT_DELETE_WARNING_CHILDS', '<strong>WARNING:</strong> There are %s (child-)categories still linked to this category!');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>WARNING:</strong> There are %s products still linked to this category!');

define('TEXT_MOVE_PRODUCTS_INTRO', 'Please select which category you wish <strong>%s</strong> to reside in');
define('TEXT_MOVE_CATEGORIES_INTRO', 'Please select which category you wish <strong>%s</strong> to reside in');
define('TEXT_MOVE', 'Move <strong>%s</strong> to:');

define('TEXT_NEW_CATEGORY_INTRO', 'Please fill out the following information for the new category');
define('TEXT_CATEGORIES_NAME', 'Category Name:');
define('TEXT_CATEGORIES_NAME_TITLE', 'Category Name');
define('TEXT_CATEGORIES_IMAGE', 'Category Image:');
define('TEXT_SORT_ORDER', 'Sort Order:');

define('TEXT_PRODUCTS_STATUS', 'Status:');

define('TEXT_PRODUCTS_NAME', 'Products Name');
define('TEXT_PRODUCTS_DESCRIPTION', 'Products Description');

define('TEXT_PRODUCTS_PRESENTATION', 'Presentation');

define('TEXT_USER_NAME','Create; / Modify by : ');

define('TEXT_CATEGORIES_IMAGE_TITLE','Categorie main image');
define('TEXT_CATEGORIES_IMAGE_VIGNETTE', 'Categorie Image label');
define('TEXT_CATEGORIES_IMAGE_VISUEL', 'Currently visible image on the categorie');
define('TEXT_CATEGORIES_DELETE_IMAGE', 'Not display this image on this categorie');

define('EMPTY_CATEGORY', 'Empty Category');

define('TEXT_HOW_TO_COPY', 'Copy Method:');
define('TEXT_COPY_AS_LINK', 'Link product');
define('TEXT_COPY_AS_DUPLICATE', 'Duplicate product');

define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Error: Can not link products in the same category.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: Catalog images directory is not writeable: ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Erreur: Vous ne pouvez pas bougez cette catégorie.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: Catalog images directory does not exist: ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Error : The categorie can not move in this under categorie.');

define('HELP_IMAGE_CATEGORIES', '<li>Please not insert more than one image in the section Categorie image label.</li><li>If you use the Firefox navigator, you must click twice on the button source to use the wisiwyg correctly.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock +)</li>');

define('TITLE_HELP_DESCRIPTION', 'Note on the use of the wysiwyg');
define('HELP_DESCRIPTION', '<li>To activate wysiwyg, you must click twice on Source button for Firefox users.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock more)</li>');


define('TITLE_HELP_SUBMIT', 'Note on the search engine optimization management');
define('HELP_SUBMIT', '<li>The system automatically manages the search engine optimization.<br />
<blockquote>It automatically included elements in the following order if the fields are not filled:<br />
Title : the name of your product (ex leather jacket)<br />	
Description: The product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Category: Category Product<br />
</blockquote></li>
<li>If you fill in the fields, the tags appear in the following way:<br />
<blockquote>
Title: The title of fields, the name of your product (ex leather jacket)<br />	
Description: The field description, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the fields keywords, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
</blockquote></li>
<li><strong>Please do not put too keywords (maximum 10)</strong> and establish consistency between each field to find the words the most important first</li>
');


define('TITLE_HELP_OPTIONS', 'Notes on the options');
define('HELP_OPTIONS', '<li>The options field on the products, there aren\'t included in the invoice.</li><li>If you wish create or delete a field, you must go in option field management in admin section.</li>
If the field is empty, it will not appear in the catalog.</li>');

define('TITLE_HELP_GENERAL', '	Briefing Notes');
define('HELP_GENERAL', '');
?>
