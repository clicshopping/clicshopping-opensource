<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Commentaires');
define('HEADING_TITLE', 'Commentaires de nos client concernant ce produit');
define('TEXT_OF_5_STARS', '%s sur 5 étoiles !');
define('REVIEWS_TEXT_READ_MORE', 'En savoir plus');
?>