<?php
/*
 * sitemap.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  
  if (MODE_VENTE_PRIVEE == 'false') {

    require($OSCOM_Template->GeTemplatetLanguageFiles('sitemap'));

    $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('sitemap.php'));

    require($OSCOM_Template->getTemplateFiles('sitemap'));
  }
