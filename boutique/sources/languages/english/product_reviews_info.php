<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Reviews');
define('HEADING_TITLE', ' %s Reviews');
define('SUB_TITLE_PRODUCT', 'Product:');
define('SUB_TITLE_FROM', 'From:');
define('SUB_TITLE_DATE', 'Date:');
define('SUB_TITLE_REVIEW', 'Review:');
define('SUB_TITLE_RATING', 'Rating:');
define('TEXT_OF_5_STARS', '%s of <span itemprop="bestRating">5</span> Stars!');
define('TEXT_CLICK_TO_ENLARGE', 'Click to enlarge');
?>