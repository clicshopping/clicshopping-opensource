<?php
  /**
   * cfm_modules_advanced_search.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_advanced_search {
    var $code = 'modules_advanced_search';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_ADVANCED_SEARCH_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_advanced_search() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_advanced_search/';
      $this->title = MODULE_CFG_MODULE_ADVANCED_SEARCH_MODULES_TITLE;
    }
  }
?>
