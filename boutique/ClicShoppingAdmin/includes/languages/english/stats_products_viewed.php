<?php
/**
 * stats_products_viewed.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */


define('HEADING_TITLE', 'Best Viewed Products');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_VIEWED', 'Viewed');
define('TABLE_HEADING_CLEAR','Clear');
?>