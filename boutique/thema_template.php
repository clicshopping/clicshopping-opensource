<?php
/*
 * thema_template.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
?>  

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>Template problem !</title>
    </head>
    <body>
      <div style="padding-top:100px;"></div>
      <div style="text-align:center;">le nom de ce template est incorrect. Veuillez changer le nom du template.</div>
      <div style="text-align:center;">This template name is incorrect. Please change your your template name.</div>
    </body>
  </html>