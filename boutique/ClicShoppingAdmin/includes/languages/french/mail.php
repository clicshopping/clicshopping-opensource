<?php
/**
 * mail.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Envoyer un courrier électronique aux clients');

define('TEXT_CUSTOMER', 'Client :');
define('TEXT_SUBJECT', 'Sujet :');
define('TEXT_FROM', 'De :');
define('TEXT_MESSAGE', 'Message :');
define('TEXT_SELECT_CUSTOMER', 'Sélectionnez le client');
define('TEXT_ALL_CUSTOMERS', 'Tous les clients');
define('TEXT_NEWSLETTER_CUSTOMERS', 'Tous les abonnés');
//define('TEXT_FOOTER','<U><font size="2">Avis de confidentialit� :</U><br />Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans �tre le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re�u ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.<br /><p>Conformément à la Loi dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit � la rectification de vos données personnelles � tout moment ou sur simple demande par email</font></div>');
define('NOTICE_EMAIL_SENT_TO', 'Courrier électronique envoyé  : %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'Erreur : Aucun client n\'a été sélectionné.');

define('TITLE_AIDE_IMAGE', 'Note sur l\'utilisation des images');
define('TITLE_HELP_DESCRIPTION', 'Note sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

?>
