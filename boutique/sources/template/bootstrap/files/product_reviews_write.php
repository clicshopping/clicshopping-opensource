<?php
/*
 * product_reviews_write.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require($OSCOM_Template->getTemplateHeaderFooter('header'));
// ne pas toucher au script ci dessous
// Do not touch the script below
?>
<script type="text/javascript"><!--
function checkForm() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  var review = document.product_reviews_write.review.value;

  if (review.length < <?php echo REVIEW_TEXT_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_REVIEW_TEXT; ?>";
    error = 1;
  }

  if ((document.product_reviews_write.rating[0].checked) || (document.product_reviews_write.rating[1].checked) || (document.product_reviews_write.rating[2].checked) || (document.product_reviews_write.rating[3].checked) || (document.product_reviews_write.rating[4].checked)) {
  } else {
    error_message = error_message + "<?php echo JS_REVIEW_RATING; ?>";
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}
//--></script>
<?php
  if ( $OSCOM_MessageStack->exists('review') ) {
    echo $OSCOM_MessageStack->get('review');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

// ---------------------- 
// ---  Form   -----  
// ---------------------- 
  
  echo osc_draw_form('product_reviews_write', osc_href_link('product_reviews_write.php', 'action=process&products_id=' . $OSCOM_Products->getId() ), 'post', 'onsubmit="return checkForm();"', true);
?>
<div class="contentContainer">
<?php
// ---------------------- 
// ---  image   -----  
// ---------------------- 
?>
  <div class="contentText">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="customers_first_name"><?php echo SUB_TITLE_FROM; ?></label>
        <div class="controls col-sm-10">
          <?php echo osc_output_string_protected($customer['customers_firstname'] . ' ' . $customer['customers_lastname']); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-2" for="inputReview"><?php echo SUB_TITLE_REVIEW; ?></label>
        <div class="controls col-sm-7">
          <?php echo osc_draw_textarea_field('review', 'soft', 150, 15, null, 'required aria-required="true" id="inputReview"') . '<br /><span class="pull-left">' . TEXT_NO_HTML . '</span>'; ?>
        </div>

        <div class="controls col-sm-3">
          <div class="pull-right" style="text-align: center;">
<?php
  echo $products_image;
// Affichage du bouton acheter selon les options cochees dans la fiche produit
  echo $products_name .'<br />';
  echo  TEXT_PRICE . ' ' . $product_price .'<br />';
  echo $min_order_quantity_products_display .'<br />';
  echo $button_small_view_details;
?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-xs-3"><?php echo SUB_TITLE_RATING; ?></label>
        <div class="col-xs-9">
          <div class="radio">
            <label>
              <?php echo osc_draw_radio_field('rating', '5') . osc_draw_stars(5, false) . ' ' . TEXT_GOOD; ?>
            </label>
          </div>
          <div class="radio">
            <label>
              <?php echo osc_draw_radio_field('rating', '4') . osc_draw_stars(4, false); ?>
            </label>
          </div>
          <div class="radio">
            <label>
              <?php echo osc_draw_radio_field('rating', '3') . osc_draw_stars(3, false); ?>
            </label>
          </div>
          <div class="radio">
            <label>
              <?php echo osc_draw_radio_field('rating', '2') . osc_draw_stars(2, false); ?>
            </label>
          </div>
          <div class="radio">
            <label>
              <?php echo osc_draw_radio_field('rating', '1', null, 'required aria-required="true"') . osc_draw_stars(1, false) . ' ' . TEXT_BAD; ?>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
// ---------------------- 
// ---  Button   -----  
// ---------------------- 
?>
  <div class="buttonSet">
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <td align="left"><?php echo osc_draw_button(IMAGE_BUTTON_BACK,null, osc_href_link('product_reviews.php', osc_get_all_get_params(array('reviews_id', 'action'))),'info'); ?></td>
      <td align="right">
        <span class="buttonAction"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success'); ?></span>
      </td>
    </table>
  </div>
</div>
</form>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>