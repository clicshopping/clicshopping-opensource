<?php
/**
 * whos_online.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Clients en ligne');

define('TABLE_HEADING_ONLINE', 'En ligne');
define('TABLE_HEADING_CUSTOMER_ID', 'ID');
define('TABLE_HEADING_FULL_NAME', 'Nom & Prénom');
define('TABLE_HEADING_IP_ADDRESS', 'Adresse IP');
define('TABLE_HEADING_ENTRY_TIME', 'Heure d\'arrivée');
define('TABLE_HEADING_LAST_CLICK', 'Dernier Clic');
define('TABLE_HEADING_LAST_PAGE_URL', 'Dernière URL');
define('TABLE_HEADING_USER_AGENT','Agent');
define('TABLE_HEADING_HTTP_REFERER','Référant');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_SHOPPING_CART', 'Panier utilisateur');
define('TEXT_SHOPPING_CART_SUBTOTAL', 'Sous-total');
define('TEXT_NUMBER_OF_CUSTOMERS', 'Actuellement il y a %s client(s) en ligne');
define('TABLE_HEADING_HTTP_REFERER', 'Reférée ');
define('TEXT_HTTP_REFERER_URL', 'HTTP URL Reférée ');
define('TEXT_HTTP_REFERER_FOUND', 'Oui');
define('TEXT_HTTP_REFERER_NOT_FOUND', 'non trouvée');
define('TEXT_STATUS_ACTIVE_CART', 'Actif avec panier');
define('TEXT_STATUS_ACTIVE_NOCART', 'Actif avec aucun panier');
define('TEXT_STATUS_INACTIVE_CART', 'Inactif avec panier');
define('TEXT_STATUS_INACTIVE_NOCART', 'Inactif avec aucun panier');
define('TEXT_STATUS_NO_SESSION_BOT', 'Bot avec aucune session active');
define('TEXT_STATUS_INACTIVE_BOT', 'Bot avec session inactive'); 
define('TEXT_STATUS_ACTIVE_BOT', 'Bot avec session active');
define('TABLE_HEADING_COUNTRY', 'Pays');
define('TABLE_HEADING_USER_SESSION', 'Session?');
define('TEXT_IN_SESSION', 'Oui');
define('TEXT_NO_SESSION', 'Non');

define('TEXT_OSCID', 'LORsid');
define('TEXT_PROFILE_DISPLAY', 'Affichage du profil');
define('TEXT_USER_AGENT', 'User Agent');
define('TEXT_ERROR', 'Erreur!');
define('TEXT_ADMIN', 'Administrateur');
define('TEXT_DUPLICATE_IPS', 'IP(s) dupliquée(s)');
define('TEXT_BOTS', 'Bots');
define('TEXT_ME', 'Moi meme!');
define('TEXT_ALL', 'Tous');

define('TEXT_ACTIVE_USER',' Client(s) Actif(s)');
define('TEXT_YOUR_IP_ADDRESS', 'Votre Addresse IP');
define('TEXT_SET_REFRESH_RATE', 'Délais de rafraichissement');
define('TEXT_NONE_', 'Aucun');
define('TEXT_CUSTOMERS', 'Cients');
define('TEXT_SHOW_BOTS', 'Montrer les Bots');
define('AZER_WHOSONLINE_WHOIS_URL', 'http://www.dnsstuff.com/tools/whois.ch?ip='); //for version 2.9 by azer - whois ip
define('TEXT_NOT_AVAILABLE', '   <strong>Note:</strong> N/A = IP non disponible'); //for version 2.9 by azer was missing
define('TEXT_LAST_REFRESH', 'Dernier refraichissement à'); //for version 2.9 by azer was missing
define('TEXT_EMPTY', 'Vide'); //for version 2.8 by azer was missing
define('TEXT_MY_IP_ADDRESS', 'Mon adresse IP '); //for version 2.8 by azer was missing
define('TABLE_HEADING_COUNTRY', 'Pays'); // azerc : 25oct05 for contrib whos_online with country and flag
define('TITLE_AIDE_WHOIS_DATE','Légende');
define('TEXT_REAL_CUSTOMERS','Clients');
define('TEXT_ACTIVE_CUSTOMERS', ' Client actifs');
?>