<?php
/**
 * configure.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: configure.php 
*/

// define our webserver variables
// FS = Filesystem (physical)
// WS = Webserver (virtual)
  define('HTTP_SERVER', '');
  define('HTTPS_SERVER', '');
  define('ENABLE_SSL', false);
  define('HTTP_COOKIE_DOMAIN', '');
  define('HTTPS_COOKIE_DOMAIN', '');
  define('HTTP_COOKIE_PATH', '');
  define('HTTPS_COOKIE_PATH', '');

  define('HTTP_CATALOG_SERVER', '');

  define('HTTPS_CATALOG_SERVER', '');
  define('ENABLE_SSL_CATALOG', 'false');

  define('DIR_FS_DOCUMENT_ROOT', '/');

  define('DIR_WS_HTTPS_ADMIN', '');
  define('DIR_WS_ADMIN', '');
  define('DIR_FS_ADMIN', '');

  define('DIR_WS_CATALOG', '');
  define('DIR_WS_HTTPS_CATALOG', '');
  define('DIR_FS_CATALOG', '');

  define('DIR_WS_IMAGES', 'images/');
  define('DIR_WS_ICONS', DIR_WS_IMAGES . 'icons/');

// fckeditor - Imanager
// template
  define('DIR_WS_SOURCES','sources/');
  define('DIR_WS_TEMPLATE', DIR_WS_SOURCES . 'template/');	
  define('DIR_WS_CATALOG_IMAGES', DIR_WS_CATALOG . DIR_WS_SOURCES . 'image/');
  define('DIR_FS_CATALOG_IMAGES', DIR_FS_CATALOG . DIR_WS_SOURCES . 'image/');

// fckeditor
  define('DIR_WS_CATALOG_PRODUCTS_IMAGES', DIR_WS_CATALOG . DIR_WS_SOURCES);
  define('DIR_FS_CATALOG_PRODUCTS_IMAGES', DIR_FS_CATALOG . DIR_WS_SOURCES);

  define('DIR_WS_INCLUDES', 'includes/');
  define('DIR_WS_BOXES', DIR_WS_INCLUDES . 'boxes/');
  define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
  define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
  define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');
  define('DIR_WS_LANGUAGES', DIR_WS_INCLUDES . 'languages/');
  define('DIR_WS_CATALOG_LANGUAGES', DIR_WS_CATALOG . DIR_WS_SOURCES . 'languages/');
  define('DIR_FS_CATALOG_LANGUAGES', DIR_FS_CATALOG . DIR_WS_SOURCES . 'languages/');
  define('DIR_FS_CATALOG_MODULES', DIR_FS_CATALOG . DIR_WS_MODULES);
  define('DIR_FS_BACKUP', DIR_FS_ADMIN . 'backups/');
  define('DIR_FS_DOWNLOAD', DIR_FS_CATALOG . DIR_WS_SOURCES . 'download/');
	
  // define our database connection
  define('DB_DRIVER', 'mysql_standard');  
  define('DB_DATABASE_TYPE', 'mysql');  
  define('DB_SERVER', 'localhost');
  define('DB_SERVER_USERNAME', '');
  define('DB_SERVER_PASSWORD', '');
  define('DB_DATABASE', '');
  define('DB_TABLE_PREFIX', '');  
  define('USE_PCONNECT', 'false');
  define('STORE_SESSIONS', 'mysql');

  define('DIR_FS_DOWNLOAD_PUBLIC', DIR_FS_CATALOG . DIR_WS_SOURCES . 'public/');
  define('DIR_FS_CACHE2', DIR_FS_CATALOG. 'cache/work/');

  define('CFG_TIME_ZONE', 'America/New_York');
?>
