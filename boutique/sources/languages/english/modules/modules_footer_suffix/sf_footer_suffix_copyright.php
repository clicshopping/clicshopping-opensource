<?php
/**
 * fo_footer_copyright.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 1.0
*/

  define('MODULES_FOOTER_SUFFIX_COPYRIGHT_TITLE', 'Do you want display  the footer copyright ?');
  define('MODULES_FOOTER_SUFFIX_COPYRIGHT_DESCRIPTION', 'Add a copyright on the shop footer ');
  define('MODULES_FOOTER_SUFFIX_COPYRIGHT_TEXT', '<br />Copyright© Juin 2010 - '. date('Y') .' '. STORE_NAME.'  All right reserved');
  define('MODULES_FOOTER_SUFFIX_TRADEMARK_TEXT', PROJECT_VERSION .' Registered® et Trademark™ <a href="http://www.e-imaginis.com" target="_blank">Imaginis</a> &amp; <a href="http://www.clicshopping.com" target="_blank">ClicShopping®</a>');
?>
