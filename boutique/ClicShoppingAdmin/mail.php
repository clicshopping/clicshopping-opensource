<?php
/**
 * mail.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/functions/template_email.php');

    $from = osc_db_prepare_input($_POST['from']);
    $subject = osc_db_prepare_input($_POST['subject']);
    $message_mail = osc_db_prepare_input($_POST['message']);

// email template       
    $template_email_signature =osc_get_template_email_signature($template_email_signature);
    $template_email_footer = osc_get_template_email_text_footer($template_email_footer);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if ( ($action == 'send_email_to_user') && isset($_POST['customers_email_address']) ) {  
  
    switch ($_POST['customers_email_address']) {
      case '***':

        $Qmail = $OSCOM_PDO->prepare('select customers_firstname,
                                             customers_lastname,
                                             customers_email_address
                                      from :table_customers
                                      and customers_email_validation = :customers_email_validation
                                     ');

        $Qmail->bindValue(':customers_email_validation',  0 );

        $Qmail->execute();

        $mail_sent_to = TEXT_ALL_CUSTOMERS;
      break;

      case '**D':

        $Qmail = $OSCOM_PDO->prepare('select customers_firstname,
                                             customers_lastname,
                                             customers_email_address
                                      from :table_customers
                                      where customers_newsletter = :customers_newsletter
                                      and customers_email_validation = :customers_email_validation
                                     ');

        $Qmail->bindInt(':customers_newsletter',  1 );
        $Qmail->bindValue(':customers_email_validation',  0 );

        $Qmail->execute();

        $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
        break;

// B2B
      case 'group':
        $QCustomersGroup = $OSCOM_PDO->prepare("select distinct customers_group_name,
                                                                customers_group_id
                                                from :table_customers_groups
                                                where customers_group_id != :customers_group_id
                                                order by customers_group_id
                                    ");
        $QCustomersGroup->bindValue(':customers_group_id', 0);
        $QCustomersGroup->execute();

// A analyse pb avec la B2B
        if ( $QCustomersGroup->rowCount() > 0 ) {
          while ($customers_group2 = $QCustomersGroup->fetch() ) {

            $Qmail = $OSCOM_PDO->prepare('select customers_firstname,
                                                customers_lastname,
                                                customers_email_address,
                                                customers_group_id
                                           from :table_customers
                                           where customers_group_id = customers_group_id
                                           and customers_email_validation = :customers_email_validation
                                        ');

             $Qmail->bindInt(':customers_group_id',  (int)$customers_group2['customers_group_id'] );
             $Qmail->bindValue(':customers_email_validation',  0 );

             $Qmail->execute();

            $mail_sent_to = 'B2B';
          }  
        }
     break;

     default:

        $customers_email_address = osc_db_prepare_input($_POST['customers_email_address']);
        $mail_sent_to = $_POST['customers_email_address'];
        $message_mail = osc_db_prepare_input($_POST['message']);
        $subject = osc_db_prepare_input($_POST['subject']);

         $Qmail = $OSCOM_PDO->prepare('select customers_id,
                                             customers_firstname,
                                             customers_lastname,
                                             customers_email_address
                                      from :table_customers
                                      where customers_email_address = :customers_email_address
                                      and customers_email_validation = :customers_email_validation
                                      ');

         $Qmail->bindValue(':customers_email_address',  osc_db_input($customers_email_address) );
         $Qmail->bindValue(':customers_email_validation',  0 );
         $Qmail->execute();


        $QmailSave = $OSCOM_PDO->prepare('select customers_id,
                                                 customers_firstname, 
                                                 customers_lastname, 
                                                 customers_email_address 
                                          from :table_customers
                                          where customers_email_address = :customers_email_address
                                          and customers_email_validation = :customers_email_validation
                                         ');
        $QmailSave->bindValue(':customers_email_address', osc_db_input($customers_email_address) );
        $QmailSave->bindValue(':customers_email_validation', 0 );
        $QmailSave->execute();

        $mail_save = $QmailSave->fetch();

        $customers_id = $mail_save['customers_id'];

        if (!empty($customers_id) && !empty($message_mail)) {
// notes clients
          $OSCOM_PDO->save('customers_notes', ['customers_id' => $customers_id,
                                               'customers_notes' =>  $subject ." <br />" . $message_mail,
                                               'customers_notes_date' => 'now()',
                                               'user_administrator' => osc_user_admin ($user_administrator),
                                              ]
                          );

        } //end !empty($customers_id)

      break;
    }

      $message = $message_mail . '<br />' . $template_email_signature . '<br />' . $template_email_footer;


  //Let's build a message object using the email class
      $mimemessage = new email(array('X-Mailer: ClicShopping'));

  // Envoie du mail avec gestion des images pour Fckeditor

      $message = str_replace('src="/', 'src="' . HTTP_CATALOG_SERVER . '/', $message);

      $mimemessage->add_html_fckeditor( $message);
      $mimemessage->build_message();

      while ($mail = $Qmail->fetch() ) {
        $mimemessage->send($mail['customers_firstname'] . ' ' . $mail['customers_lastname'], $mail['customers_email_address'], '', $from, html_entity_decode($subject));
      }

      osc_redirect(osc_href_link('mail.php', 'mail_sent_to=' . urlencode($mail_sent_to)));
    }

  if (isset($_GET['mail_sent_to'])) {
    $OSCOM_MessageStack->add(sprintf(NOTICE_EMAIL_SENT_TO, $_GET['mail_sent_to']), 'success');
  }

// dropdown
  $customers = array();
  $customers[] = array('id' => '', 'text' => TEXT_SELECT_CUSTOMER);
  $customers[] = array('id' => '***', 'text' => TEXT_ALL_CUSTOMERS);
  $customers[] = array('id' => '**D', 'text' => TEXT_NEWSLETTER_CUSTOMERS);
  //pb B2B
  //    $customers[] = array('id' => 'group', 'text' => TEXT_ALL_GROUPS);

  $QmailCustomers = $OSCOM_PDO->prepare('select customers_email_address,
                                                 customers_firstname,
                                                 customers_lastname
                                          from :table_customers
                                          where customers_email_validation = :customers_email_validation
                                          order by customers_lastname
                                        ');

  $QmailCustomers->bindValue(':customers_email_validation', '0' );
  $QmailCustomers->execute();

  while($customers_values = $QmailCustomers->fetch() ) {

    $customers[] = array('id' => $customers_values['customers_email_address'],
                          'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
  }

  require('includes/header.php');
?>
  <script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>
  <!-- body //-->

    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <div class="adminTitle">
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/mail.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (SEND_EMAILS =='true') {
?>
    <span class="pull-right"><?php echo osc_draw_form('mail', 'mail.php', 'action=send_email_to_user'); ?><?php echo osc_image_submit('button_send_mail.gif', IMAGE_SEND_EMAIL); ?></span>
<?php
  }
?>
        </div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

    <div class="row">
      <div class="col-md-12">
        <span class="col-md-2"><?php echo TEXT_CUSTOMER; ?></span>
        <span class="col-md-4"><?php echo  osc_draw_pull_down_menu('customers_email_address', $customers, (isset($_GET['customer']) ? $_GET['customer'] : '')); ?></span>
      </div>
    </div>
    <div class="spaceRow"></div>
    <div class="row">
      <div class="col-md-12">
        <span class="col-md-2"><?php echo TEXT_FROM; ?></span>
        <span class="col-md-4"><?php echo osc_draw_input_field('from', EMAIL_FROM); ?></span>
      </div>
    </div>
    <div class="spaceRow"></div>
    <div class="row">
      <div class="col-md-12">
        <span class="col-md-2"><?php echo TEXT_SUBJECT; ?></span>
        <span class="col-md-4"><?php echo osc_draw_input_field('subject'); ?></span>
      </div>
    </div>
    <div class="spaceRow"></div>
    <div class="row">
      <div class="col-md-12">
        <span class="col-md-2"><?php echo TEXT_MESSAGE; ?></span>
        <span class="col-md-8"><?php  echo osc_draw_textarea_ckeditor('message', 'soft', '750','300',  $nInfo->message);; ?></span>
      </div>
    </div>

    </form>

    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <div class="adminformAide" style="margin-left:10px;">
      <div class="row">
        <span class="col-md-12 adminformAideContent">
          <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', HELP_DESCRIPTION); ?>
          <strong><?php echo '&nbsp;' . TITLE_HELP_GENERAL; ?></strong>
        </span>
      </div>
      <div class="spaceRow"></div>
      <div class="row">
        <span class="col-md-12">
          <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
          <div class="modal fade" id="myModalWysiwyg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                </div>
                <div class="modal-body" style="text-align:center;">
                  <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                </div>
              </div>
            </div>
          </div>
        </span>
      </div>
    </div>
<?php
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
