<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Surveillance Produits');

define('HEADING_TITLE', 'Surveillance Produits');

define('MY_NOTIFICATIONS_TITLE', 'Mes surveillances produits');
define('MY_NOTIFICATIONS_DESCRIPTION', 'La liste de surveillance de produit vous permet de veiller sur les produits qui vous intéressent.<br /><br />Pour être informé de tous changements sur les produits, choisissez les <strong>suveillance globale des produits</strong>.');

define('GLOBAL_NOTIFICATIONS_TITLE', 'Surveillance globale des Produits');
define('GLOBAL_NOTIFICATIONS_DESCRIPTION', 'Recevoir des emails sur tous les produits disponibles.');

define('NOTIFICATIONS_TITLE', 'Surveillance produits');
define('NOTIFICATIONS_DESCRIPTION', 'Pour supprimer une surveillance de produit, décochez la case &agrave cocher du produit et cliquez sur Continuer.');
define('NOTIFICATIONS_NON_EXISTING', 'Il n\'y a actuellement aucun produit.<br /><br />Pour ajouter des produits &agrave votre liste de surveillance de produit, cliquez sur le lien surveillance produit disponible sur la page d\'information de produit détaillée..');

define('SUCCESS_NOTIFICATIONS_UPDATED', 'Votre demande a été mise &agrave jour.');
?>