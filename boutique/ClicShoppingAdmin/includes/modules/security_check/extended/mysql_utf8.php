<?php
  /*
   * mysql_uft8.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */


  class securityCheckExtended_mysql_utf8 {
    var $type = 'warning';
    var $has_doc = true;

    function securityCheckExtended_mysql_utf8() {

      include(DIR_FS_ADMIN . 'includes/languages/' . $_SESSION['language'] . '/modules/security_check/extended/mysql_utf8.php');
      $this->title = MODULE_SECURITY_CHECK_EXTENDED_MYSQL_UTF8_TITLE;
    }

    function pass() {
      global $OSCOM_PDO;
      $Qcheck = $OSCOM_PDO->prepare('show :table_table status');

      $Qcheck->execute();

      if ( $Qcheck->rowCount() ) {
        while ( $check = $Qcheck->fech() ) {
          if ( isset($check['Collation']) && ($check['Collation'] != 'utf8_unicode_ci') ) {
            return false;
          }
        }
      }

      return true;
    }

    function getMessage() {
      return '<a href="' . osc_href_link('database_tables.php') . '">' . MODULE_SECURITY_CHECK_EXTENDED_MYSQL_UTF8_ERROR . '</a>';
    }
  }
