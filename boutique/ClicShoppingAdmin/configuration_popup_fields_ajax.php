<?php
/**
 * configuration_popup_files_ajax.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


  require('includes/application_top.php');

  if (!empty($_POST['configuration_value'])) {

  $configuration_value = osc_db_prepare_input($_POST['configuration_value']);
  $cKey = osc_db_prepare_input($_GET['cKey']);

    $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                    set configuration_value = :configuration_value,
                                        last_modified = now()
                                    where configuration_key = :configuration_key
                                   ');
    $Qupdate->bindValue(':configuration_value', $configuration_value);
    $Qupdate->bindValue(':configuration_key', $cKey);
    $Qupdate->execute();

  //clear cache
    osc_cache_clear('configuration.cache');

    echo 'Success ';
  //  echo "From Server : ". json_encode($_POST)."<br />";
  } else {
    echo 'Error <br />';
  }
