<?php
/*
 * account_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_notifications'));

  $Qglobal = $OSCOM_PDO->prepare('select global_product_notifications 
                                 from :table_customers_info 
                                 where customers_info_id = :customers_info_id
                                ');
  $Qglobal->bindInt(':customers_info_id', $OSCOM_Customer->getID());
  $Qglobal->execute();

  $global = $Qglobal->fetch();

  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    if ( isset($_POST['product_global']) && is_numeric($_POST['product_global']) && in_array($_POST['product_global'], ['0', '1'])) {
      $product_global = (int)$_POST['product_global'];
    } else {
      $product_global = 0;
    }

    (array)$products = $_POST['products'];

    if ($product_global != $global['global_product_notifications']) {
      $product_global = (($global['global_product_notifications'] == '1') ? '0' : '1');

      $Qupdate = $OSCOM_PDO->prepare('update :table_customers_info 
                                     set global_product_notifications = :global_product_notifications 
                                     where customers_id = :customers_id
                                    ');
      $Qupdate->bindValue(':global_product_notifications', (int)$product_global);
      $Qupdate->bindInt(':customers_info_id', (int)$OSCOM_Customer->getID());
      $Qupdate->execute();

    } elseif (sizeof($products) > 0) {
      $products_parsed = array();

      foreach ($products as $value) {
        if (is_numeric($value) && !in_array($value, $products_parsed)) {
          $products_parsed[] = $value;
        }
      }

      if (sizeof($products_parsed) > 0) {

        $products_id_in = array_map(function($k) {
          return ':products_id_' . $k;
        }, array_keys($products_parsed));


        $Qcheck = $OSCOM_PDO->prepare('select products_id from :table_products_notifications 
                                      where customers_id = :customers_id 
                                      and products_id not in (:products_id) 
                                      limit 1
                                    ');

        $Qcheck->bindInt(':customers_id', $OSCOM_Customer->getID());

        foreach ($products_parsed as $k => $v) {
          $Qcheck->bindInt(':products_id_' . $k, $v);
        }

        $Qcheck->execute();

        if ( $Qcheck->fetch() !== false ) {
          $Qdelete = $OSCOM_PDO->prepare('delete from :table_products_notifications 
                                          where customers_id = :customers_id 
                                          and products_id not in (:products_id)');
          $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());

          foreach ($products_parsed as $k => $v) {
            $Qdelete->bindInt(':products_id_' . $k, $v);
          }
          
          $Qdelete->execute();
        }
      }
    } else {

      $Qcheck = $OSCOM_PDO->prepare('select customers_id 
                                    from :table_products_notifications 
                                    where customers_id = :customers_id 
                                    limit 1
                                    ');
      $Qcheck->bindInt(':customers_id', $OSCOM_Customer->getID());
      $Qcheck->execute();

      if ( $Qcheck->fetch() !== false ) {
        $Qdelete = $OSCOM_PDO->prepare('delete
                                        from :table_products_notifications 
                                        where customers_id = :customers_id
                                      ');
        $Qdelete->bindInt(':customers_id', $OSCOM_Customer->getID());
        $Qdelete->execute();  
      }
    }

    $OSCOM_MessageStack->addSuccess('account', SUCCESS_NOTIFICATIONS_UPDATED);

    osc_redirect(osc_href_link('account.php', '', 'SSL'));
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_notifications.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('account_notifications'));
