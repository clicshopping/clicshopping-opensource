<?php
/**
 * server_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Informations serveur');

define('TITLE_SERVER_HOST', 'H&ocirc;te :');
define('TITLE_SERVER_OS', 'Système d\'exploitation :');
define('TITLE_SERVER_DATE', 'Date du serveur :');
define('TITLE_SERVER_UP_TIME', 'Temps de fonctionnement :');
define('TITLE_HTTP_SERVER', 'Serveur HTTP :');
define('TITLE_PHP_VERSION', 'Version PHP :');
define('TITLE_ZEND_VERSION', 'Zend :');
define('TITLE_DATABASE_HOST', 'H&ocirc;te de la base de données :');
define('TITLE_DATABASE', 'Base de données :');
define('TITLE_DATABASE_DATE', 'Date de la base de données :');

// Onglet
define('SERVER_INFO', 'Information général sur le serveur');
define('SERVER_INFO_PHP', 'Information sur le serveur PHP');
define('TAB_INFO_SERVEUR', 'Information serveur');
define('TAB_INFO_PHP', 'Information PHP');
?>