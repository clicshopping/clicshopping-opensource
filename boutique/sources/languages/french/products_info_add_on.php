<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Mes actions ');
define('TEXT_SHARE', 'Partager dans mon réseau');
define('TEXT_EVALUATE_CUSTOMERS', 'Evaluation moyenne des clients du produit');
define('TEXT_INSERT_EVALUATION', 'Ce produit n\'a pas encore d\'évaluation, en proposer une');
define('TEXT_INSERT_NEWSLETTER', 'M\'abonner à la newsletter');
define('TEXT_SEND_A_FRIEND', 'Envoyer cet article à un ou une ami(e)');
define('TEXT_NOTIFICATIONS_NOTIFY', 'Surveiller les prix de ce produit');
?>