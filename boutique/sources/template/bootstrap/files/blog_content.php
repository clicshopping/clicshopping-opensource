<?php
/*
 * blog_content.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
  <div class="contentContainer">
    <div class="contentText">
      <div class="blogContent">
<?php
// ----------------------------------------------------------------//
//                      file not found                             //
// ----------------------------------------------------------------//
    if ($OSCOM_Blog->getBlogCount() < 1 || (is_null($id)) ) {
?>

      <div class="contentContainer">
        <div class="contentText" style="padding-top:20px;">
          <div class="alert alert-warning" style="text-align:center;">
            <h3><?php echo TEXT_BLOG_NOT_FOUND; ?></h3>
          </div>
          <div class="buttonSet pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, '', osc_href_link('index.php'),'success'); ?></div>
        </div>
      </div>
<!-- blog content not found  end -->
<?php
  } else {
// ------------------------------------------------------------
// ---- Display the blog content                           ----
// ------------------------------------------------------------
?>
        <div itemscope itemtype="http://schema.org/Blog">
           <?php echo $OSCOM_Template->getBlocks('modules_blog_content'); ?>
        </div>
<?php
  }
?>
      </div>
    </div>
  </div>
<?php   require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>