<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Faire connaitre à un ami');
define('HEADING_TITLE', 'Faire connaitre à un ami');

define('HEADING_TITLE_ERROR', 'Faire connaitre à un ami');

define('COPY_ANTISPAM', 'Recopier le code AntiSpam');

define('FORM_TITLE_CUSTOMER_DETAILS', 'Vous');
define('FORM_TITLE_FRIEND_DETAILS', 'Votre ami(e)');
define('FORM_TITLE_FRIEND_MESSAGE', 'Votre message');

define('FORM_FIELD_CUSTOMER_NAME', 'Votre nom :');
define('FORM_FIELD_CUSTOMER_EMAIL', 'Votre adresse électronique :');
define('FORM_FIELD_FRIEND_NAME', 'Le nom de votre ami(e) :');
define('FORM_FIELD_FRIEND_EMAIL', 'L\'adresse électronique de votre ami(e) :');

define('TEXT_EMAIL_SUCCESSFUL_SENT', 'Votre courrier électronique à propos de <strong>%s</strong> Ont été avec succès envoyé à <strong>%s</strong>.');
define('TEXT_EMAIL_SUBJECT', 'Votre ami %s vous recommande ce produit de %s');
define('TEXT_EMAIL_INTRO', 'Bonjour %s!' . "\n\n" . '<br /><br />Vote ami(e), %s, pense que vous pourriez être intéressés(és)  par %s de %s.<br /><br />');
define('TEXT_EMAIL_LINK', 'Pour voir le produit cliquez sur le lien ci-dessous ou copiez et collez le lien dans votre navigateur Internet :' . "\n\n" . '%s');
define('TEXT_EMAIL_SIGNATURE', 'Très cordialement,'. "\n" . '<strong>L\'équipe '.STORE_NAME.'</strong>'. "\n\n" . '
<u><font size="2">Avis de confidentialité :</u>' . "\n\n" . 'Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans être le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez reçu ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n\n" . '
<p>Conformément à la Loi dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</font>
</div>');

define('ERROR_TO_NAME', 'Erreur : Votre nom d\'ami(e) ne doit pas être vide.');
define('ERROR_TO_ADDRESS', 'Erreur : L\'adresse électronique de votre ami(e) doit être une adresse électronique valide.');
define('ERROR_FROM_NAME', 'Erreur : Votre nom ne doit pas être vide.');
define('ERROR_FROM_ADDRESS', 'Erreur : Votre adresse électronique doit être une adresse électronique valide.');
define('ERROR_TO_SPAM', 'Erreur : Vos codes concernant l\'antispam sont erronées');
define('ERROR_INVALID_PRODUCT', 'Ce produit produit n\'est pas disponible. Essayer encore une fois.');
define('ENTRY_NUMBER_EMAIL_CONFIRMATION', 'Combien font : ');
?>
