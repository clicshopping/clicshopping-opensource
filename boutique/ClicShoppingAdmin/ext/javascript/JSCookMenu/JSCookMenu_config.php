    <span class="menuJSCookMenu">
    <div id=myMenuID></div>
<script language="JavaScript" type="text/javascript"><!--
    var myMenu =
    [
      [null,'<?php echo MENU_HOME; ?>','index.php',null,'<?php echo MENU_HOME; ?>',
        ['<img class="seq1" src="images/menu/configuration_admin.gif" /><img class="seq2" src="images/menu/configuration_admin_over.gif" />','<?php echo MENU_HOME_ADMIN; ?>','index.php',null,''],
        ['<img class="seq1" src="images/menu/configuration.gif" /><img class="seq2" src="images/menu/configuration_over.gif" />','<?php echo MENU_HOME_STORE; ?>','<?php echo osc_catalog_href_link(); ?>','_blank',null],
      ],
<?php
// ----------------------
// Configuration
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_CONFIGURATION; ?>',null,null,'<?php echo BOX_HEADING_CONFIGURATION; ?>',
        ['<img class="seq1" src="images/menu/configuration.gif" /><img class="seq2" src="images/menu/configuration_over.gif" />','<?php echo MENU_CONFIGURATION_STORE; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/configuration_1.gif" /><img class="seq2" src="images/menu/configuration_1_over.gif" />','<?php echo MENU_CONFIGURATION_GENERAL; ?>','configuration.php?gID=1',null,''],
            ['<img class="seq1" src="images/menu/configuration_25.gif" /><img class="seq2" src="images/menu/configuration_25_over.gif" />','<?php echo MENU_CONFIGURATION_LEGACY; ?>','configuration.php?gID=25',null,''],
            ['<img class="seq1" src="images/menu/modules_payment.gif" /><img class="seq2" src="images/menu/modules_payment.gif" />','<?php echo MENU_CONFIGURATION_BANK; ?>','configuration.php?gID=2',null,''],
            ['<img class="seq1" src="images/menu/configuration_34.gif" /><img class="seq2" src="images/menu/configuration_34.gif" />','<?php echo MENU_CONFIGURATION_SEO; ?>','configuration.php?gID=34',null,''],
            ['<img class="seq1" src="images/menu/configuration_3.gif" /><img class="seq2" src="images/menu/configuration_3_over.gif" />','<?php echo MENU_CONFIGURATION_MAXIMUM; ?>','configuration.php?gID=3',null,''],
            ['<img class="seq1" src="images/menu/configuration_7.gif" /><img class="seq2" src="images/menu/configuration_7_over.gif" />','<?php echo MENU_CONFIGURATION_SHIPPING; ?>','configuration.php?gID=7',null,''],
            ['<img class="seq1" src="images/menu/configuration_9.gif" /><img class="seq2" src="images/menu/configuration_9_over.gif" />','<?php echo MENU_CONFIGURATION_STOCK; ?>','configuration.php?gID=9',null,''],
            ['<img class="seq1" src="images/menu/products_unit.png" /><img class="seq2" src="images/menu/products_unit.png" />','<?php echo MENU_CONFIGURATION_PRODUCTS_QUANTITY_UNIT; ?>','products_quantity_unit.php',null,''],
            ['<img class="seq1" src="images/menu/configuration_12.gif" /><img class="seq2" src="images/menu/configuration_12_over.gif" />','<?php echo MENU_CONFIGURATION_MAIL; ?>','configuration.php?gID=12',null,''],
            ['<img class="seq1" src="images/menu/configuration_13.gif" /><img class="seq2" src="images/menu/configuration_13_over.gif" />','<?php echo MENU_CONFIGURATION_DOWNLOAD; ?>','configuration.php?gID=13',null,''],
          ],
<?php
// ----------------------
// Configuration Administration
// ---------------------
?>
        ['<img class="seq1" src="images/menu/configuration_admin.gif" /><img class="seq2" src="images/menu/configuration_admin_over.gif" />','<?php echo MENU_CONFIGURATION_STORE_ADMINISTRATION; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/administrators.gif" /><img class="seq2" src="images/menu/administrators_over.gif" />','<?php echo MENU_CONFIGURATION_ADMINISTRATOR; ?>','administrators.php',null,''],
          ['<img class="seq1" src="images/menu/configuration_admin.gif" /><img class="seq2" src="images/menu/configuration_admin_over.gif" />','<?php echo BOX_MODULES_ADMIN_DASHBOARD; ?>','modules.php?set=dashboard',null,''],		
          ['<img class="seq1" src="images/menu/configuration_3.gif" /><img class="seq2" src="images/menu/configuration_3_over.gif" />','<?php echo MENU_CONFIGURATION_MAXIMUM; ?>','configuration.php?gID=21',null,''],
          ['<img class="seq1" src="images/menu/configuration_4.gif" /><img class="seq2" src="images/menu/configuration_4_over.gif" />','<?php echo MENU_CONFIGURATION_IMAGES; ?>','configuration.php?gID=23',null,''],
          ['<img class="seq1" src="images/menu/order_status_invoice.png" /><img class="seq2" src="images/menu/order_status_invoice.png" />','<?php echo MENU_PRINT_ORDERS_STATUS_INVOICE; ?>','orders_status_invoice.php',null,''],
          ['<img class="seq1" src="images/menu/tracking.png" /><img class="seq2" src="images/menu/tracking.png" />','<?php echo MENU_CONFIGURATION_ORDERS_STATUS_TRACKING; ?>','orders_status_tracking.php',null,''],
          ['<img class="seq1" src="images/menu/configuration_26.gif" /><img class="seq2" src="images/menu/configuration_26_over.gif" />','<?php echo MENU_EDIT_ORDERS_STATUS_INVOICE; ?>','configuration.php?gID=26',null,''],
          ['<img class="seq1" src="images/menu/orders_status.gif" /><img class="seq2" src="images/menu/orders_status_over.gif" />','<?php echo MENU_LOCALIZATION_ORDERS_STATUS; ?>','orders_status.php',null,''],
          ['<img class="seq1" src="images/menu/priceupdate.gif" /><img class="seq2" src="images/menu/priceupdate_over.gif" />','<?php echo MENU_CONFIGURATION_QUICK_UPDATES; ?>','configuration.php?gID=24',null,''],
          ['<img class="seq1" src="images/menu/configuration_36.png" /><img class="seq2" src="images/menu/configuration_36_over.png" />','<?php echo MENU_CONFIGURATION_EXPORT; ?>','configuration.php?gID=36',null,''],
          ['<img class="seq1" src="images/menu/configuration_44.png" /><img class="seq2" src="images/menu/configuration_44.png" />','<?php echo MENU_CONFIGURATION_WEBSERVICE; ?>','configuration.php?gID=44',null,''],
        ],
<?php
// ----------------------
// Configuration B2C
// ---------------------
?>
        ['<img class="seq1" src="images/menu/configuration_b2c.gif" /><img class="seq2" src="images/menu/configuration_b2c_over.gif" />','<?php echo MENU_CONFIGURATION_B2C; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/configuration_17.gif" /><img class="seq2" src="images/menu/configuration_17_over.gif" />','<?php echo MENU_CONFIGURATION_B2C_GESTION; ?>','configuration.php?gID=22',null,''],
          ['<img class="seq1" src="images/menu/configuration_5.gif" /><img class="seq2" src="images/menu/configuration_5_over.gif" />','<?php echo MENU_CONFIGURATION_CUSTOMERS; ?>','configuration.php?gID=5',null,''],
          ['<img class="seq1" src="images/menu/configuration_16.gif" /><img class="seq2" src="images/menu/configuration_16_over.gif" />','<?php echo MENU_CONFIGURATION_B2C_MINIMUM; ?>','configuration.php?gID=16',null,''],
        ],
<?php
// ----------------------
// Configuration B2B
// ---------------------

  if (MODE_B2B_B2C == 'true') {
?>
        ['<img class="seq1" src="images/menu/configuration_b2b.gif" /><img class="seq2" src="images/menu/configuration_b2b_over.gif" />','<?php echo MENU_CONFIGURATION_B2B; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/configuration_17.gif" /><img class="seq2" src="images/menu/configuration_17_over.gif" />','<?php echo MENU_CONFIGURATION_B2B_GESTION; ?>','configuration.php?gID=17',null,''],
          ['<img class="seq1" src="images/menu/configuration_18.gif" /><img class="seq2" src="images/menu/configuration_18_over.gif" />','<?php echo MENU_CONFIGURATION_B2B_CUSTOMERS; ?>','configuration.php?gID=18',null,''],
          ['<img class="seq1" src="images/menu/configuration_19.gif" /><img class="seq2" src="images/menu/configuration_19_over.gif" />','<?php echo MENU_CONFIGURATION_B2B_MINIMUM; ?>','configuration.php?gID=19',null,''],
          ['<img class="seq1" src="images/menu/configuration_20.gif" /><img class="seq2" src="images/menu/configuration_20_over.gif" />','<?php echo MENU_CONFIGURATION_B2B_MAXIMUM; ?>','configuration.php?gID=20',null,''],
        ],
<?php
  }
// ----------------------
// Modules
// ---------------------
?>
        ['<img class="seq1" src="images/menu/modules.gif" /><img class="seq2" src="images/menu/modules_over.gif" />','<?php echo MENU_CONFIGURATION_MODULES; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/cadenas.gif" /><img class="seq2" src="images/menu/cadenas.gif" />','<?php echo MENU_CONFIGURATION_MODULES_ACTION_RECORDER; ?>','modules.php?set=action_recorder',null,''],
          ['<img class="seq1" src="images/menu/modules_payment.gif" /><img class="seq2" src="images/menu/modules_payment_over.gif" />','<?php echo MENU_CONFIGURATION_MODULES_PAYMENT; ?>','modules.php?set=payment',null,''],
          ['<img class="seq1" src="images/menu/modules_shipping.gif" /><img class="seq2" src="images/menu/modules_shipping_over.gif" />','<?php echo MENU_CONFIGURATION_MODULES_SHIPPING; ?>','modules.php?set=shipping',null,''],
          ['<img class="seq1" src="images/menu/modules_order_total.gif" /><img class="seq2" src="images/menu/modules_order_total_over.gif" />','<?php echo MENU_CONFIGURATION_MODULES_ORDERTOTAL; ?>','modules.php?set=order_total',null,''],
          ['<img class="seq1" src="images/menu/hooks.png" /><img class="seq2" src="images/menu/hooks.png" />','<?php echo MENU_CONFIGURATION_MODULES_HOOKS; ?>','modules_hooks.php',null,''],
        ],
<?php
// ----------------------
// Referencement
// ---------------------
?>
        ['<img class="seq1" src="images/menu/referencement.gif" /><img class="seq2" src="images/menu/referencement.gif" />','<?php echo MENU_CONFIGURATION_REFERENCEMENT_STATS; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/referencement.gif" /><img class="seq2" src="images/menu/referencement.gif" />','<?php echo MENU_CONFIGURATION_REFERENCEMENT_META_DATA; ?>','page_submit.php',null,''],
          ['<img class="seq1" src="images/menu/configuration_42.gif" /><img class="seq2" src="images/menu/configuration_42.gif" />','<?php echo MENU_CONFIGURATION_MODULES_SOCIAL_BOOKMARKS; ?>','modules.php?set=social_bookmarks',null,''],
          ['<img class="seq1" src="images/menu/referencement.gif" /><img class="seq2" src="images/menu/referencement.gif" />','<?php echo MENU_CONFIGURATION_MODULES_HEADER_TAG; ?>','modules.php?set=header_tags',null,''],
        ],
<?php
// ----------------------
// Lieux et taxes
// ---------------------
?>
          ['<img class="seq1" src="images/menu/configuration_lieu_taxe.gif" /><img class="seq2" src="images/menu/configuration_lieu_taxe_over.gif" />','<?php echo BOX_HEADING_LOCATION_AND_TAXES; ?>',null,null,'<?php echo BOX_HEADING_LOCATION_AND_TAXES; ?>',
          ['<img class="seq1" src="images/menu/countries.gif" /><img class="seq2" src="images/menu/countries_over.gif" />','<?php echo MENU_TAXES_COUNTRIES; ?>','countries.php',null,''],
          ['<img class="seq1" src="images/menu/zones.gif" /><img class="seq2" src="images/menu/zones_over.gif" />','<?php echo MENU_TAXES_ZONES; ?>','zones.php',null,''],
        _cmSplit,
          ['<img class="seq1" src="images/menu/geo_zones.gif" /><img class="seq2" src="images/menu/geo_zones_over.gif" />','<?php echo MENU_TAXES_GEO_ZONE; ?>','geo_zones.php',null,''],
          ['<img class="seq1" src="images/menu/tax_classes.gif" /><img class="seq2" src="images/menu/tax_classes_over.gif" />','<?php echo MENU_TAXES_TAX_CLASSES; ?>','tax_classes.php',null,''],
          ['<img class="seq1" src="images/menu/tax_rates.gif" /><img class="seq2" src="images/menu/tax_rates_over.gif" />','<?php echo MENU_TAXES_TAX_RATES; ?>','tax_rates.php',null,''],
        ],
<?php
// ----------------------
// Divers
// ---------------------
?>				
        ['<img class="seq1" src="images/menu/configuration_localisation.gif" /><img class="seq2" src="images/menu/configuration_localisation_over.gif" />','<?php echo BOX_HEADING_LOCALIZATION; ?>',null,null,'<?php echo BOX_HEADING_LOCALIZATION; ?>',
          ['<img class="seq1" src="images/menu/currencies.gif" /><img class="seq2" src="images/menu/currencies_over.gif" />','<?php echo MENU_LOCALIZATION_CURRENCIES; ?>','currencies.php',null,''],
          ['<img class="seq1" src="images/menu/languages.gif" /><img class="seq2" src="images/menu/languages_over.gif" />','<?php echo MENU_LOCALIZATION_LANGUAGES; ?>','languages.php',null,''],
          ['<img class="seq1" src="images/menu/email.gif" /><img class="seq2" src="images/menu/email_over.gif" />','<?php echo MENU_TOOLS_TEMPLATE_EMAIL; ?>','<?php echo 'template_email.php'; ?>',null,''],
        ],
<?php
// ----------------------
// Session et cache
// ---------------------
?>
        ['<img class="seq1" src="images/menu/configuration_session.gif" /><img class="seq2" src="images/menu/configuration_session_over.gif" />','<?php echo MENU_CONFIGURATION_SESSION; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/configuration_11.gif" /><img class="seq2" src="images/menu/configuration_11_over.gif" />','<?php echo MENU_CONFIGURATION_SESSION_CACHE; ?>','configuration.php?gID=11',null,''],
                                        ['<img class="seq1" src="images/menu/cache.gif" /><img class="seq2" src="images/menu/cache_over.gif" />','<?php echo MENU_CONFIGURATION_SESSION_CONTROLE_CACHE; ?>','cache.php',null,''],
          ['<img class="seq1" src="images/menu/configuration_14.gif" /><img class="seq2" src="images/menu/configuration_14_over.gif" />','<?php echo MENU_CONFIGURATION_SESSION_GZIP; ?>','configuration.php?gID=14',null,''],
          ['<img class="seq1" src="images/menu/configuration_10.gif" /><img class="seq2" src="images/menu/configuration_10_over.gif" />','<?php echo MENU_CONFIGURATION_SESSION_LOGGING; ?>','configuration.php?gID=10',null,''],
          ['<img class="seq1" src="images/menu/configuration_15.gif" /><img class="seq2" src="images/menu/configuration_15_over.gif" />','<?php echo MENU_CONFIGURATION_SESSION_SESSION; ?>','configuration.php?gID=15',null,''],
        ],
      ],
<?php
// ----------------------
// Menu Catalogue
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_CATALOG; ?>',null,null,'<?php echo BOX_HEADING_CATALOG; ?>',
        ['<img class="seq1" src="images/menu/categorie_produit.gif" /><img class="seq2" src="images/menu/categorie_produit_over.gif" />','<?php echo MENU_CATALOG_CATEGORIES_PRODUCTS ?>','categories.php',null,null],
        _cmSplit,
        ['<img class="seq1" src="images/menu/priceupdate.gif" /><img class="seq2" src="images/menu/priceupdate_over.gif" />','<?php echo MENU_CATALOG_PRICE_UPDATE; ?>','quick_updates.php',null,''],
        ['<img class="seq1" src="images/menu/products_attributes.gif" /><img class="seq2" src="images/menu/products_attributes_over.gif" />','<?php echo MENU_CATALOG_PRODUCTS_ATTRIBUTES; ?>','products_attributes.php',null,''],
        ['<img class="seq1" src="images/menu/products_options.gif" /><img class="seq2" src="images/menu/products_options.gif" />','<?php echo MENU_CATALOG_PRODUCTS_EXTRA_FIELDS; ?>','products_extra_fields.php',null,''],
        ['<img class="seq1" src="images/menu/manufacturers.gif" /><img class="seq2" src="images/menu/manufacturers_over.gif" />','<?php echo MENU_CATALOG_MANUFACTURERS; ?>','manufacturers.php',null,''],
        ['<img class="seq1" src="images/menu/suppliers.gif" /><img class="seq2" src="images/menu/suppliers_over.gif" />','<?php echo MENU_CATALOG_SUPPLIERS; ?>','suppliers.php',null,''],
        ['<img class="seq1" src="images/menu/unpack.gif" /><img class="seq2" src="images/menu/unpack.gif" />','<?php echo MENU_CATALOG_PRODUCTS_ARCHIVES; ?>', 'products_archive.php',null,''],
      ],

<?php
// ----------------------
// Menu Clients
// ---------------------
?>

      [null,'<?php echo BOX_HEADING_CUSTOMERS; ?>',null,null,'<?php echo BOX_HEADING_CUSTOMERS; ?>',
        ['<img class="seq1" src="images/menu/orders.gif" /><img class="seq2" src="images/menu/orders_over.gif" />','<?php echo MENU_CUSTOMERS_ORDERS; ?>','orders.php',null,''],
        _cmSplit,
        ['<img class="seq1" src="images/menu/client.gif" /><img class="seq2" src="images/menu/client_over.gif" />','<?php echo MENU_CUSTOMERS; ?>','customers.php',null,''],
<?php
  if (MODE_B2B_B2C =='true') {
?>
        ['<img class="seq1" src="images/menu/client_attente.gif" /><img class="seq2" src="images/menu/client_attente_over.gif" />','<?php echo MENU_CUSTOMERS_MEMBERS_B2B; ?>','members.php',null,''],
        ['<img class="seq1" src="images/menu/group_client.gif" /><img class="seq2" src="images/menu/group_client_over.gif" />','<?php echo MENU_CUSTOMERS_GROUP_B2B; ?>','customers_groups.php',null,''],
        _cmSplit,
<?php
  }
?>
        ['<img class="seq1" src="images/menu/reviews.gif" /><img class="seq2" src="images/menu/reviews_over.gif" />','<?php echo MENU_CATALOG_REVIEWS; ?>','reviews.php',null,''],
        _cmSplit,
        ['<img class="seq1" src="images/menu/email.gif" /><img class="seq2" src="images/menu/email_over.gif" />','<?php echo MENU_CUSTOMERS_MAIL; ?>','mail.php',null,''],
        ['<img class="seq1" src="images/menu/customers_services.png" /><img class="seq2" src="images/menu/customers_services.png" />','<?php echo MENU_CONTACT_CUSTOMERS; ?>','contact_customers.php',null,''],
      ],
<?php
// ----------------------
// Menu Marketing
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_MARKETING; ?>',null,null,'<?php echo BOX_HEADING_MARKETING; ?>',
        ['<img class="seq1" src="images/menu/specials.gif" /><img class="seq2" src="images/menu/specials_over.gif" />','<?php echo MENU_CATALOG_SPECIALS; ?>','specials.php',null,''],
        ['<img class="seq1" src="images/menu/coupon.gif" /><img class="seq2" src="images/menu/coupon_over.gif" />','<?php echo MENU_MARKETING_DISCOUNT_COUPONS; ?>','discount_coupons.php',null,''],
        ['<img class="seq1" src="images/menu/products_heart.png" /><img class="seq2" src="images/menu/products_heart.png" />','<?php echo MENU_MARKETING_PRODUCTS_HEART; ?>','products_heart.php',null,''],
        ['<img class="seq1" src="images/menu/products_featured.png" /><img class="seq2" src="images/menu/products_featured.png" />','<?php echo MENU_MARKETING_PRODUCTS_FEATURED; ?>','products_featured.php',null,''],
        ['<img class="seq1" src="images/menu/products_related.gif" /><img class="seq2" src="images/menu/products_related.gif" />','<?php echo MENU_MARKETING_PRODUCTS_RELATED; ?>','products_related.php',null,''],
        _cmSplit,
        ['<img class="seq1" src="images/menu/banner_manager.gif" /><img class="seq2" src="images/menu/banner_manager_over.gif" />','<?php echo MENU_MARKETING_BANNER_MANAGER; ?>','banner_manager.php',null,''],
      ],
<?php
// ----------------------
// Menu communication
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_COMMUNICATION; ?>',null,null,'<?php echo BOX_HEADING_COMMUNICATION; ?>',
        ['<img class="seq1" src="images/menu/newsletters.gif" /><img class="seq2" src="images/menu/newsletters_over.gif" />','<?php echo MENU_COMMUNICATION_NEWSLETTER; ?>','newsletters.php',null,''],
        ['<img class="seq1" src="images/menu/page_manager.gif" /><img class="seq2" src="images/menu/page_manager_over.gif" />','<?php echo MENU_COMMUNICATION_PAGE_INFORMATION; ?>','<?php echo 'page_manager.php'; ?>',null,''],
        ['<img class="seq1" src="images/menu/blog.png" /><img class="seq2" src="images/menu/blog.png" />','<?php echo MENU_COMMUNICATION_BLOG; ?>','<?php echo 'blog.php'; ?>',null,''],
      ],
<?php
// ----------------------
// Menu Rapport
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_REPORTS; ?>',null,null,'<?php echo BOX_HEADING_REPORTS; ?>',
         ['<img class="seq1" src="images/menu/stats_products.png" /><img class="seq2" src="images/menu/stats_products.png" />','<?php echo MENU_REPORTS_STATS; ?>',null,null,'',
           ['<img class="seq1" src="images/menu/stats_products_viewed.gif" /><img class="seq2" src="images/menu/stats_products_viewed_over.gif" />','<?php echo MENU_REPORTS_PRODUCTS_VIEWED; ?>','stats_products_viewed.php',null,''],
           ['<img class="seq1" src="images/menu/stats_products_purchased.gif" /><img class="seq2" src="images/menu/stats_products_purchased_over.gif" />','<?php echo MENU_REPORTS_PRODUCTS_PURCHASED; ?>','stats_products_no_purchased.php',null,''],
           ['<img class="seq1" src="images/menu/stats_products_viewed.gif" /><img class="seq2" src="images/menu/stats_products_viewed_over.gif" />','<?php echo MENU_REPORTS_PRODUCTS_NO_VIEWED; ?>','stats_products_no_viewed.php',null,''],
           ['<img class="seq1" src="images/menu/stats_products_purchased.gif" /><img class="seq2" src="images/menu/stats_products_purchased_over.gif" />','<?php echo MENU_REPORTS_PRODUCTS_NO_PURCHASED; ?>','stats_products_no_purchased.php',null,''],
         ],
         ['<img class="seq1" src="images/menu/stats_financial.png" /><img class="seq2" src="images/menu/stats_financial.png" />','<?php echo MENU_REPORTS_STATS_FINANCIAL_MANAGEMENT; ?>',null,null,'',
<?php
  if (MODE_B2B_B2C =='true') {
?>
         ['<img class="seq1" src="images/menu/margin_report.png" /><img class="seq2" src="images/menu/margin_report.png" />','<?php echo MENU_REPORTS_MARGIN_REPORT; ?>','stats_margin_report.php',null,''],
<?php
  }
?>
         ['<img class="seq1" src="images/menu/coupon.gif" /><img class="seq2" src="images/menu/coupon_over.gif" />','<?php echo MENU_REPORTS_DISCOUNT; ?>','<?php echo 'stats_discount_coupons.php'; ?>',null,''],
         ['<img class="seq1" src="images/menu/stats_customers.gif" /><img class="seq2" src="images/menu/stats_customers_over.gif" />','<?php echo MENU_REPORTS_ORDERS_TOTAL; ?>','stats_customers.php',null,''],
       ],

       ['<img class="seq1" src="images/menu/stats_notification.png" /><img class="seq2" src="images/menu/stats_notification.png" />','<?php echo MENU_REPORTS_STATS_PRODUCTS_MANAGEMENT; ?>',null,null,'',
         ['<img class="seq1" src="images/menu/categorie_produit.gif" /><img class="seq2" src="images/menu/categorie_produit.gif" />','<?php echo MENU_REPORTS_PRODUCTS_NOTIFICATIONS; ?>','stats_products_notification.php',null,''],
         ['<img class="seq1" src="images/menu/configuration_9.gif" /><img class="seq2" src="images/menu/configuration_9.gif" />','<?php echo MENU_REPORTS_PRODUCTS_LOW_STOCK; ?>','stats_low_stock.php',null,''],
         ['<img class="seq1" src="images/menu/products_expected.gif" /><img class="seq2" src="images/menu/products_expected_over.gif" />','<?php echo MENU_REPORTS_PRODUCTS_EXPECTED; ?>','products_expected.php',null,''],
<?php
  if (MODE_B2B_B2C =='true') {
?>
             ['<img class="seq1" src="images/menu/wharehouse.png" /><img class="seq2" src="images/menu/wharehouse.png" />','<?php echo MENU_REPORTS_PRODUCTS_WHAREHOUSE; ?>','stats_products_wharehouse.php',null,''],
<?php
  }
?>
             ],
            _cmSplit,

          ['<img class="seq1" src="images/menu/suppliers.gif" /><img class="seq2" src="images/menu/suppliers_over.gif" />','<?php echo MENU_REPORTS_SUPPLIERS; ?>','<?php echo 'stats_suppliers.php'; ?>',null,''],
          ['<img class="seq1" src="images/menu/newsletters.gif" /><img class="seq2" src="images/menu/newsletters.gif" />','<?php echo MENU_REPORTS_NEWSLETTER_NO_ACCOUNT; ?>','<?php echo 'stats_newsletter_no_account.php'; ?>',null,''],
          ['<img class="seq1" src="images/menu/stats_email.png" /><img class="seq2" src="images/menu/stats_email.png" />','<?php echo MENU_REPORTS_EMAIL_VALIDATION; ?>','<?php echo 'stats_email_validation.php'; ?>',null,''],
      ],
<?php
// ----------------------
// Menu Design
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_DESIGN; ?>',null,null,'<?php echo BOX_HEADING_DESIGN; ?>',
             ['<img class="seq1" src="images/menu/configuration_43.gif" /><img class="seq2" src="images/menu/configuration_43.gif" />','<?php echo MENU_CONFIGURATION_DESIGN; ?>', null, null, '',
             ['<img class="seq1" src="images/menu/configuration_43.gif" /><img class="seq2" src="images/menu/configuration_43.gif" />','<?php echo MENU_CONFIGURATION_DESIGN; ?>','configuration.php?gID=43',null,''],
             ['<img class="seq1" src="images/menu/configuration_4.gif" /><img class="seq2" src="images/menu/configuration_4_over.gif" />','<?php echo MENU_CONFIGURATION_IMAGES; ?>','configuration.php?gID=4',null,''],
         ],
        _cmSplit,
          ['<img class="seq1" src="images/menu/configuration_session.gif" /><img class="seq2" src="images/menu/configuration_session.gif" />','<?php echo MENU_DESIGN_LISTING; ?>',null,null,'',
            ['<img class="seq1" src="images/menu/listing_products_new.gif" /><img class="seq2" src="images/menu/listing_products_new.gif" />','<?php echo MENU_DESIGN_PRODUCTS_NEW; ?>','modules.php?set=modules_products_new',null,''],
            ['<img class="seq1" src="images/menu/products_heart.png" /><img class="seq2" src="images/menu/products_heart.png" />','<?php echo MENU_DESIGN_PRODUCTS_HEART; ?>','modules.php?set=modules_products_heart',null,''],
            ['<img class="seq1" src="images/menu/products_featured.png" /><img class="seq2" src="images/menu/products_featured.png" />','<?php echo MENU_DESIGN_PRODUCTS_FEATURED; ?>','modules.php?set=modules_products_featured',null,''],
            ['<img class="seq1" src="images/menu/specials.gif" /><img class="seq2" src="images/menu/specials.gif" />','<?php echo MENU_DESIGN_PRODUCTS_SPECIAL; ?>','modules.php?set=modules_products_special',null,''],
            ['<img class="seq1" src="images/menu/configuration_session.gif" /><img class="seq2" src="images/menu/configuration_session.gif" />','<?php echo MENU_DESIGN_PRODUCTS_LISTING; ?>',null,null,'',
              ['<img class="seq1" src="images/menu/products_listing.gif" /><img class="seq2" src="images/menu/products_listing.gif" />','<?php echo MENU_DESIGN_PRODUCTS_LISTING; ?>','modules.php?set=modules_products_listing',null,''],
              ['<img class="seq1" src="images/menu/configuration_8.png" /><img class="seq2" src="images/menu/configuration_8.png" />','<?php echo MENU_DESIGN_PRODUCTS_LISTING_SORT_ORDER; ?>','configuration.php?gID=8',null,''],
            ],
          ],
          ['<img class="seq1" src="images/menu/page_manager.gif" /><img class="seq2" src="images/menu/page_manager.gif" />','<?php echo MENU_DESIGN_INDEX; ?>',null,null,'',
              ['<img class="seq1" src="images/menu/page_manager.gif" /><img class="seq2" src="images/menu/page_manager.gif" />','<?php echo MENU_DESIGN_NEW; ?>','modules.php?set=modules_front_page',null,''],
              ['<img class="seq1" src="images/menu/categorie.gif" /><img class="seq2" src="images/menu/categorie.gif" />','<?php echo MENU_DESIGN_CATEGORIES; ?>','modules.php?set=modules_index_categories',null,''],
          ],
          ['<img class="seq1" src="images/menu/client.gif" /><img class="seq2" src="images/menu/client_over.gif" />','<?php echo MENU_DESIGN_CUSTOMERS; ?>',null,null,'',
             ['<img class="seq1" src="images/menu/client.gif" /><img class="seq2" src="images/menu/client.gif" />','<?php echo MENU_DESIGN_CREATE_ACCOUNT; ?>','modules.php?set=modules_create_account',null,''],
             ['<img class="seq1" src="images/menu/whos_online.gif" /><img class="seq2" src="images/menu/whos_online.gif" />','<?php echo MENU_DESIGN_CREATE_ACCOUNT_PRO; ?>','modules.php?set=modules_create_account_pro',null,''],
             ['<img class="seq1" src="images/menu/client.gif" /><img class="seq2" src="images/menu/client_over.gif" />','<?php echo MENU_DESIGN_LOGIN_CUSTOMERS; ?>','modules.php?set=modules_login',null,''],
             ['<img class="seq1" src="images/menu/whos_online.gif" /><img class="seq2" src="images/menu/whos_online_over.gif" />','<?php echo MENU_DESIGN_ACCOUNT_CUSTOMERS; ?>','modules.php?set=modules_account_customers',null,''],
          ],
          ['<img class="seq1" src="images/menu/order_process.png" /><img class="seq2" src="images/menu/order_process.png" />','<?php echo MENU_DESIGN_PAYMENT_PROCESS; ?>',null,null,'',
             ['<img class="seq1" src="images/menu/shopping_cart.png" /><img class="seq2" src="images/menu/shopping_cart.png" />','<?php echo MENU_DESIGN_SHOPPING_CART; ?>','modules.php?set=modules_shopping_cart',null,''],
             ['<img class="seq1" src="images/menu/modules_shipping.gif" /><img class="seq2" src="images/menu/modules_shipping_over.gif" />','<?php echo MENU_DESIGN_SHIPPING; ?>','modules.php?set=modules_checkout_shipping',null,''],
             ['<img class="seq1" src="images/menu/modules_payment.gif" /><img class="seq2" src="images/menu/modules_payment_over.gif" />','<?php echo MENU_DESIGN_PAYMENT; ?>','modules.php?set=modules_checkout_payment',null,''],
             ['<img class="seq1" src="images/menu/modules_checkout_confirmation.png" /><img class="seq2" src="images/menu/modules_checkout_confirmation.png" />','<?php echo MENU_DESIGN_CONFIRMATION; ?>','modules.php?set=modules_checkout_confirmation',null,''],
             ['<img class="seq1" src="images/menu/modules_checkout_sucess.png" /><img class="seq2" src="images/menu/modules_checkout_sucess.png" />','<?php echo MENU_DESIGN_SUCESS; ?>','modules.php?set=modules_checkout_success',null,''],
          ],
          ['<img class="seq1" src="images/menu/categorie_produit.gif" /><img class="seq2" src="images/menu/categorie_produit.gif" />','<?php echo MENU_DESIGN_PRODUCTS_INFO; ?>','modules.php?set=modules_products_info',null,''],

          ['<img class="seq1" src="images/menu/communication.png" /><img class="seq2" src="images/menu/communication.png" />','<?php echo MENU_DESIGN_COMMUNICATION; ?>',null,null,'',
                ['<img class="seq1" src="images/menu/email.gif" /><img class="seq2" src="images/menu/email.gif" />','<?php echo MENU_DESIGN_CONTACT_US; ?>','modules.php?set=modules_contact_us',null,''],
                ['<img class="seq1" src="images/menu/blog.png" /><img class="seq2" src="images/menu/blog.png" />','<?php echo MENU_DESIGN_BLOG; ?>','modules.php?set=modules_blog',null,''],
                ['<img class="seq1" src="images/menu/blog_content.png" /><img class="seq2" src="images/menu/blog_content.png" />','<?php echo MENU_DESIGN_BLOG_CONTENT; ?>','modules.php?set=modules_blog_content',null,''],
                ['<img class="seq1" src="images/menu/reviews.gif" /><img class="seq2" src="images/menu/reviews_over.gif" />','<?php echo MENU_DESIGN_COMMENTS; ?>','configuration.php?gID=32',null,''],
           ],
          ['<img class="seq1" src="images/menu/miscellaneous.png" /><img class="seq2" src="images/menu/miscellaneous.png" />','<?php echo MENU_DESIGN_MISCELLANEOUS; ?>',null,null,'',
                ['<img class="seq1" src="images/menu/advanced_search.png" /><img class="seq2" src="images/menu/advanced_search.png" />','<?php echo MENU_DESIGN_ADVANCED_SEARCH; ?>','modules.php?set=modules_advanced_search',null,''],
                ['<img class="seq1" src="images/menu/sitemap.png" /><img class="seq2" src="images/menu/sitemap.png" />','<?php echo MENU_DESIGN_SITEMAP; ?>','modules.php?set=modules_sitemap',null,''],
           ],
        _cmSplit,
          ['<img class="seq1" src="images/menu/layout.png" /><img class="seq2" src="images/menu/layout.png" />','<?php echo MENU_DESIGN_LAYOUT; ?>',null,null,'',
                 ['<img class="seq1" src="images/menu/blockmenu.gif" /><img class="seq2" src="images/menu/blockmenu.gif" />','<?php echo MENU_DESIGN_BOXES; ?>','modules.php?set=modules_boxes',null,''],
                 ['<img class="seq1" src="images/menu/header.png" /><img class="seq2" src="images/menu/header.png" />','<?php echo MENU_DESIGN_HEADER; ?>','modules.php?set=modules_header',null,''],
                 ['<img class="seq1" src="images/menu/footer.png" /><img class="seq2" src="images/menu/footer.png" />','<?php echo MENU_DESIGN_FOOTER; ?>','modules.php?set=modules_footer',null,''],
          ],
      ],
<?php
// ----------------------
// Menu Outils
// ---------------------
?>
      [null,'<?php echo BOX_HEADING_TOOLS; ?>',null,null,'<?php echo BOX_HEADING_TOOLS; ?>',
        ['<img class="seq1" src="images/menu/backup.gif" /><img class="seq2" src="images/menu/backup_over.gif" />','<?php echo MENU_TOOLS_BACKUP; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/backup.gif" /><img class="seq2" src="images/menu/backup_over.gif" />','<?php echo MENU_TOOLS_BACKUP; ?>','backup.php',null,''],
          ['<img class="seq1" src="images/menu/database_analyse.png" /><img class="seq2" src="images/menu/database_analyse.png" />','<?php echo MENU_TOOLS_DATABASE_TABLE; ?>','database_tables.php',null,''],
        ],
        ['<img class="seq1" src="images/menu/desktop_enhancements.png" /><img class="seq2" src="images/menu/desktop_enhancements.png" />','<?php echo MENU_TOOLS_MODULE_INSTALLATION; ?>','modules_upload.php',null,''],
        ['<img class="seq1" src="images/menu/editor_html.png" /><img class="seq2" src="images/menu/editor_html.png" />','<?php echo MENU_TOOLS_EDITOR_HTML; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/css.png" /><img class="seq2" src="images/menu/css.png" />','<?php echo MENU_TOOLS_EDIT_CSS; ?>','edit_css.php',null,''],
          ['<img class="seq1" src="images/menu/edit_html.png" /><img class="seq2" src="images/menu/edit_html.png" />','<?php echo MENU_TOOLS_EDIT_HTML; ?>','edit_html.php',null,''],
          ['<img class="seq1" src="images/menu/edit_template.png" /><img class="seq2" src="images/menu/edit_template.png" />','<?php echo MENU_TOOLS_EDIT_TEMPLATE_PRODUCTS; ?>','edit_template_products.php',null,''],
        ],
        ['<img class="seq1" src="images/menu/configuration_36.png" /><img class="seq2" src="images/menu/configuration_36_over.png" />','<?php echo MENU_TOOLS_EXPORT; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/comparison_export.gif" /><img class="seq2" src="images/menu/comparison_export.gif" />','<?php echo MENU_TOOLS_EXPORT_COMPARATEUR; ?>','export_price_comparison.php',null,''],
          ['<img class="seq1" src="images/menu/export.png" /><img class="seq2" src="images/menu/export_over.png" />','<?php echo MENU_TOOLS_EXPORT_DATAS; ?>','export_data.php',null,''],
          ['<img class="seq1" src="images/menu/import.png" /><img class="seq2" src="images/menu/import_over.png" />','<?php echo MENU_TOOLS_IMPORT_DATAS; ?>','import.php',null,''],
        ],
        ['<img class="seq1" src="images/menu/server_info.gif" /><img class="seq2" src="images/menu/server_info_over.gif" />','<?php echo MENU_TOOLS_SECURITY; ?>',null,null,'',
          ['<img class="seq1" src="images/menu/server_info.gif" /><img class="seq2" src="images/menu/server_info_over.gif" />','<?php echo MENU_TOOLS_SECURITY_SERVER_INFO; ?>','server_info.php',null,''],
          ['<img class="seq1" src="images/menu/cybermarketing.gif" /><img class="seq2" src="images/menu/cybermarketing.gif" />','<?php echo MENU_TOOLS_SECURITY_CLICSHOPPING; ?>','security_checks.php',null,''],
          ['<img class="seq1" src="images/menu/cadenas.gif" /><img class="seq2" src="images/menu/cadenas.gif" />','<?php echo MENU_TOOLS_SECURITY_RECORDER; ?>','action_recorder.php',null,''],
        ],
        ['<img class="seq1" src="images/menu/whos_online.gif" /><img class="seq2" src="images/menu/whos_online_over.gif" />','<?php echo MENU_TOOLS_WHOS_ONLINE; ?>','whos_online.php',null,''],
      ],
<?php
// ----------------------
// Menu Market Place
// ---------------------
?>
      [null,'<?php echo MENU_MARKETPLACE; ?>',null,null,'<?php echo MENU_MARKETPLACE; ?>',
        ['<img class="seq1" src="images/menu/market_place.png" /><img class="seq2" src="images/menu/market_place.png" />','<?php echo MENU_MARKETPLACE; ?>','http://www.clicshopping.org','_blank',null],
      ],
<?php

// ----------------------
// Menu Aide
// ---------------------
?>
      [null,'<?php echo MENU_HELP; ?>',null,null,'<?php echo BOX_HEADING_HELP; ?>',
        ['<img class="seq1" src="images/menu/support.gif" /><img class="seq2" src="images/menu/support.gif" />','<?php echo MENU_HELP_IMAGINIS; ?>','http://www.clicshopping.net','_blank',null],
        ['<img class="seq1" src="images/menu/market_place.png" /><img class="seq2" src="images/menu/market_place.png" />','<?php echo MENU_HELP_CREDIT; ?>','credit.php',null,''],
      ],
    ];
cmDraw ('myMenuID', myMenu, 'hbr', cmThemeOffice2003, 'ThemeOffice2003');
--></script>  
</span>
