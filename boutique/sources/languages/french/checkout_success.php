<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Confirmation');
define('NAVBAR_TITLE_2', 'Succès');

define('HEADING_TITLE', 'Merci de nous avoir fait confiance.');


?>