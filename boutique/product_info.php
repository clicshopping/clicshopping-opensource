<?php
/*
 * product_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!isset($_GET['products_id'])) {
    osc_redirect(osc_href_link('index.php') );
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('product_info'));

  $id = (int)$OSCOM_Products->getId();

  if ( $OSCOM_Products->getProductsGroupView() == '1' ||  $OSCOM_Products->getProductsView() == '1') { // Fin condition tout en bas
    if (!empty($products_tag) &&  $spider_flag === false ) {
      $OSCOM_Products->countUpdateProductsView();
    }

    if ((USE_CACHE == 'true') && empty($SID)) {
      if (MODULE_PRODUCTS_INFO_CROSS_SELL_STATUS == 'True') {
        echo osc_cache_products_cross_sell(3600);
      }
      if (MODULE_PRODUCTS_INFO_RELATED_STATUS == 'True') {
        echo osc_cache_products_related(3600);
      }
      if (MODULE_PRODUCTS_INFO_PURCHASED_STATUS == 'True') {
        echo osc_cache_also_purchased(3600);
      }
    }
  }// end product group view

  require($OSCOM_Template->getTemplateFiles('product_info'));
