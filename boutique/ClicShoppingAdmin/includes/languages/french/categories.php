<?php
/**
 * categories.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Catégories / Produits');

define('HEADER_TAGS_CATEGORY_TITLE','Titre catégorie');
define('HEADER_TAGS_CATEGORY_DESCRIPTION','Description');
define('HEADER_TAGS_CATEGORY_KEYWORDS','Mots Clefs');
define('HEADER_TAGS_CATEGORY_REFEFRENCEMENT','Méta données pour la catégorie');

define('HEADING_TITLE_SEARCH', 'Rechercher');
define('HEADING_TITLE_GOTO', 'Aller à :');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'Catégories / Produits');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_CATEGORIES', 'Catégories');
define('TABLE_HEADING_SORT_ORDER','Ordre de tri');

define('TABLE_HEADING_PRICE','Prix');
define('TABLE_HEADING_QTY','Stock');
define('TABLE_HEADING_LAST_MODIFIED','Dernière modification');
define('TABLE_HEADING_CREATED','Créé / modifié');

define('TEXT_PRODUCTS_SEO_URL', 'URL SEO du Produit :');
define('TEXT_EDIT_CATEGORIES_SEO_URL', 'URL SEO de la catégorie :');
define('TEXT_CATEGORIES_SEO_URL', 'URL SEO de la catégorie :');

define('TEXT_NEW_PRODUCT', 'Nouveau produit dans &quot;%s&quot;');
define('TEXT_CATEGORIES', 'Catégories :');
define('TEXT_SUBCATEGORIES', 'Sous-catégories :');
define('TEXT_DIVERS_TITLE', 'Divers');
define('TEXT_DESCRIPTION_CATEGORIES','Description de la Catégorie');
define('TEXT_CATEGORIES_PREVIEW_TITLE','Prévisualisation de la catégorie');

define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Métadonnées pour le produit');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Titre');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_KEYWORDS', 'Mots clef');
define('TEXT_PRODUCTS_TAG','Tags produit');

define('TEXT_CREATE','créer');

define('TEXT_PRODUCTS', 'Produits :');
define('TEXT_PRODUCTS_PRICE_INFO', 'Prix :');
define('TEXT_PRODUCTS_COST','Prix fournisseur du produit :');
define('TEXT_PRODUCTS_HANDLING','Autres couts associés :');
define('TEXT_PRODUCTS_PRICE_MARGINS','Marge de profit :');

define('TEXT_PRODUCTS_TAX_CLASS', 'Classe Fiscale :');
define('TEXT_ECOTAX_RATES', 'Taux écotaxe :');
define('TEXT_EDIT_ECOTAX_CATEGORIES', 'Catégorie de l\'écotaxe:');
define('TEXT_PRODUCTS_AVERAGE_RATING', 'Ratio moyen :');
define('TEXT_PRODUCTS_QUANTITY_INFO', 'Quantité :');

define('TEXT_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_DATE_AVAILABLE', 'Date disponibilité    :');
define('TEXT_LAST_MODIFIED',  'Dernière modification :');
define('TEXT_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');
define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Merci de créer une nouvelle catégorie ou un produit dans ce niveau.');
define('TEXT_PRODUCT_MORE_INFORMATION', '<a href="http://%s" target="blank"><span class="main"><font color="#0000FF"><strong><u>Voir le lien</u></strong></font></span></a>');
define('TEXT_PRODUCT_DATE_ADDED', 'Produit ajouté au catalogue le :');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'Produit en stock le :');
define('TEXT_PRODUCTS_SHIPPING_DELAY','Délais de livraison spécifique (autre que par défaut) : ');
define('TEXT_PRODUCT_OPTION','Champs optionnels');
define('TEXT_PRODUCTS_SORT_ORDER','Ordre de tri');

define('TEXT_PRODUCTS_OTHER_INFORMATION','Autres informations');
define('TEXT_PRODUCTS_TIME_REPLENISHMENT','Délais de réapprovisionnement :');
define('TEXT_PRODUCTS_WHAREHOUSE','Nom de l\'entrepot / Rayon');
define('TEXT_PRODUCTS_WHAREHOUSE_ROW','Rangée :');
define('TEXT_PRODUCTS_WHAREHOUSE_LEVEL_LOCATION','Niveau de localisation / case:');
define('TEXT_PRODUCTS_WHAREHOUSE_PACKAGING','Type de packaging :');
define('TEXT_WHAREHOUSE','Entreprosage');


define('TEXT_PRODUCTS_PACKAGING_NEW','Produit neuf');
define('TEXT_PRODUCTS_PACKAGING_REPACKAGED','Produit reconditionné');
define('TEXT_PRODUCTS_PACKAGING_USED','Produit d\'occasion');


define('TEXT_PRODUCTS_ONLY_ONLINE','Exclusivité web (non vendu en magasin)');
define('TEXT_PRODUCTS_ONLY_SHOP','Exclusivité Boutique <br />(Uniquement disponible dans notre magasin physique)');
define('TEXT_YES','oui');
define('TEXT_NO','non');
define('TEXT_CHOOSE','Selectionner');

define('TEXT_PRODUCTS_GALLERY_IMAGE','Galerie d\'images');
define('TEXT_PRODUCTS_IMAGE_DIRECTORY', 'Veuillez choisir votre répertoire dans products : ');
define('TEXT_PRODUCTS_IMAGE_NEW_FOLDER', ' ou créer un répertoire dans products : <br><font size="1"><i>(ex : mon_repertoire/mon_sous_repertoire)</i></font> ');
define('TEXT_PRODUCTS_IMAGE_NEW_FOLDER_GALLERY', ' ou créer un répertoire dans products : <font size="1"><i>(ex : mon_repertoire/mon_sous_repertoire)</i></font> ');

define('TEXT_PRODUCTS_IMAGE_ROOT_DIRECTORY', '-- Sous répertoires disponibles --');
define('TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT','Contenu html (pour pop up)');
define('TEXT_PRODUCTS_MAIN_IMAGE','Ajouter une grosse image : ');
define('TEXT_PRODUCTS_IMAGE_MEDIUM','Image moyenne (pour la description du produit)');
define('TEXT_PRODUCTS_SMALL_IMAGE','Nom de la vignette ou de la petite image : ');
define('TEXT_PRODUCTS_MEDIUM_IMAGE','Nom de l\'image moyenne : ');
define('TEXT_PRODUCTS_BIG_IMAGE','Nom de la grosse image : ');
define('TEXT_DELETE_PRODUCTS_IMAGE','Suppimer l\'image');
define('TEXT_PRODUCTS_FILE_DOWNLOAD', 'Téléchargement Fichier');
define('TEXT_PRODUCTS_FILE_DOWNLOAD_PUBLIC','S\'agit-il d\'un téléchargement public ?');
define('NO_DISPLAY_CHECKBOX','Case à cocher non affichée');
define('DISPLAY_CHECKBOX','Case à cocher affichée');

define('TEXT_PRODUCTS_CATEGORIES_COPY', 'Produit à cloner ou copier');
define('CLONE_PRODUCTS_FROM','Produit à cloner');
define('CLONE_PRODUCTS_TO', 'Destination du clonage');

define('TEXT_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_EDIT_CATEGORIES_ID', 'ID de la catégorie :');
define('TEXT_EDIT_CATEGORIES_NAME', 'Nom de la catégorie :');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Image de la catégorie :');
define('TEXT_EDIT_SORT_ORDER', 'Ordre de tri :');

define('TEXT_INFO_COPY_TO_INTRO', 'Veuillez choisir une nouvelle catégorie dans laquelle vous voulez copier ce produit');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Catégories courantes :');

define('TEXT_INFO_HEADING_NEW_CATEGORY', 'Nouvelle catégorie');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Editer catégorie');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Supprimer catégorie');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Déplacer catégorie');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Supprimer produit');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Déplacer produit');
define('TEXT_INFO_HEADING_COPY_TO', 'Copier vers');
define('TEXT_INFO_HEADING_ARCHIVE','Archivage du produit');
define('TEXT_INFO_ARCHIVE_INTRO','Souhaitez vous mettre ce produit en archive ? <br /><br />Ce produit pourra être réactivé à partir de la section archive. <br /><br /><strong>Note : </strong>L\'archivage permet de faire disparaitre de votre catalogue le produit mais il ne le supprime pas. Les clients auront toujours accès au produit via les moteurs de recherche mais ils ne pourront pas le commander. Ce système est utile pour le référencement de votre site.');

define('TEXT_DELETE_CATEGORY_INTRO', 'Etes vous sur de vouloir supprimer cette catégorie ?');
define('TEXT_DELETE_PRODUCT_INTRO', 'Etes vous sur de vouloir supprimer définitivement ce produit ?');

define('TEXT_DELETE_WARNING_CHILDS', '<strong>ATTENTION :</strong> Il y a %s (sous-)catégories liées &aacute; cette catégorie !');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>ATTENTION :</strong> Il y a %s produits liées &aacute; cette catégorie !');

define('TEXT_MOVE_PRODUCTS_INTRO', 'Merci de sélectionner la catégorie ou vous voudriez que <strong>%s</strong> soit placé');
define('TEXT_MOVE_CATEGORIES_INTRO', 'Merci de sélectionner la catégorie ou vous voudriez que <strong>%s</strong> soit placé');
define('TEXT_MOVE', 'Déplacer <strong>%s</strong> vers :');

define('TEXT_NEW_CATEGORY_INTRO', 'Merci de compléter les informations suivantes pour la nouvelle catégorie');
define('TEXT_CATEGORIES_NAME', 'Nom de la catégorie :');
define('TEXT_CATEGORIES_NAME_TITLE', 'Nom de la catégorie');
define('TEXT_CATEGORIES_IMAGE', 'Image de la catégorie :');
define('TEXT_SORT_ORDER', 'Ordre de tri :');

define('TEXT_PRODUCTS_STATUS', 'Statut :');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date de disponibilité :');
define('TEXT_PRODUCT_AVAILABLE', 'En stock');
define('TEXT_PRODUCTS_DIMENSION','Dimensions (LxHxP):');
define('TEXT_PRODUCTS_DIMENSION_TYPE','Type de dimension:');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Hors stock');
define('TEXT_PRODUCTS_MANUFACTURER', 'Marque du produit:');
define('TEXT_PRODUCTS_NAME', 'Nom du produit');
define('TEXT_PRODUCTS_DESCRIPTION', 'Description du produit');
define('TEXT_PRODUCTS_DESCRIPTION_SUMMARY', 'Description courte du produit');
define('TEXT_PRODUCTS_QUANTITY', 'Quantité en stock :');

define('TEXT_PRODUCTS_MIN_ORDER_QUANTITY', 'Quantité minimale d\'achat pour une commande :');
define('TEXT_PRODUCTS_MIN_ORDER_QUANTITY_GROUP', 'Type d\'achat');
define('TEXT_PRODUCTS_VOLUME','Volume du produit :');
define('TEXT_PRODUCTS_QUANTITY_UNIT','Type quantité de commande');

define('TEXT_PRODUCTS_QUANTITY_FIXED_GROUP','Nbr de qté fixe / ');
define('TEXT_PRODUCTS_MODEL', 'Référence : ');
define('TEXT_PRODUCTS_MODEL_GROUP', 'Référence');

define('TEXT_PRODUCTS_EAN', 'Code barre : ');
define('TEXT_PRODUCTS_SKU','SKU : ');
define('TEXT_PRODUCTS_PRICE_COMPARISON','Exclure des comparateurs de prix ? ');
define('TEXT_PRODUCTS_SUPPLIERS', 'Fournisseur du produit : ');

define('ICON_EDIT_CUSTOMERS_GROUP','Appartient à un groupe client');
define('ICON_EDIT_STATUS_DISPLAY_CATALOG_ON','Affichage du champs dans le catalogue');
define('ICON_EDIT_STATUS_DISPLAY_CATALOG_OFF','Affichage du champs dans l\'administration');

define('TEXT_PRODUCTS_IMAGE', 'Image principale du produit (redimensionnement automatique)');
define('TEXT_PRODUCTS_IMAGE_CUSTOMIZE','Image principale du produit (insertion manuelle)');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE', 'Image vignette du produit');
define('TEXT_PRODUCTS_IMAGE_ZOOM', 'Image zoom du produit');
define('TEXT_PRODUCTS_IMAGE_VISUEL', 'Images actuellement visible sur le produit');
define('TEXT_PRODUCTS_IMAGE_VISUEL_ZOOM', 'Visionner le zoom');
define('TEXT_PRODUCTS_NO_IMAGE_VISUEL_ZOOM','Il n\'y a pas d\'image zoom enregristrée actuellement');
define('TEXT_PRODUCTS_DELETE_IMAGE', 'Ne plus afficher d\'image sur ce produit');
define('TEXT_PRODUCTS_LARGE_IMAGE','Grosse image');
define('TEXT_PRODUCTS_ADD_LARGE_IMAGE','Ajouter une grosse image');
define('TEXT_PRODUCTS_ADD_LARGE_IMAGE_DELETE','Veuillez confirmer la suppression de l\'image');
define('TEXT_PRODUCTS_INSERT_BIG_IMAGE_VIGNETTE','Veuillez inserér une grosse image');

define('TEXT_PRODUCTS_URL', 'URL Externe du produit :');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(sans http://)</small>');
define('TEXT_PRODUCTS_PRESENTATION', 'Présentation');
define('TEXT_PRODUCTS_STOCK', 'Stock');
define('TEXT_PRODUCTS_ALERT','Alerte Stock');
define('TEXT_PRODUCTS_PRICE_PUBLIC', 'Prix Public / taxes');
define('TEXT_PRODUCTS_PRICE', 'Prix de vente:');
define('TEXT_PRODUCTS_PRICE_NET', 'Hors Taxes');
define('TEXT_PRODUCTS_PRICE_GROSS', 'Toutes taxes');
define('TEXT_PRODUCTS_WEIGHT', 'Poids <i>(Kg)</i> :');
define('TEXT_PRODUCTS_WEIGHT_POUND', 'Poids <i>(livres)</i> :');
define('TEXT_PRODUCTS_PRICE_KILO','Afficher le prix au kilo :');
define('TEXT_PRODUCTS_DIRECTORY_DONT_EXIST', '<strong>Le répertoire n\'existe pas. Veuillez contacter votre administrateur</strong> : ');

define('TEXT_PRODUCTS_PREVIEW_TITLE', 'Prévisualisation du produit');
define('TEXT_PRODUCTS_PREVIEW_GENERAL', 'Général');
define('TEXT_PRODUCTS_PREVIEW_NAME', 'Nom du produit en');
define('TEXT_PRODUCTS_PREVIEW_URL', 'URL Externe du produit :');
define('TEXT_PRODUCTS_PREVIEW_PRICE_PUBLIC', 'Prix Public Hors Taxes : ');
define('TEXT_PRODUCTS_PREVIEW_PRICE_GROUP', 'Prix');
define('TEXT_PRODUCTS_PREVIEW_PRICE_GROUP_NO_TAXE', 'Hors Taxes :');
define('TEXT_PRODUCTS_PREVIEW_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_PREVIEW_IMAGE', 'Visuel');
define('TEXT_PRODUCTS_PREVIEW_PRICE_KILO','Px /Kg');
define('TEXT_PRODUCTS_PAGE_OPTION','Champs complémentaires');
define('TEXT_PRODUCTS_OTHERS_OPTIONS','Autres options');
define('TEXT_PRODUCTS_HEART','Souhaitez-vous que ce produit apparaisse comme coups de coeur ?');
define('TEXT_PRODUCTS_FEATURED','Souhaitez-vous que ce produit apparaisse comme produit sélectionné ?');
define('TEXT_PRODUCTS_SPECIALS','Souhaitez-vous créer une promotion ?');
define('TEXT_PRODUCTS_SPECIALS_PERCENTAGE','Montant de la remise en pourcentage (ex : 20%)');

define('TEXT_PRODUCT_TYPE', 'Type produit');
define('TEXT_STOCKABLE_PRODUCT','Produit');
define('TEXT_STOCKABLE_CONSUMABLE','Consommable');
define('TEXT_STOCKABLE_SERVICE','Service');

define('TEXT_PRODUCTS_TWITTER', 'Souhaitez-vous publier sur Twitter ?');
define('TEXT_TWITTER_PRODUCTS', 'Découvrez notre nouveau produit ! ');

define('TEXT_USER_NAME','Créé / Modifié par : ');

define('TEXT_CATEGORIES_IMAGE_TITLE','Image principale de la catégorie');
define('TEXT_CATEGORIES_IMAGE_VIGNETTE', 'Image vignette de la catégorie');
define('TEXT_CATEGORIES_IMAGE_VISUEL', 'Image actuellement visible sur la catégorie');
define('TEXT_CATEGORIES_DELETE_IMAGE', 'Ne plus afficher d\'image sur cette catégorie');

define('EMPTY_CATEGORY', 'Catégorie vide');

define('TEXT_HOW_TO_COPY', 'Méthode de copie :');
define('TEXT_COPY_AS_LINK', 'Lien produit');
define('TEXT_COPY_AS_DUPLICATE', 'Dupliquer produit');

define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Erreur : Impossible de lier des produits dans la même cat&eacutegorie.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Erreur : Impossible d\'écrire dans le répertoire images : ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Erreur: Vous ne pouvez pas bougez cette catégorie.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire d\'images n\'existe pas : ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Erreur : La catégorie ne peut pas être déplacée dans la sous-catégorie.');

define('TEXT_CUST_GROUPS', 'Prix groupe clients B2B');

if (B2B == 'true'){
define('TEXT_OVERRIDE_ON', 'Automatique (+%)');
} else {
define('TEXT_OVERRIDE_ON', 'Automatique (-%)');
}

define('TEXT_OVERRIDE_OFF', 'Manuel');

define('TAB_PRICE_GROUP_VIEW', 'Actif : Affiche le prix du groupe - Non Actif : Affiche le prix public');
define('TAB_PRODUCTS_GROUP_VIEW', 'Autoriser l\'affichage du produit pour les clients du groupe ');
define('TAB_PRODUCTS_VIEW', 'Autoriser l\'affichage du produit pour les clients Grand Public');
define('PRODUCTS_VIEW', 'Options d\'affichage :');
define('TAB_ORDERS_GROUP_VIEW', 'Autoriser la commande du produit pour les clients du groupe ');
define('TAB_ORDERS_VIEW', 'Autoriser la commande du produit pour les clients Grand Public');
define('TAB_OPTIONS_FIELDS','Champs complémentaires');
define('TAB_OTHER_OPTIONS','Autres Options');
define('TAB_STOCK','Stock');
define('TITLE_AIDE_PRICE', 'Notes sur l\'utilisation des options d\'affichage sur les produits');
define('HELP_PRICE_GROUP_VIEW', '<strong>Activé :</strong> Affiche le prix du groupe - <strong>Non Activé :</strong> Affiche le prix public');
define('HELP_PRODUCTS_VIEW', '<strong>Activé :</strong> Autorise l\'affichage du produit aux clients.');
define('HELP_ORDERS_VIEW', '<strong>Activé :</strong> Autorise aux clients de commander le produit.');
define('HELP_OTHERS_GROUP','Attention Nbr de qté fixe / Type d\'achat n\'est pas relié &agrave la quantité de commande. ex : pour 1 qté on 3 produits par lot');
define('HELP_PRICE_GROUP_VIEW_NOTE', 'Inclus les tarifs promotionnels.');

define('TEXT_EDIT_DEFAULT_CONFIGURATION','Editer la configuration par défaut');

define('TITLE_AIDE_IMAGE', 'Notes sur l\'utilisation des images');

define('HELP_IMAGE_PRODUCTS', '
<li>Redimensionnement automatique des images : <br />
 <blockquote>
    - Petite image pour les autres pages du site : ' . SMALL_IMAGE_WIDTH . ' x ' . SMALL_IMAGE_HEIGHT .'<br />
    - Image moyenne pour la page de description des produits : ' . MEDIUM_IMAGE_WIDTH . ' x ' . MEDIUM_IMAGE_HEIGHT .'<br />
    - Image Zoom pour la page de description des produits : ' . BIG_IMAGE_WIDTH . ' x ' . BIG_IMAGE_HEIGHT .'<br />
    - Formats acceptés : JPG, GIF, PNG. <br />
    - Taille maximum du fichier : 2000Ko max.<br />
    - Vous pouvez à tout moment changer le redimensionnement automatique des images (Design / Configuration générale du design / Gestion des images).<br />
    - Vous pouvez à tout moment activer aussi le téléchargement manuel (Configuration / Mon administrations / Gestion des images).<br />
 </blockquote>
</li>
<li>L\'utilisation de la galerie d\'images nécéssite d\'avoir le plug in FancyBox installé. Si n\'y a pas d\'images dans la gallerie, alors c\'est l\'image par défaut de la vignette qui est affichée sous forme de pop up.</li>
');

define('HELP_IMAGE_CATEGORIES', '<li>Veuillez à ne pas mettre plus d\'une image dans la sections "Image vignette de la catégorie".</li><li>Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

define('TITLE_HELP_DESCRIPTION', 'Notes sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

define('TITLE_HELP_OPTIONS', 'Notes sur les options');
define('HELP_OPTIONS', '<li>Les champs supplémentaires ne sont pas pris en compte dans la facture ni dans son calcul.</li><li>Si vous souhaitez créer ou supprimer un champs, vous devez le faire à partir du menu de gestion des champs supplémentaires.</li><li>
Si le champs est vide, celui n\'apparaitra pas dans la partie catalogue.</li>');

define('TITLE_HELP_GENERAL', 'Notes Informatives');
define('HELP_GENERAL', '<li>Un certain nombre de champs sont paramétrables &agrave partir du menu de configuration.</li>');


define('HELP_STOCK', '<li>Concernant la gestion des commandes via une <strong>quantité minimale d\'achat</strong>, veuillez suivre ces instructions pour une utilisation optimale.</li>
<blockquote>- Si le paramétrage par défaut (menu configuration / Ma boutique / valeur minimales - maximales) est = 1 et la configuration du produit = 0 alors la quantité minimale d\'achat d\'un produit sera de 1 (configuration d\'origine).<br />- Si le paramétrage par défaut est > 30 (exemple) et la configuration du produit = 0 alors la quantité minimale d\'achat d\'un produit sera de 30.<br />- Si le paramétrage par défaut est > 30 et  et la configuration du produit = 1 alors la quantité minimale d\'achat d\'un produit sera de 1.<br />- Si le paramétrage par défaut est = 0 et la quantité minimale d\'achat d\'un produit = 0, le client ne pourra pas passer de commande pour ce produit.</li></blockquote>
<li><strong>Alerte Stock produit</strong><blockquote>- Concernant les alertes stock, Si l\'alerte stock produit = 0 et alerte stock produit < alerte stock (voir configuration générale), aucun mail ne sera envoyé.</li></blockquote>
.');

define('TITLE_HELP_SUBMIT', 'Notes sur la gestion des métadonnées');
define('HELP_SUBMIT', '<li>Le système gère automatiquement la gestion des métadonnées.<br />
<blockquote>Il inscrit automatiquement les éléments dans l\'ordre suivant si les champs ne sont pas renseignées:<br />
Titre : le nom de votre produit (ex blouson cuir)<br />
Description : le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie,)<br />
Keywords : le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
categorie : la catégorie du produit<br />
</blockquote></li>
<li>Si vous renseignez les champs, les balises apparaitront de la facon suivante : <br />
<blockquote>
Titre : le titre du champs, le nom de votre produit (ex blouson cuir)<br />
Description : le champs description, le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
Mots clefs : le champs mots clefs, le nom du produit (blouson cuir), le nom du produit découpé (blouson, cuir), le nom de la catégorie<br />
</blockquote></li>
<li><strong>Veuillez à ne pas mettre trop de mots clefs (maximum 10)</strong> et établissez une cohérence entre chaque champs afin de retourver les mots les plus importants en premier</li>
');
?>
