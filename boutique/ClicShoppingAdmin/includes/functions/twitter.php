<?php
/**
 * twitter.php 
 * @copyright Copyright 2008 - ClicShopping http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: twitter.php 
 * http://twitter.com/oauth_clients/
*/


/**
 * Load ClicShopping Information and authentification on twitter
 * Read only the tweet
 * @param string $twitter_authentificate
 * @return string twitter_authentificate_clicshopping
 * @access public
 */
  function osc_twitter_authentificate_clicshopping() {

    $consumerKey = '5oly6irbxZPWXGYv0vfPkw';
    $consumerSecret = 'N3YwWGcTM3I2LHZK8Qlz92fx6p19vlJYzBLOK1Ej0';
    $accessToken = '64883785-uayUMxNTJOz0ElqzqasmgQmvg7h3uHoMgYBT0mpfH';
    $accessTokenSecret = 'QndMVpfPyVmRm2UKcB7inwHVmsvtWJj09hbGwBg3IRfho';

// lecture dela classe twitter

    require_once('ext/twitter/twitter.class.php');
    $twitter_authentificate_clicshopping = new Twitter($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);

    return $twitter_authentificate_clicshopping;
  }  

/**
 * Send a message on Twitter concerning the clicshopping administrator
 *
 * @param string $twitter_authentificate_administrator
 * @return string $data, $parse
 * @access public
 */

  function osc_send_twitter($twitter_authentificate_administrator) {

    $consumerKey = MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_KEY;
    $consumerSecret = MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_SECRET;
    $accessToken = MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN;
    $accessTokenSecret = MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN_SECRET;

// lecture dela classe twitter
    require_once('ext/twitter/twitter.class.php');
    $twitter_authentificate_administrator = new Twitter($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);


    if(isset($_POST['twitter_msg'])){
       $twitter_message = $_POST['twitter_msg'];
       if(strlen($twitter_message) < 1){
        $error = 1;
      } else {
        $status = $twitter_authentificate_administrator->send( $_POST['twitter_msg'], $_POST['twitter_media']);
      }
    }

    if (isset($_POST['twitter_msg']) && !isset($error)){
    } else if(isset($error)){
      echo TEXT_ERROR_TWITTER;
      return -1;
    }
     return;
  }
?>