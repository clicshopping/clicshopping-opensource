<?php
/**
 * products_expected.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Products Expected');

define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_DATE_EXPECTED', 'Date Expected');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_DATE_EXPECTED', 'Date Expected:');
?>