<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


  define('MODULE_ORDER_TOTAL_TOTAL_TITLE', 'Total');
  define('MODULE_ORDER_TOTAL_TOTAL_DESCRIPTION', 'Order Total<br /><br /><font color="#FF0000"> <strong> Warning! </strong> Disabling this module will restore the default values</font>');
?>