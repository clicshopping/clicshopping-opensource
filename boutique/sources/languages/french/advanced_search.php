<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Rechercher un produit');

define('NAVBAR_TITLE_1', 'Recherche avancée');
define('NAVBAR_TITLE_2', 'Résultats de recherche');

define('HEADING_TITLE_1', 'Recherche avancée');
define('HEADING_TITLE_2', 'Produits répondant aux critères de recherche');

define('HEADING_SEARCH_CRITERIA', 'Critère de recherche');


define('TABLE_HEADING_IMAGE', '');
define('TABLE_HEADING_MODEL', 'Modèle');
define('TABLE_HEADING_PRODUCTS', 'Produit');
define('TABLE_HEADING_MANUFACTURER', 'Marque');
define('TABLE_HEADING_QUANTITY', 'Stock');
define('TABLE_HEADING_PRICE', 'Prix');
define('TABLE_HEADING_WEIGHT', 'Poids');
define('TABLE_HEADING_BUY_NOW', 'Quantité minimum');

define('MIN_QTY_ORDER_PRODUCT','Pour réaliser une commande il est nécessaire d\'avoir au moins une quantité minimale de : ' . (int)$min_quantity .' produits dans son panier pour chaque produit. Votre quantité de produit est automatiquement mis à jour si votre commande est inférieure à la quantité mimimale autorisée lors de procédure de paiment');

define('TEXT_NO_PRODUCTS', 'Il n\'y a aucun produit correspondant à vos critères de recherche.');

define('ERROR_AT_LEAST_ONE_INPUT', 'Au moins un critere de recherche doit etre rempli.');
define('ERROR_INVALID_FROM_DATE', 'Date du champ <u>Depuis la date</u> invalide.');
define('ERROR_INVALID_TO_DATE', 'Date du champ <u>Jusqu\'à la date</u>  invalide.');
define('ERROR_TO_DATE_LESS_THAN_FROM_DATE', 'La date du champ <u>Jusqu\'à la date</u> doit être supérieure ou égale à la date du champ <u>Depuis la date</u>');
define('ERROR_PRICE_FROM_MUST_BE_NUM', 'Le prix du champ <u>Prix à partir de</u> ne doit contenir que des chiffres.');
define('ERROR_PRICE_TO_MUST_BE_NUM', 'Le prix du champ <u>Prix jusqu\'à</u> ne doit contenir que des chiffres.');
define('ERROR_PRICE_TO_LESS_THAN_PRICE_FROM', 'Le prix du champ <u>Prix jusqu\'à</u> doit être supérieure ou égale au prix du champ <u>Prix à partir de</u>.');
define('ERROR_INVALID_KEYWORDS', 'Mots-clés invalides.');
?>
