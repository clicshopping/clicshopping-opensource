<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('NAVBAR_TITLE','Blog');
  define('HEADING_TITLE', 'Blog '. STORE_NAME );
  define('TABLE_HEADING_IMAGE', '');
  define('TEXT_NO_BLOG', 'No news published');
  define('TEXT_NUMBER_OF_BLOG', 'Number of news: ');
  define('TEXT_SHOW', '');
  define('TEXT_ALL_CATEGORIES', 'Yours categories');
?>