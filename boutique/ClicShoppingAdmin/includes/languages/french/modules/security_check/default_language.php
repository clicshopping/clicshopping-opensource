<?php
/**
 * default_language.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/


define('ERROR_NO_DEFAULT_LANGUAGE_DEFINED', 'Erreur: La langue par défaut n\a pas été sélectionnée. Veuillez faire la rectification: Configuration / Divers / Langues');
?>
