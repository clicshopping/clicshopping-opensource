<?php
/*
 * account_history.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

 require($OSCOM_Template->getTemplateHeaderFooter('header'));
 require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// --- Count Order   -----  
// ---------------------- 
?>
<div class="contentContainer">
<?php 
  if ($orders_total > 0) {

    while ($history = osc_db_fetch_array($history_query)) {

      $Qproducts = $OSCOM_PDO->prepare('select count(*) as count
                                        from :table_orders_products
                                        where orders_id = :orders_id
                                      ');
      $Qproducts->bindInt(':orders_id', (int)$history['orders_id']);

      $Qproducts->execute();

      $products = $Qproducts->fetch();

      if (osc_not_null($history['delivery_name'])) {
        $order_type = TEXT_ORDER_SHIPPED_TO;
        $order_name = $history['delivery_name'];
      } else {
        $order_type = TEXT_ORDER_BILLED_TO;
        $order_name = $history['billing_name'];
      }
// ---------------------- ---------
// --- Display history number   -----  
// ---------------------- --------
?>
  <div class="row">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  </div>
  <div class="contentText">
    <div class="row">
      <div class="col-sm-10"><?php echo '<strong>' . TEXT_ORDER_NUMBER . '</strong> ' . $history['orders_id']; ?></div>
    </div>
    <div class="row">
      <div class="col-sm-10"><?php echo '<strong>' . TEXT_ORDER_STATUS . '</strong> ' . $history['orders_status_name']; ?></div>
    </div>
  </div>
  <div class="row">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  </div>
<?php
// ---------------------- 
// --- Display Order   -----  
// ---------------------- 
?>
  <div class="contentText">
    <div class="row">
      <div class="col-sm-6">
        <?php echo '<strong>' . TEXT_ORDER_DATE . '</strong> ' .  $OSCOM_Date->getLong($history['date_purchased']) . '<br /><strong>' . $order_type . '</strong> ' . osc_output_string_protected($order_name); ?>
      </div>

      <div class="col-sm-4">
        <?php echo '<strong>' . TEXT_ORDER_PRODUCTS . '</strong> ' . $products['count'] . '<br /><strong>' . TEXT_ORDER_COST . '</strong> ' . strip_tags($history['order_total']); ?>
      </div>

      <div class="col-sm-2">
          <?php echo '<p class="pull-right">'. osc_draw_button(SMALL_IMAGE_BUTTON_VIEW, null, osc_href_link('account_history_info.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'order_id=' . $history['orders_id'], 'SSL'), 'info') .'</p>'; ?>
      </div>
    </div>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
<?php
    }
  } else {
?>
  <div class="alert alert-info">
    <p><?php echo TEXT_NO_PURCHASES; ?></p>
  </div>
<?php
  }
  if ($orders_total > 0) {
?>
  <div class="contentText">
    <div class="clearfix"></div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="row">
      <div class="col-sm-6 pagenumber hidden-xs">
        <?php echo $history_split->display_count(TEXT_DISPLAY_NUMBER_OF_ORDERS); ?>
      </div>
      <div class="col-sm-6">
        <div class="pull-right pagenav"><ul class="pagination"><?php echo $history_split->display_links(MAX_DISPLAY_PAGE_LINKS, osc_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></ul></div>
        <span class="pull-right"><?php echo TEXT_RESULT_PAGE; ?></span>
      </div>
    </div>
  </div>

<?php
  }
// ---------------------- 
// --- Button   -----  
// ---------------------- 
?>
  <div class="clearfix"></div>
  <div class="control-group">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="controls">
      <div class="buttonSet"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('account.php', '', 'SSL'), 'primary', null, null); ?></div>
    </div>
  </div>
 </div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>