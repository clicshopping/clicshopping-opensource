<?php
/**
 * specials.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'setflag':
        osc_set_specials_status($_GET['id'], $_GET['flag']);
        osc_redirect(osc_href_link('specials.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'sID=' . $_GET['id']));
        break;

      case 'insert':
        $products_id = osc_db_prepare_input($_POST['products_id']);
        $products_price = osc_db_prepare_input($_POST['products_price']);
        $specials_price = osc_db_prepare_input($_POST['specials_price']);
        $expdate = osc_db_prepare_input($_POST['expdate']);
        $schdate = osc_db_prepare_input($_POST['schdate']);
        $customers_group = osc_db_prepare_input($_POST['customers_group']);
        $twitter_status = osc_db_prepare_input($_POST['specials_twitter']);

        if (osc_db_prepare_input($_POST['flash_discount']) == '1') {
          $flash_discount = '1';
        } else {
          $flash_discount = '0';
        }

        $Qprice = $OSCOM_PDO->prepare('select customers_group_price
                                       from :table_products_groups
                                       where products_id = :products_id
                                       and customers_group_id  =:customers_group_id
                                      ');

        $Qprice->bindInt(':products_id', (int)$products_id );
        $Qprice->bindValue(':customers_group_id', (int)$customers_group );
        $Qprice->execute();

// B2B
        while ($gprices =  $Qprice->fetch() ) {
            $products_price = $gprices['customers_group_price'];
        }

        if (substr($specials_price, -1) == '%') {

          $QnewSpecialinsert = $OSCOM_PDO->prepare("select products_id, 
                                                           products_price 
                                                   from :table_products
                                                   where products_id = :products_id
                                                  ");
          $QnewSpecialinsert->bindValue(':products_id', (int)$products_id );
          $QnewSpecialinsert->execute();

          $new_special_insert = $QnewSpecialinsert->fetch();

          $products_price = $new_special_insert['products_price'];
          $specials_price = ($products_price - (($specials_price / 100) * $products_price));
        }

        $expires_date = '';
        if (osc_not_null($expdate)) {
          $expires_date = substr($expdate, 0, 4) . substr($expdate, 5, 2) . substr($expdate, 8, 2);
        }

        $scheduled_date = '';
        if (osc_not_null($schdate)) {
          $schedule_date = substr($schdate, 0, 4) . substr($schdate, 5, 2) . substr($schdate, 8, 2);
        }

        $OSCOM_PDO->save('specials', [
                                      'products_id' => (int)$products_id,
                                      'specials_new_products_price' =>$specials_price,
                                      'specials_date_added' => 'now()',
                                      'scheduled_date' => (osc_not_null($schedule_date) ? "'" . $schedule_date . "'" : 'null'),
                                      'expires_date' => (osc_not_null($expires_date) ? "'" . $expires_date . "'" : 'null'),
                                      'status' => 1,
                                      'customers_group_id' => (int)$customers_group,
                                      'flash_discount' => (int)$flash_discount
                                    ]
                        );

        if (($customers_group == 0 )) {
          if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
            if ($twitter_status == '1') {

              $Qproduct = $OSCOM_PDO->prepare('select distinct p.products_image,
                                                               p.products_status,
                                                               pd.products_name,
                                                               s.flash_discount
                                             from :table_products p,
                                                  :table_products_description pd,
                                                  :table_specials s
                                              where p.products_id =  :products_id
                                              and p.products_id = pd.products_id
                                              and p.products_id = s.products_id
                                              and pd.language_id = :language_id
                                              and p.products_status = 1
                                              and s.status = 1
                                     ');

              $Qproduct->bindInt(':products_id', (int)$products_id );
              $Qproduct->bindInt(':language_id', (int)$_SESSION['languages_id']);
              $Qproduct->execute();

              $product =  $Qproduct->fetch();

              $twitter_image =  DIR_FS_CATALOG_IMAGES . $product['products_image'];

              if ( $product['flash_discount'] == 1 ) {
                $twitter_text = TEXT_NEW_SPECIALS_TWITTER;
              } else {
                $twitter_text = TEXT_NEW_SPECIALS_TWITTER;
              }

              $text_specials =  $twitter_text . $product['products_name'] .' : ' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php?products_id='. $products_id;

              $_POST['twitter_msg'] =  $text_specials;
              $_POST['twitter_media'] =  $twitter_image;
              
              if ( $product['products_status'] == 1 ) {
               echo osc_send_twitter($twitter_authentificate_administrator);
              }
            }
          }
        }

        osc_redirect(osc_href_link('specials.php', 'page=' . $_GET['page']));
      break;

      case 'update':
        $specials_id = osc_db_prepare_input($_POST['specials_id']);
        $products_price = osc_db_prepare_input($_POST['products_price']);
        $specials_price = osc_db_prepare_input($_POST['specials_price']);
        $expdate = osc_db_prepare_input($_POST['expdate']);
        $schdate = osc_db_prepare_input($_POST['schdate']);
        $customers_group=osc_db_prepare_input($_POST['customers_group']);
        $twitter_status = osc_db_prepare_input($_POST['specials_twitter']);
        $status = osc_db_prepare_input($_POST['status']);

        if (osc_db_prepare_input($_POST['flash_discount']) == '1') {
          $flash_discount = '1';
        } else {
          $flash_discount = '0';
        }

        $Qprice = $OSCOM_PDO->prepare('select customers_group_price
                                      from :table_products_groups
                                      where products_id = :products_id
                                      and customers_group_id  = :customers_group_id
                                     ');

        $Qprice->bindInt(':products_id', (int)$products_id );
        $Qprice->bindInt(':customers_group_id', (int)$customers_group);
        $Qprice->execute();

        while ($gprices =  $Qprice->fetch() ) {
            $products_price = $gprices['customers_group_price'];
        };

        if (substr($specials_price, -1) == '%') $specials_price = ($products_price - (($specials_price / 100) * $products_price));


        $expires_date = '';
        $scheduled_date = '';

        if (osc_not_null($expdate)) {
          $expires_date = substr($expdate, 0, 4) . substr($expdate, 5, 2) . substr($expdate, 8, 2);
        }

        if (osc_not_null($schdate)) {
          $scheduled_date = substr($schdate, 0, 4) . substr($schdate, 5, 2) . substr($schdate, 8, 2);
        }

        osc_db_query("update specials set specials_new_products_price = '" . osc_db_input($specials_price) . "',
                                          specials_last_modified = now(),
                                          expires_date = " . (osc_not_null($expires_date) ? "'" . osc_db_input($expires_date) . "'" : 'null') . ",
                                          scheduled_date  = " . (osc_not_null($scheduled_date) ? "'" . osc_db_input($scheduled_date) . "'" : 'null') .",
                                          customers_group_id = '" . (int)$customers_group . "',
                                          flash_discount= '" . (int)$flash_discount . "'
                      where specials_id = '" . (int)$specials_id . "'
                     ");

        osc_redirect(osc_href_link('specials.php', 'page=' . $_GET['page'] . '&sID=' . $specials_id));
      break;

      case 'delete_all':

       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $specials['specials_id'] ) {

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_specials
                                          where specials_id = :specials_id 
                                        ');
          $Qdelete->bindInt(':specials_id',   (int)$specials['specials_id'] );
          $Qdelete->execute();
         }
       }
       osc_redirect(osc_href_link('specials.php', 'page=' . $_GET['page']));
    break;

    }
  }

  if (empty($action)) {

    $specials_query_raw = "select p.products_id,
                                  p.products_model,   
                                  pd.products_name, 
                                  p.products_image,
                                  p.products_price, 
                                  s.specials_id, 
                                  s.customers_group_id, 
                                  s.specials_new_products_price, 
                                  s.specials_date_added, 
                                  s.specials_last_modified, 
                                  s.scheduled_date, 
                                  s.expires_date, 
                                  s.date_status_change, 
                                  s.status,
                                  p.products_archive,
                                  s.flash_discount
                           from products p,
                                specials s,
                                products_description pd
                           where p.products_id = pd.products_id 
                           and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                           and p.products_id = s.products_id 
                           order by pd.products_name
                          ";

    $specials_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $specials_query_raw, $specials_query_numrows);
    $specials_query = osc_db_query($specials_query_raw);
  }

  require('includes/header.php');
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
 <tr>
  <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
      <div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        <div class="adminTitle">
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/specials.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
         <span class="col-md-4 smallText" style="text-align: center;">
           <?php echo $specials_split->display_count($specials_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SPECIALS); ?><br />
           <?php echo $specials_split->display_links($specials_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
         </span>
         <span class="pull-right">
           <?php echo '<a href="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_specials.gif', IMAGE_NEW_PRODUCT) . '</a>'; ?>&nbsp;
           <form name="delete_all" <?php echo 'action="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
           <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
         </span>
<?php
  } // end empty

  if ( ($action == 'new') || ($action == 'edit') ) {
   $form_action = 'insert';
   if ( ($action == 'edit') && isset($_GET['sID']) ) {
      $form_action = 'update';
   }
?>
           <form name="new_special" <?php echo 'action="' . osc_href_link('specials.php', osc_get_all_get_params(array('action', 'info', 'sID')) . 'action=' . $form_action) . '"'; ?> method="post"><?php if ($form_action == 'update') echo osc_draw_hidden_field('specials_id', $_GET['sID']); ?>
           <span class="pull-right"><?php echo (($form_action == 'insert') ? osc_image_submit('button_insert_specials.gif', IMAGE_INSERT) : osc_image_submit('button_update.gif', IMAGE_UPDATE)); ?>&nbsp;</span>
           <span class="pull-right"><?php echo '<a href="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . (isset($_GET['sID']) ? '&sID=' . $_GET['sID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
<?php
  }
?>
         </div>
         <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      </div>
      <div class="clearfix"></div>
    </tr>
    <!-- //################################################################################################################ -->
    <!-- //                                      EDITION ET INSERTION D'UNE PROMOTION                                       -->
    <!-- //################################################################################################################ -->
<?php
  if ( ($action == 'new') || ($action == 'edit') ) {
    $form_action = 'insert';
    if ( ($action == 'edit') && isset($_GET['sID']) ) {
      $form_action = 'update';

      $Qproduct= $OSCOM_PDO->prepare('select p.products_id,
                                            pd.products_name,
                                            s.customers_group_id,
                                            p.products_price,
                                            s.specials_new_products_price,
                                            s.scheduled_date,
                                            s.expires_date,
                                            s.flash_discount
                                     from :table_products p,
                                          :table_products_description pd,
                                          :table_specials s
                                     where p.products_id = pd.products_id
                                     and pd.language_id = :language_id
                                     and p.products_id = s.products_id
                                     and s.specials_id = :specials_id
                                     ');

      $Qproduct->bindInt(':specials_id',  (int)$_GET['sID'] );
      $Qproduct->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qproduct->execute();

      $product = $Qproduct->fetch();

      $sInfo = new objectInfo($product);
    } else {

     $sInfo = new objectInfo(array());

// create an array of products on special, which will be excluded from the pull down menu of products
// (when creating a new product on special)

    $specials_array = array();

    $Qspecials = $OSCOM_PDO->prepare('select p.products_id,
                                             s.customers_group_id
                                     from :table_products p,
                                          :table_specials s
                                     where s.products_id = p.products_id
                                     and p.products_status = :products_status
                                   ');

      $Qspecials->bindValue(':products_status',  1 );
      $Qspecials->execute();

    while ($specials = $Qspecials->fetch() ) {
       $specials_array[] = (int)$specials['products_id'].":".(int)$specials['customers_group_id'];
    }

    $customers_groups_query = osc_db_query("select customers_group_name, 
                                                   customers_group_id 
                                            from customers_groups
                                            order by customers_group_id 
                                           ");
    $input_groups=array();
    $all_groups=array();

    if(isset($_GET['sID']) && $sInfo->customers_group_id != 0 ){

      $QcustomerGroupPrice = $OSCOM_PDO->prepare('select customers_group_price
                                                    from :table_products_groups
                                                    where products_id = :products_id
                                                    and customers_group_id =  :customers_group_id
                                                   ');

      $QcustomerGroupPrice->bindInt(':products_id',  (int)$sInfo->products_id);
      $QcustomerGroupPrice->bindInt(':customers_group_id',  (int)$sInfo->customers_group_id );
      $QcustomerGroupPrice->execute();

      if ($customer_group_price = $QcustomerGroupPrice->fetch() ) {
        $sInfo->products_price = $customer_group_price['customers_group_price'];
      }
    }
  }
?>
    <tr>
      <td>
        <div>
          <ul class="nav nav-tabs" role="tablist"  id="myTab">
            <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
          </ul>
          <div class="tabsClicShopping">
            <div class="tab-content">
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General de la Promotion          //-->
<!-- ------------------------------------------------------------ //-->
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td class="mainTitle"><?php echo TITLE_SPECIALS_GENERAL; ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                   <td class="main"><?php echo TEXT_SPECIALS_PRODUCT; ?>&nbsp;</td>
                   <td class="main"><?php echo (isset($sInfo->special_status)) ? $sInfo->products_name . ' <small>(' . $currencies->format($sInfo->products_price) . ')</small>' : osc_draw_products_pull_down('products_id', 'style="font-size:10px"', $specials_array); echo osc_draw_hidden_field('products_price', (isset($sInfo->products_price) ? $sInfo->products_price : '')); ?></td>
                  </tr>
<?php
  if ($action ==  'new')  {
     if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
?>
                  <tr>
                     <td class="main"><?php echo TEXT_SPECIALS_TWITTER; ?></td>
                     <td class="main"><?php echo  osc_draw_radio_field('specials_twitter', '1', $sInfo->in_accept_twitter) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('specials_twitter', '0', $sInfo->out_accept_twitter) . '&nbsp;' . TEXT_NO; ?></td>
                  </tr>
<?php
    }
  }
?>
                 </table></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               </tr>
               <tr>
                <td class="mainTitle"><?php echo TITLE_SPECIALS_GROUPE; ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
<?php
// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {
?>
                  <tr>
                   <td class="main"><?php echo TEXT_SPECIALS_GROUPS; ?>&nbsp;</td>
                   <td class="main"><?php echo osc_draw_pull_down_menu('customers_group', osc_get_customers_group(VISITOR_NAME), (isset($sInfo->customers_group_id)? $sInfo->customers_group_id:''));?> </td>
                  </tr>
<?php
  }
?>
                  <tr>
                   <td class="main"><?php echo TEXT_SPECIALS_SPECIAL_PRICE; ?>&nbsp;</td>
                    <td class="main"><?php echo osc_draw_input_field('specials_price', (isset($sInfo->specials_new_products_price) ? $sInfo->specials_new_products_price : ''),'required aria-required="true" id="specials_title"'); ?></td>
                  </tr>
                  <tr>
                   <td class="main"><?php echo TEXT_SPECIALS_FLASH_DISCOUNT; ?>&nbsp;</td>
                    <td class="main"><?php echo  osc_draw_checkbox_field('flash_discount', '1', $sInfo->flash_discount); ?></td>
                  </tr>
                  <tr>
                 </table></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               </tr>
               <tr>
                <td class="mainTitle"><?php echo TITLE_SPECIALS_DATE; ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                   <td class="main"><?php echo TEXT_SPECIALS_START_DATE; ?></td>
                   <td class="main"><?php echo osc_draw_input_field('schdate', (osc_not_null($sInfo->scheduled_date) ? substr($sInfo->scheduled_date, 0, 4) . '-' . substr($sInfo->scheduled_date, 5, 2) . '-' . substr($sInfo->scheduled_date, 8, 2) : ''), 'id="schdate"'); ?></td>
                  </tr>
                  <tr>
                   <td class="main"><?php echo TEXT_SPECIALS_EXPIRES_DATE; ?></td>
                   <td class="main"><?php echo osc_draw_input_field('expdate', (osc_not_null($sInfo->expires_date) ? substr($sInfo->expires_date, 0, 4) . '-' . substr($sInfo->expires_date, 5, 2) . '-' . substr($sInfo->expires_date, 8, 2) : ''), 'id="expdate"'); ?></td>
                  </tr>
                 </table></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
               <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
               <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                   <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_SPECIALS_PRICE); ?></td>
                   <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_SPECIALS_PRICE; ?></strong></td>
                  </tr>
                  <tr>
                   <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                   <td class="main"><?php echo TEXT_AIDE_SPECIALS_PRICE; ?></td>
                  </tr>
                 </table></td>
               </tr>
              </table>
             </div>
           </div>
         </div>
       </div>
     </td>
   </tr>
 </form>
<?php
  } else {
?>
    <!-- //################################################################################################################ -->
    <!-- //                                             LISTING DES PROMOTIONS                                              -->
    <!-- //################################################################################################################ -->
    <tr>
     <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr class="dataTableHeadingRow">
           <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
           <td class="dataTableHeadingContent">&nbsp;</td>
           <td class="dataTableHeadingContent">&nbsp;</td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MODEL; ?></td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
<?php
// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {
?>
           <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_PRODUCTS_GROUP; ?></td>
<?php
  }
?>
           <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_PERCENTAGE; ?></td>
           <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_PRODUCTS_PRICE; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SCHEDULED_DATE; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_EXPIRES_DATE; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_FLASH_DISCOUNT; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ARCHIVE; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
          </tr>
<?php

    while ($specials = osc_db_fetch_array($specials_query)) {

      if ((!isset($_GET['sID']) || (isset($_GET['sID']) && ($_GET['sID'] == $specials['specials_id']))) && !isset($sInfo)) {

        $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                                 products_image
                                          from :table_products
                                          where products_id = :products_id
                                         ');
                                       
        $Qproducts->bindInt(':products_id', (int)$specials['products_id']);

        $Qproducts->execute();

        $products = $Qproducts->fetch();

        $sInfo_array = array_merge($specials, $products);
        $sInfo = new objectInfo($sInfo_array);
      }

      if (isset($sInfo) && is_object($sInfo) && ($specials['specials_id'] == $sInfo->specials_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }

      $QcustomersGroupPrice = $OSCOM_PDO->prepare('select customers_group_price
                                                   from :table_products_groups
                                                   where products_id = :products_id
                                                   and customers_group_id =  :customers_group_id
                                                  ');

      $QcustomersGroupPrice->bindInt(':products_id', (int)$specials['products_id']);
      $QcustomersGroupPrice->bindInt(':customers_group_id', (int)$sInfo->customers_group_id);
      $QcustomersGroupPrice->execute();

      if ($scustomer_group_price = $QcustomersGroupPrice->fetch()) {
        $sInfo->products_price = $specials['products_price'] = $scustomer_group_price['customers_group_price'];
      }
?>
                <td>
<?php 
      if ($specials['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $specials['specials_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $specials['specials_id']; ?>" />
<?php 
      } 
?>
                </td>
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('products_preview.php', 'pID=' . $products['products_id']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a>'; ?></td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $specials['products_image'], $specials['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td>
                <td  class="dataTableContent"><?php echo $specials['products_model']; ?></td>
                <td  class="dataTableContent"><?php echo $specials['products_name']; ?></td>
<?php
// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {
// Ajouter afin de permettre d'afficher dans la liste des promotions deja cree le nom du groupe des clients non B2B
    $QcustomersGroup = $OSCOM_PDO->prepare('select customers_group_name,
                                                   customers_group_id
                                            from :table_customers_groups
                                            where customers_group_id = :customers_group_id
                                           ');

    $QcustomersGroup->bindInt(':customers_group_id', (int)$specials['customers_group_id']);
    $QcustomersGroup->execute();

    $customers_group = $QcustomersGroup->fetch();

    if ($specials['customers_group_id'] != 0) {
      $all_groups_name_special = $customers_group['customers_group_name'];
    } else {
      $all_groups_name_special = VISITOR_NAME;
    }
?>
                <td  class="dataTableContent"><?php echo $all_groups_name_special; ?></td>       
<?php
  }
?>
                <td  class="dataTableContent" align="left"> <?php echo  number_format(100 - (($specials['specials_new_products_price'] / $specials['products_price']) * 100)) . '%'; ?></td>
                <td  class="dataTableContent" align="left"><span class="oldPrice"><?php echo $currencies->format($specials['products_price']); ?></span><span class="specialPrice"><?php echo $currencies->format($specials['specials_new_products_price']); ?></span></td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($specials['scheduled_date']); ?></td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($specials['expires_date']); ?></td>
<?php 
       if ( $specials['flash_discount'] == '1') {
?>
                <td  class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', '', 16, 16) ; ?></td>
<?php
       } else {
?>
                <td  class="dataTableContent"></td>

<?php 
       }
       if ( $specials['products_archive'] =='1') {
?>
                <td  class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', '', 16, 16) ; ?></td>
<?php
       } else {
?>
                <td  class="dataTableContent"></td>
<?php
       }
?>
                <td  class="dataTableContent" align="center">
<?php
      if ($specials['status'] == '1') {
        echo '<a href="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . '&action=setflag&flag=0&id=' . $specials['specials_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . '&action=setflag&flag=1&id=' . $specials['specials_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
           </td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . '&sID=' . (int)$specials['specials_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($sInfo) && is_object($sInfo) && ($specials['specials_id'] == $sInfo->specials_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('specials.php', 'page=' . $_GET['page'] . '&sID=' . (int)$specials['specials_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    }
?>
              </form><!-- end form delete all -->

              <tr>
                <td colspan="13" class="smallText" valign="top"><?php echo $specials_split->display_count($specials_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SPECIALS); ?></td>
              </tr>
            </table></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

