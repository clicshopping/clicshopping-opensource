<?php
/*
 * contact_us.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

    require('includes/application_top.php');

// Recuperation des information utile pour page manager
    $page_manager->osc_display_page_manager('contact');

    require($OSCOM_Template->GeTemplatetLanguageFiles('contact_us'));

    $order_id = (int)$_GET['order_id'];

    if (isset($_GET['action']) && ($_GET['action'] == 'send') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {

      $error = false;

       $name = osc_db_prepare_input($_POST['name']);
       $email_address = osc_db_prepare_input($_POST['email']);
       $enquiry = osc_db_prepare_input($_POST['enquiry']);
       $number_email_confirmation = osc_db_prepare_input($_POST['number_email_confirmation']);
       $email_subject =  osc_db_prepare_input($_POST['email_subject']);
       $order_id = osc_db_prepare_input($_POST['order_id']);
       $send_to = osc_db_prepare_input($_POST['send_to']);
       $customer_id = osc_db_prepare_input($_POST['customer_id']);
       $customers_telephone = osc_db_prepare_input($_POST['customers_telephone']);

       if (!osc_validate_email($email_address)) {
         $error = true;

         $OSCOM_MessageStack->addError('contact', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
       }

      if (!osc_validate_number_email((int)$number_email_confirmation)) {
         $error = true;

         $OSCOM_MessageStack->addError('contact', ENTRY_EMAIL_ADDRESS_CHECK_ERROR_NUMBER);
      }

      $actionRecorder = new actionRecorder('ar_contact_us', ($OSCOM_Customer->isLoggedOn() ? $OSCOM_Customer->getID() : null), $name);

      if (!$actionRecorder->canPerform()) {
          $error = true;
          $actionRecorder->record(false);
          $OSCOM_MessageStack->addError('contact', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES') ? (int)MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES : 15)));
      }

       if ($error == false) {

          $today = date("F j, Y, g:i a");

        if (CONTACT_DEPARTMENT_LIST != '') {
          $send_to_array=explode("," ,CONTACT_DEPARTMENT_LIST);
          preg_match('/\<[^>]+\>/', $send_to_array[$send_to], $send_email_array);
          $send_to_email= preg_replace ('#>#', '', $send_email_array[0]);
          $send_to_email= preg_replace ('#<#', '', $send_to_email);

          if ($customer_id != '') {
            $num_customer_id = ENTRY_CUSTOMERS_ID . $customer_id;
          } else {
            $num_customer_id = '';
          }


          if ($OSCOM_Customer->isLoggedOn() && !empty($order_id) ) {
            $message_info_admin = ENTRY_INFORMATION_ADMIN;
          }

          $message_to_admin = $email_subject . ' ' .STORE_NAME  .  "\n\n"  .  $message_info_admin   .  "\n\n"  .  ENTRY_DATE  .  $today  .  "\n".    $num_customer_id .  "\n".  ENTRY_ORDER .  $order_id  .  "\n".  ENTRY_NAME  . $name .  "\n".  ENTRY_EMAIL . $email_address  .  "\n".  ENTRY_ENQUIRY_CUSTOMER_INFORMATION. $enquiry;
          osc_mail(preg_replace('/\<[^*]*/', '', $send_to_array[$send_to]), $send_to_email, $email_subject, $message_to_admin, $name, $email_address);
          $contact_department = $send_to_array[$send_to];

// send information to customer
          $message_to_customer = $email_subject . ' ' .STORE_NAME  .  "\n\n"  . ENTRY_DATE  .  $today .  "\n".   $num_customer_id .  "\n\n" . ENTRY_ORDER .  $order_id  .  "\n"  . ENTRY_NAME  . $name . "\n" .   ENTRY_CUSTOMERS_PHONE . $customers_telephone   . "\n"  . ENTRY_EMAIL . $email_address  . "\n"  . ENTRY_ENQUIRY_CUSTOMER . $enquiry . "\n\n"  . ENTRY_ADDITIONAL_INFORMATION . "\n\n"  . EMAIL_TEXT_LAW;
          osc_mail(STORE_OWNER, $email_address, ENTRY_EMAIL_OBJECT_CUSTOMER, $message_to_customer, $name, STORE_OWNER_EMAIL_ADDRESS);

        } else {

          $contact_department = TEXT_ADMINISTRATOR_DEPARTMENT;
          $message_to_admin = $email_subject . ' ' .STORE_NAME  .  "\n\n"  . ENTRY_DATE  .  $today .  "\n"  .  ENTRY_CUSTOMERS_ID . $customer_id .  "\n\n"  .  ENTRY_NAME  . $name . "\n" .  ENTRY_CUSTOMERS_PHONE . $customers_telephone   . "\n"  . ENTRY_EMAIL . $email_address  . "\n"  . ENTRY_ENQUIRY_CUSTOMER_INFORMATION . $enquiry;
          osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $message_to_admin, $name, $email_address);

// send information to customer
          $message_to_customer = $email_subject . ' ' .STORE_NAME  .  "\n\n"   . ENTRY_DATE  .  $today .  "\n".  ENTRY_CUSTOMERS_ID . $customer_id .  "\n\n"  .  ENTRY_NAME  . $name . "\n" .   ENTRY_CUSTOMERS_PHONE . $customers_telephone   . "\n"  . ENTRY_EMAIL . $email_address  . "\n"  . ENTRY_ENQUIRY_CUSTOMER . $enquiry  . "\n\n"  . ENTRY_ADDITIONAL_INFORMATION  . "\n\n"  . EMAIL_TEXT_LAW;
          osc_mail(STORE_OWNER, $email_address, ENTRY_EMAIL_OBJECT_CUSTOMER,  $message_to_customer, $name, STORE_OWNER_EMAIL_ADDRESS);
        }


// insert the modification in the databse
        if ($OSCOM_Customer->isLoggedOn()) {

          if ($order_id != 0) {

            $Qupdate = $OSCOM_PDO->prepare('update :table_orders 
                                            set orders_status = :orders_status,
                                            last_modified = now()
                                            where orders_id = :orders_id
                                          ');
            $Qupdate->bindValue(':orders_status', '5');
            $Qupdate->bindInt(':orders_id', (int)$order_id);
            $Qupdate->execute();


            $OSCOM_PDO->save('orders_status_history', [
                                                      'orders_id' => (int)$order_id,
                                                      'orders_status_id' => 5,
                                                      'orders_status_invoice_id' => 1,
                                                      'admin_user_name' => '<strong>' . ENTRY_CUSTOMER . ' ' . $name . '</strong>',
                                                      'date_added' => 'now()',
                                                      'customer_notified' => 1,
                                                      'comments' => $enquiry
                                                    ]
                            );


           $enquiry =  $enquiry . "\n\n" . utf8_encode(html_entity_decode(ENTRY_INFORMATION_ADMIN)) . (int)$order_id;

          } else {

            $sql_data_array = array('contact_department' => $contact_department,
                                    'contact_name' => $name,
                                    'contact_email_address' => $email_address,
                                    'contact_email_subject' => $email_subject,
                                    'contact_enquiry' => $enquiry,
                                    'languages_id' => (int)$_SESSION['languages_id'],
                                    'contact_date_added' => 'now()',
                                    'customer_id' => (int)$customer_id,
                                    'contact_telephone' => $customers_telephone,
                                   );
            $OSCOM_PDO->save('contact_customers', $sql_data_array);


//***************************************
// odoo web service
//***************************************
            if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_SUPPORT_CONTACT_US_ODOO == 'true') {
                require ('ext/odoo_xmlrpc/xml_rpc_catalog_support_customer.php');
            }
//***************************************
// End odoo web service
//***************************************

          }// end osc_session
        } elseif ($order_id == 0) {

          $sql_data_array = array('contact_department' => $contact_department,
                                   'contact_name' => $name,
                                   'contact_email_address' => $email_address,
                                   'contact_email_subject' => $email_subject,
                                   'contact_enquiry' => $enquiry,
                                   'languages_id' => (int)$_SESSION['languages_id'],
                                   'contact_date_added' => 'now()',
                                   'contact_telephone' => $customers_telephone,
                                  );
           $OSCOM_PDO->save('contact_customers', $sql_data_array);

       } // end order_id

       $actionRecorder->record();
       osc_redirect(osc_href_link('contact_us.php', 'action=success'));
     }
   }

   $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('contact_us.php'));

// number for the antispam
   $number_confirmation = '(2 + '.EMAIL_CONFIRMATION_NUMBER.') x 1';

// select multiple contact for the form
  if (CONTACT_DEPARTMENT_LIST !=''){
    foreach(explode("," ,CONTACT_DEPARTMENT_LIST) as $k => $v) {
      $send_to_array[] = array('id' => $k, 'text' => preg_replace('/\<[^*]*/', '', $v));
    }
  }

  if ($order_id != 0) {

    $QCustomers = $OSCOM_PDO->prepare('select customers_telephone
                                      from :table_orders
                                      where orders_id = :orders_id
                                     ');
    $QCustomers->bindInt(':orders_id', (int)$order_id);
    $QCustomers->execute();

    $customers_telephone = $QCustomers->value('customers_telephone');

  } else {

    $QCustomers = $OSCOM_PDO->prepare('select customers_lastname,
                                              customers_firstname,
                                              customers_email_address,
                                              customers_telephone
                                      from :table_customers
                                      where customers_id = :customers_id
                                     ');
    $QCustomers->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
    $QCustomers->execute();
    $customers = $QCustomers->fetch();

    $customers_name = $customers['customers_lastname'];
    $customers_firstname = $customers['customers_firstname'];
    $customers_email_address = $customers['customers_email_address'];
    $customers_telephone = $customers['customers_telephone'];
  }


// appel du fichier contact_us.php qui se trouve dans le template
  require($OSCOM_Template->getTemplateFiles('contact_us'));

// Duplication with includes/footer.php
  require('includes/application_bottom.php');
