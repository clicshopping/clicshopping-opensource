<?php
/**
 * newsletters.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: newsletters.php 
*/

  class newsletter {
    var $show_choose_audience, $title, $content;

    function newsletter($title, $content) {
      $this->show_choose_audience = false;
      $this->title = $title;
      $this->content = $content;
    }

    function choose_audience() {
      return false;
    }

    function confirm() {
      global $OSCOM_PDO;


//
// delete all entry in the table newsletter temp for initilization
//
      $Qdelete = $OSCOM_PDO->prepare('delete
                                      from :table_newsletter_customers_temp
                                      ');
      $Qdelete->execute();

// ----------------------
// customer with an account
// ----------------------
      if ($_GET['nlID'] == 0) {

        $Qmail = $OSCOM_PDO->prepare('select count(*) as count
                                      from :table_customers
                                      where customers_newsletter = 1
                                      and customers_group_id = :customers_group_id
                                      and customers_email_validation = 0
                                    ');
        $Qmail->bindInt(':customers_group_id', (int)$_GET['cgID']);

        $Qmail->execute();

      } else {

        $Qmail = $OSCOM_PDO->prepare('select count(*) as count
                                      from :table_customers
                                      where customers_newsletter = 1
                                      and languages_id = :languages_id
                                      and customers_group_id = :customers_group_id
                                      and customers_email_validation = 0
                                    ');
        $Qmail->bindInt(':customers_group_id', (int)$_GET['cgID']);
        $Qmail->bindInt(':languages_id', (int)$_GET['nlID'] );

        $Qmail->execute();

      }
      $mail = $Qmail->fetch();

// ----------------------
// customer without no account
// ----------------------
     if ($_GET['ana'] == 1) {
      if ($_GET['nlID'] == 0 && $_GET['cgID'] == 0)  {

        $QmailNoAccount = $OSCOM_PDO->prepare('select count(*) as count_no_account
                                               from :table_newsletter_no_account
                                               where customers_newsletter = 1
                                              ');
        $QmailNoAccount->execute();

        $mail_no_account =  $QmailNoAccount->fetch();

      } elseif ($_GET['cgID'] == 0)  {

        $QmailNoAccount = $OSCOM_PDO->prepare('select count(*) as count_no_account
                                               from :table_newsletter_no_account
                                               where customers_newsletter = 1
                                               and languages_id = :languages_id
                                              ');
        $Qmail->bindInt(':languages_id', (int)$_GET['nlID'] );

        $QmailNoAccount->execute();

        $mail_no_account =  $QmailNoAccount->fetch();
      }
    } // end $_GET['ana']

// If the b2b is selectionned, stats are no display
      if ($_GET['cgID'] != 0 && $_GET['ana'] == 0) {
        $count_customer_no_account = 0;
      } else {
        $count_customer_no_account = $mail_no_account['count_no_account'];
      }

     if($_GET['ac'] == 1) {
// newsletter file inserted in the pub directory
      if (function_exists('file_put_contents')) {
        $newsletters_id = $mail['newsletters_id'];
        $file_newsletter =  DIR_FS_DOWNLOAD_PUBLIC . 'newsletter/newsletter_'. $_GET['nID'] .'.html';
        $directory =  '<a href="'. HTTP_CATALOG_SERVER . DIR_WS_CATALOG . DIR_WS_SOURCES . 'public/newsletter/newsletter_'. $_GET['nID'] .'.html" target="_blank">'. HTTP_CATALOG_SERVER . DIR_WS_CATALOG . DIR_WS_SOURCES .'public/newsletter/newsletter_'. $_GET['nID'] .'.html</a>';
// ----------------------
// creating document
// ----------------------
        $content = '<!DOCTYPE html>
                    <html '. HTML_PARAMS .'>
                    <head>
                      <meta charset="'. CHARSET .'" />
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="robots" content="noindex,nofollow" />
                      <meta name="viewport" content="width=device-width, initial-scale=1">
                      <title>'.$this->title.'</title>
                      <meta name="Description" content ="'.$this->title.'">
                     </head>
                    <body>
                      ' . $this->content .'
                    </body>
                   </html>
                  ';
// Open the file to get existing content
        $current = @file_get_contents($file_newsletter);
 //  $current .= $content;
// Write the contents back to the file
        file_put_contents($file_newsletter, $content, LOCK_EX);
      }
      $file_name = '<strong>'.TEXT_FILE_NEWSLETTER.'</strong> newsletter_'. (int)$_GET['nID'] .'.html<br /><p style="color:#ff0000;"><strong>'.TEXT_FILE_DIRECTORIES .'</b></strong> '.$directory;
    }


// ----------------------
// Display a button if subcription is > 0
// ----------------------
    if ($mail['count'] > 0 || $count_customer_no_account > 0) {
      if (SEND_EMAILS =='true') {
        $send_button = '<span class="pull-right"><a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&nlID=' . $_GET['nlID'] . '&cgID=' . $_GET['cgID'] .'&ac=' . $_GET['ac'] .'&at=' . $_GET['at'] .'&ana=' . $_GET['ana'] .'&action=confirm_send') . '">' . osc_image_button('button_send_mail.gif', IMAGE_SEND) . '</a></span>';
      }
    }

      $confirm_string = '<div class="adminTitle">';
      $confirm_string .=  $send_button;
      $confirm_string .=  '<span class="pull-right"><a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a></span>';
      $confirm_string .=  '</div>';
      $confirm_string .=  '<div class="separator"></div>';

      $confirm_string .= '<div class="contentContainer">' . "\n" .
                         '    <p style="color:#ff0000; text-align:center;"><strong>' .sprintf(TEXT_COUNT_CUSTOMERS, $mail['count']) . '<br />' .sprintf(TEXT_COUNT_CUSTOMERS_NO_ACCOUNT, $count_customer_no_account) . '</strong><br />  '. $file_name .'  </p>' . "\n" .
                         '</div>' . "\n" .
                         '<div class="separator"></div>' . "\n" .
                         '<div class="contentContainer"><strong>' . $this->title . '</strong></div>' . "\n" .
                         '<div class="separator"></div>' . "\n" .
                         '<div class="contentContainer">' . $this->content . '</div>' . "\n" .
                         '<div class="separator"></div>';

      return $confirm_string;
    }

// Envoi du mail sans gestion de Fckeditor
    function send($newsletter_id) {
      global $OSCOM_PDO;
// ----------------------	  
// customer with account
// ----------------------
      if ($_GET['nlID'] == 0) {

        $Qmail= $OSCOM_PDO->prepare('select customers_firstname,
                                           customers_lastname,
                                           customers_email_address
                                    from :table_customers
                                    where customers_newsletter = 1
                                    and customers_group_id = :customers_group_id
                                    and customers_email_validation = 0
                                   ');
        $Qmail->bindInt(':customers_group_id', (int)$_GET['cgID']);
        $Qmail->execute();

      } else {

        $Qmail= $OSCOM_PDO->prepare('select customers_firstname,
                                           customers_lastname,
                                           customers_email_address
                                    from :table_customers
                                    where customers_newsletter = 1
                                    and languages_id = :languages_id
                                    and customers_group_id = :customers_group_id
                                    and customers_email_validation = 0
                                    ');
        $Qmail->bindInt(':customers_group_id', (int)$_GET['cgID']);
        $Qmail->bindInt(':languages_id',(int)$_GET['nlID'] );
        $Qmail->execute();

    } //end $_GET['nlID']

// ----------------------	  
//customer without_no_account
// ----------------------
     if ($_GET['ana'] == 1) {
      if ($_GET['cgID'] == 0 && $_GET['nlID'] == 0)  {
        $QmailNoAccount= $OSCOM_PDO->prepare('select customers_firstname,
                                                     customers_lastname,
                                                     customers_email_address
                                              from :table_newsletter_no_account
                                              where customers_newsletter = 1
                                              and customers_email_validation = 0
                                              ');
        $QmailNoAccount->execute();

    } elseif ($_GET['cgID'] == 0)  {
        $QmailNoAccount = $OSCOM_PDO->prepare('select customers_firstname,
                                                      customers_lastname,
                                                      customers_email_address
                                               from :table_newsletter_no_account
                                               where customers_newsletter = 1
                                               and languages_id = :languages_id
                                               and customers_email_validation = 0
                                              ');
        $QmailNoAccount->bindInt('languages_id',(int)$_GET['nlID'] );
        $Qmail->execute();
      }
    }

    $max_execution_time = 0.8 * (int)ini_get('max_execution_time');
    $time_start = explode(' ', PAGE_PARSE_START_TIME);


      $mimemessage = new email(array('X-Mailer: ClicShopping bulk'));
// ----------------------
// if the file is created
// ----------------------
      if ($_GET['ac'] == 1) {
        $mimemessage->add_text('<p align="center">'. TEXT_SEND_NEWSLETTER_EMAIL  . '</p>' .  $this->content .  TEXT_SEND_NEWSLETTER . HTTP_CATALOG_SERVER . DIR_WS_CATALOG . DIR_WS_SOURCES . 'public/newsletter/newsletter_'. $_GET['nID'] .'.html<br /><br />' . TEXT_UNSUBSCRIBE . HTTP_SERVER . DIR_WS_CATALOG . 'account_newsletters.php');
      } else {
        $mimemessage->add_text('<p align="center">'.TEXT_SEND_NEWSLETTER_EMAIL . '</p>' .  $this->content . TEXT_UNSUBSCRIBE . HTTP_SERVER . DIR_WS_CATALOG . 'account_newsletters.php');
      }
      $mimemessage->build_message();

// ------------------------------------------
// copy e-mails to a temporary table if that table is empty
// ------------------------------------------

      $Qcheck = $OSCOM_PDO->prepare('select count(customers_email_address) as num_customers_email_address
                                      from :table_newsletter_customers_temp
                                    ');
      $Qcheck->execute();

       $check_rows = $Qcheck->fetch();

       if ($check_rows['num_customers_email_address'] == 0) {

 // ------------------------------------------
 // copy customers account in temp newsletter
 // ------------------------------------------

         while($copy_customers_account = $Qmail->fetch() ) {
           if (preg_match("#^[-a-z0-9._]+@([-a-z0-9_]+\.)+[a-z]{2,6}$#i", $copy_customers_account['customers_email_address'])) {

             $OSCOM_PDO->save('newsletter_customers_temp', [
                                                             'customers_firstname' => addslashes($copy_customers_account['customers_firstname']),
                                                             'customers_lastname' => addslashes($copy_customers_account['customers_lastname']),
                                                             'customers_email_address' => $copy_customers_account['customers_email_address']
                                                           ]
                             );

           }
         }  // end while
       } else {
         echo "Il ya un pb avec la table de la base de donn&eacute;es newsletter_customers_temp, veuillez communiquer avec votre administrateur.<br />";
       }

      $QmailNewsletterAccountTemp = $OSCOM_PDO->prepare('select customers_firstname,
                                                                 customers_lastname,
                                                                 customers_email_address
                                                          from :table_newsletter_customers_temp
                                                        ');
      $QmailNewsletterAccountTemp->execute();

// ----------------------
// customer with account
// ----------------------

        while ($mail_newsletter_account_temp = $QmailNewsletterAccountTemp->fetch() ) {

        $time_end = explode(' ', microtime());
        $timer_total = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);

        if ( $timer_total > $max_execution_time ) {
//	   echo '$timer_total'. $timer_total .'<br>'; 
//	   echo '$max_execution_time'. $max_execution_time .'<br>'; 	  
//         echo 'ATTENTE AVANT LE PROCHAIN ENVOI';
          echo("<meta http-equiv=\"refresh\" content=\"12\">");
        } 

        $mimemessage->send($mail_newsletter_account_temp['customers_firstname'] . ' ' . $mail_newsletter_account_temp['customers_lastname'], $mail_newsletter_account_temp['customers_email_address'], '', EMAIL_FROM, $this->title);

// delete all entry in the table
          $Qdelete = $OSCOM_PDO->prepare('delete
                                          from :table_newsletter_customers_temp
                                          where customers_email_address = :customers_email_address
                                          ');
          $Qdelete->bindValue(':customers_email_address',  $mail_newsletter_account_temp['customers_email_address'] );
          $Qdelete->execute();
        } //end while


        $newsletter_id = osc_db_prepare_input($newsletter_id);

        $Qupdate = $OSCOM_PDO->prepare('update :table_newsletters
                                        set date_sent = now(),
                                        status = 1
                                        where newsletters_id = :newsletters_id
                                       ');
        $Qupdate->bindInt(':newsletters_id', $newsletter_id);
        $Qupdate->execute();

// ----------------------
//  customer without_no_account
// ----------------------
    if ($_GET['ana'] == 1 && $_GET['cgID'] == 0)  {

// copy e-mails to a temporary table if that table is empty
      $Qcheck = $OSCOM_PDO->prepare('select count(customers_email_address) as num_customers_email_address
                                     from :table_newsletter_customers_temp
                                    ');
      $Qcheck->execute();

      $check_rows = $Qcheck->fetch();

      if ($check_rows['num_customers_email_address'] == 0) {

        while($copy_customers_no_account = $QmailNoAccount->fetch() ) {
          if (preg_match("#^[-a-z0-9._]+@([-a-z0-9_]+\.)+[a-z]{2,6}$#i", $copy_customers_no_account['customers_email_address'])) {

            $OSCOM_PDO->save('newsletter_customers_temp', [
                                                            'customers_firstname' => addslashes($copy_customers_account['customers_firstname']),
                                                            'customers_lastname' => addslashes($copy_customers_account['customers_lastname']),
                                                            'customers_email_address' => $copy_customers_account['customers_email_address']
                                                          ]
                           );
          }
        }  // end while
      } else {
           echo "Il ya un pb avec la table de la base de donn&eacute;es newsletter_customers_temp, veuillez communiquer avec votre administrateur.<br />";
      } // end mysql num


        $QmailNewsletterNoAccountTemp = $OSCOM_PDO->prepare('select customers_firstname,
                                                                     customers_lastname,
                                                                    customers_email_address
                                                             from :table_newsletter_customers_temp
                                                           ');
        $QmailNewsletterNoAccountTemp->execute();

        while ($mail_newsletter_no_account_temp = $QmailNewsletterNoAccountTemp->fetch() ) {

            $time_end = explode(' ', microtime());
            $timer_total = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);

            if ( $timer_total > $max_execution_time ) {
//	   echo '$timer_total'. $timer_total .'<br>'; 
//	   echo '$max_execution_time'. $max_execution_time .'<br>'; 
//         echo 'ATTENTE AVANT LE PROCHAIN ENVOI';
            echo("<meta http-equiv=\"refresh\" content=\"12\">");
          } 

         $mimemessage->send($mail_newsletter_no_account_temp['customers_firstname'] . ' ' . $mail_newsletter_no_account_temp['customers_lastname'], $mail_newsletter_no_account_temp['customers_email_address'], '', EMAIL_FROM, $this->title);
// delete all entry in the table

          $Qdelete = $OSCOM_PDO->prepare('delete
                                          from :table_newsletter_customers_temp
                                          where customers_email_address = :customers_email_address
                                         ');
          $Qdelete->bindValue(':customers_email_address', $mail_newsletter_no_account_temp['customers_email_address']);
          $Qdelete->execute();

        } //end while
    } // condition $_GET['ana']

// -------------------------------------------
//               Twitter 
// -------------------------------------------
      if($_GET['ac'] == 1 && $_GET['at'] == 1)  {
        if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) { 
          $text_newsletter =  HTTP_CATALOG_SERVER . DIR_WS_CATALOG . DIR_WS_SOURCES . 'public/newsletter/newsletter_'. $_GET['nID'] .'.html'; 
          $_POST['twitter_msg'] = TEXT_TWITTER . $text_newsletter;
          echo osc_send_twitter($twitter_authentificate_administrator);      
        }
      }     
  } // end function

// ***************************************************
//                     HTML NEWSLETTER
// **************************************************

    function send_fckeditor($newsletter_id) {
      global $OSCOM_PDO;
// ----------------------	  
//customer witt account
// ----------------------
      if ($_GET['nlID'] == 0) {

        $Qmail = $OSCOM_PDO->prepare('select customers_firstname,
                                           customers_lastname,
                                           customers_email_address
                                    from :table_customers
                                    where customers_newsletter = 1
                                    and customers_group_id = :customers_group_id
                                    and customers_email_validation = 0
                                   ');
        $Qmail->bindInt(':customers_group_id',(int)$_GET['cgID']);
        $Qmail->execute();


      } else {

        $Qmail = $OSCOM_PDO->prepare('select customers_firstname,
                                           customers_lastname,
                                           customers_email_address
                                    from :table_customers
                                    where customers_newsletter = 1
                                    and languages_id = :languages_id
                                    and customers_group_id = customers_group_id
                                    and customers_email_validation = 0
                                   ');
        $Qmail->bindInt(':customers_group_id',(int)$_GET['cgID']);
        $Qmail->bindInt(':languages_id',(int)$_GET['nlID']);
        $Qmail->execute();

    }
// ----------------------	  
//customer without_no_account
// ----------------------
     if ($_GET['ana'] == 1) {
      if ($_GET['cgID'] == 0 && $_GET['nlID'] == 0)  {

        $QmailNoAccount  = $OSCOM_PDO->prepare('select customers_firstname,
                                                      customers_lastname,
                                                      customers_email_address
                                               from newsletter_no_account
                                               where customers_newsletter = 1
                                              ');

        $QmailNoAccount->execute();


      } elseif ($_GET['cgID'] == 0)  {

        $QmailNoAccount  = $OSCOM_PDO->prepare('select customers_firstname,
                                                      customers_lastname,
                                                      customers_email_address
                                               from newsletter_no_account
                                               where customers_newsletter = 1
                                               and languages_id = :languages_id
                                              ');
        $QmailNoAccount->bindInt(':languages_id', (int)$_GET['nlID']);
        $QmailNoAccount->execute();

      }
    }

      $mimemessage = new email(array('X-Mailer: ClicShopping bulk mailer'));

// ----------------------
// gestion template email
// -----------------------
      require('includes/functions/template_email.php');

      $template_email_signature = osc_get_template_email_signature($template_email_signature);
      $template_email_newsletter_footer = osc_get_template_email_newsletter_text_footer($template_email_newsletter_footer);
      $email_footer = '<br />' . $template_email_signature . '<br />'. $template_email_newsletter_footer;

      $max_execution_time = 0.8 * (int)ini_get('max_execution_time');
      $time_start = explode(' ', PAGE_PARSE_START_TIME);

      $Qcheck = $OSCOM_PDO->prepare('select count(customers_email_address) as num_customers_email_address
                                     from :table_newsletter_customers_temp
                                    ');
      $Qcheck->execute();

     $check_rows = $Qcheck->fetch();

     if ($check_rows['num_customers_email_address'] == 0) {
 // ------------------------------------------
 // copy customers account in temp newsletter
 // ------------------------------------------

        while($copy_customers_account = $Qmail->fetch() ) {
          if (preg_match("#^[-a-z0-9._]+@([-a-z0-9_]+\.)+[a-z]{2,6}$#i", $copy_customers_account['customers_email_address'])) {

            $OSCOM_PDO->save('newsletter_customers_temp', [
                                                            'customers_firstname' => addslashes($copy_customers_account['customers_firstname']),
                                                            'customers_lastname' => addslashes($copy_customers_account['customers_lastname']),
                                                            'customers_email_address' => $copy_customers_account['customers_email_address']
                                                          ]
                           );

          }
        }  // end while
       } else {
           echo "Il ya un pb avec la table de la base de donn&eacute;es newsletter_customers_temp, veuillez communiquer avec votre administrateur.<br />";
       }

      $QmailNewsletterAccountTemp = $OSCOM_PDO->prepare('select customers_firstname,
                                                                 customers_lastname,
                                                                 customers_email_address
                                                          from :table_newsletter_customers_temp
                                                       ');
      $Qcheck->execute();

      if ($_GET['ac'] == 1) {
         $message = html_entity_decode('<p align="center">' .TEXT_SEND_NEWSLETTER_EMAIL  .'</p>'.  $this->content .  TEXT_SEND_NEWSLETTER . HTTP_CATALOG_SERVER . DIR_WS_CATALOG . DIR_WS_SOURCES . 'public/newsletter/newsletter_'. $_GET['nID'] .'.html<br /><br />' . $email_footer);
      } else {
         $message = html_entity_decode('<p align="center">' .TEXT_SEND_NEWSLETTER_EMAIL  .'</p>' . $this->content . $email_footer);
      }

        $message = str_replace('src="/', 'src="' . HTTP_CATALOG_SERVER . '/', $message);
        $mimemessage->add_html_fckeditor($message);
        $mimemessage->build_message();

// ----------------------
// customer with account
// ----------------------

        while ($mail_newsletter_account_temp = $QmailNewsletterAccountTemp->fetch() ) {

          $time_end = explode(' ', microtime());
          $timer_total = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);

          if ( $timer_total > $max_execution_time ) {
//	   echo '$timer_total'. $timer_total .'<br>'; 
//	   echo '$max_execution_time'. $max_execution_time .'<br>'; 	  
//         echo 'ATTENTE AVANT LE PROCHAIN ENVOI';
            echo("<meta http-equiv=\"refresh\" content=\"12\">");
          } 

          $mimemessage->send($mail_newsletter_account_temp['customers_firstname'] . ' ' . $mail_newsletter_account_temp['customers_lastname'], $mail_newsletter_account_temp['customers_email_address'], '', EMAIL_FROM, $this->title);

// delete all entry in the table
          $Qdelete = $OSCOM_PDO->prepare('delete
                                          from :table_newsletter_customers_temp
                                          where customers_email_address = :customers_email_address
                                          ');

          $Qdelete->bindValue(':customers_email_address', $mail_newsletter_account_temp['customers_email_address']);
          $Qdelete->execute();

        } //end while

        $newsletter_id = osc_db_prepare_input($newsletter_id);

        $Qupdate = $OSCOM_PDO->prepare('update :table_newsletters
                                       set date_sent = now(),
                                       status = 1
                                       where newsletters_id = :newsletters_id
                                      ');
        $Qupdate->bindValue(':newsletters_id', $newsletter_id);
        $Qupdate->execute();

// ----------------------
//  customer without_no_account
// ----------------------
    if ($_GET['ana'] == 1 && $_GET['cgID'] == 0)  {

// copy e-mails to a temporary table if that table is empty

      $Qcheck = $OSCOM_PDO->prepare('select count(customers_email_address) as num_customers_email_address
                                    from :table_newsletter_customers_temp
                               ');
      $Qcheck->execute();

      $check_rows = $Qcheck->fetch();

      if ($check_rows['num_customers_email_address'] == 0) {

        while($copy_customers_no_account = $QmailNoAccount->fetch() ) {

          if (preg_match("#^[-a-z0-9._]+@([-a-z0-9_]+\.)+[a-z]{2,6}$#i", $copy_customers_no_account['customers_email_address'])) {

            $OSCOM_PDO->save('newsletter_customers_temp', [
                                                            'customers_firstname' => addslashes($copy_customers_account['customers_firstname']),
                                                            'customers_lastname' => addslashes($copy_customers_account['customers_lastname']),
                                                            'customers_email_address' => $copy_customers_account['customers_email_address']
                                                          ]
                            );

          }
        }  // end while
      } else {
        echo "Il ya un pb avec la table de la base de donn&eacute;es newsletter_customers_temp, veuillez communiquer avec votre administrateur.<br />";
      } // end mysql num

        $QmailNewsletterAccountTemp = $OSCOM_PDO->prepare('select customers_firstname,
                                                                    customers_lastname,
                                                                    customers_email_address
                                                           from :table_newsletter_customers_temp
                                                          ');
        $QmailNewsletterAccountTemp->execute();

        while ($mail_newsletter_no_account_temp = $QmailNewsletterAccountTemp->fetch() ) {

          $time_end = explode(' ', microtime());
          $timer_total = number_format(($time_end[1] + $time_end[0] - ($time_start[1] + $time_start[0])), 3);

          if ( $timer_total > $max_execution_time ) {
//	   echo '$timer_total'. $timer_total .'<br>'; 
//	   echo '$max_execution_time'. $max_execution_time .'<br>'; 
//         echo 'ATTENTE AVANT LE PROCHAIN ENVOI';
            echo("<meta http-equiv=\"refresh\" content=\"12\">");
          } 

          $mimemessage->send($mail_newsletter_no_account_temp['customers_firstname'] . ' ' . $mail_newsletter_no_account_temp['customers_lastname'], $mail_newsletter_no_account_temp['customers_email_address'], '', EMAIL_FROM, $this->title);
// delete all entry in the table
          $Qdelete = $OSCOM_PDO->prepare('delete
                                          from :table_newsletter_customers_temp
                                          where customers_email_address = :customers_email_address
                                          ');
          $Qdelete->bindValue(':customers_email_address',  $mail_newsletter_account_temp['customers_email_address'] );
          $Qdelete->execute();

        } //end while
    } // end $_GET['ana']

// -------------------------------------------
//               Twitter 
// -------------------------------------------
      if($_GET['ac'] == 1 && $_GET['at'] == 1)  {
        if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {	
          $text_newsletter =  HTTP_CATALOG_SERVER . DIR_WS_CATALOG . DIR_WS_SOURCES . 'public/newsletter/newsletter_'. $_GET['nID'] .'.html';				
          $_POST['twitter_msg'] = TEXT_TWITTER . $text_newsletter;
          echo osc_send_twitter($twitter_authentificate_administrator);
        }
      }
    }
  }
