<?php
/*
 * pdf.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

// Création entête du tableau des produits pour les factures
  function output_table_heading($Y_Fields_Name_position){
    global $pdf;

    $pdf->SetFillColor(245);
    $pdf->SetFont('Arial','B',8);
    $pdf->SetY($Y_Fields_Name_position);
    $pdf->SetX(6);
    $pdf->Cell(9,6, html_entity_decode(TABLE_HEADING_QTE),1,0,'C',1);
    $pdf->SetX(15);
    $pdf->Cell(27,6, html_entity_decode(TABLE_HEADING_PRODUCTS_MODEL),1,0,'C',1);
    $pdf->SetX(40);
    $pdf->Cell(103,6,TABLE_HEADING_PRODUCTS,1,0,'C',1);
    $pdf->SetX(143);
    $pdf->Cell(15,6,TABLE_HEADING_TAX,1,0,'C',1);
    $pdf->SetX(158);
    $pdf->Cell(20,6,TABLE_HEADING_PRICE_EXCLUDING_TAX,1,0,'C',1);
/*
    $pdf->SetX(138);
    $pdf->Cell(20,6,TABLE_HEADING_PRICE_INCLUDING_TAX,1,0,'C',1);
*/
    $pdf->SetX(178);
    $pdf->Cell(20,6,TABLE_HEADING_TOTAL_EXCLUDING_TAX,1,0,'C',1);
/*
    $pdf->SetX(178);
    $pdf->Cell(20,6,TABLE_HEADING_TOTAL_INCLUDING_TAX,1,0,'C',1);
*/
    $pdf->Ln();
  }

// Création entête du tableau des produits pour les bons de livraison
  function output_table_heading_packingslip($Y_Fields_Name_position){
    global $pdf;

    $pdf->SetFillColor(245);
    $pdf->SetFont('Arial','B',8);
    $pdf->SetY($Y_Fields_Name_position);
    $pdf->SetX(6);
    $pdf->Cell(14,6,utf8_decode(TABLE_HEADING_QTE),1,0,'C',1);
    $pdf->SetX(20);
    $pdf->Cell(40,6,utf8_decode(TABLE_HEADING_PRODUCTS_MODEL),1,0,'C',1);
    $pdf->SetX(60);
    $pdf->Cell(138,6,TABLE_HEADING_PRODUCTS,1,0,'C',1);
    $pdf->Ln();
  }

  function output_table_suppliers($Y_Fields_Name_position){
    global $pdf;

    $pdf->SetFillColor(245);
    $pdf->SetFont('Arial','B',8);
    $pdf->SetY($Y_Fields_Name_position);
    $pdf->SetX(6);
    $pdf->Cell(9,6,html_entity_decode(TABLE_HEADING_QTE),1,0,'C',1);
    $pdf->SetX(15);
    $pdf->Cell(27,6,html_entity_decode(TABLE_HEADING_PRODUCTS_MODEL),1,0,'C',1);
    $pdf->SetX(40);
    $pdf->Cell(78,6,TABLE_HEADING_PRODUCTS,1,0,'C',1);
    $pdf->SetX(105);
    $pdf->Cell(45,6,TABLE_HEADING_OPTIONS,1,0,'C',1);
    $pdf->SetX(150);
    $pdf->Cell(45,6,TABLE_HEADING_VALUE,1,0,'C',1);

    $pdf->Ln();
  }  

  function output_table_customers_suppliers($Y_Fields_Name_position){
    global $pdf;

    $pdf->SetFillColor(245);
    $pdf->SetFont('Arial','B',8);
    $pdf->SetY($Y_Fields_Name_position);
    $pdf->SetX(6);
    $pdf->Cell(9,6,html_entity_decode(TABLE_HEADING_QTE),1,0,'C',1);
    $pdf->SetX(15);
    $pdf->Cell(13,6,TABLE_HEADING_CUSTOMERS_ID,1,0,'C',1);
    $pdf->SetX(28);
    $pdf->Cell(45,6,TABLE_HEADING_CUSTOMERS_NAME,1,0,'C',1);
    $pdf->SetX(73);	
    $pdf->Cell(20,6,html_entity_decode(TABLE_HEADING_PRODUCTS_MODEL),1,0,'C',1);
    $pdf->SetX(93);
    $pdf->Cell(60,6,TABLE_HEADING_PRODUCTS,1,0,'C',1);
    $pdf->SetX(143);
    $pdf->Cell(30,6,TABLE_HEADING_OPTIONS,1,0,'C',1);
    $pdf->SetX(173);
    $pdf->Cell(30,6,TABLE_HEADING_VALUE,1,0,'C',1);

    $pdf->Ln();
  }  
