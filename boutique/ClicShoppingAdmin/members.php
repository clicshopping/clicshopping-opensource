<?php
/*
 * members.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

// Permettre l'utilisation de l'approbation des comptes en mode B2B
  if (MODE_B2B_B2C == 'false')  osc_redirect(osc_href_link('index.php', '', 'SSL'));

   require('includes/functions/password_funcs.php');

  if ($_GET['action']) {
    switch ($_GET['action']) {
      case 'confirmaccept':
        $customers_group_id = osc_db_prepare_input($_POST['customers_group_id']);
        $customers_id = osc_db_prepare_input($_GET['cID']);
// Recuperation des parametres facturation et livraison par defaut du groupe

          $QdefaultCustomerGroup = $OSCOM_PDO->prepare("select customers_group_id, 
                                                               group_order_taxe, 
                                                               group_payment_unallowed, 
                                                               group_shipping_unallowed   
                                                         from :table_customers_groups
                                                         where customers_group_id = :customers_group_id
                                                        ");
          $QdefaultCustomerGroup->bindInt(':customers_group_id', (int)$customers_group_id);
          $QdefaultCustomerGroup->execute();

          $default_customer_group = $QdefaultCustomerGroup->fetch();

// Approbation du membre et mise a jour de la base de donnee
          $sql_data_array = array('member_level' => '1',
                                  'customers_group_id' => (int)$customers_group_id,
                                  'customers_options_order_taxe' => $default_customer_group['group_order_taxe']);

          $OSCOM_PDO->save('customers', $sql_data_array, ['customers_id' => (int)$customers_id ] );

          $QcheckCustomer = $OSCOM_PDO->prepare("select customers_firstname, 
                                                       customers_lastname, 
                                                       customers_password, 
                                                       customers_id, 
                                                       customers_email_address  
                                                 from :table_customers
                                                 where customers_id = :customers_id
                                                ");
          $QcheckCustomer->bindInt(':customers_id', (int)$_GET['cID']);
          $QcheckCustomer->execute();

          $check_customer = $QcheckCustomer->fetch();


// Cryptage du mot de passe
          $newpass = osc_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);

          $crypted_password = osc_encrypt_password($newpass);

          $Qupdate = $OSCOM_PDO->prepare('update :table_customers
                                          set customers_password = :customers_password
                                           where customers_id = :customers_id
                                        ');

          $Qupdate->bindValue(':customers_password', $crypted_password );
          $Qupdate->bindInt(':customers_id', (int)$check_customer['customers_id']);

          $Qupdate->execute();

          if (COUPON_CUSTOMER_B2B != '') {
            $email_coupon = EMAIL_TEXT_COUPON . COUPON_CUSTOMER_B2B;
          }


          $email_coupon = html_entity_decode($email_coupon);
          $text_password_body = html_entity_decode(EMAIL_PASSWORD_REMINDER_BODY);
          $email_text_subject = html_entity_decode(EMAIL_TEXT_SUBJECT);
          $email_text_confirm = html_entity_decode(EMAIL_TEXT_CONFIRM);
          $email_text .= $email_text_confirm . $email_coupon . $email_warning;

// mails avec le mot de passe
          osc_mail($check_customer['customers_firstname'], $check_customer['customers_email_address'], $email_text_subject, $email_text, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, '');
          osc_mail($check_customer['customers_firstname'] .' ' . $check_customer['customers_lastname'], $check_customer['customers_email_address'], $email_text_subject, $email_password_body .'</br />'. nl2br(sprintf($text_password_body, $check_customer['customers_email_address'],$newpass )), STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

          $_GET['action'] = '';
          $_GET['cID'] = '';
      break;

      case 'deleteconfirm':

        $customers_id = osc_db_prepare_input($_GET['cID']);

        if ($_POST['delete_reviews'] == 'on') {

          $Qreviews = $OSCOM_PDO->prepare('select reviews_id
                                           from :table_reviews
                                           where customers_id = :customers_id
                                           ');
          $Qreviews->bindInt(':customers_id',(int)osc_db_input($customers_id));
          $Qreviews->execute();

          while ($reviews = $Qreviews->fetch() ) {

            $Qdelete = $OSCOM_PDO->prepare('delete
                                           from :table_reviews_description
                                           where reviews_id = :reviews_id
                                          ');
            $Qdelete->bindInt(':reviews_id',  (int)$reviews['reviews_id']);
            $Qdelete->execute();
          }

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_reviews
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

        } else {

          $Qupdate = $OSCOM_PDO->prepare('update :table_reviews
                                          set customers_id = :customers_id,
                                           where customers_id = :customers_id1
                                        ');

          $Qupdate->bindValue(':customers_id', null);
          $Qupdate->bindInt(':customers_id1', (int)$customers_id);

          $Qupdate->execute();
        }


          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_address_book
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_customers
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_customers_info
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_customers_basket
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_customers_basket_attributes
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

          $Qdelete = $OSCOM_PDO->prepare('delete 
                                          from :table_whos_online
                                          where customers_id = :customers_id
                                        ');
          $Qdelete->bindInt(':customers_id', (int)$customers_id);
          $Qdelete->execute();

        osc_redirect(osc_href_link('members.php', osc_get_all_get_params(array('cID', 'action'))));
        break;
    }
  }

  require('includes/header.php');
?>

<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/client_attente.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (MEMBER == 'true') {
?>
  <span class="pull-right">
    <div class="form-group">
      <div class="controls">
<?php
  echo osc_draw_form('search', 'customers.php', '', 'get');
  echo  osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');
?>
        </form>
      </div>
    </div>
  </span>
<?php
  }
?>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
  if (MEMBER == 'true') {
?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LASTNAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIRSTNAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COMPANY; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACCOUNT_CREATED; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $search = '';
    if ( ($_GET['search']) && (osc_not_null($_GET['search'])) ) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "where c.customers_lastname like '%" . $keywords . "%' 
                  or c.customers_firstname like '%" . $keywords . "%' 
                  or c.customers_email_address like '%" . $keywords . "'
                  and member_level = '0'
                ";
    }

    if ($search =='') $search = "where member_level = '0'";

    $customers_query_raw = "select c.customers_id, 
                                   c.customers_lastname, 
                                   c.customers_firstname, 
                                   a.entry_company, 
                                   c.customers_email_address, 
                                   a.entry_country_id, 
                                   c.customers_group_id 
                            from customers c left join address_book a on c.customers_id = a.customers_id
                            and c.customers_default_address_id = a.address_book_id 
                            " . $search . " 
                            order by c.customers_lastname, 
                            c.customers_firstname
                           ";
    $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $customers_query_raw, $customers_query_numrows);
    $customers_query = osc_db_query($customers_query_raw);

    while ($customers = osc_db_fetch_array($customers_query)) {
      
      $Qinfo = $OSCOM_PDO->prepare("select customers_info_date_account_created as date_account_created,
                                           customers_info_date_account_last_modified as date_account_last_modified,
                                           customers_info_date_of_last_logon as date_last_logon,
                                           customers_info_number_of_logons as number_of_logons 
                                     from :table_customers_info
                                     where customers_info_id = :customers_info_id
                                    ");
      $Qinfo->bindInt(':customers_info_id', (int)$customers['customers_id']);
      $Qinfo->execute();

      $info = $Qinfo->fetch();


      if (((!$_GET['cID']) || (@$_GET['cID'] == $customers['customers_id'])) && (!$cInfo)) {

        $Qcountry = $OSCOM_PDO->prepare("select countries_name 
                                         from :table_countries
                                         where countries_id = :countries_id
                                        ");
        $Qcountry->bindInt(':countries_id', (int)$customers['entry_country_id']);
        $Qcountry->execute();

        $country = $Qcountry->fetch();

        $Qreviews = $OSCOM_PDO->prepare("select count(*) as number_of_reviews
                                         from :table_reviews
                                         where customers_id = :customers_id
                                        ");
        $Qreviews->bindInt(':customers_id', (int)$customers['customers_id']);
        $Qreviews->execute();

        $reviews = $Qreviews->fetch();

        $customer_info = array_merge((array)$country, (array)$info, (array)$reviews);

        $cInfo_array = array_merge((array)$customers, (array)$customer_info);
        $cInfo = new objectInfo($cInfo_array);
	  }

      if ((is_object($cInfo)) && ($customers['customers_id'] == $cInfo->customers_id)) {
        echo '          <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . osc_href_link('members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=edit') . '\'">' . "\n";
      } else {
        echo '          <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . osc_href_link('members.php', osc_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '\'">' . "\n";
      }
?>
                <td class="dataTableContent"><?php echo $customers['customers_lastname']; ?></td>
                <td class="dataTableContent"><?php echo $customers['customers_firstname']; ?></td>
                <td class="dataTableContent"><?php echo $customers['entry_company']; ?></td>
                <td class="dataTableContent" align="right"><?php echo $OSCOM_Date->getShort($info['date_account_created']); ?></td>
                <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers['customers_id'] . '&action=accept') . '">' . osc_image(DIR_WS_ICONS . 'activate.gif', IMAGE_ACTIVATE) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('customers.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers['customers_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_CUSTOMER) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('mail.php', 'selected_box=tools&customer=' . $customers['customers_email_address']) . '">' . osc_image(DIR_WS_ICONS . 'email.gif', IMAGE_EMAIL) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers['customers_id'] . '&action=confirm') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      if ( (is_object($cInfo)) && ($customers['customers_id'] == $cInfo->customers_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('members.php', osc_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr></form>
<?php
    }
?>
              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
                    <td class="smallText" align="right"><?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], osc_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></td>
                  </tr>
<?php
    if (osc_not_null($_GET['search'])) {
?>
                  <tr>
                    <td align="right" colspan="2"><?php echo '<a href="' . osc_href_link('customers.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></td>
                  </tr>
<?php
    }
?>
                </table></td>
              </tr>
            </table></td>
<?php
    require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . 'customers.php');
    $heading = array();
    $contents = array();
    switch ($_GET['action']) {
      case 'confirm':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CUSTOMER . '</strong>');
        $contents = array('form' => osc_draw_form('customers', 'members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=deleteconfirm'));
        $contents[] = array('text' => TEXT_DELETE_INTRO . '<br /><br /><strong>' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname . '</strong>');
        if ($cInfo->number_of_reviews > 0) $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('delete_reviews', 'on', true) . ' ' . sprintf(TEXT_DELETE_REVIEWS, $cInfo->number_of_reviews));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
        break;

      case 'accept':
        $email = STORE_NAME . "\n" . EMAIL_SEPARATOR . "\n" . EMAIL_TEXT_CONFIRM . "\n\n" . EMAIL_CONTACT . "\n\n" . EMAIL_WARNING . "\n\n";
        $heading[] = array('text' => '<strong>' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname . '</strong>');
        $contents = array('form' => osc_draw_form('customers', 'members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=confirmaccept'));
        $contents[] = array('align' => 'center', 'text' => '<br />'. osc_draw_pull_down_menu('customers_group_id', osc_get_customers_group(''. VISITOR_NAME .''), $cInfo->customers_group_id) .'<br />');
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_activate.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('members.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id) . '"">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;
      default:
      break;
    }

    if ((osc_not_null($heading)) && (osc_not_null($contents))) {
      echo '            <td width="25%" valign="top">' . "\n";
      $box = new box;
      echo $box->infoBox($heading, $contents);
      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>

<?php
  } else {
?>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminform">
        <tr>
          <td class="main"><?php echo MEMBER_DEACTIVATED; ?></td>
        </tr>
      </table></td>
<?php
  }
?>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
