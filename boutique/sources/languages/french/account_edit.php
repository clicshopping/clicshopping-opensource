<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Editer mon compte');
define('HEADING_TITLE', 'Informations de mon compte');

define('MY_ACCOUNT_TITLE', 'Mon compte');

define('SUCCESS_ACCOUNT_UPDATED', 'Votre compte a été mis à jour avec succès.');

define('ERROR_ADDRESS_MODIFY_COMPANY', 'Vous n\'êtes pas autorisé à modifier les informations de votre société. N\'hésitez pas à nous contacter pour recevoir plus d\'informations sur cette impossibilité.');
?>