<?php
/**
 *
 * In order to minimize the number and size of HTTP requests for CSS content,
 * this script combines multiple CSS files into a single file and compresses
 * it on-the-fly.
 * 
 * To use this in your HTML, link to it in the usual way:
 * <link rel="stylesheet" type="text/css" media="screen, print, projection" href="/css/compressed.css.php" />
 */

/* Add your CSS files to this array (THESE ARE ONLY EXAMPLES) */
/* all the css doesn't work fine */


 function get_files($root_dir, $all_data=array()) {
// only include files with these extensions
   $allow_extensions = array("css");
// make any specific files you wish to be excluded
   $ignore_files = array('general/stylesheet.css',
                          'general/stylesheet_responsive.css',
                          'general/link_general.css',
                          'general/link_general_responsive.css',
                          'general/message_general.css',
                          'general/message_general_responsive.css',
                          'general/table_general.css',
                          'general/table_general_responsive.css',
                          'general/infoboxes_general.css',
                          'general/infoboxes_general_responsive.css',
                          'general/sudoslider.css',
                          'general/bootstrap_customize.css'
                        );

    $ignore_regex = '/^_/';
// skip these directories
    $ignore_dirs = array(".", "..");

// run through content of root directory
   $dir_content = scandir($root_dir);
   foreach($dir_content as $key => $content) {
      $path = $root_dir.'/'.$content;
      if(is_file($path) && is_readable($path)) {
// skip ignored files
        if(!in_array($content, $ignore_files)) {
          if (preg_match($ignore_regex,$content) == 0) {
            $content_chunks = explode(".",$content);
            $ext = $content_chunks[count($content_chunks) - 1];
// only include files with desired extensions
            if (in_array($ext, $allow_extensions)) {
// save file name with path
                $all_data[] = $path;   
            }
          }
        }
      }
// if content is a directory and readable, add path and name
      elseif(is_dir($path) && is_readable($path)) {
 // skip any ignored dirs
        if(!in_array($content, $ignore_dirs)) {
// recursive callback to open new directory
          $all_data = get_files($path, $all_data);
        }
      }
   } // end foreach
    return $all_data;
 } // end get_files()


  $root_dir = realpath( dirname( __FILE__ ) );

  $files_array = get_files($root_dir);
  $files_css_replace = str_replace ( $root_dir .'/', '', $files_array);
  $cssFilesaddon = $files_css_replace;


  $cssFiles = array('general/stylesheet.css',
                    'general/stylesheet_responsive.css',
                    'general/link_general.css',
                    'general/link_general_responsive.css',
                    'general/message_general.css',
                    'general/message_general_responsive.css',
                    'general/table_general.css',
                    'general/table_general_responsive.css',
                    'general/infoboxes_general.css',
                    'general/infoboxes_general_responsive.css',
                    'general/sudoslider.css',
                    'general/ui_jquery.css',
                    'general/ui_jquery_responsive.css',
                    'general/bootstrap_customize.css'
                  );

  /**
   * Ideally, you wouldn't need to change any code beyond this point.
   */

  $cssFiles = array_merge($cssFiles, $cssFilesaddon);

  $buffer = "";
  foreach ($cssFiles as $cssFile) {
    $buffer .= file_get_contents($cssFile);
  }

  // Remove comments
  $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);

  // Remove space after colons
  $buffer = str_replace(': ', ':', $buffer);

  // Remove whitespace
  $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

  // Enable GZip encoding.
  ob_start("ob_gzhandler");

  // Enable caching
  header('Cache-Control: public');

  // Expire in one day
  header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 86400) . ' GMT');
  // Set the correct MIME type, because Apache won't set it for us
  header("Content-type: text/css");

  // Write everything out
  echo($buffer);
?>
