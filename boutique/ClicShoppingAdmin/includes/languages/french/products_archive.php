<?php
/**
 * Product_archive.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE','Gestion des produits archivés');
define('HEADING_TITLE_SEARCH','Rechercher');

define('TABLE_HEADING_MODEL_ARCHIVES','Référence');
define('TABLE_HEADING_PRODUCTS_ARCHIVES','Nom des produits archivés');
define('TABLE_HEADING_DATE_ARCHIVES','Date de mise en archive');
define('TABLE_HEADING_STATUS','Visibilité du produit');
define('TABLE_HEADING_ACTION','Actions');
define('TEXT_HEADING_EDIT_PRODUCTS_ARCHIVE','Ré-activation du produit');
define('TEXT_EDIT_INTRO','Souhaitez vous ré-activer ce produit ?');
define('TEXT_ARCHIVE','<strong>Note : </strong>la réactivation de l\'article fera ré-apparaitre le produit dans le catalogue des produits de l\'admin à la même place ou il était précédement.<br /> En fonction du statut, celui-ci peut apparaitre ou pas dans la partie catalogue cité client');
?>