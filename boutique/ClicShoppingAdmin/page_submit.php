<?php
/**
 * page_submit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $languages = osc_get_languages();

  if (osc_not_null($action)) {
    switch ($action) {
      case 'update':

// Language	
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {

            $submit_defaut_language_title     = osc_db_prepare_input($_POST['submit_defaut_language_title_'.$languages[$i]['id']]);
            $submit_defaut_language_keywords = osc_db_prepare_input($_POST['submit_defaut_language_keywords_'.$languages[$i]['id']]);
            $submit_defaut_language_description = osc_db_prepare_input($_POST['submit_defaut_language_description_'.$languages[$i]['id']]);
            $submit_defaut_language_footer = osc_db_prepare_input($_POST['submit_defaut_language_footer_'.$languages[$i]['id']]);
            $submit_language_products_info_title = osc_db_prepare_input($_POST['submit_language_products_info_title_'.$languages[$i]['id']]);
            $submit_language_products_info_keywords = osc_db_prepare_input($_POST['submit_language_products_info_keywords_'.$languages[$i]['id']]);
            $submit_language_products_info_description = osc_db_prepare_input($_POST['submit_language_products_info_description_'.$languages[$i]['id']]);
            $submit_language_products_new_title  = osc_db_prepare_input($_POST['submit_language_products_new_title_'.$languages[$i]['id']]);
            $submit_language_products_new_keywords  = osc_db_prepare_input($_POST['submit_language_products_new_keywords_'.$languages[$i]['id']]);
            $submit_language_products_new_description	= osc_db_prepare_input($_POST['submit_language_products_new_description_'.$languages[$i]['id']]);
            $submit_language_special_title = osc_db_prepare_input($_POST['submit_language_special_title_'.$languages[$i]['id']]);
            $submit_language_special_keywords  = osc_db_prepare_input($_POST['submit_language_special_keywords_'.$languages[$i]['id']]);
            $submit_language_special_description  = osc_db_prepare_input($_POST['submit_language_special_description_'.$languages[$i]['id']]);
            $submit_language_reviews_title = osc_db_prepare_input($_POST['submit_language_reviews_title_'.$languages[$i]['id']]);
            $submit_language_reviews_keywords  = osc_db_prepare_input($_POST['submit_language_reviews_keywords_'.$languages[$i]['id']]);
            $submit_language_reviews_description  = osc_db_prepare_input($_POST['submit_language_reviews_description_'.$languages[$i]['id']]);

            $sql_data_array_pages = array('submit_display_title_default'  => $submit_display_title_default,
                                          'submit_display_keywords_default'  => $submit_display_keywords_default,
                                          'submit_display_description_default'  => $submit_display_description_default,
                                          'submit_display_title_products_info' => $submit_display_title_products_info,
                                          'submit_display_keywords_products_info'  => $submit_display_keywords_products_info,
                                          'submit_display_description_products_info'  => $submit_display_description_products_info,
                                          'submit_display_title_products_new' => $submit_display_title_products_new,
                                          'submit_display_keywords_products_new'  => $submit_display_keywords_products_new,
                                          'submit_display_description_products_new'   => $submit_display_description_products_new,
                                          'submit_display_title_special' => $submit_display_title_special,
                                          'submit_display_keywords_special'  => $submit_display_keywords_special,
                                          'submit_display_description_special' => $submit_display_description_special,
                                          'submit_display_title_reviews'   => $submit_display_title_reviews,
                                          'submit_display_keywords_reviews'  => $submit_display_keywords_reviews,
                                          'submit_display_description_reviews' => $submit_display_description_reviews
                               );

            $sql_data_array_pages_description = array( 'submit_defaut_language_title'                 => $submit_defaut_language_title,
                                                       'submit_defaut_language_keywords'              => $submit_defaut_language_keywords,
                                                       'submit_defaut_language_description'           => $submit_defaut_language_description,
                                                       'submit_defaut_language_footer'                => $submit_defaut_language_footer,
                                                       'submit_language_products_info_title'          => $submit_language_products_info_title,
                                                       'submit_language_products_info_keywords'       => $submit_language_products_info_keywords,
                                                       'submit_language_products_info_description'    => $submit_language_products_info_description,
                                                       'submit_language_products_new_title'           => $submit_language_products_new_title,
                                                       'submit_language_products_new_keywords'        => $submit_language_products_new_keywords,
                                                       'submit_language_products_new_description'     => $submit_language_products_new_description,
                                                       'submit_language_special_title'                => $submit_language_special_title,
                                                       'submit_language_special_keywords'             => $submit_language_special_keywords,
                                                       'submit_language_special_description'          => $submit_language_special_description,
                                                       'submit_language_reviews_title'                => $submit_language_reviews_title,
                                                       'submit_language_reviews_keywords'             => $submit_language_reviews_keywords,
                                                       'submit_language_reviews_description'          => $submit_language_reviews_description
                                                      );

            if ($action == 'update') {

              $Qcheck = $OSCOM_PDO->prepare('select count(*) as countrecords
                                             from :table_submit_description
                                             where submit_id = 1
                                             and language_id = :language_id
                                            ');
              $Qcheck->bindInt(':language_id', (int)$languages[$i]['id'] );
              $Qcheck->execute();

              if($Qcheck->rowCount() >= '1' )  {

                $OSCOM_PDO->save('submit_description', $sql_data_array_pages_description, ['submit_id' => 1,
                                                                                           'language_id' => (int)$languages[$i]['id']
                                                                                          ]
                                );

              } else {
                $pageid_merge= array('submit_id' => $submit_id,
                                     'language_id' => $languages[$i]['id']
                                     );

                $sql_data_array_pages_desc = array_merge($sql_data_array_pages_description, $pageid_merge);

                $OSCOM_PDO->save('submit_description', $sql_data_array_pages_desc );

              }

            }
          } //for

          osc_redirect(osc_href_link('page_submit.php'));
      break;
    }
  }

  require('includes/header.php');
?>
   <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/referencement.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  $form_action = 'update';
?>
    <form name="submit_engine" <?php echo 'action="' . osc_href_link('page_submit.php', osc_get_all_get_params(array('action')) . 'action=' . $form_action) . '"'; ?> method="post">
      <span class="pull-right"><?php echo  osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<?php
      $Qpage = $OSCOM_PDO->prepare('select  p.submit_id,
                                            p.language_id,
                                            p.submit_defaut_language_title,
                                            p.submit_defaut_language_keywords,
                                            p.submit_defaut_language_description,
                                            p.submit_defaut_language_footer,
                                            p.submit_language_products_info_title,
                                            p.submit_language_products_info_keywords,
                                            p.submit_language_products_info_description,
                                            p.submit_language_products_new_title,
                                            p.submit_language_products_new_keywords,
                                            p.submit_language_products_new_description,
                                            p.submit_language_special_title,
                                            p.submit_language_special_keywords,
                                            p.submit_language_special_description,
                                            p.submit_language_reviews_title,
                                            p.submit_language_reviews_keywords,
                                            p.submit_language_reviews_description
                                   from :table_submit_description p
                                   where  p.submit_id = 1
                                  ');
      $Qpage->execute();

      while($page = $Qpage->fetch())  {

        $languageid = $page['language_id'];
        $submit_defaut_language_title[$languageid]  = $page['submit_defaut_language_title'];
        $submit_defaut_language_keywords[$languageid]  = $page['submit_defaut_language_keywords'];
        $submit_defaut_language_description[$languageid]  = $page['submit_defaut_language_description'];
        $submit_defaut_language_footer[$languageid]  = $page['submit_defaut_language_footer'];
        $submit_language_products_info_title[$languageid]  = $page['submit_language_products_info_title'];
        $submit_language_products_info_keywords[$languageid] = $page['submit_language_products_info_keywords'];
        $submit_language_products_info_description[$languageid] = $page['submit_language_products_info_description'];
        $submit_language_products_new_title[$languageid] = $page['submit_language_products_new_title'];
        $submit_language_products_new_keywords[$languageid]  = $page['submit_language_products_new_keywords'];
        $submit_language_products_new_description[$languageid] 	= $page['submit_language_products_new_description'];
        $submit_language_special_title[$languageid]  = $page['submit_language_special_title'];
        $submit_language_special_keywords[$languageid] = $page['submit_language_special_keywords'];
        $submit_language_special_description[$languageid] = $page['submit_language_special_description'];
        $submit_language_reviews_title[$languageid]  = $page['submit_language_reviews_title'];
        $submit_language_reviews_keywords[$languageid]  = $page['submit_language_reviews_keywords'];
        $submit_language_reviews_description [$languageid] = $page['submit_language_reviews_description'];
      }


?>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General de la Page Accueil          //-->
<!-- ------------------------------------------------------------ //-->

<!-- decompte caracteres -->

<script type="text/javascript">
  $(document).ready(function(){
<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
    //default title
    $("#default_title_<?php echo $i; ?>").charCount({
      allowed: 70,
      warning: 20,
      counterText: ' Max : '
    });

    //default_description
    $("#default_description_<?php echo $i; ?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

    //products_info title
    $("#products_info_title_<?php echo $i; ?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

    //products_info description
    $("#products_info_description_<?php echo $i; ?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

//products_new title
    $("#products_new_title_<?php echo $i; ?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '
    });

// products_new description
    $("#products_new_description_<?php echo $i; ?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

//specials title
    $("#specials_title_<?php echo $i; ?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '
    });

//specials description
    $("#specials_description_<?php echo $i; ?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

//reviews title
    $("#reviews_title_<?php echo $i; ?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '
    });

//reviews description
    $("#reviews_description_<?php echo $i; ?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });
<?php
  }
?>
  });
</script>

     <?php echo osc_draw_separator('pixel_trans.gif', '2', '10'); ?>
     <div  style="padding-left: 10px";>
       <ul class="nav nav-tabs" role="tablist"  id="myTab">
         <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_SUBMIT_DEFAULT; ?></a></li>
         <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_SUBMIT_PRODUCTS_INFO; ?></a></li>
         <li><a href="#tab3" role="tab" data-toggle="tab"><?php echo TAB_SUBMIT_PRODUCTS_NEW; ?></a></li>
         <li><a href="#tab4" role="tab" data-toggle="tab"><?php echo TAB_SUBMIT_SPECIAL; ?></a></li>
         <li><a href="#tab5" role="tab" data-toggle="tab"><?php echo TAB_SUBMIT_REVIEWS; ?></a></li>
       </ul>

       <div class="tabsClicShopping">
         <div class="tab-content">
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General                          //-->
<!-- ------------------------------------------------------------ //-->
          <div class="tab-pane active" id="tab1">
            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TEXT_PAGES_SUBMIT_INFORMATION_DEFAULT; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-3"></span>
                  <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                  <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                </div>
              </div>
<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                <div class="row">
                  <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_DEFAUT_LANGUAGE_TITLE; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_input_field('submit_defaut_language_title_'.$languages[$i]['id'], $submit_defaut_language_title[$languages[$i]['id']], 'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_DEFAUT_LANGUAGE_DESCRIPTION; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_defaut_language_description_'.$languages[$i]['id'],  'soft', '75', '2', (isset($submit_defaut_language_description[$languages[$i]['id']]) ? $submit_defaut_language_description[$languages[$i]['id']] : $submit_defaut_language_description),'id="default_description_'.$i.'"'); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo TEXT_SUBMIT_DEFAUT_LANGUAGE_KEYWORDS; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_defaut_language_keywords_'.$languages[$i]['id'],  'soft', '75', '5', (isset($submit_defaut_language_keywords[$languages[$i]['id']]) ? $submit_defaut_language_keywords[$languages[$i]['id']] : $submit_defaut_language_keywords )); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo TEXT_SUBMIT_DEFAUT_LANGUAGE_FOOTER; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_defaut_language_footer_'.$languages[$i]['id'],  'soft', '75', '5', (isset($submit_defaut_language_footer[$languages[$i]['id']]) ? $submit_defaut_language_footer[$languages[$i]['id']] : $submit_defaut_language_footer )); ?></span>
                  </div>
                </div>
<?php
  }
?>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="adminformAide">
                <div class="row">
                  <span class="col-md-12">
                    <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                    <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                  </span>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                </div>
              </div>
            </div>
          </div>

<!-- ------------------------------------------------------------ //-->
<!--     ONGLET Information General de page information produit   //-->
<!-- ------------------------------------------------------------ //-->
         <div class="tab-pane" id="tab2">

           <div class="col-md-12 mainTitle">
             <div class="pull-left"><?php echo TEXT_PAGES_SUBMIT_INFORMATION_PRODUCT_INFO; ?></div>
           </div>
           <div class="adminformTitle">
             <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
             <div class="row">
               <div class="col-md-12">
                 <span class="col-md-3"></span>
                 <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                 <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
               </div>
             </div>
             <?php
             for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
               ?>
               <div class="row">
                 <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
               </div>
               <div class="row">
                 <div class="col-md-12">
                   <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_TITLE; ?></span>
                   <span  class="col-md-6"><?php echo osc_draw_input_field('submit_language_products_info_title_'.$languages[$i]['id'], $submit_language_products_info_title[$languages[$i]['id']], 'maxlength="50" size="77" id="products_info_title_'.$i.'"', false); ?></span>
                 </div>
               </div>
               <div class="spaceRow"></div>
               <div class="row">
                 <div class="col-md-12">
                   <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_DESCRIPTION; ?></span>
                   <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_products_info_description_'.$languages[$i]['id'],  'soft', '75', '2', (isset($submit_language_products_info_description[$languages[$i]['id']]) ? $submit_language_products_info_description[$languages[$i]['id']] : $submit_language_products_info_description ),'id="products_info_description_'.$i.'"'); ?></span>
                 </div>
               </div>
               <div class="spaceRow"></div>
               <div class="row">
                 <div class="col-md-12">
                   <span class="col-md-2 centerInputFields"><?php echo TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_KEYWORDS; ?></span>
                   <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_products_info_keywords_'.$languages[$i]['id'],  'soft', '75', '5', (isset($submit_language_products_info_keywords[$languages[$i]['id']]) ? $submit_language_products_info_keywords[$languages[$i]['id']] : $submit_language_products_info_keywords)); ?></span>
                 </div>
               </div>

<?php
  }
?>
             <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
             <div class="adminformAide">
               <div class="row">
                  <span class="col-md-12">
                    <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                    <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                  </span>
               </div>
               <div class="spaceRow"></div>
               <div class="row">
                 <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
               </div>
             </div>
           </div>
         </div>

<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General de la page nouveautes          //-->
<!-- ------------------------------------------------------------ //-->
          <div class="tab-pane" id="tab3">

            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TEXT_PAGES_SUBMIT_INFORMATION_PRODUCTS_NEW; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-3"></span>
                  <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                  <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                </div>
              </div>
<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                <div class="row">
                  <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_TITLE; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_input_field('submit_language_products_new_title_'.$languages[$i]['id'], $submit_language_products_new_title[$languages[$i]['id']], 'maxlength="50" size="77" id="products_new_title_'.$i.'"', false); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_DESCRIPTION; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_products_new_description_'.$languages[$i]['id'],  'soft', '75', '2', (isset($submit_language_products_new_description[$languages[$i]['id']]) ? $submit_language_products_new_description[$languages[$i]['id']] : $submit_language_products_new_description),'id="products_new_description_'.$i.'"'); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_KEYWORDS; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_products_new_keywords_'.$languages[$i]['id'],  'soft', '75', '5', (isset($submit_language_products_new_keywords[$languages[$i]['id']]) ? $submit_language_products_new_keywords[$languages[$i]['id']] : $submit_language_products_new_keywords)); ?></span>
                  </div>
                </div>
<?php
  }
?>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="adminformAide">
                <div class="row">
                  <span class="col-md-12">
                    <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                    <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                  </span>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                </div>
              </div>
            </div>
          </div>


<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General de la page promotion          //-->
<!-- ------------------------------------------------------------ //-->
          <div class="tab-pane" id="tab4">

            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TEXT_PAGES_SUBMIT_INFORMATION_SPECIAL; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-3"></span>
                  <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                  <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                </div>
              </div>
<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                <div class="row">
                  <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_SPECIAL_TITLE; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_input_field('submit_language_special_title_'.$languages[$i]['id'], $submit_language_special_title[$languages[$i]['id']], 'maxlength="50" size="77" id="specials_title_'.$i.'"', false); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_SPECIAL_DESCRIPTION; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_special_description_'.$languages[$i]['id'],  'soft', '75', '2', (isset($submit_language_special_description[$languages[$i]['id']]) ? $submit_language_special_description[$languages[$i]['id']] : $submit_language_special_description),'id="specials_description_'.$i.'"'); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo TEXT_SUBMIT_LANGUAGE_SPECIAL_KEYWORDS; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_special_keywords_'.$languages[$i]['id'],  'soft', '75', '5', (isset($submit_language_special_keywords[$languages[$i]['id']]) ? $submit_language_special_keywords[$languages[$i]['id']] : $submit_language_special_keywords)); ?></span>
                  </div>
                </div>
<?php
  }
?>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="adminformAide">
                <div class="row">
                  <span class="col-md-12">
                    <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                    <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                  </span>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                </div>
              </div>
            </div>
          </div>

<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information  Commentaires                    //-->
<!-- ------------------------------------------------------------ //-->
          <div class="tab-pane" id="tab5">

            <div class="col-md-12 mainTitle">
              <div class="pull-left"><?php echo TEXT_PAGES_SUBMIT_INFORMATION_REVIEWS; ?></div>
            </div>
            <div class="adminformTitle">
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="row">
                <div class="col-md-12">
                  <span class="col-md-3"></span>
                  <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                  <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                </div>
              </div>
<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                <div class="row">
                  <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_REVIEWS_TITLE; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_input_field('submit_language_reviews_title_'.$languages[$i]['id'], $submit_language_reviews_title[$languages[$i]['id']], 'maxlength="50" size="77" id="reviews_title_'.$i.'"', false); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo  TEXT_SUBMIT_LANGUAGE_REVIEWS_DESCRIPTION; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_reviews_description_'.$languages[$i]['id'],  'soft', '75', '2', (isset($submit_language_reviews_description[$languages[$i]['id']]) ? $submit_language_reviews_description[$languages[$i]['id']] : $submit_language_reviews_description), 'id="reviews_description_'.$i.'"'); ?></span>
                  </div>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <div class="col-md-12">
                    <span class="col-md-2 centerInputFields"><?php echo TEXT_SUBMIT_LANGUAGE_REVIEWS_KEYWORDS; ?></span>
                    <span  class="col-md-6"><?php echo osc_draw_textarea_field('submit_language_reviews_keywords_'.$languages[$i]['id'],  'soft', '75', '5', (isset($submit_language_reviews_keywords[$languages[$i]['id']]) ? $submit_language_reviews_keywords[$languages[$i]['id']] : $submit_language_reviews_keywords)); ?></span>
                  </div>
                </div>
<?php
  }
?>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
              <div class="adminformAide">
                <div class="row">
                  <span class="col-md-12">
                    <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                    <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                  </span>
                </div>
                <div class="spaceRow"></div>
                <div class="row">
                  <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</form>
<!-- body_eof //-->
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
