<?php
/**
 * fo_footer_header_tag.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 1.0
*/

  define('MODULES_FOOTER_SUFFIX_COPYRIGHT_TITLE', 'Souhaitez-vous afficher le copyright dans le bas de page ?');
  define('MODULES_FOOTER_SUFFIX_COPYRIGHT_DESCRIPTION', 'Ajouter le copyright dans le bas de page de la boutique ');
  define('MODULES_FOOTER_SUFFIX_COPYRIGHT_TEXT', '<br />Copyright© Juin 2010 - '. date('Y') .' '. STORE_NAME.'  All right reserved');
  define('MODULES_FOOTER_SUFFIX_TRADEMARK_TEXT', PROJECT_VERSION .' Registered® et Trademark™ <a href="http://www.e-imaginis.com" target="_blank">Imaginis</a> &amp; <a href="http://www.clicshopping.com" target="_blank">ClicShopping®</a>');
?>
