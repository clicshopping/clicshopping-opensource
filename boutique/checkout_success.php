<?php
/*
 * checkout_success.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
  if (!$OSCOM_Customer->isLoggedOn()) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }

  $Qorders = $OSCOM_PDO->prepare('select orders_id
                                 from orders
                                 where customers_id = :customers_id
                                 order by date_purchased desc
                                 limit 1
                                ');
  $Qorders->bindInt(':customers_id',  (int)$OSCOM_Customer->getID());
  $Qorders->execute();

// redirect to shopping cart page if no orders exist
  if ($Qorders->fetch() === false) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }

  $orders = $Qorders->toArray;

  $order_id = $orders['orders_id'];

  if ( isset($_GET['action']) && ($_GET['action'] == 'update') ) {
    osc_redirect(osc_href_link('index.php'));
  }


  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_success'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1);
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2);

  require($OSCOM_Template->getTemplateFiles('checkout_success'));
