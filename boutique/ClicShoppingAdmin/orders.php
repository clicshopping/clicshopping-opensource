<?php
/**
 * orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();

// orders_status
  $orders_statuses = array();

  $orders_status_array = array();

  $QordersStatus = $OSCOM_PDO->prepare('select orders_status_id,
                                              orders_status_name
                                      from :table_orders_status
                                      where language_id = :language_id
                                      ');
  $QordersStatus->bindInt(':language_id',  (int)$_SESSION['languages_id']);
  $QordersStatus->execute();

  while ($orders_status = $QordersStatus->fetch() ) {
    $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                               'text' => $orders_status['orders_status_name']);
    $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
  }

// orders_invoice status Dropdown
  $orders_invoice_statuses = array();
  $orders_status_invoice_array = array();

  $QordersStatusInvoice = $OSCOM_PDO->prepare('select orders_status_invoice_id,
                                                      orders_status_invoice_name
                                               from :table_orders_status_invoice
                                               where language_id = :language_id
                                              ');
  $QordersStatusInvoice->bindInt(':language_id',  (int)$_SESSION['languages_id']);
  $QordersStatusInvoice->execute();

  while ($orders_invoice_status = $QordersStatusInvoice->fetch() ) {
    $orders_invoice_statuses[] = array('id'   => $orders_invoice_status['orders_status_invoice_id'],
                                       'text' => $orders_invoice_status['orders_status_invoice_name']);
    $orders_status_invoice_array[$orders_invoice_status['orders_status_invoice_id']] = $orders_invoice_status['orders_status_invoice_name'];
  }


// orders_invoice status Dropdown
  $orders_tracking_statuses = array();
  $orders_status_tracking_array = array();

  $QordersStatusTracking = $OSCOM_PDO->prepare('select orders_status_tracking_id,
                                                       orders_status_tracking_name
                                               from :table_orders_status_tracking
                                               where language_id = :language_id
                                              ');
  $QordersStatusTracking->bindInt(':language_id',  (int)$_SESSION['languages_id']);
  $QordersStatusTracking->execute();

  while ($orders_tracking_status = $QordersStatusTracking->fetch() ) {
    $orders_tracking_statuses[] = array('id'   => $orders_tracking_status['orders_status_tracking_id'],
                                       'text' => $orders_tracking_status['orders_status_tracking_name']);
    $orders_status_tracking_array[$orders_tracking_status['orders_status_tracking_id']] = $orders_tracking_status['orders_status_tracking_name'];
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'update_order':

        $oID = osc_db_prepare_input($_GET['oID']);
        $status = osc_db_prepare_input($_POST['status']);
        $status_invoice = osc_db_prepare_input($_POST['status_invoice']);
        $comments = osc_db_prepare_input($_POST['comments']);
        $orders_tracking_id = osc_db_prepare_input($_POST['orders_tracking_id']);
        $orders_tracking_number = osc_db_prepare_input($_POST['orders_tracking_number']);

        $order_updated = false;

        $QcheckStatus = $OSCOM_PDO->prepare('select customers_name,
                                                   customers_email_address,
                                                   orders_status,
                                                   date_purchased,
                                                   orders_status_invoice,
                                                   odoo_invoice
                                             from :table_orders
                                             where orders_id = :orders_id
                                              ');
        $QcheckStatus->bindValue(':orders_id', (int)$oID);
        $QcheckStatus->execute();

        $check_status = $QcheckStatus->fetch();

// verify and update the status if changed
        if ( ($check_status['orders_status'] != $status) || ($check_status['orders_status_invoice'] != $status_invoice) || osc_not_null($comments) || osc_not_null($orders_tracking_id) || osc_not_null($orders_tracking_number)) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_orders 
                                          set orders_status = :orders_status,
                                          orders_status_invoice = :orders_status_invoice,
                                          last_modified = now()
                                          where orders_id = :orders_id  
                                        ');
          $Qupdate->bindInt(':orders_status', $status);
          $Qupdate->bindInt(':orders_status_invoice',  $status_invoice);
          $Qupdate->bindInt(':orders_id', (int)$oID );
          $Qupdate->execute();

          $customer_notified = '0';

          if (isset($_POST['notify']) && ($_POST['notify'] == 'on')) {
            $notify_comments = '';
            if (isset($_POST['notify_comments']) && ($_POST['notify_comments'] == 'on')) {
              $notify_comments = sprintf(EMAIL_TEXT_COMMENTS_UPDATE, nl2br($comments)) . "\n\n";
              $notify_comments = html_entity_decode($notify_comments);
            }

            if ($orders_history['orders_tracking_name'] !='' && $orders_history['orders_status_tracking_id'] > 1) {
              $tracking = $orders_history['orders_tracking_name'] . ' : ' . $orders_history['orders_tracking_number'] .'<br />';
            }

// email template
            require ('includes/functions/template_email.php');

            $template_email_intro_command = osc_get_template_email_intro_command($template_email_intro_command);
            $template_email_signature =osc_get_template_email_signature($template_email_signature);
            $template_email_footer = osc_get_template_email_text_footer($template_email_footer);

            $email_subject = EMAIL_TEXT_SUBJECT;
            $email_text = $template_email_intro_command . '<br />'. sprintf(EMAIL_TEXT_NEW_ORDER_STATUS, utf8_decode($orders_status_array[$status])) . '<br />'.  EMAIL_SEPARATOR . '<br /><br />'. EMAIL_TEXT_ORDER_NUMBER . ' '. $oID . '<br /><br />'. EMAIL_TEXT_INVOICE_URL . '<br />'. osc_catalog_href_link('account_history_info.php', 'order_id=' . $oID, 'SSL') . '<br /><br />' . EMAIL_TEXT_DATE_ORDERED . ' ' . $OSCOM_Date->getShort($check_status['date_purchased']) . '<br />' . $tracking . '<br />' . $notify_comments .'<br /><br />' .  $template_email_signature . '<br /><br />' . $template_email_footer;

// Envoie du mail avec gestion des images pour Fckeditor et Imanager.
            $mimemessage = new email(array('X-Mailer: ClicShopping'));
            $message = html_entity_decode($email_text);
            $message = str_replace('src="/', 'src="' . HTTP_CATALOG_SERVER . '/', $message);
            $mimemessage->add_html_fckeditor($message);
            $mimemessage->build_message();
            $from = STORE_OWNER_EMAIL_ADDRESS;
            $mimemessage->send($check_status['customers_name'], $check_status['customers_email_address'], '', $from, $email_subject);
// function mail
//            osc_mail($check_status['customers_name'], $check_status['customers_email_address'], $email_subject, $email_text, STORE_NAME, $from);

            $customer_notified = '1';
          }

// insert the modification in the databse
          osc_db_query("insert into orders_status_history (orders_id,
                                                           orders_status_id,
                                                           orders_status_invoice_id,
                                                           admin_user_name,
                                                           date_added,
                                                           customer_notified,
                                                           comments,
                                                           orders_status_tracking_id,
                                                           orders_tracking_number
                                                          )
                        values ('" . (int)$oID . "', 
                                '" . osc_db_input($status) . "', 
                                '" . osc_db_input($status_invoice) . "', 
                                '" . osc_user_admin($user_administrator) ."',
                                now(), 
                                '" . osc_db_input($customer_notified) . "', 
                                '" . osc_db_input($comments)  . "',
                                '" . osc_db_input($orders_tracking_id)  . "',
                                '" . osc_db_input($orders_tracking_number)  . "'
                               )
                       ");

          $order_updated = true;
        }

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_WEB_SERVICE_ORDERS_ADMIN =='true') {
           if ($check_status['odoo_invoice'] != 0) {
             require ('ext/odoo_xmlrpc/xml_rpc_admin_order_invoice.php');
           } else {
             require ('ext/odoo_xmlrpc/xml_rpc_admin_order.php');
           }
        }
//***************************************
// End odoo web service
//***************************************

        if ($order_updated == true) {
         $OSCOM_MessageStack->add_session(SUCCESS_ORDER_UPDATED, 'success');
        } else {
         $OSCOM_MessageStack->add_session(WARNING_ORDER_NOT_UPDATED, 'warning');
        }

        osc_redirect(osc_href_link('orders.php', osc_get_all_get_params(array('action')) . 'action=edit'));
      break;

      case 'deleteconfirm':
        $oID = osc_db_prepare_input($_GET['oID']);

        osc_remove_order($oID, $_POST['restock']);

        osc_redirect(osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action'))));
      break;

      case 'archive_to_confirm':
        if (isset($_GET['oID'])) $oID = osc_db_prepare_input($_GET['oID']);      

        $Qupdate = $OSCOM_PDO->prepare('update :table_orders 
                                        set orders_archive = :orders_archive 
                                        where orders_id = :orders_id  
                                      ');
        $Qupdate->bindInt(':orders_archive', '1' );
        $Qupdate->bindInt(':orders_id', (int)$oID );
        $Qupdate->execute();

      break;

// desarchivage
      case 'unpack':
        if (isset($_GET['oID'])) $oID = osc_db_prepare_input($_GET['oID']); 

        $Qupdate = $OSCOM_PDO->prepare('update :table_orders 
                                        set orders_archive = :orders_archive 
                                        where orders_id = :orders_id  
                                      ');
        $Qupdate->bindInt(':orders_archive', '0' );
        $Qupdate->bindInt(':orders_id', (int)$oID );
        $Qupdate->execute();

        osc_redirect(osc_href_link('orders.php'));
      break; 

    } // end switch
  } // end osc_not_null

  if (($action == 'edit') && isset($_GET['oID'])) {
    $oID = osc_db_prepare_input($_GET['oID']);

    $Qorders = $OSCOM_PDO->prepare('select orders_id
                                    from :table_orders
                                    where orders_id = :orders_id
                                    ');
    $Qorders->bindInt(':orders_id',  (int)$oID);
    $Qorders->execute();

    $order_exists = true;

    if ($Qorders->fetch() === false) {
      $order_exists = false;
      $OSCOM_MessageStack->add(sprintf(ERROR_ORDER_DOES_NOT_EXIST, $oID), 'error');
    }
  }

  include('includes/classes/order.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script type="text/javascript" src="ext/javascript/clicshopping/general.js"></script>
</head>
<body onload="SetFocus();">
<!-- header //-->
<?php
  require('includes/header.php');
?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
 <tr>
  <!-- body_text //-->
  <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
    <!-- //################################################################################################################ -->
    <!-- //                                              EDITION D'UNE COMMANDE                                             -->
    <!-- //################################################################################################################ -->
<?php
  if (($action == 'edit') && ($order_exists == true)) {
    $order = new order($oID);
?>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <tr>
     <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
       <tr>
        <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/orders.gif', HEADING_TITLE_EDIT, '40', '40'); ?></td>
        <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
        <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">
          <tr>
<?php
// recherche historique client
      $Qcustomers = $OSCOM_PDO->prepare('select c.customers_id,
                                                o.customers_id,
                                                o.orders_id
                                         from :table_customers c,
                                              :table_orders o
                                         where c.customers_id = o.customers_id
                                         and o.orders_id = :orders_id
                                       ');
      $Qcustomers->bindInt(':orders_id', (int)$_GET['oID']);
      $Qcustomers->execute();

      $customers = $Qcustomers->fetch();
?>
           
           <td><?php echo '<a href="' . osc_href_link('orders.php', 'cID=' . $customers[customers_id]) . '">' . osc_image_button('button_history.gif', IMAGE_ORDERS_HISTORY) . '</a>'; ?></td>
           <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?><?php echo '<a href="' . osc_href_link('invoice.php', 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . osc_image_button('button_print.gif', IMAGE_ORDERS_INVOICE) . '</a>'; ?></td>
           <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
           <td><?php echo '<a href="' . osc_href_link('packingslip.php', 'oID=' . $_GET['oID']) . '" target="_blank">' . osc_image_button('button_packingslip.gif', IMAGE_ORDERS_PACKINGSLIP) . '</a>'; ?></td>
           <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
           <td><?php echo '<a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('action'))) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
    <tr>
     <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
    </tr>
  </table>
<!-- -------------------------------------------------- //-->
<!--          ONGLET Information sur le client          //-->
<!-- -------------------------------------------------- //-->

  <ul class="nav nav-tabs" role="tablist"  id="myTab">
    <li class="active"><?php echo '<a href="' . substr(osc_href_link('orders.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' . TAB_GENERAL . '</a>'; ?></a></li>
    <li><?php echo '<a href="' . substr(osc_href_link('orders.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' . TAB_ORDERS_DETAILS; ?></a></li>
    <li><?php echo '<a href="' . substr(osc_href_link('orders.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' . TAB_STATUT; ?></a></li>
  </ul>

  <div class="tabsClicShopping">
    <div class="tab-content">
  <!-- ---------------------------------------------- //-->
  <!--          ONGLET info general                  //-->
  <!-- ---------------------------------------------- //-->
      <div class="tab-pane active" id="tab1">
        <table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
           <td class="mainTitle"><?php echo TITLE_ORDERS_ADRESSE; ?></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
         <tr>
          <td valign="top"><table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
             <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
               <tr>
                <td class="main" valign="top"><?php echo ENTRY_CUSTOMER; ?></td>
                <td class="main" valign="top"><strong><?php echo osc_address_format($order->customer['format_id'], $order->customer, 1, '', '<br />'); ?></strong></td>
               </tr>
              </table></td>
             <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
               <tr>
                <td class="main" valign="top"><?php echo ENTRY_SHIPPING_ADDRESS; ?></td>
                <td class="main" valign="top"><strong><?php echo osc_address_format($order->delivery['format_id'], $order->delivery, 1, '', '<br />'); ?></strong></td>
               </tr>
              </table></td>
             <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
               <tr>
                <td class="main" valign="top"><?php echo ENTRY_BILLING_ADDRESS; ?></td>
                <td class="main" valign="top"><strong><?php echo osc_address_format($order->billing['format_id'], $order->billing, 1, '', '<br />'); ?></strong></td>
               </tr>
              </table></td>
            </tr>
           </table></td>
         </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5">
         <tr>
          <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
         </tr>
         <tr>
          <td class="mainTitle"><?php echo TITLE_ORDERS_CUSTOMERS; ?></td>
         </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
         <tr>
          <td><table border="0" cellpadding="2" cellspacing="2">
            <tr>
<?php
  if (MODE_B2B_B2C == 'true') {
?>
             <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
               <tr>
                <td class="main"><?php echo ENTRY_ORDER_SIRET; ?></td>
                <td class="main"><strong><?php echo $order->customer['siret']; ?></strong></td>
               </tr>
               <tr>
                <td class="main"><?php echo ENTRY_ORDER_CODE_APE; ?></td>
                <td class="main"><strong><?php echo $order->customer['ape']; ?></strong></td>
               </tr>
               <tr>
                <td class="main"><?php echo ENTRY_TVA_INTRACOM; ?></td>
                <td class="main"><strong><?php echo $order->customer['tva_intracom']; ?></strong></td>
               </tr>
               <tr>
                <td class="main"></td>
                <td class="main"></td>
               </tr>
               <tr>
                <td class="main"></td>
                <td class="main"></td>
               </tr>
               <tr>
<script type="text/javascript"><!--
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=600,height=600,screenX=150,screenY=150,top=150,left=150')
}
//--></script>

                <td class="main" colspan="2"><?php echo '<a href="javascript:popupImageWindow(\'' . 'popup_page_manager_order_history.php' .'?order_id='. (int)$_GET['oID'].'&customer_id='.$customers[customers_id].'\')"><strong>' . TEXT_CONDITION_GENERAL_OF_SALES . '</strong></a>'; ?></td>
               </tr>
              </table></td>
<?php
  }
?>
             <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
             <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
               <tr>
                <td class="main"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
                <td class="main"><strong><?php echo $order->customer['telephone']; ?></strong></td>
                <td class="main"><?php echo ENTRY_CUSTOMER_LOCATION; ?></td>
                <td  class="main"><a target="_blank" href="http://maps.google.com/maps?q=<?php echo $order->delivery['street_address'], ',', $order->delivery['postcode'], ',', $order->delivery['state'], ',', $order->delivery['country']; ?>&hl=fr&um=1&ie=UTF-8&sa=N&tab=wl"><img src="images/google_map.gif" border="0" alt="<?php echo ENTRY_CUSTOMER_LOCATION; ?>" title="<?php echo ENTRY_CUSTOMER_LOCATION; ?>"></a>
               </tr>
               <tr>
                <td class="main"><?php echo ENTRY_CELLULAR_PHONE_NUMBER; ?></td>
                <td class="main"><strong><?php echo $order->customer['cellular_phone']; ?></strong></td>
               </tr>
               <tr>
                <td class="main"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                <td class="main"><strong><?php echo '<a href="mailto:' . $order->customer['email_address'] . '"><u>' . $order->customer['email_address'] . '</u></a>'; ?></strong></td>
               </tr>
               <tr>
                <td class="main"><?php echo ENTRY_CLIENT_COMPUTER_IP; ?></td>
                <td class="main"><strong><?php echo $order->customer['client_computer_ip']; ?></strong></td>
               </tr>
               <tr>
                <td class="main"><?php echo ENTRY_PROVIDER_NAME_CLIENT; ?></td>
                <td class="main"><strong><?php echo $order->customer['provider_name_client']; ?></strong></td>
               </tr>
              </table></td>
            </tr>
           </table></td>
         </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5">
         <tr>
          <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
         </tr>
         <tr>
          <td class="mainTitle"><?php echo TITLE_ORDERS_PAIEMENT; ?></td>
         </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
         <tr>
          <td><table border="0" cellpadding="2" cellspacing="2">
            <tr>
             <td class="main"><?php echo ENTRY_PAYMENT_METHOD; ?></td>
             <td class="main"><strong><?php echo $order->info['payment_method']; ?></strong></td>
            </tr>
<?php
  if (osc_not_null($order->info['cc_type']) || osc_not_null($order->info['cc_owner']) || osc_not_null($order->info['cc_number'])) {
?>
            <tr>
             <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
             <td class="main"><?php echo ENTRY_CREDIT_CARD_TYPE; ?></td>
             <td class="main"><?php echo $order->info['cc_type']; ?></td>
            </tr>
            <tr>
             <td class="main"><?php echo ENTRY_CREDIT_CARD_OWNER; ?></td>
             <td class="main"><?php echo $order->info['cc_owner']; ?></td>
            </tr>
            <tr>
             <td class="main"><?php echo ENTRY_CREDIT_CARD_NUMBER; ?></td>
             <td class="main"><?php echo $order->info['cc_number']; ?></td>
            </tr>
            <tr>
             <td class="main"><?php echo ENTRY_CREDIT_CARD_EXPIRES; ?></td>
             <td class="main"><?php echo $order->info['cc_expires']; ?></td>
            </tr>
<?php
  }
?>
           </table></td>
         </tr>
        </table>
      </div>

<!-- ---------------------------------------------- //-->
<!--          ONGLET informations commande          //-->
<!-- ---------------------------------------------- //-->

      <div class="tab-pane" id="tab2">


        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
         <tr class="dataTableHeadingRow">
         <td></td>
         <td></td>
        <td class="mainTitle" colspan="3" align="center"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
          <td class="mainTitle"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></td>
          <td class="mainTitle" align="right"><?php echo TABLE_HEADING_TAX; ?></td>
          <td class="mainTitle" align="right"><?php echo TABLE_HEADING_PRICE_EXCLUDING_TAX; ?></td>
          <td class="mainTitle" align="right"><?php echo TABLE_HEADING_PRICE_INCLUDING_TAX; ?></td>
          <td class="mainTitle" align="right"><?php echo TABLE_HEADING_TOTAL_EXCLUDING_TAX; ?></td>
          <td class="mainTitle" align="right"><?php echo TABLE_HEADING_TOTAL_INCLUDING_TAX; ?></td>
         </tr>
<?php
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {

      $QproductsImage = $OSCOM_PDO->prepare('select products_image,
                                                    products_id
                                            from :table_products
                                            where products_id = :products_id
                                           ');
      $QproductsImage->bindInt(':products_id', (int)$order->products[$i]['products_id']);
      $QproductsImage->execute();

      $products_image = $QproductsImage->fetch();

      echo '    <tr class="dataTableRow">' . "\n" .
           '      <td class="dataTableContent" valign="top"><a href="' . osc_href_link('products_preview.php', '&pID=' . $products_image['products_id']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) .'</a></td>' . "\n" .
           '      <td class="dataTableContent" valign="top"><a href="' . osc_href_link('categories.php', '&pID=' . $products_image['products_id'] .'&action=new_product') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) .'</a></td>' . "\n" .
           '      <td class="dataTableContent" valign="top"><img src=" ' . DIR_WS_CATALOG_IMAGES . $products_image['products_image'] . '" width=" '. SMALL_IMAGE_WIDTH_ADMIN .'" height="'. SMALL_IMAGE_HEIGHT_ADMIN .'"></td>' . "\n" .	
           '      <td class="dataTableContent" valign="top" align="left">' . $order->products[$i]['qty'] . '&nbsp;x</td>' . "\n" .
           '      <td class="dataTableContent" valign="top" align="left">' . $order->products[$i]['name'];

      if (isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
        for ($j = 0, $k = sizeof($order->products[$i]['attributes']); $j < $k; $j++) {

// attributes reference
        if ($order->products[$i]['attributes'][$j]['reference'] != '' or $order->products[$i]['attributes'][$j]['reference'] !='null') {
           $attributes_reference = '<strong> ' .$order->products[$i]['attributes'][$j]['reference'] . '</strong> - '; 
        }

          echo '<br /><nobr><small>&nbsp;<i> - ' . $attributes_reference  . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];

          if ($order->products[$i]['attributes'][$j]['price'] != '0') echo ' (' . $order->products[$i]['attributes'][$j]['prefix'] . $currencies->format($order->products[$i]['attributes'][$j]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . ')';
          echo '</i></small></nobr>';
        }
      }

      echo '      </td>' . "\n" .
           '      <td class="dataTableContent" valign="top">' . $order->products[$i]['model'] . '</td>' . "\n" .
           '      <td class="dataTableContent" align="right" valign="top">' . osc_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n" .
           '      <td class="dataTableContent" align="right" valign="top"><strong>' . $currencies->format($order->products[$i]['final_price'], true, $order->info['currency'], $order->info['currency_value']) . '</strong></td>' . "\n" .
           '      <td class="dataTableContent" align="right" valign="top"><strong>' . $currencies->format(osc_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax'], true), true, $order->info['currency'], $order->info['currency_value']) . '</strong></td>' . "\n" .
           '      <td class="dataTableContent" align="right" valign="top"><strong>' . $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</strong></td>' . "\n" .
           '      <td class="dataTableContent" align="right" valign="top"><strong>' . $currencies->format(osc_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax'], true) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</strong></td>' . "\n";      echo '    </tr>' . "\n";
      echo '    </tr>' . "\n";
    }
?>
         <tr>
          <td align="right" colspan="11"><table border="0" cellspacing="0" cellpadding="2">
<?php
    for ($i = 0, $n = sizeof($order->totals); $i < $n; $i++) {
      echo '        <tr>' . "\n" .
           '          <td align="right" class="smallText">' . $order->totals[$i]['title'] . '</td>' . "\n" .
           '          <td align="right" class="smallText">' . $order->totals[$i]['text'] . '</td>' . "\n" .
           '        </tr>' . "\n";
    }
?>
           </table></td>
         </tr>
        </table>
      </div>

<!-- ---------------------------------------- //-->
<!--          ONGLET statut commande          //-->
<!-- ---------------------------------------- //-->
      <div class="tab-pane"  id="tab3">

         <table width="100%" border="0" cellspacing="0" cellpadding="5">
           <tr>
             <td class="mainTitle"><?php echo TABLE_HEADING_COMMENTS; ?></td>
           </tr>
         </table>
         <?php echo osc_draw_form('status', 'orders.php', osc_get_all_get_params(array('action')) . 'action=update_order#tab3'); ?>
           <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
             <tr>
               <td><table border="0" cellpadding="0" cellspacing="0">
                   <tr>
                     <td><table border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <td class="main"><?php echo osc_draw_textarea_field('comments', 'soft', '60', '5'); ?></td>
                       </tr>
                     </table></td>
                     <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
                       <tr>
                         <td class="main" colspan="2"><strong><?php echo ENTRY_STATUS; ?></strong> <?php echo osc_draw_pull_down_menu('status', $orders_statuses, $order->info['orders_status']); ?></td>
                         <td class="main" colspan="2"><strong><?php echo ENTRY_STATUS_INVOICE; ?></strong> <?php echo osc_draw_pull_down_menu('status_invoice', $orders_invoice_statuses, $order->info['orders_status_invoice']); ?></td>
                       </tr>
                       <tr>
<?php
   if ($order->info['odoo_invoice'] == 1) {
?>
                         <td class="main"><strong><?php echo TEXT_ODOO_INVOICE; ?></strong>
                         <td class="main"></td>

<?php
  } elseif ($order->info['odoo_invoice'] == 2) {
?>
                             <td class="main"><strong><?php echo TEXT_ODOO_INVOICE_REGISTRED; ?></strong>
                             <td class="main"></td>
<?php
  } elseif ($order->info['odoo_invoice'] == 3) {
?>
                         <td class="main"><strong><?php echo TEXT_ODOO_INVOICE_CONFIRM; ?></strong
                         <td class="main"></td>
<?php
  } elseif ($order->info['odoo_invoice'] == 3) {
?>
                         <td class="main"><strong><?php echo TEXT_ODOO_INVOICE_CANCELLED; ?></strong>
                         <td class="main"></td>
<?php
  }
?>
                       </tr>
                       <tr>
                         <td class="main" colspan="2"><strong><?php echo ENTRY_STATUS_ORDERS_TRACKING_NAME; ?></strong> <?php echo osc_draw_pull_down_menu('orders_tracking_id', $orders_tracking_statuses, $order->info['orders_status_tracking']); ?></td>
                         <td class="main" colspan="2"><strong><?php echo ENTRY_STATUS_ORDERS_TRACKING_NUMBER; ?></strong> <?php echo osc_draw_input_field('orders_tracking_number', '', $order->info['orders_tracking_number']); ?></td>
                       </tr>
                       <tr>
                         <td class="main"><strong><?php echo ENTRY_NOTIFY_CUSTOMER; ?></strong> <?php echo osc_draw_checkbox_field('notify', '', true); ?></td>
                         <td class="main"><strong><?php echo ENTRY_NOTIFY_COMMENTS; ?></strong> <?php echo osc_draw_checkbox_field('notify_comments', '', true); ?></td>
                       </tr>
                       <tr>
                         <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
                       </tr>
                       <tr>
                         <td colspan="2"><?php echo osc_image_submit('button_mini_update.gif', IMAGE_UPDATE); ?></td>
                       </tr>
                   </table></td>
                 </tr>
               </table></td>
             </tr>
           </table>
         </form>
           <table width="100%" border="0" cellspacing="0" cellpadding="5">
             <tr>
               <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
             </tr>
             <tr>
               <td class="mainTitle"><?php echo TITLE_ORDERS_HISTORY; ?></td>
             </tr>
           </table>
           <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
           <tr>
            <td class="main"><table width="100%" border="1" cellspacing="0" cellpadding="5">
              <tr>
               <td class="smallText" align="center"><strong><?php echo TABLE_HEADING_DATE_ADDED; ?></strong></td>
               <td class="smallText" align="center"><strong><?php echo TABLE_HEADING_CUSTOMER_NOTIFIED; ?></strong></td>
               <td class="smallText" align="center"><strong><?php echo TABLE_HEADING_STATUS; ?></strong></td>
               <td class="smallText" align="center"><strong><?php echo TABLE_HEADING_COMMENTS; ?></strong></td>
              </tr>
<?php
  $QordersHistory = $OSCOM_PDO->prepare('select orders_status_id,
                                                 orders_status_invoice_id,
                                                 admin_user_name,
                                                 date_added,
                                                 customer_notified,
                                                 comments,
                                                 orders_status_tracking_id,
                                                 orders_tracking_number
                                          from :table_orders_status_history
                                          where orders_id = :orders_id
                                          order by date_added
                                         ');
  $QordersHistory->bindInt(':orders_id', osc_db_input($oID));
  $QordersHistory->execute();

    if ($QordersHistory->rowCount() > 0) {
      while ($orders_history = $QordersHistory->fetch() ) {
        echo '      <tr>' . "\n" .
             '        <td class="smallText" align="center">' . $OSCOM_Date->getShortTime($orders_history['date_added']) . '</td>' . "\n" .
             '        <td class="smallText" align="center">';
        if ($orders_history['customer_notified'] == '1') {
          echo osc_image(DIR_WS_ICONS . 'tick.gif', ICON_TICK) . "</td>\n";
        } else {
          echo osc_image(DIR_WS_ICONS . 'cross.gif', ICON_CROSS) . "</td>\n";
        }


        $QordersTracking = $OSCOM_PDO->prepare('select orders_status_tracking_id,
                                                       orders_status_tracking_link
                                               from :table_orders_status_tracking
                                               where orders_status_tracking_id = :orders_status_tracking_id
                                               and language_id = :language_id
                                         ');
        $QordersTracking->bindInt(':orders_status_tracking_id', $orders_history['orders_status_tracking_id']);
        $QordersTracking->bindInt(':language_id',(int)$_SESSION['languages_id']);
        $QordersTracking->execute();

        $orders_tracking = $QordersTracking->fetch();

        if (empty($orders_tracking['orders_status_tracking_link'])) {
          $tracking_url = ' (<a href="http://www.track-trace.com/" target="_blank">http://www.track-trace.com/</a>)';
        } else {
          $tracking_url = ' (<a href="'.$orders_tracking['orders_status_tracking_link'] .  $orders_history['orders_tracking_number'] .'" target="_blank">'.$orders_tracking['orders_status_tracking_link'] .  $orders_history['orders_tracking_number'].'</a>)';;
        }

        echo '        <td class="smallText" align="center">' . $orders_status_array[$orders_history['orders_status_id']] . '</td>' . "\n" .
             '        <td class="smallText">' . ENTRY_STATUS_COMMENT_INVOICE . $orders_status_invoice_array[$orders_history['orders_status_invoice_id']] . '<br />
' . ENTRY_STATUS_INVOICE_REALISED . $orders_history['admin_user_name'] .'<br />
' . ENTRY_STATUS_ORDERS_TRACKING_NAME . $orders_status_tracking_array[$orders_history['orders_status_tracking_id']] . '<br />' . ENTRY_STATUS_ORDERS_TRACKING_NUMBER .  $orders_history['orders_tracking_number'] .  $tracking_url .'<hr>' . ENTRY_STATUS_INVOICE_NOTE . '<br />' . nl2br(osc_db_output($orders_history['comments'])) . '</td>' . "\n" .
             '      </tr>' . "\n";
      }
    } else {
        echo '      <tr>' . "\n" .
             '        <td class="smallText" colspan="5">' . TEXT_NO_ORDER_HISTORY . '</td>' . "\n" .
             '      </tr>' . "\n";
    }
?>
            </table></td>
          </tr>
        </table>
      </div>
    </div>
  </div>

    
<?php
  } else {
?>
    <!-- //################################################################################################################ -->
    <!-- //                                              LISTE DES COMMANDES                                                -->
    <!-- //################################################################################################################ -->
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <tr>
     <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
       <tr>
        <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/orders.gif', HEADING_TITLE, '40', '40'); ?></td>
        <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
        <td class="pageHeading" align="right"><?php echo osc_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
        <td align="right"><table border="0" cellspacing="0" cellpadding="0">
          <tr>
           <td><table border="0" cellspacing="0" cellpadding="0">
             <tr><?php echo osc_draw_form('orders', 'orders.php', '', 'get'); ?>
              <td align="right" colspan="4"><table border="0" cellspacing="0" cellpadding="0">
                <tr>
                 <td class="smallText">
                   <div class="form-group">
                     <div class="controls">
                       <?php echo osc_draw_input_field('oID', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');  ?>
                     </div>
                   </div>
                  </td>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
                 <td class="smallText"><?php echo osc_image_submit('button_ok.gif', IMAGE_EDIT)  .  osc_draw_hidden_field('action', 'edit'); ?></td>
                </tr>
               </table></td>
              <?php echo osc_hide_session_id(); ?>
              </form>
             </tr>
             <tr>
<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
    if (MODE_B2B_B2C == 'true') {
      echo osc_draw_form('grouped', 'orders.php', '', 'get');
?>
              <td class="smallText" align="right"><?php echo ENTRY_CUSTOMERS_GROUP_NAME . ' ' . osc_draw_pull_down_menu('customers_group_id', array_merge(osc_get_customers_group(''. VISITOR_NAME .''), (array)$oInfo->customers_group_id), '', 'onchange="this.form.submit();"'); ?></td>
              <?php echo osc_hide_session_id(); ?>
              </form>
<?php
  }
?>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
              <?php echo osc_draw_form('status', 'orders.php', '', 'get'); ?>
              <td class="smallText" align="right"><?php echo HEADING_TITLE_STATUS . ' ' . osc_draw_pull_down_menu('status', array_merge(array(array('id' => '0', 'text' => TEXT_ALL_ORDERS)), $orders_statuses), '', 'onchange="this.form.submit();"'); ?></td>
              <?php echo osc_hide_session_id(); ?>
              </form>
             </tr>
            </table></td>
           <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
           <td><table border="0" cellspacing="0" cellpadding="0">
             <tr>
              <td class="smallText" align="right"><?php echo '<a href="' . osc_href_link('orders.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>';?></td>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
              <td class="smallText" align="right"><?php echo '<a href="' . osc_href_link('batch_print_order.php') . '">' . osc_image_button('button_print.gif', IMAGE_BATCH_PRINT_ORDER) . '</a>';?></td>
              <td class="smallText" align="right">
<?php
  $archive_id = $_GET['aID'];
  echo osc_draw_form('archive', 'orders.php', '', 'get');
  if ($archive_id !='1') {
    echo osc_draw_separator('pixel_trans.gif', '6', '1');
    echo osc_image_submit('button_see_archive.png', IMAGE_ARCHIVE) . osc_draw_hidden_field('aID','1'); 
  }
?>

                </form>
              </td>
             </tr>
            </table></td>
          </tr>
         </table></td>
       </tr>
      </table></td>
    </tr>
    <tr>
     <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
    </tr>
    <tr>
     <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
       <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
          <tr class="dataTableHeadingRow">
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDERS; ?>&nbsp</td>
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS; ?>&nbsp</td>
<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
  if (MODE_B2B_B2C == 'true') {
?>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_COLOR_GROUP; ?></td>
<?php
  } else {
?>
           <td></td>
<?php
  }
?>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ORDER_TOTAL; ?></td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?>&nbsp;</td>
           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ODOO; ?>&nbsp;</td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_REALISED_BY; ?>&nbsp;</td>
           <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
          </tr>
<?php
    if (isset($_GET['cID'])) {
      $cID = osc_db_prepare_input($_GET['cID']);

      $orders_query_raw = "select o.orders_id, 
                                  o.customers_id, 
                                  o.customers_name, 
                                  o.customers_company, 
                                  o.customers_id, 
                                  o.customers_group_id, 
                                  o.payment_method, 
                                  o.date_purchased, 
                                  o.last_modified, 
                                  o.currency, 
                                  o.currency_value, 
                                  s.orders_status_name,
                                  ot.text as order_total,
                                  o.odoo_invoice
                           from orders o left join orders_total ot on (o.orders_id = ot.orders_id),
                                orders_status s
                           where o.customers_id = '" . (int)$cID . "' 
                           and o.orders_status = s.orders_status_id 
                           and s.language_id = '" . (int)$_SESSION['languages_id'] . "'
                           and o.orders_archive = '" . (int)$archive_id . "'
                           and ot.class = 'ot_total' $sort_by
                          ";
    } elseif (isset($_GET['customers_group_id'])) {

      $customers_group_id = $_GET['customers_group_id'];

      $orders_query_raw = "select o.orders_id, 
                                  o.customers_id, 
                                  o.customers_name, 
                                  o.customers_group_id, 
                                  o.customers_company, 
                                  o.payment_method, 
                                  o.date_purchased, 
                                  o.last_modified, 
                                  o.currency, 
                                  o.currency_value, 
                                  s.orders_status_name, 
                                  ot.text as order_total,
                                  o.odoo_invoice
                           from orders o left join orders_total ot on (o.orders_id = ot.orders_id),
                                orders_status s
                           where o.orders_status = s.orders_status_id 
                           and s.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                           and o.customers_group_id = '" . (int)$customers_group_id . "'
                           and o.orders_archive = '" . (int)$archive_id . "'
                           and ot.class = 'ot_total' order by o.orders_id DESC
                          ";

  } elseif (isset($_GET['status'])) {
    $status = osc_db_prepare_input($_GET['status']);
    if ($status == "0"){
    $orders_query_raw = "select o.orders_id, 
                                o.customers_id,
                                o.customers_name,
                                o.customers_group_id,
                                o.customers_company,
                                o.payment_method,
                                o.date_purchased,
                                o.last_modified,
                                o.currency,
                                o.currency_value,
                                s.orders_status_name,
                                ot.text as order_total,
                                o.odoo_invoice
                         from orders o left join orders_total ot on (o.orders_id = ot.orders_id),
                              orders_status s
                         where o.orders_status = s.orders_status_id
                         and s.language_id = '" . (int)$_SESSION['languages_id'] . "'
                         and o.orders_archive = '" . (int)$archive_id . "'
                         and ot.class = 'ot_total'
                         order by o.orders_id DESC
                       ";
    } else {
        $orders_query_raw = "select o.orders_id, 
                                    o.customers_id, 
                                    o.customers_name, 
                                    o.customers_group_id, 
                                    o.customers_company, 
                                    o.payment_method, 
                                    o.date_purchased, 
                                    o.last_modified, 
                                    o.currency, 
                                    o.currency_value, 
                                    s.orders_status_name, 
                                    ot.text as order_total,
                                    o.odoo_invoice
                             from orders o left join orders_total ot on (o.orders_id = ot.orders_id),
                                  orders_status s
                             where o.orders_status = s.orders_status_id 
                             and s.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and s.orders_status_id = '" . (int)$status . "' 
                             and o.orders_archive = '" . (int)$archive_id . "'
                             and ot.class = 'ot_total' 
                             order by o.orders_id DESC
                            ";
      }
  } else {

      $orders_query_raw = "select o.orders_id, 
                                  o.customers_id, 
                                  o.customers_name, 
                                  o.customers_group_id, 
                                  o.customers_company, 
                                  o.payment_method, 
                                  o.date_purchased, 
                                  o.last_modified,  
                                  o.currency, 
                                  o.currency_value, 
                                  s.orders_status_name, 
                                  ot.text as order_total,
                                  o.odoo_invoice
                           from orders o left join orders_total ot on (o.orders_id = ot.orders_id),
                                orders_status s
                           where o.orders_status = s.orders_status_id 
                           and s.language_id = '" . (int)$_SESSION['languages_id'] . "'
                           and o.orders_archive = '" . (int)$archive_id . "'
                           and ot.class = 'ot_total' 
                           order by o.orders_id DESC
                          ";
    }

    $orders_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $orders_query_raw, $orders_query_numrows);
    $orders_query = osc_db_query($orders_query_raw);
    while ($orders = osc_db_fetch_array($orders_query)) {

      if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders['orders_id']))) && !isset($oInfo)) {
        $oInfo = new objectInfo($orders);
      }

      $Qcustomers = $OSCOM_PDO->prepare('select customers_id,
                                                customers_group_id
                                         from :table_customers
                                         where customers_id = :customers_id
                                       ');
      $Qcustomers->bindInt(':customers_id', (int)$orders['customers_id']);
      $Qcustomers->execute();

      $customers = $Qcustomers->fetch();

// select the last update by the admin name
      $Qhistory= $OSCOM_PDO->prepare('select osh.admin_user_name,
                                              osh.orders_id,
                                              o.orders_id
                                       from :table_orders_status_history osh,
                                            :table_orders o
                                       where osh.orders_id = o.orders_id
                                       and o.orders_id = :customers_id
                                       order by osh.date_added desc
                                       limit 1
                                      ');
      $Qhistory->bindInt(':customers_id', (int)$orders['orders_id']);
      $Qhistory->execute();

      $history = $Qhistory->fetch();

// Selectionne la couleur selon le groupe client au moment de la commande
      if ($orders['customers_group_id'] != 0){
        $Qcolor= $OSCOM_PDO->prepare('select color_bar
                                       from :table_customers_groups
                                       where customers_group_id = :customers_group_id
                                    ');
        $Qcolor->bindInt(':customers_group_id', (int)$orders['customers_group_id']);
        $Qcolor->execute();

        $color = $Qcolor->fetch();
      }

      if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) {
        echo '<tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('orders.php', osc_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id']) . '\'">' . "\n";
      } else {
        echo '<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('orders.php', osc_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id']) . '\'">' . "\n";
      }
?>
          <td class="dataTableContent"><?php echo $orders['orders_id']; ?></td>
          <td class="dataTableContent"><?php echo '' . $orders['customers_name'] .'&nbsp;(' . $orders['customers_company'] .')'; ?></td>
<?php
// Permettre l'affichage couleurs du groupe B2B auquel le client ce trouvais au moment de la commande
      if (MODE_B2B_B2C == 'true') {
        if ($orders['customers_group_id'] != '0') {
?>
           <td class="dataTableContent" align="center"><table width="15" cellspacing="0" cellpadding="0" border="0">
             <tr>
              <td bgcolor="<?php echo $color['color_bar']; ?>">&nbsp;</td>
             </tr>
            </table></td>
<?php
        } else {
?>
           <td class="dataTableContent"></td>
<?php
        }
      } else {
?>
           <td class="dataTableContent"></td>
<?php
      }
 ?>
           <td class="dataTableContent" align="right"><?php echo strip_tags($orders['order_total']); ?></td>
           <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShortTime($orders['date_purchased'], true); ?></td>
           <td class="dataTableContent" align="right"><?php echo $orders['orders_status_name']; ?></td>
<?php
      if ($orders['odoo_invoice'] == 1 ) {
?>
        <td class="dataTableContent"
            align="center"><?php echo osc_image (DIR_WS_ICONS . 'odoo_order.png', IMAGE_ORDERS_ODOO); ?></td>
<?php
      } elseif ($orders['odoo_invoice'] == 2) {
?>
        <td class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_ICONS . 'odoo_invoice.png', IMAGE_ORDERS_INVOICE_MANUAL_ODOO); ?></td>
<?php
      } elseif ($orders['odoo_invoice'] == 3) {
?>
        <td class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_ICONS . 'odoo.png', IMAGE_ORDERS_INVOICE_ODOO); ?></td>
<?php
      } elseif ($orders['odoo_invoice'] == 4)  {
?>
        <td class="dataTableContent" align="center"><?php echo osc_image(DIR_WS_ICONS . 'odoo_invoice_cancelled.png', IMAGE_ORDERS_INVOICE_CANCEL_ODOO); ?></td>
<?php
      } else {
?>
        <td class="dataTableContent" align="center"></td>
<?php
      }
?>
           <td class="dataTableContent" align="right"><?php echo $history['admin_user_name']; ?></td>
           <td class="dataTableContent" align="right">
<?php

    if ($customers['customers_group_id'] > '0' || (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id))) {
       echo '<a href="' . osc_href_link(('customers.php') . '?cID=' . $orders['customers_id']) . '&action=edit">' . osc_image(DIR_WS_ICONS . 'client_b2b.gif', ICON_EDIT_CUSTOMER) . '</a>';
    } else {
       echo '<a href="' . osc_href_link(('customers.php') . '?cID=' . $orders['customers_id']) . '&action=edit">' . osc_image(DIR_WS_ICONS . 'client_b2c.gif', ICON_EDIT_CUSTOMER) . '</a>';
    }

    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_ORDER) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('invoice.php', 'oID=' . $orders['orders_id']) . '" TARGET="_blank">' . osc_image(DIR_WS_ICONS . 'invoice.gif', IMAGE_ORDERS_INVOICE) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('packingslip.php', 'oID=' . $orders['orders_id']) . '" TARGET="_blank">' . osc_image(DIR_WS_ICONS . 'packingslip.gif', IMAGE_ORDERS_PACKINGSLIP) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');

    if ($archive_id !='1') {
        echo '<a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=archive') . '">' . osc_image(DIR_WS_ICONS . 'archive.gif', IMAGE_ARCHIVE_TO) . '</a>' ;
    } else {
        echo '<a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=unpack') . '">' . osc_image(DIR_WS_ICONS . 'unpack.gif', IMAGE_ARCHIVE_TO) . '</a>' ;
    }

    echo osc_draw_separator('pixel_trans.gif', '6', '16');    
    if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
           </td>
          </tr>
<?php
    }
?>
          <tr>
           <td colspan="9"><table border="0" width="100%" cellspacing="0" cellpadding="2">
             <tr>
              <td class="smallText" valign="top"><?php echo $orders_split->display_count($orders_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></td>
              <td class="smallText" align="right"><?php echo $orders_split->display_links($orders_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], osc_get_all_get_params(array('page', 'oID', 'action'))); ?></td>
             </tr>
            </table></td>
          </tr>
         </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_ORDER . '</strong>');

      $contents = array('form' => osc_draw_form('orders', 'orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO . '<br /><br /><strong>' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname . '</strong>');
      $contents[] = array('text' => '<br />' . osc_draw_checkbox_field('restock') . ' ' . TEXT_INFO_RESTOCK_PRODUCT_QUANTITY);
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;

    case 'archive':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_ARCHIVE . '</strong>');
      $contents = array('form' => osc_draw_form('archive', 'orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=archive_to_confirm') . osc_draw_hidden_field('oID=' . $oInfo->orders_id));
      $contents[] = array('text' => TEXT_INFO_ARCHIVE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname . '</strong><br />');
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_archive.gif', IMAGE_ARCHIVE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('orders.php', osc_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id)  . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;

    default:
    break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
       </tr>
      </table></td>
    </tr>
<?php
  }
?>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require('includes/footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php 
require('includes/application_bottom.php');
