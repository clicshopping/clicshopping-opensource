<?php
/*
 * cookie_usage.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>

<div class="contentContainer">
  <div class="contentText">
    <div class="panel panel-danger">
      <div class="panel-heading"><?php echo BOX_INFORMATION_HEADING; ?></div>
      <div class="panel-body">
        <?php echo BOX_INFORMATION; ?>
      </div>
    </div>

    <div class="panel panel-danger">
      <div class="panel-body">
        <?php echo TEXT_INFORMATION; ?>
      </div>
    </div>
  </div>
</div>
<div class="buttonSet pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, osc_href_link('index.php'), 'success', null, null); ?></div>

<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
