<?php
/*
 * stats_products_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

define('HEADING_TITLE', 'Products Notifications');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_MODEL', 'Brands');
define('TABLE_HEADING_COUNT', 'Nbr of notifications');
define('TABLE_HEADING_NAME', 'Customers Name');
define('TABLE_HEADING_EMAIL', 'Customers Email Address');
define('TABLE_HEADING_DATE', 'Notification Set on');
define('TABLE_HEADING_ACTION', 'Actions');
?>