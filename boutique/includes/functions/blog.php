<?php
/**
 * blog.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

/**
 * Display a short description for a product
 *
 * @param string $short_description, the short description of the product
 * @access public
 */
////
  function osc_display_blog_short_description() {
    global  $description, $blog_short_description;
    
    if ($blog_short_description > 0) {
      $short_description = utf8_encode(html_entity_decode($description ));
      $short_description = substr($short_description, 0, $blog_short_description);
      $short_description = osc_break_string($short_description, $blog_short_description, '-<br />') . ((strlen($description) >= $blog_short_description-1) ? ' ...' : '');
    }
    return $short_description;
  }
