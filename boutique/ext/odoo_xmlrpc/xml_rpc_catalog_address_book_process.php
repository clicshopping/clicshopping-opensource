<?php
/*
 * xml_rpc_catalog_address_book_process.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// **********************************
// search iso code ClicShopping
// **********************************

    $QcountryIdCustomer = $OSCOM_PDO->prepare("select entry_country_id
                                               from :table_address_book
                                               where customers_id = :customers_id
                                              ");
    $QcountryIdCustomer->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );
    $QcountryIdCustomer->execute();

    $country_id_customer = $QcountryIdCustomer->fetch();
    $country_id_customer = $country_id_customer['entry_country_id'];


    $QcountryCode = $OSCOM_PDO->prepare("select countries_iso_code_2
                                         from :table_countries
                                         where countries_id = :countries_id
                                        ");
    $QcountryCode->bindInt(':countries_id',(int)$country_id_customer);
    $QcountryCode->execute();

    $country_code = $QcountryCode->fetch();
    $country_code = $country_code['countries_iso_code_2'];

// **********************************
// search id country odoo
// **********************************
    $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
      $field_list = array('country_id',
                          'name'
                         );

    $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
    $country_id_odoo = $country_id_odoo[0][id];


// **********************************
// Search odoo customer id
// **********************************

     if ($OSCOM_Customer->getDefaultAddressID() != $_GET['edit']) {
       $search = $_GET['edit'];
     } else {
       $search = $OSCOM_Customer->getId();
     }


    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $search, 'res.partner');

// **********************************
// read id customer odoo
// **********************************
    $field_list = array('ref',
                        'id'
                        );

   $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
   $id_odoo_customer = $id_odoo_customer_array[0][id];

   if ($id_odoo_customer != '' && $odoo_create != '1') {
// **********************************
// Update address
// **********************************
      $values = array(
                      "name"    => new xmlrpcval( $lastname . ' ' . $firstname, "string"),
                      'street'=>new xmlrpcval($street_address,"string"),
                      'street2'=>new xmlrpcval($suburb,"string"),
                      'city'=>new xmlrpcval($city,"string"),
                      'zip'=>new xmlrpcval($postcode,"string"),
                      'country_id'=>new xmlrpcval($country_id_odoo,"int"),
                      'phone' => new xmlrpcval($telephone,"string"),
                      'mobile' => new xmlrpcval($cellular_phone,"string"),
                      'fax' => new xmlrpcval($fax,"string"),
                   );

      $OSCOM_ODOO->updateOdoo($id_odoo_customer, $values, "res.partner");

    } else {
// **********************************
// Write address
// **********************************
      $values = array(
                      "name"    => new xmlrpcval( $lastname . ' ' . $firstname, "string"),
                      'street'=>new xmlrpcval($street_address,"string"),
                      'street2'=>new xmlrpcval($suburb,"string"),
                      'city'=>new xmlrpcval($city,"string"),
                      'zip'=>new xmlrpcval($postcode,"string"),
                      'country_id'=>new xmlrpcval($country_id_odoo,"int"),
                      "comment" => new xmlrpcval('Web store New address book creation - relation with customer_id : ' . $OSCOM_Customer->getID() , "string"),
                      "ref" => new xmlrpcval('WebStore New address - ' . $new_address_book_id, "string"),
                      'phone' => new xmlrpcval($telephone,"string"),
                      'mobile' => new xmlrpcval($cellular_phone,"string"),
                      'fax' => new xmlrpcval($fax,"string"),
                      );

      $OSCOM_ODOO->createOdoo( $values, "res.partner");
    }
?>