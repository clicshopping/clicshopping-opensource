<?php
/*
 * google_sitemap_index.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

// Include the application_top.php script

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_categories.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_products.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');


    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_specials.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_products_heart.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_manufacturers.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_blog_catgories.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_blog_content.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_page_manager.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    $location = HTTP_SERVER . DIR_WS_HTTP_CATALOG . 'google_sitemap_featured_products.php';
    $url = $xml->addChild('url');
    $url->addChild('loc', htmlspecialchars(utf8_encode($location)));
    $url->addChild('lastmod', date ("Y-m-d", strtotime("now")));
    $url->addChild('changefreq', 'weekly');
    $url->addChild('priority', '0.5');

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
