<?php
/*
 * stats_products_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */
define('HEADING_TITLE', 'Surveillance des Produits');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_MODEL', 'Rèférence');
define('TABLE_HEADING_COUNT', 'Nbr de suveillance');
define('TABLE_HEADING_NAME', 'Nom Du Client');
define('TABLE_HEADING_EMAIL', 'Adresse Email du Clients');
define('TABLE_HEADING_DATE', 'Surveillé  depuis');
define('TABLE_HEADING_ACTION', 'Actions');
?>