<?php
/**
 * extended_last_run.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_LAST_RUN_OLD', 'It has been over 30 days since the extended security checks were last performed. Please re-run the extended security checks under Tools -&gt; Security Checks.');
?>
