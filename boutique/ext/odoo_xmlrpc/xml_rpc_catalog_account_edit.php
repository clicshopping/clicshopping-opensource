<?php
/*
 * xml_rpc_account_edit.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// **********************************
// Search odoo customer id
// **********************************
    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $OSCOM_Customer->getId(), 'res.partner');

// **********************************
// read id customer odoo
// **********************************
    $field_list = array('ref',
                        'id'
                        );

   $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
   $id_odoo_customer = $id_odoo_customer_array[0][id];

// **********************************
// Update data
// **********************************
    $values = array (
                      "name"    => new xmlrpcval( $lastname . ' ' . $firstname, "string"),
                      "email"  => new xmlrpcval($email_address, "string"),
                      'phone' => new xmlrpcval($telephone,"string"),
                      'mobile' => new xmlrpcval($cellular_phone,"string"),
                      'fax' => new xmlrpcval($fax,"string"),
                    );

    $OSCOM_ODOO->updateOdoo($id_odoo_customer, $values, "res.partner");

?>