<?php
/**
 * page_submit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE','Meta data management');

define('TAB_SUBMIT_DEFAULT','Default Meta Data');
define('TAB_SUBMIT_CATEGORIES','Categories');
define('TAB_SUBMIT_PRODUCTS_INFO','Products description');
define('TAB_SUBMIT_PRODUCTS_NEW','News');
define('TAB_SUBMIT_SPECIAL','Specials');
define('TAB_SUBMIT_REVIEWS','Reviews');

define('KEYWORDS_GOOGLE_TREND',' Google trend');
define('ANALYSIS_GOOGLE_TOOL','Analysis Google Tool');
define('SUCCESS_PAGE_UPDATED','Success');

define('TITLE_AIDE_IMAGE','Notes');
define('TEXT_PAGES_SUBMIT_INFORMATION_DEFAULT','Meta data management');
define('DISPLAY_TITLE_DEFAULT','Display default Title');
define('DISPLAY__DEFAULT','Display default ');
define('DISPLAY_DESCRIPTION_DEFAULT','Display default description');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_TITLE','Title');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_KEYWORDS','Keywords');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_DESCRIPTION','Description');
define('TEXT_SUBMIT_DEFAUT_LANGUAGE_FOOTER','Footer');



define('TEXT_PAGES_SUBMIT_INFORMATION_CATEGORIES','Meta data management by categories of site');
define('DISPLAY_TITLE_CATEGORIES','Display title defaut categories');
define('DISPLAY__CATEGORIES','Display default  categories');
define('DISPLAY_DESCRIPTION_CATEGORIES','Display default description categories');
define('TEXT_SUBMIT_LANGUAGE_CATEGORIES_TITLE','Title ');
define('TEXT_SUBMIT_LANGUAGE_CATEGORIES_KEYWORDS','Keywords ');
define('TEXT_SUBMIT_LANGUAGE_CATEGORIES_DESCRIPTION','Description');

define('TEXT_PAGES_SUBMIT_INFORMATION_PRODUCT_INFO','Meta data management by products description of site');
define('DISPLAY_TITLE_PRODUCT_INFO','Display default description products');
define('DISPLAY__PRODUCT_INFO','Display default products description ');
define('DISPLAY_DESCRIPTION_PRODUCT_INFO','Display default products description');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_TITLE','Title ');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_KEYWORDS','Keywords ');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_INFO_DESCRIPTION','Description');

define('TEXT_PAGES_SUBMIT_INFORMATION_PRODUCTS_NEW','Meta date management products new of site');
define('DISPLAY_TITLE_PRODUCTS_NEW','Display default products new title');
define('DISPLAY__PRODUCTS_NEW','Display  products new ');
define('DISPLAY_DESCRIPTION_PRODUCTS_NEW','Display default products new description');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_TITLE','Title ');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_KEYWORDS','Keywords ');
define('TEXT_SUBMIT_LANGUAGE_PRODUCTS_NEW_DESCRIPTION','Description ');

define('TEXT_PAGES_SUBMIT_INFORMATION_SPECIAL','Meta data management specials of site');
define('DISPLAY_TITLE_SPECIAL','Display default specials title');
define('DISPLAY__SPECIAL','Display default specials ');
define('DISPLAY_DESCRIPTION_SPECIAL','Display default specials description');
define('TEXT_SUBMIT_LANGUAGE_SPECIAL_TITLE','Title');
define('TEXT_SUBMIT_LANGUAGE_SPECIAL_KEYWORDS', 'Keywords ');
define('TEXT_SUBMIT_LANGUAGE_SPECIAL_DESCRIPTION','Description');

define('TEXT_PAGES_SUBMIT_INFORMATION_REVIEWS','Meta data management reviews of site');
define('DISPLAY_TITLE_REVIEWS','Title');
define('DISPLAY_REVIEWS','Display default reviews ');
define('DISPLAY_DESCRIPTION_REVIEWS','Displays default reviews description');
define('TEXT_SUBMIT_LANGUAGE_REVIEWS_TITLE','Title');
define('TEXT_SUBMIT_LANGUAGE_REVIEWS_KEYWORDS','Keywords ');
define('TEXT_SUBMIT_LANGUAGE_REVIEWS_DESCRIPTION','Description');


define('TITLE_HELP_SUBMIT', 'Note on the search engine optimization management');
define('HELP_SUBMIT', '<li>The system automatically manages the search engine optimization.<br />
<blockquote>It automatically included elements in the following order if the fields are not filled:<br />
Title : the name of your product (ex leather jacket)<br />
Description: The product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Category: Category Product<br />
</blockquote></li>
<li>If you fill in the fields, the tags appear in the following way:<br />
<blockquote>
Title: The title of fields, the name of your product (ex leather jacket)<br />
Description: The field description, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the fields keywords, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
</blockquote></li>
<li><strong>Please do not put too keywords (maximum 10)</strong> and establish consistency between each field to find the words the most important first</li>
');
?>
