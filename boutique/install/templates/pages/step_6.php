<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */


  require ('includes/languages/' . $language . '/' . basename($_SERVER['PHP_SELF']));


  osc_db_connect(trim($_POST['DB_SERVER']), trim($_POST['DB_SERVER_USERNAME']), trim($_POST['DB_SERVER_PASSWORD']));
  osc_db_select_db(trim($_POST['DB_DATABASE']));

  osc_db_query('update configuration
                set configuration_value = "' . trim($_POST['CFG_STORE_NAME']) . '"
                where configuration_key = "STORE_NAME"
               ');
  osc_db_query('update configuration
                set configuration_value = "' . trim($_POST['CFG_STORE_OWNER_NAME']) . '"
                where configuration_key = "STORE_OWNER"
               ');
  osc_db_query('update configuration
                set configuration_value = "' . trim($_POST['CFG_STORE_OWNER_EMAIL_ADDRESS']) . '"
                where configuration_key = "STORE_OWNER_EMAIL_ADDRESS"
               ');

  if (!empty($_POST['CFG_STORE_OWNER_NAME']) && !empty($_POST['CFG_STORE_OWNER_EMAIL_ADDRESS'])) {
    osc_db_query('update configuration
                  set configuration_value = "\"' . trim($_POST['CFG_STORE_OWNER_NAME']) . '\" <' . trim($_POST['CFG_STORE_OWNER_EMAIL_ADDRESS']) . '>"
                  where configuration_key = "EMAIL_FROM"
                ');
  } else {
    osc_db_query('update configuration
                  set configuration_value = "' . trim($_POST['CFG_STORE_OWNER_EMAIL_ADDRESS']) . '"
                  where configuration_key = "EMAIL_FROM"
                ');
  }

  osc_db_query('update configuration
                  set configuration_value = "' . trim($_POST['CFG_STORE_OWNER_EMAIL_ADDRESS']) . '"
                  where configuration_key = "SEND_EXTRA_ORDER_EMAILS_TO"
                ');

  if ( !empty($_POST['CFG_ADMINISTRATOR_USERNAME']) ) {
    $check_query = osc_db_query('select user_name
                                 from administrators
                                 where user_name = "' . trim($_POST['CFG_ADMINISTRATOR_USERNAME']) . '"
                               ');

    if (osc_db_num_rows($check_query)) {
      osc_db_query('update administrators
                    set user_password = "' . osc_encrypt_password(trim($_POST['CFG_ADMINISTRATOR_PASSWORD'])) . '",
                    name = "' . trim($_POST['CFG_ADMINISTRATOR_FIRSTNAME']) . '",
                    first_name ="' . trim($_POST['CFG_ADMINISTRATOR_NAME']) . '",
                    where user_name = "' . trim($_POST['CFG_ADMINISTRATOR_USERNAME']) . '"
                   ');
    } else {

      osc_db_query('insert into administrators (user_name,
                                                user_password,
                                                name,
                                                first_name
                                               )
                    values ("' . trim($_POST['CFG_ADMINISTRATOR_USERNAME']) . '",
                            "' . osc_encrypt_password(trim($_POST['CFG_ADMINISTRATOR_PASSWORD'])) .'",
                            "' . trim($_POST['CFG_ADMINISTRATOR_FIRSTNAME']) . '",
                            "' . trim($_POST['CFG_ADMINISTRATOR_NAME']) . '"
                            )
                   ');
    }
  }

  osc_db_query('update configuration
                set configuration_value = "' . trim($_POST['CFG_STORE_OWNER_EMAIL_ADDRESS']) . '"
                where configuration_key = "MODULE_PAYMENT_PAYPAL_EXPRESS_SELLER_ACCOUNT"
               ');

?>

  <div class="contentBlock">
    <div class="contentPane"><h1><?php echo TEXT_END_INSTALLATION; ?> </h1></div>
  </div>
  <div>
    <div class="infoPaneContents"><?php echo TEXT_END_INSTALLATION_1; ?></div>
  </div>
<br />
<br />

<div class="contentPane">
  <center>
    <?php echo TEXT_STORE_DONATION; ?><br /><br />
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank"><input name="cmd" type="hidden" value="_s-xclick" /> <input name="hosted_button_id" type="hidden" value="VGKJDJF3KNYXE" /> <input alt="PayPal - the easiest way to pay online and more secure !" name="submit" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" style="border-width: 0px; border-style: solid; height: 48px; width: 200px;" type="image" /> <img alt="" border="0" height="1" src="https://www.paypalobjects.com/fr_FR/i/scr/pixel.gif" width="1" /></form>
  </center>
</div>

<br />
<br />

  <div class="contentPane">

<?php
    $dir_fs_document_root = $_POST['DIR_FS_DOCUMENT_ROOT'];
    if ((substr($dir_fs_document_root, -1) != '\\') && (substr($dir_fs_document_root, -1) != '/')) {
      if (strrpos($dir_fs_document_root, '\\') !== false) {
        $dir_fs_document_root .= '\\';
      } else {
        $dir_fs_document_root .= '/';
      }
    }

  osc_db_query('update configuration set configuration_value = "' . $dir_fs_document_root . 'cache/work/" where configuration_key = "DIR_FS_CACHE"');
  osc_db_query('update configuration set configuration_value = "' . $dir_fs_document_root . 'cache/work/" where configuration_key = "SESSION_WRITE_DIRECTORY"');

    if ($handle = opendir($dir_fs_document_root . 'cache/work/')) {
      while (false !== ($filename = readdir($handle))) {
        if (substr($filename, strrpos($filename, '.')) == '.cache') {
          @unlink($dir_fs_document_root . 'cache/work/' . $filename);
        }
      }

      closedir($handle);
    }

    $http_url = parse_url($_POST['HTTP_WWW_ADDRESS']);
    $http_server = $http_url['scheme'] . '://' . $http_url['host'];
    $http_catalog = $http_url['path'];
    if (isset($http_url['port']) && !empty($http_url['port'])) {
      $http_server .= ':' . $http_url['port'];
    }

    if (substr($http_catalog, -1) != '/') {
      $http_catalog .= '/';
    }

    $admin_folder = 'ClicShoppingAdmin';
    if (isset($_POST['CFG_ADMIN_DIRECTORY']) && !empty($_POST['CFG_ADMIN_DIRECTORY']) && osc_is_writable($dir_fs_document_root) && osc_is_writable($dir_fs_document_root . 'admin')) {
      $admin_folder = preg_replace('/[^a-zA-Z0-9]/', '', trim($_POST['CFG_ADMIN_DIRECTORY']));

      if (empty($admin_folder)) {
        $admin_folder = 'ClicShoppingAdmin';
      }
    }

  // Catalog

  $file_contents = '<?php' . "\n" .
                   '  define(\'HTTP_SERVER\', \'' . $http_server . '\');' . "\n" .
                   '  define(\'HTTPS_SERVER\', \'' . $http_server . '\');' . "\n" .
                   '  define(\'ENABLE_SSL\', false);' . "\n" .
                   '  define(\'HTTP_COOKIE_DOMAIN\', \'' . $http_url['host'] . '\');' . "\n" .
                   '  define(\'HTTPS_COOKIE_DOMAIN\', \'' . $http_url['host'] . '\');' . "\n" .
                   '  define(\'HTTP_COOKIE_PATH\', \'' . $http_catalog . '\');' . "\n" .
                   '  define(\'HTTPS_COOKIE_PATH\', \'' . $http_catalog . '\');' . "\n" .
                   '  define(\'DIR_WS_HTTP_CATALOG\', \'' . $http_catalog . '\');' . "\n" .
                   '  define(\'DIR_WS_HTTPS_CATALOG\', \'' . $http_catalog . '\');' . "\n" .
                   '  define(\'DIR_WS_SOURCES\', \'sources/\');' . "\n" .
                   '  define(\'DIR_WS_TEMPLATE\', DIR_WS_SOURCES . \'template/\');' . "\n" .
                   '  define(\'DIR_WS_IMAGES\', DIR_WS_SOURCES .  \'image/\');' . "\n" .
                   '  define(\'DIR_WS_CATALOG_PRODUCTS_IMAGES\', DIR_WS_SOURCES);' . "\n" .
                   '  define(\'DIR_WS_ICONS\', DIR_WS_IMAGES . \'icons/\');' . "\n" .
                   '  define(\'DIR_WS_DEFAULT_IMAGES\', DIR_WS_IMAGES . \'default/\');' . "\n" .
                   '  define(\'DIR_WS_INCLUDES\', \'includes/\');' . "\n\n" .
                   '  define(\'DIR_WS_BOXES\', \'/boxes/\');' . "\n" .
                   '  define(\'DIR_WS_TEMPLATE_MODULES\', \'/modules/\');' . "\n" .
                   '  define(\'DIR_WS_TEMPLATE_GRAPHISM\', \'/graphism/\');' . "\n" .
                   '  define(\'DIR_WS_TEMPLATE_FILES\', \'/files/\');' . "\n" .
                   '  define(\'DIR_WS_FUNCTIONS\', DIR_WS_INCLUDES . \'functions/\');' . "\n" .
                   '  define(\'DIR_WS_CLASSES\', DIR_WS_INCLUDES . \'classes/\');' . "\n" .
                   '  define(\'DIR_WS_MODULES\', DIR_WS_INCLUDES . \'modules/\');' . "\n" .
                   '  define(\'DIR_WS_LANGUAGES\', DIR_WS_SOURCES . \'languages/\');' . "\n" .
                   '  define(\'DIR_WS_EXT\', \'ext/\');' . "\n\n" .
                   '  define(\'DIR_FS_CATALOG\', \'' . $dir_fs_document_root . '\');' . "\n" .
                   '  define(\'DIR_FS_DOWNLOAD\', DIR_FS_CATALOG . DIR_WS_SOURCES . \'download/\');' . "\n" .
                   '  define(\'DIR_FS_DOWNLOAD_PUBLIC\', DIR_FS_CATALOG . DIR_WS_SOURCES . \'public/download/\');' . "\n" .
                   '  // define our database connection;' . "\n\n" .
                   '  define(\'DB_DRIVER\', \'mysql_standard\');' . "\n" .
                   '  define(\'DB_DATABASE_TYPE\', \'mysql\');' . "\n" .
                   '  define(\'DB_SERVER\', \'' . trim($_POST['DB_SERVER']) . '\');' . "\n" .
                   '  define(\'DB_SERVER_USERNAME\', \'' . trim($_POST['DB_SERVER_USERNAME']) . '\');' . "\n" .
                   '  define(\'DB_SERVER_PASSWORD\', \'' . trim($_POST['DB_SERVER_PASSWORD']) . '\');' . "\n" .
                   '  define(\'DB_DATABASE\', \'' . trim($_POST['DB_DATABASE']) . '\');' . "\n" .
                   '  define(\'DB_TABLE_PREFIX\', \'' . trim($_POST['DB_TABLE_PREFIX']) . '\');' . "\n" .
                   '  define(\'USE_PCONNECT\', \'false\');' . "\n" .
                   '  define(\'STORE_SESSIONS\', \'mysql\');' . "\n\n" .
                   '  // define cache;' . "\n" .
                   '  define(\'DIR_FS_CACHE_PUBLIC\', DIR_FS_CATALOG . \'cache/public/\');' . "\n" .
                   '  define(\'DIR_FS_CACHE2\', DIR_FS_CATALOG . \'cache/work/\');' . "\n";



    if (isset($_POST['CFG_TIME_ZONE'])) {
      $file_contents .= '  define(\'CFG_TIME_ZONE\', \'' . trim($_POST['CFG_TIME_ZONE']) . '\');' . "\n";
    }

    $file_contents .= '?>';

    $fp = fopen($dir_fs_document_root . 'includes/configure.php', 'w');
    fputs($fp, $file_contents);
    fclose($fp);

// administration

  @chmod($dir_fs_document_root . 'includes/configure.php', 0644);

  $file_contents = '<?php' . "\n" .
                   '  define(\'HTTP_SERVER\', \'' . $http_server . '\');' . "\n" .
                   '  define(\'HTTPS_SERVER\', \'' . $http_server . '\');' . "\n" .
                   '  define(\'ENABLE_SSL\', false);' . "\n" .
                   '  define(\'HTTP_COOKIE_DOMAIN\', \'\');' . "\n" .
                   '  define(\'HTTPS_COOKIE_DOMAIN\', \'\');' . "\n" .
                   '  define(\'HTTP_COOKIE_PATH\', \'' . $http_catalog . $admin_folder . '\');' . "\n" .
                   '  define(\'HTTPS_COOKIE_PATH\', \'' . $http_catalog . $admin_folder . '\');' . "\n" .
                   '  define(\'HTTP_CATALOG_SERVER\', \'' . $http_server . '\');' . "\n" .
                   '  define(\'HTTPS_CATALOG_SERVER\', \'' . $http_server . '\');' . "\n" .
                   '  define(\'ENABLE_SSL_CATALOG\', \'false\');' . "\n" .
                   '  define(\'DIR_FS_DOCUMENT_ROOT\', \'' . $dir_fs_document_root . '\');' . "\n" .
                   '  define(\'DIR_WS_HTTPS_ADMIN\', \'' . $http_catalog .  $admin_folder . '/\');' . "\n" .
                   '  define(\'DIR_WS_ADMIN\', \'' . $http_catalog .  $admin_folder . '/\');' . "\n" .
                   '  define(\'DIR_FS_ADMIN\', \'' . $dir_fs_document_root .  $admin_folder . '/\');' . "\n" .
                   '  define(\'DIR_WS_CATALOG\', \'' . $http_catalog . '\');' . "\n" .
                   '  define(\'DIR_WS_HTTPS_CATALOG\', \'' . $http_catalog . '\');' . "\n" .
                   '  define(\'DIR_FS_CATALOG\', \'' . $dir_fs_document_root . '\');' . "\n" .
                   '  define(\'DIR_WS_IMAGES\', \'images/\');' . "\n" .
                   '  define(\'DIR_WS_ICONS\', DIR_WS_IMAGES . \'icons/\');' . "\n" .
                   '  define(\'DIR_WS_SOURCES\', \'sources/\');' . "\n" .
                   '  define(\'DIR_WS_TEMPLATE\', DIR_WS_SOURCES . \'template/\');' . "\n" .
                   '  define(\'DIR_WS_CATALOG_IMAGES\', DIR_WS_CATALOG . DIR_WS_SOURCES . \'image/\');' . "\n" .
                   '  define(\'DIR_FS_CATALOG_IMAGES\', DIR_FS_CATALOG . DIR_WS_SOURCES . \'image/\');' . "\n" .
                   '  define(\'DIR_WS_CATALOG_PRODUCTS_IMAGES\', DIR_WS_CATALOG . DIR_WS_SOURCES);' . "\n" .
                   '  define(\'DIR_FS_CATALOG_PRODUCTS_IMAGES\', DIR_FS_CATALOG . DIR_WS_SOURCES);' . "\n" .
                   '  define(\'DIR_WS_INCLUDES\', \'includes/\');' . "\n" .
                   '  define(\'DIR_WS_FUNCTIONS\', DIR_WS_INCLUDES . \'functions/\');' . "\n" .
                   '  define(\'DIR_WS_CLASSES\', DIR_WS_INCLUDES . \'classes/\');' . "\n" .
                   '  define(\'DIR_WS_MODULES\', DIR_WS_INCLUDES . \'modules/\');' . "\n" .
                   '  define(\'DIR_WS_LANGUAGES\', DIR_WS_INCLUDES . \'languages/\');' . "\n" .
                   '  define(\'DIR_WS_CATALOG_LANGUAGES\', DIR_WS_CATALOG . DIR_WS_SOURCES . \'languages/\');' . "\n" .
                   '  define(\'DIR_FS_CATALOG_LANGUAGES\', DIR_FS_CATALOG . DIR_WS_SOURCES . \'languages/\');' . "\n" .
                   '  define(\'DIR_FS_CATALOG_MODULES\', DIR_FS_CATALOG . DIR_WS_MODULES);' . "\n" .
                   '  define(\'DIR_FS_BACKUP\', DIR_FS_ADMIN . \'backups/\');' . "\n" .
                   '  define(\'DIR_FS_DOWNLOAD\', DIR_FS_CATALOG . DIR_WS_SOURCES .  \'download/\');' . "\n\n" .
                   '  define(\'DIR_FS_DOWNLOAD_PUBLIC\', DIR_FS_CATALOG . DIR_WS_SOURCES . \'public/download/\');' . "\n"  .

                   '  // define our database connection' . "\n" .
                   '  define(\'DB_DRIVER\', \'mysql_standard\');' . "\n" .
                   '  define(\'DB_DATABASE_TYPE\', \'mysql\');' . "\n" .
                   '  define(\'DB_SERVER\', \'' . trim($_POST['DB_SERVER']) . '\');' . "\n" .
                   '  define(\'DB_SERVER_USERNAME\', \'' . trim($_POST['DB_SERVER_USERNAME']) . '\');' . "\n" .
                   '  define(\'DB_SERVER_PASSWORD\', \'' . trim($_POST['DB_SERVER_PASSWORD']) . '\');' . "\n" .
                   '  define(\'DB_DATABASE\', \'' . trim($_POST['DB_DATABASE']) . '\');' . "\n" .
                   '  define(\'DB_TABLE_PREFIX\', \'' . trim($_POST['DB_TABLE_PREFIX']) . '\');' . "\n" .
                   '  define(\'USE_PCONNECT\', \'false\');' . "\n" .
                   '  define(\'STORE_SESSIONS\', \'mysql\');' . "\n\n".

                   '  // define cache;' . "\n" .
                   '  define(\'DIR_FS_CACHE2\', DIR_FS_CATALOG . \'cache/work/\');' . "\n";



    if (isset($_POST['CFG_TIME_ZONE'])) {
      $file_contents .= '  define(\'CFG_TIME_ZONE\', \'' . trim($_POST['CFG_TIME_ZONE']) . '\');' . "\n";
    }

    $file_contents .= '?>';

  $fp = fopen($dir_fs_document_root . 'ClicShoppingAdmin/includes/configure.php', 'w');
    fputs($fp, $file_contents);
    fclose($fp);

  @chmod($dir_fs_document_root . 'ClicShoppingAdmin/includes/configure.php', 0644);

  if ($admin_folder != 'ClicShoppingAdmin') {
    @rename($dir_fs_document_root . 'ClicShoppingAdmin', $dir_fs_document_root . $admin_folder);
  }
?>

    <div class="alert alert-danger">
     <h3><?php echo TEXT_END_INSTALLATION_2; ?></h3>
     <?php echo TEXT_END_INSTALLATION_3; ?>
        <blockquote>
           <li><?php echo TEXT_END_INSTALLATION_4 . $dir_fs_document_root . 'install'; ?>.</li>
<?php
/*
  if ($admin_folder == 'ClicShoppinAdmin') {
?>

      <li>Rename the Administration Tool directory located at <?php echo $dir_fs_document_root . 'ClicShoppingAdmin'; ?>.</li>

<?php
  }
*/
  if (file_exists($dir_fs_document_root . 'includes/configure.php') && is_writable($dir_fs_document_root . 'includes/configure.php')) {
?>
            <li><?php echo  TEXT_END_INSTALLATION_5. $dir_fs_document_root . 'includes/configure.php' . TEXT_END_INSTALLATION_6; ?></li>
<?php
  }

  if (file_exists($dir_fs_document_root .  $admin_folder . '/includes/configure.php') && is_writable($dir_fs_document_root . $admin_folder . '/includes/configure.php')) {
?>
           <li><?php echo TEXT_END_INSTALLATION_7 . $dir_fs_document_root . $admin_folder . '/includes/configure.php' . TEXT_END_INSTALLATION_6; ?></li>
<?php
  }
?>
           <li><?php echo TEXT_END_INSTALLATION_8; ?></li>
        </blockquote>
    </div>
  </div>
  <div class="contentPane"  style="padding-top:30px;">
    <div style="padding-top:20px; font-weight:bold; text-align:center;"><?php echo TEXT_END_INSTALLATION_SUCCESS; ?></div>
    <div class="row"   style="padding-top:30px;">
      <span class="pull-left"><?php echo osc_draw_button(TEXT_END_ACCESS_CATALOG, 'cart', $http_server . $http_catalog . 'index.php', 'primary', array('newwindow' => 1), 'btn-success btn-block'); ?></span>
      <span class="pull-right"><?php echo osc_draw_button(TEXT_END_ACCESS_ADMIN, 'locked', $http_server . $http_catalog . $admin_folder . '/index.php', 'primary', array('newwindow' => 1), 'btn-info btn-block'); ?></span>
    </div>
  </div>
<br /><br />


