<?php
/*
 * create_account_pro.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if ((MODE_B2B_B2C != 'true')) {
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('create_account_pro'));

  $process = false;
  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $process = true;


    if (ACCOUNT_GENDER_PRO == 'true') $gender = isset($_POST['gender']) ? trim($_POST['gender']) : null;
    $firstname = isset($_POST['firstname']) ? trim($_POST['firstname']) : null;
    $lastname = isset($_POST['lastname']) ? trim($_POST['lastname']) : null;
    if (ACCOUNT_DOB_PRO == 'true') $dob = isset($_POST['dob']) ? trim($_POST['dob']) : null;
    $email_address =  isset($_POST['email_address']) ? trim($_POST['email_address']) : null;
    $email_address_confirmation = isset($_POST['email_address_confirmation']) ? trim($_POST['email_address_confirmation']) : null;
    if (ACCOUNT_COMPANY_PRO == 'true') $company = isset($_POST['company']) ? trim($_POST['company']) : null;
    if (ACCOUNT_SIRET_PRO == 'true') $siret = isset($_POST['siret']) ? trim($_POST['siret']) : null;
    if (ACCOUNT_APE_PRO == 'true') $ape = isset($_POST['ape']) ? trim($_POST['ape']) : null;
    if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $tva_intracom = isset($_POST['tva_intracom']) ? trim($_POST['tva_intracom']) : null;
    if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $iso = isset($_POST['ISO']) ? trim($_POST['ISO']) : null;
    $street_address = isset($_POST['street_address']) ? trim($_POST['street_address']) : null;
    if (ACCOUNT_SUBURB_PRO == 'true') $suburb = isset($_POST['suburb']) ? trim($_POST['suburb']) : null;
    $postcode = isset($_POST['postcode']) ? trim($_POST['postcode']) : null;
    $city = isset($_POST['city']) ? trim($_POST['city']) : null;
    $company_website = isset($_POST['company_website']) ? trim($_POST['company_website']) : null;

    $zone_id = false;
    if (ACCOUNT_STATE_PRO == 'true') {
      $state = osc_db_prepare_input($_POST['state']);
      if (isset($_POST['zone_id'])) {
        $zone_id = osc_db_prepare_input($_POST['zone_id']);
      }
    }

    $country = isset($_POST['postcode']) ? trim($_POST['country']) : null;;
    $telephone = isset($_POST['telephone']) ? trim($_POST['telephone']) : null;

    if (ACCOUNT_CELLULAR_PHONE_PRO == 'true') $cellular_phone = isset($_POST['cellular_phone']) ? trim($_POST['cellular_phone']) : null;
    if (ACCOUNT_FAX_PRO == 'true')     $fax = isset($_POST['fax']) ? trim($_POST['fax']) : null;

    $newsletter = isset($_POST['newsletter']) ? trim($_POST['newsletter']) : null;
    $password = isset($_POST['password']) ? trim($_POST['password']) : null;
    $confirmation = isset($_POST['confirmation']) ? trim($_POST['confirmation']) : null;

    $error = false;

// Clients B2B : Controle entree de la societe
    if (ACCOUNT_COMPANY_PRO == 'true') {
      if (strlen($company) < ENTRY_COMPANY_PRO_MIN_LENGTH) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_COMPANY_ERROR_PRO);
      }
    }

// Clients B2B : Controle entree numero de siret
    if (ACCOUNT_SIRET_PRO == 'true') {
        if (strlen($siret) < ENTRY_SIRET_MIN_LENGTH) {
          $error = true;

          $OSCOM_MessageStack->addError('create_account_pro', ENTRY_SIRET_ERROR);
        }
    }

// Clients B2B : Controle entree code APE
    if (ACCOUNT_APE_PRO == 'true') {
      if (strlen($ape) < ENTRY_CODE_APE_MIN_LENGTH) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_CODE_APE_ERROR);
      }
    }

// Clients B2B : Controle entree numero de TVA Intracom
    if (ACCOUNT_TVA_INTRACOM_PRO == 'true') {
      if (strlen($tva_intracom) < ENTRY_TVA_INTRACOM_MIN_LENGTH) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_TVA_INTRACOM_ERROR);
      }
    }

// Clients B2C : Controle selection de la civilite
    if (ACCOUNT_GENDER_PRO == 'true') {
      if ( ($gender != 'm') && ($gender != 'f') ) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_GENDER_ERROR_PRO);
      }
    }

// Clients B2C : Controle entree du prenom
    if (strlen($firstname) < ENTRY_FIRST_NAME_PRO_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_FIRST_NAME_ERROR_PRO);
    }

// Clients B2C : Controle entree du nom de famille
    if (strlen($lastname) < ENTRY_LAST_NAME_PRO_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_LAST_NAME_ERROR_PRO);
    }

// Clients B2C : Controle entree date de naissance
    if (ACCOUNT_DOB_PRO == 'true') {
      if ((strlen($dob) < ENTRY_DOB_MIN_LENGTH) || (!empty($dob) && (!is_numeric(osc_date_raw($dob)) || !@checkdate(substr(osc_date_raw($dob), 4, 2), substr(osc_date_raw($dob), 6, 2), substr(osc_date_raw($dob), 0, 4))))) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_DATE_OF_BIRTH_ERROR_PRO);
      }
    }

// Clients B2C : Controle entree adresse e-mail
    if (osc_validate_email($email_address) == false) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_EMAIL_ADDRESS_CHECK_ERROR_PRO);
      
    } elseif ($email_address != $email_address_confirmation) {
    
      $error = true;
      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_EMAIL_ADDRESS_CONFIRMATION_CHECK_ERROR_PRO);

    } else {

      $Qcheckemail = $OSCOM_PDO->prepare('select count(*) as total
                                          from :table_customers
                                          where customers_email_address = :customers_email_address
                                         ');
      $Qcheckemail->bindValue(':customers_email_address', osc_db_input($email_address) );

      $Qcheckemail->execute();

      $check_email = $Qcheckemail->fetch();

      if ($check_email['total'] > 0) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS_PRO);
      }

// check if mail on newsletter_no_account	  
      $email_address1 = osc_db_prepare_input($email_address);

      $QcheckEmailNoAccount = $OSCOM_PDO->prepare('select count(*) as total 
                                                   from :table_newsletter_no_account 
                                                   where customers_email_address = :customers_email_address
                                                  ');
      $QcheckEmailNoAccount->bindValue(':customers_email_address', $email_address1);
      $QcheckEmailNoAccount->execute();
      $check_email_no_account = $QcheckEmailNoAccount->fetch();

      if ($check_email_no_account['total'] > 0) {
        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_newsletter_no_account 
                                        where customers_email_address = :email_address
                                      ');
        $Qdelete->bindValue(':email_address', $email_address);
        $Qdelete->execute();
      }	  
    }

// Clients B2C : Controle entree adresse
    if (strlen($street_address) < ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_STREET_ADDRESS_ERROR_PRO);
    }

// Clients B2C : Controle entree code postal
    if (strlen($postcode) < ENTRY_POSTCODE_PRO_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_POST_CODE_ERROR_PRO);
    }

// Clients B2C : Controle entree de la ville
    if (strlen($city) < ENTRY_CITY_PRO_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_CITY_ERROR_PRO);
    }

// Clients B2B : Controle de la selection du pays qui ce fait maintenant depuis le code ISO e cause du javascript d'affichage ISO TVA Intracom
    if (is_numeric($country) == false) {

      $Qcheck = $OSCOM_PDO->prepare('select countries_id
                                     from :table_countries
                                     where countries_iso_code_2 = :countries_iso_code_2
                                    ');
      $Qcheck->bindValue(':countries_iso_code_2', $country);
      $Qcheck->execute();

      $check = $Qcheck->fetch();

      $country = $check['countries_id'];

    } else {
      $error = true;
      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_COUNTRY_ERROR_PRO);
    }


// Clients B2C : Controle entree du departement
    if (ACCOUNT_STATE_PRO == 'true') {

      $zone_id = 0;
      $Qcheck = $OSCOM_PDO->prepare("select count(*) as total 
                                     from :table_zones 
                                     where zone_country_id = :zone_country_id
                                    ");
      $Qcheck->bindInt(':zone_country_id', (int)$country);
      $Qcheck->execute();

      $check = $Qcheck->fetch();
            
      $entry_state_has_zones = ($check['total'] > 0);

      if ($entry_state_has_zones == true) {

        $Qzone = $OSCOM_PDO->prepare('select distinct zone_id
                                     from :table_zones
                                     where zone_country_id = :zone_country_id
                                     and (zone_name = :zone_name or zone_code = :zone_code)
                                     ');
        $Qzone->bindInt(':zone_country_id', $country);
        $Qzone->bindValue(':zone_name', $state);
        $Qzone->bindValue(':zone_code', $state);
        $Qzone->execute();

        $zone = $Qzone->fetchAll();

        if ( count($zone) === 1 ) {
          $zone_id = (int)$zone[0]['zone_id'];
        } else {
          $error = true;

          $OSCOM_MessageStack->addError('create_account_pro', ENTRY_STATE_ERROR_SELECT_PRO);
        }
      } else {
        if (strlen($state) < ENTRY_STATE_PRO_MIN_LENGTH) {
          $error = true;

          $OSCOM_MessageStack->addError('create_account_pro', ENTRY_STATE_ERROR_PRO);
        }
      }
    }

// Clients B2C : Controle entree numero de telephone
    if (strlen($telephone) < ENTRY_TELEPHONE_PRO_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('create_account_pro', ENTRY_TELEPHONE_NUMBER_ERROR_PRO);
    }

// Clients B2B : Controle entree du mot de passe selon si l'approbation des membres est sur false
     if (MEMBER == 'false'){
      if (strlen($password) < ENTRY_PASSWORD_PRO_MIN_LENGTH) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_PASSWORD_ERROR_PRO);
      } elseif ($password != $confirmation) {
        $error = true;

        $OSCOM_MessageStack->addError('create_account_pro', ENTRY_PASSWORD_ERROR_NOT_MATCHING_PRO);
      }
    }

// Groupe par defaut e utiliser pour les nouveaux clients
    $QcustomersGroup = $OSCOM_PDO->prepare('select group_order_taxe,
                                                   group_payment_unallowed,
                                                   group_shipping_unallowed
                                           from :table_customers_groups
                                           where customers_group_id = :customers_group_id
                                           ');
    $QcustomersGroup->bindInt(':customers_group_id', (int)ACCOUNT_GROUP_DEFAULT_PRO);
    $QcustomersGroup->execute();

// Groupe par defaut e utiliser pour les nouveaux clients

    if ($QcustomersGroup->fetch() !== false) {
      $customers_group =  $QcustomersGroup->fetch();
    }

// Controle si le compte doit etre valide selon la configuration d'approbation des membres
    if (MEMBER == 'false'){
      $member_level_approbation = '1';
    } else {
      $member_level_approbation = '0';
    }

// Autorisation par defaut au client de pouvoir modifier les informations sur la societe
    if (ACCOUNT_MODIFY_PRO == 'false') {
      $customers_modify_company = '0';
    } else {
      $customers_modify_company = '1';
    }

// Autorisation par defaut au client de pouvoir modifier l'adresse principale
    if (ACCOUNT_MODIFY_ADRESS_DEFAULT_PRO == 'false') {
      $customers_modify_address_default = '0';
    } else {
      $customers_modify_address_default = '1';
    }

// Autorisation par defaut au client de pouvoir ajouter des adresses dans son carnet
    if (ACCOUNT_ADRESS_BOOK_PRO == 'false') {
      $customers_add_address = '0';
    } else {
      $customers_add_address = '1';
    }

    if ( $error === false ) {

// Enregistrement des informations du client dans la base de donnees
// member_level sur 1 permet d'eviter d'avoir e faire une approbation pour des clients normaux
      $sql_data_array = array('customers_firstname' => $firstname,
                              'customers_lastname' => $lastname,
                              'customers_email_address' => $email_address,
                              'customers_telephone' => $telephone,
                              'customers_newsletter' => $newsletter,
                              'customers_password' => osc_encrypt_password($password),
                              'languages_id' => $_SESSION['languages_id'],
                              'member_level' => $member_level_approbation,
                              'customers_modify_company' => $customers_modify_company,
                              'customers_modify_address_default' => $customers_modify_address_default,
                              'customers_add_address' => $customers_add_address);

      if (ACCOUNT_CELLULAR_PHONE_PRO == 'true')  $sql_data_array['customers_cellular_phone'] = $cellular_phone;
      if (ACCOUNT_FAX_PRO == 'true')  $sql_data_array['customers_fax'] = $fax;

      if ($QcustomersGroup->fetch() !== false) $sql_data_array['customers_group_id'] = ACCOUNT_GROUP_DEFAULT_PRO;
      if ($QcustomersGroup->fetch() !== false) $sql_data_array['customers_options_order_taxe'] = $customers_group['group_order_taxe'];
      if (ACCOUNT_GENDER_PRO == 'true') $sql_data_array['customers_gender'] = $gender;
      if (ACCOUNT_DOB_PRO == 'true') $sql_data_array['customers_dob'] = osc_date_raw($dob);
      if (ACCOUNT_COMPANY_PRO == 'true') $sql_data_array['customers_company'] = $company;
      if (ACCOUNT_SIRET_PRO == 'true') $sql_data_array['customers_siret'] = $siret;
      if (ACCOUNT_APE_PRO == 'true') $sql_data_array['customers_ape'] = $ape;
      if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $sql_data_array['customers_tva_intracom'] = $tva_intracom;
      if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $sql_data_array['customers_tva_intracom_code_iso'] = $iso;


      $OSCOM_PDO->save('customers', $sql_data_array);

      $customer_id = $OSCOM_PDO->lastInsertId();
// save element in address book
      $sql_data_array = array('customers_id' => (int)$customer_id,
                              'entry_firstname' => $firstname,
                              'entry_lastname' => $lastname,
                              'entry_street_address' => $street_address,
                              'entry_postcode' => $postcode,
                              'entry_city' => $city,
                              'entry_country_id' => $country);

      if (ACCOUNT_GENDER_PRO == 'true') $sql_data_array['entry_gender'] = $gender;
      if (ACCOUNT_COMPANY_PRO == 'true') $sql_data_array['entry_company'] = $company;
      if (ACCOUNT_SUBURB_PRO == 'true') $sql_data_array['entry_suburb'] = $suburb;
      if (ACCOUNT_STATE_PRO == 'true') {
        if ($zone_id > 0) {
          $sql_data_array['entry_zone_id'] = $zone_id;
          $sql_data_array['entry_state'] = '';
        } else {
          $sql_data_array['entry_zone_id'] = '0';
          $sql_data_array['entry_state'] = $state;
        }
      }

      $OSCOM_PDO->save('address_book', $sql_data_array);

      $address_id = $OSCOM_PDO->lastInsertId();

      $OSCOM_PDO->save('customers', array('customers_default_address_id' => (int)$address_id),
                                    array('customers_id' => (int)$customer_id)
                          );

      $OSCOM_PDO->save('customers_info', array('customers_info_id' => (int)$customer_id,
                                              'customers_info_number_of_logons' => '0',
                                              'customers_info_date_account_created' => 'now()'
                                              )
                          );

      if (SESSION_RECREATE == 'True') {
        osc_session_recreate();
      }

// Ouverture de la session si l'approbation n'est pas obligatoire
    if (MEMBER == 'false') {
       $OSCOM_Customer->setData($customer_id);
    }

// reset session token
      $_SESSION['sessiontoken'] = md5(osc_rand() . osc_rand() . osc_rand() . osc_rand());

// restore cart contents
      $_SESSION['cart']->restore_contents();

// build the message content

// email template
      require (DIR_WS_FUNCTIONS .'template_email.php');

      $name = $firstname . ' ' . $lastname;

      if (ACCOUNT_GENDER_PRO == 'true') {
        if ($gender == 'm') {
          $email_gender = sprintf(EMAIL_GREET_MR, $lastname);
        } else {
          $email_gender = sprintf(EMAIL_GREET_MS, $lastname);
        }
      } else {
        $email_gender = sprintf(EMAIL_GREET_NONE, $firstname);
      }

      if (COUPON_CUSTOMER_B2B != '') {
        $email_coupon = EMAIL_TEXT_COUPON . COUPON_CUSTOMER_B2B;
      }

// Envoi des e-mails selon si l'approbation est obligatoire
      if (MEMBER == 'false') {
        $template_email_welcome_catalog = osc_get_template_email_welcome_catalog($template_email_welcome_catalog);
      } else {
        $template_email_welcome_catalog = EMAIL_WELCOME;
      }

      if (COUPON_CUSTOMER != '') {
        $email_coupon_catalog = osc_get_template_email_coupon_catalog($template_email_coupon_catalog);
        $email_coupon = $email_coupon_catalog . COUPON_CUSTOMER;
      }

      $template_email_signature =osc_get_template_email_signature($template_email_signature);
      $template_email_footer = osc_get_template_email_text_footer($template_email_footer);
      $email_subject = utf8_encode(html_entity_decode(EMAIL_SUBJECT));
      
      $email_text = $email_gender .'<br /><br />'. $template_email_welcome_catalog .'<br /><br />'. $email_coupon .'<br /><br />' .   $template_email_signature . '<br /><br />' . $template_email_footer;

// Envoi du mail avec gestion des images pour Fckeditor et Imanager.
      $mimemessage = new email(array('X-Mailer: ClicShopping'));
      $message = html_entity_decode($email_text);
      $message = str_replace('src="/', 'src="' . HTTP_SERVER . '/', $message);
      $mimemessage->add_html_fckeditor($message);
      $mimemessage->build_message();
      $from = STORE_OWNER_EMAIL_ADDRESS;
      $mimemessage->send($name, $email_address, '', $from, $email_subject);

      if (EMAIL_INFORMA_ACCOUNT_ADMIN == 'true') {
// e-mail de notification a l'administrateur
        $admin_email_welcome = utf8_decode(html_entity_decode(ADMIN_EMAIL_WELCOME));
        $admin_email_text = utf8_decode(html_entity_decode(ADMIN_EMAIL_TEXT));
        $admin_email_text .= $admin_email_welcome . $admin_email_text . $email_warning;
        osc_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $admin_email_text, $name, $email_address, '');
      }

//***************************************
// odoo web service
//***************************************
      if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
        require ('ext/odoo_xmlrpc/xml_rpc_catalog_create_account_pro.php');
      }
//***************************************
// End odoo web service
//***************************************

      osc_redirect(osc_href_link('create_account_pro_success.php', '', 'SSL'));

    }// error false
  }

// Requetes SQL pour l'affichage par defaut du pays dans le menu deroulant
  $QcountryPro = $OSCOM_PDO->prepare("select countries_iso_code_2
                                       from :table_countries
                                       where countries_id = :countries_id
                                      ");
  $QcountryPro->bindInt(':countries_id',  (int)ACCOUNT_COUNTRY_PRO);
  $QcountryPro->execute();

  $country_pro = $QcountryPro->fetch();

  $default_country_pro = $country_pro['countries_iso_code_2'];

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('create_account_pro.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('create_account_pro'));
