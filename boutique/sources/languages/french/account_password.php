<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Changer le mot de passe');

define('HEADING_TITLE', 'Mon mot de passe');

define('MY_PASSWORD_TITLE', 'Mon mot de passe');

define('SUCCESS_PASSWORD_UPDATED', 'Votre mot de passe a été mise &agrave jour.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Votre mot de passe ne peut pas etre enregistré. Réessayez ou choissez un autre mot de passe.');
?>