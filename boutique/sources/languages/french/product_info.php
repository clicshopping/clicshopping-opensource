<?php
/**
 * pi_products_info.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license Licensed under GPL and MIT
 * @version $Id: 
*/

define('HEADING_TITLE','');
define('TABLE_HEADING_NEW_PRODUCTS','Nouveautés et collections');

define('PRODUCTS_NOT_SELL','Ce produit n\'est plus en vente');
define('TEXT_ALSO_PURCHASED_PRODUCTS', 'Nos clients ont acheté cet article puis ont craqué pour ceux-là');
define('TEXT_CLICK_TO_ENLARGE', 'Cliquer pour agrandir');
define('TEXT_CURRENT_REVIEWS', 'Commentaires :');
define('TEXT_DATE_ADDED', 'Ce produit a été ajouté à notre catalogue le %s.');
define('TEXT_DATE_AVAILABLE', '<p style="color:#ff0000;"><strong>Cet article sera disponible le %s.<br />Vous pouvez dès à présent le réserver</strong></p>');
define('TEXT_MORE_INFORMATION', 'Pour plus d\'informations, veuillez cliquer <a href="%s" target="_blank"><u>ici</u></a>.');

define('TEXT_PRODUCT_MODEL', 'Référence : ');
define('TEXT_PRODUCT_NOT_FOUND', 'Produit non trouvé !');
define('TEXT_PRODUCT_OPTIONS', 'Options disponibles : ');
define('BUTTON_IN_CART','Ajouter au panier');
?>
