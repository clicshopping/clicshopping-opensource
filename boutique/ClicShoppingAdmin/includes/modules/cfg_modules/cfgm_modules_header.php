<?php
  /**
   * cfm_modules_header.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_header {
    var $code = 'modules_header';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_HEADER_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_header() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_header/';
      $this->title = MODULE_CFG_MODULE_HEADER_TITLE;
    }
  }
?>
