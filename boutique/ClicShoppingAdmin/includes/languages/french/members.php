<?php
/*
 * members.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('TABLE_HEADING_LASTNAME', 'Nom');
define('TABLE_HEADING_FIRSTNAME', 'Prénom');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Compte créé');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_COMPANY', 'Compagnie');
define('BOX_CUSTOMERS_APPROVAL', 'Clients en attente');
define('HEADING_TITLE_SEARCH', 'Rechercher');
define('HEADING_TITLE', 'Clients en attente de validation');
define('MEMBER_DEACTIVATED', '<font color="#FF0000">Ce module est actuellement désactivé !</font><br /><br /> Vous pouvez activer ce module depuis le menu <strong>Configuration &raquo; Gestion B2B</strong> et approuver manuellement l\'inscription des nouveaux membres');

define('EMAIL_CONTACT', 'Pour plus d\'information sur notre service, vous pouvez nous contacter via ce mail : ' . STORE_OWNER_EMAIL_ADDRESS);

define('EMAIL_TEXT_SUBJECT', 'Attention Information Importante : Compte approuvé chez '.STORE_NAME.'');
define('EMAIL_TEXT_CONFIRM', '<div align="justify">Bonjour, <br />Vous allez recevoir tr&eagrave;s prochainement votre mot de passe pour accéder  à '.STORE_NAME. "\n\n" . '<u><font size="2">Avis de confidentialité :</font></u><font size="2">'. "\n" . 'Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans �tre le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re�u ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n" . '<p><font size="2">Conformément à la <strong>Loi</strong> dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</font></div>' . "\n\n");

define('EMAIL_WARNING', '' . "\n\n" . '<strong>Note:</strong> Si votre adresse mail nous a été donnée par un de nos clients et que vous ne souhaitez pas �tre informé, vous pouvez résilier votre compte en nous envoyant un mail à l\'adresse suivante : ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");
define('EMAIL_SEPARATOR', '----------------------------------------------');



define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Rappel d\'accès à votre compte -');
define('EMAIL_PASSWORD_REMINDER_BODY', '<div align="justify">Bonjour ! '. "\n\n" . 'Vous nous avez demandé d\'ouvrir un compte professionnel sur '.STORE_NAME.' et nous vous remercions pour votre confiance. '. "\n" . 'Nous vous confirmons que nous avons validé votre demande et que vous pouvez dès a présent vous connecter sur '.STORE_NAME.'. '. "\n\n" . 'Votre nom d\'utilisateur est :  %s  '. "\n\n" . 'Votre mot de passe est : %s '. "\n\n" . 'Nous sommes ravis de vous compter parmi nos nouveaux clients et nous restons à votre service pour vous faciliter vos achats !'. "\n\n" . '<strong>Vos liens utiles :</strong>'. "\n" . '
1) Pour accéder à votre compte client, modifier vos coordonnées, vos adresses de livraison et/ou facturation, <a href="'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account.php">'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account.php</a>'. "\n" . '2) Pour accéder directement au suivi de vos commandes, <a href="'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account_history.php">'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'account_history.php</a>'. "\n" . '3) Retrouver son mot de passe oublié : <a href="'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'password_forgotten.php">'.HTTP_CATALOG_SERVER. DIR_WS_CATALOG .'password_forgotten.php</a><br />'. "\n\n" . 'Pour tous les accès précités, vous devrez renseigner au préalable vos identifiants. '. "\n\n" . 'Très cordialement,'. "\n" . '<strong>L\'équipe '.STORE_NAME.'</strong>'. "\n" . '<p>'.STORE_NAME_ADDRESS.' '. "\n\n" . '<u><font size="2">Avis de confidentialité :</font></u><font size="2">'. "\n" . 'Ce message ainsi que les documents qui seraient joints en annexe sont adressés exclusivement à leur destinataire et pourraient contenir une information confidentielle soumise au secret professionnel ou dont la divulgation est interdite en vertu de la législation en vigueur. De ce fait, nous avertissons la personne qui le recevrait sans �tre le destinataire ou une personne autorisée, que cette information est confidentielle et que toute utilisation, copie, archive ou divulgation en est interdite. Si vous avez re�u ce message, nous vous prions de bien vouloir nous le communiquer par courriel : ' . STORE_OWNER_EMAIL_ADDRESS . '. et de procéder directement à sa destruction.'. "\n" . '
<p><font size="2">Conformément à la <strong>Loi</strong> dans le pays de résidence de la société exploitant la boutique '. STORE_NAME.', vous avez droit à la rectification de vos données personnelles à tout moment ou sur simple demande par email</font>
</div>');

define('EMAIL_TEXT_COUPON', STORE_NAME . ' se fait un plaisir de vous offrir un coupon remise sur votre prochaine commande que vous pourrez utiliser n\importe quand sur la boutique (sauf limite de validité).' . "\n\n" . ' Le numéro du coupon est : <strong>');

define('IMAGE_ACTIVATE','Activer le compte');
define('ICON_EDIT_CUSTOMER', 'Editer la fiche client');
?>