<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Login');
define('HEADING_TITLE', 'Open an account');
define('TEXT_NEW_CUSTOMER_PRIVACY','<strong>Data secure</strong><br /><br />' . STORE_NAME . ' agrees to secure all your personal information and keeping strictly confidential. We do not sell email addresses to companies email marketing.');

define('HEADING_NEW_CUSTOMER', 'New Customer');
define('TEXT_NEW_CUSTOMER', 'I am a new customer.');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'Choose an option below. Creating an account on ' . STORE_NAME . ' you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.');

define('TEXT_LOGIN_ERROR', ' Error: No match for E-Mail Address and/or Password.');
define('TEXT_VISITORS_CART', ' <strong>Note:</strong></font> Your &quot;Visitors Cart&quot; contents will be merged with your &quot;Members Cart&quot; contents once you have logged on. <a href="javascript:session_win();">[More Info]</a>');
?>
