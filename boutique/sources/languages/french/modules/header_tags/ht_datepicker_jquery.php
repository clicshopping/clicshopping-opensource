<?php
/**
 * ht_date_picker_jquery
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_TITLE', 'Souhaitez-vous utiliser le syst&egrave;me de date internationale ?');
  define('MODULE_HEADER_TAGS_DATEPICKER_JQUERY_DESCRIPTION', 'Ajouter les balises pour afficher les dates selon les pays');
?>
