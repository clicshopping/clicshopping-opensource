<?php
/*
* edit_template_products.php
* @copyright Copyright 2008 - http://www.e-imaginis.com
* @copyright Portions Copyright 2003 osCommerce
* @license GNU Public License V2.0
* @version $Id:
*/

define('HEADING_TITLE','Template products Editor');
define('TITLE_HELP_EDIT_TEMPLATE_PRODUCTS_IMAGE','Help on template products editor');
define('TITLE_HELP_EDIT_TEMPLATE_PRODUCTS','Help on Template products editor');
define('TEXT_HELP_EDIT_TEMPLATE_PRODUCTS','
the directories with a template :<br /><br />
- modules_frontpage<br />
- modules_categories_Index<br />
- modules_products_listing<br />
- modules_products_info<br />
- modules_products_heart<br />
- modules_products_new<br />
- modules_specials<br /><br />
For security reasons, permissions are not allowed in the original Template product editor. It is important to understand to change file permissions, can cause security problem in your application. <br />
It is better to download yours files via FTP and to change them on your computer. <br />
In case you want to make modification via Template products editor, you must follow this procedure. <br />
- Rights directories: chmod 755 <br />
- Rights Files: chmod 666 <br />
After the modifications, change your rights files in : chmod 444 <br />
- Please note,  the configuration of your server will impact on yours authorisations to modify the files or not. We encourage you to visit <a href="http://clicshopping.org/marketplace/blog.php" target="_blank"> our guide </a>
for further explanation.');

?>