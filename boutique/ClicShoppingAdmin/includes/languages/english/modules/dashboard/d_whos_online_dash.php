<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_TITLE', 'Do you want install Whos Online Dashboard');
define('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_DESCRIPTION', 'Show who\'s online on the dashboard.');

define('TABLE_HEADING_ONLINE', 'Online');
define('TABLE_HEADING_FULL_NAME', 'Name');
define('TABLE_HEADING_IP_ADDRESS', 'IP Address');
define('TABLE_HEADING_USER_AGENT', 'Agent');

define('TEXT_NONE_', 'None');
define('TEXT_NUMBER_OF_CUSTOMERS', 'Currently there are <strong>%s</strong> customers online.');
?>
