<?php
/**
 * cfm_modules_checkout_confirmation.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('MODULE_CFG_MODULE_CHECKOUT_CONFIRMATION_TITLE', 'Order confirmation');
?>
