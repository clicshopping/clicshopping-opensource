<?php
/**
 * d_total_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_TITLE', 'Total des clients inscrits pendant les 30 derniers jours <br /><br />');
define('MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_DESCRIPTION', 'Affiche les inscriptions client des 30 derniers jours');
define('MODULE_ADMIN_DASHBOARD_TOTAL_CUSTOMERS_CHART_LINK', 'Nbr de clients inscrit');

