<?php
/**
 * configuration.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'save':
        $configuration_value = osc_db_prepare_input($_POST['configuration_value']);
        $cID = osc_db_prepare_input($_GET['cID']);

        $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                       set configuration_value = :configuration_value,
                                       last_modified = now()
                                       where configuration_id = :configuration_id
                                      ');
        $Qupdate->bindValue(':configuration_value', $configuration_value );
        $Qupdate->bindInt(':configuration_id', (int)$cID );
        $Qupdate->execute();

//clear cache
        osc_cache_clear('configuration.cache');

        osc_redirect(osc_href_link('configuration.php', 'gID=' . $_GET['gID'] . '&cID=' . $cID));
        break;
    }
  }

  $gID = (isset($_GET['gID'])) ? $_GET['gID'] : 1;

  $QcfgGroup = $OSCOM_PDO->prepare("select configuration_group_title 
                                    from :table_configuration_group
                                    where configuration_group_id = :configuration_group_id
                                  ");
  $QcfgGroup->bindInt(':configuration_group_id', (int)$gID );
  $QcfgGroup->execute();
  $cfg_group = $QcfgGroup->fetch();

  require('includes/header.php');
?>
<!-- body //-->
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/configuration_' . $gID . '.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-10 pageHeading"><?php echo '&nbsp;' . $cfg_group['configuration_group_title']; ?></span>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>


<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $Qconfiguration = $OSCOM_PDO->prepare('select configuration_id,
                                                configuration_title,
                                                configuration_value,
                                                use_function
                                       from :table_configuration
                                       where configuration_group_id = :configuration_group_id
                                       order by sort_order
                                       ');
  $Qconfiguration->bindInt(':configuration_group_id', (int)$gID);
  $Qconfiguration->execute();

  while ($configuration = $Qconfiguration->fetch() ) {
    $cfgValue = $configuration['configuration_value'];
?>
              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
                <td class="dataTableContent"><?php echo $configuration['configuration_title']; ?></td>
                <td class="dataTableContent"><?php echo htmlspecialchars($cfgValue); ?></td>
                <td class="dataTableContent" align="right">
                  <a data-toggle="modal" data-refresh="true"  href="configuration_popup.php?<?php echo 'gID='.$_GET['gID'] . '&cID=' . $configuration['configuration_id']; ?>" data-target="#myModal_<?php echo $configuration['configuration_id']; ?>"><?php echo osc_image (DIR_WS_ICONS . 'edit.gif', ICON_EDIT) ?></a>
                  <div class="modal fade" id="myModal_<?php echo $configuration['configuration_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content"  style="padding:10px 10px 10px 10px;">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel"><?php echo $cInfo->configuration_title; ?></h4>
                        </div>
                        <div class="modal-body"><div class="te"></div></div>
                      </div> <!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div><!-- /.modal -->
                </td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

