<?php
/**
 * orders.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Commandes');
define('HEADING_TITLE_SEARCH', 'No de commande');
define('HEADING_TITLE_STATUS', 'Statut :');

define('TITLE_ORDERS_ADRESSE', 'Adresses');
define('TITLE_ORDERS_CUSTOMERS', 'Informations sur le client');
define('TITLE_ORDERS_PAIEMENT', 'Paiement');
define('TITLE_ORDERS_HISTORY', 'Historique');

define('TABLE_HEADING_COMMENTS', 'Commentaires');
define('TABLE_HEADING_CUSTOMERS', 'Clients');
define('TABLE_HEADING_ORDER_TOTAL', 'Montant Total');
define('TABLE_HEADING_DATE_PURCHASED', 'Date d\'achat');
define('TABLE_HEADING_STATUS', 'Statut');
define('TABLE_HEADING_ACTION', 'Actions');
define('TABLE_HEADING_QUANTITY', 'Qté.');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Référence');
define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_REALISED_BY','Modifié par');
define('TABLE_HEADING_TAX', 'Taxe');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Prix (ht)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Prix (ttc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ht)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (ttc)');
define('TABLE_HEADING_COLOR_GROUP','Groupe B2B');
define('TABLE_HEADING_ORDERS','No Commande');
define('TABLE_HEADING_ODOO', 'Odoo');
define('TABLE_HEADING_CUSTOMER_NOTIFIED', 'Client notifié');
define('TABLE_HEADING_DATE_ADDED', 'Date d\'ajout');

define('ENTRY_CUSTOMER', 'Client :');
define('ENTRY_SOLD_TO', 'VENDU A :');
define('ENTRY_DELIVERY_TO', 'Livraison à :');
define('ENTRY_SHIP_TO', 'Envoyé à; :');
define('ENTRY_SHIPPING_ADDRESS', 'Adresse d\'expédition :');
define('ENTRY_BILLING_ADDRESS', 'Adresse de facturation :');
define('ENTRY_PAYMENT_METHOD', 'Méthode de paiement :');
define('ENTRY_CREDIT_CARD_TYPE', 'Type de carte de crédit :');
define('ENTRY_CREDIT_CARD_OWNER', 'Propriétaire de la carte de crédit :');
define('ENTRY_CREDIT_CARD_NUMBER', 'Numéro de la carte de crédit :');
define('ENTRY_CREDIT_CARD_EXPIRES', 'Date d\'expiration de la carte de crédit :');
define('ENTRY_SUB_TOTAL', 'Sous-Total :');
define('ENTRY_TAX', 'Taxe :');
define('ENTRY_SHIPPING', 'Expédition :');
define('ENTRY_TOTAL', 'Total :');
define('ENTRY_DATE_PURCHASED', 'Date d\'achat :');
define('ENTRY_STATUS', 'Statut :');
define('ENTRY_ATOS_BOARD_INFORMATIONS','Informations transaction');
define('ENTRY_STATUS_ORDERS_TRACKING_NAME','Nom de société d\'expédition : ');
define('ENTRY_STATUS_ORDERS_TRACKING_NUMBER','Numéro de tracking  : ');

define('ENTRY_STATUS_INVOICE', 'Edition PDF à générer : ');
define('ENTRY_STATUS_COMMENT_INVOICE','Edition PDF générée : ');
define('ENTRY_STATUS_INVOICE_REALISED', 'Traité par : ');
define('ENTRY_STATUS_INVOICE_NOTE','<u>Informations complémentaires</u> :<br /> ');

define('ENTRY_CLIENT_COMPUTER_IP','Adresse IP du client : ');
define('ENTRY_PROVIDER_NAME_CLIENT','Provider du client : ');

define('TEXT_CONDITION_GENERAL_OF_SALES','Contrat général de vente concernant cette commande');

define('ENTRY_DATE_LAST_UPDATED', 'Dernière date de mise à jour :');
define('ENTRY_NOTIFY_CUSTOMER', 'Informer client :');
define('ENTRY_NOTIFY_COMMENTS', 'Ajouter un commentaire :');
define('ENTRY_PRINTABLE', 'Imprimer la facture');

define('TEXT_INFO_HEADING_DELETE_ORDER', 'Supprimer la commande');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer cette commande ?');
define('TEXT_INFO_RESTOCK_PRODUCT_QUANTITY', 'Restaurer la valeur de stock');
define('TEXT_DATE_ORDER_CREATED', 'Date de création :');
define('TEXT_DATE_ORDER_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_INFO_PAYMENT_METHOD', 'Méthode de paiement :');
define('ENTRY_CUSTOMER_LOCATION','Localiser');
define('TEXT_ALL_ORDERS', 'Toutes les commandes');
define('TEXT_NO_ORDER_HISTORY', 'Aucun historique de commande disponible');

define('TEXT_INFO_HEADING_ARCHIVE','Archiver une commande');
define('TEXT_INFO_ARCHIVE_INTRO','Souhaitez-vous archiver cette commande ?');

define('EMAIL_SEPARATOR', '------------------------------------------------------');

define('EMAIL_TEXT_SUBJECT', 'Actualisation de votre commande sur '. STORE_NAME);
define('EMAIL_INTRO_COMMAND','Bonjour,' . "\n\n" . 'Une actualisation de votre commande a été effectuée. Veuillez trouver ci-dessous l\'ensemble des informations.'. "\n\n" . ''.STORE_NAME.' '. "\n" . ' '.STORE_NAME_ADDRESS.'');

define('EMAIL_TEXT_ORDER_NUMBER', 'No de commande :');
define('EMAIL_TEXT_INVOICE_URL', 'Votre bon de commande ou facture sont disponibles dans votre espace d\'administration de la '.STORE_NAME.' :');
define('EMAIL_TEXT_DATE_ORDERED', ''. "\n\n" . 'Date de commande :');

define('EMAIL_TEXT_NEW_ORDER_STATUS','Nouveau statut : %s');

define('EMAIL_TEXT_COMMENTS_UPDATE', 'Les commentaires de votre commande sont :<br /><br />' . "\n\n%s\n\n");

define('ERROR_ORDER_DOES_NOT_EXIST', 'Erreur : La commande n\'existe pas.');
define('SUCCESS_ORDER_UPDATED', 'Succès : La commande est mise à jour avec succès.');
define('WARNING_ORDER_NOT_UPDATED', 'Attention : Aucune modification n\'a été effectuée. La commande n\'a pas été mis à jour.');

define('ENTRY_ORDER_SIRET', 'N&deg; d\'immatriculation <i>(ex : No RCS)</i> : ');
define('ENTRY_TVA_INTRACOM', 'N&deg; de TVA intracom : ');
define('ENTRY_ORDER_CODE_APE', 'N&deg; de nomenclature <i>(ex : Code APE)</i> : ');

define('ICON_EDIT_CUSTOMER', 'Editer la fiche client');
define('ICON_EDIT_ORDER', 'Editer la commande');

define('TEXT_ODOO_INVOICE', '<p style="color:red;">Commande enregistrée dans Odoo</p>');
define('TEXT_ODOO_INVOICE_REGISTRED', '<p style="color:red;">Facture enregistrée dans Odoo</p>');
define('TEXT_ODOO_INVOICE_CANCELLED', '<p style="color:red;">Facture annulée dans Odoo</p>');
define('TEXT_ODOO_INVOICE_CONFIRM', '<p style="color:red;">Facture comptabilisée dans Odoo</p>');
?>