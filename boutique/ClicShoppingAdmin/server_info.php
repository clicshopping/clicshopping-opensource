<?php
/**
 * server_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch ($action) {
    case 'export':
      $info = osc_get_system_information();
    break;

    case 'submit':
      $target_host = '';
      $target_path = '';

      $encoded = base64_encode(serialize(osc_get_system_information()));

      $response = false;

      if (function_exists('curl_init')) {
        $data = array('info' => $encoded);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://' . $target_host . $target_path);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = trim(curl_exec($ch));
        curl_close($ch);
      } else {
        if ($fp = @fsockopen($target_host, 80, $errno, $errstr, 30)) {
          $data = 'info=' . $encoded;

          fputs($fp, "POST " . $target_path . " HTTP/1.1\r\n");
          fputs($fp, "Host: " . $target_host . "\r\n");
          fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
          fputs($fp, "Content-length: " . strlen($data) . "\r\n");
          fputs($fp, "Connection: close\r\n\r\n");
          fputs($fp, $data."\r\n\r\n");

          $response = '';

          while (!feof($fp)) {
            $response .= fgets($fp, 4096);
          }

          fclose($fp);

          $response = trim(substr($response, strrpos($response, "\r\n\r\n")));
        }
      }

      if ($response != 'OK') {
        $OSCOM_MessageStack->add_session(ERROR_INFO_SUBMIT, 'error');
      } else {
        $OSCOM_MessageStack->add_session(SUCCESS_INFO_SUBMIT, 'success');
      }

      osc_redirect(osc_href_link('server_info.php'));
    break;

    case 'save':
      $info = osc_get_system_information();
      $info_file = 'server_info-' . date('YmdHis') . '.txt';
      header('Content-type: text/plain');
      header('Content-disposition: attachment; filename=' . $info_file);
      echo osc_format_system_info_array($info);
      exit;

    break;

    default:
      $info = osc_get_system_information();
      break;
  }

  require('includes/header.php');
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/server_info.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if ($action == 'export') {
?>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText" colspan="2"><?php echo TEXT_EXPORT_INTRO; ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_textarea_field('server configuration', 'soft', '100', '15', osc_format_system_info_array($info)); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
          <td align="right" class="main"><?php // echo '<a href="' . osc_href_link('server_info.php', 'action=submit') . '">' . osc_image_button('button_send.gif', IMAGE_SEND) . '</a>&nbsp;' . '<a href="' . osc_href_link('server_info.php', 'action=save') . '">' . osc_image_button('button_save.gif', IMAGE_SAVE) . '</a>';?>
      </tr>
  <?php
  } else {
    $server = parse_url(HTTP_SERVER);
?>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td>
          <div  style="padding-left: 10px";>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_INFO_SERVEUR; ?></a></li>
              <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_INFO_PHP; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
                <!-- -------------------------------------- //-->
                <!--         ONGLET GENERAL SERVEUR         //-->
                <!-- -------------------------------------- //-->
                <div class="tab-pane active" id="tab1">
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo SERVER_INFO; ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                        <tr>
                          <td><table border="0" cellspacing="0" cellpadding="3">
                            <tr>
                              <td class="smallText"><strong><?php echo TITLE_SERVER_HOST; ?></strong></td>
                              <td class="smallText"><?php echo $server['host'] . ' (' . gethostbyname($server['host']) . ')'; ?></td>
                              <td class="smallText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo TITLE_DATABASE_HOST; ?></strong></td>
                              <td class="smallText"><?php echo DB_SERVER . ' (' . gethostbyname(DB_SERVER) . ')'; ?></td>
                            </tr>
                            <tr>
                              <td class="smallText"><strong><?php echo TITLE_SERVER_OS; ?></strong></td>
                              <td class="smallText"><?php echo $info['system']['os'] . ' ' . $info['system']['kernel']; ?></td>
                              <td class="smallText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo TITLE_DATABASE; ?></strong></td>
                              <td class="smallText"><?php echo 'MySQL ' . $info['mysql']['version']; ?></td>
                            </tr>
                            <tr>
                              <td class="smallText"><strong><?php echo TITLE_SERVER_DATE; ?></strong></td>
                              <td class="smallText"><?php echo $info['system']['date']; ?></td>
                              <td class="smallText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo TITLE_DATABASE_DATE; ?></strong></td>
                              <td class="smallText"><?php echo $info['mysql']['date']; ?></td>
                            </tr>
                            <tr>
                              <td class="smallText"><strong><?php echo TITLE_SERVER_UP_TIME; ?></strong></td>
                              <td colspan="3" class="smallText"><?php echo $info['system']['uptime']; ?></td>
                            </tr>
                            <tr>
                              <td colspan="4"><?php echo osc_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
                            </tr>
                            <tr>
                              <td class="smallText"><strong><?php echo TITLE_HTTP_SERVER; ?></strong></td>
                              <td colspan="3" class="smallText"><?php echo $info['system']['http_server']; ?></td>
                            </tr>
                            <tr>
                              <td class="smallText"><strong><?php echo TITLE_PHP_VERSION; ?></strong></td>
                              <td colspan="3" class="smallText"><?php echo $info['php']['version'] . ' (' . TITLE_ZEND_VERSION . ' ' . $info['php']['zend'] . ')'; ?></td>
                            </tr>
                          </table></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>
              </div>
<!-- -------------------------------------- //-->
<!--            ONGLET Infos PHP            //-->
<!-- -------------------------------------- //-->
                <div class="tab-pane" id="tab2">
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo SERVER_INFO_PHP; ?></td>
                    </tr>
                  </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                      <tr>
                        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr>
                            <td>
<?php
  if (function_exists('ob_start')) {
?>
<style type="text/css">
.p {text-align: left;}
.e {background-color: #ccccff; font-weight: bold;}
.h {background-color: #9999cc; font-weight: bold;}
.v {background-color: #cccccc;}
i {color: #666666;}
hr {display: none;}
</style>
<?php
    ob_start();
    phpinfo();
    $phpinfo = ob_get_contents();
    ob_end_clean();

    $phpinfo = str_replace('border: 1px', '', $phpinfo);
    preg_match('/<body>(.*)<\/body>/is', $phpinfo, $regs);
    echo $regs[1];
  } else {
    phpinfo();
  }
?>
                          </td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
<?php
  }
 ?>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
