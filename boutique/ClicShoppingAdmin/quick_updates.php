<?php
/*
 * quick_quantity_update.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

  require('includes/application_top.php');

// Register global off
   if (isset($_GET['row_by_page'])) {
     $row_by_page = (int)$_GET['row_by_page'];
   }

   if (isset($_GET['manufacturer'])) {
     $manufacturer = (int)$_GET['manufacturer'];
   }

   if (isset($_GET['supplier'])) {
     $supplier = (int)$_GET['supplier'];
   }

   if (isset($_GET['sort_by'])) {
     $sort_by = $_GET['sort_by'];
   }
 
   if (isset($_GET['search'])) {
     $search = $_GET['search'];
   }
 
 
    if (isset($_GET['page'])) {
     $page = $_GET['page'];
   }
    if (isset($_GET['customers_group_id'])) {
     $customers_group_id = (int)$_GET['customers_group_id'];
   }
 
// spec_price
    if (isset($_POST['spec_price'])) {
     $spec_price = $_POST['spec_price'];
   }

  ($row_by_page) ? define('MAX_DISPLAY_ROW_BY_PAGE' , $row_by_page ) : $row_by_page = MAX_DISPLAY_SEARCH_RESULTS_ADMIN;
  define('MAX_DISPLAY_ROW_BY_PAGE' , MAX_DISPLAY_SEARCH_RESULTS_ADMIN);

// Tax Row
  $tax_class_array = array(array('id' => '0', 'text' => NO_TAX_TEXT));

  $QtaxClass = $OSCOM_PDO->prepare('select tax_class_id,
                                          tax_class_title
                                   from :table_tax_class
                                   order by tax_class_title
                                ');
  $QtaxClass->execute();

  while ($tax_class = $QtaxClass->fetch() ) {
    $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                               'text' => $tax_class['tax_class_title']);
  }

// Info Row pour le champ fabricant
  $manufacturers_array = array(array('id' => '0',
                                     'text' => NO_MANUFACTURER));

  $Qmanufacturers = $OSCOM_PDO->prepare('select manufacturers_id,
                                                manufacturers_name
                                        from :table_manufacturers
                                        order by manufacturers_name
                                        ');
  $Qmanufacturers->execute();

  while ($manufacturers = $Qmanufacturers->fetch() ) {
    $manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'],
                                   'text' => $manufacturers['manufacturers_name']);
  }

// Info Row pour le champ fournisseur
  $suppliers_array = array(array('id' => '0',
                                 'text' => NO_SUPPLIER));

  $Qsuppliers = $OSCOM_PDO->prepare('select suppliers_id,
                                            suppliers_name
                                     from :table_suppliers
                                     order by suppliers_name
                                    ');
  $Qsuppliers->execute();

   while ($suppliers = $Qsuppliers->fetch() ) {
    $suppliers_array[] = array('id' => $suppliers['suppliers_id'],
                               'text' => $suppliers['suppliers_name']);
  }


##// Update database
  switch ($_GET['action']) {
    case 'update' :

      $count_update=0;
      $item_updated = array();

      if ($_POST['product_new_model']) {
        foreach($_POST['product_new_model'] as $id => $new_model) {
          if (trim($_POST['product_new_model'][$id]) != trim($_POST['product_old_model'][$id])) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_model = :products_model
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_model', $new_model);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_name']) {
        foreach($_POST['product_new_name'] as $id => $new_name) {
          if (trim($_POST['product_new_name'][$id]) != trim($_POST['product_old_name'][$id])) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products_description
                                            set products_name= :products_name
                                            where products_id = :products_id
                                            and language_id= :language_id
                                          ');
            $Qupdate->bindValue(':products_name', $new_name);
            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindInt(':language_id', $_SESSION['languages_id']);
            $Qupdate->execute();

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_last_modified = now()
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_price']) {
        foreach($_POST['product_new_price'] as $id => $new_price) {

          if ($_POST['product_new_price'][$id] != $_POST['product_old_price'][$id] && $_POST['update_price'][$id] == 'yes') {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_price = :products_price
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_price', $new_price);
            $Qupdate->execute();

// B2B
            $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                                   customers_group_name,
                                                                   customers_group_discount
                                                     from :table_customers_groups
                                                     where customers_group_id >  0
                                                     order by customers_group_id
                                                    ');
            $QcustomersGroup->execute();

           while ($customers_group =$QcustomersGroup->fetch() ) {
             if ($QcustomersGroup->rowCount() > 0) {

               $Qattributes = $OSCOM_PDO->prepare('select customers_group_id,
                                                          customers_group_price,
                                                          products_price
                                                   from :table_products_groups
                                                   where products_id = :products_id
                                                   and customers_group_id = :customers_group_id
                                                   order by customers_group_id
                                                ');

               $Qattributes->bindInt(':products_id', (int)$id );
               $Qattributes->bindInt(':customers_group_id',  (int)$customers_group['customers_group_id'] );
               $Qattributes->execute();

               $attributes = $Qattributes->fetch();

               $Qdiscount = $OSCOM_PDO->prepare('select discount
                                                from :table_groups_to_categories
                                                where customers_group_id = :customers_group_id
                                                and categories_id = :categories_id
                                                ');

               $Qdiscount->bindInt(':categories_id', (int)$current_category_id );
               $Qdiscount->bindInt(':customers_group_id',  (int)$customers_group['customers_group_id'] );
               $Qdiscount->execute();

               $discount = $Qdiscount->fetch();

                if (is_null($discount['discount'])) {
                  $ricarico = $customers_group['customers_group_discount'];
                } else {
                  $ricarico = $discount['discount'];
                }
             }
             $pricek = $new_price ;
//$ricarico = $customers_group['customers_group_discount'];

           if ($pricek > 0) {
             if (B2B == 'true') {
               if ($ricarico > 0) $newprice = $pricek+($pricek/100)*$ricarico;
               if ($ricarico == 0) $newprice = $pricek;
             }
             if (B2B == 'false') {
               if ($ricarico > 0) $newprice = $pricek-($pricek/100)*$ricarico;
               if ($ricarico == 0) $newprice = $pricek;
             }
           } else {
             $newprice = 0;
           }

           if ($attributes['customers_group_id'] == NULL ) {
             osc_db_query("insert into products_groups (customers_group_id,
                                                       customers_group_price,
                                                       products_id,
                                                       products_price)
                         values (". $customers_group['customers_group_id'] . ", 
                                 " . $newprice . ", 
                                 " . $id . ", 
                                 " . $pricek . ")
                         ");
           } else {
             $Qupdate = $OSCOM_PDO->prepare('update :table_products_groups
                                            set customers_group_price = :customers_group_price
                                            where products_id = :products_id
                                            and customers_group_id= :customers_group_id
                                          ');

             $Qupdate->bindInt(':products_id', (int)$id);
             $Qupdate->bindValue(':customers_group_price', $new_price);
             $Qupdate->bindInt(':customers_group_id', (int)$attributes['customers_group_id']);
             $Qupdate->execute();
          }

             $count_update++;
             $item_updated[$id] = 'updated';
          }

         }
//***************************************
// odoo web service
//***************************************
            if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
              require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
            }
//***************************************
// End odoo web service
//***************************************
       }
     }

      if ($_POST['product_new_weight']) {
        foreach($_POST['product_new_weight'] as $id => $new_weight) {
          if ($_POST['product_new_weight'][$id] != $_POST['product_old_weight'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_weight = :products_weight
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_weight', $new_weight);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_quantity']) {
        foreach($_POST['product_new_quantity'] as $id => $new_quantity) {
          if ($_POST['product_new_quantity'][$id] != $_POST['product_old_quantity'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_quantity = :products_quantity
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_quantity', $new_quantity);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

// min order quantity
      if ($_POST['new_products_min_qty_order']) {
        foreach($_POST['new_products_min_qty_order'] as $id => $new_products_min_qty_order) {
          if ($_POST['new_products_min_qty_order'][$id] != $_POST['products_old_min_qty_order'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_min_qty_order = :products_min_qty_order
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_min_qty_order', $new_products_min_qty_order);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_manufacturer']) {
        foreach($_POST['product_new_manufacturer'] as $id => $new_manufacturer) {
          if ($_POST['product_new_manufacturer'][$id] != $_POST['product_old_manufacturer'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set manufacturers_id = :manufacturers_id
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindInt(':manufacturers_id', $new_manufacturer);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_supplier']) {
        foreach($_POST['product_new_supplier'] as $id => $new_supplier) {
          if ($_POST['product_new_supplier'][$id] != $_POST['product_old_supplier'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set suppliers_id = :suppliers_id
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindInt(':suppliers_id', $new_supplier);
            $Qupdate->execute();

          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_image']) {
        foreach($_POST['product_new_image'] as $id => $new_image) {
          if (trim($_POST['product_new_image'][$id]) != trim($_POST['product_old_image'][$id])) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_image = :products_image
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_image', $new_image);
            $Qupdate->execute();
          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      if ($_POST['product_new_status']) {
        foreach($_POST['product_new_status'] as $id => $new_status) {
          if ($_POST['product_new_status'][$id] != $_POST['product_old_status'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';
            osc_set_product_status($id, $new_status);

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                              set products_last_modified = now()
                                              where products_id = :products_id
                                            ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->execute();
          }
        }
//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
        }
//***************************************
// End odoo web service
//***************************************
      }

      if ($_POST['product_new_products_price_comparison']) {
        foreach($_POST['product_new_products_price_comparison'] as $id => $new_products_price_comparison) {
          if ($_POST['product_new_products_price_comparison'][$id] != $_POST['product_old_products_price_comparison'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';
            osc_set_product_products_price_comparison($id, $new_products_price_comparison);

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                              set products_last_modified = now()
                                              where products_id = :products_id
                                            ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->execute();
          }
        }
//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
        }
//***************************************
// End odoo web service
//***************************************
      }


      if ($_POST['product_new_products_only_online']) {
        foreach($_POST['product_new_products_only_online'] as $id =>$new_products_only_online) {
          if ($_POST['product_new_products_only_online'][$id] != $_POST['product_old_products_only_online'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';
            osc_set_product_products_only_online($id, $new_products_only_online);

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                              set products_last_modified = now()
                                              where products_id = :products_id
                                            ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->execute();

          }
        }
//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
        }
//***************************************
// End odoo web service
//***************************************
      }

      if ($_POST['product_new_tax']) {
        foreach($_POST['product_new_tax'] as $id => $new_tax_id) {
          if ($_POST['product_new_tax'][$id] != $_POST['product_old_tax'][$id]) {
            $count_update++;
            $item_updated[$id] = 'updated';

            $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                            set products_tax_class_id = :products_tax_class_id
                                            where products_id = :products_id
                                          ');

            $Qupdate->bindInt(':products_id', (int)$id);
            $Qupdate->bindValue(':products_tax_class_id', $new_tax_id);
            $Qupdate->execute();

          }
//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_admin_quick_update.php');
          }
//***************************************
// End odoo web service
//***************************************
        }
      }

      $count_item = array_count_values($item_updated);
      if ($count_item['updated'] > 0) $OSCOM_MessageStack->add($count_item['updated'].' '.TEXT_PRODUCTS_UPDATED . " $count_update " . TEXT_QTY_UPDATED, 'success');

// seo update
      if (SEO_URLS_ENABLED == 'true') {
         osc_reset_cache_data_usu5('reset');
      }
    break;

    case 'calcul':
      if ($_POST['spec_price']) $preview_global_price = 'true';
    break;
  }

// explode string parameters from preview product
  if ($info_back && $info_back!="-") {
    $infoback = explode('-',$info_back);
    $sort_by = $infoback[0];
    $page =  $infoback[1];
    $current_category_id = $infoback[2];
    $row_by_page = $infoback[3];
    $manufacturer = $infoback[4];
    $supplier = $infoback[5];
  }

// define the step for rollover lines per page
  $row_bypage_array = array(array());
  for ($i = 10; $i <=300 ; $i=$i+10) {
    $row_bypage_array[] = array('id' => $i,
                                'text' => $i);
  }

// Let's start displaying page with forms .$products['products_id'].

  require('includes/header.php');
?>
<!-- Debut surlignage //-->
<script language="javascript">
<!--
var browser_family;
var up = 1;

if (document.all && !document.getElementById)
  browser_family = "dom2";
else if (document.layers)
  browser_family = "ns4";
else if (document.getElementById)
  browser_family = "dom2";
else
  browser_family = "other";

function display_ttc(action, prix, taxe, up){ <!-- Script pour afficher le prix de vente TTC  //-->
  if(action == 'display'){
    if(up != 1)
      valeur = Math.round((prix + (taxe / 100) * prix) * 100) / 100;
  }else{
    if(action == 'keyup'){
      valeur = Math.round((parseFloat(prix) + (taxe / 100) * parseFloat(prix)) * 100) / 100;
    }else{
      valeur = '0';
    }
  }
  switch (browser_family){
    case 'dom2':
      document.getElementById('descDiv').innerHTML = '<font color="#ff0000"><?php echo TOTAL_COST; ?> :</font><font color="#0000ff">  '+valeur+'&nbsp;&euro;</font>';
    break;
    case 'ie4':
      document.all.descDiv.innerHTML = '<font color="#ff0000"><?php echo TOTAL_COST; ?> :</font><font color="#0000ff">  '+valeur+'&nbsp;&euro;</font>';
    break;
    case 'ns4':
      document.descDiv.document.descDiv_sub.document.write(valeur);
      document.descDiv.document.descDiv_sub.document.close();
    break;
    case 'other':
    break;
  }
}

-->
</script>
<!-- Fin surlignage //-->
<!-- body //-->
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/priceupdate.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="bottom"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
<?php
// ----------------------------
// --------- Suppliers
// ----------------------------
  if (DISPLAY_MAJR_SUPPLIER == 'true') {

    echo osc_draw_form('suppliers', 'quick_updates.php', '', 'get');
    echo osc_draw_hidden_field( 'row_by_page', $row_by_page); 
    echo osc_draw_hidden_field( 'cPath', $current_category_id);
    echo osc_draw_hidden_field( 'manufacturer', $manufacturer); 
?>
                        <td class="smallText" align="center" valign="top"><?php echo DISPLAY_SUPPLIERS . '&nbsp;&nbsp' . osc_suppliers_list(); ?></td>
<?php echo osc_hide_session_id(); ?>
                    </form>
<?php
  }
?>  
                  </tr>
                </table></td>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
                <td valign="bottom"><table border="0" cellspacing="0" cellpadding="0">
<?php
// ----------------------------
// --------- Manufactuer
// ----------------------------
  if (DISPLAY_MAJR_MANUFACTURER == 'true') {
    echo osc_draw_form('manufacturers', 'quick_updates.php', '', 'get');
    echo osc_draw_hidden_field( 'row_by_page', $row_by_page); 
    echo osc_draw_hidden_field( 'cPath', $current_category_id);
    echo osc_draw_hidden_field( 'supplier', $supplier); 
?>
                <td class="smallText" align="center" valign="top"><?php echo DISPLAY_MANUFACTURERS . '&nbsp;&nbsp' . manufacturers_list(); ?></td>
<?php echo osc_hide_session_id(); ?>
                  </form>
<?php
  }
?>  
               </tr>
             </table></td>
             <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
             <td valign="bottom"><table border="0" cellspacing="0" cellpadding="0">
               <tr>
<?php 
// ----------------------------
// --------- Row by page
// ----------------------------
echo osc_draw_form('row_by_page', 'quick_updates.php', '', 'get');
echo osc_draw_hidden_field( 'manufacturer', $manufacturer); 
echo osc_draw_hidden_field( 'supplier', $supplier); 
echo osc_draw_hidden_field( 'cPath', $current_category_id);
?>
                 <td class="smallText" align="right"><?php echo TEXT_MAXI_ROW_BY_PAGE . '&nbsp;&nbsp;' . osc_draw_pull_down_menu('row_by_page', $row_bypage_array, $row_by_page, 'onchange="this.form.submit();"'); ?></td>
                 <?php echo osc_hide_session_id(); ?>
                 </form>
               </tr>
               <tr>
<?php 
// ----------------------------
// --------- Categories
// ----------------------------
echo osc_draw_form('categorie', 'quick_updates.php', '', 'get');
echo osc_draw_hidden_field( 'row_by_page', $row_by_page); 
echo osc_draw_hidden_field( 'manufacturer', $manufacturer); 
echo osc_draw_hidden_field( 'supplier', $supplier); 
?>
                   <td class="smallText" align="right"><?php echo DISPLAY_CATEGORIES . '&nbsp;&nbsp;' . osc_draw_pull_down_menu('cPath', osc_get_category_tree(), $current_category_id, 'onchange="this.form.submit();"'); ?></td>
                   <?php echo osc_hide_session_id(); ?>
                    </form>
                 </tr>
               </table></td>
              </tr>
            </table></td>
         </tr>
       </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminform">
          <tr>
        <td class="pageHeading" align="left">
<?php 
  if (DISPLAY_DOUBLE_TAXE == 'false') {
?>
            <script language="javascript"><!--
              switch (browser_family) {
                case 'dom2':
                case 'ie4':
                  document.write("<div id='descDiv'>");
                  break;
                default:
                  document.write("<ilayer id='descDiv'><layer id='descDiv_sub'>");
                  break;
                }
          --></script></td>
        <td class="pageHeading" align="left"><script language="javascript"><!--
                switch (browser_family) {
                  case 'dom2':
                  case 'ie4':
                    document.write("<div id='descDiv2'>");
                    break;
                  default:
                    document.write("<ilayer id='descDiv2'><layer id='descDiv_sub2'>");
                    break;
                  }
             --></script>
<?php
  }
?>
            </td>
            <td align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right"><table border="0" cellspacing="0" cellpadding="0">

<?php 
// ----------------------------
// --------- Search
// ----------------------------
echo osc_draw_form('search', 'quick_updates.php', '', 'get');
?>
                 <td align="center" style="padding-top:30px;"><br /><?php echo osc_draw_input_field('search','', 'id="search" placeholder="'.TEXT_SEARCH.'"');  ?><br /></td>
                 <?php echo osc_hide_session_id(); ?>
                 </form>
             </td>
             <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>          
             </td></table>           
               <td><table border="0" cellspacing="0" cellpadding="0">
                  <form name="spec_price" <?php echo 'action="' . osc_href_link('quick_updates.php', osc_get_all_get_params(array('action', 'info', 'pID')) . "action=calcul&page=" . $page . "&sort_by=" . $sort_by . "&cPath=" . $current_category_id . "&row_by_page=". $row_by_page . "&manufacturer=" . $manufacturer . "&supplier=" . $supplier ."") . '"'; ?> method="post">
                    <tr>
<?php
  if ($preview_global_price != 'true') {
?>
                      <td class="smallText" align="center"><?php echo TEXT_INPUT_SPEC_PRICE . '&nbsp;&nbsp;' . osc_draw_input_field('spec_price',0,'size="5"') ; ?></td>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '20', '1'); ?></td>
<?php
  } else {
?>
                      <td class="smallText" align="center"><?php echo TEXT_SPEC_PRICE_INFO_UPDATE; ?></td>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '20', '1'); ?></td>
<?php
  }
  if ($preview_global_price != 'true') {
?>
                       <td><?php echo osc_image_submit('button_preview_quick_update.gif', IMAGE_PREVIEW, "page=$page&sort_by=$sort_by&cPath=$current_category_id&row_by_page=$row_by_page&manufacturer=$manufacturer&supplier=$supplier"); ?></td>
<?php
  } else {
?>
                        <td><?php echo '<a href="' . osc_href_link('quick_updates.php', "page=$page&sort_by=$sort_by&cPath=$current_category_id&row_by_page=$row_by_page&manufacturer=$manufacturer&supplier=$supplier") . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
<?php
  }
  if (ACTIVATE_COMMERCIAL_MARGIN == 'true') {
?>
            <td><?php echo '&nbsp;&nbsp;&nbsp;&nbsp;' . osc_draw_checkbox_field('marge','yes','','no') . '&nbsp;' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', TEXT_MARGE_INFO); ?></td>
<?php
  }
?>
          </tr>
          <?php echo osc_hide_session_id(); ?>
          </form>
          </table></td>
          <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
          <td><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <form name="update" method="POST" action="<?php echo 'quick_updates.php' . "?action=update&page=$page&sort_by=$sort_by&cPath=$current_category_id&row_by_page=$row_by_page&manufacturer=$manufacturer&supplier=$supplier&search=$search"; ?>">
              <td><?php echo '<a href="javascript:window.print()">' . osc_image_button('button_print.gif', PRINT_TEXT) . '</a>'; ?></td>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
              <td><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE, "action=update&cPath=$current_category_id&page=$page&sort_by=$sort_by&row_by_page=$row_by_page"); ?></td>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
              <td><?php echo '<a href="' . osc_href_link('quick_updates.php',"row_by_page=$row_by_page") . '">' . osc_image_button('button_reset.gif', IMAGE_CANCEL) . '</a>'; ?></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
     </tr>
    </table></td>
    </tr>
    <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
    </tr>
<?php
// get the specials products list
  $specials_array = array();

  $Qspecials = $OSCOM_PDO->prepare('select p.products_id
                                    from :table_products p,
                                         :table_specials s
                                    where s.products_id = p.products_id
                                   ');

  $Qspecials->execute();

  while ($specials = $Qspecials->fetch() ) {
    $specials_array[] = $specials['products_id'];
  }

// control string sort page
  if ($sort_by && !preg_match('#order by#',$sort_by)) {
    $sort_by = 'order by ' . $sort_by;
  }

// define the string parameters for good back preview product
  $origin = 'quick_updates.php'."?info_back=$sort_by-$page-$current_category_id-$row_by_page-$manufacturer-$supplier";
  $split_page = (int)$_GET['page'];

  if ($split_page > 1) $rows = $split_page * MAX_DISPLAY_ROW_BY_PAGE - MAX_DISPLAY_ROW_BY_PAGE;
  if ($current_category_id == 0) {

  if (isset($_GET['search'])) {
    $search = osc_db_prepare_input($_GET['search']);
  }
    
    if ($manufacturer) {
      $products_query_raw = "select p.products_id,
                                    p.products_percentage, 
                                    p.products_image, 
                                    p.products_model, 
                                    pd.products_name, 
                                    p.products_status, 
                                    p.products_weight, 
                                    p.products_quantity, 
                                    p.manufacturers_id,
                                    p.suppliers_id,
                                    p.products_price, 
                                    p.products_tax_class_id,
                                    p.products_min_qty_order,
                                    p.products_price_comparison,
                                    p.products_only_online
                             from  products p,
                                   products_description pd
                             where p.products_id = pd.products_id 
                             and products_archive='0' 
                             and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and p.manufacturers_id = " . (int)$manufacturer . " 
                             and (pd.products_name like '%" . osc_db_input($search) . "%'
                                  or  p.products_model like '%" . osc_db_input($search) . "%'
                                  or p.products_ean like '%" . osc_db_input($search) . "%'
                                 )
                             $sort_by";
    } elseif ($supplier) {
       $products_query_raw = "select p.products_id,
                                    p.products_percentage, 
                                    p.products_image, 
                                    p.products_model, 
                                    pd.products_name, 
                                    p.products_status, 
                                    p.products_weight, 
                                    p.products_quantity, 
                                    p.manufacturers_id,
                                    p.suppliers_id,
                                    p.products_price, 
                                    p.products_tax_class_id,
                                    p.products_min_qty_order,
                                    p.products_price_comparison,
                                    p.products_only_online
                             from  products p,
                                   products_description pd
                             where p.products_id = pd.products_id 
                             and products_archive='0' 
                             and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and p.suppliers_id = " . (int)$supplier ."
                             and (pd.products_name like '%" . osc_db_input($search) . "%'
                                  or  p.products_model like '%" . osc_db_input($search) . "%'
                                  or p.products_ean like '%" . osc_db_input($search) . "%'
                                 )
                             $sort_by";

    } else {	
      $products_query_raw = "select p.products_id, 
                                    p.products_percentage, 
                                    p.products_image, 
                                    p.products_model, 
                                    pd.products_name, 
                                    p.products_status, 
                                    p.products_weight, 
                                    p.products_quantity, 
                                    p.manufacturers_id,
                                    p.suppliers_id,
                                    p.products_price, 
                                    p.products_tax_class_id,
                                    p.products_min_qty_order,
                                    p.products_price_comparison, 
                                    p.products_only_online
                             from  products p,
                                   products_description pd
                             where p.products_id = pd.products_id 
                             and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and products_archive='0'
                             and (pd.products_name like '%" . osc_db_input($search) . "%'
                                  or  p.products_model like '%" . osc_db_input($search) . "%' 
                                  or p.products_ean like '%" . osc_db_input($search) . "%'
                                 )
                             $sort_by";
    }
  } else {
    if ($manufacturer) {
      $products_query_raw = "select p.products_id,
                                    p.products_percentage, 
                                    p.products_image, 
                                    p.products_model, 
                                    pd.products_name, 
                                    p.products_status, 
                                    p.products_weight, 
                                    p.products_quantity, 
                                    p.manufacturers_id,
                                    p.suppliers_id, 
                                    p.products_price, 
                                    p.products_tax_class_id,
                                    p.products_min_qty_order,
                                    p.products_price_comparison,
                                    p.products_only_online
                             from  products p,
                                   products_description pd,
                                   products_to_categories pc
                             where p.products_id = pd.products_id 
                             and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and p.products_id = pc.products_id 
                             and pc.categories_id = '" . $current_category_id . "' 
                             and p.manufacturers_id = " . (int)$manufacturer . " 
                             and products_archive='0'
                             and (pd.products_name like '%" . osc_db_input($search) . "%'
                                  or  p.products_model like '%" . osc_db_input($search) . "%' 
                                  or p.products_ean like '%" . osc_db_input($search) . "%'
                                 )
                             $sort_by";   	

    } elseif ($supplier) {
      $products_query_raw = "select p.products_id,
                                    p.products_percentage, 
                                    p.products_image, 
                                    p.products_model, 
                                    pd.products_name, 
                                    p.products_status, 
                                    p.products_weight, 
                                    p.products_quantity, 
                                    p.manufacturers_id,
                                    p.suppliers_id, 
                                    p.products_price, 
                                    p.products_tax_class_id,
                                    p.products_min_qty_order,
                                    p.products_price_comparison,
                                    p.products_only_online
                             from  products p,
                                   products_description pd,
                                   products_to_categories pc
                             where p.products_id = pd.products_id 
                             and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                             and p.products_id = pc.products_id 
                             and pc.categories_id = '" . $current_category_id . "' 
                             and p.suppliers_id = " . (int)$supplier ."
                             and products_archive='0'
                             and (pd.products_name like '%" . osc_db_input($search) . "%'		
                                  or  p.products_model like '%" . osc_db_input($search) . "%' 
                                  or p.products_ean like '%" . osc_db_input($search) . "%'
                                 )
                             $sort_by";

    } else {
      $products_query_raw = "select p.products_id, 
                                    p.products_percentage, 
                                    p.products_image, 
                                    p.products_model, 
                                    pd.products_name, 
                                    p.products_status, 
                                    p.products_weight, 
                                    p.products_quantity, 
                                    p.manufacturers_id,
                                    p.suppliers_id,
                                    p.products_price, 
                                    p.products_tax_class_id ,
                                    p.products_min_qty_order,
                                    p.products_price_comparison,
                                    p.products_only_online
                           from  products p,
                                 products_description pd,
                                 products_to_categories pc
                           where p.products_id = pd.products_id 
                           and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                           and p.products_id = pc.products_id 
                           and pc.categories_id = '" . $current_category_id . "' 
                           and products_archive='0'
                           and (pd.products_name like '%" . osc_db_input($search) . "%'
                                or  p.products_model like '%" . osc_db_input($search) . "%' 
                                or p.products_ean like '%" . osc_db_input($search) . "%'
                               )
                           $sort_by";
    }
  }

// page splitter and display each products info
  $products_split = new splitPageResults($split_page, MAX_DISPLAY_ROW_BY_PAGE, $products_query_raw, $products_query_numrows);
  $products_query = osc_db_query($products_query_raw);
?>
      <tr>
      <td align="center"><table width="100%" cellspacing="0" cellpadding="0" border="1" bgcolor="#F3F9FB" bordercolor="#D1E7EF" height="100" valgin="top">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr class="dataTableHeadingRow">
                   <td class="dataTableHeadingContent" align="left" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_PREVIEW_IMAGE == 'true') {
?>
                        <td class="dataTableHeadingContent"></td>
                      </tr>
<?php
  }
?>                      
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle" height="25"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php                    
  if (DISPLAY_MAJR_MODEL == 'true') {
    if ($sort_by == 'order by p.products_model DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_model ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_model ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_MODEL . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_model DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_MODEL . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_MODEL; ?></td>

<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if ($sort_by == 'order by pd.products_name DESC') {
    $icon_up = 'icon_up.gif';
  $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by pd.products_name ASC') {
  $icon_up = 'icon_up_active.gif';
  $icon_down = 'icon_down.gif';
  } else {
  $icon_up = 'icon_up.gif';
  $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=pd.products_name ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_PRODUCTS . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=pd.products_name DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_PRODUCTS . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_PRODUCTS; ?></td>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_STATUS == 'true') {
    if ($sort_by == 'order by p.products_status DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_status ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_status ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . 'OFF ' . TEXT_ASCENDINGLY)."</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_status DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS .  $icon_down, TEXT_SORT_ALL . 'ON ' . TEXT_ASCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_STATUS; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_PRICE_COMPARISON == 'true') {
    if ($sort_by == 'order by p.products_price_comparison DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_price_comparison ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_price_comparison ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . 'OFF ' . TEXT_ASCENDINGLY)."</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_price_comparison DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS .  $icon_down, TEXT_SORT_ALL . 'ON ' . TEXT_ASCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_PRICE_COMPARISON; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_PRODUCTS_ONLY_ONLINE == 'true') {
    if ($sort_by == 'order by p.products_only_online DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_only_online ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_only_online ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . 'OFF ' . TEXT_ASCENDINGLY)."</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_only_online DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS .  $icon_down, TEXT_SORT_ALL . 'ON ' . TEXT_ASCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_PRODUCTS_ONLY_ONLINE; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_WEIGHT == 'true') {
    if ($sort_by == 'order by p.products_weight DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_weight ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_weight ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_WEIGHT . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_weight DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_WEIGHT . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_WEIGHT; ?></td>

<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_QUANTITY == 'true') {
    if ($sort_by == 'order by p.products_quantity DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_quantity ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="right" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_quantity ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_QUANTITY . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_quantity DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_QUANTITY . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="right" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_QUANTITY; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_QTY_MIN_ORDER == 'true') {
    if ($sort_by == 'order by p.products_min_qty_order DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_min_qty_order ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_min_qty_order ASC&page=' . $page.'&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_MIN_ORDER_QUANTITY . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_min_qty_order DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_MIN_ORDER_QUANTITY . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="right" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_MIN_ORDER_QUANTITY; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
          <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_IMAGE == 'true') {
    if ($sort_by == 'order by p.products_image DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_image ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="left" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_image ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_IMAGE . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_image DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_IMAGE . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="left" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_IMAGE; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_MANUFACTURER == 'true')  {
    if ($sort_by == 'order by manufacturers_id DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by manufacturers_id ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="left" valign="middle"><?php echo "<a href=\"" . osc_href_link( 'quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=manufacturers_id ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_MANUFACTURERS . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link( 'quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=manufacturers_id DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_MANUFACTURERS . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="left" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_MANUFACTURERS; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
<?php
  if (DISPLAY_MAJR_SUPPLIER == 'true')  {
    if ($sort_by == 'order by suppliers_id DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by suppliers_id ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
                        <td class="dataTableHeadingContent" align="left" valign="middle"><?php echo "<a href=\"" . osc_href_link( 'quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=suppliers_id ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_SUPPLIERS . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link( 'quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=suppliers_id DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_SUPPLIERS . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
                        <td class="dataTableHeadingContent" align="left" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_SUPPLIERS; ?></td>
<?php
  }
?>
                      </tr>
                    </table></td>
                   <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo TABLE_HEADING_PRICE; ?></td>
            </tr>
          </table></td>
<?php
// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {
    if (DISPLAY_MAJR_B2B == 'true') {

      $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_name
                                              from :table_customers_groups
                                              where customers_group_id >  0
                                              order by customers_group_id
                                             ');
      $QcustomersGroup->execute();

      while ($customers_group2 = $QcustomersGroup->fetch() ) {
      if ($QcustomersGroup->rowCount() > 0) {
?>
                <td class="dataTableHeadingContent" align="right" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="dataTableHeadingContent" align="right" valign="middle"><?php echo $customers_group2['customers_group_name']; ?></td>
            </tr>
          </table></td>
<?php
        }
      }
    }
  }
?>
                    <td class="dataTableHeadingContent" align="center" valign="middle"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
<?php
  if (DISPLAY_MAJR_TAX == 'true') {
    if ($sort_by == 'order by p.products_tax_class_id DESC') {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down_active.gif';
  } else if ($sort_by == 'order by p.products_tax_class_id ASC') {
    $icon_up = 'icon_up_active.gif';
    $icon_down = 'icon_down.gif';
    } else {
    $icon_up = 'icon_up.gif';
    $icon_down = 'icon_down.gif';
  }
?>
              <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo "<a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_tax_class_id ASC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS . $icon_up, TEXT_SORT_ALL . TABLE_HEADING_TAX . ' ' . TEXT_ASCENDINGLY) . "</a><a href=\"" . osc_href_link('quick_updates.php', 'cPath=' . $current_category_id . '&sort_by=p.products_tax_class_id DESC&page=' . $page . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier) . "\" >" . osc_image(DIR_WS_ICONS  . $icon_down, TEXT_SORT_ALL . TABLE_HEADING_TAX . ' ' . TEXT_DESCENDINGLY) . "</a>"; ?></td>
              <td class="dataTableHeadingContent" align="center" valign="middle"><?php echo '&nbsp;' . TABLE_HEADING_TAX; ?></td>
<?php
  }
?>
            </tr>
          </table></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"></td>
                    <td class="dataTableHeadingContent" align="center" valign="middle"></td>
                  </tr>
          <tr class="datatableRow">
<?php
  while ($products = osc_db_fetch_array($products_query)) {
    $rows++;
    if (strlen($rows) < 2) {
      $rows = '0' . $rows;
    }

// check for global add value or rates, calcul and round values rates
    if ($_POST['spec_price']){
      $flag_spec = 'true' ;
      $spec_price = $_POST['spec_price'];
      if (substr($_POST['spec_price'],-1) == '%') {
      if ($_POST['marge'] && substr($_POST['spec_price'],0,1) != '-') {
      $valeur = (1 - (preg_replace("#%#", "", $_POST['spec_price']) / 100));
      $price = sprintf("%01.2f", round($products['products_price'] / $valeur,2));
    } else {
          $price = sprintf("%01.2f", round($products['products_price'] + (($spec_price / 100) * $products['products_price']),2));
        }
    } else {
      $price = sprintf("%01.2f", round($products['products_price'] + $spec_price,2));
    }
    } else {
     $price = sprintf("%01.2f", round($products['products_price'], 2));
    }

// Check Tax_rate for displaying TTC

    $Qtax = $OSCOM_PDO->prepare('select r.tax_rate,
                                       c.tax_class_title
                                from :table_tax_rates r,
                                     :table_tax_class c
                                where r.tax_class_id = :tax_class_id
                                and c.tax_class_id = :tax_class_id
                               ');
    $Qtax->bindInt(':tax_class_id', (int)$products['products_tax_class_id'] );

    $Qtax->execute();

    $tax_rate = $Qtax->fetch();

    if ($tax_rate['tax_rate'] == '')$tax_rate['tax_rate'] = 0;

    if (MODIFY_MAJR_MANUFACTURER == 'false') {

      $Qmanufacturer = $OSCOM_PDO->prepare('select manufacturers_name
                                            from :table_manufacturers
                                            where manufacturers_id = :manufacturers_id
                                           ');
      $Qmanufacturer->bindInt(':manufacturers_id', (int)$products['manufacturers_id'] );
      $Qmanufacturer->execute();

      $manufacturer = $Qmanufacturer->fetch();

    }
    
    if (MODIFY_MAJR_SUPPLIER == 'false') {

      $Qsupplier = $OSCOM_PDO->prepare('select suppliers_name
                                        from :table_suppliers
                                        where suppliers_id = :suppliers_id
                                       ');
      $Qsupplier->bindInt(':suppliers_id', (int)$products['suppliers_id'] );
      $Qsupplier->execute();

      $supplier =  $Qsupplier->fetch();
   }

// display infos per row
    if ($flag_spec) {
    echo '<tr class="dataTableRow" onmouseover="';
    if (DISPLAY_MAJR_TVA_OVER == 'true') {
    echo 'display_ttc(\'display\', ' . $price . ', ' . $tax_rate['tax_rate'] . ');';
    }
    echo 'this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="';
    if (DISPLAY_MAJR_TVA_OVER == 'true') {
    echo 'display_ttc(\'delete\');';
    }
    echo 'this.className=\'dataTableRow\'">';
  } else {
    echo '<tr class="dataTableRow" onmouseover="';
    if (DISPLAY_MAJR_TVA_OVER == 'true') {
      echo 'display_ttc(\'display\', ' . $products['products_price'] . ', ' . $tax_rate['tax_rate'] . ');';
    }
    echo 'this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="';
    if (DISPLAY_MAJR_TVA_OVER == 'true') {
      echo 'display_ttc(\'delete\', \'\', \'\', 0);';
    }
    echo 'this.className=\'dataTableRow\'">';
  }

// Affiche l'image
    if (DISPLAY_MAJR_PREVIEW_IMAGE == 'true') {
       echo "<td class=\"smallText\" align=\"left\">" .  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN) . "</td>\n";
    } else {
    echo "<td class=\"smallText\" align=\"left\">";
  }

// Affiche la reference du produit
    if (DISPLAY_MAJR_MODEL == 'true') {
    if(MODIFY_MAJR_MODEL == 'true') {
      echo "<td class=\"smallText\" align=\"center\"><input type=\"text\" size=\"10\" name=\"product_new_model[" . $products['products_id'] . "]\" value=\"" . $products['products_model'] . "\"></td>\n";
    } else {
      echo "<td class=\"smallText\" align=\"left\">" . $products['products_model'] . "</td>\n";
    }
    } else {
    echo "<td class=\"smallText\" align=\"left\">";
  }

// Affiche le nom du produit
    if (MODIFY_MAJR_NAME == 'true') {
    echo "<td class=\"smallText\" align=\"center\"><input type=\"text\" size=\"20\" name=\"product_new_name[" . $products['products_id'] . "]\" value=\"" . $products['products_name'] . "\"></td>\n";
    } else {
      echo "<td class=\"smallText\" align=\"left\">" . $products['products_name'] . "</td>\n";
    }

// Affiche le statut du produit
    if (DISPLAY_MAJR_STATUS == 'true') {
      if ($products['products_status'] == '1') {
        echo "<td class=\"smallText\" align=\"center\"><input type=\"radio\" name=\"product_new_status[" . $products['products_id'] . "]\" value=\"0\" ><input type=\"radio\" name=\"product_new_status[" . $products['products_id'] . "]\" value=\"1\" checked ></td>\n";
      } else {
        echo "<td class=\"smallText\" align=\"center\"><input type=\"radio\" style=\"background-color: #EEEEEE\" name=\"product_new_status[" . $products['products_id'] . "]\" value=\"0\" checked ><input type=\"radio\" style=\"background-color: #EEEEEE\" name=\"product_new_status[" . $products['products_id'] . "]\" value=\"1\"></td>\n";
      }
    } else {
      echo "<td class=\"smallText\" align=\"center\"></td>";
    }

// Affiche le comparateur de prix du produit
    if (DISPLAY_MAJR_PRICE_COMPARISON == 'true') {
      if ($products['products_price_comparison'] == '1') {
        echo "<td class=\"smallText\" align=\"center\"><input type=\"radio\" name=\"product_new_products_price_comparison[" . $products['products_id'] . "]\" value=\"0\" ><input type=\"radio\" name=\"product_new_products_price_comparison[" . $products['products_id'] . "]\" value=\"1\" checked ></td>\n";
      } else {
        echo "<td class=\"smallText\" align=\"center\"><input type=\"radio\" style=\"background-color: #EEEEEE\" name=\"product_new_products_price_comparison[" . $products['products_id'] . "]\" value=\"0\" checked ><input type=\"radio\" style=\"background-color: #EEEEEE\" name=\"product_new_products_price_comparison[" . $products['products_id'] . "]\" value=\"1\"></td>\n";
      }
    } else {
      echo "<td class=\"smallText\" align=\"center\"></td>";
    }

// Affiche le comparateur de prix du produit
    if (DISPLAY_MAJR_PRODUCTS_ONLY_ONLINE == 'true') {
      if ($products['products_only_online'] == '1') {
        echo "<td class=\"smallText\" align=\"center\"><input type=\"radio\" name=\"product_new_products_only_online[" . $products['products_id'] . "]\" value=\"0\" ><input type=\"radio\" name=\"product_new_products_only_online[" . $products['products_id'] . "]\" value=\"1\" checked ></td>\n";
      } else {
         echo "<td class=\"smallText\" align=\"center\"><input type=\"radio\" style=\"background-color: #EEEEEE\" name=\"product_new_products_only_online[" . $products['products_id'] . "]\" value=\"0\" checked ><input type=\"radio\" style=\"background-color: #EEEEEE\" name=\"product_new_products_only_online[" . $products['products_id'] . "]\" value=\"1\"></td>\n";
     }
    } else {
      echo "<td class=\"smallText\" align=\"center\"></td>";
    }


// Affiche le poids du produit
    if (DISPLAY_MAJR_WEIGHT == 'true') {
      echo "<td class=\"smallText\" align=\"center\"><input type=\"text\" size=\"5\" name=\"product_new_weight[" . $products['products_id'] . "]\" value=\"" . $products['products_weight'] . "\"></td>\n";
    } else {
    echo "<td class=\"smallText\" align=\"center\"></td>";
    }

// Affiche la quantite du produit
    if (DISPLAY_MAJR_QUANTITY == 'true') {
    echo "<td class=\"smallText\" align=\"center\"><input type=\"text\" size=\"3\" name=\"product_new_quantity[" . $products['products_id'] . "]\" value=\"" . $products['products_quantity'] . "\"></td>\n";
  } else {
    echo "<td class=\"smallText\" align=\"center\"></td>";
    }

// Affiche la quantite minimum de commande du produit
    if (DISPLAY_QTY_MIN_ORDER == 'true') {
    echo "<td class=\"smallText\" align=\"center\"><input type=\"text\" size=\"3\" name=\"new_products_min_qty_order[" . $products['products_id'] . "]\" value=\"" . $products['products_min_qty_order'] . "\"></td>\n";
  } else {
    echo "<td class=\"smallText\" align=\"center\"></td>";
    }

// Affiche l'images du produit
    if (DISPLAY_MAJR_IMAGE == 'true') {
    echo "<td class=\"smallText\" align=\"center\"><input type=\"text\" size=\"8\" name=\"product_new_image[" . $products['products_id'] . "]\" value=\"" . $products['products_image'] . "\"></td>\n";
    } else {
    echo "<td class=\"smallText\" align=\"center\"></td>";
    }

// Affiche le fabricant du produit
  if (DISPLAY_MAJR_MANUFACTURER == 'true') {
    if(MODIFY_MAJR_MANUFACTURER == 'true') {
      echo "<td class=\"smallText\" align=\"center\">".osc_draw_pull_down_menu("product_new_manufacturer[" . $products['products_id'] . "]\"", $manufacturers_array, $products['manufacturers_id']) . "</td>\n";
    } else {
      echo "<td class=\"smallText\" align=\"center\">" . $manufacturer['manufacturers_name'] . "</td>";
    }
  } else {
    echo "<td class=\"smallText\" align=\"center\"></td>";
  }

// Affiche le fabricant du produit
  if (DISPLAY_MAJR_SUPPLIER == 'true') {
    if(MODIFY_MAJR_SUPPLIER == 'true') {
      echo "<td class=\"smallText\" align=\"center\">".osc_draw_pull_down_menu("product_new_supplier[" . $products['products_id'] . "]\"", $suppliers_array, $products['suppliers_id']) . "</td>\n";
    } else {
      echo "<td class=\"smallText\" align=\"center\">" . $supplier['suppliers_name'] . "</td>";
    }
  } else {
    echo "<td class=\"smallText\" align=\"center\"></td>";
  }

// Affiche le prix du produit
    if (in_array($products['products_id'],$specials_array)) {
      echo "<td class=\"smallText\" align=\"right\">&nbsp;&nbsp;&nbsp;<a href=\"" . osc_href_link ('specials.php', '') . "\">" . osc_image(DIR_WS_ICONS . 'specials.gif', TEXT_QUICK_UPDATES_SPECIALS) . "</a>&nbsp;<input type=\"text\" size=\"6\" name=\"product_new_price[" . $products['products_id'] . "]\" value=\"" . $products['products_price'] . "\" disabled >" . TEXT_NO_TAXE . "</td>\n";
  } else if ($products['products_percentage'] == '0') {
    echo "<td class=\"smallText\" align=\"right\">&nbsp;&nbsp;&nbsp;<a href=\"" . osc_href_link ('categories.php', 'pID=' . $products['products_id'] . '&cPath=' . $categories_products[0] . '&action=new_product') . "\">" . osc_image(DIR_WS_ICONS . 'locked.gif', TEXT_QUICK_UPDATES_LOCKED) . "</a>&nbsp;<input type=\"text\" size=\"6\" name=\"product_new_price[" . $products['products_id'] . "]\" value=\"" . $products['products_price'] . "\" disabled >" . TEXT_NO_TAXE . "</td>\n";
  } else {
          if ($flag_spec == 'true') {
            echo "<td class=\"smallText\" align=\"right\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" size=\"6\" name=\"product_new_price[" . $products['products_id'] . "]\" ";
      if (DISPLAY_MAJR_TVA_UP == 'true') {
      echo "onKeyUp=\"display_ttc('keyup', this.value" . ", " . $tax_rate['tax_rate'] . ", 1);\"";
      }
            echo " value=\"" . $price ."\">" . osc_draw_checkbox_field('update_price[' . $products['products_id'] . ']','yes','checked','no') . TEXT_NO_TAXE . "</td>\n";
          } else {
        echo "<td class=\"smallText\" align=\"right\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\" size=\"6\" name=\"product_new_price[" . $products['products_id'] . "]\" ";
      if (DISPLAY_MAJR_TVA_UP == 'true') {
      echo "onKeyUp=\"display_ttc('keyup', this.value" . ", " . $tax_rate['tax_rate'] . ", 1);\"";
      }
            echo " value=\"" . $price . "\">" . osc_draw_hidden_field('update_price[' . $products['products_id'] . ']','yes') . TEXT_NO_TAXE . "</td>\n";
          }
        }

// Permettre le changement de groupe en mode B2B
  if (MODE_B2B_B2C == 'true') {

// Affichage des prix en B2B
     if (DISPLAY_MAJR_B2B == 'true') {

       $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                                customers_group_name,
                                                                customers_group_discount
                                                from :table_customers_groups
                                                where customers_group_id > 0
                                                order by customers_group_id
                                             ');
       $QcustomersGroup->execute();

      $header = false;

      while ($customers_group = $QcustomersGroup->fetch() ) {
        if (!$header) {
          $header = true;
      }

      if ($QcustomersGroup->rowCount() > 0) {

        $Qattributes = $OSCOM_PDO->prepare('select customers_group_id,
                                                    customers_group_price
                                            from :table_products_groups
                                            where products_id = :products_id
                                            and customers_group_id = :customers_group_id
                                            order by customers_group_id
                                ');
        $Qattributes->bindInt(':products_id', (int)$products['products_id'] );
        $Qattributes->bindInt(':customers_group_id', (int)$customers_group['customers_group_id']);
        $Qattributes->execute();

        }

      if ($attributes2 =  $Qattributes->fetch() ) {

        $price2 = $attributes2['customers_group_price'];
        if ($price2 == '') {
        echo "<td class=\"smallText\" align=\"right\">" . NO_PRICE . "</td>\n";
        } else {
        echo "<td class=\"smallText\" align=\"right\">" . $price2 .  TEXT_NO_TAXE .  "</td>\n";
                    }
      } else {
        echo "<td class=\"smallText\" align=\"right\">" . NO_PRICE . "</td>\n";
      }
    }
    }
  }

// Affichage de la TVA
    if (DISPLAY_MAJR_TAX == 'true') {
    if (MODIFY_MAJR_TAX == 'true') {
      echo "<td class=\"smallText\" align=\"right\">" . osc_draw_pull_down_menu("product_new_tax[" . $products['products_id'] . "]\"", $tax_class_array, $products['products_tax_class_id']) . "</td>\n";
    } else {
      echo "<td class=\"smallText\" align=\"right\">" . $tax_rate['tax_class_title'] . "</td>";
    }
  } else {
    echo "<td class=\"smallText\" align=\"right\"></td>";
  }

// Afficher la visluation du produit
    if (DISPLAY_MAJR_PREVIEW == 'true') {
    echo "<td class=\"smallText\" align=\"left\"><a href=\"" . osc_href_link ('products_preview.php', 'pID=' . $products['products_id'] . '&sort_by=' . $sort_by . '&page=' . $split_page . '&origin=' . $origin) . "\">" . osc_image(DIR_WS_ICONS . 'preview.gif', TEXT_IMAGE_PREVIEW) ."</a></td>\n";
  }

// Editer la fiche produit
    if (DISPLAY_MAJR_EDIT == 'true') {
    echo "<td class=\"smallText\" align=\"left\"><a href=\"" . osc_href_link ('categories.php', 'pID=' . $products['products_id'] . '&cPath=' . $categories_products[0] . '&action=new_product') . "\">" . osc_image(DIR_WS_ICONS . 'products.gif', TEXT_IMAGE_SWITCH_EDIT) . "</a></td>\n";
  }

// Hidden parameters for cache old values
    if (MODIFY_MAJR_NAME == 'true') echo osc_draw_hidden_field('product_old_name[' . $products['products_id'] . '] ',$products['products_name']);
    if (MODIFY_MAJR_MODEL == 'true') echo osc_draw_hidden_field('product_old_model[' . $products['products_id'] . '] ',$products['products_model']);
    echo osc_draw_hidden_field('product_old_status[' . $products['products_id'] . ']',$products['products_status']);
    echo osc_draw_hidden_field('product_old_products_price_comparison[' . $products['products_id'] . ']',$products['products_price_comparison']);
    echo osc_draw_hidden_field('product_old_products_only_online[' . $products['products_id'] . ']',$products['products_only_online']);
    echo osc_draw_hidden_field('product_old_quantity[' . $products['products_id'] . ']',$products['products_quantity']);
    echo osc_draw_hidden_field('products_old_min_qty_order[' . $products['products_id'] . ']',$products['products_min_qty_order']);
    echo osc_draw_hidden_field('product_old_image[' . $products['products_id'] . ']',$products['products_image']);
    if (MODIFY_MAJR_MANUFACTURER == 'true') echo osc_draw_hidden_field('product_old_manufacturer[' . $products['products_id'] . ']',$products['manufacturers_id']);
    if (MODIFY_MAJR_SUPPLIER == 'true') echo osc_draw_hidden_field('product_old_supplier[' . $products['products_id'] . ']',$products['suppliers_id']);
    echo osc_draw_hidden_field('product_old_weight[' . $products['products_id'] . ']',$products['products_weight']);
    echo osc_draw_hidden_field('product_old_price[' . $products['products_id'] . ']',$products['products_price']);
    if (MODIFY_MAJR_TAX == 'true') echo osc_draw_hidden_field('product_old_tax[' . $products['products_id'] . ']',$products['products_tax_class_id']);

// hidden display parameters
    echo osc_draw_hidden_field( 'row_by_page', $row_by_page);
    echo osc_draw_hidden_field( 'sort_by', $sort_by);
    echo osc_draw_hidden_field( 'page', $split_page);
  }
?>
                  </tr>
                </table></td>
              </tr>
              <?php echo osc_hide_session_id(); ?>
             </form>
             </table></td>
           </tr>
          </table></td>
    </tr>
    <tr>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
    </tr>
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_ROW_BY_PAGE, $split_page, TEXT_DISPLAY_NUMBER_OF_PRODUCTS);  ?></td>
            <td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_ROW_BY_PAGE, MAX_DISPLAY_PAGE_LINKS, $split_page, '&cPath='. $current_category_id .'&sort_by='.$sort_by . '&row_by_page=' . $row_by_page . '&manufacturer=' . $manufacturer. '&supplier=' . $supplier); ?> </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
