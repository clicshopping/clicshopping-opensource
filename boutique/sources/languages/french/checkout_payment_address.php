<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE_1', 'Commande');
define('NAVBAR_TITLE_2', 'Changer l\'adresse de facturation');

define('HEADING_TITLE', 'Information paiement');

define('TABLE_HEADING_PAYMENT_ADDRESS', 'Adresse de facturation');
define('TEXT_SELECTED_PAYMENT_DESTINATION', 'Ceci est l\'adresse séléctionnée, o&ugrave; la facture de cette commande sera expédiée.');
define('TITLE_PAYMENT_ADDRESS', 'Adresse de facturation :');

define('TABLE_HEADING_ADDRESS_BOOK_ENTRIES', 'Carnet d\'adresses');
define('TEXT_SELECT_OTHER_PAYMENT_DESTINATION', 'Veuillez choisir l\'adresse de facturation  si la facture de cette commande doit être envoyée ailleurs.');
define('TITLE_PLEASE_SELECT', 'Veuillez choisir');

define('TABLE_HEADING_NEW_PAYMENT_ADDRESS', 'Nouvelle adresse de facturation');
define('TEXT_CREATE_NEW_PAYMENT_ADDRESS', 'Veuillez utiliser ce formulaire pour créer une nouvelle adresse de facturation pour cette commande.');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continuer la procédure de commande');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'pour choisir la méthode préférée de paiement.');
define('ERROR_ADDRESS_BOOK_NO_MODIFY_DEFAULT', 'Vous n\'etes pas autorisé à modifier l\'adresse de facturation. N\'hésitez pas à nous contacter pour toutes informations complémentaires.');
?>