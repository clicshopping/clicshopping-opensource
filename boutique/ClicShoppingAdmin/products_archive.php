<?php
/**
 * Product_archive.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {

// ULTIMATE Seo Urls 5
// If the action will affect the cache entries  
// bug usu
/*
    if ($action == 'update' || $action == 'setflag' || $action == 'delete_all') { 
      osc_reset_cache_data_usu5('reset'); //bug usu
    }  
*/
    switch ($action) {   
     case 'setflag':
        if (isset($_GET['aID'])) $products_id = osc_db_prepare_input($_POST['products_id']); 
        $products_id = $_GET['aID'];
        osc_set_product_status($_GET['aID'], $_GET['flag']);

          if (USE_CACHE == 'true') {
            osc_reset_cache_block('categories');
            osc_reset_cache_block('also_purchased');
            osc_reset_cache_block('products_related');
            osc_reset_cache_block('products_cross_sell');
            osc_reset_cache_block('upcoming');
          }
        osc_redirect(osc_href_link('products_archive.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'aID=' . $products_id));
     break;	

     case 'update':
        osc_reset_cache_data_usu5( 'reset' );
        if (isset($_GET['aID'])) $products_id = osc_db_prepare_input($_POST['products_id']); 
        $products_id = $_GET['aID'];

        $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                        set products_archive = :products_archive 
                                        where products_id = :products_id
                                      ');
        $Qupdate->bindInt(':products_archive', '0' );
        $Qupdate->bindInt(':products_id', (int)$products_id );
        $Qupdate->execute();


        if (USE_CACHE == 'true') {
          osc_reset_cache_block('categories');
          osc_reset_cache_block('also_purchased');
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
          osc_reset_cache_block('upcoming');
        }

        osc_redirect(osc_href_link('products_archive.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '' : '')));
     break; 


// the images are not deleted in this case
      case 'delete_all':

       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $products['products_id'] ) {
         
          $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                          set products_archive = :products_archive 
                                          where products_id = :products_id
                                        ');
          $Qupdate->bindInt(':products_archive', '0' );
          $Qupdate->bindInt(':products_id', (int)$products['products_id'] );
          $Qupdate->execute();
         }
       }

          if (USE_CACHE == 'true') {
            osc_reset_cache_block('categories');
            osc_reset_cache_block('also_purchased');
            osc_reset_cache_block('products_related');
            osc_reset_cache_block('products_cross_sell');
            osc_reset_cache_block('upcoming');
          }

       osc_redirect(osc_href_link('products_archive.php'));
      break;	
    }
  }

  require('includes/header.php');
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<!-- body_text //-->
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/archive.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-6 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
    <span class="col-md-3">
      <div class="form-group">
        <div class="controls">
<?php
  echo osc_draw_form('search', 'products_archive.php', '', 'get');
  echo osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');
?>
        </div>
      </div>
    </span>
<?php
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
?>
    <span class="pull-right"><?php echo '<a href="' . osc_href_link('products_archive.php') . '">' . osc_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></span>
<?php
    }
    echo osc_hide_session_id(); 
?>
      </form>
    <span class="pull-right">
      <form name="delete_all" <?php echo 'action="' . osc_href_link('products_archive.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
      <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button">&nbsp;<span><?php echo osc_image_button('button_unpack_all.gif', IMAGE_DELETE); ?></span></a>&nbsp;
    </span>
  </div>



<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<?php // Gestion des erreurs et de succes des validations
  if ($OSCOM_MessageStack->size > 0) {
    $ClassMessageStack = "messageStackSuccess";
    if ($ClassMessageStackError == 1) {
      $ClassMessageStack = "messageStackError";
  }
?>
  </tr>
  <tr>
    <td><table border="0" width="100%" cellspacing="5" cellpadding="0" class="<?php echo $ClassMessageStack; ?>">
      <tr>
        <td class="<?php echo $ClassMessageStack; ?>">
          <table width="100%">
            <?php echo $OSCOM_MessageStack->output(); ?>
          </table>
        </td>
      </tr>
    </table></td>
 </tr>
<?php
    }
?>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<!-- //################################################################################################################ -->
<!-- //                                             LISTING DES produits                                      -->
<!-- //################################################################################################################ -->
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="dataTableHeadingContent">&nbsp;</td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MODEL_ARCHIVES; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_ARCHIVES; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_ARCHIVES; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
// Recherche
    $search = '';
    if (isset($_GET['search']) && osc_not_null($_GET['search'])) {
      $keywords = osc_db_input(osc_db_prepare_input($_GET['search']));
      $search = "and (p.products_model like '%" . $keywords . "%' or  pd.products_name like '%" . $keywords . "%') ";
    }

    $products_archive_query_raw = "select p.products_id, 
                                          p.products_model,
                                          p.products_image,   
                                          p.products_price, 
                                          pd.products_name,
                                          p.products_date_added, 
                                          p.products_last_modified,
                                          p.products_status,
                                          p.products_archive
                                   from products p,
                                        products_description pd
                                   where p.products_id = pd.products_id
                                   and p.products_archive = '1'
                                   and pd.language_id = '" .(int)$_SESSION['languages_id'] ."'
                                   " . $search . " 
                                   order by  p.products_last_modified DESC, pd.products_name
                                  ";
      $products_archive_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN,  $products_archive_query_raw, $products_archive_query_numrows);
      $products_query = osc_db_query($products_archive_query_raw);

      while ($products = osc_db_fetch_array($products_query)) {
        if ((!isset($_GET['aID']) || (isset($_GET['aID']) && ($_GET['aID'] == $products['products_id']))) && !isset($mInfo)) {

          $products_archive_query = osc_db_query("select count(*) as products_count 
                                                  from products
                                                  where products_id = '" . (int)$products['products_id'] . "' 
                                                  and products_archive = '1'
                                                ");
          $products_archive= osc_db_fetch_array($products_archive_query);

          $mInfo_array = array_merge($products, $products_archive);
          $mInfo = new objectInfo($mInfo_array);
        }

      if (isset($mInfo) && is_object($mInfo) && ($products['products_id'] == $mInfo->products_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
/*
        if (isset($mInfo) && is_object($mInfo) && ($products['products_id'] == $mInfo->products_id)) {
          echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('products_archive.php', 'page=' . $_GET['page'] . '&aID=' . $products['products_id'] . '&action=edit') . '\'">' . "\n";
        } else {
          echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('products_archive.php', 'page=' . $_GET['page'] . '&aID=' . $products['products_id']) . '\'">' . "\n";
        }
*/
?>
                <td>
<?php 
      if ($products['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $products['products_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $products['products_id']; ?>" />
<?php 
      } 
?>
                </td>
                <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                <td class="dataTableContent"><?php echo $products['products_model']; ?></td>
                <td class="dataTableContent"><?php echo $products['products_name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $products['products_last_modified']; ?></td>
                <td class="dataTableContent" align="center">
<?php
       if ($products['products_status'] == '1') {
         echo '<a href="' . osc_href_link('products_archive.php', 'action=setflag&flag=0&aID=' . $products['products_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
       } else {
          echo '<a href="' . osc_href_link('products_archive.php', 'action=setflag&flag=1&aID=' . $products['products_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
       }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php
           echo '<a href="' . osc_href_link('products_archive.php', 'page=' . $_GET['page'] . '&aID=' . $products['products_id'] . '&action=update') . '">' . osc_image(DIR_WS_ICONS . 'unpack.gif', IMAGE_UNPACK) . '</a>' ;
           echo osc_draw_separator('pixel_trans.gif', '6', '16');
           if (isset($mInfo) && is_object($mInfo) && ($products['products_id'] == $mInfo->products_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . osc_href_link('products_archive.php', 'page=' . $_GET['page'] . '&aID=' . $products['products_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $products_archive_split->display_count($products_archive_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                    <td class="smallText" align="right"><?php echo $products_archive_split->display_links($products_archive_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </form>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_HEADING_EDIT_PRODUCTS_ARCHIVE . '</strong>');
      $contents = array('form' => osc_draw_form('products', 'products_archive.php', 'page=' . $_GET['page'] . '&aID=' . (int)$_GET['aID']. '&action=update', 'post', 'enctype="multipart/form-data"'));
      $contents[] = array('text' => TEXT_EDIT_INTRO);
// Bouton de sauvegarde ou annuler
      $contents[] = array('align' => 'center', 'text' => '<br />' . osc_image_submit('button_unpack.gif', IMAGE_UNPACK) . ' <a href="' . osc_href_link('products_archive.php', 'page=' . $_GET['page'] . '&aID=' . (int)$_GET['aID']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;

    default:
      break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

