<?php
/**
 * products_notification.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('TEXT_COUNT_CUSTOMERS', 'Clients recevant le bulletin d\'informations : %s');
define('TEXT_PRODUCTS', 'Produits');
define('TEXT_SELECTED_PRODUCTS', 'Produits sélectionnés');
define('TEXT_UNSUBSCRIBE', '<font size="2">Conformément aux <strong>Lois</strong> en vigueur dans le pays de la boutique '.STORE_NAME.', vous avez droit � la rectification de vos données personnelles � tout moment ou sur simple demande par email.<br />Pour se désabonner de notre Newsletter, cliquez sur le lien suivant :</font><br />');

define('JS_PLEASE_SELECT_PRODUCTS', 'Veuillez sélectionner des produits avant de cliquer sur le bouton "Envoyer".');

define('BUTTON_GLOBAL', 'Global');
define('BUTTON_SELECT', '>>>');
define('BUTTON_UNSELECT', '<<<');
define('BUTTON_SUBMIT', 'Envoyer');
define('BUTTON_CANCEL', 'Annuler');
?>