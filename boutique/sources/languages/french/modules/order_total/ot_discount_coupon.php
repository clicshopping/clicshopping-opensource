<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TITLE', 'Coupon réduction');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TAX_NOT_APPLIED', ' dont Taxe appliquée');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DESCRIPTION', 'Coupon de réduction<br /><br /><p style="color:#FF0000;"></style><strong>Attention!</strong> La désactivation de ce module rétablira les valeurs par défaut</p>');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DISPLAY_FILE', 'Code coupon [code] applique');
  define('MODULE_ORDER_TOTAL_DISCOUNT_COUPON_TEXT_SHIPPING_DISCOUNT', 'off shipping');
/*
Use this to define how the order total line will display on the order confirmation, invoice, etc.
You can insert variables to have dynamic data display.
Variables:
[code]
[coupon_desc]
[discount_amount]
[min_order]
[number_available]
[tax_desc]
*/
?>
