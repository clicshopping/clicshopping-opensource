<?php
/**
 * server_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Server Information');

define('TITLE_SERVER_HOST', 'Server Host:');
define('TITLE_SERVER_OS', 'Server OS:');
define('TITLE_SERVER_DATE', 'Server Date:');
define('TITLE_SERVER_UP_TIME', 'Server Up Time:');
define('TITLE_HTTP_SERVER', 'HTTP Server:');
define('TITLE_PHP_VERSION', 'PHP Version:');
define('TITLE_ZEND_VERSION', 'Zend:');
define('TITLE_DATABASE_HOST', 'Database Host:');
define('TITLE_DATABASE', 'Database:');
define('TITLE_DATABASE_DATE', 'Database Date:');

// Onglet
define('SERVER_INFO', 'General information on the server');
define('SERVER_INFO_PHP', 'Information on the server PHP');
define('TAB_INFO_SERVEUR', 'Information server');
define('TAB_INFO_PHP', 'Information PHP');
?>