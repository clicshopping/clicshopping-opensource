<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ACTION_RECORDER_CONTACT_US_TITLE', 'Contact Us');
  define('MODULE_ACTION_RECORDER_CONTACT_US_DESCRIPTION', 'Record usage of the Contact Us feature.');
?>