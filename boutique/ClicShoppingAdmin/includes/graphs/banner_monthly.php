<?php
/*
 * banner_monthly.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

  include('includes/classes/phplot.php');

  $year = (($_GET['year']) ? $_GET['year'] : date('Y'));

  $stats = array();
  for ($i=1; $i<13; $i++) {
    $stats[] = array(strftime('%b', mktime(0,0,0,$i)), '0', '0');
  }

  $QbannerStats = $OSCOM_PDO->prepare('select month(banners_history_date) as banner_month,
                                             sum(banners_shown) as value,
                                             sum(banners_clicked) as dvalue
                                      from :table_banners_history
                                      where banners_id = :banners_id
                                      and year(banners_history_date) = :year
                                      group by banner_month
                                    ');
  $QbannerStats->bindInt(':banners_id', (int)$banner_id);
  $QbannerStats->bindValue(':year', $year);

  $QbannerStats->execute();

  while ($banner_stats = $QbannerStats->fetch() ) {
    $stats[($banner_stats['banner_month']-1)] = array(strftime('%b', mktime(0,0,0,$banner_stats['banner_month'])), (($banner_stats['value']) ? $banner_stats['value'] : '0'), (($banner_stats['dvalue']) ? $banner_stats['dvalue'] : '0'));
  }

  $graph = new PHPlot(600, 350, 'images/graphs/banner_monthly-' . $banner_id . '.' . $banner_extension);

  $graph->SetFileFormat($banner_extension);
  $graph->SetIsInline(1);
  $graph->SetPrintImage(0);

  $graph->SetSkipBottomTick(1);
  $graph->SetDrawYGrid(1);
  $graph->SetPrecisionY(0);
  $graph->SetPlotType('bars');

  $graph->SetPlotBorderType('left');
  $graph->SetTitleFontSize('4');
  $graph->SetTitle(sprintf(TEXT_BANNERS_MONTHLY_STATISTICS, $banner['banners_title'], $year));

  $graph->SetBackgroundColor('white');

  $graph->SetVertTickPosition('plotleft');
  $graph->SetDataValues($stats);
  $graph->SetDataColors(array('blue','red'),array('blue', 'red'));

  $graph->DrawGraph();

  $graph->PrintImage();

