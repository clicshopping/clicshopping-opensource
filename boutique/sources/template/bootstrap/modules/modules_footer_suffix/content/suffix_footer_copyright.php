<div class="clearfix"></div>
<div class="ContentText">
  <div class="footerSuffix">
    <div class="footerSuffixCopyright">
      <span class="footerSuffixCopyright"><?php echo MODULES_FOOTER_SUFFIX_COPYRIGHT_TEXT; ?></span>
    </div>
    <div class="footerSuffixTrademark">
      <span class="footerSuffixTrademark"><?php echo  $logo . MODULES_FOOTER_SUFFIX_TRADEMARK_TEXT; ?></span>
    </div>
  </div>
</div>