<?php
/*
 * product_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require($OSCOM_Template->getTemplateHeaderFooter('header'));

// ----------------------------------------------------------------//
//                      file not found                             //
// ----------------------------------------------------------------//

  if ($OSCOM_Products->getProductsCount() < 1 || (is_null($id)) ) {
//    header('HTTP/1.0 404 Not Found');
?>

<div class="contentContainer">
  <div class="contentText" style="padding-top:20px;">
    <div class="alert alert-warning" style="text-align:center;">
       <h3><?php echo TEXT_PRODUCT_NOT_FOUND; ?></h3>
    </div>
    <div class="buttonSet pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, '', osc_href_link('index.php'),'success'); ?></div>
  </div>
</div>    

<?php
  }
// ----------------------------------------------------------------
// ---- Affiche la fiche produit selon les autorisations   ----
// ------------------------------------------------------------

    if ( $OSCOM_Products->getProductsGroupView() == '1' ||  $OSCOM_Products->getProductsView() == '1') {
      require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
  <div class="contentContainer">
    <div itemscope itemtype="http://schema.org/Product">
<?php
        echo $OSCOM_Template->getBlocks('modules_products_info');
?>
    </div>
  </div>
<?php     
    }
    require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>
