<?php
/*
 * create_account_success.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('create_account_success'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1);
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2);

  if (sizeof($_SESSION['navigation']->snapshot) > 0) {
    $origin_href = osc_href_link($_SESSION['navigation']->snapshot['page'], osc_array_to_string($_SESSION['navigation']->snapshot['get'], array(session_name())), $_SESSION['navigation']->snapshot['mode']);
    $_SESSION['navigation']->clear_snapshot();
  } else {
    $origin_href = osc_href_link('index.php');
  }

  require($OSCOM_Template->getTemplateFiles('create_account_success'));
