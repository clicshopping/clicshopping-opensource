<?php
  /**
   * cfm_modules_footer.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_modules_footer {
    var $code = 'modules_footer';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_FOOTER_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_footer() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_footer/';
      $this->title = MODULE_CFG_MODULE_FOOTER_TITLE;
    }
  }
?>
