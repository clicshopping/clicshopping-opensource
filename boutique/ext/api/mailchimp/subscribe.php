<?php
/**
 * subscribe.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */


  if (isset($_POST['email_address'])) {

    $api_key = $_POST['u']; // MODULES_HEADER_TAGS_MAILCHIMP_API
    $list_id = $_POST['id']; // MODULES_HEADER_TAGS_MAILCHIMP_LIST_ANONYMOUS

    require('Mailchimp.php');
    $Mailchimp = new Mailchimp($api_key);
    $Mailchimp_Lists = new Mailchimp_Lists($Mailchimp);

    $email = array('email' => htmlentities($_POST['email_address']));
    $merge_vars = array('FNAME' =>  $_POST['firstname'],
                        'LNAME' => $_POST['lastname']);
    $email_type = 'html';

    $subscriber = $Mailchimp_Lists->subscribe($list_id, $email, $merge_vars, $email_type);

    if (!empty($subscriber['leid'])) {
      echo "success";
    } else {
      echo "fail";
    }
  }
?>