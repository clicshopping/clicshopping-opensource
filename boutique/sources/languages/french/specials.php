<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Promotions');
define('HEADING_TITLE', 'A ne pas manquer !');
define('MIN_QTY_ORDER_PRODUCT','Qté min : ');
define('TEXT_NO_PRODUCTS','Aucune promotion en ce moment');
define('TEXT_PRODUCT_OPTIONS','Options possibles ');
?>