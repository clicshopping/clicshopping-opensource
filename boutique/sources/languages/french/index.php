<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('TABLE_HEADING_NEW_PRODUCTS','Nouveautés');
define('TABLE_HEADING_NEW_SPECIALS','Promotions du moment');
define('TABLE_HEADING_UPCOMING_PRODUCTS', 'Articles à venir');
define('TABLE_HEADING_DATE_EXPECTED', 'Date présumée');

  define('HEADING_TITLE', '');
  define('TABLE_HEADING_IMAGE', '');
  define('TABLE_HEADING_MODEL', 'Référence');
  define('TABLE_HEADING_PRODUCTS', 'Articles ');
  define('TABLE_HEADING_MANUFACTURER', 'Marque');
  define('TABLE_HEADING_QUANTITY', 'Stock');
  define('TABLE_HEADING_PRICE', 'Prix');
  define('TABLE_HEADING_WEIGHT', 'Poids');
  define('TABLE_HEADING_BUY_NOW', 'Quantité minimum');
  define('TEXT_NO_PRODUCTS', 'Il n\'y a aucun articles dans cette catégorie.');
  define('TEXT_NO_PRODUCTS2', 'Aucun article de cette marque n\'est disponible.');
  define('TEXT_NUMBER_OF_PRODUCTS', 'Nombre d\'articles: ');
  define('TEXT_SHOW', '');
  define('TEXT_BUY', 'Acheter 1 \'');
  define('TEXT_NOW', '\' maintenant');
  define('TEXT_ALL_CATEGORIES', 'Nos catégories');
  define('TEXT_ALL_MANUFACTURERS', 'Nos Marques');

define('MIN_QTY_ORDER_PRODUCT','Qté min : ');
define('TEXT_PRICE','Tarif : ');
define('MIN_QTY_ORDER_PRODUCT','Pour réaliser une commande, vous devez respecter les quantités; minimum.');
?>
