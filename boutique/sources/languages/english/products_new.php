<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'New Products');
define('HEADING_TITLE', 'New Products');

define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_MANUFACTURER', 'Brands:');
define('TEXT_PRICE', 'Price:');
define('TEXT_PRODUCT_OPTIONS','options suggested:');
define('TEXT_NO_PRODUCTS','No news in this moment');
?>