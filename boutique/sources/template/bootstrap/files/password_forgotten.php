<?php
/*
 * passwrd_forgotten.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('password_forgotten') ) {
    echo $OSCOM_MessageStack->get('password_forgotten');
  }
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  if ($password_reset_initiated == true) {
?>

<div class="contentContainer">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '20'); ?></div>
  <div class="contentText">
    <?php echo TEXT_PASSWORD_RESET_INITIATED; ?>
  </div>
  <div class="control-group">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '20'); ?></div>
    <div class="controls">
      <div class="buttonSet" align="right"><?php echo  osc_draw_button(IMAGE_BUTTON_CONTINUE, null, osc_href_link('login.php', '', 'SSL'), 'success'); ?>
      </div>
    </div>
  </div>
</div>
<?php
  } else {
// ----------------------
// --- Password   -----  
// ---------------------- 
 echo osc_draw_form('password_forgotten', osc_href_link('password_forgotten.php', 'action=process', 'SSL'), 'post', '', true);
?>
<div class="contentContainer">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '20'); ?></div>
  <div class="contentText">
    <div><?php echo TEXT_MAIN; ?></div>
  </div>
  <div class="row">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="form-group has-feedback">
      <label class="control-label col-sm-2" for="email_address"><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
      <div class="controls col-sm-8">
        <?php echo osc_draw_input_field('email_address', null, 'required aria-required="true" autofocus="autofocus" id="email_address" placeholder="'.ENTRY_EMAIL_ADDRESS.'"'); ?>
      </div>
    </div>
  </div>
  <div class="buttonSet row">
    <div class="col-xs-6"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('login.php', '', 'SSL'), 'info'); ?></div>
    <div class="col-xs-6 text-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success'); ?></div>
  </div>
</div>

</form>

<?php
  }
  require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>
