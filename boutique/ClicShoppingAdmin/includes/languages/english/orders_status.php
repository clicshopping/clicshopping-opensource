<?php
/*
 * orders_status.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('HEADING_TITLE', 'Orders Status');

define('TABLE_HEADING_ORDERS_STATUS', 'Orders Status');
define('TABLE_HEADING_PUBLIC_STATUS', 'Orders Public Status : ');
define('TABLE_HEADING_DOWNLOADS_STATUS', 'Downloads Status: : ');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Orders Status:');
define('TEXT_INFO_INSERT_INTRO', 'Please enter the new orders status with its related data');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this order status?');
define('TEXT_INFO_HEADING_NEW_ORDERS_STATUS', 'New Orders Status');
define('TEXT_INFO_HEADING_EDIT_ORDERS_STATUS', 'Edit Orders Status');
define('TEXT_INFO_HEADING_DELETE_ORDERS_STATUS', 'Delete Orders Status');

define('TEXT_SET_PUBLIC_STATUS', 'Show the order to the customer at this order status level');
define('TEXT_SET_DOWNLOADS_STATUS', 'Allow downloads of virtual products at this order status level');
define('TEXT_SET_SUPPORT_ORDERS_FLAG','Delete the invoice PDF display at the customer (To apply only the customer support status) ');
define('TABLE_HEADING_PUBLIC_STATUS_DOWNLOAD', 'Download Statut : ');
define('TEXT_INFO_EDIT_PUBLIC_STATUS_VIEW', 'Visible');
define('TEXT_INFO_EDIT_PUBLIC_STATUS_NOVIEW', 'Invisible');
define('TEXT_INFO_EDIT_PUBLIC_DOWNLOAD_VIEW', 'Visible');
define('TEXT_INFO_EDIT_PUBLIC_DOWNLOAD_NOVIEW', 'Invisible');

define('ERROR_REMOVE_DEFAULT_ORDER_STATUS', 'Error: The default order status can not be removed. Please set another order status as default, and try again.');
define('ERROR_STATUS_USED_IN_ORDERS', 'Error: This order status is currently used in orders.');
define('ERROR_STATUS_USED_IN_HISTORY', 'Error: This order status is currently used in the order status history.');
?>