<?php
/**
 * template_email.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE','Template des emails envoyés');
define('TABLE_HEADING_TEMPLATE_EMAIL_NAME','Nom du template');
define('TABLE_HEADING_TEMPLATE_EMAIL_DESCRIPTION','Description');
define('TABLE_HEADING_TEMPLATE_EMAIL_TYPE','Email envoyé de');
define('TABLE_HEADING_TEMPLATE_CUSTOMER_GROUPS','Mode du groupe client');
define('TABLE_HEADING_ACTION','Actions');



define('TEXT_INFO_TEMPLATE_CATALOG','Template Admin / Catalogue');
define('TITLE_TEMPLATE_EMAIL_DESCRIPTION','Description Email');

define('TEMPLATE_EMAIL_TEXT_NAME','Nom du template : ');
define('TEMPLATE_EMAIL_TEXT_SHORT_DESCRIPTION','Description courte du template : ');
define('TEMPLATE_EMAIL_TEXT_DESCRIPTION','Description du message : ');
define('TEMPLATE_EMAIL_TEXT_CATALOG_ADMIN','Message envoyé à partir du :');
define('TEMPLATE_EMAIL_TEXT_CUSTOMER_GROUP','Groupe client destinataire : ');
define('TEMPLATE_EMAIL_TEXT_VARIABLE','Variable');
define('TEXT_TEMPLATE_EMAIL_CATALOG','Catalogue');
define('TEXT_TEMPLATE_EMAIL_ADMIN','Administration');
define('TEXT_TEMPLATE_EMAIL_ADMIN_CATALOG','Administration et catalogue');
define('TEXT_TEMPLATE_EMAIL_B2C','Mode B2C');
define('TEXT_TEMPLATE_EAIL_B2C_B2B','Mode B2B B2C');

define('TAB_DESCRIPTION','Description');
define('TAB_GENERAL','Général');
define('TITLE_INFORMATION_NAME', 'Informations Générales');
define('TITLE_MESSAGE','Message à envoyer');

define('AIDE_TITLE_ONGLET_GENERAL', 'Informations sur la gestion du template');

define('TEXT_AIDE_TEMPLATE', '<ul>
<li>%%HTTP_CATALOG%% vous permettra d\'insérer l\'URL du catalogue du site dans votre email : ex : %%HTTP_CATALOG%%login.php = http://www.maboutique/boutique/login.php</li>
<li>%%STORE_NAME%% vous permettra d\'insérer le nom de la boutique dans votre email</li>
<li>%%STORE_OWNER_EMAIL_ADDRESS%%  vous permettra d\'insérer votre adresse email de la boutique dans votre email</li>
</ul>');
?>