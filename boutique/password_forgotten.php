<?php
/*
 * passwrd_forgotten.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('password_forgotten'));

  $password_reset_initiated = false;

  if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $email_address = isset($_POST['email_address']) ? trim($_POST['email_address']) : null;


    if ( !empty($email_address) ) {

      $Qc = $OSCOM_PDO->prepare('select customers_id, 
                                       customers_firstname, 
                                       customers_lastname,
                                       member_level 
                                  from :table_customers 
                                  where customers_email_address = :customers_email_address 
                                  limit 1');
      $Qc->bindValue(':customers_email_address', $email_address);
      $Qc->execute();

      if ( $Qc->fetch() !== false ) {


        if ($Qc->value('member_level') == '1') {
           $actionRecorder = new actionRecorder('ar_reset_password', $Qc->valueInt('customers_id'), $email_address);

          if ($actionRecorder->canPerform()) {
            $actionRecorder->record();

            $reset_key = osc_create_random_value(40);

            $OSCOM_PDO->save('customers_info', array('password_reset_key' => $reset_key,
                                                     'password_reset_date' => 'now()'),
                                                array('customers_info_id' => $Qc->valueInt('customers_id')));

            $reset_key_url = osc_href_link('password_reset.php', 'account=' . urlencode($email_address) . '&key=' . $reset_key, 'SSL', false);

            if ( strpos($reset_key_url, '&amp;') !== false ) {
              $reset_key_url = str_replace('&amp;', '&', $reset_key_url);
            }

            $message = utf8_decode(EMAIL_PASSWORD_RESET_BODY);
            $email_password_reminder_body = html_entity_decode($message);

            osc_mail($Qc->value('customers_firstname') . ' ' . $Qc->value('customers_lastname'), $email_address, EMAIL_PASSWORD_RESET_SUBJECT, sprintf($email_password_reminder_body, $reset_key_url), STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

            $password_reset_initiated = true;
          } else {
            $actionRecorder->record(false);

            $OSCOM_MessageStack->addError('password_forgotten', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_RESET_PASSWORD_MINUTES') ? (int)MODULE_ACTION_RECORDER_RESET_PASSWORD_MINUTES : 5)));
          }

        } else {
          $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
        }
      } else {
        $OSCOM_MessageStack->addError('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
      } 
    } // end $email_address
  } // end form

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('login.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('password_forgotten.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('password_forgotten'));
