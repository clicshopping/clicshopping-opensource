<?php
/**
 * discount_coupons.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');
  require('includes/functions/discount_coupons.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');


  $error = (isset($_GET['error']) ? $_GET['error'] : '');
  $message = (isset($_GET['message']) ? $_GET['message'] : '');

  $coupons_id = (!empty($_POST['coupons_id'] ) ? osc_db_input($_POST['coupons_id'] ) : (!empty( $_GET['cID'] ) ? osc_db_input($_GET['cID'] ) : kgt_create_random_coupon() ) );
  $coupons_description = osc_db_prepare_input($_POST['coupons_description']);
  $coupons_discount_amount = osc_db_prepare_input($_POST['coupons_discount_amount']);
  $coupons_discount_type = osc_db_prepare_input($_POST['coupons_discount_type']);
  $coupons_max_use = osc_db_prepare_input($_POST['coupons_max_use']);
  $coupons_min_order = osc_db_prepare_input($_POST['coupons_min_order']);
  $coupons_min_order_type  = osc_db_prepare_input($_POST['coupons_min_order_type']);
  $coupons_number_available =  osc_db_prepare_input($_POST['coupons_number_available']);
  $coupons_date_start = osc_db_prepare_input($_POST['coupons_date_start']);
  $coupons_date_end = osc_db_prepare_input($_POST['coupons_date_end']);

// Definir la position 0 ou 1 pour --> coupons_create_account : Code coupon envoye lors de la creation d'un compte par le client
   if (osc_db_prepare_input($_POST['coupons_create_account_b2c']) == '1') {
     $coupons_create_account_b2c = '1';
   } else {
     $coupons_create_account_b2c = '0';
   }

// Definir la position 0 ou 1 pour --> coupons_create_account : Code coupon envoye lors de la creation d'un compte par le client pour les clients B2B
   if (osc_db_prepare_input($_POST['coupons_create_account_b2b']) == '1') {
     $coupons_create_account_b2b = '1';
   } else {
     $coupons_create_account_b2b = '0';
   }

// Definir la position 0 ou 1 pour --> coupons_twitter : Code coupon envoye sur twitter
   if (osc_db_prepare_input($_POST['coupons_twitter']) == '1') {
     $coupons_twitter = '1';
   } else {
     $coupons_twitter = '0';
   }

// Error Message
  if( osc_not_null($error)) {
    $OSCOM_MessageStack->add( $error, 'error' );
  }
  if( osc_not_null($message)) {
    $OSCOM_MessageStack->add( $message, 'success' );
  }


// Count the coupon for create account b2b for an alert message
  $QcouponsCountB2b = $OSCOM_PDO->prepare("select count(coupons_create_account_b2b) as coupons_b2b_count 
                                           from :table_discount_coupons
                                           where coupons_create_account_b2b = :coupons_create_account_b2b
                                         ");
  $QcouponsCountB2b->bindValue(':coupons_create_account_b2b', '1' );
  $QcouponsCountB2b->execute();

  $coupons_b2b_count = $QcouponsCountB2b->fetch();

// Count the coupon for create account b2b for an alert message
  $QcouponsCountB2c = $OSCOM_PDO->prepare("select count(coupons_create_account_b2c) as coupons_b2c_count 
                                           from :table_discount_coupons
                                           where coupons_create_account_b2c = :coupons_create_account_b2c
                                          ");
  $QcouponsCountB2c->bindValue(':coupons_create_account_b2c', '1' );
  $QcouponsCountB2c->execute();

  $coupons_b2c_count = $QcouponsCountB2c->fetch();

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':

//some error checking:
        if(empty($_POST['coupons_discount_amount'])) {
          $OSCOM_MessageStack->add( ERROR_DISCOUNT_COUPONS_NO_AMOUNT, 'error' );
          $action = 'new';
        } else {


          $OSCOM_PDO->save('discount_coupons', [
                                                'coupons_id' => $coupons_id,
                                                'coupons_description' => $coupons_description,
                                                'coupons_discount_amount' => $coupons_discount_amount,
                                                'coupons_discount_type' => $coupons_discount_type,
                                                'coupons_max_use' => (!empty($coupons_max_use) ? (int)$coupons_max_use : 0),
                                                'coupons_min_order' => (!empty($coupons_min_order) ? $coupons_min_order : 0),
                                                'coupons_min_order_type' => $coupons_min_order_type,
                                                'coupons_number_available' => (!empty($coupons_number_available) ? (int)$coupons_number_available : 0),
                                                'coupons_create_account_b2b' => $coupons_create_account_b2b,
                                                'coupons_create_account_b2c' => $coupons_create_account_b2c,
                                                'coupons_twitter' => $coupons_twitter
                                               ]
          );

// Coupon end date update
          if (osc_not_null($coupons_date_end)) {

// for twitter
           $coupons_twitter_date_end = $coupons_date_end;

            list($day, $month, $year) = explode('/', $coupons_date_end);

            $coupons_date_end = $year .
                            ((strlen($month) == 1) ? '0' . $month : $month) .
                            ((strlen($day) == 1) ? '0' . $day : $day);

          }

            $Qupdate = $OSCOM_PDO->prepare('update :table_discount_coupons
                                            set coupons_date_end = :coupons_date_end
                                            where coupons_id = :coupons_id
                                          ');
            $Qupdate->bindValue(':coupons_date_end', $coupons_date_end);
            $Qupdate->bindValue(':coupons_id', $coupons_id);
            $Qupdate->execute();


// Coupon start date update
          if (osc_not_null($coupons_date_start)) {
            list($day, $month, $year) = explode('/', $coupons_date_start);

            $coupons_date_start = $year .
                            ((strlen($month) == 1) ? '0' . $month : $month) .
                            ((strlen($day) == 1) ? '0' . $day : $day);

            $Qupdate = $OSCOM_PDO->prepare('update :table_discount_coupons
                                            set coupons_date_start = :coupons_date_start
                                            where coupons_id = :coupons_id
                                          ');
            $Qupdate->bindValue(':coupons_date_start', $coupons_date_start);
            $Qupdate->bindValue(':coupons_id', $coupons_id);
            $Qupdate->execute();
          }

//Update the configuration coupon in configuration database
        if (COUPON_CUSTOMER == '') {
          if ($coupons_create_account_b2c == '1') {

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                            set configuration_value = :configuration_value 
                                            where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_value', $coupons_id);
            $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER' );
            $Qupdate->execute();

          }
        }

//Update the configuration coupon in configuration database
        if (COUPON_CUSTOMER_B2B == '') {
           if ($coupons_create_account_b2b == '1') {

              $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                              set configuration_value = :configuration_value 
                                              where configuration_key = :configuration_key
                                            ');
              $Qupdate->bindValue(':configuration_value', $coupons_id);
              $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER_B2B' );
              $Qupdate->execute();
            }
        }

// send email to twitter
        if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) { 
          if ($coupons_twitter == '1') {
            if (!empty($coupons_date_end)) $date_coupon_twitter_end = TEXT_DISCOUNT_DATE_END .  $coupons_twitter_date_end; 
            $text_discount =  TEXT_NEW_DISCOUNT_TWITTER . $coupons_id . ' : ' .$date_coupon_twitter_end . TEXT_TWITTER_AMOUNT . $coupons_discount_amount .' : ' . HTTP_CATALOG_SERVER;   
            $_POST['twitter_msg'] =  $text_discount;
            echo osc_send_twitter($twitter_authentificate_administrator); 
          }
        }


        osc_redirect( osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&cID=' . $coupons_id ) );
      }

	    break;

      case 'update':

        osc_db_query("update discount_coupons set coupons_discount_amount = '".$coupons_discount_amount."',
                                                coupons_discount_type = '".$coupons_discount_type."',
                                                coupons_description = '" .$coupons_description. "',
                                                coupons_max_use = '" .(!empty($coupons_max_use) ? (int)$coupons_max_use : 0 ). "',
                                                coupons_min_order = '".(!empty($coupons_min_order) ? $coupons_min_order : 0 )."',
                                                coupons_number_available = '" .(!empty($coupons_number_available) ? (int)$coupons_number_available : 0 ). "',
                                                coupons_discount_type = '".(!empty($coupons_discount_type) ? $coupons_discount_type : 'null' )."'
                      where coupons_id = '" . $coupons_id . "'
                     ");

// Coupon end date update
          if (osc_not_null($coupons_date_end)) {
            list($day, $month, $year) = explode('/', $coupons_date_end);

            $coupons_date_end = $year .
                            ((strlen($month) == 1) ? '0' . $month : $month) .
                            ((strlen($day) == 1) ? '0' . $day : $day);
          }

          $Qupdate = $OSCOM_PDO->prepare('update :table_discount_coupons
                                          set coupons_date_end = :coupons_date_end 
                                          where coupons_id = :coupons_id
                                        ');
          $Qupdate->bindValue(':coupons_date_end', osc_db_input($coupons_date_end) );
          $Qupdate->bindValue(':coupons_id', $coupons_id);
          $Qupdate->execute();


// Coupon start date update
          if (osc_not_null($coupons_date_start)) {
            list($day, $month, $year) = explode('/', $coupons_date_start);

            $coupons_date_start = $year .
                            ((strlen($month) == 1) ? '0' . $month : $month) .
                            ((strlen($day) == 1) ? '0' . $day : $day);

            $Qupdate = $OSCOM_PDO->prepare('update :table_discount_coupons
                                            set coupons_date_start = :coupons_date_start 
                                            where coupons_id = :coupons_id
                                          ');
            $Qupdate->bindValue(':coupons_date_start', osc_db_input($coupons_date_start) );
            $Qupdate->bindValue(':coupons_id', $coupons_id);
            $Qupdate->execute();
          }

// update the different coupon if there are another value
          $QcouponsTest = $OSCOM_PDO->prepare("select coupons_id, 
                                                coupons_create_account_b2b, 
                                                coupons_create_account_b2c 
                                               from :table_discount_coupons
                                               where  coupons_id = :coupons_id
                                              ");
          $QcouponsTest->bindValue(':coupons_id', $coupons_id );
          $QcouponsTest->execute();

          $coupons_test_query = $QcouponsTest->fetch();

         if ($coupons_create_account_b2c != $coupons_test_query['coupons_create_account_b2c']) {
            $Qupdate = $OSCOM_PDO->prepare('update :table_discount_coupons
                                            set coupons_create_account_b2c = :coupons_create_account_b2c
                                            where coupons_id = :coupons_id
                                          ');
            $Qupdate->bindValue(':coupons_create_account_b2c', $coupons_create_account_b2c);
            $Qupdate->bindValue(':coupons_id', $coupons_id);
            $Qupdate->execute();
         }

         if ($coupons_create_account_b2b != $coupons_test_query['coupons_create_account_b2b']) {
        
           $Qupdate = $OSCOM_PDO->prepare('update :table_discount_coupons
                                           set coupons_create_account_b2b = :coupons_create_account_b2b
                                           where coupons_id = :coupons_id
                                          ');
           $Qupdate->bindValue(':coupons_create_account_b2b', $coupons_create_account_b2b);
           $Qupdate->bindValue(':coupons_id', $coupons_id);
           $Qupdate->execute();
          }

//Update the configuration coupon in configuration database
        if ((COUPON_CUSTOMER == $coupons_id) || (COUPON_CUSTOMER == '')) {
           if ($coupons_create_account_b2c == '1') {

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                            set configuration_value = :configuration_value 
                                            where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER' );
            $Qupdate->bindValue(':configuration_value', $coupons_id);
            $Qupdate->execute();

           } else {

              $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                              set configuration_value = :configuration_value 
                                              where configuration_key = :configuration_key
                                            ');
              $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER' );
              $Qupdate->bindValue(':configuration_value', '');
              $Qupdate->execute();

          }
       }

        if ((COUPON_CUSTOMER_B2B == $coupons_id) || (COUPON_CUSTOMER_B2B == '')) {
           if ($coupons_create_account_b2b == '1') {

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                            set configuration_value = :configuration_value 
                                            where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER_B2B' );
            $Qupdate->bindValue(':configuration_value', $coupons_id);
            $Qupdate->execute();

           } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                            set configuration_value = :configuration_value 
                                            where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER_B2B' );
            $Qupdate->bindValue(':configuration_value', '');
            $Qupdate->execute();

          }
       }

        osc_redirect( osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&cID=' . $coupons_id ) );
        break;

      case 'deleteconfirm':

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons_to_orders
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();

//exclusions
        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons_to_categories
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons_to_manufacturers
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons_to_products
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons_to_customers
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();

        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_discount_coupons_to_zones
                                        where coupons_id = :coupons_id 
                                      ');
        $Qdelete->bindValue(':coupons_id', $coupons_id);
        $Qdelete->execute();
 //end exclusions

// suppression du numero de coupons B2B dans la table configurationn

        $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value 
                                        where configuration_key = :configuration_key
                                      ');
        $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER' );
        $Qupdate->bindValue(':configuration_value', '');
        $Qupdate->execute();

        $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                        set configuration_value = :configuration_value 
                                        where configuration_key = :configuration_key
                                      ');
        $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER_B2B' );
        $Qupdate->bindValue(':configuration_value', '');
        $Qupdate->execute();

        osc_redirect(osc_href_link('discount_coupons.php', 'page=' . $_GET['page']));

        break;

      case 'delete_all':

        if ($_POST['selected'] != '') {
          foreach ($_POST['selected'] as $coupons['coupons_id']) {

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons_to_orders
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

//exclusions
            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons_to_categories
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons_to_manufacturers
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons_to_products
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons_to_customers
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_discount_coupons_to_zones
                                            where coupons_id = :coupons_id 
                                          ');
            $Qdelete->bindValue(':coupons_id', $coupons['coupons_id']);
            $Qdelete->execute();

// suppression du numero de coupons B2B dans la table configuration
            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                            set configuration_value = :configuration_value 
                                            where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER' );
            $Qupdate->bindValue(':configuration_value', '');
            $Qupdate->execute();
      

            $Qupdate = $OSCOM_PDO->prepare('update :table_configuration
                                            set configuration_value = :configuration_value 
                                            where configuration_key = :configuration_key
                                          ');
            $Qupdate->bindValue(':configuration_key', 'COUPON_CUSTOMER_B2B' );
            $Qupdate->bindValue(':configuration_value', '');
            $Qupdate->execute();

          } 
        }

       osc_redirect(osc_href_link('discount_coupons.php'));
      break;
    }
  }

  require('includes/header.php');
?>

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
<?php
  if ( ($action == 'new') || ($action == 'edit') ) {
    $form_action = 'insert';
    if ( ($action == 'edit') && isset($_GET['cID']) ) {
      $form_action = 'update';

      $Qcoupons = $OSCOM_PDO->prepare('select coupons_id,
                                             coupons_description,
                                             coupons_discount_amount,
                                             coupons_discount_type,
                                             coupons_max_use,
                                             coupons_min_order,
                                             coupons_min_order_type,
                                             coupons_number_available,
                                             coupons_create_account_b2b,
                                             coupons_create_account_b2c,
                                             date_format(coupons_date_start, "%d/%m/%Y") as coupons_date_start,
                                             date_format(coupons_date_end, "%d/%m/%Y") as coupons_date_end,
                                             coupons_twitter
                                     from :table_discount_coupons
                                     where coupons_id = :coupons_id
                                    ');
      $Qcoupons->bindValue(':coupons_id',  $coupons_id);
      $Qcoupons->execute();

      $coupons = $Qcoupons->fetch();

      $cInfo = new objectInfo($coupons);
    } else {
      $cInfo = new objectInfo(array());
    }
?>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

       <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
              <div class="adminTitle">
                <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/coupon.gif', HEADING_TITLE, '40', '40'); ?></span>
                <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
                <form name="new_coupon" action="<?php echo osc_href_link('discount_coupons.php', osc_get_all_get_params(array('action', 'info', 'cID')) . 'action=' . $form_action) . '"'; ?> method="post">
<?php
// delete button if the Discount module is not activated
     if (MODULE_ORDER_TOTAL_DISCOUNT_COUPON_RANDOM_CODE_LENGTH  != '') {
       if (($action == 'insert') || ($action == 'new')) {
?>
                <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_INSERT); ?>&nbsp;</span>
                <span class="pull-right"><a href="<?php echo osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . (isset($_GET['cID']) ? '&cID=' . $_GET['cID'] : '')); ?>"><?php echo osc_image_button('button_cancel.gif', IMAGE_CANCEL); ?></a>&nbsp;</span>
<?php
      echo osc_hide_session_id();
    }
?>
                </span>
                <span class="pull-right">
<?php
    if (($action == 'update') || ($action == 'edit')) {
      echo osc_draw_hidden_field('coupons_id', $_GET['cID']);
?>
                  <span class="pull-right"><?php echo osc_image_submit('button_update.gif', IMAGE_UPDATE); ?></span>
                  <span class="pull-right"><a href="<?php echo osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . (isset($_GET['cID']) ? '&cID=' . $_GET['cID'] : '')); ?>"><?php echo osc_image_button('button_cancel.gif', IMAGE_CANCEL); ?></a>&nbsp;</span>
      <?php
      echo osc_hide_session_id();
    }
  }
?>
                </span>
              </div>
              <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
            <div class="clearfix"></div>
           </tr>
          <tr>
            <td>
<?php
//Display a message if the Discount module is not activated
  if (MODULE_ORDER_TOTAL_DISCOUNT_COUPON_RANDOM_CODE_LENGTH != '') {
?>
<!-- ------------------------------------------------------------ //-->
<!--          ONGLET Information General du Coupon               //-->
<!-- ------------------------------------------------------------ //-->
              <div>
                <ul class="nav nav-tabs" role="tablist"  id="myTab">
                  <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
                </ul>
                <div class="tabsClicShopping">
                  <div class="tab-content">
                       <table width="100%" border="0" cellspacing="0" cellpadding="5">
                          <tr>
                            <td class="mainTitle"><?php echo TITLE_DISCOUNT_COUPONS; ?></td>
                          </tr>
                       </table>
                       <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                         <tr>
                           <td><table border="0" cellpadding="2" cellspacing="2">
                             <tr>
                               <td class="main" align="left"><?php echo TEXT_DISCOUNT_COUPONS_ID; ?>&nbsp;</td>
                               <td class="main"><?php echo osc_draw_input_field('coupons_id',$cInfo->coupons_id, 'required aria-required="true" id="coupon_title" size="10" maxlength="32"'.( $action == 'edit' ? ' disabled' : '')); ?></td>
                             </tr>
                             <tr>
                               <td class="main" align="left"><?php echo TEXT_DISCOUNT_COUPONS_DESCRIPTION; ?>&nbsp;</td>
                               <td class="main"><?php echo osc_draw_input_field('coupons_description',$cInfo->coupons_description, 'size="25" maxlength="64"'); ?></td>
                             </tr>
                             <tr>
                               <td class="main" align="left" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_CREATE_ACCOUNT; ?>&nbsp;</td>
                               <td class="main"><?php echo osc_draw_checkbox_field('coupons_create_account_b2c', '1', $cInfo->coupons_create_account_b2c); ?></td>
                             </tr>
<?php
 if ($coupons_b2c_count['coupons_b2c_count'] > 0) {
?>
                              <tr>
                                <td class="main" align="right" valign="top">&nbsp;</td>
                                <td class="main"><?php echo TEXT_DISCOUNT_COUPONS_WARNING; ?></td>
                              </tr>
<?php
  }
  if (MODE_B2B_B2C == 'true') {
?>
                              <tr>
                                <td class="main" align="left"><?php echo TEXT_DISCOUNT_CUSTOMERS_GROUP; ?>&nbsp;</td>
                                <td class="main"><?php echo osc_draw_checkbox_field('coupons_create_account_b2b', '1', $cInfo->coupons_create_account_b2b); ?></td>
                              </tr>
<?php
    if ($coupons_b2b_count['coupons_b2b_count'] > 0) {
?>
                              <tr>
                                <td class="main" align="right" valign="top">&nbsp;</td>
                                <td class="main"><?php echo TEXT_DISCOUNT_COUPONS_WARNING; ?></td>
                              </tr>
<?php
    }
  }
 if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
   if( $action != 'edit') {
?>
                              <tr>
                                <td class="main" align="left"><?php echo TEXT_DISCOUNT_TWITTER; ?>&nbsp;</td>
                                <td class="main"><?php echo osc_draw_checkbox_field('coupons_twitter', '1', $cInfo->coupons_twitter); ?></td>
                              </tr>
<?php
    }
  }
?>
                            </table></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                          <tr>
                            <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                          </tr>
                          <tr>
                            <td class="mainTitle"><?php echo TITLE_DISCOUNT_AMOUNT; ?></td>
                          </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                          <tr>
                            <td><table border="0" cellpadding="2" cellspacing="2">
                              <tr>
                                <td class="main" align="left" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_AMOUNT; ?>&nbsp;</td>
                                <td class="main">
<?php
  echo osc_draw_input_field('coupons_discount_amount', $cInfo->coupons_discount_amount, 'required aria-required="true" id="coupon_amount" maxlength="10"');
  echo '<small>'.TEXT_INFO_DISCOUNT_AMOUNT_HINT.'</small>';
?>
                                </td>
                              </tr>
                              <tr>
                                <td class="main" align="left" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_TYPE; ?></td>
                                <td><?php echo kgt_draw_type_drop_down( 'discount', 'coupons_discount_type', $cInfo->coupons_discount_type ); ?></td>
                              </tr>
                            </table></td>
                           </tr>
                         </table>
                         <table width="100%" border="0" cellspacing="0" cellpadding="5">
                           <tr>
                             <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                           </tr>
                           <tr>
                             <td class="mainTitle"><?php echo TITLE_DISCOUNT_DATE; ?></td>
                           </tr>
                         </table>
                         <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                           <tr>
                             <td><table border="0" cellpadding="2" cellspacing="2">
                               <tr>
                                 <td class="main" align="left" valign="top"><?php echo TEXT_DISCOUNT_COUPONS_DATE_START; ?></td>
                                 <td class="main"><?php echo osc_draw_input_field('coupons_date_start', $cInfo->coupons_date_start, 'id="schdate"'); ?></td>
                               </tr>
                               <tr>
                                 <td class="main" align="left" valign="top"><?php echo TEXT_DISCOUNT_COUPONS_DATE_END; ?></td>
                                 <td class="main"><?php echo osc_draw_input_field('coupons_date_end', $cInfo->coupons_date_end, 'id="expdate"'); ?></td>
                               </tr>
                             </table></td>
                           </tr>
                         </table>
                         <table width="100%" border="0" cellspacing="0" cellpadding="5">
                           <tr>
                             <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                           </tr>
                           <tr>
                             <td class="mainTitle"><?php echo TITLE_DISCOUNT_ORDER; ?></td>
                           </tr>
                         </table>
                         <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                           <tr>
                             <td><table border="0" cellpadding="2" cellspacing="2">
                               <tr>
                                 <td class="main" align="left" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_MIN_ORDER; ?></td>
                                 <td><?php  echo osc_draw_input_field('coupons_min_order', $cInfo->coupons_min_order, 'size="5" maxlength="5"'); ?></td>
                               </tr>
                               <tr>
                                 <td class="main" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_MIN_ORDER_TYPE; ?></td>
                                 <td><?php echo kgt_draw_type_drop_down( 'min_order', 'coupons_min_order_type', $cInfo->coupons_min_order_type );?></td>
                               </tr>
                             </table></td>
                            </tr>
                         </table>
                         <table width="100%" border="0" cellspacing="0" cellpadding="5">
                           <tr>
                             <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                           </tr>
                           <tr>
                             <td class="mainTitle"><?php echo TITLE_DISCOUNT_USE; ?></td>
                           </tr>
                           </table>
                           <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                             <tr>
                               <td><table border="0" cellpadding="2" cellspacing="2">
                                 <tr>
                                   <td class="main" align="left" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_MAX_USE; ?>&nbsp;</td>
                                   <td class="main"><?php echo osc_draw_input_field('coupons_max_use', $cInfo->coupons_max_use, 'size="5" maxlength="5"'); ?></td>
                                 </tr>
                                 <tr>
                                   <td class="main" align="left" valign="middle"><?php echo TEXT_DISCOUNT_COUPONS_NUMBER_AVAILABLE; ?>&nbsp;</td>
                                   <td class="main"><?php echo osc_draw_input_field('coupons_number_available', $cInfo->coupons_number_available, 'size="5" maxlength="5"'); ?></td>
                                 </tr>
                               </table></td>
                             </tr>
                           </table>
                           <table width="100%" border="0" cellspacing="0" cellpadding="5">
                             <tr>
                               <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                             </tr>
                           </table>
                           <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
                             <tr>
                               <td><table border="0" cellpadding="2" cellspacing="2">
                                 <tr>
                                   <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_DISCOUNT_IMAGE); ?></td>
                                   <td class="main"><strong><?php echo '&nbsp;' . TITLE_HELP_DISCOUNT_PRICE; ?></strong></td>
                                 </tr>
                                 <tr>
                                   <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                                   <td class="main"><?php echo TEXT_HELP_DISCOUNT_PRICE; ?></td>
                                 </tr>
                               </table></td>
                             </tr>
                           </table></td>
                         </tr>
                       </table></form>
                   </div>
                 </div>
               </div>
  <?php
   } else {
?>
             <tr>
              <td class="main" align="center"><?php echo ACTIVATE_MODULE_DISCOUNT_COUPON; ?></td>
             </tr>
<?php
  }
?>
            </td>
          </tr>	
<?php
  } else {
   require('includes/classes/currencies.php');
    $currencies = new currencies();
?>
<!-- //################################################################################################-->
<!-- //                                      Page Principale	                                       -->
<!-- //################################################################################################ -->
          <tr>
            <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
            <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
                  <tr>
                    <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/coupon.gif', HEADING_TITLE, '40', '40'); ?></td>
                    <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
<?php
  if (empty($action)) {
?>
                    <td align="right">
                       <form name="delete_all" <?php echo 'action="' . osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
                       &nbsp;<a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
                       <?php  echo '<a href="' . osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_coupon.gif', IMAGE_NEW_COUPON) . '</a>'; ?>
                   </td>
<?php
  }
?>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
                      <tr class="dataTableHeadingRow">
                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_COUPONS_ID; ?></td>
<?php
// Permettre l'affichage de la colonne si mode B2B2 active
    if (MODE_B2B_B2C == 'true') {
?>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_CUSTOMER_GROUP; ?></td>
<?php
  }
?>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_AMOUNT; ?></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_CREATE_ACCOUNT; ?></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_DATE_START; ?></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_DATE_END; ?></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_MAX_USE; ?></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_MIN_ORDER; ?></td>
                        <td class="dataTableHeadingContent" align="center"><?php echo TEXT_HEADING_DISCOUNT_NUMBER_AVAILABLE; ?></td>
                        <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                      </tr>
                      <tr>
<?php
    $coupons_query_raw = "select * 
                          from discount_coupons cd
                          order by cd.coupons_date_end, 
                                   cd.coupons_date_start
                         ";
    $coupons_split = new splitPageResults( $_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $coupons_query_raw, $coupons_query_numrows );
    $coupons_query = osc_db_query($coupons_query_raw);

    while( $coupons = osc_db_fetch_array( $coupons_query ) ) {
      if( ( !isset( $_GET['cID'] ) || ( isset( $_GET['cID'] ) && ( $_GET['cID'] == $coupons['coupons_id'] ) ) ) && !isset( $cInfo ) && ( substr( $action, 0, 3 ) != 'new' ) ) {
        $cInfo = new objectInfo($coupons);
      }

      if (isset($cInfo) && is_object($cInfo) && ($coupons['coupons_id'] == $cInfo->coupons_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
/*
      if (isset($cInfo) && is_object($cInfo) && ($coupons['coupons_id'] == $cInfo->coupons_id) ) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->coupons_id . '&action=edit') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&cID=' . $coupons['coupons_id']) . '\'">' . "\n";
      }
*/
?>
                      <td>
<?php 
      if ($coupon['selected']) { 
?>
                        <input type="checkbox" name="selected[]" value="<?php echo $coupons['coupons_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                        <input type="checkbox" name="selected[]" value="<?php echo $coupons['coupons_id']; ?>" />
<?php 
      } 
?>
                      </td>
                      <td class="dataTableContent" align="left"><?php echo $coupons['coupons_id'].' <small>'.( !empty( $coupons['coupons_description'] ) ? '( '.$coupons['coupons_description'].' )' : '' ) .'</small>'; ?></td>

<?php
// Permettre l'affichage des couleurs des groupes en mode B2B
  if (MODE_B2B_B2C == 'true') {
// If a customer B2B code is sending when the customer B2b cretae an account
//    if (($coupons['coupons_create_account_b2b'] == '1') and (COUPON_CUSTOMER_B2B != '')) {
    if ($coupons['coupons_create_account_b2b'] == '1') {
       $coupon_create_account_send_b2b = 'X';
    } else {
       $coupon_create_account_send_b2b = '';
    }
?>
                      <td class="dataTableContent" align="center"><?php echo $coupon_create_account_send_b2b; ?></td>
<?php
  }
?>
                      <td class="dataTableContent" align="center">
<?php 
    switch( $coupons['coupons_discount_type'] ) {

      case 'percent':
        echo ( $coupons['coupons_discount_amount'] * 100 ).'%';
      break;
      case 'fixed':
        echo $currencies->format( $coupons['coupons_discount_amount'] );
      break;
    }


// If a account code is sending when the customer cretae an account
//    if (($coupons['coupons_create_account_b2c'] =='1') and (COUPON_CUSTOMER != '')) {
    if ($coupons['coupons_create_account_b2c'] =='1') {
       $coupon_create_account_send_b2c = 'X';
    } else {
       $coupon_create_account_send_b2c = '';
    }
?>
                      </td>
                      <td class="dataTableContent" align="center"><?php echo $coupon_create_account_send_b2c; ?></td>
                      <td class="dataTableContent" align="center"><?php echo !empty( $coupons['coupons_date_start'] ) ? $OSCOM_Date->getShort( $coupons['coupons_date_start'] ) : TEXT_DISPLAY_UNLIMITED; ?></td>
                      <td class="dataTableContent" align="center"><?php echo !empty( $coupons['coupons_date_end'] ) ? $OSCOM_Date->getShort( $coupons['coupons_date_end'] ) : TEXT_DISPLAY_UNLIMITED; ?></td>
                      <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_max_use'] != 0 ? $coupons['coupons_max_use'] : TEXT_DISPLAY_UNLIMITED ); ?></td>
                      <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_min_order'] != 0 ? ( $coupons['coupons_min_order_type'] == 'price' ? $currencies->format( $coupons['coupons_min_order'] ) : (int)$coupons['coupons_min_order'] ) : TEXT_DISPLAY_UNLIMITED ); ?></td>
                      <td class="dataTableContent" align="center"><?php echo ( $coupons['coupons_number_available'] != 0 ? $coupons['coupons_number_available'] : TEXT_DISPLAY_UNLIMITED ); ?></td>
                      <td class="dataTableContent" align="right">&nbsp;
<?php 

    echo '<a href="' . osc_href_link('discount_coupons.php', osc_get_all_get_params(array('cID', 'action')) . 'cID=' . $coupons['coupons_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT_ORDER) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('discount_coupons_exclusions.php', 'cID='.$coupons['coupons_id'].'&type=products') . '">' .  osc_image(DIR_WS_ICONS . 'remove_product.png', IMAGE_PRODUCT_EXCLUSIONS) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('discount_coupons_exclusions.php', 'cID='.$coupons['coupons_id'].'&type=manufacturers') . '">' . osc_image(DIR_WS_ICONS . 'remove_manufacturer.png', IMAGE_MANUFACTURER_EXCLUSIONS). '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('discount_coupons_exclusions.php', 'cID='.$coupons['coupons_id'].'&type=categories') . '">' . osc_image(DIR_WS_ICONS . 'remove_categories.png', IMAGE_CATEGORY_EXCLUSIONS) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');
    echo '<a href="' . osc_href_link('discount_coupons_exclusions.php', 'cID='.$coupons['coupons_id'].'&type=customers') . '">' . osc_image(DIR_WS_ICONS . 'remove_customers.png', IMAGE_CUSTOMER_EXCLUSIONS) . '</a>';
    echo osc_draw_separator('pixel_trans.gif', '6', '16');

    if( isset( $cInfo ) && is_object( $cInfo ) && ( $coupons['coupons_id'] == $cInfo->coupons_id ) ) { 
      echo osc_image( DIR_WS_IMAGES.'icon_arrow_right.gif', '' ); 
    } else { 
      echo '<a href="'.osc_href_link( 'discount_coupons.php', 'page='.$_GET['page'].'&cID='.$coupons['coupons_id'] ).'">' . osc_image( DIR_WS_IMAGES.'icon_info.gif', IMAGE_ICON_INFO ).'</a>';
    } 
?>
                        </td>
                      </tr>

<?php
    }
?>
                      <tr>
                        <td colspan="11"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                          <tr>
                            <td class="smallText" align="right"><?php echo $coupons_split->display_links($coupons_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                          </tr>
                        </table></td>
                      </tr>
                    </table></td>
                  </form>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_DISCOUNT_COUPONS . '</strong>');

      $contents = array('form' => osc_draw_form('coupons', 'discount_coupons.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->coupons_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $cInfo->coupons_id . '</strong>');
      $contents[] = array('align' => 'center', 'text' => '<br />' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '&nbsp;<a href="' . osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->coupons_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    default:
    break;
  }
  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
}
?>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
