<?php
/*
 * google_sitemap_specials.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $special_array = array();

    $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                     coalesce(NULLIF(specials_last_modified, :specials_last_modified),
                                                     specials_date_added) as last_modified
                                      from :table_specials
                                      where status = :status
                                      and customers_group_id = :customers_group_id
                                      order by last_modified DESC
                                      ');

    $Qproducts->bindValue(':specials_last_modified', '');
    $Qproducts->bindValue(':status', '1');
    $Qproducts->bindValue(':customers_group_id', '0');
    $Qproducts->execute();


    while ($specials = $Qproducts->fetch() ) {

      $special_array[$specials['products_id']]['loc'] = osc_href_link('product_info.php', 'products_id=' . (int)$specials['products_id'], 'NONSSL', false);
      $special_array[$specials['products_id']]['lastmod'] = $specials['last_modified'];
      $special_array[$specials['products_id']]['changefreq'] = 'weekly';
      $special_array[$specials['products_id']]['priority'] = '0.5';
    }

    foreach ($special_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }
