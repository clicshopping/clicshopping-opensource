<?php
/*
 * checkout_confirmation.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('checkout_confirmation') ) {
    echo $OSCOM_MessageStack->get('checkout_confirmation');
  }

  if (isset($$_SESSION['payment']->form_action_url)) {
    $form_action_url = $$_SESSION['payment']->form_action_url;
  } else {
    $form_action_url = osc_href_link('checkout_process.php', '', 'SSL');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

  echo osc_draw_form('checkout_confirmation', $form_action_url, 'post', 'onsubmit="return checkCheckBox(this)"');

// ---------------------- 
// --- Shipping information   -----  
// ---------------------- 
?>

<div class="contentContainer">
  <legend><?php echo HEADING_SHIPPING_INFORMATION; ?></legend>

  <div class="contentText">
    <table border="0" width="100%" cellspacing="1" cellpadding="2">
      <tr>

<?php
  if ($_SESSION['sendto'] != false) {
?>

            <td width="30%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main"><?php echo '<strong>' . HEADING_DELIVERY_ADDRESS . '</strong> <a href="' . osc_href_link('checkout_shipping_address.php', '', 'SSL') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo osc_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />'); ?></td>
              </tr>

<?php
    if ($order->info['shipping_method']) {
?>

              <tr>
                <td class="main"><?php echo '<strong>' . HEADING_SHIPPING_METHOD . '</strong> <a href="' . osc_href_link('checkout_shipping.php', '', 'SSL') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo $order->info['shipping_method']; ?></td>
              </tr>
<?php
    }
?>

        </table></td>

<?php
  }
?>

            <td width="<?php echo (($_SESSION['sendto'] != false) ? '70%' : '100%'); ?>" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">

<?php
  if (sizeof($order->info['tax_groups']) > 1) {
?>

                  <tr>
                    <td class="main" colspan="2"><?php echo '<strong>' . HEADING_PRODUCTS . '</strong> <a href="' . osc_href_link('shopping_cart.php') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
                    <td class="smallText" align="right"><strong><?php echo HEADING_TAX; ?></strong></td>
                    <td class="smallText" align="right"><strong><?php echo HEADING_TOTAL; ?></strong></td>
                  </tr>

<?php
  } else {
?>

                  <tr>
                    <td class="main" colspan="3"><?php echo '<strong>' . HEADING_PRODUCTS . '</strong> <a href="' . osc_href_link('shopping_cart.php') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
                  </tr>

<?php
  }

  for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
    echo '          <tr>' . "\n" .
         '            <td class="main" align="right" valign="top" width="30">' . $order->products[$i]['qty'] . '&nbsp;x</td>' . "\n" .
         '            <td class="main" valign="top">' . $order->products[$i]['name'];

    if (STOCK_CHECK == 'true') {
      echo osc_check_stock($order->products[$i]['id'], $order->products[$i]['qty']);
    }

    if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        echo '<br /><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . '</i></small></nobr>';
      }
    }

    echo '</td>' . "\n";

    if (sizeof($order->info['tax_groups']) > 1) echo '            <td class="main" valign="top" align="right">' . osc_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n";

    echo '            <td class="main" align="right" valign="top">' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . '</td>' . "\n" .
         '          </tr>' . "\n";
  }
?>

        </table></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:10px;"></div>
  <div class="hr"></div>
<?php
// ------------------------------
// --- billing information   -----
// -------------------------------
?>
  <h3><?php echo HEADING_BILLING_INFORMATION; ?></h3>

  <div class="contentText">
    <table border="0" width="100%" cellspacing="1" cellpadding="2">
      <tr>
        <td width="30%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
<?php
// Controle autorisation au client de modifier son adresse par defaut
  if (osc_count_customers_modify_address_default() == 1) {
?>
            <td class="main"><?php echo '<strong>' . HEADING_BILLING_ADDRESS . '</strong> <a href="' . osc_href_link('checkout_payment_address.php', '', 'SSL') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
          </tr>
<?php
  } else {
?>
              <td class="main"><?php echo '<strong>' . HEADING_BILLING_ADDRESS . '</strong>'; ?></td>
            </tr>
<?php
  }
?>
             <tr>
               <td class="main"><?php echo osc_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></td>
             </tr>
             <tr>
               <td class="main"><?php echo '<strong>' . HEADING_PAYMENT_METHOD . '</strong> <a href="' . osc_href_link('checkout_payment.php', '', 'SSL') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
             </tr>
             <tr>
               <td class="main"><?php echo $order->info['payment_method']; ?></td>
             </tr>
            </table></td>
            <td width="70%" valign="top" align="right"><table border="0" cellspacing="0" cellpadding="2">
<?php
  if (MODULE_ORDER_TOTAL_INSTALLED) {
    echo $order_total_modules->output();
  }
?>

        </table></td>
      </tr>
    </table>
  </div>
  <div style="padding-top:10px;"></div>
  <div class="hr"></div>
<?php
// ------------------------------
// --- Heading payment information   -----
// -------------------------------
?>
<?php
  if (is_array($payment_modules->modules)) {
// Voir ligne 114 /boutique/checkout_confirmation
//    if ($confirmation = $_SESSION['payment']_modules->confirmation()) {
    if ($confirmation) {
?>

  <h3><?php echo HEADING_PAYMENT_INFORMATION; ?></h3>

  <div class="contentText">
    <table border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td colspan="4"><?php echo $confirmation['title']; ?></td>
      </tr>

<?php
    if (isset($confirmation['fields'])) {
      for ($i=0, $n=sizeof($confirmation['fields']); $i<$n; $i++) {
?>
      <tr>
        <td width="10"><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
        <td class="main"><?php echo $confirmation['fields'][$i]['title']; ?></td>
        <td width="10"><?php echo osc_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
        <td class="main"><?php echo $confirmation['fields'][$i]['field']; ?></td>
      </tr>
<?php
      }
    }
?>

    </table>
  </div>
  <div class="clearfix"></div>

<?php
    }
  }

  if (osc_not_null($order->info['comments'])) {
?>

  <h3><?php echo '<strong>' . HEADING_ORDER_COMMENTS . '</strong> <a href="' . osc_href_link('checkout_payment.php', '', 'SSL') . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></h3>
<?php
// ------------------------------
// --- Comment   -----
// -------------------------------
?>
  <div class="contentText">
    <?php echo nl2br(osc_output_string_protected($order->info['comments'])) . osc_draw_hidden_field('comments', $order->info['comments']); ?>
  </div>

<?php
  }
?>
<?php
// ------------------------------
// ---Payment conformation   -----
// -------------------------------
?>
   <div>
<?php
// Do not change this part above

    echo osc_draw_form('checkout_confirmation', $form_action_url, 'post');

// Payment Process - Don't change

  if (is_array($payment_modules->modules)) {
     echo $payment_modules->process_button();
  }

// Atos - Don't change
  // Delete the button if atos module is installed
  if ( substr($_SESSION['payment'], 0, 4) != 'atos' ) {
    if (CONFIGURATION_LAW_HAMON == "true") {
?>
   <SCRIPT language=JavaScript>
     function checkCheckBox(f){
       if (f.agree.checked == false )
       {
         alert('<?php echo TEXT_CONDITION_CONFIRMATION_ERROR_AGREEMENT; ?>');
         return false;
       }else
         return true;
     }
   </SCRIPT>
     <div class="contentText" style="padding-top: 15px;">
       <div class="pull-left">
         <div class="checkbox">
           <label>
              <span><?php echo  osc_draw_checkbox_field('agree', '1', false, 'id="agree"'); ?></span>
              <span><br /><label for="agree">&nbsp;&nbsp;<?php echo TEXT_CONDITIONS_CONFIRMATION; ?></label></span>
           </label>
         </div>
       </div>
     </div>
     <div class="clearfix"></div>
<?php
    } // end CONFIGURATION_LAW_HAMON
?>
     <div class="contentText pull-right" style="padding-top:10px;">
<?php echo osc_draw_button(sprintf(IMAGE_BUTTON_PAY_TOTAL_NOW, $currencies->format($order->info['total'], true, $order->info['currency'], $order->info['currency_value'])), null, null, 'success', array('params' => 'data-button="payNow"'));
  } // end atos

?>
   </div>
  </div>
 </div>
  <script type="text/javascript">
    $('form[name="checkout_confirmation"]').submit(function() {
        $('button[data-button="payNow"]').button('option', { label: '<?php echo addslashes(IMAGE_BUTTON_PAY_TOTAL_PROCESSING); ?>', disabled: true });
    });
  </script>
</form>
<div class="contentContainer"><?php echo $OSCOM_Template->getBlocks('modules_checkout_confirmation'); ?></div>
<?php
  require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>
