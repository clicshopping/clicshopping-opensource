<?php
/*
 * product_reviews_write.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('product_reviews_write'));

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  if (!isset($_GET['products_id'])) {
    osc_redirect(osc_href_link('product_reviews.php', osc_get_all_get_params(array('action'))));
  }

  $Qproducts = $OSCOM_PDO->prepare('select products_id,
                                           products_image
                                    from :table_products
                                    where products_id = :products_id
                                    and products_status = :products_status
                                    and products_view = :products_view
                                  ');
  $Qproducts->bindInt(':products_id', $OSCOM_Products->getID());
  $Qproducts->bindInt(':products_status', 1);
  $Qproducts->bindInt(':products_view', 1);

  $Qproducts->execute();


// Redirige le navigateur vers la page 'product_reviews.php' si aucune informations est disponnible dans la base de donnees
  if ($Qproducts->fetch() === false) {
    osc_redirect(osc_href_link('product_reviews.php', osc_get_all_get_params(array('action'))));
  }

  $Qcustomer = $OSCOM_PDO->prepare('select customers_firstname
                                    from :table_customers
                                    where customers_id = :customers_id
                                    ');
   
  $Qcustomer->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );   

  $Qcustomer->execute();

  $customer = $Qcustomer->fetch();


  if (isset($_GET['action']) && ($_GET['action'] == 'process')  && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $rating = osc_db_prepare_input($_POST['rating']);
    $review = osc_db_prepare_input($_POST['review']);

    $error = false;
    if (strlen($review) < REVIEW_TEXT_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('review', JS_REVIEW_TEXT);
    }

    if (($rating < 1) || ($rating > 5)) {
      $error = true;

      $OSCOM_MessageStack->addError('review', JS_REVIEW_RATING);
    }

    if ($error == false) {
      osc_db_query("insert into reviews (products_id,
                                         customers_id,
                                         customers_name,
                                         reviews_rating,
                                         date_added,
                                         status)
                    values ('" . (int)$OSCOM_Products->getId() . "',
                            '" . (int)$OSCOM_Customer->getID() . "', 
                            '" . osc_db_input($customer['customers_firstname']) ."', 
                            '" . osc_db_input($rating) . "', 
                            now(),
                            '0'
                           )
                  ");

      $insert_id = osc_db_insert_id();

      osc_db_query("insert into reviews_description (reviews_id,
                                                   languages_id,
                                                   reviews_text)
                   values ( '" . (int)$insert_id . "', 
                            '" . (int)$_SESSION['languages_id'] . "', 
                            '" . osc_db_input($review) . "')
                  ");

      if (REVIEW_COMMENT_SEND_EMAIL == 'true') {
        $email_subject = EMAIL_SUBJECT;
        $email_text = EMAIL_TEXT;

// Envoi du mail avec gestion des images pour Fckeditor et Imanager.
        $mimemessage = new email(array('X-Mailer: ClicShopping'));
        $message = html_entity_decode($email_text);
        $mimemessage->add_html_fckeditor($message);
        $mimemessage->build_message();
        $from = STORE_OWNER_EMAIL_ADDRESS;
        $mimemessage->send($name, $email_address, '', $from, $email_subject);

// e-mail de notification a l'administrateur
        $email_subject = html_entity_decode($email_subject);
        $email_text = html_entity_decode($email_text);

        osc_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $email_text, $name, STORE_OWNER_EMAIL_ADDRESS, '');
      }

      osc_redirect(osc_href_link('product_reviews.php', osc_get_all_get_params(array('action'))));
    } // end review comment
  }


//  Affichage du nom et reference du produit
  if (osc_not_null($OSCOM_Products->getProductsModel())) {
    $products_name = $OSCOM_Products->getProductsName() . '<br /><span class="smallText">[' . $OSCOM_Products->getProductsModel() . ']</span>';
  } else {
    $products_name = $OSCOM_Products->getProductsName();
  }

// Products attributes
  if (osc_has_product_attributes($OSCOM_Products->getId() ) > 1 ) {
    $osc_has_product_attributes = osc_has_product_attributes($OSCOM_Products->getId() );
  }

// Minimum quantity to take an order
  if ($OSCOM_Products->getProductsMinimumQuantityToTakeAnOrder() > 1) {
    $min_order_quantity_products_display = MIN_QTY_ORDER_PRODUCT .' ' . $OSCOM_Products->getProductsMinimumQuantityToTakeAnOrder();
  }
// display the differents prices before button
  $product_price = $OSCOM_Products->getCustomersPrice();
// display a message in function the customer group applied - before submit button
  if ($OSCOM_Products->getProductsMinimumQuantity() != '0' && $OSCOM_Products->getProductsQuantity() != '0') {
    $submit_button_view = $OSCOM_Products->getProductsAllowingTakeAnOrderMessage();
  }
// display buy button
    $buy_button = osc_draw_button(BUTTON_IN_CART, '', null, 'success', null, null);
// display the differents buttons before minorder qty
  if ($OSCOM_Products->getProductsMinimumQuantity() != '0' && $OSCOM_Products->getProductsQuantity() != '0') {
    $submit_button = $OSCOM_Products->getProductsBuyButton() ;
  }
// Display an input allowing for the customer to insert a quantity
  if ($OSCOM_Products->getProductsAllowingToInsertQuantity() !='' ) {
    $input_quantity =  CUSTOMER_QUANTITY . ' ' . $OSCOM_Products->getProductsAllowingToInsertQuantity();
  }

// Quantity type
  if ($OSCOM_Customer->getCustomersGroupID() == '0') {
    if (osc_not_null( $OSCOM_Products->getProductQuantityUnitType() )) {
      $products_quantity_unit = $OSCOM_Products->getProductQuantityUnitType();
    }
  } else {
    $products_quantity_unit = $OSCOM_Products->getProductQuantityUnitTypeCustomersGroup();
  }
// Display an information if the stock is exhausted for all groups
  if ($OSCOM_Products->getProductsExhausted() != '') {
    $submit_button = $OSCOM_Products->getProductsExhausted();
    $min_quantity = 0;
    $input_quantity ='';
    $min_order_quantity_products_display = '';
  }

// 9 - See the button more wview details
      $button_small_view_details = osc_draw_button(SMALL_IMAGE_BUTTON_DETAILS, '', osc_href_link('product_info.php', 'products_id='.(int)$OSCOM_Products->getId() ), 'info', null,'small');

// Gestion de l'affichage des images et des zooms
   if ($Qproducts->value('products_image') != '') {
     $products_image = '<h3>' . osc_image(DIR_WS_IMAGES . $Qproducts->value('products_image'), $OSCOM_Products->getProductsName(), SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'hspace="5" vspace="5"') . '</h3>';
   }


// Doit etre a la fin du fichier
// archive management
    if ($OSCOM_Products->getProductsArchive() == '1' &&  $products['products_id'] == (int)$OSCOM_Products->getId()) {
        $products_price = '';
        $products_not_sell = PRODUCTS_NOT_SELL;
    }

    $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('product_reviews.php', osc_get_all_get_params()));

    require($OSCOM_Template->getTemplateFiles('product_reviews_write'));
