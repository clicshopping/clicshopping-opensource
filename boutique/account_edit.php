<?php
/*
 * account_edit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_edit'));

  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {

      if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
        $gender = isset($_POST['gender']) ? trim($_POST['gender']) : null;
      }

      $firstname = osc_db_prepare_input($_POST['firstname']);
      $lastname = isset($_POST['lastname']) ? trim($_POST['lastname']) : null;

      if (((ACCOUNT_DOB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_DOB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
        $dob = isset($_POST['dob']) ? trim($_POST['dob']) : null;
      }

      $email_address = osc_db_prepare_input($_POST['email_address']);
      $telephone = isset($_POST['telephone']) ? trim($_POST['telephone']) : null;

      if (((ACCOUNT_CELLULAR_PHONE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_CELLULAR_PHONE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
       $cellular_phone = isset($_POST['cellular_phone']) ? trim($_POST['cellular_phone']) : null;
      }
    
      if (((ACCOUNT_FAX == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_FAX_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {   
        $fax = isset($_POST['fax']) ? trim($_POST['fax']) : null;
      }
// Clients en mode B2B : Informations societe
      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        if (ACCOUNT_COMPANY_PRO == 'true') $company = isset($_POST['company']) ? trim($_POST['company']) : null;
        if (ACCOUNT_SIRET_PRO == 'true') $siret = isset($_POST['siret']) ? trim($_POST['siret']) : null;
        if (ACCOUNT_APE_PRO == 'true') $ape = isset($_POST['ape']) ? trim($_POST['ape']) : null;
        if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $tva_intracom = isset($_POST['tva_intracom']) ? trim($_POST['tva_intracom']) : null;
        if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $iso = isset($_POST['iso']) ? trim($_POST['iso']) : null;
      }

      $error = false;

// Clients B2C et B2B : Controle selection de la civilite
      if ((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
        if ( ($gender != 'm') && ($gender != 'f') ) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_GENDER_ERROR);
        }
      } else if ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        if ( ($gender != 'm') && ($gender != 'f') ) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_GENDER_ERROR_PRO);
        }
      }

// Clients B2B : Controle de la selection du pays pour le code ISO
      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        if (is_numeric($country) == false) {

          $Qcheck = $OSCOM_PDO->prepare('select countries_id 
                                        from :table_countries
                                        where countries_iso_code_2 = :countries_iso_code_2 
                                        limit 1');
          $Qcheck->bindValue(':countries_iso_code_2', $country );
          $Qcheck->execute();

          $check = $Qcheck->fetch();

          $country=$check['countries_id'];     
        }else{      
        $error = true;
          $OSCOM_MessageStack->add('create_account', ENTRY_COUNTRY_ERROR);
        }
      } 

// Clients B2B : Controle entree de la societe
      if ((ACCOUNT_COMPANY_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        if (strlen($company) < ENTRY_COMPANY_PRO_MIN_LENGTH) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_COMPANY_ERROR_PRO);
        }
      }

// Clients B2B : Controle entree numero de siret
      if ((ACCOUNT_SIRET_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        if (strlen($siret) < ENTRY_SIRET_MIN_LENGTH) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_SIRET_ERROR);
        }
      }

// Clients B2B : Controle entree code APE
      if ((ACCOUNT_APE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        if (strlen($ape) < ENTRY_CODE_APE_MIN_LENGTH) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_CODE_APE_ERROR);
        }
      }

// Clients B2B : Controle entree numero de TVA Intracom
     if ((ACCOUNT_TVA_INTRACOM_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        if (strlen($tva_intracom) < ENTRY_TVA_INTRACOM_MIN_LENGTH) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_TVA_INTRACOM_ERROR);
        }
      }

// Clients B2C et B2B : Controle entree du prenom
      if ((strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
        $error = true;

        $OSCOM_MessageStack->addError('account_edit', ENTRY_FIRST_NAME_ERROR);
      } else if ((strlen($firstname) < ENTRY_FIRST_NAME_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        $error = true;

        $OSCOM_MessageStack->addError('account_edit', ENTRY_FIRST_NAME_ERROR_PRO);
      }

// Clients B2C et B2B : Controle entree du nom de famille
      if ((strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
        $error = true;

        $OSCOM_MessageStack->addError('account_edit', ENTRY_LAST_NAME_ERROR);
      } else if ((strlen($lastname) < ENTRY_LAST_NAME_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        $error = true;

        $OSCOM_MessageStack->addError('account_edit', ENTRY_LAST_NAME_ERROR_PRO);
      }

// Clients B2C et B2B : Controle entree date de naissance
      if ((ACCOUNT_DOB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
        if ((strlen($dob) < ENTRY_DOB_MIN_LENGTH) || (!empty($dob) && (!is_numeric(osc_date_raw($dob)) || !@checkdate(substr(osc_date_raw($dob), 4, 2), substr(osc_date_raw($dob), 6, 2), substr(osc_date_raw($dob), 0, 4))))) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_DATE_OF_BIRTH_ERROR);
        }
      } else if ((ACCOUNT_DOB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
        if (!checkdate(substr(osc_date_raw($dob), 4, 2), substr(osc_date_raw($dob), 6, 2), substr(osc_date_raw($dob), 0, 4))) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_DATE_OF_BIRTH_ERROR_PRO);
        }
      }

      if (!osc_validate_email($email_address)) {
        $error = true;

        if ($OSCOM_Customer->getCustomersGroupID() == 0) {
          $OSCOM_MessageStack->addError('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
        } else if ($OSCOM_Customer->getCustomersGroupID() != 0) {
          $OSCOM_MessageStack->addError('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR_PRO);
        }
      }

      $QcheckEmail = $OSCOM_PDO->prepare('select count(*) as total
                                         from :table_customers
                                         where customers_email_address = :customers_email_address
                                         and customers_id != :customers_id
                                        ');
      $QcheckEmail->bindValue(':customers_email_address', osc_db_input($email_address) );
      $QcheckEmail->bindInt(':customers_id', (int)$OSCOM_Customer->getID() );
      $QcheckEmail->execute();

      $check_email = $QcheckEmail->fetch();


      if ($check_email['total'] > 0) {

        $Qcheck = $OSCOM_PDO->prepare('select customers_id 
                                      from :table_customers 
                                      where customers_email_address = :customers_email_address 
                                      and customers_id != :customers_id 
                                      limit 1');
        $Qcheck->bindValue(':customers_email_address', $email_address);
        $Qcheck->bindInt(':customers_id', $OSCOM_Customer->getID());
        $Qcheck->execute();

        if ( $Qcheck->fetch() !== false ) {
          $error = true;

          if ($OSCOM_Customer->getCustomersGroupID() == 0) {
            $OSCOM_MessageStack->addError('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
          } else if ($OSCOM_Customer->getCustomersGroupID() != 0) {
              $OSCOM_MessageStack->addError('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS_PRO);
          }
        }
      }

// Clients B2C et B2B : Controle entree telephone
        if ((strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR);
        } else if ((strlen($telephone) < ENTRY_TELEPHONE_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR_PRO);
        }


        if ($error == false) {
          $sql_data_array = array('customers_firstname' => $firstname,
                                  'customers_lastname' => $lastname,
                                  'customers_email_address' => $email_address,
                                  'customers_telephone' => $telephone);

          if (((ACCOUNT_CELLULAR_PHONE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_CELLULAR_PHONE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
            $sql_data_array['customers_cellular_phone'] = $cellular_phone;
          }

          if (((ACCOUNT_FAX == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_FAX_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
            $sql_data_array['customers_fax'] = $fax;
          }

          if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
            $sql_data_array['customers_gender'] = $gender;
          }

          if (((ACCOUNT_DOB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_DOB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
            $sql_data_array['customers_dob'] = osc_date_raw($dob);
          }

// Clients en mode B2B : Informations societe
        if ($OSCOM_Customer->getCustomersGroupID() != 0) {
          if (ACCOUNT_COMPANY_PRO == 'true') $sql_data_array['customers_company'] = $company;
          if (ACCOUNT_SIRET_PRO == 'true') $sql_data_array['customers_siret'] = $siret;
          if (ACCOUNT_APE_PRO == 'true') $sql_data_array['customers_ape'] = $ape;
          if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $sql_data_array['customers_tva_intracom'] = $tva_intracom;
          if (ACCOUNT_TVA_INTRACOM_PRO == 'true') $sql_data_array['customers_tva_intracom_code_iso'] = $iso;  
        }

        $OSCOM_PDO->save('customers', $sql_data_array, array('customers_id' => $OSCOM_Customer->getID()));
        $OSCOM_PDO->save('customers_info', array('customers_info_date_account_last_modified' => 'now()'),
                                              array('customers_info_id' => $OSCOM_Customer->getID())
                           );

        $sql_data_array = array('customers_firstname' => $firstname,
                                'customers_lastname' => $lastname);

        $OSCOM_PDO->save('customers', $sql_data_array, array('customers_id' => $OSCOM_Customer->getID()),
                                                          array('address_book_id' => $OSCOM_Customer->getDefaultAddressID() )
                           );

// Clients en mode B2B : Modifier le nom de la societe sur toutes les adresses ce trouvant dans le carnet d'adresse
        if (($OSCOM_Customer->getCustomersGroupID() != 0) && (ACCOUNT_COMPANY_PRO == 'true')) {
          $sql_data_array = array('entry_company' => $company);

          $OSCOM_PDO->save('customers', $sql_data_array, array('customers_id' => $OSCOM_Customer->getID()) );
        }

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
          require ('ext/odoo_xmlrpc/xml_rpc_catalog_account_edit.php');
        }
//***************************************
// End odoo web service
//***************************************

// reset the session variables
        $_SESSION['customer_first_name'] = $firstname;

        $OSCOM_MessageStack->addSuccess('account', SUCCESS_ACCOUNT_UPDATED);

        osc_redirect(osc_href_link('account.php', '', 'SSL'));
      } 
  }

  $Qaccount = $OSCOM_PDO->prepare('select customers_gender, 
                                          customers_firstname, 
                                          customers_lastname, 
                                          customers_dob, 
                                          customers_email_address, 
                                          customers_telephone,
                                          customers_fax,
                                          customers_cellular_phone,
                                          customers_company,  
                                          customers_siret, 
                                          customers_ape, 
                                          customers_tva_intracom, 
                                          customers_tva_intracom_code_iso 
                                   from :table_customers 
                                   where customers_id = :customers_id
                                  ');
  $Qaccount->bindInt(':customers_id', $OSCOM_Customer->getID());
  $Qaccount->execute();

  $account = $Qaccount->fetch();

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_edit.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('account_edit'));
