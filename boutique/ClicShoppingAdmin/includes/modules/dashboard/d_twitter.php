<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class d_twitter {
    var $code = 'd_twitter';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_twitter() {
      $this->title = MODULE_ADMIN_DASHBOARD_TWITTER_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_TWITTER_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_TWITTER_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_TWITTER_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True');
      }
    }

    function getOutput() {

// -------------------------------------------
//               Twitter 
// -------------------------------------------

      $output = '
<script type="text/javascript">
var ns6=document.getElementById&&!document.all

function restrictinput(maxlength,e,placeholder){
  if (window.event&&event.srcElement.value.length>=maxlength)
    return false
  else if (e.target&&e.target==eval(placeholder)&&e.target.value.length>=maxlength){
    var pressedkey=/[a-zA-Z0-9\.\,\/]/ 
    if (pressedkey.test(String.fromCharCode(e.which)))
      e.stopPropagation()
  }
}

function countlimit(maxlength,e,placeholder){
  var theform=eval(placeholder)
  var lengthleft=maxlength-theform.value.length
  var placeholderobj=document.all? document.all[placeholder] : document.getElementById(placeholder)
  if (window.event||e.target&&e.target==eval(placeholder)){
    if (lengthleft<0)
      theform.value=theform.value.substring(0,maxlength)
      placeholderobj.innerHTML=lengthleft
  } 
}

function displaylimit(thename, theid, thelimit){
  var theform=theid!=""? document.getElementById(theid) : thename
  var limit_text=\'<b><span id="\'+theform.toString()+\'">\'+thelimit+\'</span></b> Max.\'
  if (document.all||ns6)
    document.write(limit_text)
  if (document.all){
    eval(theform).onkeypress=function(){ return restrictinput(thelimit,event,theform)}
    eval(theform).onkeyup=function(){ countlimit(thelimit,event,theform)}
  }
  else if (ns6){
    document.body.addEventListener(\'keypress\', function(event) { restrictinput(thelimit,event,theform) }, true); 
    document.body.addEventListener(\'keyup\', function(event) { countlimit(thelimit,event,theform) }, true); 
  }
}
</script>';

      $output = '<div class="clearfix"></div>';
      $output .= '<div style="padding-left:90px;">';
      $output .= '<div class="row">';
      $output .= osc_draw_form('twitter', 'index.php', 'post');
      $output .= '<div class="col-sm-1"></div>';
      $output .= '<div class="col-sm-8" style="text-align:center">';
      $output .= '<script type="text/javascript"> ';
      $output .= '$(document).ready(function(){ ';
//default title
      $output .= '$("#caracter").charCount({ ';
      $output .= 'allowed: 140, ';
      $output .= 'warning: 10, ';
      $output .= 'counterText: \' Max : \' ';
      $output .= '}); ';
      $output .= '}); ';
      $output .= '</script> ';
// -------------------------------------------
//               Twitter 
// -------------------------------------------
      $output .=  osc_send_twitter($twitter_authentificate_administrator) .
      TEXT_TITLE_TWITTER  .
      osc_draw_textarea_field('twitter_msg', 'soft', '70', '2', '', 'id="caracter" maxlenght="140" required aria-required="true" id="twitter"');
      $output .= '</div>';
      $output .= '<div class="col-sm-2" style="padding-top:20px;">';
      $output .= '<input type="submit" name="button" id="button" value="post" />';
      $output .= '</div>';
      $output .= '</form>';
      $output .= '</div>';
      $output .= '</div>';
      $output .= '<div>'. osc_draw_separator('pixel_trans.gif', '1', '30') .'</div>';
      $output .= '<div class="clearfix"></div>';

      return $output;
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_TWITTER_STATUS');
    }

    function install() {
      global $OSCOM_PDO;
      if ($_SESSION['languages_id'] =='1') {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Veuillez ins&eacute;rer votre code consumerKey',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_KEY',
            'configuration_value' => '',
            'configuration_description' => 'Veuillez indiquer le code consumerKey',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Veuillez ins&eacute;rer votre code consumerSecret',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_SECRET',
            'configuration_value' => '',
            'configuration_description' => 'Veuillez indiquer le code consumerSecret',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Veuillez ins&eacute;rer votre code accessToken',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN',
            'configuration_value' => '',
            'configuration_description' => 'Veuillez indiquer le code accessToken<br /><br /><b>Note : </b> cliquer sur le bouton My access token',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Veuillez ins&eacute;rer votre code accessTokenSecret',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN_SECRET',
            'configuration_value' => '',
            'configuration_description' => 'Veuillez indiquer le code accessTokenSecret<br /><br /> <b>Note : </b> cliquer sur le bouton My access token',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_SORT_ORDER',
            'configuration_value' => '80',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr� en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      } else {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Do you want enable this Module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_SECURITY_CHECKS_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Do you want enable this Module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Please insert your code consumerKey',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_KEY',
            'configuration_value' => '',
            'configuration_description' => 'Please insert your code consumerKey',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Please insert your code consumerSecret',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_SECRET',
            'configuration_value' => '',
            'configuration_description' => 'Please insert your code consumerSecret',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Please insert your code accessToken',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN',
            'configuration_value' => '',
            'configuration_description' => 'Please specify the code accessToken<br /><br /><b>Note : </b> Click on the My Access Token button',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Please insert your code accessTokenSecret',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN_SECRET',
            'configuration_value' => '',
            'configuration_description' => 'Please specify the code accessTokenSecret<br /><br /> <b>Note : </b> Click on the My Access Token button',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Sort Order',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TWITTER_SORT_ORDER',
            'configuration_value' => '80',
            'configuration_description' => 'Sort order of display. Lowest is displayed first.',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );
      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_TWITTER_STATUS',
                   'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_KEY',
                   'MODULE_ADMIN_DASHBOARD_TWITTER_CONSUMMER_SECRET',
                   'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN',
                   'MODULE_ADMIN_DASHBOARD_TWITTER_ACCESS_TOKEN_SECRET',
                   'MODULE_ADMIN_DASHBOARD_TWITTER_SORT_ORDER'
                  );
    }
  }
