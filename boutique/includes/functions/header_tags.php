<?php
/**
 * header_tage.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  
/**
 * Function to replace in the metatag
 * @version 1.0
 * public function
 * @param string $string
 * @return string metatag  in the meta language 
 * remplace les espaces par un +
*/

// pb au niveau php5
  function osc_replace_string($string) {
    $string = preg_replace("# #", ",", $string);  
    return preg_replace("/[<>]/", '_', $string);
  }

/**
 * Function to clean the metatag
 * @version 1.0
 * public function
 * @param string $clean_html
 * @return string $its_cleaned  in the meta language 
*/	
function clean_html_comments($clean_html) {
  global $its_cleaned;
  if ( strpos($clean_html,'<!--//*')>1 ) {
    $the_end1= strpos($clean_html,'<!--//*')-1;
    $the_start2= strpos($clean_html,'*//-->')+7;
    $its_cleaned= substr($clean_html,0,$the_end1);
    $its_cleaned.= substr($clean_html,$the_start2);
  } else {
    $its_cleaned = $clean_html;
  }
  return $its_cleaned;
}

/**
 * Function to return the metatag in the footer 
 * @version 1.0
 * public function
 * @param string $footer
 * @return string metatag in the footer
*/	

  function osc_get_submit_footer() {
    global $OSCOM_PDO;

     $Qsubmit_footer = $OSCOM_PDO->prepare('select submit_defaut_language_footer
                                            from :table_submit_description
                                            where language_id = :language_id 
                                            ');
     $Qsubmit_footer->bindInt(':language_id', (int)$_SESSION['languages_id'] );
     $Qsubmit_footer->execute();

     $submit_footer = $Qsubmit_footer->fetch();

     $footer = clean_html_comments(osc_output_string_protected($submit_footer['submit_defaut_language_footer']));

     $delimiter =',';
     $footer = trim(preg_replace('|\\s*(?:' . preg_quote($delimiter) . ')\\s*|', $delimiter, $footer));
     $footer1 = explode(",", $footer);

     foreach ($footer1 as $value) {
       $footer_content .= '#<a href="'.osc_href_link('advanced_search_result.php', 'keywords='.$value.'&search_in_description=1', 'NONSSL') . '">'.$value.'</a> ';
     }

     return $footer_content;
  }


/*
 * Function to return the canonical URL

 * @version 1.0
 * public function
 * @param string $canonical_link
 * @return string url of the website
*/	  
  function osc_CanonicalUrl() {

   $domain = substr((($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER), 0); // gets the base URL minus the trailing slash
   $string = $_SERVER['REQUEST_URI'];	 // gets the url 
   $search = '\&LORsid.*|\?LORsid.*'; // searches for the session id in the url  
   $replace = '';	 // replaces with nothing i.e. deletes
   $str = $string;
   $chars = preg_split('/&/', $str, -1);

   if($chars[1]){
     $newstring= "?".$chars[1];
   }

   if($chars[2]){
     $newstring = $newstring ."&".$chars[2];
   }

    if($newstring){
      $canonical_link = $domain . preg_replace( '#$search#', $replace, $string )  . $newstring; // merges the variables and echoing them 
    } else {
      $canonical_link = $domain . preg_replace( '#$search#', $replace, $string );	 // merges the variables and echoing them 
    }
   return $canonical_link;
  }

