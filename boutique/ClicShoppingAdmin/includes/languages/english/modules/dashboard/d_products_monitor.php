<?php
/**
 * d_products_monitor.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_TITLE', 'Products Monitor');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_DESCRIPTION', 'Display Products Errors in Monitor System');

define('MODULE_ADMIN_DASHBOARD_PRODUCTS_INFO_PRODUCTS_ID', 'Id');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_INFO_PRODUCTS_NAME','Products');
define('MODULE_ADMIN_DASHBOARD_MODEL_INFO_PRODUCTS_NAME', 'Model');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_PRODUCTS_PRODUCTS_LAST_MODIFIED', 'Last Modified');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_PRODUCTS_ERRORS', 'Errors Description');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_MODEL', 'no model');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_PICTURE', 'no picture');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_TAX', 'no tax class');
define('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_PRICE', 'zero price');

define('MODULE_ADMIN_DASHBOARD_PERFECT_PRODUCTS', 'No critical products detected.');
?>