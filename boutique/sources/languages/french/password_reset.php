<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Login');
define('NAVBAR_TITLE_2', 'Réinitialisation mot de passe');

define('HEADING_TITLE', 'Réinitialisation mot de passe');

define('TEXT_MAIN', 'Veuillez compléter les élements suivants.');

define('TEXT_NO_RESET_LINK_FOUND', 'Erreur: le lien concernant le reset du mot de passe n\'a pas été trouvée dans nos enregistrements, veuillez recommencer ou générer un nouveau lien.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Erreur: L\'adresse email n\'a pas été trouvée, veuillez recommencer.');

define('SUCCESS_PASSWORD_RESET', 'Votre mot de passe a é;té mis à jour. Veuillez vous connecter avec votre nouveau mot de passe.');

define('TEXT_EMAIL_SUBJECT','Réinitialisation mot de passe effectué');
define('TEXT_EMAIL_BODY','Bonjour,' . "\n\n" . ' Votre mot de passe a été réinitialisé avec succès. Vous pouvez vous connecter dès à présent sur '.STORE_NAME);


?>