<?php
/**
 * contact_customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require('includes/functions/template_email.php');


  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
     case 'setflag':
        osc_set_contact_customers_archive($_GET['id'], $_GET['flag']);
        osc_redirect(osc_href_link('contact_customers.php', (isset($_GET['page']) ? 'page=' . $_GET['page'] . '&' : '') . 'rID=' . $_GET['id']));
     break;

     case 'update':
        $contact_customers_id = osc_db_prepare_input($_GET['rID']);
        $customers_response = osc_db_prepare_input($_POST['customers_response']);
        $administrator_user_name = osc_user_admin($user_administrator);
        $contact_customers_archive = osc_db_prepare_input($_POST['contact_customers_archive']);
        $contact_customers_status = osc_db_prepare_input($_POST['contact_customers_status']);
        $contact_department = osc_db_prepare_input($_POST['contact_department']);
        $contact_name = osc_db_prepare_input($_POST['contact_name']);
        $contact_email_address = osc_db_prepare_input($_POST['contact_email_address']);
        $contact_telephone = osc_db_prepare_input($_POST['contact_telephone']);

        if (!empty($customers_response)) {
           $sql_data_array = array('contact_customers_id' => $contact_customers_id,
                                   'administrator_user_name' => $administrator_user_name,
                                   'customers_response' => $customers_response,
                                   'contact_date_sending' => 'now()'
                                   );
          $OSCOM_PDO->save('contact_customers_follow', $sql_data_array );
        }

        $Qupdate = $OSCOM_PDO->prepare('update :table_contact_customers
                                        set contact_customers_archive = :contact_customers_archive,
                                             contact_customers_status = :contact_customers_status
                                        where contact_customers_id = :contact_customers_id
                                      ');
        $Qupdate->bindInt(':contact_customers_archive', $contact_customers_archive);
        $Qupdate->bindInt(':contact_customers_status', $contact_customers_status);
        $Qupdate->bindInt(':contact_customers_id', (int)$contact_customers_id);
        $Qupdate->execute();

        $from = EMAIL_FROM;
        $subject = SUBJECT_EMAIL;
        $message = osc_db_prepare_input($_POST['customers_response']);

// email template
        $template_email_signature = osc_get_template_email_signature($template_email_signature);
        $template_email_footer = osc_get_template_email_text_footer($template_email_footer);
        $consult_message = TEXT_INFO_MESSAGE;

        $message .= '<br />'. $consult_message . '<br />'. $template_email_signature . '<br />' . $template_email_footer;
        $message = $message;

//Let's build a message object using the email class
        $mimemessage = new email(array('X-Mailer: ClicShopping'));

// Envoie du mail avec gestion des images pour Fckeditor
        $message = html_entity_decode($message);
        $message = str_replace('src="/', 'src="' . HTTP_CATALOG_SERVER . '/', $message);

        $mimemessage->add_html_fckeditor($message);
        $mimemessage->build_message();
        $mimemessage->send($contact_name, $contact_email_address, '', $from, $subject);

        osc_redirect(osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $contact_customers_id));
      break;

      case 'delete_all':
       if ($_POST['selected'] != '') { 
          foreach ($_POST['selected'] as $contact['contact_customers_id'] ) {

            $Qdelete = $OSCOM_PDO->prepare('delete
                                            from :table_contact_customers
                                            where contact_customers_id = :contact_customers_id
                                          ');
            $Qdelete->bindInt(':contact_customers_id', (int)$contact['contact_customers_id']);
            $Qdelete->execute();

            $Qdelete = $OSCOM_PDO->prepare('delete 
                                            from :table_contact_customers_follow
                                            where contact_customers_id = :contact_customers_id
                                          ');
            $Qdelete->bindInt(':contact_customers_id', (int)$contact['contact_customers_id']);
            $Qdelete->execute();

          }
       }

        osc_redirect(osc_href_link('contact_customers.php', 'page=' . $_GET['page']));
      break;
    }
  }


    if ($action == 'archive') {
      $contact_query_raw = "select c.contact_customers_id,
                                   c.contact_department, 
                                   c.contact_name, 
                                   c.contact_email_address, 
                                   c.contact_email_subject, 
                                   c.contact_date_added,
                                   c.languages_id ,
                                   c.contact_customers_archive,
                                   c.contact_customers_status,
                                   c.customer_id,
                                   c.contact_telephone
                            from contact_customers c
                            where c.contact_customers_archive ='1'
                            order by contact_customers_id desc
                           ";
    } else {
      $contact_query_raw = "select c.contact_customers_id,
                                   c.contact_department, 
                                   c.contact_name, 
                                   c.contact_email_address, 
                                   c.contact_email_subject, 
                                   c.contact_date_added,
                                   c.languages_id ,
                                   c.contact_customers_archive,
                                   c.contact_customers_status,
                                   c.customer_id,
                                   c.contact_telephone
                            from contact_customers c
                            where c.contact_customers_archive ='0'
                            order by contact_customers_id desc
                           ";
    }

    $contact_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $contact_query_raw, $contact_query_numrows);
    $contact_query = osc_db_query($contact_query_raw);

  if (empty($action)) {
    $languages = osc_get_languages();
    $languages_array = array();
    $languages_selected = DEFAULT_LANGUAGE;
    for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
      $languages_array[] = array('id' => $languages[$i]['code'],
                                 'text' => $languages[$i]['name']);
      if ($languages[$i]['directory'] == $language) {
        $languages_selected = $languages[$i]['code'];
      }
    }
  }

  require('includes/header.php');
?>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/customers_services.png', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading" width="30%"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
<?php
  if (empty($action)) {
?>
            <td class="smallText" valign="middle" align="center" width="50%">
<?php echo $contact_split->display_count($contact_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></br />
<?php echo $contact_split->display_links($contact_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?>
            </td>
            <td align="right"><table border="0" cellspacing="0" cellpadding="0">
              <tr valign="middle">
                <td><?php echo osc_draw_form('adminlanguage', 'contact_customers.php', 'page=' . $_GET['page'], 'get') .  osc_draw_pull_down_menu('language', $languages_array, $languages_selected, 'onchange="this.form.submit();"')  . osc_hide_session_id(); ?></form></td>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
              </tr>
             </table></td>
              <td align="right"><?php echo '<a href="' . osc_href_link('contact_customers.php', '&action=archive') .'">' . osc_image_button('button_see_archive.png', IMAGE_ARCHIVE) . '</a>&nbsp;'; ?></td>
              <td align="right">
                <form name="delete_all" <?php echo 'action="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&action=delete_all') . '"'; ?> method="post">
                <a onclick="$('delete').prop('action', ''); $('form').submit();" class="button"><span><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></span></a>&nbsp;
              </td>

<?php
  } elseif ($action == 'archive') {
?>  
              <td align="right"><?php echo '<a href="' . osc_href_link('contact_customers.php') .'">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
<?php
  } elseif ($action == 'edit') {
          echo osc_draw_form('contact', 'contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $_GET['rID'] . '&action=update');
?>
              <td align="right" width="80%"><?php echo osc_image_submit('button_send_mail.gif', IMAGE_PREVIEW); ?></td>
              <td  align="right"><?php echo '<a href="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $_GET['rID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
<?php
  } elseif ($action == 'preview') {
    if (osc_not_null($_POST)) {
      $rInfo = new objectInfo($_POST);
    } else {
      $rID = osc_db_prepare_input($_GET['rID']);

      $Qcontact = $OSCOM_PDO->prepare("select contact_customers_id,
                                              contact_department, 
                                              contact_name, 
                                              contact_email_address, 
                                              contact_email_subject, 
                                              contact_enquiry, 
                                              contact_date_added,
                                              languages_id,  
                                              contact_customers_archive,
                                              contact_customers_status,
                                              customer_id,
                                              contact_telephone
                                     from :table_contact_customers 
                                     where contact_customers_id = :contact_customers_id
                                    ");
      $Qcontact->bindInt(':contact_customers_id', (int)$rID );
      $Qcontact->execute();

      $contact = $Qcontact->fetch();

      $rInfo_array = array_merge($contact);
      $rInfo = new objectInfo($rInfo_array);
    } 

    echo osc_draw_form('update', 'contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $_GET['rID'] . '&action=update', 'post', 'enctype="multipart/form-data"');

    if (osc_not_null($_POST)) {
/* Re-Post all POST'ed variables */
      reset($_POST);
      while (list($key, $value) = each($_POST)) echo osc_draw_hidden_field($key, htmlspecialchars($value));
?>
          <td><?php echo '<a href="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $_GET['rID'] . '&action=edit') . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
          <td><?php echo osc_image_submit('button_send_mail.gif', IMAGE_SEND); ?></td>
          <td><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></td>
          <td><?php echo '<a href="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $_GET['rID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
<?php
    } else {
      if (isset($_GET['origin'])) {
        $back_url = $_GET['origin'];
        $back_url_params = '';
      } else {
        $back_url = 'contact_customers.php';
        $back_url_params = 'page=' . $_GET['page'] . '&rID=' . $rInfo->contact_customers_id;
      }
?>
                <td  align="right"><?php echo '<a href="' . osc_href_link($back_url, $back_url_params) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
<?php
    }
  }
?>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<!-- //################################################################################################################ -->
<!-- //                                               EDITION AVIS CLIENTS                                              -->
<!-- //################################################################################################################ -->
<?php
  if ($action == 'edit') {
    $rID = osc_db_prepare_input($_GET['rID']);

    $Qcontact = $OSCOM_PDO->prepare("select contact_customers_id,
                                              contact_department,
                                              contact_name,
                                              contact_email_address,
                                              contact_email_subject,
                                              contact_enquiry,
                                              contact_date_added,
                                              languages_id,
                                              contact_customers_archive,
                                              contact_customers_status,
                                              customer_id,
                                              contact_telephone
                                     from :table_contact_customers
                                     where contact_customers_id = :contact_customers_id
                                    ");
    $Qcontact->bindInt(':contact_customers_id', (int)$rID );
    $Qcontact->execute();

    $contact = $Qcontact->fetch();

    $rInfo_array = array(array($contact));
    $rInfo = new objectInfo($rInfo_array);

//creation du tableau pour le  dropdown des status des messages
   $contact_customers_status_array = array(array('id' => '0', 'text' => ENTRY_STATUS_MESSAGE_REALISED),
                                           array('id' => '1', 'text' => ENTRY_STATUS_MESSAGE_NOT_REALISED));

//creation du tableau pour le  dropdown pour les archives
   $contact_customers_archive_array = array(array('id' => '1', 'text' => ENTRY_ARCHIVE_YES),
                                            array('id' => '0', 'text' => ENTRY_ARCHIVE_NO));
?>
      <tr>
        <td>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
              <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_HISTORY; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
<?php
// -- ------------------------------------------------------------ //
// --          ONGLET Information General contact client          //
// -- ------------------------------------------------------------ //
?>

                <div class="tab-pane active" id="tab1">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_REVIEWS_GENERAL; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_ID_CUSTOMERS; ?></span>
                        <span class="col-md-4"><?php echo $contact['contact_customers_id']; ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_CUSTOMER_ID; ?></span>
                        <span class="col-md-4"><?php echo $contact['customer_id'];  ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_DEPARTMENT; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_hidden_field('contact_department',$contact['contact_department']) . $contact['contact_department']; ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_CUSTOMERS_NAME; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_hidden_field('contact_name', $contact['contact_name']) . $contact['contact_name']; ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_CUSTOMERS_TELEPHONE; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_hidden_field('contact_telephone', $contact['contact_telephone']) . $contact['contact_telephone']; ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_EMAIL_ADDRESS; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_hidden_field('contact_email_address', $contact['contact_email_address']) . $contact['contact_email_address'];  ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_EMAIL_SUBJECT; ?></span>
                        <span class="col-md-8"><?php echo $contact['contact_email_subject'];  ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_DATE_ADDED; ?></span>
                        <span class="col-md-4"><?php echo $OSCOM_Date->getShortTime($contact['contact_date_added']);  ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_CUSTOMERS_MESSAGE; ?></span>
                        <span class="col-md-4"><?php echo $contact['contact_enquiry'];  ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_ARCHIVE; ?></span>
                        <span class="col-md-4"><?php echo osc_draw_pull_down_menu('contact_customers_archive', $contact_customers_archive_array, (( $contact['contact_customers_archive'] == '0') ? '0' : '1'));  ?></span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo ENTRY_STATUS_MESSAGE; ?></span>
                        <span class="col-md-4"><?php echo  osc_draw_pull_down_menu('contact_customers_status', $contact_customers_status_array, (($contact['contact_customers_status'] == '0') ? '0' : '1'));?></span>
                      </div>
                    </div>
                  </div>

                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TITLE_REVIEWS_ENTRY; ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2"><?php echo ENTRY_REVIEW; ?></span>
                        <span class="col-md-4">
                          <div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('customers_response', 'soft', '750', '300', $rInfo->customers_response); ?></div>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
<?php
// -- ------------------------------------------------------------ //
// --          ONGLET comentaires contact client          //
// -- ------------------------------------------------------------ //
?>
                  <div class="tab-pane" id="tab2">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                     <tr>
                       <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                         <tr class="dataTableHeadingRow">
                           <td class="dataTableHeadingContent" align="center" width="100"><?php echo TABLE_HEADING_REF; ?></td>
                           <td class="dataTableHeadingContent" align="center"width="150"><?php echo TABLE_HEADING_DATE_SENDING; ?></td>
                           <td class="dataTableHeadingContent" align="center"width="150"><?php echo TABLE_HEADING_USER_NAME; ?></td>
                           <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_RESPONSE; ?></td>
                         </tr>
<?php
  $QfollowContact = $OSCOM_PDO->prepare('select c.contact_customers_id,
                                                ccf.id_contact_customers_follow,
                                                ccf.administrator_user_name,
                                                ccf.customers_response,
                                                ccf.contact_date_sending
                                         from :table_contact_customers c,
                                              :table_contact_customers_follow ccf
                                         where  c.contact_customers_id = ccf.contact_customers_id
                                       ');

  $QfollowContact->execute();

    while ($follow_contact = $QfollowContact->fetch() ) {
?>
                        <tr>
                          <td class="dataTableContent" align="left"><?php echo $follow_contact['contact_customers_id']; ?></td>
                          <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShortTime($follow_contact['contact_date_sending']); ?></td>
                          <td class="dataTableContent" align="left"><?php echo $follow_contact['administrator_user_name']; ?></td>
                          <td class="dataTableContent" align="left"><?php echo $follow_contact['customers_response']; ?></td>
                        </tr>
<?php
    }
?>
                      </table></td>
                     </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
    </form>


<!-- //################################################################################################################ -->
<!-- //                                          PREVISUALISATION AVIS CLIENTS                                          -->
<!-- //################################################################################################################ -->
<?php
  } elseif ($action == 'preview') {
?>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <div>
                <ul class="nav nav-tabs" role="tablist"  id="myTab">
                  <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><?php echo TAB_GENERAL; ?></a></li>
                  <li><a href="#tab2" role="tab" data-toggle="tab"><?php echo TAB_HISTORY; ?></a></li>
                </ul>
                <div class="tabsClicShopping">
                  <div class="tab-content">
<!-- //################################################################################################################ -->
<!--          ONGLET Previsualisation d'information General sur l'avis client          //-->
<!-- //################################################################################################################ -->
                    <div class="tab-pane active" id="tab1">
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td class="mainTitle"><?php echo TITLE_REVIEWS_GENERAL; ?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                        <tr>
                          <td valign="top"><table border="0" cellpadding="2" cellspacing="2">
                           <tr>
                              <td class="main"><?php echo ENTRY_ID_CUSTOMERS; ?></td>
                              <td class="main"><?php echo $rInfo->contact_customers_id; ?></td>
                           </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_CUSTOMER_ID; ?></td>
                              <td class="main"><?php echo $contact['customer_id']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_DEPARTMENT; ?></td>
                              <td class="main"><?php echo $contact['contact_department']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_CUSTOMERS_NAME; ?></td>
                              <td class="main"><?php echo  $contact['contact_name']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_CUSTOMERS_TELEPHONE; ?></td>
                              <td class="main"><?php echo  $contact['contact_telephone']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                              <td class="main"><?php echo $contact['contact_email_address']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_EMAIL_SUBJECT; ?></td>
                              <td class="main"><?php echo $contact['contact_email_subject']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_DATE_ADDED; ?></td>
                              <td class="main"><?php echo $OSCOM_Date->getShortTime($contact['contact_date_added']); ?></td>
                            </tr>
                            <tr>
                              <td class="main"  valign="top"><?php echo ENTRY_CUSTOMERS_MESSAGE; ?></td>
                              <td class="main"><?php echo $contact['contact_enquiry']; ?></td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_ARCHIVE; ?></td>
                              <td class="main">
<?php
       if ($rInfo->contact_customers_archive == '1') {
        echo osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16);
       } else {
        echo osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16);
       }
?>
                             </td>
                            </tr>
                            <tr>
                              <td class="main"><?php echo ENTRY_STATUS_MESSAGE; ?></td>
<?php
      if ( $contact['contact_customers_status'] == 1) {
?>
                              <td class="main"><?php echo ENTRY_STATUS_MESSAGE_REALISED; ?></td>
<?php
      } else {
?>
                             <td class="main"><?php echo ENTRY_STATUS_MESSAGE_NOT_REALISED; ?></td>
<?php
     }
?>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                          <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                         <tr>
                          <td class="mainTitle"><?php echo TITLE_REVIEWS_ENTRY; ?></td>
                        </tr>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                        <tr>
                          <td><table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                              <td class="main" valign="top"><?php echo $rInfo->customers_response; ?></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                    </div>
<!-- //################################################################################################################ -->
<!--          ONGLET Previsualisation commentaires         //-->
<!-- //################################################################################################################ -->
                    <div class="tab-pane" id="tab2">
                      <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                            <tr class="dataTableHeadingRow">
                              <td class="dataTableHeadingContent" align="center" width="100"><?php echo TABLE_HEADING_REF; ?></td>
                              <td class="dataTableHeadingContent" align="center"width="150"><?php echo TABLE_HEADING_DATE_SENDING; ?></td>
                              <td class="dataTableHeadingContent" align="center"width="150"><?php echo TABLE_HEADING_USER_NAME; ?></td>
                              <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_RESPONSE; ?></td>
                            </tr>
<?php
  $QfollowContact = $OSCOM_PDO->prepare('select c.contact_customers_id,
                                                ccf.id_contact_customers_follow,
                                                ccf.administrator_user_name,
                                                ccf.customers_response,
                                                ccf.contact_date_sending
                                         from :table_contact_customers c,
                                              :table_contact_customers_follow ccf
                                         where   c.contact_customers_id = :contact_customers_id
                                       ');
  $QfollowContact->execute();

    while ($follow_contact = $QfollowContact->fetch() ) {
?>
                            <tr>
                              <td class="dataTableContent" align="left"><?php echo $follow_contact['contact_customers_id']; ?></td>
                              <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShortTime($follow_contact['contact_date_sending']); ?></td>
                              <td class="dataTableContent" align="left"><?php echo $follow_contact['administrator_user_name']; ?></td>
                              <td class="dataTableContent" align="left"><?php echo $follow_contact['customers_response']; ?></td>
                            </tr>
<?php
    }
?>
                          </table></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </td>
          </tr>
<?php
  } else {
?>
<!-- //################################################################################################################ -->
<!-- //                                            LISTING DES CONTACT CLIENTS                                             -->
<!-- //################################################################################################################ -->
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>    
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_REF; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_ID; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_ADDED; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DEPARTMENT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_EMAIL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMERS_LANGUAGE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_USER_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ARCHIVE; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>

              </tr>
<?php
    while ($contact = osc_db_fetch_array($contact_query)) {

      if ((!isset($_GET['rID']) || (isset($_GET['rID']) && ($_GET['rID'] == $contact['contact_customers_id']))) && !isset($rInfo)) {

        $rInfo_array = array_merge((array)$contact);
        $rInfo = new objectInfo($rInfo_array);

      } // end !isset($_GET['rID']

      if (isset($rInfo) && is_object($rInfo) && ($contact['contact_customers_id'] == $rInfo->contact_customers_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } // ed isset($rInfo)

      $QfollowContact = $OSCOM_PDO->prepare('select c.contact_customers_id,
                                                    ccf.administrator_user_name
                                             from :table_contact_customers c,
                                                  :table_contact_customers_follow ccf
                                             where  c.contact_customers_id = :contact_customers_id
                                             and c.contact_customers_id = ccf.contact_customers_id
                                           ');
      $QfollowContact->bindInt(':contact_customers_id', (int)$contact['contact_customers_id'] );
      $QfollowContact->execute();

      $follow_contact = $QfollowContact->fetch();


      $QlanguageContact = $OSCOM_PDO->prepare('select name
                                             from languages
                                             where languages_id = :languages_id
                                           ');
      $QlanguageContact->bindInt(':languages_id', (int)$contact['languages_id'] );
      $QlanguageContact->execute();

      $language_contact = $QlanguageContact->fetch();
?>
                <td>
<?php 
      if ($contact['selected']) { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $contact['contact_customers_id']; ?>" checked="checked" />
<?php 
      } else { 
?>
                  <input type="checkbox" name="selected[]" value="<?php echo $contact['contact_customers_id']; ?>" />
<?php 
      } //end $contact['selected']
?>
                </td>
                  <td class="dataTableContent" align="left"><strong><?php echo $contact['contact_customers_id']; ?></strong></td>
<?php 
      if ($contact['customer_id']) { 
?>
                <td class="dataTableContent" align="left"><strong><?php echo $contact['customer_id']; ?></strong></td>
<?php 
      } else { 
?>
                <td class="dataTableContent" align="left">&nbsp;</td>
<?php 
      } // end $contact['customer_id']
?>

                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShortTime($contact['contact_date_added']); ?></td>
                <td class="dataTableContent" align="left"><?php echo $contact['contact_department']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $contact['contact_name']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $contact['contact_email_address']; ?></td>
                <td class="dataTableContent" align="left"><?php echo $language_contact['name']; ?></td>
<?php
      if ($contact['contact_customers_status'] == 0) {
?>
                <td class="dataTableContent" align="left"><?php echo ENTRY_STATUS_MESSAGE_REALISED; ?></td>
<?php
      } else {
?>
                <td class="dataTableContent" align="left"><?php echo ENTRY_STATUS_MESSAGE_NOT_REALISED; ?></td>
<?php
      } // end $contact['contact_customers_status']
?>
                <td class="dataTableContent" align="center"><?php echo  $follow_contact['administrator_user_name'] ?></td>
                <td></td>
                <td class="dataTableContent" align="center">
<?php
      if ($contact['contact_customers_archive'] == '1') {
        echo '<a href="' . osc_href_link('contact_customers.php', 'action=setflag&flag=0&id=' . $contact['contact_customers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('contact_customers.php', 'action=setflag&flag=1&id=' . $contact['contact_customers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $contact['contact_customers_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $contact['contact_customers_id'] . '&action=preview') . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW_COMMENT) . '</a>';
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      if ( (is_object($rInfo)) && ($contact['contact_customers_id'] == $rInfo->contact_customers_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . osc_href_link('contact_customers.php', 'page=' . $_GET['page'] . '&rID=' . $contact['contact_customers_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
    } // end while
?>

              </form><!-- end form delete all -->
              <tr>
                <td colspan="12" class="smallText" valign="top"><?php echo $contact_split->display_count($contact_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></td>
              </tr>
            </table></td>
<?php
  } // end else
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

