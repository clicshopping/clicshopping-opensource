<?php
/**
 * discount_coupons.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE', 'Discount Coupons');
define('TITLE_DISCOUNT_COUPONS','Coupon code');
define('TITLE_DISCOUNT_AMOUNT','Discount amount');
define('TITLE_DISCOUNT_DATE','Start and end date');
define('TITLE_DISCOUNT_ORDER','ordeer amount and type');
define('TITLE_DISCOUNT_USE','Number use coupon');

define('TEXT_HEADING_DISCOUNT_COUPONS_ID', 'Coupon Code');
define('TEXT_HEADING_DISCOUNT_CREATE_ACCOUNT','Create account'); 	
define('TEXT_HEADING_DISCOUNT_AMOUNT', 'Discount');
define('TEXT_HEADING_DISCOUNT_TYPE', 'Discount Rype');
define('TEXT_HEADING_DISCOUNT_DATE_START', 'Start Date');
define('TEXT_HEADING_DISCOUNT_DATE_END', 'End Date');
define('TEXT_HEADING_DISCOUNT_MAX_USE', 'Max Use');
define('TEXT_HEADING_DISCOUNT_MIN_ORDER', 'Min Order');
define('TEXT_HEADING_DISCOUNT_NUMBER_AVAILABLE', 'Number Available');
define('TEXT_HEADING_DISCOUNT_CUSTOMER_GROUP','Customer Group');
define('TABLE_HEADING_ACTION','Actions');

define('TITLE_HELP_DISCOUNT_PRICE', 'Notes on the Coupons Management');
define('TEXT_HELP_DISCOUNT_PRICE','<li>Coupon code can be generated randomly.</li>
<li>To use the coupons indefinitely please insert 0 for the amount or number of use of the coupon.</li>
<li>For calculations, please refer to the Configuration menu / modules / Total Control.</li>
<li>Before creating a new coupon sent to any user create a new account, please delete the old coupon. This coupon will not replace the old. Please update or delete the old coupon before to insert a new coupon</li>
');


define('TEXT_DISCOUNT_COUPONS_ID', 'Coupon Code:');
define('TEXT_DISCOUNT_COUPONS_DESCRIPTION', 'Description:');
define('TEXT_DISCOUNT_COUPONS_AMOUNT', 'Discount Amount:');
define('TEXT_DISCOUNT_COUPONS_TYPE', 'Discount Type:');
define('TEXT_DISCOUNT_COUPONS_DATE_START', 'Start Date:');
define('TEXT_DISCOUNT_COUPONS_DATE_END', 'End Date:');
define('TEXT_DISCOUNT_COUPONS_MAX_USE', 'Max Use:');
define('TEXT_DISCOUNT_COUPONS_MIN_ORDER', 'Min Order:');
define('TEXT_DISCOUNT_COUPONS_MIN_ORDER_TYPE', 'Min Order Type:');
define('TEXT_DISCOUNT_COUPONS_NUMBER_AVAILABLE', 'Number Available:');
define('TEXT_DISCOUNT_COUPONS_CREATE_ACCOUNT','Do you want offer this coupon when the client create an account ?');
define('TEXT_DISCOUNT_COUPONS_WARNING','Becarefull !  You have already define a coupon for the customers who create an account. If you select the checkobox, this coupons will be the new value');
define('TEXT_DISCOUNT_CUSTOMERS_GROUP','Customers Group:');
define('TEXT_DISPLAY_NUMBER_OF_DISCOUNT_COUPONS', 'Items:');

define('TEXT_DISPLAY_UNLIMITED', 'unlimited' );
define('TEXT_DISPLAY_SHIPPING_DISCOUNT', 'off shipping');

define('TEXT_INFO_DISCOUNT_AMOUNT_HINT', 'For percentage or shipping discounts, enter a percentage as a decimal.  Example: .10 for 10%');
define('TEXT_INFO_DISCOUNT_AMOUNT', 'Discount:');
define('TEXT_INFO_DISCOUNT_TYPE', 'Discount Type:');
define('TEXT_INFO_DATE_START', 'Start:');
define('TEXT_INFO_DATE_END', 'End:');
define('TEXT_INFO_MAX_USE', 'Max Use:');
define('TEXT_INFO_MIN_ORDER', 'Min Order:');
define('TEXT_INFO_MIN_ORDER_TYPE', 'Min Order Type:');
define('TEXT_INFO_NUMBER_AVAILABLE', 'Available:');

define('TEXT_DISCOUNT_TWITTER','Do you wish publish this coupon on twitter ?');
define('TEXT_NEW_DISCOUNT_TWITTER','Get a discount coupon on our products CODE : ');
define('TEXT_DISCOUNT_DATE_END', ' until ');
define('TEXT_TWITTER_AMOUNT', ' : Amount / Discount : ');

define('TEXT_INFO_HEADING_DELETE_DISCOUNT_COUPONS', 'Delete Discount Coupon');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this discount coupon?');

define('ERROR_DISCOUNT_COUPONS_NO_AMOUNT', 'Please enter a discount amount.' );

//exclusions
define('IMAGE_PRODUCT_EXCLUSIONS', 'Product Exclusions');
define('IMAGE_MANUFACTURER_EXCLUSIONS', 'Manufacturer Exclusions');
define('IMAGE_CATEGORY_EXCLUSIONS', 'Category Exclusions');
define('IMAGE_CUSTOMER_EXCLUSIONS', 'Customer Exclusions');
define('IMAGE_SHIPPING_ZONE_EXCLUSIONS', 'Shipping Zone Exclusions');
//end exclusions

define('IMAGE_NEW_COUPON', 'New Coupon');
?>
