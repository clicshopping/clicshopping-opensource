<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  require ('includes/languages/' . $language . '/' . basename($_SERVER['PHP_SELF']));

  $dir_fs_document_root = $_POST['DIR_FS_DOCUMENT_ROOT'];
  if ((substr($dir_fs_document_root, -1) != '\\') && (substr($dir_fs_document_root, -1) != '/')) {
    if (strrpos($dir_fs_document_root, '\\') !== false) {
      $dir_fs_document_root .= '\\';
    } else {
      $dir_fs_document_root .= '/';
    }
  }
?>
  <div class="contentBlock">
    <div class="contentPane">
      <h1><?php echo TEXT_END_CONFIGURATION; ?></h1>
      <?php echo TEXT_INFO_1; ?>
    </div>
  </div>


   <div class="contentBlock">
     <div class="contentPane">
        <h2><?php echo TEXT_INFO_2; ?></h2>
        <?php echo TEXT_INFO_3; ?>
    </div>
  </div>


<form name="install" id="installForm" action="index.php?step=6" method="post" class="form-horizontal" role="form">
   <div class="contentBlock">
      <div class="contentPane">
        <h2>Configuration d'acc&eacute;s &agrave; la boutique ClicShopping</h2>


      <div class="form-group has-feedback">
        <label for="storeName" class="control-label col-xs-3"><?php echo TEXT_STORE_NAME; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_STORE_NAME', NULL, 'required aria-required="true" id="storeName" placeholder="Your Store Name"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_HELP; ?></span>
        </div>
      </div>
     
       <div class="form-group has-feedback">
        <label for="StoreOwnerEmail" class="control-label col-xs-3"><?php echo TEXT_STORE_OWNER; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_STORE_OWNER_NAME', NULL, 'required aria-required="true" id="StoreOwnerEmail" placeholder="'.TEXT_STORE_OWNER.'"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_OWNER_HELP; ?></span>
        </div>
      </div>    
      
      
      <div class="form-group has-feedback">
        <label for="ownerEmail" class="control-label col-xs-3"><?php echo TEXT_STORE_OWNER_EMAIL; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_STORE_OWNER_EMAIL_ADDRESS', NULL, 'required aria-required="true" id="ownerEmail" placeholder="'.TEXT_STORE_OWNER_EMAIL.'"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_OWNER_EMAIL_HELP; ?></span>
        </div>
      </div>      
      <div class="form-group has-feedback">
        <label for="StoreNameAdmin" class="control-label col-xs-3"><?php echo TEXT_STORE_NAME_ADMIN; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_ADMINISTRATOR_NAME', NULL, 'required aria-required="true" id="StoreNameAdmin" placeholder="'.TEXT_STORE_NAME_ADMIN.'"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_NAME_ADMIN_HELP; ?></span>
        </div>
      </div>


      <div class="form-group has-feedback">
        <label for="adminFirstName" class="control-label col-xs-3"><?php echo TEXT_STORE_FIRST_NAME; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_ADMINISTRATOR_FIRSTNAME', NULL, 'required aria-required="true" id="adminFirstName" placeholder="'.TEXT_STORE_FIRST_NAME.'"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_FIRST_NAME_HELP; ?></span>
        </div>
      </div>




      <div class="form-group has-feedback">
        <label for="adminUsername" class="control-label col-xs-3"><?php echo TEXT_STORE_EMAIL_ADMIN; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_ADMINISTRATOR_USERNAME', NULL, 'required aria-required="true" id="adminUsername" placeholder="'.TEXT_STORE_EMAIL_ADMIN.'"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_EMAIL_ADMIN_HELP; ?></span>
        </div>
      </div>
      
      <div class="form-group has-feedback">
        <label for="adminPassword" class="control-label col-xs-3"><?php echo TEXT_STORE_PASSWORD; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_ADMINISTRATOR_PASSWORD', NULL, 'required aria-required="true" id="adminPassword"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_PASSWORD_HELP; ?></span>
        </div>
      </div>

<?php
/*
  if (osc_is_writable($dir_fs_document_root) && osc_is_writable($dir_fs_document_root . 'admin')) {
?>
      <div class="form-group has-feedback">
        <label for="adminDir" class="control-label col-xs-3"><?php echo TEXT_STORE_DIRECTORY; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_input_field('CFG_ADMIN_DIRECTORY', 'clicShoppingAdmin', 'required aria-required="true" id="adminDir"'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_DIRECTORY_HELP; ?></span>
        </div>
      </div>
<?php
  }
*/
?>
      <div class="form-group has-feedback">
        <label for="Zulu" class="control-label col-xs-3"><?php echo TEXT_STORE_TIME_ZONE; ?></label>
        <div class="col-xs-9">
          <?php echo osc_draw_time_zone_select_menu('CFG_TIME_ZONE'); ?>
          <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
          <span class="help-block"><?php echo TEXT_STORE_TIME_ZONE_HELP; ?></span>
        </div>
      </div>
    </div>
  </div>
  <div class="pull-right" style="padding-bottom: 10px;"><?php echo osc_draw_button('Continue', null, null, 'success', null, null); ?></div>

<?php
      foreach ( $_POST as $key => $value ) {
        if (($key != 'x') && ($key != 'y')) {
          echo osc_draw_hidden_field($key, $value);
        }
      }
?>

    </form>
