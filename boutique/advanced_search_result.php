<?php
/*
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('advanced_search'));

  $error = false;

  if ( (isset($_GET['keywords']) && empty($_GET['keywords'])) &&
       (isset($_GET['dfrom']) && (empty($_GET['dfrom']) || ($_GET['dfrom'] == DOB_FORMAT_STRING))) &&
       (isset($_GET['dto']) && (empty($_GET['dto']) || ($_GET['dto'] == DOB_FORMAT_STRING))) &&
       (isset($_GET['pfrom']) && !is_numeric($_GET['pfrom'])) &&
       (isset($_GET['pto']) && !is_numeric($_GET['pto'])) ) {
    $error = true;

    $OSCOM_MessageStack->addError('search', ERROR_AT_LEAST_ONE_INPUT);

  } else {

    $dfrom = '';
    $dto = '';
    $pfrom = '';
    $pto = '';
    $keywords = '';

    if (isset($_GET['dfrom'])) {
      $dfrom = (($_GET['dfrom'] == DOB_FORMAT_STRING) ? '' : $_GET['dfrom']);
    }

    if (isset($_GET['dto'])) {
      $dto = (($_GET['dto'] == DOB_FORMAT_STRING) ? '' : $_GET['dto']);
    }

    if (isset($_GET['pfrom'])) {
      $pfrom = $_GET['pfrom'];
    }

    if (isset($_GET['pto'])) {
      $pto = $_GET['pto'];
    }

    if (isset($_GET['keywords'])) {
      $keywords = osc_db_prepare_input($_GET['keywords']);
    }

    $date_check_error = false;
    if (osc_not_null($dfrom)) {
      if (!$OSCOM_Date->validate($dfrom, DOB_FORMAT_STRING, $dfrom_array)) {
        $error = true;
        $date_check_error = true;

        $OSCOM_MessageStack->addError('search', ERROR_INVALID_FROM_DATE);
      }
    }

    if (osc_not_null($dto)) {
      if (!$OSCOM_Date->validate($dto, DOB_FORMAT_STRING, $dto_array)) {
        $error = true;
        $date_check_error = true;

        $OSCOM_MessageStack->addError('search', ERROR_INVALID_TO_DATE);
      }
    }

    if (($date_check_error == false) && osc_not_null($dfrom) && osc_not_null($dto)) {
      if (mktime(0, 0, 0, $dfrom_array[1], $dfrom_array[2], $dfrom_array[0]) > mktime(0, 0, 0, $dto_array[1], $dto_array[2], $dto_array[0])) {
        $error = true;

        $OSCOM_MessageStack->addError('search', ERROR_TO_DATE_LESS_THAN_FROM_DATE);
      }
    }

    $price_check_error = false;
    if (osc_not_null($pfrom)) {
      if (!settype($pfrom, 'double')) {
        $error = true;
        $price_check_error = true;

        $OSCOM_MessageStack->addError('search', ERROR_PRICE_FROM_MUST_BE_NUM);
      }
    }

    if (osc_not_null($pto)) {
      if (!settype($pto, 'double')) {
        $error = true;
        $price_check_error = true;

        $OSCOM_MessageStack->addError('search', ERROR_PRICE_TO_MUST_BE_NUM);
      }
    }

    if (($price_check_error == false) && is_float($pfrom) && is_float($pto)) {
      if ($pfrom >= $pto) {
        $error = true;

        $OSCOM_MessageStack->addError('search', ERROR_PRICE_TO_LESS_THAN_PRICE_FROM);
      }
    }

    if (osc_not_null($keywords)) {
      if (!osc_parse_search_string($keywords, $search_keywords)) {
        $error = true;

        $OSCOM_MessageStack->addError('search', ERROR_INVALID_KEYWORDS);
      }
    }
  }

  if (empty($dfrom) && empty($dto) && empty($pfrom) && empty($pto) && empty($keywords)) {
    $error = true;

    $OSCOM_MessageStack->addError('search', ERROR_AT_LEAST_ONE_INPUT);
  }

  if ($error == true) {
    osc_redirect(osc_href_link('advanced_search.php', osc_get_all_get_params(), 'NONSSL', true, false));
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('advanced_search.php'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('advanced_search_result.php', osc_get_all_get_params(), 'NONSSL', true, false));

// create column list
  $define_list = array('PRODUCT_LIST_MODEL' => PRODUCT_LIST_MODEL,
                       'PRODUCT_LIST_NAME' => PRODUCT_LIST_NAME,
                       'PRODUCT_LIST_MANUFACTURER' => PRODUCT_LIST_MANUFACTURER,
                       'PRODUCT_LIST_PRICE' => PRODUCT_LIST_PRICE,
                       'PRODUCT_LIST_QUANTITY' => PRODUCT_LIST_QUANTITY,
                       'PRODUCT_LIST_WEIGHT' => PRODUCT_LIST_WEIGHT,
                       'PRODUCT_LIST_IMAGE' => PRODUCT_LIST_IMAGE,
                       'PRODUCT_LIST_DATE' => PRODUCT_LIST_DATE,
                      );

  asort($define_list);

  $column_list = array();

  foreach($define_list as $key => $value) {
    if ($value > 0) $column_list[] = $key;
  }

  $select_column_list = '';

  for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
    switch ($column_list[$i]) {
      case 'PRODUCT_LIST_MODEL':
        $select_column_list .= 'p.products_model, ';
      break;
      case 'PRODUCT_LIST_MANUFACTURER':
        $select_column_list .= 'm.manufacturers_name, ';
      break;
      case 'PRODUCT_LIST_QUANTITY':
        $select_column_list .= 'p.products_quantity, ';
      break;
      case 'PRODUCT_LIST_IMAGE':
        $select_column_list .= 'p.products_image, ';
      break;
      case 'PRODUCT_LIST_WEIGHT':
        $select_column_list .= 'p.products_weight, ';
      break;
      case 'PRODUCT_LIST_DATE':
        $select_column_list .= 'p.products_date_added, ';
      break;
    }
  }

// Debut des differentes requetes SQL sur la recherche du produit
  if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B
    $select_str = "select distinct " . $select_column_list . " m.manufacturers_id, 
                                                              p.products_id,
                                                              pd.products_name,
                                                              pd.products_head_keywords_tag,
                                                              pd.products_description,
                                                              p.products_ean,
                                                              p.products_price,
                                                              p.products_date_added,
                                                              p.products_tax_class_id,
                                                              g.customers_group_price,
                                                              g.price_group_view,
                                                              g.products_group_view,
                                                              g.orders_group_view,
                                                              IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price,
                                                              IF(s.status, s.specials_new_products_price, p.products_price) as final_price ";
  } else { // Clients Grand Public
    $select_str = "select distinct " . $select_column_list . " m.manufacturers_id, 
                                                              p.products_id,
                                                              pd.products_name,
                                                              pd.products_head_keywords_tag,
                                                              pd.products_description,
                                                              p.products_price,
                                                              p.products_ean,
                                                              p.products_date_added,
                                                              p.products_tax_class_id,
                                                              p.products_view,
                                                              p.orders_view,
                                                              IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price,
                                                              IF(s.status, s.specials_new_products_price, p.products_price) as final_price ";
  }

  if ( (DISPLAY_PRICE_WITH_TAX == 'true') && (osc_not_null($pfrom) || osc_not_null($pto)) ) {
    $select_str .= ", SUM(tr.tax_rate) as tax_rate ";
  }

  if ($OSCOM_Customer->getCustomersGroupID() != 0) {
// Clients en mode B2B
    $from_str = "from products p left join manufacturers m using(manufacturers_id) left join specials s on p.products_id = s.products_id
                  and s.status ='1' 
                  and s.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "' left join products_groups g on p.products_id = g.products_id
                ";
  } else {
// Clients Grand Public
     $from_str = "from products p left join manufacturers m using(manufacturers_id) left join specials s on p.products_id = s.products_id
                  and s.status = '1' 
                  and s.customers_group_id = '0'
                 ";
  }

  if ( (DISPLAY_PRICE_WITH_TAX == 'true') && (osc_not_null($pfrom) || osc_not_null($pto)) ) {
    $from_str .= " left join " . TABLE_TAX_RATES . " tr on p.products_tax_class_id = tr.tax_class_id left join zones_to_geo_zones gz on tr.tax_zone_id = gz.geo_zone_id and (gz.zone_country_id is null or gz.zone_country_id = '0' or gz.zone_country_id = '" . ($OSCOM_Customer->hasDefaultAddress() ? (int)$OSCOM_Customer->getCountryID() : (int)STORE_COUNTRY) . "') and (gz.zone_id is null or gz.zone_id = '0' or gz.zone_id = '" . ($OSCOM_Customer->hasDefaultAddress() ? (int)$OSCOM_Customer->getZoneID() : (int)STORE_ZONE) . "')";
  }

  $from_str .= ", products_description pd, categories c, products_to_categories p2c";

  if ($OSCOM_Customer->getCustomersGroupID() != 0) { // Clients en mode B2B
    $where_str = " where p.products_status = '1' 
                  and g.customers_group_id = '" . (int)$OSCOM_Customer->getCustomersGroupID() . "' 
                  and g.products_group_view = '1' 
                  and p.products_id = pd.products_id 
                  and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                  and p.products_id = p2c.products_id 
                  and p2c.categories_id = c.categories_id 
                  and p.products_archive = '0'
                  and c.virtual_categories = 0
                ";
  } else { // Clients Grand Public
    $where_str = " where p.products_status = '1' 
                  and p.products_view = '1' 
                  and p.products_id = pd.products_id 
                  and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                  and p.products_id = p2c.products_id 
                  and p2c.categories_id = c.categories_id 
                  and p.products_archive = '0'
                  and c.virtual_categories = 0
                 ";
  }

  if (isset($_GET['categories_id']) && osc_not_null($_GET['categories_id'])) {
    if (isset($_GET['inc_subcat']) && ($_GET['inc_subcat'] == '1')) {
      $subcategories_array = array();
      osc_get_subcategories($subcategories_array, $_GET['categories_id']);

      $where_str .= " and p2c.products_id = p.products_id 
                      and p2c.products_id = pd.products_id 
                      and (p2c.categories_id = '" . (int)$_GET['categories_id'] . "'
                    ";

      for ($i=0, $n=sizeof($subcategories_array); $i<$n; $i++ ) {
        $where_str .= " or p2c.categories_id = '" . (int)$subcategories_array[$i] . "'";
      }

      $where_str .= ")";
    } else {
      $where_str .= " and p2c.products_id = p.products_id 
                      and p2c.products_id = pd.products_id 
                      and pd.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                      and p2c.categories_id = '" . (int)$_GET['categories_id'] . "'
                    ";
    }
  }

  if (isset($_GET['manufacturers_id']) && osc_not_null($_GET['manufacturers_id'])) {
    $where_str .= " and m.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "'";
  }

  if (isset($search_keywords) && (sizeof($search_keywords) > 0)) {
    $where_str .= " and (";
      
    for ($i=0, $n=sizeof($search_keywords); $i<$n; $i++ ) {
      switch ($search_keywords[$i]) {
        case '(':
        case ')':
        case 'and':
        case 'or':
          $where_str .= " " . $search_keywords[$i] . " ";
        break;
        default:
          $keyword = osc_db_prepare_input($search_keywords[$i]);

          $where_str .= "(lower(pd.products_name) like '%" . osc_db_input($keyword) . "%'" . ((defined('DB_DATABASE_CHARSET') && DB_DATABASE_CHARSET == 'utf8') ? " COLLATE utf8_bin" : "") . "
                          or lower(p.products_model) like '%" . osc_db_input($keyword) . "%'" . ((defined('DB_DATABASE_CHARSET') && DB_DATABASE_CHARSET == 'utf8') ? " COLLATE utf8_bin" : "") . "
                          or p.products_model like '%" . osc_db_input($keyword) . "%' 
                          or lower(m.manufacturers_name) like '%" . osc_db_input($keyword) . "%'" . ((defined('DB_DATABASE_CHARSET') && DB_DATABASE_CHARSET == 'utf8') ? " COLLATE utf8_bin" : "") . "
                          or lower(pd.products_head_keywords_tag) like '%" . osc_db_input($keyword) . "%'" . ((defined('DB_DATABASE_CHARSET') && DB_DATABASE_CHARSET == 'utf8') ? " COLLATE utf8_bin" : "") . "
                          or p.products_ean like '%" . osc_db_input($keyword) . "%'";
          if (isset($_GET['search_in_description']) && ($_GET['search_in_description'] == '1')) $where_str .= " or lower(pd.products_description) like '%" . osc_db_input($keyword) . "%'" . ((defined('DB_DATABASE_CHARSET') && DB_DATABASE_CHARSET == 'utf8') ? " COLLATE utf8_bin" : "") . "";
          $where_str .= ')';
        break;
      }
    }
    $where_str .= " )";
  }

  if (osc_not_null($dfrom)) {
    $where_str .= " and p.products_date_added >= '" . osc_date_raw($dfrom) . "'";
  }

  if (osc_not_null($dto)) {
    $where_str .= " and p.products_date_added <= '" . osc_date_raw($dto) . "'";
  }

  if (osc_not_null($pfrom)) {
    if ($currencies->is_set($_SESSION['currency'])) {
      $rate = $currencies->get_value($_SESSION['currency']);

      $pfrom = $pfrom / $rate;
    }
  }

  if (osc_not_null($pto)) {
    if (isset($rate)) {
      $pto = $pto / $rate;
    }
  }

  if (DISPLAY_PRICE_WITH_TAX == 'true') {
    if ($pfrom > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) >= " . (double)$pfrom . ")";
    if ($pto > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) <= " . (double)$pto . ")";
  } else {
    if ($pfrom > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) >= " . (double)$pfrom . ")";
    if ($pto > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) <= " . (double)$pto . ")";
  }

  if ( (DISPLAY_PRICE_WITH_TAX == 'true') && (osc_not_null($pfrom) || osc_not_null($pto)) ) {
    $where_str .= " group by p.products_id, tr.tax_priority";
  }

  if ( (!isset($_GET['sort'])) || (!preg_match('/^[1-8][ad]$/', $_GET['sort'])) || (substr($_GET['sort'], 0, 1) > sizeof($column_list)) ) {
    for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
      if ($column_list[$i] == 'PRODUCT_LIST_NAME') {
        $_GET['sort'] = $i+1 . 'a';
        $order_str = " order by p.products_sort_order DESC, pd.products_name";
        break;
      }
    }
  } else {
    $sort_col = substr($_GET['sort'], 0 , 1);
    $sort_order = substr($_GET['sort'], 1);

    switch ($column_list[$sort_col-1]) {
      case 'PRODUCT_LIST_MODEL':
        $order_str .= " order by p.products_model " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
      case 'PRODUCT_LIST_NAME':
        $order_str .= " order by pd.products_name " . ($sort_order == 'd' ? 'desc' : '');
        break;
      case 'PRODUCT_LIST_MANUFACTURER':
        $order_str .= " order by m.manufacturers_name " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
      case 'PRODUCT_LIST_QUANTITY':
        $order_str .= " order by p.products_quantity " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
      case 'PRODUCT_LIST_IMAGE':
        $order_str .= " order by pd.products_name";
        break;
      case 'PRODUCT_LIST_WEIGHT':
        $order_str .= " order by p.products_weight " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
      case 'PRODUCT_LIST_PRICE':
// ########## B2B ##########
//        $order_str = " final_price " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        $order_str .= " order by p.products_price " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
// ####### END - B2B #######
        break;
      case 'PRODUCT_LIST_DATE':
        $order_str .= " order by p.products_date_added " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
        break;
    }
  }

  $listing_sql = $select_str . $from_str . $where_str . $order_str;
  $products_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
// body //-->
  require($OSCOM_Template->getTemplateFiles('advanced_search_result'));
