<?php
/*
 * desjardins_response.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  

*/

  chdir('../../../../');

// set the level of error reporting
  defined( 'E_DEPRECATED' ) ? error_reporting( E_ALL & ~E_NOTICE & ~E_DEPRECATED ) : error_reporting( E_ALL & ~E_NOTICE );


//recuperation des donnees renvoyees par la banque
/*
  $request_method = $_SERVER["REQUEST_METHOD"];
  $wRequestVars = "_" . $request_method;
  $DESJARDINS_bruteVars = ${$wRequestVars};

  foreach ($_REQUEST as $var => $valeur) {
    $DESJARDINS_bruteVars[$var] = $valeur;
  }
*/
/*------------------------------------------------------------*/
/*-------------------- Application top ----------------------*/
/*------------------------------------------------------------ */

// include server parameters
  require('includes/configure.php');

  if ($request_type == 'NONSSL') {
    define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG);
  } else {
    define('DIR_WS_CATALOG', DIR_WS_HTTPS_CATALOG);
  }

// include the list of project filenames
  require('includes/filenames.php');

// include the list of project database tables
  require('includes/database_tables.php');


// define general functions used application-wide
  require('includes/functions/general.php');

// define general add on functions used application-wide
  require('includes/functions/general_addon.php');

// define general functions used  General B2B
  require('includes/functions/general_b2b.php');

// define general functions used  for the fields and button
  require('includes/functions/html_output.php');


// include the database functions
  require('includes/functions/database_' . DB_DATABASE_TYPE . '.php');

// make a connection to the database... now
  osc_db_connect() or die('Unable to connect to database server!');

  require(DIR_WS_CLASSES . 'cache.php');
  $OSCOM_Cache = new cache();

  require(DIR_WS_CLASSES . 'db.php');
  $OSCOM_PDO = db::initialize();

// set the application parameters
  $configuration_query = osc_db_query("select configuration_key as cfgKey,
                                              configuration_value as cfgValue 
                                       from configuration
                                      ");

  while ($configuration = osc_db_fetch_array($configuration_query)) {
    define($configuration['cfgKey'], $configuration['cfgValue']);
  } 

// include shopping cart class
  require(DIR_WS_CLASSES . 'shopping_cart.php');

// define how the session functions will be used
  require('includes/functions/sessions.php');

// set the session name and save path
  session_name('LORsid');
  session_save_path(SESSION_WRITE_DIRECTORY);

// ********************************************************************************
// recuperation de l'id de session contenu dans texte-libre

   session_id($_REQUEST['texte-libre']);

// session_id(substr($_REQUEST['texte-libre'],0,26));
// ********************************************************************************

// Client ip and provider client ip
  $client_computer_ip = osc_get_ip_address();
  $provider_name_client = osc_get_provider_name_client();

// start the session
  $session_started = false;
  osc_session_start();
  $session_started = true;

  if ( ($session_started == true) && (PHP_VERSION >= 4.3) && function_exists('ini_get') && (ini_get('register_globals') == false) ) {
      extract($_SESSION, EXTR_OVERWRITE+EXTR_REFS);
  }

// initialize a session token
  if (!isset($_SESSION['sessiontoken'])) {
    $_SESSION['sessiontoken'] = session_id($_REQUEST['texte-libre']);;
  }

// create the shopping cart & fix the cart if necesary
  require(DIR_WS_CLASSES . 'customer.php');
  $OSCOM_Customer = new customer();

// create the shopping cart
  if (!isset($_SESSION['cart']) || !is_object($_SESSION['cart'])) {
    $_SESSION['cart'] = new shoppingCart;
  }

// include currencies class and create an instance
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

// include the mail classes
  require(DIR_WS_CLASSES . 'mime.php');
  require(DIR_WS_CLASSES . 'email.php');

// set the language
  if (!isset($_SESSION['language']) || isset($_GET['language'])) {
    include(DIR_WS_CLASSES . 'language.php');
    $lng = new language();

    if ( isset($_GET['language']) && !empty($_GET['language']) ) {
      $lng->set_language($_GET['language']);
    } else {
      $lng->get_browser_language();
    }

    $_SESSION['language'] = $lng->language['directory'];
    $_SESSION['languages_id'] = $lng->language['id'];
  } //  end !isset($_SESSION['language']

  if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . '/languages/'. $_SESSION['language']  . '.php') ) {
    require(DIR_WS_TEMPLATE . SITE_THEMA . '/languages/'. $_SESSION['language']   . '.php');
    require(DIR_WS_LANGUAGES . $_SESSION['language'] . '.php');
  } else {
    require(DIR_WS_LANGUAGES . $_SESSION['language'] . '.php');
  }

// currency
  if (!isset($_SESSION['currency']) || isset($_GET['currency']) || ( (USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && (LANGUAGE_CURRENCY != $_SESSION['currency']) ) ) {
    if (isset($_GET['currency']) && $currencies->is_set($_GET['currency'])) {
      $_SESSION['currency'] = $_GET['currency'];
    } else {
      $_SESSION['currency'] = ((USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && $currencies->is_set(LANGUAGE_CURRENCY)) ? LANGUAGE_CURRENCY : DEFAULT_CURRENCY;
    }
  }

 if (file_exists(DIR_FS_CATALOG . DIR_WS_TEMPLATE .  SITE_THEMA . '/languages/'. $_SESSION['language']  . '/' . 'checkout_process.php') ) {
   require(DIR_WS_TEMPLATE . SITE_THEMA . '/languages/'. $_SESSION['language']  . '/' . 'checkout_process.php');
   require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . 'checkout_process.php');
 } else {
   require(DIR_WS_LANGUAGES . $_SESSION['language'] . '/' . 'checkout_process.php');
 }

// Client ip and provider client ip
    $client_computer_ip = osc_get_ip_address();
    $provider_name_client = osc_get_provider_name_client();


/*------------------------------------------------------------*/
/*-------------------- Checkout_process ----------------------*/
/*------------------------------------------------------------ */

// load selected payment module
  include(DIR_WS_MODULES . 'payment/desjardins.php');
  $payment_modules = new desjardins();

// load the selected shipping module
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping($_SESSION['shipping']);

// orders information
  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// require(DIR_WS_CLASSES . 'order_total.php');
// $order_total_modules = new order_total;

// on archive les donnees recues de la banque dans la table desjardins_response
  $sql_data_array = array('ref_number' => $_REQUEST['reference'],
                          'MAC' => $_REQUEST['MAC'],
                          'TPE' => $_REQUEST['TPE'],
                          'date' => $_REQUEST['date'],
                          'montant' => $_REQUEST['montant'],
                          'texte_libre' => $_REQUEST['texte-libre'],
                          'code_retour' => $_REQUEST['code-retour'],
                          'retourPLUS' => $_REQUEST['retourPLUS']
                        );

  $OSCOM_PDO->save('desjardins_response', $sql_data_array);

  require(DIR_WS_CLASSES . 'order_total.php');
  $order_total_modules = new order_total;

// Begin Main : Retrieve Variables posted by DESJARDINS Payment Server 
// $DESJARDINS_bruteVars = getMethodeDesjardins();

// TPE init variables
  $oTpe = new desjardins_Tpe();
  $oHmac = new desjardins_Hmac($oTpe);

// Message Authentication
  $cgi2_fields = sprintf(DESJARDINS_CGI2_FIELDS, $oTpe->sNumero,
                                            $_REQUEST["date"],
                                            $_REQUEST['montant'],
                                            $_REQUEST['reference'],
                                            $_REQUEST['texte-libre'],
                                            $oTpe->sVersion,
                                            $_REQUEST['code-retour'],
                                            $_REQUEST['cvx'],
                                            $_REQUEST['vld'],
                                            $_REQUEST['brand'],
                                            $_REQUEST['status3ds'],
                                            $_REQUEST['numauto'],
                                            $_REQUEST['motifrefus'],
                                            $_REQUEST['originecb'],
                                            $_REQUEST['bincb'],
                                            $_REQUEST['hpancb'],
                                            $_REQUEST['ipclient'],
                                            $_REQUEST['originetr'],
                                            $_REQUEST['veres'],
                                            $_REQUEST['pares']
                     );

  if ($oHmac->computeHmac($cgi2_fields) == strtolower($_REQUEST['MAC'])) {
    switch($_REQUEST['code-retour']) {
      case "Annulation" :
// Payment has been refused
// Attention : an autorization may still be delivered for this payment
         osc_mail(STORE_NAME . ': CYBERMUT', STORE_OWNER_EMAIL_ADDRESS , "order cancelled : " . $_REQUEST['reference'], "This order has been cancelled", STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);
      break;

      case "payetest":
//----------------------------------------------------------------------------------------------------------------//
// Payment has been accepeted on the test server
//----------------------------------------------------------------------------------------------------------------//
      if (MODULE_PAYMENT_DESJARDINS_SIMULATION == 'True') {

        $order_totals = $order_total_modules->process();

        $sql_data_array = array('customers_id' => $OSCOM_Customer->getID(),
                                'customers_group_id' => $order->customer['group_id'],
                                'customers_name' => $order->customer['firstname'] . ' ' . $order->customer['lastname'],
                                'customers_company' => $order->customer['company'],
                                'customers_street_address' => $order->customer['street_address'],
                                'customers_suburb' => $order->customer['suburb'],
                                'customers_city' => $order->customer['city'],
                                'customers_postcode' => $order->customer['postcode'], 
                                'customers_state' => $order->customer['state'], 
                                'customers_country' => $order->customer['country']['title'], 
                                'customers_telephone' => $order->customer['telephone'], 
                                'customers_email_address' => $order->customer['email_address'],
                                'customers_address_format_id' => $order->customer['format_id'], 
                                'delivery_name' => trim($order->delivery['firstname'] . ' ' . $order->delivery['lastname']),
                                'delivery_company' => $order->delivery['company'],
                                'delivery_street_address' => $order->delivery['street_address'], 
                                'delivery_suburb' => $order->delivery['suburb'], 
                                'delivery_city' => $order->delivery['city'], 
                                'delivery_postcode' => $order->delivery['postcode'], 
                                'delivery_state' => $order->delivery['state'], 
                                'delivery_country' => $order->delivery['country']['title'], 
                                'delivery_address_format_id' => $order->delivery['format_id'], 
                                'billing_name' => $order->billing['firstname'] . ' ' . $order->billing['lastname'], 
                                'billing_company' => $order->billing['company'],
                                'billing_street_address' => $order->billing['street_address'], 
                                'billing_suburb' => $order->billing['suburb'], 
                                'billing_city' => $order->billing['city'], 
                                'billing_postcode' => $order->billing['postcode'], 
                                'billing_state' => $order->billing['state'], 
                                'billing_country' => $order->billing['country']['title'], 
                                'billing_address_format_id' => $order->billing['format_id'], 
                                'payment_method' => $order->info['payment_method'], 
                                'cc_type' => $order->info['cc_type'], 
                                'cc_owner' => $cc_owner, 
                                'cc_number' => $order->info['cc_number'], 
                                'cc_expires' => $order->info['cc_expires'], 
                                'date_purchased' => 'now()', 
                                'orders_status' => $order->info['order_status'],
                                'orders_status_invoice' => $order->info['order_status_invoice'], 
                                'currency' => $order->info['currency'], 
                                'currency_value' => $order->info['currency_value'],
                                'client_computer_ip' => $client_computer_ip,
                                'provider_name_client' => $provider_name_client,
                                'customers_cellular_phone' => $order->customer['cellular_phone']
                               );

// recuperation des informations societes pour les clients B2B (voir fichier classes/order.php)
        if ($OSCOM_Customer->getCustomersGroupID() != 0) {
          $sql_data_array['customers_siret'] = $order->customer['siret'];
          $sql_data_array['customers_ape'] = $order->customer['ape'];
          $sql_data_array['customers_tva_intracom'] = $order->customer['tva_intracom'];
        }

        $OSCOM_PDO->save('orders', $sql_data_array);

        $insert_id = $OSCOM_PDO->lastInsertId();

// insert the general condition of sales
        $page_manager_general_condition = osc_page_manager_general_condition();

        $sql_data_array = array('orders_id' => (int)$insert_id,
                                 'customers_id' => (int)$OSCOM_Customer->getID(),
                                 'page_manager_general_condition' => $page_manager_general_condition
                                );

        $OSCOM_PDO->save('orders_pages_manager', $sql_data_array);

// orders total
        for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
          $sql_data_array = array('orders_id' => $insert_id,
                                  'title' => $order_totals[$i]['title'],
                                  'text' => $order_totals[$i]['text'],
                                  'value' => $order_totals[$i]['value'],
                                  'class' => $order_totals[$i]['code'],
                                  'sort_order' => $order_totals[$i]['sort_order']);
          $OSCOM_PDO->save('orders_total', $sql_data_array);
        }

        $customer_notification = (SEND_EMAILS == 'true') ? '1' : '0';

        $comment = $order->info['comments'];
        $comment .=  "\n" . "\n";
        $comment .= '------------ Info DESJARDINS ---------- ' . "\n" . "\n";
        $comment .= 'Ref Number : ' . $_REQUEST['reference'] . "\n";
//         $comment .= 'MAC : ' . $_REQUEST['MAC'] . "\n";
        $comment .= 'Date : ' .  $_REQUEST['date'] . "\n";
        $comment .= 'ClicShopping Session : ' .  $_REQUEST['texte-libre'] . "\n";

        $sql_data_array = array('orders_id' => $insert_id,
                                'orders_status_id' => $order->info['order_status'],
                                'orders_status_invoice_id' => $order->info['order_status_invoice'],
                                'date_added' => 'now()',
                                'customer_notified' => $customer_notification,
                                'comments' => $comment
                               );
        $OSCOM_PDO->save('orders_status_history', $sql_data_array);

// discount coupons
        if( isset($_SESSION['coupon']) && is_object( $order->coupon ) ) {
          $sql_data_array = array( 'coupons_id' => $order->coupon->coupon['coupons_id'],
                                  'orders_id' => $insert_id );

          $OSCOM_PDO->save('discount_coupons_to_orders', $sql_data_array);
        }

// initialized for the email confirmation
        $products_ordered = '';

        for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
// Stock Update
          if (STOCK_LIMITED == 'true') {
            if (DOWNLOAD_ENABLED == 'true') {
              $stock_query_raw = "SELECT products_quantity,
                                         pad.products_attributes_filename 
                                  FROM products p
                                    LEFT JOIN products_attributes pa ON p.products_id=pa.products_id
                                    LEFT JOIN products_attributes_download pad  ON pa.products_attributes_id=pad.products_attributes_id
                                  WHERE p.products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                                ";
// Will work with only one option for downloadable products
// otherwise, we have to build the query dynamically with a loop
              $products_attributes = (isset($order->products[$i]['attributes'])) ? $order->products[$i]['attributes'] : '';

              if (is_array($products_attributes)) {
                 $stock_query_raw .= " AND pa.options_id = '" . (int)$products_attributes[0]['option_id'] . "'
                                      AND pa.options_values_id = '" . (int)$products_attributes[0]['value_id'] . "'
                                     ";
              }
              $stock_query = osc_db_query($stock_query_raw);
            } else {
              $stock_query = osc_db_query("select products_quantity,
                                                  products_quantity_alert
                                           from products
                                           where products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                                          ");
            } // end DOWNLOAD_ENABLED

            if (osc_db_num_rows($stock_query) > 0) {
              $stock_values = osc_db_fetch_array($stock_query);
// do not decrement quantities if products_attributes_filename exists

              if ((DOWNLOAD_ENABLED != 'true') || (!$stock_values['products_attributes_filename'])) {
// select the good qty in B2B ti decrease the stock. See shopping_cart top display out stock or not
                if ($OSCOM_Customer->getCustomersGroupID() != '0') {

                  $productsQuantityCustomersGroup = $OSCOM_PDO->prepare('select products_quantity_fixed_group
                                                                          from :table_products_groups
                                                                          where products_id = :products_id
                                                                          and customers_group_id =  :customers_group_id                                   
                                                                  ');
                  $productsQuantityCustomersGroup->bindInt(':products_id', osc_get_prid($order->products[$i]['id']) );
                  $productsQuantityCustomersGroup->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID()  );
                  $productsQuantityCustomersGroup->execute();

                  $products_quantity_customers_group = $productsQuantityCustomersGroup->fetch();

// do the exact qty in function the customer group and product          
                  $products_quantity_customers_group[$i] = $products_quantity_customers_group['products_quantity_fixed_group'];
                } else {
                  $products_quantity_customers_group[$i] = 1;
                } // end $OSCOM_Customer->getCustomersGroupID()

                $stock_left = $stock_values['products_quantity'] - ($order->products[$i]['qty'] * $products_quantity_customers_group[$i]);
              } else {
                $stock_left = $stock_values['products_quantity'];
                $stock_products_quantity_alert = $stock_values['products_quantity_alert'];
              } // end DOWNLOAD_ENABLED

// alert an email if the product stock is < stock reorder level
// Alert by mail if a product is 0 or < 0
              if (STOCK_ALERT_PRODUCT_REORDER_LEVEL == 'true') {
                if ((STOCK_ALLOW_CHECKOUT == 'false') && (STOCK_CHECK == 'true')) {
                  $warning_stock = STOCK_REORDER_LEVEL;
                  $current_stock = $stock_left;

// alert email if stock product alert < warning stock
                  if (($stock_products_quantity_alert <= $warning_stock) && ($stock_products_quantity_alert != '0')) {

                    $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_ALERT_STOCK);
                    $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
                    $reorder_stock_email = stripslashes(EMAIL_REORDER_LEVEL_TEXT_ALERT_STOCK);
                    $reorder_stock_email = html_entity_decode($reorder_stock_email);
                    $reorder_stock_email .= "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" .   EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id'])  .  "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_URL . '</strong>' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php' . '?products_id=' . $order->products[$i]['id'] . "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_ALERT_STOCK . $stock_products_quantity_alert  .'</strong>';

                    osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $reorder_stock_email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
                  }

                  if ($current_stock <= $warning_stock) {

                    $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_STOCK);
                    $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
                    $reorder_stock_email = stripslashes(EMAIL_REORDER_LEVEL_TEXT_STOCK);
                    $reorder_stock_email = html_entity_decode($reorder_stock_email);
                    $reorder_stock_email .= "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" .   EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id'])  .  "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_URL . '</strong>' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php' . '?products_id=' . $order->products[$i]['id'] . "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_STOCK . $current_stock  .'</strong>';

                    osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $reorder_stock_email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
                  }          
                } // end STOCK_ALLOW_CHECKOUT
              } // end STOCK_ALERT_PRODUCT_REORDER_LEVEL

              osc_db_query("update products set products_quantity = '" . (int)$stock_left . "'
                            where products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                           ");

              if ( ($stock_left < 1) && (STOCK_ALLOW_CHECKOUT == 'false') ) {

                $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                                set products_status = :products_status
                                                where products_id = :products_id
                                              ');
                $Qupdate->bindValue(':products_status', '0');
                $Qupdate->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
                $Qupdate->execute();
                            
              }

// Alert by mail product exhausted if a product is 0 or < 0
              if (STOCK_ALERT_PRODUCT_EXHAUSTED == 'true') {
                if (($stock_left < 1) && (STOCK_ALLOW_CHECKOUT == 'false') && (STOCK_CHECK == 'true')) {

                  $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_STOCK);
                  $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
                  $email_product_exhausted_stock = stripslashes(EMAIL_TEXT_STOCK);
                  $email_product_exhausted_stock = html_entity_decode($email_product_exhausted_stock);
                  $email_product_exhausted_stock .=  "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" . EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id']) .  "\n";

                  osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $email_product_exhausted_stock, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
                }
              } // end STOCK_ALERT_PRODUCT_EXHAUSTED
            } // end osc_db_num_rows($stock_query)
          } // end

// Update products_ordered (for bestsellers list)
          osc_db_query("update products
                        set products_ordered = products_ordered + " . sprintf('%d', $order->products[$i]['qty']) . "
                        where products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                      ");

// search the good model
    if ($OSCOM_Customer->getCustomersGroupID() != '0') {
      $QproductsModuleCustomersGroup = $OSCOM_PDO->prepare('select products_model_group
                                                              from :table_products_groups
                                                              where products_id = :products_id
                                                              and customers_group_id =  :customers_group_id
                                                            ');
      $QproductsModuleCustomersGroup->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
      $QproductsModuleCustomersGroup->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
      $QproductsModuleCustomersGroup->execute();

      $productsModuleCustomersGroup = $QproductsModuleCustomersGroup->fetch();

      $products_model = $productsModuleCustomersGroup['products_model_group'];

      if (empty($products_model)) $products_model = $order->products[$i]['model'];

    } else {
      $products_model = $order->products[$i]['model'];
    }


            $sql_data_array = array('orders_id' => $insert_id,
                                    'products_id' => osc_get_prid($order->products[$i]['id']),
                                    'products_model' => $products_model,
                                    'products_name' => $order->products[$i]['name'],
                                    'products_price' => $order->products[$i]['price'],
                                    'final_price' => $order->products[$i]['final_price'],
                                    'products_tax' => $order->products[$i]['tax'],
                                    'products_quantity' => $order->products[$i]['qty'],
                                    'products_full_id' => $order->products[$i]['id']
                                    );
            $OSCOM_PDO->save('orders_products', $sql_data_array);

            $order_products_id = $OSCOM_PDO->lastInsertId();
 // ---------------------------------------------
 // ------   insert customer choosen option to order  --------------
 // ---------------------------------------------
          $attributes_exist = '0';
          $products_ordered_attributes = '';
          if (isset($order->products[$i]['attributes'])) {
            $attributes_exist = '1';
            for ($j = 0, $n2 = sizeof($order->products[$i]['attributes']); $j < $n2; $j++) {
              if (DOWNLOAD_ENABLED == 'true') {
                $attributes_query = "select popt.products_options_name, 
                                            poval.products_options_values_name, 
                                            pa.options_values_price, 
                                            pa.price_prefix, 
                                            pad.products_attributes_maxdays, 
                                            pad.products_attributes_maxcount, 
                                        pad.products_attributes_filename, 
                                        pa.products_attributes_reference
                                     from products_options popt,
                                          products_options_values poval,
                                          products_attributes pa
                                     left join products_attributes_download pad on pa.products_attributes_id=pad.products_attributes_id
                                     where pa.products_id = '" . (int)$order->products[$i]['id'] . "' 
                                     and pa.options_id = '" . (int)$order->products[$i]['attributes'][$j]['option_id'] . "' 
                                     and pa.options_id = popt.products_options_id 
                                     and pa.options_values_id = '" . (int)$order->products[$i]['attributes'][$j]['value_id'] . "' 
                                     and pa.options_values_id = poval.products_options_values_id 
                                     and popt.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                                     and poval.language_id = '" . (int)$_SESSION['languages_id'] . "'";
                $attributes = osc_db_query($attributes_query);
              } else {
                $attributes = osc_db_query("select popt.products_options_name, 
                                                   poval.products_options_values_name, 
                                                   pa.options_values_price, 
                                               pa.price_prefix,
                                               pa.products_attributes_reference
                                            from products_options popt,
                                                 products_options_values poval,
                                                 products_attributes pa
                                            where pa.products_id = '" . (int)$order->products[$i]['id'] . "' 
                                            and pa.options_id = '" . (int)$order->products[$i]['attributes'][$j]['option_id'] . "' 
                                            and pa.options_id = popt.products_options_id 
                                            and pa.options_values_id = '" . (int)$order->products[$i]['attributes'][$j]['value_id'] . "' 
                                            and pa.options_values_id = poval.products_options_values_id 
                                            and popt.language_id = '" . (int)$_SESSION['languages_id'] . "' 
                                            and poval.language_id = '" . (int)$_SESSION['languages_id'] . "'
                                           ");
              } // end DOWNLOAD_ENABLED
           
              $attributes_values = osc_db_fetch_array($attributes);

              $sql_data_array = array('orders_id' => $insert_id,
                                      'orders_products_id' => $order_products_id,
                                      'products_options' => $attributes_values['products_options_name'],
                                      'products_options_values' => $attributes_values['products_options_values_name'],
                                      'options_values_price' => $attributes_values['options_values_price'],
                                      'price_prefix' => $attributes_values['price_prefix'],
                                      'products_attributes_reference' => $attributes_values['products_attributes_reference']);
              $OSCOM_PDO->save('orders_products_attributes', $sql_data_array);

              if ((DOWNLOAD_ENABLED == 'true') && isset($attributes_values['products_attributes_filename']) && osc_not_null($attributes_values['products_attributes_filename'])) {
                $sql_data_array = array('orders_id' => $insert_id,
                                        'orders_products_id' => $order_products_id,
                                        'orders_products_filename' => $attributes_values['products_attributes_filename'],
                                        'download_maxdays' => $attributes_values['products_attributes_maxdays'],
                                        'download_count' => $attributes_values['products_attributes_maxcount']);
                $OSCOM_PDO->save('orders_products_download', $sql_data_array);
              } // end DOWNLOAD_ENABLED

              $products_ordered_attributes .= "\n\t". $attributes_values['products_attributes_reference']. ' - ' . $attributes_values['products_options_name'] . ' ' . $attributes_values['products_options_values_name'];
            }  // end for
          }  // end $order->products[$i]['attributes'])
 
 // ------insert customer choosen option eof ----
          $total_weight += ($order->products[$i]['qty'] * $order->products[$i]['weight']);
          $total_tax += osc_calculate_tax($total_products_price, $products_tax) * $order->products[$i]['qty'];
          $total_cost += $total_products_price;
          $products_ordered .= $order->products[$i]['qty'] . ' x ' . $order->products[$i]['name'] . ' (' . $order->products[$i]['model'] . ') = ' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . $products_ordered_attributes . "\n";
        } // end for


 // ---------------------------------------------
 // ------             EMAIL SENT  --------------
 // ---------------------------------------------

// lets start with the email confirmation
      $message_order = stripslashes(EMAIL_TEXT_ORDER_NUMBER) . ' ' . $insert_id . "\n" . stripslashes(EMAIL_TEXT_INVOICE_URL);
      $message_order = $message_order;
      $email_order .= $message_order . ' ' . osc_href_link('account_history_info.php', 'order_id=' . $insert_id, 'SSL', false) . "\n" .  EMAIL_TEXT_DATE_ORDERED . ' ' . strftime(DATE_FORMAT_LONG) . "\n\n";

      if ($order->info['comments']) {
// pb utf8 avec le &     et "
        $email_order .= osc_db_output($order->info['comments']) . "\n\n";
      }

      $message_order = tripslashes(EMAIL_TEXT_PRODUCTS);
      $email_order .= html_entity_decode($message_order) . "\n" . 
                      EMAIL_SEPARATOR . "\n" . 
                      $products_ordered . 
                      EMAIL_SEPARATOR . "\n";

      for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
        $email_order .= strip_tags($order_totals[$i]['title']) . ' ' . strip_tags($order_totals[$i]['text']) . "\n";
      }

      if ($order->content_type != 'virtual') {
        $message_order = stripslashes(EMAIL_TEXT_DELIVERY_ADDRESS);   
        $email_order .= "\n" . html_entity_decode($message_order) . "\n" . 
                        EMAIL_SEPARATOR . "\n" .
                        osc_address_label($OSCOM_Customer->getID(), $_SESSION['sendto'], 0, '', "\n") . "\n";
      }

      $message_order = stripslashes(EMAIL_TEXT_BILLING_ADDRESS); 
      $email_order .= "\n" .html_entity_decode($message_order) . "\n" .
                      EMAIL_SEPARATOR . "\n" .
                      osc_address_label($OSCOM_Customer->getID(), $_SESSION['billto'], 0, '', "\n") . "\n\n";

      if (is_object($$_SESSION['payment'])) {
        $message_order = stripslashes(EMAIL_TEXT_PAYMENT_METHOD);    
        $email_order .= html_entity_decode($message_order) . "\n" . 
                        EMAIL_SEPARATOR . "\n";
        $payment_class = $$_SESSION['payment'];
        $email_order .= $order->info['payment_method'] . "\n\n";
        if (isset($payment_class->email_footer)) {
          $email_order .= $payment_class->email_footer;
        }
      } // end is_object($$_SESSION['payment'])

      $message_order = stripslashes(EMAIL_TEXT_FOOTER); 
      $email_order .= html_entity_decode($message_order);
      
      osc_mail($order->customer['firstname'] . ' ' . $order->customer['lastname'], $order->customer['email_address'], EMAIL_TEXT_SUBJECT, $email_order, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

// send emails to other people
       if (SEND_EXTRA_ORDER_EMAILS_TO != '') {
        $email_text_subject = stripslashes(EMAIL_TEXT_SUBJECT);
        $email_text_subject =html_entity_decode($email_text_subject);

        $text = extract_email_address (SEND_EXTRA_ORDER_EMAILS_TO);

        foreach($text as $email){
          osc_mail('', $email, $email_text_subject, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
        }
      }


/* test value return
     print_r($oHmac->computeHmac($cgi2_fields) . '       '  . strtolower($_REQUEST['MAC'] . '     '));
     print_r('session : '.session_id($_REQUEST['texte-libre']). '      ');
*/


// load the after_process function from the payment modules
        $payment_modules->after_process();

        $_SESSION['cart']->reset(true);

// unregister session variables used during checkout
        unset($_SESSION['sendto']);
        unset($_SESSION['billto']);
        unset($_SESSION['shipping']);
        unset($_SESSION['payment']);
        unset($_SESSION['comments']);
        unset($_SESSION['coupon']);

// ---------------------------------------------
// ----------------  SAVE ORDER  --------------
// ---------------------------------------------

       $order_id = $insert_id;
       $_SESSION['order_id'];

        $sql_data_array = array('order_id' => $order_id);
        $OSCOM_PDO->save('desjardins_reference', $sql_data_array, array('ref_number' => $_REQUEST['reference']));

      } else {

        osc_mail(STORE_NAME . ': CYBERMUT', STORE_OWNER_EMAIL_ADDRESS , "order tested : " . $_REQUEST['reference'],
        "Simple test. This order has been tested but not saved in the backoffice. Please, change your simulation mode in true", STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

        $_SESSION['cart']->reset(true);

      } // end MODULE_PAYMENT_DESJARDINS_SIMULATION

      break;

      case "paiement":
//----------------------------------------------------------------------------------------------------------------//
// Payment has been accepeted on the production server 
//----------------------------------------------------------------------------------------------------------------//

      $order_totals = $order_total_modules->process();

      $sql_data_array = array('customers_id' => $OSCOM_Customer->getID(),
                          'customers_group_id' => $order->customer['group_id'],
                          'customers_name' => $order->customer['firstname'] . ' ' . $order->customer['lastname'],
                          'customers_company' => $order->customer['company'],
                          'customers_street_address' => $order->customer['street_address'],
                          'customers_suburb' => $order->customer['suburb'],
                          'customers_city' => $order->customer['city'],
                          'customers_postcode' => $order->customer['postcode'], 
                          'customers_state' => $order->customer['state'], 
                          'customers_country' => $order->customer['country']['title'], 
                          'customers_telephone' => $order->customer['telephone'], 
                          'customers_email_address' => $order->customer['email_address'],
                          'customers_address_format_id' => $order->customer['format_id'], 
                          'delivery_name' => trim($order->delivery['firstname'] . ' ' . $order->delivery['lastname']),
                          'delivery_company' => $order->delivery['company'],
                          'delivery_street_address' => $order->delivery['street_address'], 
                          'delivery_suburb' => $order->delivery['suburb'], 
                          'delivery_city' => $order->delivery['city'], 
                          'delivery_postcode' => $order->delivery['postcode'], 
                          'delivery_state' => $order->delivery['state'], 
                          'delivery_country' => $order->delivery['country']['title'], 
                          'delivery_address_format_id' => $order->delivery['format_id'], 
                          'billing_name' => $order->billing['firstname'] . ' ' . $order->billing['lastname'], 
                          'billing_company' => $order->billing['company'],
                          'billing_street_address' => $order->billing['street_address'], 
                          'billing_suburb' => $order->billing['suburb'], 
                          'billing_city' => $order->billing['city'], 
                          'billing_postcode' => $order->billing['postcode'], 
                          'billing_state' => $order->billing['state'], 
                          'billing_country' => $order->billing['country']['title'], 
                          'billing_address_format_id' => $order->billing['format_id'], 
                          'payment_method' => $order->info['payment_method'], 
                          'cc_type' => $order->info['cc_type'], 
                          'cc_owner' => $cc_owner, 
                          'cc_number' => $order->info['cc_number'], 
                          'cc_expires' => $order->info['cc_expires'], 
                          'date_purchased' => 'now()', 
                          'orders_status' => $order->info['order_status'],
                          'orders_status_invoice' => $order->info['order_status_invoice'],
                          'currency' => $order->info['currency'], 
                          'currency_value' => $order->info['currency_value'],
                          'client_computer_ip' => $client_computer_ip,
                          'provider_name_client' => $provider_name_client,
                          'customers_cellular_phone' => $order->customer['cellular_phone']
                         );

// recuperation des informations societes pour les clients B2B (voir fichier classes/order.php)
      if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        $sql_data_array['customers_siret'] = $order->customer['siret'];
        $sql_data_array['customers_ape'] = $order->customer['ape'];
        $sql_data_array['customers_tva_intracom'] = $order->customer['tva_intracom'];
      }

      $OSCOM_PDO->save('orders', $sql_data_array);

      $insert_id = $OSCOM_PDO->lastInsertId();

// insert the general condition of sales
      $page_manager_general_condition = osc_page_manager_general_condition();

      $sql_data_array = array('orders_id' => (int)$insert_id,
                          'customers_id' => (int)$OSCOM_Customer->getID(),   
                          'page_manager_general_condition' => $page_manager_general_condition 
                         );

      $OSCOM_PDO->save('orders_pages_manager', $sql_data_array);

// orders total
      for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
        $sql_data_array = array('orders_id' => $insert_id,
                                'title' => $order_totals[$i]['title'],
                                'text' => $order_totals[$i]['text'],
                                'value' => $order_totals[$i]['value'], 
                                'class' => $order_totals[$i]['code'], 
                                'sort_order' => $order_totals[$i]['sort_order']);
        $OSCOM_PDO->save('orders_total', $sql_data_array);
      }

      $customer_notification = (SEND_EMAILS == 'true') ? '1' : '0';

      $comment = $order->info['comments'];
      $comment .=  "\n" . "\n";
      $comment .= '------------ Info DESJARDINS ---------- ' . "\n" . "\n";
      $comment .= 'Ref Number transaction : ' . $_REQUEST['reference'] . "\n";
//      $comment .= 'MAC : ' . $_REQUEST['MAC'] . "\n";
      $comment .= 'Date : ' .  $_REQUEST['date'] . "\n";
//      $comment .= 'Texte_libre : ' .  $_REQUEST['texte-libre'] . "\n";

      $sql_data_array = array('orders_id' => $insert_id, 
                              'orders_status_id' => $order->info['order_status'], 
                              'orders_status_invoice_id' => $order->info['order_status_invoice'],
                              'date_added' => 'now()', 
                              'customer_notified' => $customer_notification,
                              'comments' => $comment);
      $OSCOM_PDO->save('orders_status_history', $sql_data_array);

// discount coupons
      if( isset($_SESSION['coupon']) && is_object( $order->coupon ) ) {
        $sql_data_array = array( 'coupons_id' => $order->coupon->coupon['coupons_id'],
                                 'orders_id' => $insert_id );
        $OSCOM_PDO->save('discount_coupons_to_orders', $sql_data_array);
      }

// initialized for the email confirmation
      $products_ordered = '';
      $subtotal = 0;
      $total_tax = 0;

      for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
// Stock Update
        if (STOCK_LIMITED == 'true') {
          if (DOWNLOAD_ENABLED == 'true') {
            $stock_query_raw = "SELECT products_quantity, 
                                       pad.products_attributes_filename 
                                FROM products p
                                  LEFT JOIN products_attributes pa ON p.products_id=pa.products_id
                                  LEFT JOIN products_attributes_download pad  ON pa.products_attributes_id=pad.products_attributes_id
                                WHERE p.products_id = '" . osc_get_prid($order->products[$i]['id']) . "'";
// Will work with only one option for downloadable products
// otherwise, we have to build the query dynamically with a loop
            $products_attributes = (isset($order->products[$i]['attributes'])) ? $order->products[$i]['attributes'] : '';

            if (is_array($products_attributes)) {
              $stock_query_raw .= " AND pa.options_id = '" . (int)$products_attributes[0]['option_id'] . "' 
                                    AND pa.options_values_id = '" . (int)$products_attributes[0]['value_id'] . "'
                                   ";
            }
            $stock_query = osc_db_query($stock_query_raw);
          } else {
            $stock_query = osc_db_query("select products_quantity,
                                                products_quantity_alert
                                         from products
                                         where products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                                        ");
          } // end DOWNLOAD_ENABLED

          if (osc_db_num_rows($stock_query) > 0) {
            $stock_values = osc_db_fetch_array($stock_query);
// do not decrement quantities if products_attributes_filename exists

            if ((DOWNLOAD_ENABLED != 'true') || (!$stock_values['products_attributes_filename'])) {
// select the good qty in B2B ti decrease the stock. See shopping_cart top display out stock or not
              if ($OSCOM_Customer->getCustomersGroupID() != '0') {

                $productsQuantityCustomersGroup = $OSCOM_PDO->prepare('select products_quantity_fixed_group
                                                                        from :table_products_groups
                                                                        where products_id = :products_id
                                                                        and customers_group_id =  :customers_group_id                                   
                                                                      ');
                $productsQuantityCustomersGroup->bindInt(':products_id', osc_get_prid($order->products[$i]['id']) );
                $productsQuantityCustomersGroup->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID()  );
                $productsQuantityCustomersGroup->execute();

                $products_quantity_customers_group = $productsQuantityCustomersGroup->fetch();

// do the exact qty in function the customer group and product          
                $products_quantity_customers_group[$i] = $products_quantity_customers_group['products_quantity_fixed_group'];
              } else {
                $products_quantity_customers_group[$i] = 1;
              } // end $OSCOM_Customer->getCustomersGroupID()

              $stock_left = $stock_values['products_quantity'] - ($order->products[$i]['qty'] * $products_quantity_customers_group[$i]);
            } else {
              $stock_left = $stock_values['products_quantity'];
              $stock_products_quantity_alert = $stock_values['products_quantity_alert'];
            } // end DOWNLOAD_ENABLED

// alert an email if the product stock is < stock reorder level
// Alert by mail if a product is 0 or < 0
            if (STOCK_ALERT_PRODUCT_REORDER_LEVEL == 'true') {
              if ((STOCK_ALLOW_CHECKOUT == 'false') && (STOCK_CHECK == 'true')) {
                $warning_stock = STOCK_REORDER_LEVEL;
                $current_stock = $stock_left;

// alert email if stock product alert < warning stock
                if (($stock_products_quantity_alert <= $warning_stock) && ($stock_products_quantity_alert != '0')) {

                  $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_ALERT_STOCK);
                  $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
                  $reorder_stock_email = stripslashes(EMAIL_REORDER_LEVEL_TEXT_ALERT_STOCK);
                  $reorder_stock_email = html_entity_decode($reorder_stock_email);
                  $reorder_stock_email .= "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" .   EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id'])  .  "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_URL . '</strong>' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php' . '?products_id=' . $order->products[$i]['id'] . "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_ALERT_STOCK . $stock_products_quantity_alert  .'</strong>';

                  osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $reorder_stock_email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
                }

                if ($current_stock <= $warning_stock) {

                  $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_STOCK);
                  $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
                  $reorder_stock_email = stripslashes(EMAIL_REORDER_LEVEL_TEXT_STOCK);
                  $reorder_stock_email = html_entity_decode($reorder_stock_email);
                  $reorder_stock_email .= "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" .   EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id'])  .  "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_URL . '</strong>' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php' . '?products_id=' . $order->products[$i]['id'] . "\n" . '<strong>' . EMAIL_TEXT_PRODUCT_STOCK . $current_stock  .'</strong>';

                  osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $reorder_stock_email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
                }          
              } // end STOCK_ALLOW_CHECKOUT
            } //  end STOCK_ALERT_PRODUCT_REORDER_LEVEL

            osc_db_query("update products set products_quantity = '" . (int)$stock_left . "'
                          where products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                         ");

            if ( ($stock_left < 1) && (STOCK_ALLOW_CHECKOUT == 'false') ) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                          set products_status = :products_status
                                          where products_id = :products_id
                                        ');
          $Qupdate->bindValue(':products_status', '0');
          $Qupdate->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
          $Qupdate->execute();

        }

// Alert by mail product exhausted if a product is 0 or < 0
            if (STOCK_ALERT_PRODUCT_EXHAUSTED == 'true') {
              if (($stock_left < 1) && (STOCK_ALLOW_CHECKOUT == 'false') && (STOCK_CHECK == 'true')) {

                $email_text_subject_stock = stripslashes(EMAIL_TEXT_SUBJECT_STOCK);
                $email_text_subject_stock = html_entity_decode($email_text_subject_stock);
                $email_product_exhausted_stock = stripslashes(EMAIL_TEXT_STOCK);
                $email_product_exhausted_stock = html_entity_decode($email_product_exhausted_stock);
                $email_product_exhausted_stock .=  "\n"  . EMAIL_TEXT_DATE_ALERT . ' ' . strftime(DATE_FORMAT_LONG) .  "\n" . EMAIL_TEXT_MODEL  . $order->products[$i]['model']  .  "\n" . EMAIL_TEXT_PRODUCTS_NAME . $order->products[$i]['name']  .  "\n" . EMAIL_TEXT_ID_PRODUCT .  osc_get_prid($order->products[$i]['id']) .  "\n";

                osc_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, $email_text_subject_stock, $email_product_exhausted_stock, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS); 
              }
            } // end STOCK_ALERT_PRODUCT_EXHAUSTED
          } // end osc_db_num_rows($stock_query)
        } // end

// Update products_ordered (for bestsellers list)
        osc_db_query("update products
                      set products_ordered = products_ordered + " . sprintf('%d', $order->products[$i]['qty']) . " 
                      where products_id = '" . osc_get_prid($order->products[$i]['id']) . "'
                    ");

// search the good model
        if ($OSCOM_Customer->getCustomersGroupID() != '0') {
          $QproductsModuleCustomersGroup = $OSCOM_PDO->prepare('select products_model_group
                                                                  from :table_products_groups
                                                                  where products_id = :products_id
                                                                  and customers_group_id =  :customers_group_id
                                                                ');
          $QproductsModuleCustomersGroup->bindInt(':products_id', osc_get_prid($order->products[$i]['id']));
          $QproductsModuleCustomersGroup->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
          $QproductsModuleCustomersGroup->execute();

          $productsModuleCustomersGroup = $QproductsModuleCustomersGroup->fetch();

          $products_model = $productsModuleCustomersGroup['products_model_group'];

          if (empty($products_model)) $products_model = $order->products[$i]['model'];

        } else {
          $products_model = $order->products[$i]['model'];
        }


        $sql_data_array = array('orders_id' => $insert_id, 
                                'products_id' => osc_get_prid($order->products[$i]['id']), 
                                'products_model' => $products_model,
                                'products_name' => $order->products[$i]['name'], 
                                'products_price' => $order->products[$i]['price'], 
                                'final_price' => $order->products[$i]['final_price'], 
                                'products_tax' => $order->products[$i]['tax'],
                                'products_quantity' => $order->products[$i]['qty'],
                                'products_full_id' => $order->products[$i]['id']
                                );
        $OSCOM_PDO->save('orders_products', $sql_data_array);

        $order_products_id = $OSCOM_PDO->lastInsertId();
// ---------------------------------------------
//------insert customer choosen option to order--------
// ---------------------------------------------
        $attributes_exist = '0';
        $products_ordered_attributes = '';
        if (isset($order->products[$i]['attributes'])) {
          $attributes_exist = '1';
          for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
            if (DOWNLOAD_ENABLED == 'true') {

              $Qattributes = $OSCOM_PDO->prepare('select popt.products_options_name,
                                                         poval.products_options_values_name,
                                                          pa.options_values_price,
                                                          pa.price_prefix,
                                                          pad.products_attributes_maxdays,
                                                          pad.products_attributes_maxcount,
                                                          pad.products_attributes_filename,
                                                          pa.products_attributes_reference
                                                   from :table_products_options popt,
                                                        :table_products_options_values poval,
                                                        :table_products_attributes pa
                                                        :table_products_attributes pa
                                                   left join products_attributes_download pad on pa.products_attributes_id = pad.products_attributes_id
                                                   where pa.products_id = :products_id
                                                    and pa.options_id = :options_id
                                                    and pa.options_id = popt.products_options_id
                                                    and pa.options_values_id = :options_values_id
                                                    and pa.options_values_id = poval.products_options_values_id
                                                    and popt.language_id = :language_id
                                                    and poval.language_id = :language_id
                                                 ');

              $Qattributes->bindInt(':products_id', (int)$order->products[$i]['id'] );
              $Qattributes->bindInt(':options_id', (int)$order->products[$i]['attributes'][$j]['option_id'] );
              $Qattributes->bindInt(':options_values_id', (int)$order->products[$i]['attributes'][$j]['value_id'] );
              $Qattributes->bindInt(':language_id', (int)$_SESSION['languages_id'] );

            } else {

              $Qattributes = $OSCOM_PDO->prepare('select popt.products_options_name,
                                                         poval.products_options_values_name,
                                                         pa.options_values_price,
                                                         pa.price_prefix,
                                                         pa.products_attributes_reference
                                                  from :table_products_options popt,
                                                       :table_products_options_values poval,
                                                       :table_products_attributes pa
                                                  where pa.products_id = :products_id
                                                  and pa.options_id = :options_id
                                                  and pa.options_id = popt.products_options_id
                                                  and pa.options_values_id = :options_values_id
                                                  and pa.options_values_id = poval.products_options_values_id
                                                  and popt.language_id = :language_id
                                                  and poval.language_id = :language_id
                                                ');

              $Qattributes->bindInt(':products_id', (int)$order->products[$i]['id'] );
              $Qattributes->bindInt(':options_id', (int)$order->products[$i]['attributes'][$j]['option_id'] );
              $Qattributes->bindInt(':options_values_id', (int)$order->products[$i]['attributes'][$j]['value_id'] );
              $Qattributes->bindInt(':language_id', (int)$_SESSION['languages_id'] );
            }

            $Qattributes->execute();
            $attributes_values = $Qattributes->fetch();

            $sql_data_array = array('orders_id' => $insert_id, 
                                    'orders_products_id' => $order_products_id, 
                                    'products_options' => $attributes_values['products_options_name'],
                                    'products_options_values' => $attributes_values['products_options_values_name'], 
                                    'options_values_price' => $attributes_values['options_values_price'], 
                                    'price_prefix' => $attributes_values['price_prefix'],
                                    'products_attributes_reference' => $attributes_values['products_attributes_reference']);
            $OSCOM_PDO->save('orders_products_attributes', $sql_data_array);

            if ((DOWNLOAD_ENABLED == 'true') && isset($attributes_values['products_attributes_filename']) && osc_not_null($attributes_values['products_attributes_filename'])) {
              $sql_data_array = array('orders_id' => $insert_id, 
                                      'orders_products_id' => $order_products_id, 
                                      'orders_products_filename' => $attributes_values['products_attributes_filename'], 
                                      'download_maxdays' => $attributes_values['products_attributes_maxdays'], 
                                      'download_count' => $attributes_values['products_attributes_maxcount']);
              $OSCOM_PDO->save('orders_products_download', $sql_data_array);
            }
            $products_ordered_attributes .= "\n\t" . $attributes_values['products_attributes_reference'] . ' - ' . $attributes_values['products_options_name'] . ' ' . $attributes_values['products_options_values_name'];
          } // end for
        } // end isset($order->products[$i]['attributes'])

 // ------insert customer choosen option eof ----
//        $total_weight += ($order->products[$i]['qty'] * $order->products[$i]['weight']);
//        $total_tax += osc_calculate_tax($total_products_price, $products_tax) * $order->products[$i]['qty'];
//        $total_cost += $total_products_price;
        $products_ordered .= $order->products[$i]['qty'] . ' x ' . $order->products[$i]['name'] . ' (' . $order->products[$i]['model'] . ') = ' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . $products_ordered_attributes . "\n";
      } // end for

 // ---------------------------------------------
 // ------             EMAIL SENT  --------------
 // ---------------------------------------------

// lets start with the email confirmation
      $message_order = stripslashes(EMAIL_TEXT_ORDER_NUMBER) . ' ' . $insert_id . "\n" . stripslashes(EMAIL_TEXT_INVOICE_URL);
      $message_order = $message_order;
      $email_order .= $message_order . ' ' . osc_href_link('account_history_info.php', 'order_id=' . $insert_id, 'SSL', false) . "\n" .  EMAIL_TEXT_DATE_ORDERED . ' ' . strftime(DATE_FORMAT_LONG) . "\n\n";

      if ($order->info['comments']) {
// pb utf8 avec le &     et "
        $email_order .= osc_db_output($order->info['comments']) . "\n\n";
      }

      $message_order = stripslashes(EMAIL_TEXT_PRODUCTS);
      $email_order .= html_entity_decode($message_order) . "\n" . 
                      EMAIL_SEPARATOR . "\n" . 
                      $products_ordered . 
                      EMAIL_SEPARATOR . "\n";

      for ($i=0, $n=sizeof($order_totals); $i<$n; $i++) {
        $email_order .= strip_tags($order_totals[$i]['title']) . ' ' . strip_tags($order_totals[$i]['text']) . "\n";
      }

      if ($order->content_type != 'virtual') {
        $message_order = stripslashes(EMAIL_TEXT_DELIVERY_ADDRESS);   
        $email_order .= "\n" . html_entity_decode($message_order) . "\n" . 
                        EMAIL_SEPARATOR . "\n" .
                        osc_address_label($OSCOM_Customer->getID(), $_SESSION['sendto'], 0, '', "\n") . "\n";
      }

      $message_order = stripslashes(EMAIL_TEXT_BILLING_ADDRESS); 
      $email_order .= "\n" .html_entity_decode($message_order) . "\n" .
                      EMAIL_SEPARATOR . "\n" .
                      osc_address_label($OSCOM_Customer->getID(), $_SESSION['billto'], 0, '', "\n") . "\n\n";

      if (is_object($$_SESSION['payment'])) {
        $message_order = stripslashes(EMAIL_TEXT_PAYMENT_METHOD);    
        $email_order .= html_entity_decode($message_order) . "\n" . 
                        EMAIL_SEPARATOR . "\n";
        $payment_class = $$_SESSION['payment'];
        $email_order .= $order->info['payment_method'] . "\n\n";
        if (isset($payment_class->email_footer)) {
          $email_order .= $payment_class->email_footer;
        }
      } // end is_object($$_SESSION['payment'])

      $message_order = stripslashes(EMAIL_TEXT_FOOTER); 
      $email_order .= html_entity_decode($message_order);
      
      osc_mail($order->customer['firstname'] . ' ' . $order->customer['lastname'], $order->customer['email_address'], EMAIL_TEXT_SUBJECT, $email_order, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS);

// send emails to other people
      if (SEND_EXTRA_ORDER_EMAILS_TO != '') {
        $email_text_subject = stripslashes(EMAIL_TEXT_SUBJECT);
        $email_text_subject =html_entity_decode($email_text_subject);

        $text = extract_email_address (SEND_EXTRA_ORDER_EMAILS_TO);

        foreach($text as $email){
          osc_mail('', $email, $email_text_subject, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
        }
      }

// load the after_process function from the payment modules
      $payment_modules->after_process();

      $_SESSION['cart']->reset(true);

//***************************************
// odoo web service
//***************************************
      if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_ORDER_CATALOG != 'none') {
        $delivery_postcode = $order->delivery['postcode'];
        $billing_postcode = $order->billing['postcode'];
        if (ODOO_ACTIVATE_ORDER_CATALOG == 'order') {
          require ('ext/odoo_xmlrpc/xml_rpc_catalog_checkout_process_order.php');
        }else {
          require ('ext/odoo_xmlrpc/xml_rpc_catalog_checkout_process_invoice.php');
        }
      }
//***************************************
// End odoo web service
//***************************************

// unregister session variables used during checkout
      unset($_SESSION['sendto']);
      unset($_SESSION['billto']);
      unset($_SESSION['shipping']);
      unset($_SESSION['payment']);
      unset($_SESSION['comments']);
      unset($_SESSION['coupon']);

// ---------------------------------------------
// ----------------  SAVE ORDER  --------------
// ---------------------------------------------

      $order_id = $insert_id;
      $_SESSION['order_id'];

      $sql_data_array = array('order_id' => $order_id);
      $OSCOM_PDO->save('desjardins_reference', $sql_data_array, array('ref_number' => $_REQUEST['reference']));

      break;

    } // $_REQUEST['code-retour']

    $receipt = DESJARDINS_CGI2_MACOK;

  } else {
// your code if the HMAC doesn't match
  	$receipt = DESJARDINS_CGI2_MACNOTOK.$cgi2_fields;
  } // end $oHmac->computeHmac($cgi2_fields)
//-----------------------------------------------------------------------------
// Send receipt to DESJARDINS server
//-----------------------------------------------------------------------------
   printf (DESJARDINS_CGI2_RECEIPT, $receipt);
  register_shutdown_function('session_write_close');
?>