<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  class product_notification {
    var $show_choose_audience, $title, $content;

    function product_notification($title, $content) {
      $this->show_choose_audience = true;
      $this->title = $title;
      $this->content = $content;
    }

    function choose_audience() {
      global $OSCOM_PDO;

      $products_array = array();

      $Qproducts = $OSCOM_PDO->prepare('select pd.products_id,
                                               pd.products_name
                                        from :table_products p,
                                             :table_products_description pd
                                       where pd.language_id = :language_id
                                       and pd.products_id = p.products_id
                                       and p.products_status = 1
                                       and p.products_archive = 0
                                       and p.products_view = 1
                                       order by pd.products_name
                                        ');
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);

      $Qproducts->execute();

      while ($products =  $Qproducts->fetch() ) {
        $products_array[] = array('id' => $products['products_id'],
                                  'text' => $products['products_name']);
      }

$choose_audience_string = '<script type="text/javascript"><!--
function mover(move) {
  if (move == \'remove\') {
    for (x=0; x<(document.notifications.products.length); x++) {
      if (document.notifications.products.options[x].selected) {
        with(document.notifications.elements[\'chosen[]\']) {
          options[options.length] = new Option(document.notifications.products.options[x].text,document.notifications.products.options[x].value);
        }
        document.notifications.products.options[x] = null;
        x = -1;
      }
    }
  }
  if (move == \'add\') {
    for (x=0; x<(document.notifications.elements[\'chosen[]\'].length); x++) {
      if (document.notifications.elements[\'chosen[]\'].options[x].selected) {
        with(document.notifications.products) {
          options[options.length] = new Option(document.notifications.elements[\'chosen[]\'].options[x].text,document.notifications.elements[\'chosen[]\'].options[x].value);
        }
        document.notifications.elements[\'chosen[]\'].options[x] = null;
        x = -1;
      }
    }
  }
  return true;
}

function selectAll(FormName, SelectBox) {
  temp = "document." + FormName + ".elements[\'" + SelectBox + "\']";
  Source = eval(temp);

  for (x=0; x<(Source.length); x++) {
    Source.options[x].selected = "true";
  }

  if (x<1) {
    alert(\'' . JS_PLEASE_SELECT_PRODUCTS . '\');
    return false;
  } else {
    return true;
  }
}
//--></script>';

      $global_button = '<script language="javascript"><!--' . "\n" .
                       'document.write(\'<input type="button" value="' . BUTTON_GLOBAL . '" style="width: 8em;" onclick="document.location=\\\'' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&action=confirm&global=true') . '\\\'">\');' . "\n" .
                       '//--></script><noscript><a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&action=confirm&global=true') . '">[ ' . BUTTON_GLOBAL . ' ]</a></noscript>';

      $choose_audience_string .= '    <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">' .
                                 '     <form name="notifications" action="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&action=confirm') . '" method="post" onSubmit="return selectAll(\'notifications\', \'chosen[]\')">' . "\n" .
						         '      <tr>' .
						         '          <td align="right">' . osc_image_submit('button_send_mail.gif', IMAGE_BUTTON_CONTINUE) . '</td>' .
						         '          <td>' . osc_draw_separator('pixel_trans.gif', '2', '1') . '</td>' .
						         '          <td align="right"><a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a></td>' .
						         '        </tr>' .
						         '      </table></td>' .
						         '    </tr>' .
					             '  </table></td>' .
					             '</tr>' .
                                 '<tr>' .
                                 '  <td>' . osc_draw_separator('pixel_trans.gif', '1', '10') . '</td>' .
                                 '</tr>';

      $choose_audience_string .= '<table border="0" width="100%" cellspacing="0" cellpadding="2"><tr>' . "\n" .
                                 '  <tr>' . "\n" .
                                 '    <td align="center" class="main"><b>' . TEXT_PRODUCTS . '</b><br />' . osc_draw_pull_down_menu('products', $products_array, '', 'size="20" style="width: 20em;" multiple') . '</td>' . "\n" .
                                 '    <td align="center" class="main">&nbsp;<br />' . $global_button . '<br /><br /><br /><input type="button" value="' . BUTTON_SELECT . '" style="width: 8em;" onClick="mover(\'remove\');"><br /><br /><input type="button" value="' . BUTTON_UNSELECT . '" style="width: 8em;" onClick="mover(\'add\');"></td>' . "\n" .
                                 '    <td align="center" class="main"><b>' . TEXT_SELECTED_PRODUCTS . '</b><br />' . osc_draw_pull_down_menu('chosen[]', array(), '', 'size="20" style="width: 20em;" multiple') . '</td>' . "\n" .
                                 '  </tr>' . "\n" .
                                 '</table></form>';

      return $choose_audience_string;
    }

    function confirm() {
      global $OSCOM_PDO;

      $audience = array();

      if (isset($_GET['global']) && ($_GET['global'] == 'true')) {

        $Qproducts = $OSCOM_PDO->prepare('select distinct customers_id
                                         from :table_products_notifications
                                        ');

        $Qproducts->execute();

        while ($products = $Qproducts->fetch() ) {
          $audience[$products['customers_id']] = 1;
        }

        $Qcustomers = $OSCOM_PDO->prepare('select customers_info_id
                                         from :table_customers_info
                                         where global_product_notifications = 1
                                        ');

        $Qcustomers->execute();

        while ($customers = $Qcustomers->fetch() ) {
          $audience[$customers['customers_info_id']] = 1;
        }

      } else {
        $chosen = $_POST['chosen'];

        $ids = implode(',', $chosen);

        $Qproducts = $OSCOM_PDO->prepare('select distinct customers_id
                                         from :table_products_notifications
                                         where products_id in ( :products_id )
                                        ');
        $Qproducts->bindInt(':products_id', $ids);
        $Qproducts->execute();

        while ($products = $Qproducts->fetch() ) {
          $audience[$products['customers_id']] = 1;
        }

        $Qcustomers = $OSCOM_PDO->prepare('select customers_info_id
                                         from :table_customers_info
                                         where global_product_notifications = 1
                                        ');

        $Qcustomers->execute();

        while ($customers = $Qcustomers->fetch() ) {
          $audience[$customers['customers_info_id']] = '1';
        }
      }

      if (sizeof($audience) > 0) {
        if (isset($_GET['global']) && ($_GET['global'] == 'true')) {
          $confirm_button_string .= osc_draw_hidden_field('global', 'true');
        } else {
          for ($i = 0, $n = sizeof($chosen); $i < $n; $i++) {
            $confirm_button_string .= osc_draw_hidden_field('chosen[]', $chosen[$i]);
          }
        }
        $confirm_button_string .= osc_image_submit('button_send_mail.gif', IMAGE_SEND) . ' ';
      }

      $confirm_string = '    <td class="pageHeading" align="right"><table border="0" cellspacing="0" cellpadding="0">' .
						'      <tr>' . osc_draw_form('confirm', 'newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&action=confirm_send') .
						'          <td align="right">' . $confirm_button_string . '</td>' .
						'          <td>' . osc_draw_separator('pixel_trans.gif', '2', '1') . '</td>' .
						'          <td align="right"><a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID'] . '&action=send') . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a></td>' .
						'          <td>' . osc_draw_separator('pixel_trans.gif', '2', '1') . '</td>' .
						'          <td align="right"><a href="' . osc_href_link('newsletters.php', 'page=' . $_GET['page'] . '&nID=' . $_GET['nID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a></td>' .
						'        </tr>' .
						'      </table></td>' .
						'    </tr>' .
					    '  </table></td>' .
					    '</tr>' .
                        '<tr>' .
                        '  <td>' . osc_draw_separator('pixel_trans.gif', '1', '10') . '</td>' .
                        '</tr>';

      $confirm_string .= '<table border="0" cellspacing="0" cellpadding="2">' . "\n" .
                        '  <tr>' . "\n" .
                        '    <td class="main"><font color="#ff0000"><strong>' . sprintf(TEXT_COUNT_CUSTOMERS, sizeof($audience)) . '</strong></font></td>' . "\n" .
                        '  </tr>' . "\n" .
                        '  <tr>' . "\n" .
                        '    <td>' . osc_draw_separator('pixel_trans.gif', '1', '10') . '</td>' . "\n" .
                        '  </tr>' . "\n" .
                        '  <tr>' . "\n" .
                        '    <td class="main"><strong>' . $this->title . '</strong></td>' . "\n" .
                        '  </tr>' . "\n" .
                        '  <tr>' . "\n" .
                        '    <td>' . osc_draw_separator('pixel_trans.gif', '1', '10') . '</td>' . "\n" .
                        '  </tr>' . "\n" .
                        '  <tr>' . "\n" .
                        '    <td class="main"><tt>' . $this->content . '</tt></td>' . "\n" .
                        '  </tr>' . "\n" .
                        '  <tr>' . "\n" .
                        '    <td>' . osc_draw_separator('pixel_trans.gif', '1', '10') . '</td>' . "\n" .
                        '  </tr>' . "\n" .
                        '</table>';

      return $confirm_string;
    }

// Envoie du mail sans gestion de Fckeditor
    function send($newsletter_id) {
      global $OSCOM_PDO;

      $audience = array();

      if (isset($_POST['global']) && ($_POST['global'] == 'true')) {

        $Qproducts = $OSCOM_PDO->prepare('select distinct pn.customers_id,
                                                        c.customers_firstname,
                                                        c.customers_lastname,
                                                        c.customers_email_address
                                          from :table_customers c,
                                               :table_products_notifications pn
                                          where c.customers_id = pn.customers_id
                                          and customers_email_validation = 0
                                        ');

        $Qproducts->execute();

        while ($products = $Qproducts->fetch() ) {
          $audience[$products['customers_id']] = array('firstname' => $products['customers_firstname'],
                                                       'lastname' => $products['customers_lastname'],
                                                       'email_address' => $products['customers_email_address']);
        }

        $Qcustomers = $OSCOM_PDO->prepare('select c.customers_id,
                                                  c.customers_firstname,
                                                  c.customers_lastname,
                                                  c.customers_email_address
                                           from :table_customers c,
                                                :table_customers_info ci
                                           where c.customers_id = ci.customers_info_id
                                           and ci.global_product_notifications = 1
                                           and customers_email_validation = 0
                                          ');

        $Qcustomers->execute();

        while ($customers = $Qcustomers->fetch() ) {
          $audience[$customers['customers_id']] = array('firstname' => $customers['customers_firstname'],
                                                        'lastname' => $customers['customers_lastname'],
                                                        'email_address' => $customers['customers_email_address']);
        }
      } else {
        $chosen = $_POST['chosen'];

        $ids = implode(',', $chosen);

        $Qproducts = $OSCOM_PDO->prepare('select distinct pn.customers_id,
                                                        c.customers_firstname,
                                                        c.customers_lastname,
                                                        c.customers_email_address
                                        from :table_customers c,
                                             :table_products_notifications pn
                                        where c.customers_id = pn.customers_id
                                        and pn.products_id in ( :products_id)
                                        and customers_email_validation = 0
                                        ');
        $Qproducts->bindInt('products_id', $ids );
        $Qproducts->execute();

        while ($products = $Qproducts->fetch() ) {
          $audience[$products['customers_id']] = array('firstname' => $products['customers_firstname'],
                                                       'lastname' => $products['customers_lastname'],
                                                       'email_address' => $products['customers_email_address']);
        }


        $Qcustomers = $OSCOM_PDO->prepare('select c.customers_id,
                                                c.customers_firstname,
                                                c.customers_lastname,
                                                c.customers_email_address
                                         from :table_customers c,
                                              :table_customers_info ci
                                         where c.customers_id = ci.customers_info_id
                                         and ci.global_product_notifications = 1
                                         and customers_email_validation = 0
                                        ');

        $Qcustomers->execute();

        while ($customers = $Qcustomers->fetch() ) {
          $audience[$customers['customers_id']] = array('firstname' => $customers['customers_firstname'],
                                                        'lastname' => $customers['customers_lastname'],
                                                        'email_address' => $customers['customers_email_address']);
        }
      } //end else

        $mimemessage = new email(array('X-Mailer: ClicShopping'));

// Build the text version
        $text = strip_tags($this->content);
        $mimemessage->add_text($this->content . TEXT_UNSUBSCRIBE . HTTP_SERVER . DIR_WS_CATALOG . 'account_newsletters.php');
        $mimemessage->build_message();

        foreach ( $audience as $key => $value ) {
          $mimemessage->send($value['firstname'] . ' ' . $value['lastname'], $value['email_address'], '', EMAIL_FROM, $this->title);
        }

        $newsletter_id = osc_db_prepare_input($newsletter_id);

        $Qupdate = $OSCOM_PDO->prepare('update :table_newsletters
                                        set date_sent = now(), status = 1
                                        where newsletters_id = :newsletters_id
                                      ');
        $Qupdate->bindValue(':newsletters_id', $newsletter_id);
        $Qupdate->execute();

    } //end function send
   
// Envoie du mail avec gestion des images pour Fckeditor et Imanager.
    function send_fckeditor($newsletter_id) {
      global $OSCOM_PDO;

      $audience = array();

      if (isset($_POST['global']) && ($_POST['global'] == 'true')) {

        $Qproducts = $OSCOM_PDO->prepare('select distinct pn.customers_id,
                                                        c.customers_firstname,
                                                        c.customers_lastname,
                                                        c.customers_email_address
                                        from :table_customers c,
                                             :table_products_notifications pn
                                        where c.customers_id = pn.customers_id
                                        and customers_email_validation = 0
                                        ');

        $Qproducts->execute();

        while ($products = $Qproducts->fetch() ) {
          $audience[$products['customers_id']] = array('firstname' => $products['customers_firstname'],
                                                       'lastname' => $products['customers_lastname'],
                                                       'email_address' => $products['customers_email_address']);
        }

        $Qcustomers = $OSCOM_PDO->prepare('select c.customers_id,
                                                c.customers_firstname,
                                                c.customers_lastname,
                                                c.customers_email_address
                                         from :table_customers c,
                                              :table_customers_info ci
                                         where c.customers_id = ci.customers_info_id
                                         and ci.global_product_notifications = 1
                                         and customers_email_validation = 0
                                        ');

        $Qcustomers->execute();


        while ($customers = $Qcustomers->fetch() ) {
          $audience[$customers['customers_id']] = array('firstname' => $customers['customers_firstname'],
                                                        'lastname' => $customers['customers_lastname'],
                                                        'email_address' => $customers['customers_email_address']);
        }
      } else {
        $chosen = $_POST['chosen'];

        $ids = implode(',', $chosen);

        $Qproducts = $OSCOM_PDO->prepare('select distinct pn.customers_id,
                                                        c.customers_firstname,
                                                        c.customers_lastname,
                                                        c.customers_email_address
                                         from :table_customers c,
                                              :table_products_notifications pn
                                         where c.customers_id = pn.customers_id
                                         and pn.products_id in ( :products_id )
                                         and customers_email_validation = 0
                                        ');
        $Qproducts->binInt('products_id', $ids);
        $Qproducts->execute();

        while ($products = $Qproducts->fetch()) {
          $audience[$products['customers_id']] = array('firstname' => $products['customers_firstname'],
                                                       'lastname' => $products['customers_lastname'],
                                                       'email_address' => $products['customers_email_address']);
        }


        $Qcustomers = $OSCOM_PDO->prepare('select c.customers_id,
                                                c.customers_firstname,
                                                c.customers_lastname,
                                                c.customers_email_address
                                         from :table_customers c,
                                              :table_customers_info ci
                                         where c.customers_id = ci.customers_info_id
                                         and ci.global_product_notifications = 1
                                         and customers_email_validation = 0
                                        ');

        $Qcustomers->execute();

        while ($customers = $Qcustomers->fetch() ) {
          $audience[$customers['customers_id']] = array('firstname' => $customers['customers_firstname'],
                                                        'lastname' => $customers['customers_lastname'],
                                                        'email_address' => $customers['customers_email_address']);
        }
      } // end else

        $mimemessage = new email(array('X-Mailer: ClicShopping'));
     	  $message = html_entity_decode($this->content . TEXT_UNSUBSCRIBE . HTTP_SERVER . DIR_WS_CATALOG . 'account_newsletters.php');
        $message = str_replace('src="/', 'src="' . HTTP_CATALOG_SERVER . '/', $message);

        $mimemessage->add_html_fckeditor($message);
        $mimemessage->build_message();

        reset($audience);

        while (list($key, $value) = each ($audience)) {
          $mimemessage->send($value['firstname'] . ' ' . $value['lastname'], $value['email_address'], '', EMAIL_FROM, $this->title);
        }

        $newsletter_id = osc_db_prepare_input($newsletter_id);


      $Qupdate = $OSCOM_PDO->prepare('update :table_newsletters
                                      set date_sent = now(),
                                          status = 1
                                      where newsletters_id = :newsletters_id
                                     ');
      $Qupdate->bindValue(':newsletters_id', $newsletter_id);
      $Qupdate->execute();

    } // end isset($_POST['global']) 
  } // end class
