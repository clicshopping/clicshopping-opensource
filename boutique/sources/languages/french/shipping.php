<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('NAVBAR_TITLE', 'Expédition &amp; retours');
define('HEADING_TITLE', 'Expédition &amp; retours');

define('TEXT_INFORMATION', 'Insérer ici vos informations expédition &amp; retours.');
?>