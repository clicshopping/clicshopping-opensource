<?php
/**
 * header.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license Licensed under GPL2 and MIT
 * @version $Id: index.php 
*/
// error and information management do not delete
  require ('includes/header.php');
?>
<!DOCTYPE html>
<html <?php echo HTML_PARAMS; ?>>
  <head>
     <meta charset="<?php echo CHARSET; ?>">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG;?>">
     <title><?php echo osc_output_string_protected($OSCOM_Template->getTitle());?></title>
     <meta name="Description" content="<?php echo osc_output_string_protected($OSCOM_Template->getDescription());?>" />
     <meta name="Keywords" content="<?php echo osc_output_string_protected($OSCOM_Template->getKeywords());?>" />
     <meta name="news_keywords" content="<?php echo osc_output_string_protected($OSCOM_Template->getNewsKeywords());?>" />
     <meta name="no-email-collection" content="<?php echo HTTP_SERVER; ?>" />
     <meta name="generator" content="ClicShopping" />
     <meta name="author" content="e-Imaginis & Innov Concept" />
     <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">

    <link rel="stylesheet" media="screen, print, projection" href="<?php echo $OSCOM_Template->getTemplategraphism();?>" />
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
     <?php echo $OSCOM_Template->getBlocks('header_tags'); ?>
  </head>
<body>
  <div id="bodyWrapper" class="<?php echo BOOTSTRAP_CONTAINER;?>">
   <div id="wrap">
      <header>
        <div class="row">
          <div class="col-sm-12">
<?php
  if  ( MODE_VENTE_PRIVEE == 'false' || (MODE_VENTE_PRIVEE == 'true' && $OSCOM_Customer->isLoggedOn() )) {
    echo $OSCOM_Template->getBlocks('modules_header');
  }
?>
          </div>
        </div>
      </header>
      <div class="<?php echo BOOTSTRAP_CONTAINER;?>">
        <div id="bodyContent" class="col-md-<?php echo $OSCOM_Template->getGridContentWidth(); ?>
          <?php echo ($OSCOM_Template->hasBlocks('boxes_column_left') ? 'col-md-push-' . $OSCOM_Template->getGridColumnWidth() : ''); ?>">