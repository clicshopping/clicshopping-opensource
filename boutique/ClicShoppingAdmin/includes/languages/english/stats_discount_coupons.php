<?php
/*
 * stats_discount_coupons.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Discount Coupon Codes Usage Information');
define('HEADING_COUPONS_LIST', 'All coupons');
define('HEADING_CUSTOMERS_LIST', 'Customers who have used coupon %s');
define('HEADING_ORDERS_LIST', 'Orders for customer %s using coupon %s');

define('TABLE_HEADING_CODE', 'Code');
define('TABLE_HEADING_PERCENTAGE', 'Discount Amount');
define('TABLE_HEADING_CUSTOMER', 'Customer Name');
define('TABLE_HEADING_ORDER', 'Order Number');
define('TABLE_HEADING_MAX_USE', 'Max Use per Customer' );
define('TABLE_HEADING_USE_STILL_AVAIL', 'Number Available for Use' );
define('TABLE_HEADING_NUM_AVAIL', 'Global Number Available');
define('TABLE_HEADING_NUM_STILL_AVAIL', 'Global Number Available Remaining');
define('TABLE_HEADING_USE_COUNT', 'Number Used');
define('TABLE_HEADING_CUSTOMERS', 'Customers');
define('TABLE_HEADING_ORDER_TOTAL', 'Order Total');
define('TABLE_HEADING_ORDER_DISCOUNT', 'Order Discount');
define('TABLE_HEADING_DATE_PURCHASED', 'Date Purchased');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_CONTENT_UNLIMITED','unlimited');
define('TEXT_DISPLAY_SHIPPING_DISCOUNT', 'off shipping');
?>