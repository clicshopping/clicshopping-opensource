<?php
/*
 * account_password.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_password'));

  if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
    $password_current = osc_db_prepare_input($_POST['password_current']);
    $password_new = osc_db_prepare_input($_POST['password_new']);
    $password_confirmation = osc_db_prepare_input($_POST['password_confirmation']);

    $error = false;

    if (strlen($password_current) < ENTRY_PASSWORD_MIN_LENGTH) {
      $error = true;

      $OSCOM_MessageStack->addError('account_password', ENTRY_PASSWORD_NEW_ERROR);
    } elseif ($password_new != $password_confirmation) {
      $error = true;

      $OSCOM_MessageStack->addError('account_password', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
    }

    if ($error == false) {
      $QcheckCustomer = $OSCOM_PDO->prepare('select customers_password   
                                             from :table_customers
                                             where customers_id = :customers_id
                                            ');
      $QcheckCustomer->bindInt(':customers_id', $OSCOM_Customer->getID());
      $QcheckCustomer->execute();

      $check_customer = $QcheckCustomer->fetch();

      if (osc_validate_password($password_current, $check_customer['customers_password'])) {
        $Qupdate = $OSCOM_PDO->prepare('update :table_customers
                                        set customers_password = :customers_password
                                         where customers_id = :customers_id
                                       ');
        $Qupdate->bindValue(':customers_password', osc_encrypt_password($password_new));
        $Qupdate->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
        $Qupdate->execute();


        $Qupdate = $OSCOM_PDO->prepare('update :table_customers_info
                                        set customers_info_date_account_last_modified = now()
                                         where customers_info_id = :customers_info_id
                                       ');
        $Qupdate->bindInt(':customers_info_id', (int)$OSCOM_Customer->getID());
        $Qupdate->execute();

        $OSCOM_MessageStack->addSuccess('account', SUCCESS_PASSWORD_UPDATED);

        osc_redirect(osc_href_link('account.php', '', 'SSL'));
      } else {
        $error = true;

        $OSCOM_MessageStack->adderror('account_password', ERROR_CURRENT_PASSWORD_NOT_MATCHING);
      }
    }
  }

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_password.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('account_password'));
