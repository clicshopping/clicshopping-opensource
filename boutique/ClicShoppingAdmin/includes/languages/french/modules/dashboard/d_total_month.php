<?php
/**
 * d_total_month.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_TITLE', 'Evolution cumulée du CA par mois sur 1 an <br /><br />');
define('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_DESCRIPTION', 'Graphique permettant de voir l\'évolution du chiffre d\'affaires par mois basé en fonction du sous-total');
define('MODULE_ADMIN_DASHBOARD_TOTAL_MONTH_CHART_LINK', 'Evolution cumulée du CA sur 1 an');
