<?php
/*
 * stats_products_validation.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

  require('includes/application_top.php');
  require('includes/header.php');

  ini_set ('max_execution_time', 0); // Aucune limite d'execution


  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (osc_not_null($action)) {
    switch ($action) {
      case 'reset':

        $Qupdate = $OSCOM_PDO->prepare('update :table_customers 
                                        set customers_email_validation = :customers_email_validation
                                      ');
        $Qupdate->bindInt(':customers_email_validation', '0');

        $Qupdate->execute();

      break;
    }  
  }
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_email.png', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading" width="90%"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td><?php echo osc_draw_separator('pixel_trans.gif', '6', '1'); ?></td>
            <td align="right"><?php echo '<a href="' . osc_href_link('stats_email_validation.php',  '&action=reset') . '">' . osc_image_button('button_reset_email.png', IMAGE_RESET_EMAIL) . '</a>'; ?></td>
            <td align="right"><?php echo '<a href="' . osc_href_link('stats_email_validation.php',  '&action=analyse') . '">' . osc_image_button('button_analyse_email.png', IMAGE_ANALYSE) . '</a>'; ?></td>
            <td align="right"><?php echo '<a href="' . osc_href_link('stats_email_validation.php') . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>

          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIRST_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LAST_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ADDRESS_EMAIL; ?>&nbsp;</td> 
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_VALIDATE_EMAIL; ?>&nbsp;</td>        
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_CLEAR; ?>&nbsp;</td>
              </tr>
<?php 
  if ($action =='analyse') {	

    if (isset($_GET['page']) && ($_GET['page'] > 1)) $rows = $_GET['page'] * MAX_DISPLAY_SEARCH_RESULTS_ADMIN - MAX_DISPLAY_SEARCH_RESULTS_ADMIN;
    $rows = 0;
    $validation_email_query_raw = "select customers_id,
                                        customers_email_address,
                                        customers_firstname,
                                        customers_lastname,
                                        customers_email_validation
                                 from customers
                                 where customers_email_validation = '0'
                                ";

    $validation_email_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $validation_email_query_raw, $validation_email_query_numrows);
    $validation_email_query = osc_db_query($validation_email_query_raw);

    while ($validation_email = osc_db_fetch_array($validation_email_query)) {
      $rows++;

      $email_validation = osc_validate_domain_email($validation_email['customers_email_address']);

      if ($email_validation != 1) {

        $Qupdate = $OSCOM_PDO->prepare('update :table_customers
                                        set customers_email_validation = 1
                                        where customers_email_address = :customers_email_address
                                            ');
        $Qupdate->bindValue(':customers_email_address', $validation_email['customers_email_address']);

        $Qupdate->execute();
      }

      if (strlen($rows) < 2) {
        $rows = '0' . $rows;
      }
?>
              <tr class="dataTableRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)"">
                <td class="dataTableContent"><?php echo $validation_email['customers_id'] ; ?></td>			  
                <td class="dataTableContent"><?php echo $validation_email['customers_firstname'] ; ?></td>
                <td class="dataTableContent"><?php echo $validation_email['customers_lastname'] ; ?></td>
                <td class="dataTableContent"><?php echo $validation_email['customers_email_address'] ; ?></td>
                <td class="dataTableContent">
<?php
      if ($email_validation == 1) echo TEXT_SUCCESS_DOMAIN;
      if ($email_validation != 1) echo TEXT_NO_SUCCESS_DOMAIN;
?>
                </td>
                <td class="dataTableContent" align="right">
<?php echo '<a href="' . osc_href_link('customers.php', '&cID=' .$validation_email['customers_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>'; ?>&nbsp;
<?php echo '<a href="' . osc_href_link('customers.php', '&cID=' .$validation_email['customers_id'] . '&action=confirm') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>'; ?>&nbsp;
                </td>
              </tr>
<?php
    } // end while
?>
            </table></td>
          </tr>
          <tr>
            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $validation_email_split->display_count($validation_email_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
                <td class="smallText" align="right"><?php echo $validation_email_split->display_links($validation_email_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table></td>
          </tr>

<?php
  } else {
?>  
         <tr>
           <table width="100%" border="0" cellspacing="0" cellpadding="5">
             <tr>
              <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
             </tr>
           </table>
           <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
             <tr>
               <td><table border="0" cellpadding="2" cellspacing="2">
                 <tr>
                   <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_EMAIL_IMAGE); ?></td>
                   <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_EMAIL; ?></strong></td>
                 </tr>
                 <tr>
                   <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                   <td class="main"><?php echo TITLE_TEXT_AIDE_EMAIL; ?></td>
                 </tr>
               </table></td>
             </tr>
           </table>
         </tr>
<?php
  }
?>

        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
 
</table>
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>