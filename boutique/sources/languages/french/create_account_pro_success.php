<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Créer un comtpe');
define('NAVBAR_TITLE_2', 'Succès');

// Affichage texte d'ouverture d'une session si l'approbation n'est pas obligatoire
if (MEMBER == 'false') {
  define('HEADING_TITLE', 'Votre compte a été créé !');
  define('TEXT_ACCOUNT_CREATED', 'Félicitations ! Votre nouveau compte a été créé avec succès !<br /><br />Vous pouvez maintenant profiter de tous les privilèges notre magasin en ligne.<br /><br />N\'hésitez pas à contacter notre <a href="' . osc_href_link('contact_us.php') . '">service commercial</a> pour toute question sur le fonctionnement de nos services.<br /><br />Note : Une confirmation de votre inscription a été envoyée dans votre boite e-mail. Si vous ne l\'avez pas reçu d\'ici une heure, n\'hésitez pas à <a href="' . osc_href_link('contact_us.php') . '">nous contacter</a>.');
	define('TABLE_HEADING_NEW_PRODUCTS','Nouveautés et Collections');
    define('MIN_QTY_ORDER_PRODUCT','Qté min : ');
} else {
  define('HEADING_TITLE', 'Demande de création compte professionnel');
  define('TEXT_ACCOUNT_CREATED', 'Nous avons bien prit en compte votre demande d\'inscription pour l\'ouverture d\'un compte professionnel auprès de notre réseau.<br /><br />Vous recevrez rapidement un e-mail comportant vos codes d\'accès et vous permettant de bénéficier d\'un tarif préférentiel après vérification et validation de votre compte par notre service commercial.<br /><br />N\'hésitez pas à contacter notre support en ligne pour toutes demandes d\'informations complémentaires.<br /><br />Cordialement,<br />L\'équipe ' .  	STORE_NAME);
}
?>
