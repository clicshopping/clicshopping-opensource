<?php
/*
 * reviews.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('reviews'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('reviews.php'));

  $products_split = $OSCOM_ProductsReviews->getReviewsSplit($OSCOM_ProductsReviews->getReviewsRaw());

  require($OSCOM_Template->getTemplateFiles('reviews'));
