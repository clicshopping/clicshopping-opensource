<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

define('TEXT_TITLE_WELCOME', 'Bienvenue sur ClicShopping');
define('TEXT_LICENCE','Veuillez accepter la licence avant de continuer');
define('TEXT_ACCEPT_LICENCE','J\'accepte les termes et les conditions');
define('TEXT_AGREEMENT','Acceptation de la licence');

define('TEXT_TITLE_WELCOME','Bienvenue sur ClicShopping !');
define('TEXT_INTRO_WELCOME','  <p>ClicShopping a &eacute;t&eacute; con&ccedil;ue sp&eacute;cialement pour permettre de pr&eacute;senter et de vendre des produits sur Internet. L\'administration vous permettra de g&eacute;rer vos produits, clients, commandes, campagnes marketing ......</p>
  <p>Nous avons fait particuli&egrave;rement attention au d&eacute;veloppement de ClicShopping et nous esp&eacute;rons qu\'il vous apportera une enti&egrave;re satisfaction.</p>
  <p>ClicShopping a &eacute;t&eacute; d&eacute;velopp&eacute; sp&eacute;cifiquement pour permettre des actions marketing adapt&eacute;es &agrave; vos clients.</p>
  <p>ClicShopping est en version B2B, B2C, B2B & B2C et soit en vente ouverte ou priv&eacute;e.</p>');

define('TEXT_SERVER_CARACTERISTICS','Caract&eacute;ristiques du serveur');
define('TEXT_DIRECTORIES','Permissions des répertoires devant etre mis en 777 ou 755 en fonction de serveurs');
define('TEXT_PHP_SETTINGS','Configuration Serveur');
define('TEXT_R_G','Register Globals');
define('TEXT_M_Q','Magic Quotes');
define('TEXT_F_U','Files upload');
define('TEXT_SA_S','Session auto start');
define('TEXT_SU_T_S','Session use trans sid');


define('TEXT_PHP_VERSION','Version PHP');
define('TEXT_PARAMETERS','Paramètres de PHP');
define('TEXT_LIBRAIRIES_NO_ACCEPTED','Librairie Graphique non configur&eacute;e.<br />. Vous pouvez ignorer cette erreur lors de l\'installation de ClicShopping mais les graphiques ne fonctionneront pas');
define('TEXT_ACCEPTED','Accept&eacute;');
define('TEXT_NOT_ACCEPTED','Non accept&eacute;');
define('TEXT_INSTALLED_VERSION','Version install&eacute;e');
define('TEXT_LIBRAIRIES_NOT_WELL_CONFIGURED','Librairie graphique disponible, mais la configuration n\'est pas correcte');
define('TEXT_LIBRAIRIES_NOT_WELL_CONFIGURED_CONTINUE','Vous pouvez ignorer cette erreur mais l\'apparition des graphiques ne fonctionnera pas');
define('TEXT_ANTIALIASING','avec anti-aliasing!');
define('TEXT_NO_ANTIALIASING','sans anti-aliasing!');
define('TEXT_SOAP','SOAP (webservice)');
define('TEXT_MANDATORY','Obligatoire');
define('TEXT_XML','XML');
define('TEXT_XML_RPC', 'XML-RPC (webservice)' );
define('TEXT_CURL','cURL');
define('TEXT_OPENSSL','OpenSSL');

define('TEXT_NOTES','Notes :');
define('TEXT_NOTICE',' <blockquote>- Veuillez prendre note que certains serveur n\'acceptent pas l\'analyse des droits sur les répertoires.</blockquote>
    <blockquote>- ClicShopping a été testé sur un serveur Linux ubuntu / Debian, Il peut y avoir des incompatibiltés sur d\'autres systèmes d\'exploitation. Les problèmes peuvent survenir aussi de la configuration du serveur.</blockquote>
');
define('TEXT_NEW_INSTALLATION','Nouvelle Installation :');
define('TEXT_REGISTER_GLOBAL','La compatibilité avec register_globals est supportée depuis PHP 4.3 +. Ce paramètre doit être activé en raison d\'une ancienne version de PHP utilisée');
define('TEXT_MYSQL_EXTANSION','L\'extension MySQL est nécessaire mais n\'est pas installée. Ce paramètre doit être activé');
define('TEXT_NOT_SAVE_PARAMETERS','<p>Le serveur ne peut pas enregistrer vos param&egrave;tres, veuillez rectifier les permissions des fichiers.</p>
<p>les fichiers suivants doivent avoir <strong>une permission 777 (chmod 777):</strong></p>');

define('TEXT_INFO_CUSTOMER','
      <p>Les fichiers se situant dans l\'administration (ClicShoppingAdmin) et catalogue  doivent avoir <strong>une permission 777 lors du processus d\'installation :</strong></p>
      <blockquote>
         /includes/configure.php (permission 777)<br />
         /ClicShoppingAdmin/includes/configures.php (permission 777)<br />
      </blockquote>
      <p>Les r&eacute;pertoires se situant dans l\'administration (ClicShoppingAdmin) doivent avoir <strong>une permission 777 ou 755 (chmod 777) selon les serveurs:</strong></p>
      <blockquote>
        /ClicShoppingAdmin/cache (permission 755 ou 777)<br />
        /ClicShoppingAdmin/images/banners permission 755 ou 777)<br />
        /ClicShoppingAdmin/backups (permission 755 ou 777)<br />
      </blockquote>
      <p>Les r&eacute;pertoires et sous r&eacute;pertoires se situant dans la boutique doivent avoir une <strong>>permission 755 ou 777 (chmod 777) :</strong></p>
      <blockquote>
        /template/images (permission 755 ou 777)<br />
        /template/products (permission 755 ou 777)<br />
        /cache<br />
      </blockquote>
      <p>Veuillez noter que certains <b><u>fichiers de paiement</u></b> doivent &ecirc;tre transf&eacute;r&eacute;s en mode binaire et non ASCII pour pouvoir fonctionner correctement ou certains modules bancaires ne fonctionneront que si vous &ecirc;tes sur un serveur d&eacute;di&eacute;:</p>
      <blockquote>
        /atos/atos_response<br />
        /atos/request<br />
      </blockquote>
<strong>Note : </strong> Nous conseillons fortement de mettre votre serveur <strong>register_global sur OFF</strong> dans votre fichier de configuration du php.ini. Des fonctionnalit&eacute;s ult&eacute;rieures propres &agrave; ClicShopping fonctionneront uniquement dans ce mode de configuration.<br />
Vous renforcez aussi la s&eacute;curit&eacute; de votre serveur
');
define('TEXT_INFO_CUSTOMER_ALERT','<blockquote>Veuillez corriger les erreurs suivantes avant de continuer la proc&eacute;dure d\'installation.</blockquote>');
define('TEXT_INFO_CUSTOMER_ALERT_SERVER','TEXT_INFO_CUSTOMER_ALERT_SERVER','<p><i>Si des changements doivent &ecirc;tre effectu&eacute;s sur le serveur, veuillez penser &agrave; le red&eacute;marrer et &agrave; relancer l\'installation de ClicShopping.</i></p>');

define('TEXT_INFO_SERVER_OK','<p>L\'environnement du serveur a &eacute;t&eacute; v&eacute;rifi&eacute; et il est conforme au standard de fonctionnement technologique de ClicShopping.</p>
    <p>Si dans la section configuration, vous voyez une croix rouge, veuillez noter qu\'il se peut que des composants ne puissent pas fonctionner correctement ou produisent des dysfonctionnements, certains ne sont pas forc&eacute;ment indispensables mais fortement recommand&eacute;s pour une utilisation maximale</p>
<strong>Note : </strong> Nous conseillons fortement de mettre votre <strong>register_global sur OFF</strong> dans votre fichier de configuration php.ini et d\'utiliser la technologie PHP5 . Des fonctionnalit&eacute;s ult&eacute;rieures propres &agrave; ClicShopping fonctionneront uniquement dans ce mode de configuration.<br />
    <p>Veuillez continuer la proc&eacute;dure d\'installation.</p>Vous renforcerez aussi la s&eacute;curit&eacute; de votre serveur et de ClicSHopping<br /> <br />
');


//---------------------------
// Step 3
//--------------------------

define('TEXT_STEP_INTRO','<strong> Nous vous conseillons fortement d\'être sur une technologie serveur MYSQL 5</strong>  <p>Veuillez suivre les instructions qui vous seront demandées au fur et à mesure de l\'installation.</p>  <p>Il s\'agit de configurer correctement ClicShopping afin de le rendre fonctionnel sur votre hébergement.</p>');
define('TEXT_STEP_INTRO_1','<strong>Etape 1: Configuration du serveur de base de données</strong>');

define('TEXT_STEP_INTRO_2','<p>Le serveur de base de données permet de stocker l\'ensemble de vos informations que vous allez y incorporé, les produits, les clients, les commandes ....</p>
  <p>Note : Avant de commencer, veuillez créer une base de données via le module phpmyadmin et retenir le mo de passe et le user....</p>');


define('TEXT_DATABASE_SERVER','Serveur de la base de données<br />');
define('TEXT_DATABASE_SERVER_HELP','Indiquer l\'adresse du serveur de votre base de données ou l\'IP');
define('TEXT_USERNAME','Nom utilisateur<br />');
define('TEXT_USERNAME_HELP','Indiquer le nom d\'utilisateur du serveur de base données');
define('TEXT_PASSWORD','Mot de Passe<br />');
define('TEXT_PASSWORD_HELP','Indiquer le mot de passe du serveur de base données.');
define('TEXT_DATABASE_NAME','Nom  base de données<br />');
define('TEXT_DATABASE_HELP','Indiquer le nom de votre base de données (doit étre créée sur le serveur de base de données) ');

//---------------------------
// Step 4
//--------------------------

define('TEXT_STEP_INTRO_STEP4', '<p>L\'installation est entrain de r&eacute;aliser la configuration des répertoires et chemins pour les fichiers de configuration.</p>
<p>Veuillez suivre les instructions qui vous sont donn&eacute;es lors de la proc&eacute;dure d\'installation.</p>');
define('TEXT_STEP_INTRO_3', '<p>Step 2: Serveur Internet</p>');
define('TEXT_STEP_INTRO_4','<p>Le serveur internet permet d\'afficher les pages de vos produits pour vos clients.</p>');


define('TEXT_STEP_HELP_4','Indiquer l\'adresse internet de votre boutique ClicShopping');
define('TEXT_STEP_HELP_5','R&eacute;pertoire de votre boutique ClicShopping sur le serveur.');
define('TEXT_STEP_INTRO_4', 'Adresse WWW<br />');
define('TEXT_STEP_INTRO_5', 'R&eacute;pertoire Root du serveur Internet<br />');


//---------------------------
// Step 5
//--------------------------

define('TEXT_END_CONFIGURATION','Fin de l\'installation');
define('TEXT_INFO_1','<p>C\'est la derni&egrave;re &eacute;tape  configuration de votre boutique en ligne ClicShopping.</p><p>Veuillez prendre soin &agrave; remplir l\'ensemble des champs ci-dessous.</p>');
define('TEXT_INFO_2','Step 3: Configuration d\'acc&egrave;s &agrave; la boutique ClicShopping');


define('TEXT_INFO_3','<p>Vous pouvez d&eacute;finir le nom de votre boutique ClicShopping, les informations de contact concernant le propri&eacute;taire de la boutique.</p>
      <p>Le nom d\'utilisateur et le mot de passe sont une protection pour vous connecter à votre espace d\'administration.</p>
      <p>&nbsp;</p>');

define('TEXT_STORE_NAME','Nom de la boutique');
define('TEXT_STORE_HELP','Indiquer le nom de la boutique ClicShopping qui sera affich&eacute;&eacute; vos clients.');
define('TEXT_STORE_OWNER','Nom du propri&eacute;taire*');
define('TEXT_STORE_OWNER_HELP','Indiquer le nom du propri&eacute;taire de la boutique.');
Define('TEXT_STORE_NAME_ADMIN','Nom de l\'administrateur*');
define('TEXT_STORE_NAME_ADMIN_HELP','Indiquer le nom de l\'administrateur.');
define('TEXT_STORE_FIRST_NAME','Pr&eacute;nom  de l\'administrateur*');
define('TEXT_STORE_FIRST_NAME_HELP','Indiquer le pr&eacute;nom  de l\'administrateur de la boutique.');
define('TEXT_STORE_OWNER_EMAIL','L\'adresse email du propri&eacute;taire*');
define('TEXT_STORE_OWNER_EMAIL_HELP','Indiquer votre adresse email');
define('TEXT_STORE_EMAIL_ADMIN','Email de l\'administrateur (Votre nom d\'utilisateur)*');
define('TEXT_STORE_EMAIL_ADMIN_HELP','Indiquer votre email de connexion à l\'administration de la boutique.');
define('TEXT_STORE_PASSWORD','Mot de passe de l\'administrateur*');
define('TEXT_STORE_PASSWORD_HELP','Indiquer le mot de passe de l\'administrateur de la boutique (ex : UiO/J-4). un mot de passe compliqu&eacute; assurera une meilleure protection');
define('TEXT_STORE_DIRECTORY','Nom du répertoire d\'administration');
define('TEXT_STORE_DIRECTORY_HELP','Il s\'agit du répertoire où d\'administration sera installé. Vous devriez changer cela pour des raisons de sécurité.');
define('TEXT_STORE_TIME_ZONE','Zone horaire<br />');
define('TEXT_STORE_TIME_ZONE_HELP','Choisissez votre fuseau horaire');
define('TEXT_STORE_MANDATORY', 'Obligatoire');
define('TEXT_STORE_DONATION', ' Vous pouvez faire un don pour nous encourager &agrave; continuer par l\'intermédiaire de notre association e-Imaginis');
//---------------------------
// Step 6
//--------------------------

define('TEXT_END_INSTALLATION','Step 4: Termin&eacute;e !');
define('TEXT_END_INSTALLATION_1','
<p>F&eacute;licitation, vous avez r&eacute;ussi &agrave; configurer correctement votre boutique ClicShopping !</p>
<p>Nous vous souhaitons un excellent succ&eacute;s dans votre projet de commerce &eacute;lectronique.</p>
<br />
<p>Nous vous invitons à consulter <a href="http://www.clicshopping.org/marketplace/blog.php" target="_blank"><strong>nos guides</strong></a> pour un meilleur apprentissage et expérience d\'utilisation de ClicShopping</p>
<br />
<br />
<p>Si vous appr&eacute;ciez le travail que nous r&eacute;alisons, alors n\'h&eacute;sitez pas &eacute; communiquer le projet à vos amis et à votre entourage.
Si vous souhaitez nous aider et à p&eacute;r&eacute;niser et d&eacute;velopper ce projet, nous vous invitons à souscrire à l\'un de nos abonnements, support, aide forum, h&eacute;bergement locatif ecommerce mais aussi &eacute; nous faire part des retours
 sur vos besoins, vos probl&eacute;mes de gestion au quotidien de ClicShopping, les bugs que vous voyez ou encore les fonctionnalit&eacute;s que vous souhaiteriez voir incluses dans ClicShopping. </p>
<p><br /><strong>
Vos avis et sugestions</strong> nous importent beaucoup dans le cadre du d&eacute;veloppement de ce projet car nous souhaitons proposer un des meilleures outils de ventes en ligne existant. </p>
<p align="right"> L\'&eacute;quipe ClicShopping</p>
');

define('TEXT_END_INSTALLATION_SUCCESS','L\'installation s\'est pass&eacute;e correctement, vous pouvez d&eacute;sormais acc&eacute;der &eacute; votre boutique ClicShopping!');

define('TEXT_END_INSTALLATION_2','Post-Installation Notes');
define('TEXT_END_INSTALLATION_3','<p>Il est recommandé de supprimer le r&eacute;pertoire install pour s&eacute;curiser ClicShopping</p>');
define('TEXT_END_INSTALLATION_4','Supprimer le r&eacute;pertoire ');
define('TEXT_END_INSTALLATION_5','Veuillez changer les permissions de ce fichier ');
define('TEXT_END_INSTALLATION_6',' en 644 (or 444 si le fichier est encore en mode écriture)');

define('TEXT_END_INSTALLATION_7','Veuillez  changer les permissions de ce fichier ');
define('TEXT_END_INSTALLATION_8','L\'administration devrait etre prot&eacute;g&eacute; en activant le  htaccess/htpasswd and can be set-up within the Configuration -> Administrators page.<br />Tous les systèmes n\'acceptent pas for&eacute;ment cette protection');

define('TEXT_END_ACCESS_CATALOG','Acc&eacute;der &agrave; votre catalogue');
define('TEXT_END_ACCESS_ADMIN','<p style="color:#f29400; text-align:center;">Acc&eacute;der &agrave; l\'administration </p>');
?>