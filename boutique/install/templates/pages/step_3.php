<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */


require ('includes/languages/' . $language . '/' . basename($_SERVER['PHP_SELF']));
?>
<script>
<!--

  var dbServer;
  var dbUsername;
  var dbPassword;
  var dbName;

  var formSubmited = false;
  var formSuccess = false;

  function prepareDB() {
    if (formSubmited == true) {
      return false;
    }

    formSubmited = true;

    $('#mBox').show();

    $('#mBoxContents').html('<p><i class="fa fa-spinner fa-spin fa-2x"></i> Testing database connection..</p>');

    dbServer = $('#DB_SERVER').val();
    dbUsername = $('#DB_SERVER_USERNAME').val();
    dbPassword = $('#DB_SERVER_PASSWORD').val();
    dbName = $('#DB_DATABASE').val();

    $.get('rpc.php?action=dbCheck&server=' + encodeURIComponent(dbServer) + '&username=' + encodeURIComponent(dbUsername) + '&password=' + encodeURIComponent(dbPassword) + '&name=' + encodeURIComponent(dbName), function (response) {
      var result = /\[\[([^|]*?)(?:\|([^|]*?)){0,1}\]\]/.exec(response);
      result.shift();

      if (result[0] == '1') {
        $('#mBoxContents').html('<p><i class="fa fa-spinner fa-spin fa-2x"></i> The database structure is now being imported. Please be patient during this procedure.</p>');

        $.get('rpc.php?action=dbImport&server=' + encodeURIComponent(dbServer) + '&username=' + encodeURIComponent(dbUsername) + '&password='+ encodeURIComponent(dbPassword) + '&name=' + encodeURIComponent(dbName), function (response2) {
          var result2 = /\[\[([^|]*?)(?:\|([^|]*?)){0,1}\]\]/.exec(response2);
          result2.shift();

          if (result2[0] == '1') {
            $('#mBoxContents').html('<p class="text-success"><i class="fa fa-thumbs-up fa-2x"></i> Database imported successfully.</p>');

            formSuccess = true;

            setTimeout(function() {
              $('#installForm').submit();
            }, 2000);
          } else {
            var result2_error = result2[1].replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');

            $('#mBoxContents').html('<p class="text-danger"><i class="fa fa-thumbs-down fa-2x text-danger"></i> There was a problem importing the database. The following error had occured:</p><p  class="text-danger"><strong>%s</strong></p><p class="text-danger">Please verify the connection parameters and try again.</p>'.replace('%s', result2_error));

            formSubmited = false;
          }
        }).fail(function() {
          formSubmited = false;
        });
      } else {
        var result_error = result[1].replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');

        $('#mBoxContents').html('<p class="text-danger"><i class="fa fa-thumbs-down fa-2x text-danger"></i> There was a problem connecting to the database server. The following error had occured:</p><p class="text-danger"><strong>%s</strong></p><p class="text-danger">Please verify the connection parameters and try again.</p></div>'.replace('%s', result_error));

        formSubmited = false;
      }
    }).fail(function() {
      formSubmited = false;
    });
  }

  $(function() {
    $('#installForm').submit(function(e) {
      if ( formSuccess == false ) {
        e.preventDefault();

        prepareDB();
      }
    });
  });

//-->
</script>




  <div class="contentBlock">  
    <div class="contentPane">
      <h1><?php echo TEXT_NEW_INSTALLATION; ?></h1>
      <?php echo TEXT_STEP_INTRO; ?>
    </div>
  </div>

  <div class="contentBlock">
    <div>
      <h3><?php echo TEXT_STEP_INTRO_1; ?></h3>
    </div>
    <div class="infoPaneContents"><?php echo TEXT_STEP_INTRO_2; ?></div>
  </div>
  <div id="mBox">
      <div class="well well-sm">
      <div id="mBoxContents"></div>
    </div>
  </div>
  <div class="contentPane">
    <strong><?php echo TEXT_DATABASE_SERVER; ?></strong>
  </div>


  <form name="install" id="installForm" action="index.php?step=4" method="post" class="form-horizontal" role="form">
    <div class="contentBlock">
      <div class="contentPane">
        <div class="form-group has-feedback">
          <label for="dbServer" class="control-label col-xs-3">Database Server</label>
          <div class="col-xs-9">
            <?php echo osc_draw_input_field('DB_SERVER', NULL, 'required aria-required="true" id="dbServer" placeholder="localhost"'); ?>
            <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
            <span class="help-block"><?php echo TEXT_DATABASE_SERVER_HELP; ?></span>
          </div>
        </div>
    
        <div class="form-group has-feedback">
          <label for="userName" class="control-label col-xs-3"><?php echo TEXT_USERNAME; ?></label>
          <div class="col-xs-9">
            <?php echo osc_draw_input_field('DB_SERVER_USERNAME', NULL, 'required aria-required="true" id="userName" placeholder="Username"'); ?>
            <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
            <span class="help-block"><?php echo TEXT_USERNAME_HELP; ?></span>
          </div>
        </div>
    
        <div class="form-group has-feedback">
          <label for="passWord" class="control-label col-xs-3"><?php echo TEXT_PASSWORD; ?></label>
          <div class="col-xs-9">
            <?php echo osc_draw_password_field('DB_SERVER_PASSWORD', NULL, 'required aria-required="true" id="passWord"'); ?>
            <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
            <span class="help-block"><?php echo TEXT_PASSWORD_HELP; ?></span>
          </div>
        </div>
        <div class="form-group has-feedback">
          <label for="dbName" class="control-label col-xs-3"><?php echo TEXT_DATABASE_NAME; ?></label>
          <div class="col-xs-9">
            <?php echo osc_draw_input_field('DB_DATABASE', NULL, 'required aria-required="true" id="dbName" placeholder="Database"'); ?>
            <span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span>
            <span class="help-block"><?php echo TEXT_DATABASE_HELP; ?></span>
          </div>
        </div>
      </div>
    </div>
    <div class="pull-right" style="padding-bottom: 10px;"><?php echo osc_draw_button('Continue', null, null, 'success', null, null); ?></div>

  </form>

