<?php
/**
 * stats_products_wharehouse.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
define('HEADING_TITLE', 'Gestion de l\'entreprot des commandes');
define('TABLE_HEADING_PRODUCTS', 'Nom du produit');
define('TABLE_HEADING_MODEL', 'Référence produit');
define('TABLE_HEADING_QTY_LEFT', 'Quantités en stock');
define('TABLE_HEADING_CUSTOMERS_ORDERS_QUANTITY','Quantités commandées');
define('TABLE_HEADING_ACTION','Action');
define('TABLE_HEADING_WAHREHOUSE_TIME_REPLENISHMENT','Délais de réapprovisionnement');
define('TABLE_HEADING_WHAREHOUSE','Nom de l\'entrepot');
define('TABLE_HEADING_WHAREHOUSE_ROW','Rangée de l\'entreprot');
define('TABLE_HEADING_WHAREHOUSE_LEVEL','Niveau de localisation');
define('TABLE_HEADING_CUSTOMERS_ID','No client');
define('TABLE_HEADING_CUSTOMERS_NAME','Nom du client');
define('TABLE_HEADING_CUSTOMERS_PHONE','Téléphone');
define('TABLE_HEADING_ORDER_ID','Ref Commande');
define('TABLE_HEADING_PRODUCTS_NAME','Nom du produit');
define('TABLE_HEADING_PACKAGING','Conditionnement');
define('TITLE_STATUS','Statut de commande');
define('TEXT_ALL_ORDERS','Toutes les commandes');
define('TEXT_ACTION','Action');
define('TEXT_PRODUCTS_PACKAGING_NEW','Produit neuf');
define('TEXT_PRODUCTS_PACKAGING_REPACKAGED','Produit reconditionné');
define('TEXT_PRODUCTS_PACKAGING_USED','Produit d\'occasion');
?>