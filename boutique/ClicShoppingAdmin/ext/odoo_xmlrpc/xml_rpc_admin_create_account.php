<?php
/*
 * xml_rpc_admin_create_account.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// update newsletter
  if ($customers_newsletter == 1) $customers_newsletter = 0;

// **********************************
// search iso code ClicShopping
// **********************************

      $QcountryIdCustomer = $OSCOM_PDO->prepare("select entry_country_id
                                                 from :table_address_book
                                                 where customers_id = :customers_id
                                                ");
      $QcountryIdCustomer->bindInt(':customers_id', (int)$customer_id );
      $QcountryIdCustomer->execute();

      $country_id_customer = $QcountryIdCustomer->fetch();
      $country_id_customer = $country_id_customer['entry_country_id'];


      $QcountryCode = $OSCOM_PDO->prepare("select countries_iso_code_2
                                             from :table_countries
                                             where countries_id = :countries_id
                                            ");
      $QcountryCode->bindInt(':countries_id',(int)$country_id_customer);
      $QcountryCode->execute();

      $country_code = $QcountryCode->fetch();
      $country_code = $country_code['countries_iso_code_2'];


// **********************************
// search id country odoo
// **********************************
      $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
      $field_list = array('country_id',
                        'name'
                      );

      $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
      $country_id_odoo = $country_id_odoo[0][id];


// **********************************
// write data
// **********************************

      $values = array (
                    "ref" => new xmlrpcval('WebStore - ' . $customer_id, "string"),
                    "phone" => new xmlrpcval($customers_telephone,"string"),
                    "mobile" => new xmlrpcval($customers_cellular_phone,"string"),
                    "fax" => new xmlrpcval($customers_fax,"string"),
                    "street" => new xmlrpcval($customers_street_address,"string"),
                    "street2"=> new xmlrpcval($customers_suburb,"string"),
                    "zip" => new xmlrpcval($customers_postcode,"string"),
                    "city" => new xmlrpcval($customers_city,"string"),
                    "comment" => new xmlrpcval('Website Registration - Admin Creation', "string"),
                    "name"    => new xmlrpcval( $customers_lastname . ' ' . $customers_firstname, "string"),
                    "email"  => new xmlrpcval($customers_email_address, "string"),
                    "country_id" => new xmlrpcval($country_id_odoo,"int"),
                    "tz"      => new xmlrpcval("Europe/Paris", "string"),
                    "opt_out"  => new xmlrpcval($customers_newsletter, "double"),
                    );

      $OSCOM_ODOO->createOdoo($values, "res.partner");
?>