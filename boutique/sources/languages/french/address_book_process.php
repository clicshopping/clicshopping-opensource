<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Carnet d\'adresses');

define('HEADING_TITLE','Gestion des adresses');

define('NAVBAR_TITLE_ADD_ENTRY', 'Nouvelle entrée');
define('NAVBAR_TITLE_MODIFY_ENTRY', 'Mise à jour entrée');
define('NAVBAR_TITLE_DELETE_ENTRY', 'Supprimer entrée');

define('HEADING_TITLE_ADD_ENTRY', 'Ajouter une nouvelle adresse');
define('HEADING_TITLE_MODIFY_ENTRY', 'Modification de l\'adresse');
define('HEADING_TITLE_DELETE_ENTRY', 'Supprimer une adresse de votre carnet');

define('DELETE_ADDRESS_TITLE', 'Adresse à supprimer');
define('DELETE_ADDRESS_DESCRIPTION', 'Etes-vous s&ucirc;r de vouloir supprimer l\'adresse cette adresse ?');

define('NEW_ADDRESS_TITLE', 'Nouvelle adresse');

define('SELECTED_ADDRESS', 'Adresse choisie');
define('SET_AS_PRIMARY', 'Sélectionner comme adresse principale.');

define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'L\'adresse sélectionnée a été supprimée avec succès de votre carnet d\'adresses.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'Votre carnet d\'adresses a été mis à jour avec succès.');

define('WARNING_PRIMARY_ADDRESS_DELETION', 'L\'adresse principale ne peut pas etre suprimée. Veuillez sélectionner une autre adresse comme adresse principale et réessayez.');

define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'L\'entrée du carnet d\'adresses n\'existe pas. ');
define('ERROR_ADDRESS_BOOK_FULL', 'Votre carnet d\'adresses est plein. Veuillez supprimer une adresse inutile pour en enregistrer une nouvelle.');
define('ERROR_ADDRESS_BOOK_NO_ADD', 'Vous n\'etes pas autorisé à ajouter une nouvelle adresse dans votre carnet personnel. N\'hésitez pas à nous contacter pour recevoir plus d\'informations sur cette impossibilité.');
define('ERROR_ADDRESS_BOOK_NO_MODIFY_DEFAULT', 'Vous n\'etes pas autorisé à modifier l\'adresse principale de votre carnet personnel. N\'hésitez pas à nous contacter pour recevoir plus d\'informations sur cette impossibilité.');
?>