<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class d_total_ca_by_year {
    var $code = 'd_total_ca_by_year';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_total_ca_by_year() {
      $this->title = MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS == 'True');
      }
    }

    function getOutput() {
     global $OSCOM_PDO;
     
      $year = array();
      for($i = 0; $i < 30; $i++) {
        $year[date('Y-m-d', strtotime('-'. $i .' days'))] = 0;
      }

      $Qorder = $OSCOM_PDO->prepare('select date_format(o.date_purchased, "%Y") as year,
                                           sum(ot.value) as total
                                    from :table_orders  o,
                                         :table_orders_total ot
                                    where  o.orders_id = ot.orders_id
                                    and o.orders_status = 3
                                    and ot.class = :class
                                    group by year
                                   ');
      $Qorder->bindValue(':class', 'ot_subtotal');
      $Qorder->execute();

      while ($orders = $Qorder->fetch() ) {
        $year[$orders['year']] = $orders['total'];
      }

      $year = array_reverse($year, true);

      $js_array = '';
      foreach ($year as $date => $total) {
        $js_array .= '[' . (mktime(0, 0, 0, substr($date, 5, 2), substr($date, 8, 2), substr($date, 0, 4))*1000) . ', ' . $total . '],';
      }

      if (!empty($js_array)) {
        $js_array = substr($js_array, 0, -1);
      }

      $chart_label = osc_output_string(MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_CHART_LINK);
      $chart_label_link = osc_href_link('orders.php');
      $chart_title = osc_output_string(MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_TITLE);

      $output = <<<EOD
<div style="float:left; width:50%;">
<div style="text-align: center;">$chart_title</div>
<div id="d_total_ca_by_year" style="width: 100%; height: 200px;"></div>
<script language="javascript" type="text/javascript">
$(function () {
  var plot30 = [$js_array];
  $.plot($("#d_total_ca_by_year"), [ { 
    label: '$chart_label',
    data: plot30,
    bars: {
      show: true, 
      fill: true, 
      lineWidth: 20,
      barWidth: 20,
      align:  "center"},
    color: '#2a6ac4'
    }], {

    xaxis: {
      ticks: 4,
      mode: 'time'
    },

    yaxis: {
      ticks: 5,
      min: 0
    },

    grid: {
      backgroundColor: { colors:  ['#FAFAFA', '#FAFAFA'] }, //gradient ['#d3d3d3', '#fff']
      hoverable: true,
      borderWidth: 1
    },


    legend: {
      labelFormatter: function(label, series) {
        return '<a href="$chart_label_link">' + label + '</a>';
      }
    }
  });
});
</script>
</div>
EOD;

      return $output;
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS');
    }

    function install() {
     global $OSCOM_PDO;
     
      if ($_SESSION['languages_id'] =='1') {
        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_SORT_ORDER',
            'configuration_value' => '30',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montré en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '99',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      }else {
        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Do you want enable this Module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Do you want enable this Module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Sort Order',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_SORT_ORDER',
            'configuration_value' => '30',
            'configuration_description' => 'Sort order of display. Lowest is displayed first.',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );
      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_STATUS', 
                   'MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_SORT_ORDER'
                  );
    }
  }
