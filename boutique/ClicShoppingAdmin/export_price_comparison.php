<?php
/*
 * export_price_comparison.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $QCountProducts = $OSCOM_PDO->prepare('select count(p.products_id) as count_products
                                         from :table_products p,
                                              :table_products_description pd
                                         where p.products_status = :products_status
                                         and p.products_archive = :products_archive
                                         and p.products_id = pd.products_id 
                                         and pd.language_id = :language_id
                                         and p.products_price_comparison = :products_price_comparison
                                        ');

  $QCountProducts->bindValue(':products_status', 1  );
  $QCountProducts->bindValue(':products_archive', 0 );
  $QCountProducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $QCountProducts->bindValue(':products_price_comparison', 0 );

  $QCountProducts->execute();

  $count_products = $QCountProducts->fetch();


 require('includes/header.php');
?>

<script language="Javascript">
function affiche()
{
var val;
val = "<?php echo HTTP_CATALOG_SERVER . DIR_WS_CATALOG . 'export_price_comparison.php' ."?format="; ?>";
val += document.ExU.format.value;
val += "&p=";
val += document.ExU.p.value;
val += "&language=";
val += document.ExU.language.value;
if (document.ExU.tax[0].checked) {
    val += "&tax="; 
    val += document.ExU.tax[0].value;
}
if (document.ExU.ean[0].checked) {
    val += "&ean="; 
    val += document.ExU.ean[0].value;
}
if (document.ExU.cache[0].checked) {
    val += "&cache=";
    val += document.ExU.cache[0].value;
    if (document.ExU.rep.checked) val += "&rep=1";
    val += "&fichier=";
    val += document.ExU.fichier.value;
  }
val += "&libre=";
val += document.ExU.libre.value;
document.ExU.url.value = val;
}
</script>
<!-- body //-->
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/stats_customers.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-4 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">

      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr><form name="ExU">
            <td class="mainTitle"><table border="0" cellpadding="0" cellpadding="5">
              <tr>
                <td class="mainTitle"><?php echo TEXT_TITLE_EXPORT; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <table width="100%" border="0" cellpadding="2" cellspacing="2" class="adminformTitle">
              <tr>
               <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
<?php

  function netoyage_html() {
   return false;
  }
  $comparateur = '';
  $comparateur[] = array('id' => '', 'text' => '-- ' . COMPARATEUR_SELECT_EXPORT . ' --');
  $dir = opendir(DIR_FS_CATALOG_MODULES . 'export/');
  while ($fichier = readdir($dir)) {
    if(substr($fichier,-3) == 'php') {
      include(DIR_FS_CATALOG_MODULES . 'export/' . $fichier);
      foreach ($comp as $value){
        $comparateur[] = array('id' => $fichier, 'text' => $value);
      }
    }
  }
  closedir($dir);
  
  $languages = osc_get_languages();
  $languages_array = array();
  $languages_selected = DEFAULT_LANGUAGE;
  for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
    $languages_array[] = array('id' => $languages[$i]['code'],
                               'text' => $languages[$i]['name']);
  }   
?>
              <tr>
                <td class="main"><?php echo COMPARATEUR_SELECT; ?><font color="#FF0000">*</font></td>
                <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?><?php echo osc_draw_pull_down_menu('format', $comparateur, '', 'onchange ="affiche()"');?></td>
                </tr>
              <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                 <td class="main"><?php echo COMPARATEUR_LNG; ?><font color="#FF0000">*</font></td>
                <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?><?php echo osc_draw_pull_down_menu('language', $languages_array, '', 'onchange ="affiche()"');?></td>
             </tr>
              <tr>
                <td class="main"><?php //echo COMPARATEUR_CODE; ?></td>
                <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?><?php echo osc_draw_hidden_field('p', EXPORT_CODE, 'onblur ="affiche()"'); ?></td>
                </tr>
              <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
               <td class="main"><?php echo COMPARATEUR_TAX; ?><font color="#FF0000">*</font></td>
               <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?>
                  <input type="radio" name="tax" value="true" onChange ="affiche()" />
                  <?php echo COMPARATEUR_OUI; ?>
                  <input type="radio"  name="tax" value="false" checked="checked" onChange ="affiche()" />
                  <?php echo COMPARATEUR_NON; ?> </td>
              </tr>
              <tr>
               <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
               <td class="main"><?php echo COMPARATEUR_EAN; ?><font color="#FF0000">*</font></td>
               <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?>
                  <input type="radio" name="ean" value="true" onChange ="affiche()" />
                  <?php echo COMPARATEUR_OUI; ?>
                  <input type="radio"  name="ean" value="false" checked="checked" onChange ="affiche()" />
                  <?php echo COMPARATEUR_NON; ?> </td>
              </tr>
              <tr>
               <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td valign="top" class="main"><?php echo COMPARATEUR_SECU; ?><font color="#FF0000">*</font></td>
                <td valign="top" class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?>
                  <input name="rep" type="checkbox" id="rep" value="1" onBlur ="affiche()" /></td>
                </tr>
              <tr>
               <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
               <td class="main"><?php echo COMPARATEUR_CACHE; ?><font color="#FF0000">*</font></td>
               <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?>
                  <input name="cache"  type="radio" value="true" onChange ="affiche()" />
                  <?php echo COMPARATEUR_OUI; ?>
                  <input name="cache" type="radio" value="false" checked="checked" onChange ="affiche()" />
                  <?php echo COMPARATEUR_NON; ?> 
               </td>
              </tr>
              <tr>
               <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
               <td>&nbsp;</td>
              </tr>
              <tr>
               <td class="main"><?php echo COMPARATEUR_FICHIER; ?><font color="#FF0000">*</font></td>
                <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?><?php echo osc_draw_input_field('fichier', '', 'onchange ="affiche()"'); ?><span class="smalltext"><?php echo ' ' . COMPARATEUR_OBLIG; ?></span></td>
              </tr>
              <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="main"><?php echo COMPARATEUR_CHAMP; ?></td>
                <td class="main"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?><?php echo osc_draw_input_field('libre', '', 'onchange ="affiche()"'); ?></td>
              </tr>
              <tr>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                 <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="main" colspan="2"><?php echo COMPARATEUR_URL; ?></td>
              </tr>
              <tr>
                <td class="main" colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?><?php echo osc_draw_input_field('url', '', 'size="125"'); ?></td>
              </tr>
              <tr>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                 <td>&nbsp;</td>
              </tr>
            </table></td>
          </form></tr>
          <tr>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformAide">
              <tr>
                <td><table border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_EXPORT_IMAGE); ?></td>
                    <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_EXPORT; ?></strong></td>
                  </tr>
                  <tr>
                    <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                    <td class="main"><?php echo TEXT_AIDE_EXPORT; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
?>
