<?php
/**
 * admin_backup_direcotry_listing.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_SECURITY_CHECK_EXTENDED_ADMIN_BACKUP_DIRECTORY_LISTING_HTTP_200', 'I am able to access the ' . DIR_FS_BACKUP . ' directory - please disable directory listing for this directory in your web server configuration.');
?>
