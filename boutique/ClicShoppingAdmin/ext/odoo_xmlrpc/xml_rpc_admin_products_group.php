<?php
  /*
   * xml_rpc_products_group.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */


// **********************************
// Search odoo id ofproducts group
// **********************************

  $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('clicshopping_customers_group_id', '=', $groups_id, "clicshopping.products.group","int",
                                              'clicshopping_products_id', '=', $pricek_values['products_id'], "int");

// **********************************
// read id Categories odoo
// **********************************

  $field_list = array('id');

  $QodooCustomersGroupId = $OSCOM_ODOO->readOdoo($ids, $field_list, "clicshopping.products.group");
  $odoo_customers_group_id = $QodooCustomersGroupId[0][id];

  if  (empty($odoo_customers_group_id)) {

    $values = array("clicshopping_customers_group_id" => new xmlrpcval($groups_id, "int"),
                    "clicshopping_customers_group_price" => new xmlrpcval($newprice, "double"),
                    "clicshopping_products_id" => new xmlrpcval($pricek_values['products_id'], "int"),
                    "clicshopping_products_price" => new xmlrpcval($pricek, "double"),
                    );

    $OSCOM_ODOO->createOdoo($values, "clicshopping.products.group");

  } else {

    $values = array("clicshopping_customers_group_id" => new xmlrpcval($groups_id, "int"),
                    "clicshopping_customers_group_price" => new xmlrpcval($newprice, "double"),
                    "clicshopping_products_id" => new xmlrpcval($pricek_values['products_id'], "int"),
                  );

    $OSCOM_ODOO->updateOdoo($odoo_customers_group_id, $values, "clicshopping.products.group");

  }