<?php
/*
 * checkout_shipping_address.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the login page
  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($_SESSION['cart']->count_contents() < 1) {
    osc_redirect(osc_href_link('shopping_cart.php'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('checkout_shipping_address'));

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
  if ($order->content_type == 'virtual') {
    $_SESSION['shipping'] = false;
    $_SESSION['sendto'] = false;
    osc_redirect(osc_href_link('checkout_payment.php', '', 'SSL'));
  }

  $error = false;
  $process = false;

    if ((int)$_GET['newcustomer'] == 1) {

      $Qaddresse = $OSCOM_PDO->prepare('select customers_default_address_id
                                       from :table_customers
                                       where customers_id = :customers_id
                                     ');
      $Qaddresse->bindInt(':customers_id',(int)$OSCOM_Customer->getID());
      $Qaddresse->execute();

      if ($Qaddresse->rowCount() == 1) {
          $addresses_book = $Qaddresse->fetch();
          $addresses_book_id = $addresses_book['customers_default_address_id'];
          osc_redirect(osc_href_link('address_book_process.php','newcustomer=1&shopping=1&edit='.$addresses_book_id,'SSL'));
      }
    }

    if (isset($_POST['action']) && ($_POST['action'] == 'submit') && isset($_POST['formid']) && ($_POST['formid'] == $_SESSION['sessiontoken'])) {
// process a new shipping address

      if ( !$OSCOM_Customer->hasDefaultAddress() || (isset($_POST['firstname']) && !empty($_POST['firstname']) && isset($_POST['lastname']) && !empty($_POST['lastname']) && isset($_POST['street_address']) && !empty($_POST['street_address'])) ) {

        $process = true;


        if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? trim($_POST['gender']) : null;
        if (ACCOUNT_COMPANY == 'true') $company = isset($_POST['company']) ? trim($_POST['company']) : null;
        $firstname = trim($_POST['firstname']);
        $lastname = trim($_POST['lastname']);
        $street_address = trim($_POST['street_address']);
        if (ACCOUNT_SUBURB == 'true') $suburb = isset($_POST['suburb']) ? trim($_POST['suburb']) : null;
        $postcode = isset($_POST['postcode']) ? trim($_POST['postcode']) : null;
        $city = isset($_POST['city']) ? trim($_POST['city']) : null;
        $country = isset($_POST['country']) ? trim($_POST['country']) : null;
        $entry_telephone = isset($_POST['entry_telephone']) ? trim($_POST['entry_telephone']) : null;

        if ( ACCOUNT_STATE == 'true' ) {
          $zone_id = isset($_POST['zone_id']) ? trim($_POST['zone_id']) : null;
          $state = isset($_POST['state']) ? trim($_POST['state']) : null;
        }

        if ((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          if ( ($gender != 'm') && ($gender != 'f') ) {
            $error = true;

            $OSCOM_MessageStack->addError('checkout_address', ENTRY_GENDER_ERROR);
          }
        } else if ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          if ( ($gender != 'm') && ($gender != 'f') ) {
            $error = true;

            $OSCOM_MessageStack->addError('checkout_address', ENTRY_GENDER_ERROR_PRO);
          }
        }

        if ((strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_FIRST_NAME_ERROR);
        } else if ((strlen($firstname) < ENTRY_FIRST_NAME_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_FIRST_NAME_ERROR_PRO);
        }

        if ((strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_LAST_NAME_ERROR);
        } else if ((strlen($lastname) < ENTRY_LAST_NAME_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_LAST_NAME_ERROR_PRO);
        }

        if ((strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_STREET_ADDRESS_ERROR);
        } else if ((strlen($street_address) < ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_STREET_ADDRESS_ERROR_PRO);
        }

        if ((strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_POST_CODE_ERROR);
        } else if ((strlen($postcode) < ENTRY_POSTCODE_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_POST_CODE_ERROR_PRO);
        }

        if ((strlen($city) < ENTRY_CITY_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_CITY_ERROR);
        } else if ((strlen($city) < ENTRY_CITY_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_CITY_ERROR_PRO);
        }

        if (((ACCOUNT_STATE == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_STATE_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
          $zone_id = 0;


          $Qcheck = $OSCOM_PDO->prepare('select zone_id
                                         from :table_zones
                                         where zone_country_id = :zone_country_id
                                         limit 1');
          $Qcheck->bindInt(':zone_country_id', $country);
          $Qcheck->execute();

          $entry_state_has_zones = ($Qcheck->fetch() !== false);

          if ( $entry_state_has_zones === true ) {
            $Qzone = $OSCOM_PDO->prepare('select distinct zone_id
                                          from :table_zones
                                          where zone_country_id = :zone_country_id
                                          and (zone_name = :zone_name or zone_code = :zone_code)
                                        ');
            $Qzone->bindInt(':zone_country_id', $country);
            $Qzone->bindValue(':zone_name', $state);
            $Qzone->bindValue(':zone_code', $state);
            $Qzone->execute();

            if (count($Qzone->fetchAll()) === 1) {
              $zone_id = $Qzone->valueInt('zone_id');
            } else {
              $error = true;

              if ($OSCOM_Customer->getCustomersGroupID() == 0) {
                  $OSCOM_MessageStack->addError('checkout_address', ENTRY_STATE_ERROR_SELECT);
              } else if ($OSCOM_Customer->getCustomersGroupID() != 0) {
                  $OSCOM_MessageStack->addError('checkout_address', ENTRY_STATE_ERROR_SELECT_PRO);
              }
            }
          } else {
            if ((strlen($state) < ENTRY_STATE_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() == 0)) {
              $error = true;

              $OSCOM_MessageStack->addError('checkout_address', ENTRY_STATE_ERROR);
            } else if ((strlen($state) < ENTRY_STATE_PRO_MIN_LENGTH) && ($OSCOM_Customer->getCustomersGroupID() != 0)) {
              $error = true;

              $OSCOM_MessageStack->addError('checkout_address', ENTRY_STATE_ERROR_PRO);
            }
          }
        }
// Clients B2C et B2B : Controle de la selection du pays
        if ((is_numeric($country) == false) && ($OSCOM_Customer->getCustomersGroupID() == 0) || ($country < 1)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_COUNTRY_ERROR);
        } else if ((is_numeric($country) == false) && ($OSCOM_Customer->getCustomersGroupID() != 0) || ($country < 1)) {
          $error = true;

          $OSCOM_MessageStack->addError('checkout_address', ENTRY_COUNTRY_ERROR_PRO);
        }

        if ($error == false) {
          $sql_data_array = array('customers_id' => $OSCOM_Customer->getID(),
                                  'entry_firstname' => $firstname,
                                  'entry_lastname' => $lastname,
                                  'entry_street_address' => $street_address,
                                  'entry_postcode' => $postcode,
                                  'entry_city' => $city,
                                  'entry_country_id' => $country,
                                  'entry_telephone' => $entry_telephone
                                  );

          if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
          if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
          if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
          if (ACCOUNT_STATE == 'true') {
            if ($zone_id > 0) {
              $sql_data_array['entry_zone_id'] = $zone_id;
              $sql_data_array['entry_state'] = '';
            } else {
              $sql_data_array['entry_zone_id'] = '0';
              $sql_data_array['entry_state'] = $state;
            }
          }

          $OSCOM_PDO->save('address_book', $sql_data_array);

          $_SESSION['sendto'] = $OSCOM_PDO->lastInsertId();

          if ( !$OSCOM_Customer->hasDefaultAddress() ) {
            $OSCOM_Customer->setCountryID($country);
            $OSCOM_Customer->setZoneID(($zone_id > 0) ? (int)$zone_id : '0');
            $OSCOM_Customer->setDefaultAddressID($_SESSION['sendto']);
          }


//***************************************
// odoo web service : Customer create another address
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
            require ('ext/odoo_xmlrpc/xml_rpc_catalog_checkout_shipping_address.php');
          }
//***************************************
// End odoo web service
//***************************************

          if ( isset($_SESSION['shipping']) ) {
            unset($_SESSION['shipping']);
          }

         osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
        }
// process the selected shipping destination
      } elseif (isset($_POST['address'])) {
        $reset_shipping = false;

        if (isset($_SESSION['sendto'])) {
          if ($_SESSION['sendto'] != $_POST['address']) {
            if (isset($_SESSION['shipping'])) {
              $reset_shipping = true;
            }
          }
        }

        $_SESSION['sendto'] = $_POST['address'];

        $Qcheck = $OSCOM_PDO->prepare('select address_book_id
                                       from :table_address_book
                                       where address_book_id = :address_book_id
                                       and customers_id = :customers_id
                                      ');
        $Qcheck->bindInt(':address_book_id', (int)$_SESSION['sendto']);
        $Qcheck->bindInt(':customers_id', $OSCOM_Customer->getID());
        $Qcheck->execute();

        if ( $Qcheck->fetch() !== false ) {

//***************************************
// odoo web service : Customer select another address
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_CUSTOMERS_CATALOG == 'true') {
          $_POST['odoo_create_shipping_address'] = '1';
          require ('ext/odoo_xmlrpc/xml_rpc_catalog_checkout_shipping_address.php');
        }
//***************************************
// End odoo web service
//***************************************

          if ( $reset_shipping == true ) {
            unset($_SESSION['shipping']);
          }

          osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
        } else {
          unset($_SESSION['sendto']);
        }
      } else {

// no addresses to select from - customer decided to keep the current assigned address
        $_SESSION['sendto'] = $OSCOM_Customer->getDefaultAddressID();

        osc_redirect(osc_href_link('checkout_shipping.php', '', 'SSL'));
      }
    }

// if no shipping destination address was selected, use their own address as default
    if (!isset($_SESSION['sendto'])) {
      $_SESSION['sendto'] = $OSCOM_Customer->getDefaultAddressID();
    }

//select the customer address
  $Qaddresses = $OSCOM_PDO->prepare('select address_book_id,
                                           entry_firstname as firstname,
                                           entry_lastname as lastname,
                                           entry_company as company,
                                           entry_street_address as street_address,
                                           entry_suburb as suburb,
                                           entry_city as city,
                                           entry_postcode as postcode,
                                           entry_state as state,
                                           entry_zone_id as zone_id,
                                           entry_country_id as country_id,
                                           entry_telephone as telephone
                                  from :table_address_book
                                  where customers_id = :customers_id
                                  ');
  $Qaddresses->bindInt(':customers_id', (int)$OSCOM_Customer->getID());

  $Qaddresses->execute();

// count the adress customer
  $addresses_count = osc_count_customer_address_book_entries();

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('checkout_shipping.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('checkout_shipping_address.php', '', 'SSL'));

  require($OSCOM_Template->getTemplateFiles('checkout_shipping_address'));
