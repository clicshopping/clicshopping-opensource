<?php
/*
 * ssl_check.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('ssl_check'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('ssl_check.php'));

 require($OSCOM_Template->getTemplateFiles('ssl_check'));
