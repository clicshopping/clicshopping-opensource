<?php
/*
 * reviews.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
// ---------------------- 
// ---  details   -----  
// ---------------------- 
?>
<div class="contentContainer">
<?php
  if ($products_split->number_of_rows > 0) {
    $Qproducts = $OSCOM_PDO->prepare($products_split->sql_query);
    $Qproducts->execute();

    if ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3')) {
?>
        <div class="clearfix"></div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="row">
          <div class="col-sm-6 pagenumber hidden-xs">
            <?php echo $products_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?>
          </div>
          <div class="col-sm-6">
            <div class="pull-right pagenav"><ul class="pagination"><?php echo $products_split->display_links(MAX_DISPLAY_PAGE_LINKS, osc_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></ul></div>
            <span class="pull-right"><?php echo TEXT_RESULT_PAGE; ?></span>
          </div>
        </div>
        <div class="clearfix"></div>
<?php
    }

    while ($products = $Qproducts->fetch() ) {

      $products_id = (int)$products['products_id'];
// **************************
//    product name
// **************************
      $products_name = '<a href="'.osc_href_link('product_info.php', 'products_id=' . $products_id) . '">' . $OSCOM_ProductsListing->getProductsName($products_id) .'</a>';
      $products_name_image =  $OSCOM_ProductsListing->getProductsName($products_id);
// *************************
//       Flash discount
// **************************
      $products_flash_discount = '';
      if ($OSCOM_ProductsListing->getProductsFlashDiscount($products_id) != '') {
        $products_flash_discount =  TEXT_FLASH_DISCOUNT . $OSCOM_ProductsListing->getProductsFlashDiscount($products_id);
      }
// *************************
// display the differents prices before button
// **************************
      $product_price = $OSCOM_ProductsListing->getCustomersPrice($products_id);
// **************************
// See the button more view details
// **************************
        $button_small_view_details = osc_draw_button(SMALL_IMAGE_BUTTON_DETAILS, '', osc_href_link('product_info.php', 'products_id='. $products_id), 'info', null,'small');
// **************************
//  Display the image
// **************************
        $products_image = '<a href="' . osc_href_link('product_info.php', 'products_id=' . $products_id, 'NONSSL', false) . '">' . osc_image(DIR_WS_IMAGES . $OSCOM_ProductsListing->getProductsImage($products_id), $products_name_image, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, null, true) . '</a>';
?>
      <div class="contentText">
        <div class="row">
          <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
          <div class="col-sm-10">
            <h3><?php echo '<a href="' . osc_href_link('product_reviews_info.php', 'products_id=' . $products_id . '&reviews_id=' . $products['reviews_id']) . '"><u><strong>' . $products_name_image . '</strong></u></a> <span class="smallText">' . sprintf(TEXT_REVIEW_BY, osc_output_string_protected($products['customers_name'])) . '</span>'; ?></h3>
            <span><?php echo sprintf(TEXT_REVIEW_DATE_ADDED,  $OSCOM_Date->getLong($products['date_added'])); ?></span>
          </div>
        </div>

        <div class="row">
          <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
          <div class="col-sm-9">
            <?php echo osc_break_string(osc_output_string_protected($products['reviews_text']), 60, '-<br />') . ((strlen($products['reviews_text']) >= 100) ? '...' : ''); ?>
            <br /><br />
             <?php echo '<br /><br />' .  sprintf(TEXT_REVIEW_RATING, osc_draw_stars($products['reviews_rating']), sprintf(TEXT_OF_5_STARS, $products['reviews_rating'])) . '<a href="' . osc_href_link('product_reviews_info.php', 'products_id=' . $products_id . '&reviews_id=' . $products['reviews_id']) . '"><span class="pull-right label label-info">'. REVIEWS_TEXT_READ_MORE .'</span></a>'; ?>
          </div>
          <div class="col-sm-3">
            <div class="pull-right" style="text-align: center;">
              <div>
                <?php  echo $products_image; ?>
                <div class="clearfix"></div>
                <div>
<?php
  echo  TEXT_PRICE . ' ' . $product_price .'<br />';
  echo $products_flash_discount . '<br />';
  echo $button_small_view_details . ' ';
?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="hr"></div>
<?php
     }
   } else {
?>
      <div class="contentContainer">
        <div class="contentText" style="padding-top:20px;">
          <div class="alert alert-info">
            <?php echo TEXT_NO_REVIEWS; ?>
          </div>
        </div>
        <div class="pull-right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, osc_href_link('index.php'), 'success'); ?></div>
      </div>
<?php
   }

  if (($products_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>
        <div class="clearfix"></div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="row">
          <div class="col-sm-6 pagenumber hidden-xs">
            <?php echo $products_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?>
          </div>
          <div class="col-sm-6">
            <div class="pull-right pagenav"><ul class="pagination"><?php echo $products_split->display_links(MAX_DISPLAY_PAGE_LINKS, osc_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></ul></div>
            <span class="pull-right"><?php echo TEXT_RESULT_PAGE; ?></span>
          </div>
        </div>
        <div class="clearfix"></div>
<?php
  }
?>
</div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>