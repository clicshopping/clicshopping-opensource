<?php
/**
 * index.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

define('HEADING_TITLE', 'Home');
define('HEADING_SHORT_ORDERS', 'Orders shortcut');
define('HEADING_SHORT_CATEGORIES', 'Products / Categories shortcut');
define('HEADING_TITLE_ORDERS','Orders');
define('HEADING_TITLE_DIVERS','Miscelleaneous');

define('BOX_TITLE_ORDERS', 'Orders');
define('BOX_TITLE_STATISTICS', 'Statistics');

define('BOX_ENTRY_CUSTOMERS', 'Customers number');
define('BOX_ENTRY_PRODUCTS', 'Products number');
define('BOX_ENTRY_PRODUCTS_OFF_LINE', 'Products numbe off line');
define('BOX_ENTRY_PRODUCTS_ARCHIVES', 'Products archives number');
define('BOX_ENTRY_REVIEWS', 'Comments number in pending');
define('BOX_ENTRY_NEWSLETTER','Newsletter subscription number');
define('BOX_ENTRY_NEWSLETTER_NO_ACCOUNT','Newsletter subscription number with no account');;
define('BOX_ENTRY_NOTIFICATION','Products notification number');
define('BOX_ENTRY_STOCK_EXPECTING','Products expected number');
define('BOX_ENTRY_CUSTOMER_BASKET','Recover cart sales number');
define('BOX_ENTRY_BASKET','Average basket by customer (delivery status)');
define('BOX_ENTRY_BUY_AVERAGE_ORDER_TOTAL_DELIVERY', 'Average purchase by order (delivery status)');
define('BOX_ENTRY_CONTACT_DEMAND_NO_ARCHIVE','Contact requests not archived');


define('BOX_CONNECTION_PROTECTED', 'You are protected by %s a SSL connexion');;
define('BOX_CONNECTION_UNPROTECTED', 'You are <font color="#ff0000">not</font> protected by a SSL connexion.');
define('BOX_CONNECTION_UNKNOWN', 'Unknown');

define('CATALOG_CONTENTS', 'Content');
define('REPORTS_PRODUCTS', 'Products');
define('REPORTS_ORDERS', 'Orders');

define('TITLE_DB_INDEX','Size of Data Base');
define('TITLE_WEB_SITE_SIZE','Taille du site internet');
define('TITLE_CUSTOMER_INFORMATION','Informations sur le compte client');
define('TITLE_IMAGINIS_INFORMATION','Informations ClicShopping');

define('TAB_IMAGINIS','ClicShopping informations');
define('TAB_STATISITICS','Statistics');
define('TAB_CUSTOMER','Account & Server');

define('PACK','Pack : ');
define('CONTRACT_DATE', 'Birthday date : ');
define('EMAIL_SIZE','Mailbox size : ');
define('EMAIL_SIZE_INFO','20 Mo');
define('CONTRACT_RENEWAL','Contract Renewal : ');
define('CONTRATC_RENEWAL_INFO','Automatic renewal anniversary');
define('NOTICE_TERMINATION','Notice of termination : ');
define('NOTICE_TERMINATION_INFO','2 months before deadline');
define('SPACE','Total storage space : ');
define('BACKUP_SITE','backup Site : ');
define('BACKUP_SITE_INFO','Daily');
define('ASK_INFORMATIONS','Request Information : ');
define('BANDWIDTH','Band width by month');

define('ONGLET_STAT_GENERALES','General statistics');
define('ONGLET_STAT_CA','Turn Over');
define('ONGLET_MON_COMPTE','My account');
define('ONGLET_COMMERCE','Infos RSS');
define('ONGLET_OPTIONS','Options');

define('TITLE_STAT_CA_EVOLUTION','Turnover cumulative per month (delivery status)');
define('TITLE_STATS_GROWTH_CA','Growth of your Turn Over');
define('TITLE_STATS_AVERAGE_SALES','Annual Average growth of your basket');
define('STATUS','Orders status total');
define('STATISTICS','Main Statistics');

define('TEXT_STATS_MAN','% men<br /> Average age ');
define('TEXT_STATS_WOMAN','% women<br /> Average age ');
define('TEXT_YEAR','years');

define('YEAR_N0','Year n');
define('YEAR_N1','Year n-1');
define('YEAR_N2','Year n-2');
define('YEAR_N3','Year n-3');

define('ICON_EDIT_ORDER','Edit order');
define('ICON_EDIT_CUSTOMER','Edit customer');
define('ICON_VIEW_CUSTOMERS_ALL_ORDERS','See all customer orders');
define('ICON_VIEW_ORDER','See the customer order');


define('TEXT_ERROR_TWITTER','Error, Please insert a message.<br />');	
define('TEXT_SUBSCRIPTION_TWITTER','Take a subscription on <a href="http://www.twitter.com" target="_blank">Twitter</a> to benefit the power of social web for your marketing development.<br />After configure ClicShopping (Configuration / my administration / Web social configuration) to use Twitter.');

// Email customer information
define('EMAIL_CUSTOMERS_J90','
Dear customer, <br /> <br />
We inform you on your products evolution concernerning your shopping cart and we will send you a reminder 90, 60, 30, 15, 7 days before they expire. <br /> <br />
To renew your service, simply send us a check upon receipt of your invoice.
<br /> <br />
As stated in the contract, any cancellation must be made 60 days before the anniversary date of your contract by registered mail with return receipt. After this period, the contract is renewed automatically. <br /> <br />
If you have already paid your bill, please disregard this message
<br /> <br />
For more information, please contact us via our support: http://www.e-imaginis.com/support
<br /> <br />
Sincerely <br /> <br />
the customer team 
');

define('EMAIL_CUSTOMERS_J60','
Dear customer, <br /> <br />
We inform you on your products evolution concernerning your shopping cart  and we will send you a reminder  60, 30, 15, 7 days before they expire. <br /> <br />
To renew your service, simply send us a check upon receipt of your invoice.
<br /> <br />
As stated in the contract, any cancellation must be made 60 days before the anniversary date of your contract by registered mail with return receipt. After this period, the contract is renewed automatically. <br /> <br />
If you have already paid your bill, please disregard this message
<br /> <br />
For more information, please contact us via our support: http://www.e-imaginis.com/support
<br /> <br />
Sincerely <br /> <br />
the customer team 
');


define('EMAIL_CUSTOMERS_J30','
Dear customer, <br /> <br />
We inform you on your products evolution concernerning your shopping cart  and we will send you a reminder 30, 15, 7 days before they expire. <br /> <br />
To renew your service, simply send us a check upon receipt of your invoice.
<br /> <br />
As stated in the contract, any cancellation must be made 60 days before the anniversary date of your contract by registered mail with return receipt. After this period, the contract is renewed automatically. <br /> <br />
If you have already paid your bill, please disregard this message
<br /> <br />
For more information, please contact us via our support: http://www.e-imaginis.com/support
<br /> <br />
Sincerely <br /> <br />
the customer team 
');


define('EMAIL_CUSTOMERS_J15','Dear customer, <br /> <br />
We inform you on your products evolution concernerning your shopping cart  and we will send you a reminder 15, 7 days before they expire. <br /> <br />
<br /><br />
To renew your service, simply send us a check upon receipt of your invoice.
<br /> <br />
As stated in the contract, any cancellation must be made 60 days before the anniversary date of your contract by registered mail with return receipt. After this period, the contract is renewed automatically. <br /> <br />
If you have already paid your bill, please disregard this message
<br /> <br />
For more information, please contact us via our support: http://www.e-imaginis.com/support
<br /> <br />
Sincerely <br /> <br />
the customer team 
');

define('EMAIL_CUSTOMERS_J7','
Dear customer, <br /> <br />
We inform you a last time on your products evolution concernerning your shopping cart and we will send you a reminder 7 days before they expire. <br /> <br />
To renew your service, simply send us a check upon receipt of your invoice.
<br /> <br />
As stated in the contract, any cancellation must be made 60 days before the anniversary date of your contract by registered mail with return receipt. After this period, the contract is renewed automatically. <br /> <br />
If you have already paid your bill, please disregard this message
<br /> <br />
For more information, please contact us via our support: http://www.e-imaginis.com/support
<br /> <br />
Sincerely <br /> <br />
the customer team 
');


define('ADMINISTRATOR_EMAIL',' Hello Administrator,<br /><br />
This is the final reminder about renewing the contract for the shop '. STORE_OWNER. '. <br /> <br />
Reminders are made every 90, 60, 30, 15, 7 days before the contract expires. <br /> <br />
Remember to send a renewal invoice the '. STORE_OWNER. ' customer !
<br /> <br />
If you have already sent the invoice, please disregard this message.
<br /> <br />
For more information, please contact us via the customer service
<br /> <br />
Sincerely <br /> <br />
the customer team
');

define('ADMINISTRATOR_EMAIL_SUBJECT','Information for contract renewal ');
define('CUSTOMERS_EMAIL_SUBJECT','Information concerning the renewal of contract');
define('LINKEDIN_CERTIFICATE_CLICSHOPPING', 'Get Your ClicShopping Certification !');

