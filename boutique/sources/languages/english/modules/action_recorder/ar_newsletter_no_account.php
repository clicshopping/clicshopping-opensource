<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  define('MODULE_ACTION_RECORDER_ACCOUNT_NEWSLETTER_TITLE', 'Newsletter Anonymes');
  define('MODULE_ACTION_RECORDER_ACCOUNT_NEWSLETTER_DESCRIPTION', 'Enregistrement des utilisations du module Newsletter Anonymes.');
?>