<?php
/**
 * index.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */

define('HEADING_TITLE', 'Accueil');
define('HEADING_SHORT_ORDERS', 'Raccourci commandes');
define('HEADING_SHORT_CATEGORIES', 'Raccourci catégories / produits');
define('HEADING_TITLE_ORDERS','Commandes');
define('HEADING_TITLE_DIVERS','Divers');

define('BOX_TITLE_ORDERS', 'Commandes');
define('BOX_TITLE_STATISTICS', 'Statistiques');

define('BOX_ENTRY_CUSTOMERS', 'Nombre de clients');
define('BOX_ENTRY_PRODUCTS', 'Nombre de produits');
define('BOX_ENTRY_PRODUCTS_OFF_LINE', 'Nombre de produits hors ligne');
define('BOX_ENTRY_PRODUCTS_ARCHIVES', 'Nombre d\'archives');
define('BOX_ENTRY_REVIEWS', 'Nombre de commentaires en attente');
define('BOX_ENTRY_NEWSLETTER','Nombre de souscriptions à la Newsletter');
define('BOX_ENTRY_NEWSLETTER_NO_ACCOUNT','Nombre de souscriptions à la Newsletter sans compte');
define('BOX_ENTRY_NOTIFICATION','Nombre de produits surveillés');
define('BOX_ENTRY_STOCK_EXPECTING','Nombre de produits en attente ou hors stock');
define('BOX_ENTRY_CUSTOMER_BASKET','Nombre de commande pouvant être "recouvrés"');
define('BOX_ENTRY_BASKET','Achat moyen par client (statut livré)');
define('BOX_ENTRY_BUY_AVERAGE_ORDER_TOTAL_DELIVERY', 'Achat moyen par commande (statut livré)');
define('BOX_ENTRY_CONTACT_DEMAND_NO_ARCHIVE','Demandes de contact non Archivés');


define('BOX_CONNECTION_PROTECTED', 'Vous êtes protégé par %s une connexion SSL sécurisée.');
define('BOX_CONNECTION_UNPROTECTED', 'Vous n\'êtes <font color="#ff0000">pas</font> protégé par une connexion SSL sécurisée.');
define('BOX_CONNECTION_UNKNOWN', 'inconnu');

define('CATALOG_CONTENTS', 'Contenus');
define('REPORTS_PRODUCTS', 'Produits');
define('REPORTS_ORDERS', 'Commandes');

define('TITLE_DB_INDEX','Taille base de données');
define('TITLE_WEB_SITE_SIZE','Taille du site internet');
define('TITLE_CUSTOMER_INFORMATION','Informations sur le compte client');
define('TITLE_IMAGINIS_INFORMATION','Informations ClicShopping');

define('TAB_IMAGINIS','Informations ClicShopping');
define('TAB_STATISITICS','Statistiques');
define('TAB_CUSTOMER','Compte & Serveur');

define('PACK','Pack : ');
define('CONTRACT_DATE', 'Date anniversaire contrat : ');
define('EMAIL_SIZE','Taille par boite email : ');
define('EMAIL_SIZE_INFO','20 Mo');
define('CONTRACT_RENEWAL','Renouvellement du contrat : ');
define('CONTRATC_RENEWAL_INFO','Reconduction automatique à la date d\'anniversaire');
define('NOTICE_TERMINATION','Préavis de résiliation : ');
define('NOTICE_TERMINATION_INFO','2 mois avant échéance');
define('SPACE','Stockage total de l\'espace disque : ');
define('BACKUP_SITE','Sauvegarde du site : ');
define('BACKUP_SITE_INFO','journalière');
define('ASK_INFORMATIONS','Demande Informations : ');
define('BANDWIDTH','Bande passante totale par mois');

define('ONGLET_STAT_GENERALES','Statistiques Générales');
define('ONGLET_STAT_CA','Chiffre d Affaires');
define('ONGLET_MON_COMPTE','Mon Compte');
define('ONGLET_COMMERCE','Infos RSS');
define('ONGLET_OPTIONS','Options');

define('TITLE_STAT_CA_EVOLUTION','Evolution du CA cumulé par mois (Statut  livré)');
define('TITLE_STATS_GROWTH_CA','Croissance du chiffre d\'affaires');
define('TITLE_STATS_AVERAGE_SALES','Croissance moyenne annuelle du panier');
define('STATUS','Statut total des commandes');
define('STATISTICS','Statistiques Générales');

define('TEXT_STATS_MAN','% hommes <br />Age moyen ');
define('TEXT_STATS_WOMAN','% femmes <br />Age moyen ');
define('TEXT_YEAR','ans');

define('YEAR_N0','Année n');
define('YEAR_N1','Année n-1');
define('YEAR_N2','Année n-2');
define('YEAR_N3','Année n-3');

define('ICON_EDIT_ORDER','Editer la commande');
define('ICON_EDIT_CUSTOMER','Editer le client');
define('ICON_VIEW_CUSTOMERS_ALL_ORDERS','Voir toutes les commandes du client');
define('ICON_VIEW_ORDER','Voir la commande du client');


define('TEXT_ERROR_TWITTER','Erreur, veuillez insérer un message.<br />');
define('TEXT_SUBSCRIPTION_TWITTER','Inscrivez vous sur <a href="http://www.twitter.com" target="_blank">Twitter</a> pour bénéficier de toute la puissance des web sociaux dans votre strategie de développement marketing.<br />Puis configurer ClicShopping (Configuration / mon administration / Configuration du web social) pour pouvoir utiliser Twitter.');

// Email customer information
define('EMAIL_CUSTOMERS_J90','Cher(e) Client(e),<br /><br />
Nous vous informons sur l\'évolution de vos produits chez Imaginis et nous vous envoyons un rappel 90, 60, 30, 15, 7 jours avant leur expiration. <br /><br />
Pour renouveler vos services, il suffit de nous envoyer un chèque lors de la réception de votre facture qui vous sera envoyé au plus tard un mois avant l\expiration du contrat.
<br /><br />
Comme le stipule le contrat, toute résiliation doit se faire impérativement avant les 60 jours de la date d\'anniversaire de votre contrat par envoi recommandé avec accusé de réception. Passé ce délai, le contrat est renouvelé automatiquement. <br /><br />
Si vous avez déjà réglé votre facture, veuillez ne pas tenir compte de ce message
<br /><br />
Pour de plus amples informations, veuillez nous contacter via notre support : http://www.e-imaginis.com/support
<br /><br />
Cordialement<br /><br />
l\'équipe commerciale imaginis
');

define('EMAIL_CUSTOMERS_J60','Cher(e) Client(e),<br /><br />Nous vous informons sur l\'évolution de vos produits chez Imaginis  et nous vous envoyons un rappel 60, 30, 15, 7  jours  avant leur expiration. <br /><br />
Pour renouveler vos services, il suffit de nous envoyer un chèque lors de la réception de votre facture qui vous sera envoyé au plus tard un mois avant l\expiration du contrat.
<br /><br />
Comme le stipule le contrat, toute résiliation doit se faire impérativement avant les 60 jours de la date d\'anniversaire de votre contrat. Passé ce délai, le contrat est renouvelé automatiquement et n\est plus résiliable.
<br /><br />
Pour de plus amples informations, veuillez nous contacter via notre support : http://www.e-imaginis.com/support
<br /><br />
Cordialement<br /><br />
l\'équipe commerciale imaginis
');


define('EMAIL_CUSTOMERS_J30','Cher(e) Client(e),<br /><br />
Nous vous informons sur l\'évolution de vos produits chez Imaginis et nous vous envoyons un rappel 30, 15, 7  jours  avant leur expiration. <br /><br />
Pour renouveler vos services, vous recevrez prochainement une facture et il suffitra de nous envoyer un chèque à l\'ordre de e-imaginis avant l\expiration de votre contrat.
<br /><br />
Comme le stipule le contrat, toute résiliation doit se faire impérativement avant les 60 jours de la date d\'anniversaire de votre contrat par envoi recommandé avec accusé de réception. Passé ce délai, le contrat est renouvelé automatiquement.
<br /><br />
Si vous avez déjà réglé votre facture, veuillez ne pas tenir compte de ce message
<br /><br />
Pour de plus amples informations, veuillez nous contacter via notre support : http://www.e-imaginis.com/support
<br /><br />
Cordialement<br /><br />
l\'équipe commerciale imaginis
');


define('EMAIL_CUSTOMERS_J15','Cher(e) Client(e),<br /><br />
Nous vous informons sur l\'évolution de vos produits chez Imaginis et nous vous envoyons un rappel 15, 7  jours  avant leur expiration.
<br /><br />
Pour renouveler vos services, il suffit de nous envoyer un chèque lors de la réception de votre facture.
<br /><br />
Comme le stipule le contrat, toute résiliation doit se faire impérativement avant les 60 jours de la date d\'anniversaire de votre contrat par envoi recommandé avec accusé de réception. Passé ce délai, le contrat est renouvelé automatiquement.
<br /><br />
Si vous avez déjà réglé votre facture, veuillez ne pas tenir compte de ce message
<br /><br />
Pour de plus amples informations, veuillez nous contacter via notre support : http://www.e-imaginis.com/support
<br /><br />
Cordialement<br /><br />
l\'équipe commerciale imaginis
');

define('EMAIL_CUSTOMERS_J7','Cher(e) Client(e),<br /><br />
Nous vous informons sur l\'évolution de vos produits chez Imaginis et nous vous envoyons un rappel 7  jours  avant leur expiration. <br /><br />
Pour renouveler vos services, il suffit de nous envoyer un chèque lors de la réception de votre facture avant la date d\expiration.
<br /><br />
Comme le stipule le contrat, toute résiliation doit se faire impérativement avant les 60 jours de la date d\'anniversaire de votre contrat par envoi recommandé avec accusé de réception. Passé ce délai, le contrat est renouvelé automatiquement.
<br /><br />
Ceci est le dernier mail que vous recevrez avant l\'expiration définitive de votre abonnement. La nom réception de votre chèque entraînera automatiquement la suspension de votre site.
Si vous avez déjà réglé votre facture, veuillez ne pas tenir compte de ce message
<br /><br />
Pour de plus amples informations, veuillez nous contacter via notre support : http://www.e-imaginis.com/support
<br /><br />
Cordialement<br /><br />
l\'équipe commerciale imaginis
');


define('ADMINISTRATOR_EMAIL',' Bonjour,<br /><br />
Ceci est le dernier rappel concernant le renouvellement du contrat concernant la boutique ' . STORE_OWNER . '.<br /><br />
Les rappels sont réalisés tous les 90, 60, 30, 15, 7 jours avant l\'expiration du contrat.<br /><br />
N\'oubliez pas d\'envoyer une facture de renouvellement !
<br /><br />
Si vous avez déjà envoyé la facture, veuillez ne pas tenir compte de ce message
<br /><br />
Pour de plus amples informations, veuillez nous contacter via notre support : http://www.e-imaginis.com/support
<br /><br />
Cordialement<br /><br />
l\'équipe commerciale imaginis
');

define('ADMINISTRATOR_EMAIL_SUBJECT','Information renouvellement contrat pour ');
define('CUSTOMERS_EMAIL_SUBJECT','Information concernant le renouvellement du contrat');
define('LINKEDIN_CERTIFICATE_CLICSHOPPING', 'Obtenez votre Certification ClicShopping !');
