<?php
/**
 * d_total_ca_by_year.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_TITLE', 'Total du CA par année (ventes réalisées) <br /> <br />');
define('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_DESCRIPTION', 'Graphique permettant de voir l\'évolution du chiffre d\'affaires des ventes réalisées par année en fonction du sous-total?');
define('MODULE_ADMIN_DASHBOARD_TOTAL_CA_BY_YEAR_CHART_LINK', 'Total CA par année');

