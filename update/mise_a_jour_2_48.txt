
--
-- Mise a jour version / Update
--

-- cosmetics
-- Update and improve  Odoo Web Service
-- Bulk For odoo : ClicShopping to Odoo for product and Customers
-- Added products options for odoo limited at one option / products and limited on option text caracters (by odoo)
-- added products image inside Odoo
-- added suppliers for Odoo
-- Tax for Odoo
-- change directory mobile detect to /ext
-- update datepicker for create account
-- script oocharts deprecated, deleted
-- update install process
-- bootstrap 3.3.1
-- products_attributes bug fix
-- update who's online
-- Updated alert conform bootlint
-- remove constant FILENAME in catalog
-- remove constant FILENAME in admin
-- replace french html caracters by normal
-- remove constant DATABASE in admin
-- remove constant DATABASE in catalog
-- added new sitemap blog and page manager
-- Invoice utF8 compatibility
-- Review seo metatag sql
-- Include meta tag in blog categories


====================================================================================================================================================

update `configuration` SET `configuration_value` = 'ClicShopping est une marque d&eacute;pos&eacute;e (INPI : 3810384) V2.48 -  All right Reserved - ' where configuration_key ='PROJECT_VERSION' LIMIT 1 ;

DELETE FROM `configuration` WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_ORDER_CATALOG';
ALTER TABLE `tax_rates` ADD `code_tax_odoo` VARCHAR(15) NULL ;
UPDATE `tax_rates` SET `code_tax_odoo` = '5.5' WHERE `tax_rates`.`tax_rates_id` = 2;
UPDATE `tax_rates` SET `code_tax_odoo` = '20.0' WHERE `tax_rates`.`tax_rates_id` = 1;


ALTER TABLE `blog_categories_description` ADD `blog_categories_head_title_tag` VARCHAR(50) NULL , ADD `blog_categories_head_desc_tag` VARCHAR(250) NULL , ADD `blog_categories_head_keywords_tag` LONGTEXT NOT NULL ;

ALTER TABLE `blog_categories_description` CHANGE `blog_categories_head_keywords_tag` `blog_categories_head_keywords_tag` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `blog_categories_description` CHANGE `blog_categories_head_title_tag` `blog_categories_head_title_tag` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `categories_description` CHANGE `categories_head_title_tag` `categories_head_title_tag` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `blog_content_description` CHANGE `blog_content_head_title_tag` `blog_content_head_title_tag` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `products_description` CHANGE `products_head_title_tag` `products_head_title_tag` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `products_description` CHANGE `products_head_keywords_tag` `products_head_keywords_tag` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `pages_manager_description` CHANGE `page_manager_head_keywords_tag` `page_manager_head_keywords_tag` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `categories_description` CHANGE `categories_head_keywords_tag` `categories_head_keywords_tag` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `blog_content_description` CHANGE `blog_content_head_keywords_tag` `blog_content_head_keywords_tag` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `blog_categories_description` CHANGE `blog_categories_head_keywords_tag` `blog_categories_head_keywords_tag` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE  `products_description` CHANGE  `products_head_tag`  `products_head_tag` VARCHAR( 70 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

===================================================================================================================================================
French
===================================================================================================================================================


UPDATE `configuration` SET `configuration_description` = 'Activation permettant d\'utiliser le webservice Odoo<br /><br/><Strong>Note :</strong><br /><br/> - Veuillez bien faire attention aux options ci-dessous.' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';
UPDATE `configuration` SET `configuration_title` = 'Souhaitez-vous permettre le changement d\'adresse de facturation par d&eacutefaut dans ClicShopping ?' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'Cela permet au client de changer son adresse de facturation par d&eacute;faut.<br /><br /><strong>Note :</strong> - Nous vous recommandons fortement de laisser cette option sur false quand vous utilisez l\'option facturation (ci-dessous). Accepter les changements d\'adresse, aura une <u>r&eacutepercution importante dans toute la gestion de vos factures dans Odoo</u> (bug Odoo)<br /><br /><i>(Valeur True = Oui - Valeur False = Non)</i>.' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'Activation permettant d\'utiliser le webservice Odoo<br /><br/><Strong>Note :</strong><br /><br/> - Veuillez bien faire attention aux options ci-dessous.<br /><br />- Veuillez installer les modules suivants dans Odoo :<br />* Comptabilit&eacute; <br />* Gestion des ventes.<br /><br /> Pour de plus amples informations, veuillez consulter notre guide Odoo sur ClicShopping.org ' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';
UPDATE `configuration` SET `configuration_description` =  'Veuillez indiquer le nom de votre entreprise cr&eacute;e dans l\'administration Odoo<br />- Veuillez respecter correctement le nom de l\'entreprise inscrite dans Odoo' WHERE `configuration`.`configuration_key` = 'ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE';



INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Veuillez indiquer len num&eacutero comptable des Rabais/Remises/Ristournes de Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_DISCOUNT', '', 'Odoo utilise un compte de comptabilit&eacute; pour enregistrer les RRR de marchandise.<br /><br /><strong>Note :</strong><br /><br />- En France, c\'est le compte 709700 (comptabilit&eacute; francaise)<br />- Veuillez respecter correctement le num&eacute;ro de compte de Odoo', '44', '16', now());
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Souhaitez-vous activer le web service pour enregistrer les cr&eacute;ations de commande du catalogue vers Odoo ?', 'ODOO_ACTIVATE_ORDER_CATALOG', 'none', 'Le type de facturation enregistr&eacute; doit etre s&eacute;lectionn&eacute; en fonction de votre process.<br /><br /><strong>Note :</strong><br /><br />- None : Aucun enregistrement dans Odoo ne sera trait&eacute; par ClicShopping.<br/><br />- Order : Enregistrer la commande du site comme commande dans Odoo. Vous devrez traiter et compl&eacute;ter par la suite le traitement dans Odoo et ClicShopping.<br /><br />- Invoice : La commande effectu&eacute;e par le client est directement enregistr&eacute;e comme facture.<br /><br />- Conseil : N\'oubliez pas d\'adapter votre statut de commande par d&eacute;faut en fonction de vos choix.<br /><br /><u>Veuillez prendre note que les taxes ne sont pas enregistr&eacute;es dans Odoo lors du processus de traitement</u>', '44', '10', 'osc_cfg_select_option(array(\'none\', \'order\', \'invoice\'), ', now());
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES('Souhaitez-vous exporter vos produits dans Odoo', 'ODOO_EXPORT_PRODUCTS',  'false', 'Exporte tous les produits de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de produits, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes : <strong><br /><br />- 1- Pour commencer le processus d\'export, veuillez <strong>s&eacute;lectionner l\'option Export</strong>, puis valider et r&eacute;-&eacute;diter l\'option. Cela d&eacute;clenchera le processus d\'exportation <br />- 2 - D&eacute;s que le processus d\'export est termin&eacute;, veuillez <strong>s&eacute;lectionner l\'option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d\'exportation ult&eacute;rieurement.<br /><br />- En cas d\'erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des produits - Valeur False = Non)</i>', 44, 80,  now(),  now(), 'osc_bulk_product', 'osc_cfg_select_option(array(\'export\', \'false\'), ');
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES('Souhaitez-vous exporter vos clients dans Odoo', 'ODOO_EXPORT_CUSTOMERS',  'false', 'Exporte tous les clients de ClicShopping dans Odoo.<br /><br /><strong>Note :</strong><br /><br />- En fonction du nombre de clients, le temps peut etre plus ou moins long. Veuillez ne pas interrompre le processus et <strong>suivre les instructions suivantes :</strong><br /><br />- 1- Pour commencer le processus d\'export, veuillez <strong>s&eacute;lectionner l\'option Export</strong>, puis valider et r&eacute;-&eacute;diter l\'option. Cela d&eacute;clenchera le processus d\'exportation <br />- 2 - D&eacute;s que le processus d\'export est termin&eacute;, veuillez <strong>s&eacute;lectionner l\'option sur False</strong>. Cela &eacute;vitera de d&eacute;clencher de nouveau le processus d\'exportation ult&eacute;rieurement.<br /><br />- En cas d\'erreur ou de fausse manipulation de votre part, nous d&eacute;clinons toute responsabilit&eacute;<br /><br /><i>(Valeur Export = Exportation des clients - Valeur False = Non)</i>', 44, 81,  now(),  now(), 'osc_bulk_customer', 'osc_cfg_select_option(array(\'export\', \'false\'), ');
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Souhaitez-vous activer la cr&eacute;ation  ou modification de Fournisseurs dans Odoo ?', 'ODOO_ACTIVATE_SUPPLIERS_ADMIN', 'false', 'Enregistre toutes les modifications ou cr&eacute;ation de fournisseur dans l\'administration vers Odoo', '44', '8', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now());
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Souhaitez-vous activer le webservice pour cr&eacute;er ou mettre &agrave; jour les clients &agrave partir de l\'administration vers Odoo?', 'ODOO_ACTIVATE_CUSTOMERS_ADMIN', 'false', 'Enregistre toutes les modifications de la fiche client de ClicShopping dans Odoo', '44', '9', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now());




===================================================================================================================================================
English
===================================================================================================================================================

UPDATE `configuration` SET `configuration_description` = 'Activation allowing to use the Odoo webservice<br /><br/><Strong>Note :</strong><br /><br/> - Please, becarefull at all options below.' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';
UPDATE `configuration` SET `configuration_title` = 'Do you want allow a modification on defaul in ClicShopping ?' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'This allows customer to change his default billing address.<br /><br /><strong>Note :</strong><br /><br />- He is strongly recommended to keep this feature option on <strong>false</strong> when you use invoice option (below). Accept address changes, will an <u>important repercussion in the whole management of your invoice Odoo</u> (bug Odoo)<br />.' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_CHECKOUT_ADDRESS_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'Activation allowing to use the Odoo webservice<br /><br/><Strong>Note :</strong><br /><br/> - Please becarefull with theses options below.<br /><br />- Please, install the following modules in Odoo :<br />* Accounting <br />* Sales management.<br /><br /> For more informations, please read our Odoo guide on ClicShopping.org ' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_WEB_SERVICE';
UPDATE `tax_rates` SET `code_tax_odoo` = '5.5' WHERE `tax_rates`.`tax_rates_id` = 2;
UPDATE `tax_rates` SET `code_tax_odoo` = '20.0' WHERE `tax_rates`.`tax_rates_id` = 1;
UPDATE `configuration` SET `configuration_description` = 'Save all orders created via the catalog in Odoo administration<br /><br /> <strong>Note :</strong><br /><br /> - Do not forget to check the tax codes in the section ClicShopping Location and taxes / tax rate' WHERE `configuration`.`configuration_key` = 'ODOO_ACTIVATE_ORDER_CATALOG';
UPDATE `configuration` SET `configuration_description` = 'Enregistre la mise &agrave; jour de la commande de l\'administration vers Odoo<strong>Note :</strong><br /><br /> - - Do not forget to check the tax codes in the section ClicShopping Location and taxes / tax rate' WHERE `configuration`.`configuration_key` = 'ODOO_WEB_SERVICE_ORDERS_ADMIN';

INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Please insert the discount account number of Odoo', 'ODOO_WEB_SERVICE_ACCOUNT_DISCOUNT', '', 'Odoo use an account to save the invoice inside the accounting.<br /><br /><strong>Note :</strong><br /><br />- In France, it\'s the 709700 (french accounting)<br />- Please, respect the Odoo account number', '44', '16', now());
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Do you want activate the web service to save the catalog during the order creation to Odoo ?', 'ODOO_ACTIVATE_ORDER_CATALOG', 'none', 'Billing type must be selected registered according to your process.<br /><br /><strong>Note :</strong><br /><br />- None : No registration will be processed in Odoo by ClicShopping.<br/><br />- Order : Save the ClicShopping order like order in Odoo. You will be treated and fill afterwards  the treatment in Odoo and ClicShopping.<br /><br />- Invoice : The order placed by the customer is directly saved as invoice Odoo.<br /><br />- Advise : Do not forget adjust your order status by default according to your choice.<br />br /><u>Please note that taxes are not saved in Odoo during the treatment process</u>', '44', '10', 'osc_cfg_select_option(array(\'none\', \'order\', \'invoice\'), ', now());
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Do you want activate the creation or modification for the suppliers to Odoo ?', 'ODOO_ACTIVATE_SUPPLIERS_ADMIN', 'false', 'Save all creations or modifications for the suppliers to Odoo ?', '44', '8', 'osc_cfg_select_option(array(\'true\', \'false\'), ', now());
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES('Do you want export your products in Odoo', 'ODOO_EXPORT_PRODUCTS',  'false', 'Export all ClicShopping products inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of products, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong> then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed,  please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = products exportation - Valeur False = No)</i>', 44, 80,  now(),  now(), 'osc_bulk_product', 'osc_cfg_select_option(array(\'export\', \'false\'), ');
INSERT INTO configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES('Do you want export your customers in Odoo', 'ODOO_EXPORT_CUSTOMERS',  'false', 'Export all ClicShopping customers inside Odoo.<br /><br /><strong>Note :</strong><br /><br />- Depending number of customers, the time can be longer or shorter. Please do not interrupt the process and <strong>follow the instructions carefully :</strong><br /><br />- 1- To start the export process , please <strong>select the Export option</strong>, then validate and re-edit option. This will trigger the export process <br />- 2 - As soon as the export process is completed, please <strong>select the option on False</strong>. This will avoid triggering the export process again later.<br /><br />- In case of error or mishandling by you, we accept no liability.<br /><br /><i>(Value Export = customers exportation - Valeur False = No)</i>', 44, 81,  now(),  now(), 'osc_bulk_customer', 'osc_cfg_select_option(array(\'export\', \'false\'), ');

