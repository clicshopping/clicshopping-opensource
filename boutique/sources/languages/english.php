<?php

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'en_US.ISO_8859-1');
@setlocale(LC_ALL, array('en_US.UTF-8', 'en_US.UTF8', 'enu_usa'));

define('DATE_FORMAT_SHORT', '%m/%d/%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
define('DATE_FORMAT', 'm/d/Y'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('JQUERY_DATEPICKER_FORMAT', 'mm/dd/yy');

////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function osc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 3, 2) . substr($date, 0, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 0, 2) . substr($date, 3, 2);
  }
}

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'USD');

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="ltr" lang="en"');

// language site
define('HTML_LANG', 'en');

// charset for web pages and emails
define('CHARSET', 'UTF-8');


// Modification reference & design facture 
// Y = Annee / m = mois / d = jours  (Voir aussi fonction getDateReferenceShort() dans general.php)
define('DATE_FORMAT_REFERENCE', 'Ym'); 

// page title
define('TITLE', STORE_NAME);
define('ACCESS_CATALOG', 'Access to '.STORE_NAME.' shop');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Create an Account');
define('HEADER_TITLE_MY_ACCOUNT', 'My Account');
define('HEADER_TITLE_CART_CONTENTS', 'Cart Contents');
define('HEADER_TITLE_CHECKOUT', 'Checkout');
define('HEADER_TITLE_TOP', 'Index');
define('HEADER_TITLE_CATALOG', ''. STORE_NAME .'');
define('HEADER_TITLE_LOGOFF', 'Log Off');
define('HEADER_TITLE_LOGIN', 'Log In');
define('HEADER_TITLE_NEWSLETTER','Newsletter');
define('HEADER_TITLE_INDEX','Index');
define('HEADER_TITLE_SPECIALS','Specials');
define('HEADER_TITLE_PRODUCTS_NEW','News');
define('HEADER_TITLE_PRODUCTS_HEART','Favorites');
define('HEADER_TITLE_CONTACT_US','Contact us');

// footer text in includes/footer.php
define('FOOTER_TEXT_REQUESTS_SINCE', 'requests since');

//Displey an attribut before the product model if customer id is different 0
define('PRODUCTS_GROUP_QUANTITY_UNIT_TITLE','FRS-');

// text for gender
define('MALE', 'Male');
define('FEMALE', 'Female');
define('MALE_ADDRESS', 'Mr.');
define('FEMALE_ADDRESS', 'Ms.');

// text for date of birth example
define('DOB_FORMAT_STRING', 'mm/dd/yyyy');

// categories box text in includes/boxes/categories.php
define('BOX_HEADING_CATEGORIES', 'Categories');

// manufacturers box text in includes/boxes/manufacturers.php
define('BOX_HEADING_MANUFACTURERS', 'Manufacturers');

// whats_new box text in includes/boxes/whats_new.php
define('BOX_HEADING_WHATS_NEW', 'New Products');

// quick_find box text in includes/boxes/quick_find.php
define('BOX_HEADING_SEARCH', 'Quick Find');
define('BOX_SEARCH_TEXT', 'Use keywords to find the product you are looking for.');
define('BOX_SEARCH_ADVANCED_SEARCH', 'Advanced Search');

// specials box text in includes/boxes/specials.php
define('BOX_HEADING_SPECIALS', 'Specials offers');

// reviews box text in includes/boxes/reviews.php
define('BOX_HEADING_REVIEWS', 'Reviews');
define('BOX_REVIEWS_WRITE_REVIEW', 'Write a review on this product!');
define('BOX_REVIEWS_NO_REVIEWS', 'There are currently no product reviews');
define('BOX_REVIEWS_TEXT_OF_5_STARS', '%s of 5 Stars!');

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_HEADING_SHOPPING_CART', 'Shopping Cart');
define('BOX_SHOPPING_CART_EMPTY', '0 items');

// order_history box text in includes/boxes/order_history.php
define('BOX_HEADING_CUSTOMER_ORDERS', 'Order History');

// best_sellers box text in includes/boxes/best_sellers.php
define('BOX_HEADING_BESTSELLERS', 'Bestsellers');
define('BOX_HEADING_BESTSELLERS_IN', 'Bestsellers in<br />&nbsp;&nbsp;');

// notifications box text in includes/boxes/products_notifications.php
define('BOX_HEADING_NOTIFICATIONS', 'Notifications');
define('BOX_NOTIFICATIONS_NOTIFY', 'Notify me of updates to <strong>%s</strong>');
define('BOX_NOTIFICATIONS_NOTIFY_REMOVE', 'Do not notify me of updates to <strong>%s</strong>');

// manufacturer box text
define('BOX_HEADING_MANUFACTURER_INFO', 'Manufacturer Info');
define('BOX_MANUFACTURER_INFO_HOMEPAGE', '%s Homepage');
define('BOX_MANUFACTURER_INFO_OTHER_PRODUCTS', 'Other products');

// languages box text in includes/boxes/languages.php
define('BOX_HEADING_LANGUAGES', 'Languages');

// currencies box text in includes/boxes/currencies.php
define('BOX_HEADING_CURRENCIES', 'Currencies');

// information box text in includes/boxes/information.php
define('BOX_HEADING_INFORMATION', 'Information');
define('BOX_INFORMATION_PRIVACY', 'Privacy Notice');
define('BOX_INFORMATION_CONDITIONS', 'Terms &amp; Conditions');
define('BOX_INFORMATION_SHIPPING', 'Shipping &amp; Returns');
define('BOX_INFORMATION_CONTACT', 'Contact Us');

// social bookmarks box in includes/boxes/product_social_bookmarks.php
define('BOX_HEADING_SOCIAL_BOOKMARKS', 'Share this products with your friends');

// product_heart
define('BOX_HEADING_PRODUCTS_HEART', 'Favorites');

//Sitemap
define('BOX_HEADING_SITEMAP', 'Sitemap');

// checkout procedure text
define('CHECKOUT_BAR_CART_CONTENTS', 'Basket content');
define('CHECKOUT_BAR_DELIVERY', 'Delivery Information');
define('CHECKOUT_BAR_DELIVERY_ADDRESS', 'Delivery Address');
define('CHECKOUT_BAR_PAYMENT_METHOD', 'Payment method');
define('CHECKOUT_BAR_PAYMENT', 'Payment Information');
define('CHECKOUT_BAR_CONFIRMATION', 'Confirmation');
define('CHECKOUT_BAR_FINISHED', 'Finished!');

// pull down default text
define('PULL_DOWN_DEFAULT', 'Please Select');
define('PLEASE_SELECT', 'Select');
define('TYPE_BELOW', 'Type Below');

// Secured payment
define('BOX_HEADING_PAYMENT', 'Secured payment by :');
define('BOX_HEADING_PAYMENT_SPPLUS', 'This site has a protection and a SSL certification connection:');
define('SSLBOX_CONTENT','128 Bits connexion SSL activated and securised');

// politique de confidentialite
define('ERROR_CONDITIONS_NOT_ACCEPTED', 'Please confirm the terms and conditions bound to this order by ticking the box below.');

// javascript messages
define('JS_ERROR', 'Errors have occured during the process of your form.\n\nPlease make the following corrections:\n\n');

define('JS_REVIEW_TEXT', '* The \'Review Text\' must have at least ' . REVIEW_TEXT_MIN_LENGTH . ' characters.\n');
define('JS_REVIEW_RATING', '* You must rate the product for your review.\n');

define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* Please select a payment method for your order.\n');

define('JS_ERROR_SUBMITTED', 'This form has already been submitted. Please press Ok and wait for this process to be completed.');

define('ERROR_NO_PAYMENT_MODULE_SELECTED', 'Please select a payment method for your order.');

define('CATEGORY_COMPANY', 'Company Details');
define('CATEGORY_PERSONAL', 'Your Personal Details');
define('CATEGORY_ADDRESS', 'Your Address');
define('CATEGORY_CONTACT', 'Your Contact Information');
define('CATEGORY_OPTIONS', 'Options');
define('CATEGORY_PASSWORD', 'Your Password');

define('ENTRY_COMPANY', 'Company Name:');
define('ENTRY_COMPANY_ERROR', 'Your Company Name must contain a minimum of ' . ENTRY_COMPANY_MIN_LENGTH . ' characters.');
define('ENTRY_COMPANY_ERROR_PRO', 'Your Company Name must contain a minimum of ' . ENTRY_COMPANY_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_COMPANY_TEXT', '*');
define('ENTRY_COMPANY_TEXT_PRO', '*');
define('ENTRY_SIRET', 'N&deg; of registration :');
define('ENTRY_SIRET_ERROR', 'The number of registration of your company must contain a minimum of ' . ENTRY_SIRET_MIN_LENGTH .' characters.');
define('ENTRY_SIRET_TEXT', '*');
define('ENTRY_SIRET_EXEMPLE', '(ex : RCS Code for the French company)');
define('ENTRY_TVA_INTRACOM', 'N&deg; of VAT intracom :');
define('ENTRY_TVA_INTRACOM_ERROR', 'The number of VAT Intracom of your company must contain a minimum of ' . ENTRY_TVA_INTRACOM_MIN_LENGTH .' characters.');
define('ENTRY_TVA_INTRACOM_TEXT', '* Select the country in first.');
define('ENTRY_CODE_APE', 'Nomenclatur Number :');
define('ENTRY_CODE_APE_ERROR', 'The nomenclature number your company must contain a minimum of ' . ENTRY_CODE_APE_MIN_LENGTH .' characters.');
define('ENTRY_CODE_APE_TEXT', '*');
define('ENTRY_CODE_APE_EXEMPLE', '(ex : APE Code for the French company)');
define('ENTRY_TAXE_PROVINCIALE','Provincial Tax number : ');
define('ENTRY_CODE_TAXE_PROVINCIALE_ERROR', 'Le provincial tax code of your company must contain a minimum of  ' . ENTRY_CODE_TAXE_PROVINCIALE_MIN_LENGTH .' characters.');
define('ENTRY_CODE_TAXE_PROVINCIALE_TEXT', '*');
define('ENTRY_TAXE_FEDERALE','Federal Tax : ');
define('ENTRY_CODE_TAXE_FEDERALE_ERROR', 'the federal tax code of your company must contain a minimum of  ' . ENTRY_CODE_TAXE_FEDERALE_MIN_LENGTH .' characters.');
define('ENTRY_CODE_TAXE_FEDERALE_TEXT', '*');
define('ENTRY_GENDER', 'Gender:');
define('ENTRY_GENDER_ERROR', 'Please select your Gender.');
define('ENTRY_GENDER_ERROR_PRO', 'Please select your Gender.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME', 'First Name:');
define('ENTRY_FIRST_NAME_ERROR', 'Your First Name must contain a minimum of ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters.');
define('ENTRY_FIRST_NAME_ERROR_PRO', 'Your First Name must contain a minimum of ' . ENTRY_FIRST_NAME_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Last Name:');
define('ENTRY_LAST_NAME_ERROR', 'Your Last Name must contain a minimum of ' . ENTRY_LAST_NAME_MIN_LENGTH . ' characters.');
define('ENTRY_LAST_NAME_ERROR_PRO', 'Your Last Name must contain a minimum of ' . ENTRY_LAST_NAME_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Date of Birth:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Your Date of Birth must be in this format: MM/DD/YYYY (eg 05/21/1970)');
define('ENTRY_DATE_OF_BIRTH_ERROR_PRO', 'Your Date of Birth must be in this format: MM/DD/YYYY (eg 05/21/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (eg. 05/21/1970)');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail Address:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Your email address is not correct.');
define('ENTRY_EMAIL_ADDRESS_CONFIRM', 'Confirm E-Mail:');
define('ENTRY_EMAIL_ADDRESS_CONFIRM_NOT_MATCHING', 'The Confirmation Email must match your Email Address.');
define('ENTRY_EMAIL_ADDRESS_ERROR_PRO', 'Your E-Mail Address must contain a minimum of ' . ENTRY_EMAIL_ADDRESS_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Your E-Mail Address does not appear to be valid - please make any necessary corrections.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR_NUMBER', 'You made an error in the calculation formula applied at the bottom of the form.<br />Please make any necessary corrections.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR_PRO', 'Your E-Mail Address does not appear to be valid - please make any necessary corrections.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Your E-Mail Address already exists in our records - please log in with the e-mail address or create an account with a different address.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS_PRO', 'Your E-Mail Address already exists in our records - please log in with the e-mail address or create an account with a different address.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION', 'Email of confirmation :');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION_TEXT', '* Confirm your address email.');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION_CHECK_ERROR', 'You did not seize an identical address email at the time of the confirmation.');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION_CHECK_ERROR_PRO', 'You did not seize an identical address email at the time of the confirmation.');
define('ENTRY_STREET_ADDRESS', 'Street Address:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Your Street Address must contain a minimum of ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' characters.');
define('ENTRY_STREET_ADDRESS_ERROR_PRO', 'Your Street Address must contain a minimum of ' . ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB', 'Suburb:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Post Code:');
define('ENTRY_POST_CODE_ERROR', 'Your Post Code must contain a minimum of ' . ENTRY_POSTCODE_MIN_LENGTH . ' characters.');
define('ENTRY_POST_CODE_ERROR_PRO', 'Your Post Code must contain a minimum of ' . ENTRY_POSTCODE_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'City:');
define('ENTRY_CITY_ERROR', 'Your City must contain a minimum of ' . ENTRY_CITY_MIN_LENGTH . ' characters.');
define('ENTRY_CITY_ERROR_PRO', 'Your City must contain a minimum of ' . ENTRY_CITY_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'State/Province:');
define('ENTRY_STATE_ERROR', 'Your State must contain a minimum of ' . ENTRY_STATE_MIN_LENGTH . ' characters.');
define('ENTRY_STATE_ERROR_SELECT', 'Please select a state from the States pull down menu.');
define('ENTRY_STATE_ERROR_PRO', 'Your State must contain a minimum of ' . ENTRY_STATE_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_STATE_ERROR_SELECT_PRO', 'Please select a state from the States pull down menu.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Country:');
define('ENTRY_COUNTRY_ERROR', 'You must select a country from the Countries pull down menu.');
define('ENTRY_COUNTRY_ERROR_PRO', 'You must select a country from the Countries pull down menu.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Telephone Number:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Your Telephone Number must contain a minimum of ' . ENTRY_TELEPHONE_MIN_LENGTH . ' characters.');
define('ENTRY_TELEPHONE_NUMBER_ERROR_PRO', 'Your Telephone Number must contain a minimum of ' . ENTRY_TELEPHONE_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_CELLULAR_PHONE_NUMBER', 'Cellular phone Number:');
define('ENTRY_CELLULAR_PHONE_NUMBER_ERROR', 'Your Cellular phone Number must contain a minimum of ' . ENTRY_CELLULAR_PHONE_MIN_LENGTH . ' characters.');
define('ENTRY_CELLULAR_PHONE_NUMBER_ERROR_PRO', 'Your Cellular phone Number must contain a minimum of ' . ENTRY_CELLULAR_PHONE_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_CELLULAR_PHONE_NUMBER_TEXT', '');
define('ENTRY_FAX_NUMBER', 'Fax Number:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_MESSAGE_TEXT','*');
define('ENTRY_NEWSLETTER', 'Newsletter:');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Subscribed');
define('ENTRY_NEWSLETTER_NO', 'Unsubscribed');
define('ENTRY_NEWSLETTER_ERROR', '');
define('ENTRY_PASSWORD', 'Password:');
define('ENTRY_PASSWORD_ERROR', 'Your Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'The Password Confirmation must match your Password.');
define('ENTRY_PASSWORD_ERROR_PRO', 'Your Password must contain a minimum of ' . ENTRY_PASSWORD_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING_PRO', 'The Password Confirmation must match your Password.');
define('ENTRY_PASSWORD_TEXT', '*');
define('ENTRY_PASSWORD_CONFIRMATION', 'Password Confirmation:');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '* Confirm your password.');
define('ENTRY_PASSWORD_CURRENT', 'Current Password:');
define('ENTRY_PASSWORD_CURRENT_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Your Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_NEW', 'New Password:');
define('ENTRY_PASSWORD_NEW_TEXT', '*');
define('ENTRY_PASSWORD_NEW_ERROR', 'Your new Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'The Password Confirmation must match your new Password.');
define('ENTRY_PASSWORD_NEW_ERROR_PRO', 'Your new Password must contain a minimum of ' . ENTRY_PASSWORD_PRO_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING_PRO', 'The Password Confirmation must match your new Password.');
define('ENTRY_SPAM_TEXT','*');

define('PASSWORD_HIDDEN', '--HIDDEN--');
define('FORM_REQUIRED_INFORMATION', '* Required information');


// constants for use in osc_prev_next_display function
define('TEXT_RESULT_PAGE', 'Result Pages:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> products)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> orders)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> reviews)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> new products)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> specials)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_HEART', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> favorites)');
define('TEMPLATE_DOES_NOT_EXIST', '<br /><br /><br />This template does not exist ! Please verify your settings or download the files on the marketplace .<br /><br /><br />');

define('PREVNEXT_TITLE_FIRST_PAGE', 'First Page');
define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Previous Page');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Next Page');
define('PREVNEXT_TITLE_LAST_PAGE', 'Last Page');
define('PREVNEXT_TITLE_PAGE_NO', 'Page %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Previous Set of %d Pages');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Next Set of %d Pages');
define('PREVNEXT_BUTTON_FIRST', '&lt;&lt;FIRST');
define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;Prev]');
define('PREVNEXT_BUTTON_NEXT', '[Next&nbsp;&gt;&gt;]');
define('PREVNEXT_BUTTON_LAST', 'LAST&gt;&gt;');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Add Address');
define('IMAGE_BUTTON_ADDRESS_BOOK', 'Address Book');
define('IMAGE_BUTTON_BACK', 'Back');
define('IMAGE_BUTTON_BUY_NOW', 'Buy Now');
define('IMAGE_BUTTON_CHANGE_ADDRESS', 'Change Address');
define('IMAGE_BUTTON_CHECKOUT', 'Checkout');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Confirm the order with payment');
define('IMAGE_BUTTON_PAY_TOTAL_NOW', 'Confirm the order with payment (%s)');
define('IMAGE_BUTTON_PAY_TOTAL_PROCESSING', 'Processing, please wait ..');
define('IMAGE_BUTTON_CONTINUE', 'Continue');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continue Shopping');
define('IMAGE_BUTTON_DELETE', 'Delete');
define('IMAGE_BUTTON_CONFIRM_DELETE', 'Confirm the deletion');
define('IMAGE_BUTTON_EDIT_ACCOUNT', 'Edit Account');
define('IMAGE_BUTTON_HISTORY', 'Order History');
define('IMAGE_BUTTON_LOGIN', 'Sign In');
define('IMAGE_BUTTON_IN_CART', 'Add to Cart');
define('IMAGE_BUTTON_NOTIFICATIONS', 'Notifications');
define('IMAGE_BUTTON_QUICK_FIND', 'Quick Find');
define('IMAGE_BUTTON_REMOVE_NOTIFICATIONS', 'Remove Notifications');
define('IMAGE_BUTTON_CUSTOMERS', 'New customer account');
define('IMAGE_BUTTON_RETAILERS','New retailer account');
define('IMAGE_BUTTON_REVIEWS', 'Reviews');
define('IMAGE_BUTTON_SEARCH', 'Search');
define('IMAGE_BUTTON_SHIPPING_OPTIONS', 'Shipping Options');
define('IMAGE_BUTTON_STOCK_MARK_PRODUCT_EXHAUSTED','Product exhausted');
define('IMAGE_BUTTON_TELL_A_FRIEND', 'Tell a Friend');
define('IMAGE_BUTTON_UPDATE', 'Update');
define('IMAGE_BUTTON_UPDATE_CART', 'Update Cart');
define('IMAGE_BUTTON_WRITE_REVIEW', 'Write Review');

define('SMALL_IMAGE_BUTTON_DELETE', 'Delete');
define('SMALL_IMAGE_BUTTON_EDIT', 'Edit');
define('SMALL_IMAGE_BUTTON_VIEW', 'View');
define('SMALL_IMAGE_BUTTON_DETAILS', 'Details ');

define('IMAGE_BUTTON_DISPLAY_STOCK_GOOD','In stock');
define('IMAGE_BUTTON_DISPLAY_STOCK_ALERT','Soon out of stock');
define('IMAGE_BUTTON_DISPLAY_STOCK_OUT','Product out of stock');


define('ICON_ARROW_RIGHT', 'more');
define('ICON_CART', 'In Cart');
define('ICON_ERROR', 'Error');
define('ICON_SUCCESS', 'Success');
define('ICON_WARNING', 'Warning');

define('TEXT_GREETING_PERSONAL', 'Welcome <span class="greetUser">%s!</span> We have some <a href="%s"><u>the products new</u></a> available ?');
define('TEXT_GREETING_GUEST', 'Welcome <span class="greetUser">Mrs, Mr</span>. Feel free to <a href="%s"><u>log yourself in</u></a> ? browse some of ourr <a href="%s"><u>new products</u></a> ?');


define('TEXT_SORT_PRODUCTS', 'Sort products ');
define('TEXT_DESCENDINGLY', 'descendingly');
define('TEXT_ASCENDINGLY', 'ascendingly');
define('TEXT_BY', ' by ');

define('TEXT_STOCK','Stock level : ');
define('TEXT_FLASH_DISCOUNT','FLASH DISCOUNT : ');
define('DAYS', 'D ');
define('HOURS', 'H ');
define('MINUTES', 'Mn ');
define('SECONDES', 'sec ');

define('TEXT_SORT_BY', 'Display by ');

define('TEXT_REVIEW_BY', 'by %s');
define('TEXT_REVIEW_WORD_COUNT', '%s words');
define('TEXT_REVIEW_RATING', 'Rating: %s [%s]');
define('TEXT_REVIEW_DATE_ADDED', 'Date Added: %s');
define('TEXT_NO_REVIEWS', 'There are currently no product reviews.');
define('TEXT_ICON_NEW_PRODUCT','News');
define('TEXT_NO_NEW_PRODUCTS', 'There are currently no products.');
define('TEXT_PRICE','');
define('TEXT_PRICE_PRODUCTS_INFO','');

define('TEXT_UNKNOWN_TAX_RATE', 'Unknown tax rate');

define('TEXT_REQUIRED', '<span class="errorText">Required</span>');
define('ERROR_TEP_MAIL', '<strong><small>TEP ERROR:</small> Cannot send the email through the specified SMTP server. Please check your php.ini setting and correct the SMTP server if necessary.</strong>');

define('TEXT_CCVAL_ERROR_INVALID_DATE', 'The expiry date entered for the credit card is invalid.<br />Please check the date and try again.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'The credit card number entered is invalid.<br />Please check the number and try again.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'The first four digits of the number entered are: %s<br />If that number is correct, we do not accept that type of credit card.<br />If it is wrong, please try again.');


define('CATEGORY_ADDRESS_PRO', 'Address company');
define('CATEGORY_TELEPHONE_PRO', 'Telephone contact');
define('CATEGORY_PERSONAL_PRO', 'Commercial contact');

define('PRICES_LOGGED_IN_TEXT', 'Please log in for prices.');
define('PRICES_LOGGED_IN_SUBMIT','Sorry you must be logged in for prices.');
define('PRICES_LOGGED_IN_BUY_TEXT', 'Please log in for "Buy-now');
define('TAX_INCLUDED', 'TAX inc.');
define('TAX_EXCLUDED', 'TAX exc.');

define('NO_ORDERS_GROUP', 'No product avalaible');
define('NO_ORDERS_PUBLIC', '');

// Antispam
define('EMAIL_CONFIRMATION_NUMBER','three');

// Discount Coupon
define('ENTRY_DISCOUNT_COUPON_ERROR', 'The coupon code you have entered is not valid.');
define('ENTRY_DISCOUNT_COUPON_AVAILABLE_ERROR', 'The coupon code you have entered is no longer valid.');
define('ENTRY_DISCOUNT_COUPON_USE_ERROR', 'Our records show that you have used this coupon %s time(s).  You may not use this code more than %s time(s).');
define('ENTRY_DISCOUNT_COUPON_MIN_PRICE_ERROR', 'The minimum order total for this coupon is %s');
define('ENTRY_DISCOUNT_COUPON_MIN_QUANTITY_ERROR', 'The minimum number of products required for this coupon is %s');
define('ENTRY_DISCOUNT_COUPON_EXCLUSION_ERROR', 'Some or all of the products in your cart are excluded.' );
define('ENTRY_DISCOUNT_COUPON', 'Coupon Code:');
define('ENTRY_DISCOUNT_COUPON_SHIPPING_CALC_ERROR', 'Your calculated shipping charges have changed.');

define('ENTRY_MIN_QTY_ORDER_PRODUCT','');
define('MIN_QTY_ORDER_PRODUCT','Minimum quantity for a purchase :  ');
define('TEXT_PRODUCTS_QUANTITY_TYPE','Purchase type : ');
define('TEXT_CUSTOMER_QUANTITY','Quantity ');
define('TEXT_PRODUCTS_FREE','Free');

define('TABLE_HEADING_DATE_ADDED','Date');
define('TABLE_HEADING_PRICE','Price');
define('TABLE_HEADING_MODEL','Model');
define('TABLE_HEADING_QUANTITY','Quantity');
define('TABLE_HEADING_WEIGHT','Weight');
define('TABLE_HEADING_DATE','Date');

define('TEXT_INSTALL','
   <div style="text-align:center; padding-top: 50px;"><a href="http://www.clicshopping.org" target="_blank"><img src="images/logo_clicshopping_1.png" border="0" height="105" width="284" alt="Market Place"></a><br />
   <div style="padding-top: 120px;">Congratulation, you have make with success the install process, please  read the informations below</div>
   <div style="padding-top: 20px;">Please, click on ClicShopping logo to connect you to the Market Place to begin and select your module that you want install</div>
   <div>ClicShopping is built on modular architecture, to use, you must install the modules. For more information, please, go to MarketPlace</div>
   <div><br /> - More about install module, please click on this link : <a href ="http://www.clicshopping.org/marketplace/mes-premiers-pas-bl-9.html" target="_blank">My first step</a></div>
   <div> - To download the Starter Pack : please click on : <a href="http://www.clicshopping.org/marketplace/sources/public/clicshopping/" target="_blank">Pack Starter</a> and install via the installation system of ClicShopping and activate the modules</div>
   <div> - Form more informations, please go to see our : <a href="http://www.clicshopping.org/marketplace/blog.php" target="_blank">blog</a></div>
');
?>
