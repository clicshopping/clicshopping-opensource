<?php
/**
 * order.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  class order {
    var $info, $totals, $products, $customer, $delivery, $content_type, $coupon;

    function order($order_id = '') {
      $this->info = array();
      $this->totals = array();
      $this->products = array();
      $this->customer = array();
      $this->delivery = array();

      if (osc_not_null($order_id)) {
        $this->query($order_id);
      } else {
        $this->cart();
      }
    }

    function query($order_id) {
      global $OSCOM_PDO;
      $order_total = $shipping_title = '';
      
      $Qorder = $OSCOM_PDO->prepare('select *
                                     from :table_orders
                                     where orders_id = :orders_id
                                    ');
      $Qorder->bindInt(':orders_id', (int)$order_id);
      $Qorder->execute();

// orders total
      $Qtotals = $OSCOM_PDO->prepare('select title,
                                             text
                                      from :table_orders_total
                                      where orders_id = :orders_id
                                      order by sort_order
                                    ');
      $Qtotals->bindInt(':orders_id', (int)$order_id);
      $Qtotals->execute();

      while ($Qtotals->fetch()) {
        $this->totals[] = array('title' => $Qtotals->value('title'),
                                'text' => $Qtotals->value('text'));

        if ($Qtotals->value('class') == 'ot_total') {
          $order_total = strip_tags($Qtotals->value('text'));
        } elseif ($Qtotals->value('class') == 'ot_shipping') {
          $shipping_title = strip_tags($Qtotals->value('title'));

          if (substr($shipping_title, -1) == ':') {
            $shipping_title = substr($shipping_title, 0, -1);
          }
        }
      }


// order status
      $Qstatus = $OSCOM_PDO->prepare('select orders_status_name
                                           from :table_orders_status
                                           where orders_status_id = :orders_status_id
                                           and language_id = :language_id
                                          ');
      $Qstatus->bindInt(':orders_status_id', (int)$Qorder->value['orders_status']);
      $Qstatus->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qstatus->execute();

// status invoice
      $QorderStatusInvoice = $OSCOM_PDO->prepare('select orders_status_invoice_name
                                                  from :table_orders_status_invoice
                                                  where orders_status_invoice_id = :orders_status_invoice_id
                                                  and language_id = :language_id
                                                ');
      $QorderStatusInvoice->bindInt(':orders_status_invoice_id', (int)$Qorder->value['orders_status_invoice']);
      $QorderStatusInvoice->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $QorderStatusInvoice->execute();

      $this->info = array('currency' => $Qorder->value('currency'),
                          'currency_value' => $Qorder->valueDecimal('currency_value'),
                          'payment_method' => $Qorder->value('payment_method'),
                          'cc_type' => $Qorder->value('cc_type'),
                          'cc_owner' => $Qorder->value('cc_owner'),
                          'cc_number' => $Qorder->value('cc_number'),
                          'cc_expires' => $Qorder->value('cc_expires'),
                          'date_purchased' => $Qorder->value('date_purchased'),
                          'orders_status' => $Qstatus->value('orders_status_name'),
                          'orders_status_invoice' => $QorderStatusInvoice->value['orders_status_invoice_name'],
                          'last_modified' => $Qorder->value('last_modified'),
                          'total' => $order_total,
                          'shipping_method' => $shipping_title);

      $this->customer = array('id' => $Qorder->valueInt('customers_id'),
                              'group_id' => $Qorder->valueInt['customers_group_id'],
                              'name' => $Qorder->value('customers_name'),
                              'company' => $Qorder->value('customers_company'),
                              'street_address' => $Qorder->value('customers_street_address'),
                              'suburb' => $Qorder->value('customers_suburb'),
                              'city' => $Qorder->value('customers_city'),
                              'postcode' => $Qorder->value('customers_postcode'),
                              'state' => $Qorder->value('customers_state'),
                              'country' => array('title' => $Qorder->value('customers_country')),
                              'format_id' => $Qorder->valueInt('customers_address_format_id'),
                              'telephone' => $Qorder->value('customers_telephone'),
                              'cellular_phone' => $Qorder->value['customers_cellular_phone'],
                              'email_address' => $Qorder->value('customers_email_address'));

      $this->delivery = array('name' => $Qorder->value('delivery_name'),
                              'company' => $Qorder->value('delivery_company'),
                              'street_address' => $Qorder->value('delivery_street_address'),
                              'suburb' => $Qorder->value('delivery_suburb'),
                              'city' => $Qorder->value('delivery_city'),
                              'postcode' => $Qorder->value('delivery_postcode'),
                              'state' => $Qorder->value('delivery_state'),
                              'country' => array('title' => $Qorder->value('delivery_country')),
                              'format_id' => $Qorder->valueInt('delivery_address_format_id'));

      if (empty($this->delivery['name']) && empty($this->delivery['street_address'])) {
        $this->delivery = false;
      }

      $this->billing = array('name' => $Qorder->value('billing_name'),
                             'company' => $Qorder->value('billing_company'),
                             'street_address' => $Qorder->value('billing_street_address'),
                             'suburb' => $Qorder->value('billing_suburb'),
                             'city' => $Qorder->value('billing_city'),
                             'postcode' => $Qorder->value('billing_postcode'),
                             'state' => $Qorder->value('billing_state'),
                             'country' => array('title' => $Qorder->value('billing_country')),
                             'format_id' => $Qorder->valueInt('billing_address_format_id'));

      $index = 0;

      $Qproducts = $OSCOM_PDO->prepare('select orders_products_id,
                                              products_id, 
                                              products_name, 
                                              products_model, 
                                              products_price, 
                                              products_tax, 
                                              products_quantity, 
                                              final_price,
                                              products_full_id 
                                              from :table_orders_products 
                                      where orders_id = :orders_id
                                      ');
      $Qproducts->bindInt(':orders_id', $order_id);
      $Qproducts->execute();
      
      while ($Qproducts->fetch()) {

        $this->products[$index] = array('qty' => $Qproducts->valueInt('products_quantity'),
                                        'id' => $Qproducts->valueInt('products_id'),
                                        'name' => $Qproducts->value('products_name'),
                                        'model' => $Qproducts->value('products_model'),
                                        'tax' => $Qproducts->valueDecimal('products_tax'),
                                        'price' => $Qproducts->valueDecimal('products_price'),
                                        'final_price' => $Qproducts->valueDecimal('final_price'),
                                        'products_full_id' =>  $Qorder->valueInt['products_id']
                                        );

        $subindex = 0;

// attributes
        $Qattributes = $OSCOM_PDO->prepare('select products_options,
                                                   products_options_values,
                                                   options_values_price,
                                                   price_prefix,
                                                   products_attributes_reference
                                            from :table_orders_products_attributes
                                            where orders_id = :orders_id
                                            and orders_products_id = :orders_products_id
                                          ');
        $Qattributes->bindInt(':orders_id', (int)$order_id);
        $Qattributes->bindInt(':orders_products_id',  $Qproducts->valueInt('orders_products_id'));
        $Qattributes->execute();

        if ($Qattributes->fetch() !== false) {
          do {
             $this->products[$index]['attributes'][$subindex] = array('option' => $Qattributes->value('products_options'),
                                                                     'value' => $Qattributes->value('products_options_values'),
                                                                     'prefix' => $Qattributes->value('price_prefix'),
                                                                     'price' => $Qattributes->valueDecimal('options_values_price'),
                                                                     'reference' => $Qattributes->value('products_attributes_reference')
                                                                     );
            $subindex++;
          } while ($Qattributes->fetch());
        }

        $this->info['tax_groups']["{$this->products[$index]['tax']}"] = '1';

        $index++;
      }
    }

    function cart() {
// Ajout de $customer_group pour connaitre le groupe auquel fait partie le client
      global $OSCOM_Customer, $OSCOM_PDO, $currencies, $OSCOM_DetectMobile;

      $this->content_type = $_SESSION['cart']->get_content_type();

      if ( ($this->content_type != 'virtual') && ($_SESSION['sendto'] == false) ) {
        $_SESSION['sendto'] = $OSCOM_Customer->getDefaultAddressID();
      }

// recuperation des informations clients B2B pour enregistrement commandes

      if ($OSCOM_Customer->getCustomersGroupID() != '0') {

        $Qcustomer = $OSCOM_PDO->prepare('select c.customers_firstname,
                                                 c.customers_lastname,
                                                 c.customers_group_id,
                                                 c.customers_company,
                                                 c.customers_telephone,
                                                 c.customers_cellular_phone,
                                                 c.customers_email_address,
                                                 c.customers_siret,
                                                 c.customers_ape,
                                                 c.customers_tva_intracom,
                                                 ab.entry_company,
                                                 ab.entry_street_address,
                                                 ab.entry_suburb,
                                                 ab.entry_postcode,
                                                 ab.entry_city,
                                                 ab.entry_zone_id,
                                                 z.zone_name,
                                                 co.countries_id,
                                                 co.countries_name,
                                                 co.countries_iso_code_2,
                                                 co.countries_iso_code_3,
                                                 co.address_format_id,
                                                 ab.entry_state
                                       from :table_customers c,
                                            :table_address_book ab left join :table_zones z on (ab.entry_zone_id = z.zone_id)
                                                                   left join :table_countries co on (ab.entry_country_id = co.countries_id)
                                      where c.customers_id = :customers_id
                                      and ab.customers_id = :customers_id
                                      and c.customers_default_address_id = ab.address_book_id
                                          ');
        $Qcustomer->bindInt(':customers_id', $OSCOM_Customer->getID());

// recuperation des informations clients normaux pour enregistrement commandes avec en plus infos sur customers_group_id
      } else {

        $Qcustomer = $OSCOM_PDO->prepare('select c.customers_firstname,
                                                 c.customers_lastname,
                                                 c.customers_group_id,
                                                 c.customers_telephone,
                                                 c.customers_cellular_phone,
                                                 c.customers_email_address,
                                                 c.customers_tva_intracom,
                                                 ab.entry_company,
                                                 ab.entry_street_address,
                                                 ab.entry_suburb,
                                                 ab.entry_postcode,
                                                 ab.entry_city,
                                                 ab.entry_zone_id,
                                                 z.zone_name,
                                                 co.countries_id,
                                                 co.countries_name,
                                                 co.countries_iso_code_2,
                                                 co.countries_iso_code_3,
                                                 co.address_format_id,
                                                 ab.entry_state
                                         from :table_customers c,
                                              :table_address_book ab left join :table_zones z on (ab.entry_zone_id = z.zone_id)
                                                                    left join :table_countries co on (ab.entry_country_id = co.countries_id)
                                         where c.customers_id = :customers_id
                                         and ab.customers_id = :customers_id
                                         and c.customers_default_address_id = ab.address_book_id
                                    ');
        $Qcustomer->bindInt(':customers_id', $OSCOM_Customer->getID());
      }

      $Qcustomer->execute();
      $customer_address = $Qcustomer->toArray();

      if (is_array($_SESSION['sendto']) && !empty($_SESSION['sendto'])) {
        $shipping_address = array('entry_firstname' => $_SESSION['sendto']['firstname'],
                                  'entry_lastname' => $_SESSION['sendto']['lastname'],
                                  'entry_company' => $_SESSION['sendto']['company'],
                                  'entry_street_address' => $_SESSION['sendto']['street_address'],
                                  'entry_suburb' => $_SESSION['sendto']['suburb'],
                                  'entry_postcode' => $_SESSION['sendto']['postcode'],
                                  'entry_city' => $_SESSION['sendto']['city'],
                                  'entry_zone_id' => $_SESSION['sendto']['zone_id'],
                                  'zone_name' => $_SESSION['sendto']['zone_name'],
                                  'entry_country_id' => $_SESSION['sendto']['country_id'],
                                  'countries_id' => $_SESSION['sendto']['country_id'],
                                  'countries_name' => $_SESSION['sendto']['country_name'],
                                  'countries_iso_code_2' => $_SESSION['sendto']['country_iso_code_2'],
                                  'countries_iso_code_3' => $_SESSION['sendto']['country_iso_code_3'],
                                  'address_format_id' => $_SESSION['sendto']['address_format_id'],
                                  'entry_state' => $_SESSION['sendto']['zone_name']);

      } elseif (is_numeric($_SESSION['sendto'])) {

        $Qaddress = $OSCOM_PDO->prepare('select ab.entry_firstname,
                                               ab.entry_lastname,
                                               ab.entry_company,
                                               ab.entry_street_address,
                                               ab.entry_suburb,
                                               ab.entry_postcode,
                                               ab.entry_city,
                                               ab.entry_zone_id,
                                               z.zone_name,
                                               ab.entry_country_id,
                                               c.countries_id,
                                               c.countries_name,
                                               c.countries_iso_code_2,
                                               c.countries_iso_code_3,
                                               c.address_format_id,
                                               ab.entry_state
                                       from :table_address_book ab left join :table_zones z on (ab.entry_zone_id = z.zone_id)
                                                                   left join :table_countries c on (ab.entry_country_id = c.countries_id)
                                       where ab.customers_id = :customers_id
                                       and ab.address_book_id = :address_book_id
                                    ');
        $Qaddress->bindInt(':customers_id', $OSCOM_Customer->getID());
        $Qaddress->bindInt(':address_book_id', (int)$_SESSION['sendto']);
        $Qaddress->execute();

        $shipping_address = $Qaddress->toArray();

      } else {

        $shipping_address = array('entry_firstname' => null,
                                  'entry_lastname' => null,
                                  'entry_company' => null,
                                  'entry_street_address' => null,
                                  'entry_suburb' => null,
                                  'entry_postcode' => null,
                                  'entry_city' => null,
                                  'entry_zone_id' => null,
                                  'zone_name' => null,
                                  'entry_country_id' => null,
                                  'countries_id' => null,
                                  'countries_name' => null,
                                  'countries_iso_code_2' => null,
                                  'countries_iso_code_3' => null,
                                  'address_format_id' => 0,
                                  'entry_state' => null);
      }

      if (is_array($_SESSION['billto']) && !empty($_SESSION['billto'])) {

        $billing_address = array('entry_firstname' => $_SESSION['billto']['firstname'],
                                 'entry_lastname' => $_SESSION['billto']['lastname'],
                                 'entry_company' => $_SESSION['billto']['company'],
                                 'entry_street_address' => $_SESSION['billto']['street_address'],
                                 'entry_suburb' => $_SESSION['billto']['suburb'],
                                 'entry_postcode' => $_SESSION['billto']['postcode'],
                                 'entry_city' => $_SESSION['billto']['city'],
                                 'entry_zone_id' => $_SESSION['billto']['zone_id'],
                                 'zone_name' => $_SESSION['billto']['zone_name'],
                                 'entry_country_id' => $_SESSION['billto']['country_id'],
                                 'countries_id' => $_SESSION['billto']['country_id'],
                                 'countries_name' => $_SESSION['billto']['country_name'],
                                 'countries_iso_code_2' => $_SESSION['billto']['country_iso_code_2'],
                                 'countries_iso_code_3' => $_SESSION['billto']['country_iso_code_3'],
                                 'address_format_id' => $_SESSION['billto']['address_format_id'],
                                 'entry_state' => $_SESSION['billto']['zone_name']);
      } else {

        $Qaddress = $OSCOM_PDO->prepare('select ab.entry_firstname,
                                                      ab.entry_lastname,
                                                      ab.entry_company,
                                                      ab.entry_street_address,
                                                      ab.entry_suburb,
                                                      ab.entry_postcode,
                                                      ab.entry_city,
                                                      ab.entry_zone_id,
                                                      z.zone_name,
                                                      ab.entry_country_id,
                                                      c.countries_id,
                                                      c.countries_name,
                                                      c.countries_iso_code_2,
                                                      c.countries_iso_code_3,
                                                      c.address_format_id,
                                                      ab.entry_state
                                             from :table_address_book ab left join :table_zones z on (ab.entry_zone_id = z.zone_id)
                                                                         left join :table_countries c on (ab.entry_country_id = c.countries_id)
                                             where ab.customers_id = :customers_id
                                             and ab.address_book_id = :address_book_id
                                    ');
        $Qaddress->bindInt(':customers_id', (int)$OSCOM_Customer->getID());
        $Qaddress->bindInt(':address_book_id', (int)$_SESSION['billto']);
        $Qaddress->execute();

        $billing_address = $Qaddress->toArray();

      }

      if ($this->content_type == 'virtual') {
        $tax_address = array('entry_country_id' => $billing_address['entry_country_id'],
                             'entry_zone_id' => $billing_address['entry_zone_id']);
      } else {
        $tax_address = array('entry_country_id' => $shipping_address['entry_country_id'],
                             'entry_zone_id' => $shipping_address['entry_zone_id']);
      }

      $this->info = array('order_status' => DEFAULT_ORDERS_STATUS_ID,
                          'order_status_invoice' => DEFAULT_ORDERS_STATUS_INVOICE_ID,
                          'currency' => $_SESSION['currency'],
                          'currency_value' => $currencies->currencies[$_SESSION['currency']]['value'],
                          'payment_method' =>  $_SESSION['payment'],
                          'cc_type' => '',
                          'cc_owner' => '',
                          'cc_number' => '',
                          'cc_expires' => '',
                          'shipping_method' => $_SESSION['shipping']['title'],
                          'shipping_cost' => $_SESSION['shipping']['cost'],
                          'subtotal' => 0,
                          'tax' => 0,
                          'tax_groups' => array(),
                          'comments' => (isset($_SESSION['comments']) && !empty($_SESSION['comments']) ? $_SESSION['comments'] : ''));

      if (isset($GLOBALS[$_SESSION['payment']]) && is_object($GLOBALS[$_SESSION['payment']])) {
        if (isset($GLOBALS[$_SESSION['payment']]->public_title)) {
          $this->info['payment_method'] = $GLOBALS[$_SESSION['payment']]->public_title;
        } else {
          $this->info['payment_method'] = $GLOBALS[$_SESSION['payment']]->title;
        }

        if ( isset($GLOBALS[$_SESSION['payment']]->order_status) && is_numeric($GLOBALS[$_SESSION['payment']]->order_status) && ($GLOBALS[$_SESSION['payment']]->order_status > 0) ) {
          $this->info['order_status'] = $GLOBALS[$_SESSION['payment']]->order_status;
        }
      }



// prise en compte de la compagnie en fonction du mode B2B ou non
      if (!empty($customer_address['customers_company'])) {
        $company_name = $customer_address['customers_company'];
      } else {
        $company_name =  $customer_address['entry_company'];
      }

      $this->customer = array('firstname' => $customer_address['customers_firstname'],
                              'group_id' => $customer_address['customers_group_id'],
                              'lastname' => $customer_address['customers_lastname'],
                              'company' => $company_name,
                              'street_address' => $customer_address['entry_street_address'],
                              'suburb' => $customer_address['entry_suburb'],
                              'city' => $customer_address['entry_city'],
                              'postcode' => $customer_address['entry_postcode'],
                              'state' => ((osc_not_null($customer_address['entry_state'])) ? $customer_address['entry_state'] : $customer_address['zone_name']),
                              'zone_id' => $customer_address['entry_zone_id'],
                              'country' => array('id' => $customer_address['countries_id'], 'title' => $customer_address['countries_name'], 'iso_code_2' => $customer_address['countries_iso_code_2'], 'iso_code_3' => $customer_address['countries_iso_code_3']),
                              'format_id' => $customer_address['address_format_id'],
                              'telephone' => $customer_address['customers_telephone'],
                              'cellular_phone' => $customer_address['customers_cellular_phone'],
                              'email_address' => $customer_address['customers_email_address']);

// recuperation des informations societes pour les clients B2B qui est transmit au fichier checkout_process.php
      if ($OSCOM_Customer->getCustomersGroupID() != '0') {
        $this->customer['siret'] = $customer_address['customers_siret'];
        $this->customer['ape'] = $customer_address['customers_ape'];
        $this->customer['tva_intracom'] = $customer_address['customers_tva_intracom'];
      }

      $this->delivery = array('firstname' => $shipping_address['entry_firstname'],
                              'lastname' => $shipping_address['entry_lastname'],
                              'company' => $shipping_address['entry_company'],
                              'street_address' => $shipping_address['entry_street_address'],
                              'suburb' => $shipping_address['entry_suburb'],
                              'city' => $shipping_address['entry_city'],
                              'postcode' => $shipping_address['entry_postcode'],
                              'state' => ((osc_not_null($shipping_address['entry_state'])) ? $shipping_address['entry_state'] : $shipping_address['zone_name']),
                              'zone_id' => $shipping_address['entry_zone_id'],
                              'country' => array('id' => $shipping_address['countries_id'], 'title' => $shipping_address['countries_name'], 'iso_code_2' => $shipping_address['countries_iso_code_2'], 'iso_code_3' => $shipping_address['countries_iso_code_3']),
                              'country_id' => $shipping_address['entry_country_id'],
                              'format_id' => $shipping_address['address_format_id']);

      $this->billing = array('firstname' => $billing_address['entry_firstname'],
                             'lastname' => $billing_address['entry_lastname'],
                             'company' => $billing_address['entry_company'],
                             'street_address' => $billing_address['entry_street_address'],
                             'suburb' => $billing_address['entry_suburb'],
                             'city' => $billing_address['entry_city'],
                             'postcode' => $billing_address['entry_postcode'],
                             'state' => ((osc_not_null($billing_address['entry_state'])) ? $billing_address['entry_state'] : $billing_address['zone_name']),
                             'zone_id' => $billing_address['entry_zone_id'],
                             'country' => array('id' => $billing_address['countries_id'], 'title' => $billing_address['countries_name'], 'iso_code_2' => $billing_address['countries_iso_code_2'], 'iso_code_3' => $billing_address['countries_iso_code_3']),
                             'country_id' => $billing_address['entry_country_id'],
                             'format_id' => $billing_address['address_format_id']);

      $index = 0;
      $products = $_SESSION['cart']->get_products();

      if( isset($_SESSION['coupon']) && osc_not_null($_SESSION['coupon']) ) {
        require(DIR_WS_CLASSES.'discount_coupon.php' );
        $this->coupon = new discount_coupon(  $_SESSION['coupon'], $this->delivery );
        $this->coupon->total_valid_products( $products );
        $valid_products_count = 0;
      }

      for ($i=0, $n=sizeof($products); $i<$n; $i++) {

// Display an indicator to identify if the product belongs at a customer group or not.
        $QproductsQuantityUnitId = $OSCOM_PDO->prepare('select products_quantity_unit_id_group
                                                        from :table_products_groups
                                                        where products_id = :products_id
                                                        and customers_group_id =  :customers_group_id
                                                       ');

        $QproductsQuantityUnitId->bindInt(':products_id', (int)$products[$i]['id'] );
        $QproductsQuantityUnitId->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());

        $QproductsQuantityUnitId->execute();

        $products_quantity_unit_id = $QproductsQuantityUnitId->fetch();

         if ($products_quantity_unit_id['products_quantity_unit_id_group'] > '0') {
           $model[$i] =  PRODUCTS_GROUP_QUANTITY_UNIT_TITLE . $products[$i]['model'];
         } else {
           $model[$i] =  $products[$i]['model'];
         }

// Marketing : increase the price on the mobile phone and tablet
// see shopping_cart cart class - currencies.php - order.php
         if (PRICE_MOBILE_ACCEPT == 'true') {
           if ($OSCOM_Customer->getCustomersGroupID() == 0) {
             if ($OSCOM_DetectMobile->isMobile() || $OSCOM_DetectMobile->isTablet()) {
               if ( (PRICE_MOBILE_POURCENTAGE != 0) || (PRICE_MOBILE_POURCENTAGE != '') ) {
                 $products[$i]['price'] = $products[$i]['price'] * (1+(PRICE_MOBILE_POURCENTAGE/100));
               }
             }
           }
         } // end PRICE_MOBILE_ACCEPT

        $this->products[$index] = array('qty' => $products[$i]['quantity'],
                                        'name' => $products[$i]['name'],
                                        'model' => $model[$i],
                                        'tax' => osc_get_tax_rate($products[$i]['tax_class_id'], $tax_address['entry_country_id'], $tax_address['entry_zone_id']),
                                        'tax_description' => osc_get_tax_description($products[$i]['tax_class_id'], $tax_address['entry_country_id'], $tax_address['entry_zone_id']),
                                        'price' => $products[$i]['price'],
                                        'final_price' => $products[$i]['price'] + $_SESSION['cart']->attributes_price($products[$i]['id']),
                                        'weight' => $products[$i]['weight'],
                                        'id' => $products[$i]['id']);

// Requetes SQL pour savoir si le groupe B2B a les prix affiches en HT ou TTC
        if ($OSCOM_Customer->getCustomersGroupID() != '0') {
//Group tax
          $QgroupTax = ''; // reinitialise la valeur comme null
          $QgroupTax = $OSCOM_PDO->prepare('select group_order_taxe,
                                                   group_tax
                                           from :table_customers_groups
                                           where customers_group_id = :customers_group_id
                                          ');
          $QgroupTax->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
          $QgroupTax->execute();

          $group_tax = $QgroupTax->fetch();

// order customers price
          $QordersCustomersPrice = $OSCOM_PDO->prepare('select customers_group_price
                                                        from :table_products_groups
                                                        where customers_group_id = :customers_group_id
                                                        and products_id = :products_id
                                                        ');
          $QordersCustomersPrice->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID());
          $QordersCustomersPrice->bindInt(':products_id', (int)$products[$i]['id']);
          $QordersCustomersPrice->execute();

          $orders_customers = $QordersCustomersPrice->fetch();

          if ($orders_customers = $QordersCustomersPrice->fetch() ) {

            $this->products[$index] = array('price' => $orders_customers['customers_group_price'],
                                            'final_price' => $orders_customers['customers_group_price'] + $_SESSION['cart']->attributes_price($products[$i]['id']));
          }
        }

        if ($products[$i]['attributes']) {
          $subindex = 0;

          foreach($products[$i]['attributes'] as $option => $value) {

            $Qattributes = $OSCOM_PDO->prepare('select popt.products_options_name,
                                                       poval.products_options_values_name,
                                                       pa.options_values_price,
                                                       pa.price_prefix,
                                                       pa.products_attributes_reference
                                                from :table_products_options popt,
                                                     :table_products_options_values poval,
                                                     :table_products_attributes pa
                                                where pa.products_id = :products_id
                                                and pa.options_id = :options_id
                                                and pa.options_id = popt.products_options_id
                                                and pa.options_values_id = :options_values_id
                                                and pa.options_values_id = poval.products_options_values_id
                                                and popt.language_id = :language_id
                                                and poval.language_id = :language_id
                                               ');
            $Qattributes->bindInt(':products_id', (int)$products[$i]['id']);
            $Qattributes->bindInt(':options_id', (int)$option);
            $Qattributes->bindInt(':options_values_id', (int)$value);
            $Qattributes->bindInt(':language_id', (int)$_SESSION['languages_id']);

            $Qattributes->execute();

            
            $this->products[$index]['attributes'][$subindex] = array('option' => $Qattributes->value('products_options_name'),
                                                                     'value' => $Qattributes->value('products_options_values_name'),
                                                                     'option_id' => $option,
                                                                     'value_id' => $value,
                                                                     'prefix' => $Qattributes->value('price_prefix'),
                                                                     'price' => $Qattributes->value('options_values_price'),
                                                                     'reference' => $Qattributes->value('products_attributes_reference')
                                                                     );

            $subindex++;
          }
        }

// Mise en commentaire dela mise a jour derniere version MS2 RC2a
//$shown_price = $currencies->calculate_price($this->products[$index]['final_price'], $this->products[$index]['tax'], $this->products[$index]['qty']);
// discount coupons
        if( is_object( $this->coupon ) ) {

          $applied_discount = 0;
          $discount = $this->coupon->calculate_discount( $this->products[$index], $valid_products_count );
          if( $discount['applied_discount'] > 0 ) $valid_products_count++;
          $shown_price = $this->coupon->calculate_shown_price( $discount, $this->products[$index] );
          $this->info['subtotal'] += $shown_price['shown_price'];
          $shown_price = $shown_price['actual_shown_price'];

        } else {

          $shown_price = osc_add_tax($this->products[$index]['final_price'], $this->products[$index]['tax']) * $this->products[$index]['qty'];
//          $shown_price = $currencies->calculate_price($this->products[$index]['final_price'], $this->products[$index]['tax'], $this->products[$index]['qty']);
          $this->info['subtotal'] += $shown_price;

        }

        $products_tax = $this->products[$index]['tax'];
        $products_tax_description = $this->products[$index]['tax_description'];

// Controle pour calculer la taxe selon configuration du groupe B2B
        if (((DISPLAY_PRICE_WITH_TAX == 'true') && ($OSCOM_Customer->getCustomersGroupID() == '0')) || (($OSCOM_Customer->getCustomersGroupID() != '0') && ($group_tax['group_tax'] == 'true'))) {

          $this->info['tax'] += $shown_price - ($shown_price / (($products_tax < 10) ? "1.0" . str_replace('.', '', $products_tax) : "1." . str_replace('.', '', $products_tax)));

          if (isset($this->info['tax_groups']["$products_tax_description"])) {
            $this->info['tax_groups']["$products_tax_description"] += $shown_price - ($shown_price / (($products_tax < 10) ? "1.0" . str_replace('.', '', $products_tax) : "1." . str_replace('.', '', $products_tax)));

          } else {
            $this->info['tax_groups']["$products_tax_description"] = $shown_price - ($shown_price / (($products_tax < 10) ? "1.0" . str_replace('.', '', $products_tax) : "1." . str_replace('.', '', $products_tax)));
          }

        } else {

          $this->info['tax'] += ($products_tax / 100) * $shown_price;

          if (isset($this->info['tax_groups']["$products_tax_description"])) {
            $this->info['tax_groups']["$products_tax_description"] += ($products_tax / 100) * $shown_price;
          } else {
            $this->info['tax_groups']["$products_tax_description"] = ($products_tax / 100) * $shown_price;
          }
        }

        $index++;
      }

// Calcul de la TVA sur le montant total de la facture.
// Client B2C : Prix TOTAL toujours affiche en TTC meme si la boutique est en mode affichage prix en HT (Prix TTC selon la class TVA mise sur les produits)
// Clients B2B : Prix TOTAL afffiche en HT ou en TTC selon si l'assujetti de la TVA est active dans les groupes
      if (((DISPLAY_PRICE_WITH_TAX == 'true') && ($OSCOM_Customer->getCustomersGroupID() == '0')) || (($OSCOM_Customer->getCustomersGroupID() != '0') && ($group_tax['group_tax'] == 'true')) || (($OSCOM_Customer->getCustomersGroupID() != '0') && ($group_tax['group_order_taxe'] == 1))) {
        $this->info['total'] = $this->info['subtotal'] + $this->info['shipping_cost'];
      } else {
        $this->info['total'] = $this->info['subtotal'] + $this->info['tax'] + $this->info['shipping_cost'];
      }

// discount coupon
      if( is_object( $this->coupon ) ) {
        $this->info['total'] = $this->coupon->finalize_discount( $this->info );
      }
    }
  }

