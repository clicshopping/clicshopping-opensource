<?php
  /**
   * cfm_modules_products_specials.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */


  class cfgm_modules_products_special {
    var $code = 'modules_products_special';
    var $directory;
    var $language_directory = DIR_FS_CATALOG_LANGUAGES;
    var $key = 'MODULE_MODULES_PRODUCTS_SPECIAL_INSTALLED';
    var $title;
    var $template_integration = true;

    function cfgm_modules_products_special() {
      $this->directory = DIR_FS_CATALOG . DIR_WS_TEMPLATE . SITE_THEMA . '/modules/modules_products_special/';
      $this->title = MODULE_CFG_MODULE_PRODUCTS_SPECIAL_MODULES_TITLE;
    }
  }
?>
