<?php
/**
 * products.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
 */

class Blog {

  protected $blog_content_id;

  Public function __construct($blog_content_id = '') {
    global $OSCOM_Customer, $OSCOM_PDO;

    if ( (!$blog_content_id) || (!is_numeric($blog_content_id)) ) return false;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {

      $QblogContent = $OSCOM_PDO->prepare('select blog_content_id,
                                                  blog_content_status,
                                                  blog_content_archive
                                            where  blog_content_id = :blog_content_id
                                            and blog_content_archive = :blog_content_archive
                                            and customers_group_id = :customers_group_id
                                            and blog_content_status = :blog_content_status
                                          ');
      $QblogContent->bindInt(':blog_content_id', (int)$_GET['blog_content_id'] );
      $QblogContent->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
      $QblogContent->bindValue(':blog_content_archive', '0' );
      $QblogContent->bindValue(':products_status',   '1' );

    } else {

      $QblogContent = $OSCOM_PDO->prepare('select blog_content_id,
                                                  blog_content_status,
                                                  blog_content_archive
                                           from :table_blog_content
                                           where  blog_content_id = :blog_content_id
                                           and blog_content_archive = :blog_content_archive
                                           and customers_group_id = :customers_group_id
                                           and blog_content_status = :blog_content_status
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$_GET['blog_content_id'] );
      $QblogContent->bindValue(':customers_group_id','0' );
      $QblogContent->bindValue(':blog_content_archive', '0');
      $QblogContent->bindValue(':blog_content_status','1' );

    }

    $QblogContent->execute();
    $blogContent = $QblogContent->fetch();

    $this->data = $blogContent;
    $this->id = $blogContent['blog_content_id'];
    $this->blogContentArchive = $blogContent['blog_content_archive'];
    $this->blogContentstatus = $blogContent['blog_content_status'];
  }


  public function getData()  {
    return $this->data;
  }

// returns a single element of the data array
  public function get($obj)  {
    return $this->data[$obj];
  }

  Public function getId() {
    return $this->id;
  }

  Public function getBlogArchive() {
    return $this->blogContentArchive;
  }

  Public function getStatus() {
    return $this->blogContentstatus;
  }

/**
 * Number of blog content
 *
 * @param string
 * @return string $blog_check['total'], blog total
 * @access private
 */
  Private function setBlogCount() {
    global $OSCOM_PDO;

    $QblogCheck = $OSCOM_PDO->prepare('select count(*) as total
                                         from :table_blog_content bc,
                                              :table_blog_content_description bcd
                                         where bc.blog_content_status = :blog_content_status
                                         and bc.blog_content_id = :blog_content_id
                                         and bcd.blog_content_id = bc.blog_content_id
                                         and bcd.language_id = :language_id
                                      ');
    $QblogCheck->bindInt(':blog_content_id', (int)$this->id );
    $QblogCheck->bindInt(':language_id', (int)$_SESSION['languages_id'] );
    $QblogCheck->bindValue(':blog_content_status', $this->blogContentstatus );
    $QblogCheck->execute();

    $blog_check = $QblogCheck->fetch();

    return $blog_check['total'];
  }

/**
 * Number of blog
 *
 * @param string
 * @return string $blog_check['total'], blog total
 * @access public
 */
  Public function getBlogCount() {
    return $this->setBlogCount();
  }

/**
 * Author of blog
 *
 * @param string
 * @return string $author, author
 * @access private
 */
  Private function setBlogContentAuthor() {
    global $OSCOM_PDO, $OSCOM_Customer;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {
      $QblogContent = $OSCOM_PDO->prepare('select blog_content_author
                                           from :table_blog_content
                                           where  blog_content_id = :blog_content_id
                                           and customers_group_id = :customers_group_id
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );

    } else {

      $QblogContent = $OSCOM_PDO->prepare('select blog_content_author
                                           from :table_blog_content
                                           where  blog_content_id = :blog_content_id
                                           and customers_group_id = :customers_group_id
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindValue(':customers_group_id','0' );
    }

    $QblogContent->execute();
    $blog_content = $QblogContent->fetch();

    if (!empty($blog_content['blog_content_author'])) {
      $author = osc_output_string_protected($blog_content['blog_content_author']);
    } else {
      $author = '';
    }

    return $author;

  }


/**
  * Display Author of blog
  *
  * @param string
  * @return string $author, author
  * @access public
*/
  Public function getBlogContentAuthor() {
    return $this->setBlogContentAuthor();
  }




  /**
   * Description of blog
   *
   * @param string
   * @return string $author, author
   * @access private
   */
  Private function setBlogContentDescription() {
    global $OSCOM_Customer, $OSCOM_PDO;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {
// Mode b2b
      $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_description
                                               from :table_blog_content bc,
                                                    :table_blog_content_description bcd
                                               where bc.blog_content_status = :blog_content_status
                                               and bc.blog_content_id = :blog_content_id
                                               and bcd.blog_content_id = bc.blog_content_id
                                               and bcd.language_id = :language_id
                                               and bc.blog_content_archive = :blog_content_archive
                                               and bc.customers_group_id = :customers_group_id
                                             ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
      $QblogContent->bindInt(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindInt(':blog_content_archive', $this->blogContentArchive);
    } else {
// Mode normal
      $QblogContent = $OSCOM_PDO->prepare('select  bcd.blog_content_description
                                                 from :table_blog_content bc,
                                                      :table_blog_content_description bcd
                                                 where bc.blog_content_status = :blog_content_status
                                                 and bc.blog_content_id = :blog_content_id
                                                 and bcd.blog_content_id = bc.blog_content_id
                                                 and bcd.language_id = :language_id
                                                 and bc.blog_content_archive = :blog_content_archive
                                                 and bc.customers_group_id = :customers_group_id
                                               ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindInt(':customers_group_id','0' );
      $QblogContent->bindInt(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindInt(':blog_content_archive',$this->blogContentArchive);
    }



    $QblogContent->execute();
    $blog_content = $QblogContent->fetch();

    $blog_content_description =  $blog_content['blog_content_description'];

    return $blog_content_description;
  }


/**
* Display description of blog
*
* @param string
* @return string $blog_content_description, blog description
* @access public
*/
  Public function getBlogContentDescription() {
    return $this->setBlogContentDescription();
  }





/**
 * Name of blog
 *
 * @param string
 * @return string $blog_content_name, blog name
 * @access private
 */
  Private function setBlogContentName() {
    global $OSCOM_PDO, $OSCOM_Customer;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {
      $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_name
                                           from :table_blog_content bc,
                                                :table_blog_content_description bcd
                                             where bc.blog_content_status = :blog_content_status
                                             and bc.blog_content_id = :blog_content_id
                                             and bcd.blog_content_id = bc.blog_content_id
                                             and bcd.language_id = :language_id
                                             and bc.blog_content_archive = :blog_content_archive
                                             and bc.customers_group_id = :customers_group_id
                                           ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
      $QblogContent->bindValue(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindValue(':blog_content_archive', $this->blogContentArchive);


    } else {

      $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_name
                                           from :table_blog_content bc,
                                                :table_blog_content_description bcd
                                           where bc.blog_content_status = :blog_content_status
                                           and bc.blog_content_id = :blog_content_id
                                           and bcd.blog_content_id = bc.blog_content_id
                                           and bc.blog_content_archive = :blog_content_archive
                                           and bcd.language_id = :language_id
                                           and bc.customers_group_id = :customers_group_id
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindValue(':customers_group_id','0' );
      $QblogContent->bindValue(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindValue(':blog_content_archive', $this->blogContentArchive);
    }

    $QblogContent->execute();
    $blog_content = $QblogContent->fetch();

    $blog_content_name =  osc_output_string_protected($blog_content['blog_content_name']);
    $blog_content_name = '<a href="' . osc_href_link('blog_content.php', 'blog_content_id=' . (int)$_GET['blog_content_id']) . '" itemprop="url" class="blogContentName" rel="author"><span itemprop="name">' . $blog_content_name . '</span></a>';

    return  $blog_content_name;
  }


/**
 * Display name of blog
 *
 * @param string
 * @return string $blog_content_name, blog name
 * @access public
 */
  Public function getBlogContentName() {
    return $this->setBlogContentName();
  }


/**
 * Tag of blog
 *
 * @param string
 * @return string $tag, blog tag
 * @access private
 */
  Private function setBlogContentTag() {
    global $OSCOM_PDO, $OSCOM_Customer;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {
        $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_head_tag_blog
                                             from :table_blog_content bc,
                                                  :table_blog_content_description bcd
                                             where bc.blog_content_status = :blog_content_status
                                             and bc.blog_content_id = :blog_content_id
                                             and bcd.blog_content_id = bc.blog_content_id
                                             and bcd.language_id = :language_id
                                             and bc.blog_content_archive = :blog_content_archive
                                             and bc.customers_group_id = :customers_group_id
                                             ');
        $QblogContent->bindInt(':blog_content_id', (int)$this->id );
        $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
        $QblogContent->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
        $QblogContent->bindValue(':blog_content_status', $this->blogContentstatus);
        $QblogContent->bindValue(':blog_content_archive', $this->blogContentArchive);
    } else {
      $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_head_tag_blog
                                           from :table_blog_content bc,
                                                :table_blog_content_description bcd
                                           where bc.blog_content_status = :blog_content_status
                                           and bc.blog_content_id = :blog_content_id
                                           and bcd.blog_content_id = bc.blog_content_id
                                           and bc.blog_content_archive = :blog_content_archive
                                           and bcd.language_id = :language_id
                                           and bc.customers_group_id = :customers_group_id
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindValue(':customers_group_id','0' );
      $QblogContent->bindValue(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindValue(':blog_content_archive', $this->blogContentArchive);
    }

    $QblogContent->execute();
    $blog_content = $QblogContent->fetch();

    if (!empty($blog_content['blog_content_head_tag_blog'])) {
      $blog_content_tag = osc_output_string_protected($blog_content['blog_content_head_tag_blog']);
      $delimiter =',';
      $blog_content_tag = trim(preg_replace('|\\s*(?:' . preg_quote($delimiter) . ')\\s*|', $delimiter, $blog_content_tag));
      $tag = explode(",", $blog_content_tag);
    } else {
      $tag = '';
    }

    return $tag;
  }


/**
 * Display tag of blog
 *
 * @param string
 * @return string $tag, blog tag
 * @access public
 */
  Public function getBlogContentTag() {
    return $this->setBlogContentTag();
  }



  /**
   * Tag of product for blog
   *
   * @param string
   * @return string $tag, blog tag
   * @access private
   */
  Private function setBlogContentTagProduct() {

    global $OSCOM_PDO, $OSCOM_Customer;

    if ($OSCOM_Customer->getCustomersGroupID() != 0) {

      $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_head_tag_product
                                           from :table_blog_content bc,
                                                :table_blog_content_description bcd
                                           where bc.blog_content_status = :blog_content_status
                                           and bc.blog_content_id = :blog_content_id
                                           and bcd.blog_content_id = bc.blog_content_id
                                           and bcd.language_id = :language_id
                                           and bc.blog_content_archive = :blog_content_archive
                                           and bc.customers_group_id = :customers_group_id
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindInt(':customers_group_id', (int)$OSCOM_Customer->getCustomersGroupID() );
      $QblogContent->bindValue(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindValue(':blog_content_archive', $this->blogContentArchive);

    } else {
      $QblogContent = $OSCOM_PDO->prepare('select bcd.blog_content_head_tag_product
                                           from :table_blog_content bc,
                                                :table_blog_content_description bcd
                                           where bc.blog_content_status = :blog_content_status
                                           and bc.blog_content_id = :blog_content_id
                                           and bcd.blog_content_id = bc.blog_content_id
                                           and bc.blog_content_archive = :blog_content_archive
                                           and bcd.language_id = :language_id
                                           and bc.customers_group_id = :customers_group_id
                                         ');
      $QblogContent->bindInt(':blog_content_id', (int)$this->id );
      $QblogContent->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QblogContent->bindValue(':customers_group_id','0' );
      $QblogContent->bindValue(':blog_content_status', $this->blogContentstatus);
      $QblogContent->bindValue(':blog_content_archive', $this->blogContentArchive);
    }

    $QblogContent->execute();
    $blog_content = $QblogContent->fetch();


    if (!empty($blog_content['blog_content_head_tag_product'])) {

      $blog_content_tag = osc_output_string_protected($blog_content['blog_content_head_tag_product']);
      $delimiter =',';
      $blog_content_tag = trim(preg_replace('|\\s*(?:' . preg_quote($delimiter) . ')\\s*|', $delimiter, $blog_content_tag));
      $tag = explode(",", $blog_content_tag);
    } else {
      $tag = '';
    }

    return $tag;
  }

/**
 * Display tag of blog
 *
 * @param string
 * @return string $tag, blog tag
 * @access public
 */
  Public function getBlogContentTagProduct() {
    return $this->setBlogContentTagProduct();
  }
}