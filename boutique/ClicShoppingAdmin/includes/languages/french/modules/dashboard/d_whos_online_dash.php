<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_TITLE', 'Souhiatez vous installer l\'indicateur des clients en ligne');
define('MODULE_ADMIN_DASHBOARD_WHOS_ONLINE_DASH_DESCRIPTION', 'Affiche les clients qui sont en ligne');

define('TABLE_HEADING_ONLINE', 'En ligne');
define('TABLE_HEADING_FULL_NAME', 'Nom');
define('TABLE_HEADING_IP_ADDRESS', 'Addresse IP');
define('TABLE_HEADING_USER_AGENT', 'Agent');

define('TEXT_NONE_', 'Aucun');
define('TEXT_NUMBER_OF_CUSTOMERS', 'Actuellement il y a  <strong>%s</strong> clients en ligne.');
?>
