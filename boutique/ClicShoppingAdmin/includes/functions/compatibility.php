<?php
/*
 * compatibility.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: html_output.php 

*/

// set default timezone if none exists (PHP 5.3 throws an E_WARNING)
  date_default_timezone_set(CFG_TIME_ZONE);
?>
