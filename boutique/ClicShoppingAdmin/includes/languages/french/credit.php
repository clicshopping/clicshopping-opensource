<?php
  /*
   * credit.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  define('HEADING_TITLE','Crédits');
  define('TEXT_IDEA','Idée : ');
  define('TEXT_GRAPHISM','Graphisme Administration : ');
  define('TEXT_ARCHITECTURE','Architecture / developpement / concept  : ');
  define('TEXT_COMPANY','Entreprise : ');
  define('TEXT_COPYRIGHT','Depôt de marque : ');
  define('TEXT_LICENCE','Licences utilisées : ');
  define('TEXT_THANK_YOU','Remerciement');
  define('TEXT_THANK_YOU_1','L\'équipe souhaite remercier l\'ensemble des communautés dont particulièrement la communauté OpenSource Oscommerce qui tout au long des années nous a apportés un support important et d\'aide à la résolution de problèmes.<br /><br />
  En fonction de l\'évolution de se projet nous allons nous engager à apporter un support financier afin que cette communauté puisse continuer dans le temps tant que celle-ci ne deviendra par payante..<br /><br />
  Les sommes obtenues à partir de la marketplace servent à amortir l\'infrastructure de la communauté.<br /><br />
  Nous souhaitons aussi remercier tous ceux qui nous font confiance dans le cadre du développement de ce projet');
  define('TEXT_CLICSHOPPING_OPENSOURCE','Ce projet vous tient à coeur, n\'hésitez pas à souscrire un de nos services, ainsi nous pourrons continuer et pénéniser ce projet dans le temps. La mise en place de l\'infrastructure à un coût important en gestion et maintenance.');

?>