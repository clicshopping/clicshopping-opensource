<?php
/**
 * ot_total.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class ot_total {
    var $title, $output;

    function ot_total() {
      $this->code = 'ot_total';
      $this->title = MODULE_ORDER_TOTAL_TOTAL_TITLE;
      $this->description = MODULE_ORDER_TOTAL_TOTAL_DESCRIPTION;
      $this->enabled = ((MODULE_ORDER_TOTAL_TOTAL_STATUS == 'true') ? true : false);
      $this->sort_order = MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER;

      $this->output = array();
    }

    function process() {
      global $order, $currencies;

      $this->output[] = array('title' => $this->title . ':',
                              'text' => ' ' . $currencies->format($order->info['total'], true, $order->info['currency'], $order->info['currency_value']) . ' ',
                              'value' => $order->info['total']);
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = osc_db_query("select configuration_value from configuration where configuration_key = 'MODULE_ORDER_TOTAL_TOTAL_STATUS'");
        $this->_check = osc_db_num_rows($check_query);
      }

      return $this->_check;
    }

    function keys() {
      return array('MODULE_ORDER_TOTAL_TOTAL_STATUS',
                   'MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER');
    }

    function install() {
      if ($_SESSION['languages_id'] =='1') {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Affichage du total', 'MODULE_ORDER_TOTAL_TOTAL_STATUS', 'true', 'Voulez-vous montrer le total de la commande ?', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ordre de tri', 'MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER', '4', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier).', '6', '2', now())");
      } else {
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Display Total', 'MODULE_ORDER_TOTAL_TOTAL_STATUS', 'true', 'Do you want to display the total order value?', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
        osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER', '4', 'Sort order of display.', '6', '2', now())");
      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>
