<?php
/**
 * products_quantity_unit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Status of  type of quantity of products');

define('TABLE_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS', 'Status of the type of quantity of products');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_INTRO', 'Thank you for making the necessary changes');
define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_STATUS_NAME', 'Status :');
define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_INTRO', 'Thank you for completing this new type of quantity');
define('TEXT_INFO_PRODUCTS_UNIT_QUANTITY_INTRO', 'Are you sure you want to remove this status ?');
define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS', 'New status ');
define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_DELETE', 'Edit status ');
define('TEXT_INFO_DELETE_INTRO', 'Remove this status');

define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_DELETE', 'Delete this status');
define('TEXT_INFO_DELETE_INTRO','Do you want to delete this quantity type ?');
define('TEXT_INFO_HEADING_PRODUCTS_UNIT_QUANTITY_STATUS', 'new status ');


define('ERROR_REMOVE_DEFAULT_PRODUCTS_UNIT_QUANTITY_STATUS', 'Error: The default status can not be deleted. Thank you for choosing another default status and try again');
define('ERROR_STATUS_USED_IN_PRODUCTS_UNIT_QUANTITY', 'Error : this status is used.');

?>