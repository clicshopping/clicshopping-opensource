<?php
/*
 * login.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

define('HEADING_TITLE', 'Panel d\'Administration');

define('TEXT_USERNAME', 'Pseudonyme:');
define('TEXT_PASSWORD', 'Mot de passe:');
define('TEXT_NAME','Nom de l\'administrateur:');
define('TEXT_FIRSTNAME','Prénenom de l\'administrateur:');

define('TEXT_CREATE_FIRST_ADMINISTRATOR', 'Il n\'y a aucun administrateur actuellement dans la base de données. Veuillez remplir les informations suivantes pour créer un Administrateur. (Une connexion manuelle est requise après cette étape)');

define('HEADING_TITLE_SENT_PASSWORD', 'Récupération mot de passe perdue');
define('TEXT_SENT_PASSWORD', 'Si vous avez oublié votre mot de passe, veuillez insérer votre email ci-dessous afin d\'obtenir un nouveau mot de passe.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND','Aucune adresse email ou pseudo n\'a été trouvé');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' (Panel d\'administration) - Nouveau mot de passe');
define('EMAIL_PASSWORD_REMINDER_BODY', 'Un nouveau mot de passe vous a été envoyé &agrave partir de cette adresse IP : '. $_SERVER['REMOTE_ADDR'] . '.' . "\n\n" . 'Votre nouveau mode passe pour accéder au panneau d\'administration de ' . STORE_NAME . ' est :' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'Succècces: un nouveau de passe a été envoyé &agrave votre adresse email.');
define('BUTTON_SUBMIT', 'Envoyer');
define('TEXT_NEW_TEXT_PASSWORD', 'Nouveau mot de passe');
define('TEXT_ADMINISTRATION_PANEL','Panneau d\'administration');
define('TEXT_EMAIL_LOST_PASSWORD','Votre email');

define('ERROR_INVALID_ADMINISTRATOR', 'Erreur: Connexion invalide, veuillez recommencer.');

define('BUTTON_LOGIN', 'Se connecter');
define('BUTTON_CREATE_ADMINISTRATOR', 'Créer un administrateur');

define('ERROR_ACTION_RECORDER', 'Erreur: Le nombre maximum de login a été atteint. Veuillez recommencer dans %s minutes. Un email a été envoyé à l\'administrateur avec l\'ensemble de vos informations de provenance (géolocalisation). Vous avez droit encore à une tentative avant d\'être définitivement bloqué');


define('REPORT_ACCESS_LOGIN','Quelqu\'un essaye de se connecter a l\'administration de votre boutique de vente en ligne. Il s\'agit peut etre d\'une erreur de connexion. Veuillez vous connecter dans votre administration pour avoir des informations complementaires dans la section Outil / Surveillance des actions. Ci-dessous les elements initiaux a notre connaissance. S\'il y a plusieurs tentatives, l\'adresse sera bloquee pendant un periode de temps');
define ('REPORT_SENDER_IP_ADRESS','Adresse IP : ');
define ('REPORT_SENDER_HOST_NAME','Nom du serveur ou provider : ');
define ('REPORT_SUBJECT_EMAIL','[Rapport] Erreur de connexion pour acceder a votre administration');
define ('REPORT_SENDER_USERNAME','Nom du utilsateur : ');
?>
