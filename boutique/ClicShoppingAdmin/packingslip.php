<?php
/*
 * packingslip.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  define('FPDF_FONTPATH','../includes/javascript/fpdf/font/');
  require('../includes/javascript/fpdf/fpdf.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();

  require('includes/classes/pdf.php');
  require('includes/functions/pdf.php');

  include('includes/classes/order.php');

// Recuperation de la valeur no id de order.php
  if (isset($_GET['oID'])) {
    $oID = (int)$_GET['oID'];
  } else {
    osc_redirect('orders.php');
  }


// Recuperations de la facture

  $QordersInfo = $OSCOM_PDO->prepare('select orders_id, 
                                             customers_id
                                      from :table_orders
                                      where orders_id = :orders_id
                                     ');
  $QordersInfo->bindInt(':orders_id', (int)$oID);
  $QordersInfo->execute();

  $orders_info = $QordersInfo->fetch();

// Recuperations de la date de la facture (Voir aussi french.php & invoice.php)

    $QordersHistory = $OSCOM_PDO->prepare("select orders_status_id, 
                                                   date_added, 
                                                   customer_notified,
                                                   orders_status_invoice_id, 
                                                   comments
                                           from :table_orders_status_history
                                           where orders_id = :orders_id
                                           order by date_added DESC LIMIT 1;
                                          ");
    $QordersHistory->bindInt(':orders_id', (int)$oID );
    $QordersHistory->execute();

    $orders_history = $QordersHistory->fetch();

    $orders_history_display = $orders_history['orders_status_invoice_id'];

// Recuperations du nom du type de facture generee

    $QordersStatusInvoice = $OSCOM_PDO->prepare('select orders_status_invoice_id, 
                                                        orders_status_invoice_name,
                                                        language_id  
                                                 from :table_orders_status_invoice
                                                 where orders_status_invoice_id = :orders_status_invoice_id
                                                 and language_id = :language_id
                                               ');
    $QordersStatusInvoice->bindInt(':orders_status_invoice_id',  (int)$orders_history_display );
    $QordersStatusInvoice->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
    $QordersStatusInvoice->execute();

    $orders_status_invoice = $QordersStatusInvoice->fetch();

    $order_status_invoice_display = $orders_status_invoice['orders_status_invoice_name'];

    $QstatusOrder = $OSCOM_PDO->prepare('select orders_status
                                         from :table_orders
                                         where orders_id = :orders_id
                                       ');
    $QstatusOrder->bindInt(':orders_id',  (int)$oID);
    $QstatusOrder->execute();

    $status_order = $QstatusOrder->fetch();

    $order = new order($oID);

//Instanciation of inherited class
// Classe pdf.php
  $pdf = new PDF();

// Set the Page Margins
// Marge de la page
  $pdf->SetMargins(10,2,6);

// Add the first page
// Ajoute page
  $pdf->AddPage();

//Draw the bottom line with invoice text
// Ligne pour le pied de page
  if (DISPLAY_INVOICE_FOOTER == 'false') {
    $pdf->Cell(50);
    $pdf->SetY(-25);
    $pdf->SetDrawColor(153,153,153);
    $pdf->Cell(185,.1,'',1,1,'L',1);
  }

// Ligne de pliage pour mise en enveloppe
  $pdf->Cell(-5);
  $pdf->SetY(103);
  $pdf->SetX(0);
  $pdf->SetDrawColor(220,220,220);
  $pdf->Cell(3,.1,'',1,1,'',1);

// Cadre pour l'adresse de facturation
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(245);
  $pdf->RoundedRect(6, 40, 90, 35, 2, 'DF');

//Draw the invoice address text
// Adresse de facturation
  $pdf->SetFont('Arial','B',8);
  $pdf->SetTextColor(0);
  $pdf->Text(11,44, ENTRY_SOLD_TO);
  $pdf->SetX(0);
  $pdf->SetY(47);
  $pdf->Cell(9);
  $pdf->MultiCell(70, 3.3, utf8_decode(osc_address_format($order->customer['format_id'], $order->customer, '', '', "\n")),0,'L');

//Draw Box for Delivery Address
// Cadre pour l'adresse de livraison
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(255);
  $pdf->RoundedRect(108, 40, 90, 35, 2, 'DF');

//Draw the invoice delivery address text
// Adresse de livraison
  $pdf->SetFont('Arial','B',8);
  $pdf->SetTextColor(0);
  $pdf->Text(113,44,ENTRY_SHIP_TO);
  $pdf->SetX(0);
  $pdf->SetY(47);
  $pdf->Cell(111);
  $pdf->MultiCell(70, 3.3, utf8_decode(osc_address_format($order->delivery['format_id'], $order->delivery, '', '', "\n")),0,'L');

// Information client
  $pdf->SetFont('Arial','B',8);
  $pdf->SetTextColor(0);
  $pdf->Text(10,85,ENTRY_CUSTOMER_INFORMATION);

//  email
// Email du client
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(15,90,ENTRY_EMAIL . $order->customer['email_address']);

//  Customer Number
// Numero de client
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(15,95,utf8_decode(ENTRY_CUSTOMER_NUMBER) . $orders_info['customers_id']);

//  Customer phone
// Telephone du client
  $pdf->SetFont('Arial','',8);
  $pdf->SetTextColor(0);
  $pdf->Text(15,100, utf8_decode(ENTRY_PHONE) . $order->customer['telephone']);

//Draw Box for Order Number, Date & Payment method
// Cadre du numero de commande, date de commande et methode de paiemenent
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(245);
  $pdf->RoundedRect(6, 107, 192, 11, 2, 'DF');

// Order management
  if (($orders_history['orders_status_invoice_id'] == '1')) {
// Display the order
    $temp = str_replace('&nbsp;', ' ', 'No ' .  $order_status_invoice_display . ' : ');
    $pdf->Text(10,113, $temp . osc_db_input($oID));
  } elseif ($orders_history['orders_status_invoice_id'] == '2') {
//Display the invoice
    $temp = str_replace('&nbsp;', ' ', 'No ' . $order_status_invoice_display . ' : '. $OSCOM_Date->getDateReferenceShort($orders_history['date_added']) . 'S');
    $pdf->Text(10,113, $temp . osc_db_input($oID));
  } elseif ($orders_history['orders_status_invoice_id'] == '3') {
//Display the cancelling
    $temp = str_replace('&nbsp;', ' ', $order_status_invoice_display . ': ');
    $pdf->Text(10,113, $temp);
  } else {
// Display the order
    $temp = str_replace('&nbsp;', ' ', $order_status_invoice_display . ': ');
    $pdf->Text(10,113, $temp . osc_db_input($oID));
  }

// Center information order management
  if (($orders_history['orders_status_invoice_id'] == '1')) {
// Display the order
    $temp = str_replace('&nbsp;', ' ', PRINT_ORDER_DATE . $order_status_invoice_display . ' : ');
    $pdf->Text(75,113,$temp . $OSCOM_Date->getShort($order->info['date_purchased']));
  } elseif ($orders_history['orders_status_invoice_id'] == '2') {
//Display the invoice
    $temp = str_replace('&nbsp;', ' ', PRINT_ORDER_DATE . $order_status_invoice_display . ' : ');
    $pdf->Text(75,113,$temp . $OSCOM_Date->getShort($order->info['date_purchased']));
  } elseif ($orders_history['orders_status_invoice_id'] == '3') {
//Display the cancelling
    $temp = str_replace('&nbsp;', ' ', '');
    $pdf->Text(10,113, $temp);
  } else {
// Display the order
    $temp = str_replace('&nbsp;', ' ', PRINT_ORDER_DATE . $order_status_invoice_display . ' : ');
    $pdf->Text(75,113,$temp . $OSCOM_Date->getShort($order->info['date_purchased']));
  }


//Draw Payment Method Text
  $temp = substr($order->info['payment_method'] , 0, 23);
  $pdf->Text(130,113,ENTRY_PAYMENT_METHOD . ' ' . $temp);

// Cadre pour afficher "BON DE COMMANDE" ou "FACTURE"
  $pdf->SetDrawColor(0);
  $pdf->SetLineWidth(0.2);
  $pdf->SetFillColor(245);
  $pdf->RoundedRect(108, 32, 90, 7, 2, 'DF');

// Affichage titre "BON DE COMMANDE" ou "FACTURE"
  $pdf->SetFont('Arial','',10);
  $pdf->SetY(32);
  $pdf->SetX(108);
  $pdf->MultiCell(90,7,$order_status_invoice_display,0,'C');

// Fields Name position
  $Y_Fields_Name_position = 125;

// Table position, under Fields Name
  $Y_Table_Position = 131;

// Entete du tableau des produits
  output_table_heading_packingslip($Y_Fields_Name_position);

// Boucle sur les produits
// Show the products information line by line
  for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {

// Quantity
    $pdf->SetFont('Arial','',7);
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(6);
    $pdf->MultiCell(14,6,$order->products[$i]['qty'],1,'C');

// Attribut management and Product Name
    $prod_attribs='';

 // Get attribs and concat
    if ((isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0)) {
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        $prod_attribs .=  " - " .$order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
      }
    }
  $product_name_attrib_contact = $order->products[$i]['name'] . $prod_attribs;

//	product name
// Nom du produit
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(60);
    if (strlen($product_name_attrib_contact) > 40 && strlen($product_name_attrib_contact) < 70){
      $pdf->SetFont('Arial','',6);
      $pdf->MultiCell(138,6,utf8_decode($product_name_attrib_contact),1,'L');
    } else if (strlen($product_name_attrib_contact) > 70) {
      $pdf->SetFont('Arial','',6);
      $pdf->MultiCell(138,6,utf8_decode(substr($product_name_attrib_contact,0,70)) ." .. ",1,'L');
    } else {
      $pdf->SetFont('Arial','',6);
      $pdf->MultiCell(138,6,utf8_decode($product_name_attrib_contact),1,'L');
      $pdf->Ln();
    }

// Model
    $pdf->SetY($Y_Table_Position);
    $pdf->SetX(20);
    $pdf->SetFont('Arial','',7);
    $pdf->MultiCell(40,6,utf8_decode($order->products[$i]['model']),1,'C');
    $Y_Table_Position += 6;

// Check for product line overflow
    $item_count++;
    if ((is_long($item_count / 32) && $i >= 20) || ($i == 20)) {
      $pdf->AddPage();
// Fields Name position
      $Y_Fields_Name_position = 125;
// Table position, under Fields Name
      $Y_Table_Position = 70;
      output_table_heading_packingslip($Y_Table_Position-6);
      if ($i == 20) $item_count = 1;
    }
  }

// PDF's created now output the file
  $pdf->Output();
?>