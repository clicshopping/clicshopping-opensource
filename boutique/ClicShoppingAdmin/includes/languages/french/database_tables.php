<?php
/**
 * database_tables.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Table des bases de données');

define('TABLE_HEADING_TABLE', 'Tables');
define('TABLE_HEADING_ROWS', 'Colonnes');
define('TABLE_HEADING_SIZE', 'Taille');
define('TABLE_HEADING_ENGINE', 'Engine');
define('TABLE_HEADING_COLLATION', 'Collation');
define('TABLE_HEADING_MSG_TYPE', 'Type Message');
define('TABLE_HEADING_MSG', 'Message');
define('TABLE_HEADING_QUERIES', 'Requetes');

define('ACTION_CHECK_TABLES', 'Vérification des Tables');
define('ACTION_ANALYZE_TABLES', 'Analyser les Tables');
define('ACTION_OPTIMIZE_TABLES', 'Optimiser les Tables');
define('ACTION_REPAIR_TABLES', 'Repairer les Tables');
define('ACTION_UTF8_CONVERSION', 'Convertir en UTF8');

define('ACTION_UTF8_CONVERSION_FROM_AUTODETECT', 'Auto-Detect');
define('ACTION_UTF8_CONVERSION_FROM', 'de %s');
define('ACTION_UTF8_DRY_RUN', 'Montrer uniquement les requetes ? %s&nbsp;');

?>