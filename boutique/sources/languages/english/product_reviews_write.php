<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Reviews');
define('HEADING_TITLE', 'Do you want bring your comments ?');
define('SUB_TITLE_PRODUCT', 'product :');
define('SUB_TITLE_FROM', 'From:');
define('SUB_TITLE_REVIEW', 'Your Review:');
define('SUB_TITLE_RATING', 'Rating:');

define('TEXT_NO_HTML', '<p class="x-small"><style="color:#ff0000;"><strong>NOTE : </strong>&nbsp;HTML is not translated !</p>');
define('TEXT_BAD', '<p class="x-small"><style="color="#ff0000;"><strong>BAD </strong></p>');
define('TEXT_GOOD', '<p class="x-small"><style="color:#ff0000;"><strong>GOOD </strong></p>');

define('TEXT_CLICK_TO_ENLARGE', 'Click to enlarge');

define('EMAIL_SUBJECT','New comment by a customer on ' . STORE_NAME);
define('EMAIL_TEXT','A new comment has been inserted by a customer concerning a product. You can consult your administration to validate or not this comment');
?>
