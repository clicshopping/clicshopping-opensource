<?php
/*
 * sessions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

  if ( (PHP_VERSION >= 4.3) && ((bool)ini_get('register_globals') == false) ) {
    @ini_set('session.bug_compat_42', 1);
    @ini_set('session.bug_compat_warn', 0);
  }

// Time Session for administration client // modification
  if (STORE_SESSIONS == 'mysql') {

    function _sess_open($save_path, $session_name) {
      return true;
    }

    function _sess_close() {
      return true;
    }

    function _sess_read($key) {
      $value_query = osc_db_query("select value 
                                   from sessions
                                   where sesskey = '" . osc_db_input($key) . "' 
                                  ");
      $value = osc_db_fetch_array($value_query);

      if (isset($value['value'])) {
        return $value['value'];
      }

      return '';
    }

    function _sess_write($key, $value) {

      $check_query = osc_db_query("select 1
                                  from sessions
                                  where sesskey = '" . osc_db_input($key) . "'
                                 ");

      if ( osc_db_num_rows($check_query) > 0 ) {
        $result = osc_db_query("update sessions
                            set expiry = '" . osc_db_input(time()) . "',
                            value = '" . osc_db_input($value) . "'
                            where sesskey = '" . osc_db_input($key) . "'
                          ");
      } else {
        $result =  osc_db_query("insert into sessions
                             values ('" . osc_db_input($key) . "', 
                                     '" . osc_db_input(time()) . "', 
                                     '" . osc_db_input($value) . "')
                            ");
      }

      return $result !== false;
    }

    function _sess_destroy($key) {
      $result =  osc_db_query("delete from sessions where sesskey = '" . osc_db_input($key) . "'");
      return $result !== false;
    }

    function _sess_gc($maxlifetime) {
      $result =  osc_db_query("delete from sessions where expiry < '" . (time() - $maxlifetime) . "'");
      return $result !== false;
    }

    session_set_save_handler('_sess_open', '_sess_close', '_sess_read', '_sess_write', '_sess_destroy', '_sess_gc');
  }

  function osc_session_start() {
    global $_COOKIE;

    $sane_session_id = true;

    if ( isset($_GET[session_name()]) ) {
      if ( (SESSION_FORCE_COOKIE_USE == 'True') || (preg_match('/^[a-zA-Z0-9,-]+$/', $_GET[session_name()]) == false) ) {
        unset($_GET[session_name()]);

        $sane_session_id = false;
      }
    }

    if ( isset($_POST[session_name()]) ) {
      if ( (SESSION_FORCE_COOKIE_USE == 'True') || (preg_match('/^[a-zA-Z0-9,-]+$/', $_POST[session_name()]) == false) ) {
        unset($_POST[session_name()]);

        $sane_session_id = false;
      }
    }

    if ( isset($_COOKIE[session_name()]) ) {
      if ( preg_match('/^[a-zA-Z0-9,-]+$/', $_COOKIE[session_name()]) == false ) {
        $session_data = session_get_cookie_params();

        setcookie(session_name(), '', time()-42000, $session_data['path'], $session_data['domain']);
        unset($_COOKIE[session_name()]);

        $sane_session_id = false;
      }
    }

    if ($sane_session_id == false) {
      osc_redirect(osc_href_link('index.php', '', 'SSL', false));
    }

    register_shutdown_function('session_write_close');

    return session_start();
  }
?>
