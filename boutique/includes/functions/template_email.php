<?php
/**
 * template_email.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


/**
 * the name of the template
 * 
 * @param string  $template_email_id, $language_id
 * @return string $template_email_name['template_name'],  name.of the template email
 * @access public
 */
  function osc_get_template_email_name($template_email_id, $language_id) {
    global $OSCOM_PDO;

    $QtemplateEmail = $OSCOM_PDO->prepare('select template_email_name
                                          from :table_template_email_description
                                          where template_email_id = :template_email_id
                                          and language_id = :language_id
                                         ');
    $QtemplateEmail->bindInt(':template_email_id',(int)$template_email_id);
    $QtemplateEmail->bindInt(':language_id',(int)$language_id);
    $QtemplateEmail->execute();

    $template_email_name = $QtemplateEmail->fetch();

    return $template_email_name['template_email_name'];
  }


/**
 * the template email short description
 * 
 * @param string  $template_email_id, $language_id
 * @return string $template_email['template_short_description'],  the short description of the template email
 * @access public
 */
  function osc_get_template_email_short_description($template_email_id, $language_id) {
    global $OSCOM_PDO;

    $QtemplateEmailShortDescription = $OSCOM_PDO->prepare('select template_email_short_description
                                                          from :table_template_email_description
                                                          where template_email_id = :template_email_id
                                                          and language_id = :language_id
                                                         ');
    $QtemplateEmailShortDescription->bindInt(':template_email_id',(int)$template_email_id);
    $QtemplateEmailShortDescription->bindInt(':language_id',(int)$language_id);
    $QtemplateEmailShortDescription->execute();

    $template_email_short_description = $QtemplateEmailShortDescription->fetch();

    return $template_email_short_description['template_email_short_description'];
  }

/**
 * the template email description who is sent
 * 
 * @param string  $template_email_id, $language_id
 * @return string $template_email['template_email_description'],  the description of the template email who is sent
 * @access public
 */
  function osc_get_template_email_description($template_email_id, $language_id) {
    global $OSCOM_PDO;

    $QtemplateEmailDescription = $OSCOM_PDO->prepare('select template_email_description
                                                      from :table_template_email_description
                                                      where template_email_id = :template_email_id
                                                      and language_id = :language_id
                                                     ');
    $QtemplateEmailDescription->bindInt(':template_email_id',(int)$template_email_id);
    $QtemplateEmailDescription->bindInt(':language_id',(int)$language_id);
    $QtemplateEmailDescription->execute();

    $template_email_description = $QtemplateEmailDescription->fetch();

    return $template_email_description['template_email_description'];
  }

/**
 * the footer of email
 * 
 * @param string  $template_email_footer
 * @return string $template_email_footer,  the footer of the email template who is sent
 * @access public
 */
  function osc_get_template_email_text_footer($template_email_footer) {
    global $OSCOM_PDO;

    $QtextTemplateEmailFooter = $OSCOM_PDO->prepare('select te.template_email_variable,
                                                            ted.template_email_description
                                                     from :table_template_email te,
                                                          :table_template_email_description  ted
                                                     where te.template_email_variable = :template_email_variable
                                                     and te.template_email_id = ted.template_email_id
                                                     and ted.language_id = :language_id
                                                    ');

    $QtextTemplateEmailFooter->bindValue(':template_email_variable', 'TEMPLATE_EMAIL_TEXT_FOOTER');
    $QtextTemplateEmailFooter->bindInt(':language_id',(int)$_SESSION['languages_id']);
    $QtextTemplateEmailFooter->execute();

    $text_template_email_footer = $QtextTemplateEmailFooter->fetch();

    $template_email_footer = $text_template_email_footer['template_email_description'];

            $keywords = array('/%%STORE_NAME%%/',
                              '/%%STORE_OWNER_EMAIL_ADDRESS%%/',
                              '/%%HTTP_CATALOG%%/'
                             );

    $replaces = array(STORE_NAME,
                      STORE_OWNER_EMAIL_ADDRESS,
                      HTTP_SERVER . DIR_WS_HTTP_CATALOG
                      );


    $template_email_footer = preg_replace($keywords, $replaces, $template_email_footer);

    return $template_email_footer;
  } 


/**
 * the signature of email
 * 
 * @param string  $template_email_signature
 * @return string $template_email_signature,  the signature of the email template who is sent
 * @access public
 */
   function osc_get_template_email_signature($template_email_signature) {
     global $OSCOM_PDO;

     $QtextTemplateEmailSignature = $OSCOM_PDO->prepare('select te.template_email_variable,
                                                            ted.template_email_description
                                                     from :table_template_email te,
                                                          :table_template_email_description  ted
                                                     where te.template_email_variable = :template_email_variable
                                                     and te.template_email_id = ted.template_email_id
                                                     and ted.language_id = :language_id
                                                    ');

     $QtextTemplateEmailSignature->bindValue(':template_email_variable', 'TEMPLATE_EMAIL_SIGNATURE');
     $QtextTemplateEmailSignature->bindInt(':language_id',(int)$_SESSION['languages_id']);
     $QtextTemplateEmailSignature->execute();

     $text_template_email_signature = $QtextTemplateEmailSignature->fetch();

     $template_email_signature = $text_template_email_signature['template_email_description'];

     $keywords = array('/%%STORE_NAME%%/',
                        '/%%STORE_OWNER_EMAIL_ADDRESS%%/',
                        '/%%HTTP_CATALOG%%/'

                        );

     $replaces = array(STORE_NAME,
                       STORE_OWNER_EMAIL_ADDRESS,
                       HTTP_SERVER . DIR_WS_HTTP_CATALOG
                       );

    $template_email_signature = preg_replace($keywords, $replaces, $template_email_signature);

    return $template_email_signature;
  }


/**
 * the template email welcome catalog who is sent
 * 
 * @param string  $template_email_welcome_admin
 * @return string $template_email_welcome_admin,  the description of the template email welcome admin who is sent
 * @access public
 */
   function osc_get_template_email_welcome_catalog($template_email_welcome_catalog) {
     global $OSCOM_PDO;

     $QtextTemplateEmailWelcomeCatalog = $OSCOM_PDO->prepare('select te.template_email_variable,
                                                                      ted.template_email_description
                                                               from :table_template_email te,
                                                                    :table_template_email_description  ted
                                                               where te.template_email_variable = :template_email_variable
                                                               and te.template_email_id = ted.template_email_id
                                                               and ted.language_id = :language_id
                                                              ');

     $QtextTemplateEmailWelcomeCatalog->bindValue(':template_email_variable', 'TEMPLATE_EMAIL_WELCOME');
     $QtextTemplateEmailWelcomeCatalog->bindInt(':language_id',(int)$_SESSION['languages_id']);
     $QtextTemplateEmailWelcomeCatalog->execute();

     $text_template_email_welcome_catalog = $QtextTemplateEmailWelcomeCatalog->fetch();

     $template_email_welcome_catalog = $text_template_email_welcome_catalog['template_email_description'];

     $keywords = array('/%%STORE_NAME%%/',
                      '/%%STORE_OWNER_EMAIL_ADDRESS%%/',
                      '/%%HTTP_CATALOG%%/'
                      );

     $replaces = array(STORE_NAME,
                      STORE_OWNER_EMAIL_ADDRESS,
                      HTTP_SERVER . DIR_WS_HTTP_CATALOG
                      );

     $template_email_welcome_catalog = preg_replace($keywords, $replaces, $template_email_welcome_catalog);

     return $template_email_welcome_catalog;
   }


/**
 * the template email coupon who is sent
 * 
 * @param string  $template_email_coupon_admin
 * @return string $template_email_coupon_admin,  the description of the template email coupon who is sent
 * @access public
 */
  function osc_get_template_email_coupon_catalog($template_email_coupon_catalog) {
    global $OSCOM_PDO;

    $QtextTemplateEmailCouponCatalog = $OSCOM_PDO->prepare('select te.template_email_variable,
                                                                      ted.template_email_description
                                                               from :table_template_email te,
                                                                    :table_template_email_description  ted
                                                               where te.template_email_variable = :template_email_variable
                                                               and te.template_email_id = ted.template_email_id
                                                               and ted.language_id = :language_id
                                                              ');

    $QtextTemplateEmailCouponCatalog->bindValue(':template_email_variable', 'TEMPLATE_EMAIL_TEXT_COUPON');
    $QtextTemplateEmailCouponCatalog->bindInt(':language_id',(int)$_SESSION['languages_id']);
    $QtextTemplateEmailCouponCatalog->execute();

    $text_template_email_coupon_catalog = $QtextTemplateEmailCouponCatalog->fetch();

    $template_email_coupon_catalog = $text_template_email_coupon_catalog['template_email_description'];

    $keywords = array('/%%STORE_NAME%%/',
                      '/%%STORE_OWNER_EMAIL_ADDRESS%%/',
                      '/%%HTTP_CATALOG%%/'
                      );

    $replaces = array(STORE_NAME,
                      STORE_OWNER_EMAIL_ADDRESS,
                      HTTP_SERVER . DIR_WS_HTTP_CATALOG
                      );

    $template_email_coupon_catalog = preg_replace($keywords, $replaces, $template_email_coupon_catalog);

    return $template_email_coupon_catalog;
  }

/**
 * the template order intro command who is sent
 * 
 * @param string  $template_email_intro_command
 * @return string $template_email_intro_command,  the description of the template email order intro command who is sent
 * @access public
 */
  function osc_get_template_email_intro_command($template_email_intro_command) {
    global $OSCOM_PDO;

    $QtextTemplateEmailIntroCommand = $OSCOM_PDO->prepare('select te.template_email_variable,
                                                                  ted.template_email_description
                                                           from :table_template_email te,
                                                                :table_template_email_description  ted
                                                           where te.template_email_variable = :template_email_variable
                                                           and te.template_email_id = ted.template_email_id
                                                           and ted.language_id = :language_id
                                                          ');

    $QtextTemplateEmailIntroCommand->bindValue(':template_email_variable', 'TEMPLATE_EMAIL_INTRO_COMMAND');
    $QtextTemplateEmailIntroCommand->bindInt(':language_id',(int)$_SESSION['languages_id']);
    $QtextTemplateEmailIntroCommand->execute();

    $text_template_email_intro_command = $QtextTemplateEmailIntroCommand->fetch();

    $template_email_intro_command = $text_template_email_intro_command['template_email_description'];

    $keywords = array('/%%STORE_NAME%%/',
                      '/%%STORE_OWNER_EMAIL_ADDRESS%%/',
                      '/%%HTTP_CATALOG%%/'
                      );

    $replaces = array(STORE_NAME,
                      STORE_OWNER_EMAIL_ADDRESS,
                      HTTP_SERVER . DIR_WS_HTTP_CATALOG
                      );

    $template_email_intro_command = preg_replace($keywords, $replaces, $template_email_intro_command);

    return $template_email_intro_command;
  }
