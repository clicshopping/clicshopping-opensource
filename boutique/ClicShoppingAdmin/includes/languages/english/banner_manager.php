<?php
/**
 * banner_manager.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

// FCKeditor
define('ALERTE_NOTICE_IMAGE', 'Not to put more than one image.');
define('NOTICE_IMAGE', 'If an image already exists, want initially to select it before choosing another of them.');
define('TEXT_DELETE_IMAGE', 'To remove the currently visible image');

define('HEADING_TITLE', 'Banner Manager');
define('HEADING_TITLE_SEARCH','Search');

define('TABLE_HEADING_BANNERS', 'Banners Catalog');
define('TABLE_HEADING_BANNERS_ADMIN', 'Banners Admin');
define('TABLE_HEADING_GROUPS', 'Groups');
define('TABLE_HEADING_STATISTICS', 'Displays / Clicks');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_CUSTOMERS_GROUP','Customers group');
define('TABLE_HEADING_LANGUAGE','languages');

define('TEXT_BANNERS_TITLE', 'Catalog Banner Title:');
define('TEXT_BANNERS_TITLE_ADMIN', 'Administration Banner Title:');
define('TEXT_BANNERS_URL', 'URL de redirection:');
define('TEXT_BANNERS_GROUP', 'Group:');
define('TEXT_BANNERS_TARGET', 'Mouse Click Reaction:');
define('TEXT_BANNERS_SAME_WINDOWS','Same window');
define('TEXT_BANNERS_NEW_WINDOWS','New window');
define('TEXT_BANNERS_NEW_GROUP', 'New group:');

define('TEXT_BANNERS_CUSTOMERS_GROUP','Display for a client group');
define('TEXT_BANNERS_LANGUAGE','Display language');
define('TEXT_ALL_LANGUAGES','All languages');
define('NORMAL_CUSTOMER','Normal Customers');
define('TEXT_ALL_CUSTOMERS','Normal Customers');


define('TEXT_BANNERS_IMAGE', 'Image of the banner');
define('TEXT_BANNERS_IMAGE_LOCAL', 'Image local:');
define('TEXT_BANNERS_IMAGE_TARGET', 'File of save:');
define('TEXT_BANNERS_IMAGE_DELETE', 'Not display this image on this banner');
define('TEXT_BANNERS_HTML_TEXT', 'HTML Code:');
define('TEXT_BANNERS_EXPIRES_ON', 'Expires On:');
define('TEXT_BANNERS_OR_AT', 'or');
define('TEXT_BANNERS_IMPRESSIONS', 'impressions/views.');
define('TEXT_BANNERS_SCHEDULED_AT', 'Scheduled At:');
define('TEXT_BANNERS_BANNER_NOTE', '<li>Use an image or HTML text for the banner - not both.</li><li>HTML Text has priority over an image</li>');
define('TEXT_BANNERS_INSERT_NOTE', '<li>To activate wysiwyg, you must click twice on Source button for Firefox users.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock +)</li><li>HTML Text has priority over an image</li>');
define('TEXT_BANNERS_EXPIRCY_NOTE', '<u>Expire Notes</u> :<ul><li>Only one of the two fields should be submited</li><li>If the banner is not to expire automatically, then leave these fields blank</li></ul>');
define('TEXT_BANNERS_SCHEDULE_NOTE', '<u>Schedule Notes</u> :<ul><li>If a schedule is set, the banner will be activated on this date.</li><li>All scheduled banners are marked as desactive until their date has arrived, to which they will then be marked active.</li><li>The banners are automatically updated (the store catalog manages this element), it is unnecessary to change the status of the banner after registration.</li></ul>');

define('TEXT_BANNERS_DATE_ADDED', 'Date Added:');
define('TEXT_BANNERS_SCHEDULED_AT_DATE', 'Scheduled At: <strong>%s</strong>');
define('TEXT_BANNERS_EXPIRES_AT_DATE', 'Expires At: <strong>%s</strong>');
define('TEXT_BANNERS_EXPIRES_AT_IMPRESSIONS', 'Expires At: <strong>%s</strong> impressions');
define('TEXT_BANNERS_STATUS_CHANGE', 'Status Change: %s');

define('TEXT_BANNERS_DATA', 'D<br />A<br />T<br />A');
define('TEXT_BANNERS_LAST_3_DAYS', 'Last 3 Days');
define('TEXT_BANNERS_BANNER_VIEWS', 'Banner Views');
define('TEXT_BANNERS_BANNER_CLICKS', 'Banner Clicks');

define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this banner?');
define('TEXT_INFO_DELETE_IMAGE', 'Delete banner image');

define('SUCCESS_BANNER_INSERTED', 'Success: The banner has been inserted.');
define('SUCCESS_BANNER_UPDATED', 'Success: The banner has been updated.');
define('SUCCESS_BANNER_REMOVED', 'Success: The banner has been removed.');
define('SUCCESS_BANNER_STATUS_UPDATED', 'Success: The status of the banner has been updated.');

define('ERROR_BANNER_TITLE_REQUIRED', 'Error: Banner title required.');
define('ERROR_BANNER_GROUP_REQUIRED', 'Error: Banner group required.');
define('ERROR_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: Target directory does not exist: %s');
define('ERROR_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: Target directory is not writeable: %s');
define('ERROR_IMAGE_DOES_NOT_EXIST', 'Error: Image does not exist.');
define('ERROR_IMAGE_IS_NOT_WRITEABLE', 'Error: Image can not be removed.');
define('ERROR_UNKNOWN_STATUS_FLAG', 'Error: Unknown status flag.');

define('ERROR_GRAPHS_DIRECTORY_DOES_NOT_EXIST', 'Error: Graphs directory does not exist. Please create a \'graphs\' directory inside \'images\'.');
define('ERROR_GRAPHS_DIRECTORY_NOT_WRITEABLE', 'Error: Graphs directory is not writeable.');

define('TITLE_BANNERS_GENERAL', 'General information');
define('TITLE_BANNERS_GROUPE', 'Group');
define('TITLE_BANNERS_DATE', 'Date');
define('TITLE_BANNERS_IMAGE', 'Image banner');
define('TITLE_BANNERS_HTML', 'Banner HTML code');

define('TITLE_AIDE_BANNERS_IMAGE', 'Note on the banner image');
define('TITLE_AIDE_BANNERS_DATE', 'Note on the use of the dates of a banner');
define('TITLE_AIDE_BANNERS_HTML_TEXT', 'Note on code HTML of a banner');

define('ICON_VIEW_BANNER', 'View banner');
define('SET_INACTIVE','Inactive');
define('SET_ACTIVE','Active');
?>