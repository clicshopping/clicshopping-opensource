<?php
/**
  *
  * @copyright Copyright 2008 - http://www.e-imaginis.com
  * @copyright Portions Copyright 2003 osCommerce
  * @license GNU Public License V2.0
  * @version $Id:
*/


// close session (store variables)
  session_write_close();

  if (STORE_PAGE_PARSE_TIME == 'true') {
    if (!is_object($logger)) $logger = new logger;
    echo $logger->timer_stop(DISPLAY_PAGE_PARSE_TIME);
  }
?>