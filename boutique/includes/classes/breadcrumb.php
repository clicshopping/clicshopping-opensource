<?php
/**
 * breadcrumb.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class breadcrumb {
    protected $_trail;

    public function __construct() {
      $this->reset();
    }

    public function reset() {
      $this->_trail = array();
    }

    public function add($title, $link = null) {
      $this->_trail[] = array('title' => $title, 'link' => $link);
    }

    public function get($separator = null) {

      $breadcrumb_count = 1;
      $trail_string = '<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

      for ($i=0, $n=sizeof($this->_trail); $i<$n; $i++) {
        if (isset($this->_trail[$i]['link']) && osc_not_null($this->_trail[$i]['link'])) {
          $trail_string .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $this->_trail[$i]['link'] . '" itemprop="item"><span itemprop="name">' . $this->_trail[$i]['title'] . '</span></a><meta itemprop="position" content="' . $breadcrumb_count . '" /></li>' . "\n";
        } else {
          $trail_string .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . $this->_trail[$i]['title'] . '</span><meta itemprop="position" content="' . $breadcrumb_count . '" /></li>';
        }
        $breadcrumb_count++;
      }

      $trail_string .= '</ol>';
      return $trail_string;
    }
  }
