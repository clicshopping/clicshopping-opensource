<?php 
/*
 * create_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
<div class="contentContainer">
  <div class="contentText"><?php  require($OSCOM_Template->getTemplateModules('products_listing/index_listing')); ?></div>
<?php
    if ($category_depth == 'nested') {
?>
  <div class="contentText"><?php echo $category['categories_description']; ?></div>
<?php 
    } elseif (isset($_GET['manufacturers_id']) ) {
?>
  <div class="contentText"><?php echo $manufacturer_description; ?></div>
<?php
    }
?>

  <div class="contentContainer">
    <?php echo $OSCOM_Template->getBlocks('modules_products_listing'); ?>
  </div>

</div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>