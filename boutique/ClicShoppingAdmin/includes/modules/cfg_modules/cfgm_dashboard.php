<?php
  /**
   * cfm_modules_dashboard.php
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
   */

  class cfgm_dashboard {
    var $code = 'dashboard';
    var $directory;
    var $language_directory;
    var $key = 'MODULE_ADMIN_DASHBOARD_INSTALLED';
    var $title;
    var $template_integration = false;

    function cfgm_dashboard() {
      $this->directory = DIR_FS_ADMIN . 'includes/modules/dashboard/';
      $this->language_directory = DIR_FS_ADMIN . 'includes/languages/';
      $this->title = MODULE_CFG_MODULE_DASHBOARD_TITLE;
    }
  }
?>
