<?php

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'fr_FR.ISO_8859-1');
@setlocale(LC_TIME, 'fr_FR.utf8');

define('DATE_FORMAT_SHORT', '%d/%m/%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
define('DATE_FORMAT', 'd/m/Y'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('JQUERY_BOOTSTRAP_FORMAT', 'dd/mm/yyyy');
//define('DOB_FORMAT_STRING', 'jj/mm/aaaa');
////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function osc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 0, 2) . substr($date, 3, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 3, 2) . substr($date, 0, 2);
  }
}

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'EUR');

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="ltr" lang="fr"');

// language of site
define('HTML_LANG', 'fr');

// charset for web pages and emails
define('CHARSET', 'UTF-8');


// Modification référence & design facture
// Y = Annee / m = mois / d = jours  (Voir aussi fonction getDateReferenceShort() dans general.php)
define('DATE_FORMAT_REFERENCE', 'Ym'); 

// page title
define('TITLE', STORE_NAME);
define('ACCESS_CATALOG', 'Accéder à la boutique '.STORE_NAME.'');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Créer un compte');
define('HEADER_TITLE_MY_ACCOUNT', 'Mon compte');
define('HEADER_TITLE_CART_CONTENTS', 'Mon panier');
define('HEADER_TITLE_CHECKOUT', 'Commander');
define('HEADER_TITLE_TOP', 'Accueil');
define('HEADER_TITLE_CATALOG', ''. STORE_NAME .'');
define('HEADER_TITLE_LOGOFF', 'Déconnexion');
define('HEADER_TITLE_LOGIN', 'Connexion');
define('HEADER_TITLE_NEWSLETTER','Newsletter');
define('HEADER_TITLE_INDEX','Accueil');
define('HEADER_TITLE_SPECIALS','Promotions');
define('HEADER_TITLE_PRODUCTS_NEW','Nouveautés');
define('HEADER_TITLE_PRODUCTS_HEART','Coups de coeur');
define('HEADER_TITLE_CONTACT_US','Nous contacter');

// footer text in includes/footer.php
define('FOOTER_TEXT_REQUESTS_SINCE', 'requêtes depuis le');

//Displey an attribut before the product model if customer id is different 0
define('PRODUCTS_GROUP_QUANTITY_UNIT_TITLE','FRS-');

// text for gender
define('MALE', 'Mr.');
define('FEMALE', 'Mme.');
define('MALE_ADDRESS', 'Mr.');
define('FEMALE_ADDRESS', 'Mme.');

// text for date of birth example
define('DOB_FORMAT_STRING', 'jj/mm/aaaa');

// categories box text in includes/boxes/categories.php
define('BOX_HEADING_CATEGORIES', 'Catégories');

// manufacturers box text in includes/boxes/manufacturers.php
define('BOX_HEADING_MANUFACTURERS', 'Marques');

// whats_new box text in includes/boxes/whats_new.php
define('BOX_HEADING_WHATS_NEW', 'Nouveautés');

// quick_find box text in includes/boxes/quick_find.php
define('BOX_HEADING_SEARCH', 'Rechercher');
define('BOX_SEARCH_TEXT', 'Recherche rapide');
define('BOX_SEARCH_ADVANCED_SEARCH', 'Recherche avancée');

// specials box text in includes/boxes/specials.php
define('BOX_HEADING_SPECIALS', 'Promotions');

// reviews box text in includes/boxes/reviews.php
define('BOX_HEADING_REVIEWS', 'Commentaires');
define('BOX_REVIEWS_WRITE_REVIEW', 'Ecrire un commentaire!');
define('BOX_REVIEWS_NO_REVIEWS', 'Il n\'y a pas encore de commentaires ou celui-ci est en cours de validation.');
define('BOX_REVIEWS_TEXT_OF_5_STARS', '%s sur 5 étoiles !');

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_HEADING_SHOPPING_CART', 'Mon panier');
define('BOX_SHOPPING_CART_EMPTY', ' article(s)');

// order_history box text in includes/boxes/order_history.php
define('BOX_HEADING_CUSTOMER_ORDERS', 'Historique commandes');

// best_sellers box text in includes/boxes/best_sellers.php
define('BOX_HEADING_BESTSELLERS', 'Meilleures ventes');
define('BOX_HEADING_BESTSELLERS_IN', 'Meilleures ventes dans<br />&nbsp;&nbsp;');

// notifications box text in includes/boxes/products_notifications.php
define('BOX_HEADING_NOTIFICATIONS', 'Surveillance Produit');
define('BOX_NOTIFICATIONS_NOTIFY', 'Surveiller <strong>%s</strong>');
define('BOX_NOTIFICATIONS_NOTIFY_REMOVE', 'Ne pas m\'informer d\'un changement <strong>%s</strong>');

// manufacturer box text
define('BOX_HEADING_MANUFACTURER_INFO', 'Information sur la marque');
define('BOX_MANUFACTURER_INFO_HOMEPAGE', 'Page d\'accueil de %s');
define('BOX_MANUFACTURER_INFO_OTHER_PRODUCTS', 'Autres articles');

// languages box text in includes/boxes/languages.php
define('BOX_HEADING_LANGUAGES', 'Langues');

// currencies box text in includes/boxes/currencies.php
define('BOX_HEADING_CURRENCIES', 'Devises');

// information box text in includes/boxes/information.php
define('BOX_HEADING_INFORMATION', 'Informations');
define('BOX_INFORMATION_PRIVACY', 'Politique de Confidentialité');
define('BOX_INFORMATION_CONDITIONS', 'Conditions Générales');
define('BOX_INFORMATION_SHIPPING', 'Expéditions et retours');
define('BOX_INFORMATION_CONTACT', 'Contactez-nous');

// social bookmarks box in includes/boxes/product_social_bookmarks.php
define('BOX_HEADING_SOCIAL_BOOKMARKS', 'Partager ce produit avec vos amis');

// product_heart
define('BOX_HEADING_PRODUCTS_HEART', 'Coups de coeur');

//Sitemap
define('BOX_HEADING_SITEMAP', 'Cartographie du site');

// checkout procedure text
define('CHECKOUT_BAR_CART_CONTENTS', 'Contenu du Panier');
define('CHECKOUT_BAR_DELIVERY', 'Information livraison');
define('CHECKOUT_BAR_DELIVERY_ADDRESS', 'Adresse de Livraison');
define('CHECKOUT_BAR_PAYMENT_METHOD', 'Méthode de Paiement');
define('CHECKOUT_BAR_PAYMENT', 'Information paiement');
define('CHECKOUT_BAR_CONFIRMATION', 'Confirmation');
define('CHECKOUT_BAR_FINISHED', 'Fini !');

// pull down default text
define('PULL_DOWN_DEFAULT', 'Choisissez');
define('PLEASE_SELECT', 'Merci de sélectionner');
define('TYPE_BELOW', 'Ecrire ci-dessous');

// Secured payment
define('BOX_HEADING_PAYMENT', 'Infos Paiement');
define('SSLBOX_CONTENT','Connexion sécurisée SSL 128 bits activée');
define('BOX_HEADING_PAYMENT_SPPLUS', 'Le paiement en ligne est sécurisé par :');

// Politique de confidentialité
define('ERROR_CONDITIONS_NOT_ACCEPTED', 'Veuillez confirmer que vous acceptez les termes et les conditions de vente.');

// javascript messages
define('JS_ERROR', 'Des erreurs sont survenues durant le traitement de votre formulaire.\n\nVeuillez effectuer les corrections suivantes :\n\n');

define('JS_REVIEW_TEXT', '* Le \'commentaire\' que vous avez rentré doit avoir au moins ' . REVIEW_TEXT_MIN_LENGTH . ' caracteres.\n');
define('JS_REVIEW_RATING', '* Vous devez mettre une appréciation pour cet article.\n');

define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* Veuillez choisir une méthode de paiement pour votre commande.\n');

define('JS_ERROR_SUBMITTED', 'Ce formulaire a été déjà soumis. Veuillez appuyer sur Ok et attendez jusqu\'à ce que le traitement soit fini.');

define('ERROR_NO_PAYMENT_MODULE_SELECTED', 'Veuillez choisir une méthode de paiement pour votre commande.');

define('CATEGORY_PERSONAL', 'Vos informations');
define('CATEGORY_COMPANY', 'Détail sur votre société');
define('CATEGORY_ADDRESS', 'Votre adresse');
define('CATEGORY_CONTACT', 'Contact téléphonique');
define('CATEGORY_OPTIONS', 'Options');
define('CATEGORY_PASSWORD', 'Votre mot de passe');

define('ENTRY_COMPANY', 'Nom de la société :');
define('ENTRY_COMPANY_ERROR', 'Le nom de la societe doit contenir un minimum de ' . ENTRY_COMPANY_MIN_LENGTH . 'caracteres.');
define('ENTRY_COMPANY_ERROR_PRO', 'Le nom de la societe doit contenir un minimum de ' . ENTRY_COMPANY_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_COMPANY_TEXT', '*');
define('ENTRY_COMPANY_TEXT_PRO', '*');
define('ENTRY_SIRET', 'No d\'immatriculation société :');
define('ENTRY_SIRET_ERROR', 'Le numero d\'immatriculation de votre societe doit contenir un minimum de ' . ENTRY_SIRET_MIN_LENGTH .' caracteres.');
define('ENTRY_SIRET_TEXT', '*');
define('ENTRY_SIRET_EXEMPLE', '(ex : No RCS pour les sociétés francaises)');
define('ENTRY_TVA_INTRACOM', 'No de TVA Intracom :');
define('ENTRY_TVA_INTRACOM_ERROR', 'Le numero de TVA Intracom de votre societe doit contenir un minimum de ' . ENTRY_TVA_INTRACOM_MIN_LENGTH .' caracteres.');
define('ENTRY_TVA_INTRACOM_TEXT', '* Sélectionnez le pays en premier.');
define('ENTRY_CODE_APE', 'Numero de nomenclature :');
define('ENTRY_CODE_APE_ERROR', 'Le numero de code nomenclature de votre societe doit contenir un minimum de ' . ENTRY_CODE_APE_MIN_LENGTH .' caracteres.');
define('ENTRY_CODE_APE_TEXT', '*');
define('ENTRY_CODE_APE_EXEMPLE', '(ex : Code APE pour les sociétés francaises)');
define('ENTRY_TAXE_PROVINCIALE','Numéro de la Taxe Provinciale : ');
define('ENTRY_CODE_TAXE_PROVINCIALE_ERROR', 'Le code Taxe Provinciale de votre societe doit contenir un minimum de ' . ENTRY_CODE_TAXE_PROVINCIALE_MIN_LENGTH .' caracteres.');
define('ENTRY_CODE_TAXE_PROVINCIALE_TEXT', '*');
define('ENTRY_TAXE_FEDERALE','Numéro de la Taxe Fédérale : ');
define('ENTRY_CODE_TAXE_FEDERALE_ERROR', 'Le code Taxe Fédérale de votre societe doit contenir un minimum de ' . ENTRY_CODE_TAXE_FEDERALE_MIN_LENGTH .' caracteres.');
define('ENTRY_CODE_TAXE_FEDERALE_TEXT', '*');
define('ENTRY_GENDER', 'Genre :');
define('ENTRY_GENDER_ERROR', 'Vous devez sélectionner votre civilité homme ou femme.');
define('ENTRY_GENDER_ERROR_PRO', 'Vous devez sélectionner votre civilité homme ou femme.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME', 'Prénom :');
define('ENTRY_FIRST_NAME_ERROR', 'Votre prenom doit contenir un minimum de ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caracteres.');
define('ENTRY_FIRST_NAME_ERROR_PRO', 'Votre prenom doit contenir un minimum de ' . ENTRY_FIRST_NAME_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Nom :');
define('ENTRY_LAST_NAME_ERROR', 'Votre nom doit contenir un minimum de ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caracteres.');
define('ENTRY_LAST_NAME_ERROR_PRO', 'Votre nom doit contenir un minimum de ' . ENTRY_LAST_NAME_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Date de naissance :');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Votre date de naissance doit avoir ce format : JJ/MM/AAAA (exemple : 21/08/2001)');
define('ENTRY_DATE_OF_BIRTH_ERROR_PRO', 'Votre date de naissance doit avoir ce format : JJ/MM/AAAA (exemple : 21/08/2001)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* Format : JJ/MM/AAAA');
define('ENTRY_EMAIL_ADDRESS', 'Adresse email :');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Votre adress email n\'est pas correcte.');
define('ENTRY_EMAIL_ADDRESS_CONFIRM', 'Confirmez votre email :');
define('ENTRY_EMAIL_ADDRESS_CONFIRM_NOT_MATCHING', 'L\'email de confirmation ne correspond pas votre email');
define('ENTRY_EMAIL_ADDRESS_ERROR_PRO', 'Votre adresse email doit contenir un minimum de ' . ENTRY_EMAIL_ADDRESS_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Votre adresse email ne semble pas être valide - veuillez effectuer toutes les corrections nécessaires.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR_NUMBER', 'Vous avez fait une erreur concernant la formule de calcul apposée au bas du formulaire.<br />Veuillez effectuer toutes les corrections nécessaires.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR_PRO', 'Votre adresse email ne semble pas être valide - veuillez effectuer toutes les corrections nécessaires.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Votre adresse email est déjà enregistrée sur notre site - Veuillez ouvrir une session avec cette adresse email ou créez un compte avec une adresse différente.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS_PRO', 'Votre adresse email est déjà enregistrée sur notre site - Veuillez ouvrir une session avec cette adresse email ou créez un compte avec une adresse différente.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION', 'Email de confirmation :');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION_TEXT', '* Confirmez votre adresse email.');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION_CHECK_ERROR', 'Vous n\'avez pas saisie une adresse e-mail identique lors de la confirmation.');
define('ENTRY_EMAIL_ADDRESS_CONFIRMATION_CHECK_ERROR_PRO', 'Vous n\'avez pas saisie une adresse e-mail identique lors de la confirmation.');
define('ENTRY_STREET_ADDRESS', 'Adresse :');
define('ENTRY_STREET_ADDRESS_ERROR', 'Votre adresse postal doit contenir un minimum de ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caracteres.');
define('ENTRY_STREET_ADDRESS_ERROR_PRO', 'Votre adresse postal doit contenir un minimum de ' . ENTRY_STREET_ADDRESS_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB', 'Complément d\'adresse :');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Code postal :');
define('ENTRY_POST_CODE_ERROR', 'Votre code postal doit contenir un minimum de ' . ENTRY_POSTCODE_MIN_LENGTH . ' caracteres.');
define('ENTRY_POST_CODE_ERROR_PRO', 'Votre code postal doit contenir un minimum de ' . ENTRY_POSTCODE_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Ville: ');
define('ENTRY_CITY_ERROR', 'Votre ville doit contenir un minimum de ' . ENTRY_CITY_MIN_LENGTH . ' caracteres.');
define('ENTRY_CITY_ERROR_PRO', 'Votre ville doit contenir un minimum de ' . ENTRY_CITY_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Etat/Département :');
define('ENTRY_STATE_ERROR', 'Le departement ou l\'Etat doit contenir un minimum de ' . ENTRY_STATE_MIN_LENGTH . ' caracteres.');
define('ENTRY_STATE_ERROR_SELECT', 'Veuillez choisir un état à partir de la liste déroulante.');
define('ENTRY_STATE_ERROR_PRO', 'Le departement ou état doit contenir un minimum de ' . ENTRY_STATE_PRO_MIN_LENGTH . 'caracteres.');
define('ENTRY_STATE_ERROR_SELECT_PRO', 'Veuillez choisir un état à partir de la liste déroulante.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Pays :');
define('ENTRY_COUNTRY_ERROR', 'Veuillez choisir un pays à partir de la liste déroulante.');
define('ENTRY_COUNTRY_ERROR_PRO', 'Veuillez choisir un pays à partir de la liste déroulante.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Numéro de téléphone :');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Votre numero de telephone doit contenir un minimum de ' . ENTRY_TELEPHONE_MIN_LENGTH . ' caracteres.');
define('ENTRY_TELEPHONE_NUMBER_ERROR_PRO', 'Votre numero de telephone doit contenir un minimum de ' . ENTRY_TELEPHONE_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_CELLULAR_PHONE_NUMBER', 'Numéro du téléphone portable:');
define('ENTRY_CELLULAR_PHONE_NUMBER_ERROR', 'Votre numero de telephone portable doit contenir un minimum de ' . ENTRY_CELLULAR_PHONE_MIN_LENGTH . ' caracteres.');
define('ENTRY_CELLULAR_PHONE_NUMBER_ERROR_PRO', 'Votre numero de telephone portable doit contenir un minimum de ' . ENTRY_CELLULAR_PHONE_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_CELLULAR_PHONE_NUMBER_TEXT', '');
define('ENTRY_FAX_NUMBER', 'Numéro de fax :');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_MESSAGE_TEXT','*');
define('ENTRY_NEWSLETTER', 'Lettre d\'information :');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'S\'abonner');
define('ENTRY_NEWSLETTER_NO', 'Ne pas s\'abonner');
define('ENTRY_NEWSLETTER_ERROR', '');
define('ENTRY_PASSWORD', 'Mot de passe :');
define('ENTRY_PASSWORD_ERROR', 'Votre mot de passe doit contenir un minimum de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Le mot de passe de confirmation doit être identique à votre mot de passe.');
define('ENTRY_PASSWORD_ERROR_PRO', 'Votre mot de passe doit contenir un minimum de ' . ENTRY_PASSWORD_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING_PRO', 'Le mot de passe de confirmation doit être identique à votre mot de passe.');
define('ENTRY_PASSWORD_TEXT', '*');
define('ENTRY_PASSWORD_CONFIRMATION', 'Mot de passe de confirmation :');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '* Confirmez votre mot de passe.');
define('ENTRY_PASSWORD_CURRENT', 'Mot de passe actuel :');
define('ENTRY_PASSWORD_CURRENT_TEXT', '*');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Votre mot de passe doit contenir un minimum de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW', 'Nouveau mot de passe :');
define('ENTRY_PASSWORD_NEW_TEXT', '*');
define('ENTRY_PASSWORD_NEW_ERROR', 'Votre nouveau mot de passe doit contenir un minimum de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Le mot de passe de confirmation doit être identique à votre nouveau mot de passe.');
define('ENTRY_PASSWORD_NEW_ERROR_PRO', 'Votre nouveau mot de passe doit contenir un minimum de ' . ENTRY_PASSWORD_PRO_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING_PRO', 'Le mot de passe de confirmation doit être identique à votre nouveau mot de passe.');
define('ENTRY_SPAM_TEXT','*');

define('PASSWORD_HIDDEN', '--CACHE--');
define('FORM_REQUIRED_INFORMATION', '* Informations requises');


// constants for use in osc_prev_next_display function
define('TEXT_RESULT_PAGE', 'Pages de résultat :');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Afficher <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> produits)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Afficher <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> orders)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Afficher <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> avis clients)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW', 'Afficher <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> nouveaux produits)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Afficher <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> promotions)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_HEART','Afficher <strong>%d</strong> à <strong>%d</strong> (sur <strong>%d</strong> coups de coeur)');
define('TEMPLATE_DOES_NOT_EXIST', '<br /><br /><br />Ce template n\'existe pas ! Veuillez v&eacute;rifier vos param&eacute;trages ou t&eacute;l&eacute;charger le fichier sur la market place.<br /><br /><br />');

define('PREVNEXT_TITLE_FIRST_PAGE', 'Première page');
define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Page précédente');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Page suivante');
define('PREVNEXT_TITLE_LAST_PAGE', 'Dernière page');
define('PREVNEXT_TITLE_PAGE_NO', 'Page %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Ensemble précédent de %d pages');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Ensemble suivant de %d pages');
define('PREVNEXT_BUTTON_FIRST', '&lt;&lt;PREMIER');
define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;Préc]');
define('PREVNEXT_BUTTON_NEXT', '[Suiv&nbsp;&gt;&gt;]');
define('PREVNEXT_BUTTON_LAST', 'Dernière&gt;&gt;');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Ajouter adresse');
define('IMAGE_BUTTON_ADDRESS_BOOK', 'Carnet d\'adresses');
define('IMAGE_BUTTON_BACK', 'Retour');
define('IMAGE_BUTTON_BUY_NOW', 'Acheter');
define('IMAGE_BUTTON_CHANGE_ADDRESS', 'Changez l\'adresse');
define('IMAGE_BUTTON_CHECKOUT', 'Commander');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Confirmer la commande avec paiement');
define('IMAGE_BUTTON_PAY_TOTAL_NOW', 'Confirmer la commande avec paiement (%s)');
define('IMAGE_BUTTON_PAY_TOTAL_PROCESSING', 'En cours de traitement, veuillez attendre ..');
define('IMAGE_BUTTON_CONTINUE', 'Continuer');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continuer vos achats');
define('IMAGE_BUTTON_DELETE', 'Supprimer');
define('IMAGE_BUTTON_CONFIRM_DELETE', 'Confirmer la suppression');
define('IMAGE_BUTTON_EDIT_ACCOUNT', 'Editez le compte');
define('IMAGE_BUTTON_HISTORY', 'Historique commandes');
define('IMAGE_BUTTON_LOGIN', 'Se connecter');
define('IMAGE_BUTTON_IN_CART', 'Panier');
define('IMAGE_BUTTON_NOTIFICATIONS', 'Surveiller le produit');
define('IMAGE_BUTTON_QUICK_FIND', 'Recherche rapide');
define('IMAGE_BUTTON_REMOVE_NOTIFICATIONS', 'Supprimer la surveillance sur le produit');
define('IMAGE_BUTTON_CUSTOMERS', 'Compte client');
define('IMAGE_BUTTON_RETAILERS','Compte revendeur');
define('IMAGE_BUTTON_REVIEWS', 'Commentaires clients');
define('IMAGE_BUTTON_SEARCH', 'Rechercher');
define('IMAGE_BUTTON_SHIPPING_OPTIONS', 'Options d\'expédition');
define('IMAGE_BUTTON_STOCK_MARK_PRODUCT_EXHAUSTED','Epuisé');
define('IMAGE_BUTTON_TELL_A_FRIEND', 'Envoyer à un ami');
define('IMAGE_BUTTON_UPDATE', 'Mise à jour');
define('IMAGE_BUTTON_UPDATE_CART', 'Mise à jour panier');
define('IMAGE_BUTTON_WRITE_REVIEW', 'Ecrire un commentaire');

define('SMALL_IMAGE_BUTTON_DELETE', 'Supprimer');
define('SMALL_IMAGE_BUTTON_EDIT', 'Editer');
define('SMALL_IMAGE_BUTTON_VIEW', 'Afficher');
define('SMALL_IMAGE_BUTTON_DETAILS', 'Détails');

define('IMAGE_BUTTON_DISPLAY_STOCK_GOOD','En stock');
define('IMAGE_BUTTON_DISPLAY_STOCK_ALERT','Bientot en rupture de stock');
define('IMAGE_BUTTON_DISPLAY_STOCK_OUT','Produit en rupture de stock');


define('ICON_ARROW_RIGHT', 'Plus');
define('ICON_CART', 'Panier');
define('ICON_ERROR', 'Erreur');
define('ICON_SUCCESS', 'Réussi');
define('ICON_WARNING', 'Attention');

define('TEXT_GREETING_PERSONAL', 'Bienvenue <span class="greetUser">%s!</span> Voudriez vous voir <a href="%s"><u>les nouveautés</u></a> disponibles?');
define('TEXT_GREETING_GUEST', 'Bienvenue <span class="greetUser">Madame, Monsieur</span>. Voulez vous vous <a href="%s"><u>identifier</u></a> ? Préférez vous voir <a href="%s"><u>les nouveautés</u></a> ?');

define('TEXT_SORT_PRODUCTS', 'Trier les produits ');
define('TEXT_DESCENDINGLY', 'décroissant');
define('TEXT_ASCENDINGLY', 'croissant');
define('TEXT_BY', ' par ');

define('TEXT_STOCK','Niveau du stock : ');
define('TEXT_FLASH_DISCOUNT','VENTE FLASH : ');
define('DAYS', 'J ');
define('HOURS', 'H ');
define('MINUTES', 'Mn ');
define('SECONDES', 'Sec ');

define('TEXT_SORT_BY', 'Afficher par ');
define('TEXT_MANUFACTURER','Marques : ');

define('TEXT_REVIEW_BY', 'par %s');
define('TEXT_REVIEW_WORD_COUNT', '%s mots');
define('TEXT_REVIEW_RATING', 'Classement : %s [%s]');
define('TEXT_REVIEW_DATE_ADDED', 'Date d\'ajout : %s');
define('TEXT_NO_REVIEWS', 'Il n\'y a pas encore de commentaires sur ce produit ou celci-ci est en attente de validation.');
define('TEXT_ICON_NEW_PRODUCT','Nouveauté');
define('TEXT_NO_NEW_PRODUCTS', 'Il n\'y a pour le moment aucun nouveau produit.');
define('TEXT_PRICE','');
define('TEXT_PRICE_PRODUCTS_INFO','');

define('TEXT_UNKNOWN_TAX_RATE', 'Taxe inconnue');

define('TEXT_REQUIRED', '<span class="errorText">Requis</span>');
define('ERROR_TEP_MAIL', '<strong><small>TEP ERROR:</small> Impossible d\'envoyer l\'email par le serveur SMTP spécifié. Vérifiez le fichier PHP.INI et corrigez le nom du serveur SMTP si nécessaire.</strong>');

define('TEXT_CCVAL_ERROR_INVALID_DATE', 'La date d\'expiration entrée pour cette carte de crédit n\'est pas valide. Veuillez vérifier la date et réessayez.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Le numéro entrée pour cette carte de crédit n\'est pas valide. Veuillez vérifier le numéro et réessayez.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Le code a 4 chiffres que vous avez entre est : %s. Si ce code est correct, nous n\'acceptons pas ce type de carte credit. S il est errone, veuillez reessayer.');


define('CATEGORY_ADDRESS_PRO', 'Adresse de la société');
define('CATEGORY_TELEPHONE_PRO', 'Contact téléphonique');
define('CATEGORY_PERSONAL_PRO', 'Contact commercial');

define('PRICES_LOGGED_IN_TEXT', 'Veuillez vous connecter si vous souhaitez voir nos tarifs.');
define('PRICES_LOGGED_IN_SUBMIT','Désolé, vous devez être connectés pour voir les tarifs.');
define('PRICES_LOGGED_IN_BUY_TEXT', 'Veuillez vous connecter pour commander nos produits');
define('TAX_INCLUDED', 'TTC');
define('TAX_EXCLUDED', 'HT');

define('NO_ORDERS_GROUP', 'Indisponible');
define('NO_ORDERS_PUBLIC', '');

// Antispam
define('EMAIL_CONFIRMATION_NUMBER','trois');

// Discount Coupon
define('ENTRY_DISCOUNT_COUPON_ERROR', 'Le code du coupon est invalide, veuillez reessayer.');
define('ENTRY_DISCOUNT_COUPON_AVAILABLE_ERROR', 'Le code du coupon insere n est pas ou plus valide.');
define('ENTRY_DISCOUNT_COUPON_USE_ERROR', 'Nos informations nous disent que vous avez deja utilise ce coupon %s fois. Vous ne pouvez plus l utiliser.');
define('ENTRY_DISCOUNT_COUPON_MIN_PRICE_ERROR', 'La commande mimnimale pour ce coupon est de %s');
define('ENTRY_DISCOUNT_COUPON_MIN_QUANTITY_ERROR', 'Le nombre minimum de produits requis pour ce coupon est de %s');
define('ENTRY_DISCOUNT_COUPON_EXCLUSION_ERROR', 'Tout ou une partie de vos produits selectionnes sont exclues de la promotion permise par le coupon' );
define('ENTRY_DISCOUNT_COUPON', 'Code du coupon :');
define('ENTRY_DISCOUNT_COUPON_SHIPPING_CALC_ERROR', 'Le calcul des frais d\'expedition a ete rectifie.');

define('ENTRY_MIN_QTY_ORDER_PRODUCT','');
define('MIN_QTY_ORDER_PRODUCT','Quantité minimum pour une commande :  ');
define('TEXT_PRODUCTS_QUANTITY_TYPE','Type de commande : ');
define('TEXT_CUSTOMER_QUANTITY','Quantité ');
define('TEXT_PRODUCTS_FREE','Gratuit');

define('TABLE_HEADING_DATE_ADDED','Date');
define('TABLE_HEADING_PRICE','prix');
define('TABLE_HEADING_MODEL','Référence');
define('TABLE_HEADING_QUANTITY','Quantité');
define('TABLE_HEADING_WEIGHT','Poids');
define('TABLE_HEADING_DATE','Date');

define('TEXT_INSTALL','
   <div style="text-align:center; padding-top: 50px;"><a href="http://www.clicshopping.org" target="_blank"><img src="images/logo_clicshopping_1.png" border="0" height="105" width="284" alt="Market Place"></a><br />
   <div style="padding-top: 120px;">F&eacute;licitation, vous avez compl&egrave;t&egrave le processus d\'installation, veuillez lire les informations ci-dessous</div>
   <div style="padding-top: 20px;">Veuillez vous connecter &agrave; la Market Place pour commencer &agrave; s&eacute;lectionner les modules en cliquant sur le logo ClicShopping</div>
   <div>ClicShopping est construit sur une architecture modulaire (brique), pour l\'utiliser vous devez installer les modules qui sont disponibles sur la marketplace.</div>
   <div><br /> - Pour en savoir plus sur l\'installation des modules , veuillez vous rendre sur la Market Place &agrave; section du guide : <a href ="http://www.clicshopping.org/marketplace/mes-premiers-pas-bl-9.html" target="_blank">Mes premiers pas</a></div>
   <div> - Pour t&eacute;l&eacute;charger le pack Starter : veuillez cliquer sur : <a href="http://www.clicshopping.org/marketplace/sources/public/clicshopping/" target="_blank">Pack Starter</a> et l\'installer via le syst&egrave;me d\'installation de modules de ClicShopping</div>
   <div> - Pour de plus amples informations, veuillez consulter notre : <a href="http://www.clicshopping.org/marketplace/blog.php" target="_blank">blog</a></div>
');
?>
