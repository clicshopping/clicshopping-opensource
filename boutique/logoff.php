<?php
/*
 * logoff.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  require($OSCOM_Template->GeTemplatetLanguageFiles('logoff'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE);

  $OSCOM_Customer->reset();
  
  if ( isset($_SESSION['sendto']) ) {
    unset($_SESSION['sendto']);
  }

  if ( isset($_SESSION['billto']) ) {
    unset($_SESSION['billto']);
  }

  if ( isset($_SESSION['shipping']) ) {
    unset($_SESSION['shipping']);
  }

  if ( isset($_SESSION['payment']) ) {
    unset($_SESSION['payment']);
  }

  if ( isset($_SESSION['comments']) ) {
    unset($_SESSION['comments']);
  }
  
  if ( isset($_SESSION['coupon']) ) {
    unset($_SESSION['coupon']);
  } 
    
  $_SESSION['cart']->reset();

  osc_redirect(osc_href_link('index.php'));

 require('includes/application_bottom.php');
