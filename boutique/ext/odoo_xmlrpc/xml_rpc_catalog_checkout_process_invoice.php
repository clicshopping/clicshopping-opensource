<?php
/*
 * xml_rpc_admin_checkout_process.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/


// **********************************
//  search customer_id
// **********************************

  $Qcustomers = $OSCOM_PDO->prepare('select customers_id,
                                            orders_id,
                                            orders_status,
                                            date_purchased,
                                            orders_status_invoice,
                                            orders_date_finished,
                                            odoo_invoice,
                                            delivery_postcode,
                                            billing_postcode,
                                            provider_name_client,
                                            client_computer_ip,
                                            customers_group_id
                                       from :table_orders
                                       where orders_id = :orders_id
                                     ');
  $Qcustomers->bindValue(':orders_id', (int)$insert_id);
  $Qcustomers->execute();

  $customers = $Qcustomers->fetch();

  $customers_id = $customers['customers_id'];
  $customers_group_id = $customers['customers_group_id'];
  $order_id = $customers['orders_id'];
  $provider_name_client = $customers['provider_name_client'];
  $client_computer_ip = $customers['client_computer_ip'];
  $date_invoice = $customers['date_purchased'];
  $orders_status_invoice = $customers['orders_status_invoice'];
  $ref_webstore = $OSCOM_Date->getDateReferenceShort($date_invoice) . 'S';
  $odoo_invoice = $customers['odoo_invoice'];


  if ($odoo_invoice == 0) {

// **********************************
// Search odoo customer id / partner_id
// **********************************

    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $customers_id, 'res.partner');

// **********************************
// read id invoice odoo
// **********************************

    $field_list = array('ref',
                        'id'
                      );

    $partner_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
    $partner_id = $partner_id[0][id];


// **********************************
// Search odoo company id / name
// **********************************

    $ids = $OSCOM_ODOO->odooSearch('name', '=', ODOO_WEB_SERVICE_COMPANY_WEB_SERVICE, 'res.company');

// **********************************
// read id company odoo
// **********************************

    $field_list = array('id');

    $QcompanyId = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.company');
    $company_id = $QcompanyId[0][id];

// **********************************
// Search odoo id purchased account.account for customers
// **********************************
// ODOO_WEB_SERVICE_ACCOUNT_PURCHASE ===> CUSTOMERS

    $ids = $OSCOM_ODOO->odooSearch('code', '=', ODOO_WEB_SERVICE_ACCOUNT_PURCHASE, 'account.account');

// read id purchased odoo
    $field_list = array('id',
                        'name',
                        'code',
                      );

    $account_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
    $account_id = $account_id[0][id];

// **********************************
// Create top invoice
// **********************************

// top of invoice
     $values = array ("partner_id" => new xmlrpcval($partner_id, "int"),
                      "date_invoice" => new xmlrpcval(date('Y-m-d'), "string"),
                      "company_id" => new xmlrpcval($company_id, "int"),
                      "origin" => new xmlrpcval("WebStore - order : " . $ref_webstore . $insert_id, "string"),
                      "account_id" => new xmlrpcval($account_id, "int"),
                      "clicshopping_invoice_id" => new xmlrpcval($order_id, "int"),
                      "clicshopping_invoice_reference" => new xmlrpcval($ref_webstore . $oID, "string"),
                      "clicshopping_invoice_customer_id" => new xmlrpcval($customers_id, "int"),
                      "clicshopping_invoice_provider_name_client" => new xmlrpcval($provider_name_client, "string"),
                      "clicshopping_invoice_client_computer_ip" => new xmlrpcval($client_computer_ip, "string"),
                      "clicshopping_invoice_customer_comments" => new xmlrpcval($order->info['comments'], "string"),
                      "clicshopping_invoice_status" => new xmlrpcval('instance', "string"),
                      "clicshopping_invoice_customers_group_id" => new xmlrpcval($customers_group_id, "int"),
                     );

     $OSCOM_ODOO->createOdoo($values, "account.invoice");

//******************************************
// invoice line search
//***************************************

    $ids = $OSCOM_ODOO->odooSearch('code', '=',ODOO_WEB_SERVICE_ACCOUNT_SELL, 'account.account');

// **********************************
// read id account odoo
// **********************************

    $field_list = array('id',
                        'name',
                        'code',
                      );

    $account_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
    $account_id = $account_id[0][id];

// research invoice_id by origin invoice
    $ids = $OSCOM_ODOO->odooSearch('origin', '=', "WebStore - order : " . $ref_webstore . $insert_id, 'account.invoice');

// read id account odoo
    $field_list = array('id');

    $invoice_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.invoice');
    $invoice_id = $invoice_id[0][id];


// **********************************
// write the lines concerning the products
// **********************************

// search the amount of invoice of shipping
      $QshippingAmount = $OSCOM_PDO->prepare('select title,
                                                     value,
                                                     class
                                               from :table_orders_total
                                               where orders_id = :orders_id
                                               and class = :class
                                             ');
      $QshippingAmount->bindInt(':orders_id', (int)$insert_id);
      $QshippingAmount->bindValue(':class', 'ot_shipping');
      $QshippingAmount->execute();

      $Qshipping_amount = $QshippingAmount->fetch();

      $shipping_title = html_entity_decode($Qshipping_amount['title']);
      $shipping_amount = $Qshipping_amount['value'];

// Search odoo id shipping account.account for shipping
      $ids = $OSCOM_ODOO->odooSearch('code', '=', ODOO_WEB_SERVICE_ACCOUNT_SHIPPING, 'account.account');

// read id shipping odoo
    $field_list = array('id',
                        'name',
                        'code',
                      );

      $QshippingAccountId = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
      $shipping_account_id = $QshippingAccountId[0][id]; // 864

// **********************************
// Search odoo id discount for customer and coupon account.account
// **********************************
      $ids = $OSCOM_ODOO->odooSearch('code', '=', ODOO_WEB_SERVICE_ACCOUNT_DISCOUNT, 'account.account');



//search discount coupon
      $QDiscountCouponAmount = $OSCOM_PDO->prepare('select title,
                                                           value,
                                                           class
                                                     from :table_orders_total
                                                     where orders_id = :orders_id
                                                     and class = :class
                                                   ');
      $QDiscountCouponAmount->bindInt(':orders_id', (int)$insert_id);
      $QDiscountCouponAmount->bindValue(':class', 'ot_discount_coupon');
      $QDiscountCouponAmount->execute();

      $Qdiscount_coupon_amount = $QDiscountCouponAmount->fetch();

      $discount_coupon_title = html_entity_decode($Qdiscount_coupon_amount['title']);

// apply if the option - is selectied in discount_coupon
      if ($Qdiscount_coupon_amount['value'] < 0) {
        $discount_coupon_amount = $Qdiscount_coupon_amount['value'];
      } else {
        $discount_coupon_amount = $Qdiscount_coupon_amount['value'] * (-1);
      }

// **********************************
// Search odoo id discount account.account
// **********************************
      $QCustomerDiscountAmount = $OSCOM_PDO->prepare('select title,
                                                             value,
                                                             class
                                                       from :table_orders_total
                                                       where orders_id = :orders_id
                                                       and class = :class
                                                     ');
      $QCustomerDiscountAmount->bindInt(':orders_id', (int)$insert_id);
      $QCustomerDiscountAmount->bindValue(':class', 'ot_customer_discount');
      $QCustomerDiscountAmount->execute();

      $Qcustomer_discount_amount = $QCustomerDiscountAmount->fetch();

      $customer_discount_title = html_entity_decode($Qcustomer_discount_amount['title']);
      $customer_discount_amount = $Qcustomer_discount_amount['value'] * (-1);
     
// read id discount odoo
    $field_list = array('id',
                        'name',
                        'code',
                      );

    $Qdiscount_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'account.account');
    $discount_id = $Qdiscount_id[0][id];

// **********************************
// write the lines concerning the products
// **********************************

// count number of product
      $count_products = sizeof($order->products);

      for ($o=0, $n=$count_products; $o<$n; $o++) {

//******************************************
// research invoice_id by origin invoice
//***************************************
// ====> mettre en relation avec la B2B
        $ids = $OSCOM_ODOO->odooSearch('default_code', '=', $order->products[$o]['model'], 'product.template');

// **********************************
// read id products odoo
// **********************************

        $field_list = array('default_code',
                            'id'
                            );

        $Qodoo_products_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.template');
        $odoo_products_id = $Qodoo_products_id[0][id];

// **********************************
// name of products and options description
// doesn't take more than one options
// text limited  by odoo
// **********************************

        $products_name = $order->products[$o]['name'];

        if (isset($order->products[$o]['attributes']) && (sizeof($order->products[$o]['attributes']) > 0)) {
          for ($j = 0, $k = sizeof($order->products[$o]['attributes']); $j < $k; $j++) {
// attributes reference
            if ($order->products[$o]['attributes'][$j]['reference'] != '' or $order->products[$o]['attributes'][$j]['reference'] !='null') {
              $attributes_reference = $order->products[$o]['attributes'][$j]['reference'] . ' - ';
            }
            $products_attributes = ' ' . $attributes_reference  . $order->products[$o]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
            $products_attributes = osc_strip_html_tags($products_attributes);
          }
        }

        if ($products_attributes != '') {
          $products_attributes = ' - ' . $products_attributes;
        }

        $products_name_odoo = $products_name . $products_attributes;

// **********************************
// tax
// **********************************

// select the good tax in function the products
          $QProductsTax = $OSCOM_PDO->prepare('select products_tax
                                               from :table_orders_products
                                               where orders_id = :orders_id
                                               and products_id = :products_id
                                             ');
          $QProductsTax->bindInt(':orders_id', (int)$insert_id);
          $QProductsTax->bindValue(':products_id', $order->products[$o]['id']);
          $QProductsTax->execute();

          $Qproducts_tax = $QProductsTax->fetch();


// select the tax code_tax_odoo in function the products tax
          $QProductsCodeTax = $OSCOM_PDO->prepare('select code_tax_odoo
                                                   from :table_tax_rates
                                                   where tax_rate = :tax_rate
                                                 ');

          $QProductsCodeTax->bindValue(':tax_rate', $Qproducts_tax['products_tax']);
          $QProductsCodeTax->execute();

          $Qproducts_code_tax = $QProductsCodeTax->fetch();
          $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];

//******************************************
// research id tax by description
//***************************************

          if($products_code_tax != null) {
            $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
            $odoo_products_tax_id = $ids;
            $odoo_products_tax_id = $odoo_products_tax_id[0];

            if (!empty($odoo_products_tax_id)) {

              $type_tax_string = 'array';

              $tax = array(new xmlrpcval(
                                          array(
                                                  new xmlrpcval(6, "int"),// 6 : id link
                                                  new xmlrpcval(0, "int"),
                                                  new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                ), "array"
                                        )
                          );
            } else  {
              $tax = 0;
              $type_tax_string = 'int';
            }
          } else {
            $tax = 0;
            $type_tax_string = 'int';
          }

// **********************************
// Write a new line concerning the invoice
// **********************************

        $values = array (
                        "invoice_id" => new xmlrpcval($invoice_id, "int"),
                        "account_id" => new xmlrpcval($account_id, "int"),
                        "company_id" => new xmlrpcval($company_id, "int"),
                        "product_id" => new xmlrpcval($odoo_products_id, "int"),
                        "name" => new xmlrpcval($products_name_odoo, "string"),
                        "price_unit" => new xmlrpcval($order->products[$o]['final_price'],"double"),
                        "quantity" => new xmlrpcval($order->products[$o]['qty'],'double'),
                        "invoice_line_tax_id" =>  new xmlrpcval($tax, $type_tax_string),
                      );

        $OSCOM_ODOO->createOdoo($values, "account.invoice.line");

      } //end for


// **********************************
// Write a new line concerning the shipping by the service line available in odoo
// **********************************

      if ($shipping_amount != 0 ) {

        if (DISPLAY_DOUBLE_TAXE =='true') {

          $count_products = sizeof($order->products);

// select the good tax in function the products
            $QProductsTax = $OSCOM_PDO->prepare('select products_tax
                                                   from :table_orders_products
                                                   where orders_id = :orders_id
                                                   and products_id = :products_id
                                                 ');
          $QProductsTax->bindInt(':orders_id', (int)$insert_id);
          $QProductsTax->bindValue(':products_id', $order->products[$o]['id']);
          $QProductsTax->execute();

          $Qproducts_tax = $QProductsTax->fetch();

          if  ( $Qproducts_tax['products_tax'] != '' || $Qproducts_tax['products_tax'] != 0) {
// select the tax code_tax_odoo in function the products tax
            $QProductsCodeTax = $OSCOM_PDO->prepare('select code_tax_odoo
                                                       from :table_tax_rates
                                                       where tax_rate = :tax_rate
                                                     ');

            $QProductsCodeTax->bindValue(':tax_rate', $Qproducts_tax['products_tax']);

            $QProductsCodeTax->execute();

            $Qproducts_code_tax = $QProductsCodeTax->fetch();
            $products_code_tax = $Qproducts_code_tax['code_tax_odoo'];


            if($products_code_tax != null) {

              $ids = $OSCOM_ODOO->odooSearch('description', '=', $products_code_tax, 'account.tax', 'string');
              $odoo_products_tax_id = $ids;
              $odoo_products_tax_id = $odoo_products_tax_id[0];

              if (!empty($odoo_products_tax_id)) {

                $type_tax_string = 'array';

                $tax = array(new xmlrpcval(
                                            array(
                                                    new xmlrpcval(6, "int"),// 6 : id link
                                                    new xmlrpcval(0, "int"),
                                                    new xmlrpcval(array(new xmlrpcval($odoo_products_tax_id, "int")), "array")
                                                  ), "array"
                                          )
                            );
              } else  {
                $tax = 0;
                $type_tax_string = 'int';
              }
            } else {
              $tax = 0;
              $type_tax_string = 'int';
            }
          }
        } else {
          $tax = 0;
          $type_tax_string = 'int';
        }

        $values = array (
                          "invoice_id" => new xmlrpcval($invoice_id, "int"),
                          "account_id" => new xmlrpcval($shipping_account_id, "int"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "product_id" => new xmlrpcval(0, "int"),
                          "name" => new xmlrpcval($shipping_title, "string"),
                          "price_unit" => new xmlrpcval($shipping_amount,"double"),
                          "quantity" => new xmlrpcval(1,'int'),
                          "invoice_line_tax_id" =>  new xmlrpcval($tax, $type_tax_string),
                        );

        $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
	
      }

// **********************************
// Write a new line concerning the customer discount  by the service line available in odoo
// **********************************

    if ($customer_discount_amount != 0 ) {
      $values = array (
                        "invoice_id" => new xmlrpcval($invoice_id, "int"),
                        "company_id" => new xmlrpcval($company_id, "int"),
                        "account_id" => new xmlrpcval($discount_id, "int"),
                        "product_id" => new xmlrpcval(0, "int"),
                        "name" => new xmlrpcval($customer_discount_title, "string"),
                        "price_unit" => new xmlrpcval($customer_discount_amount,"double"),
                        "quantity" => new xmlrpcval(1,'int'),
                        "invoice_line_tax_id" =>  new xmlrpcval(0, 'int'),
                      );
      $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
    }


// **********************************
// Write a new line concerning the discount coupon by the service line available in odoo
// **********************************

      if ($discount_coupon_amount != 0 ) {
        $values = array (
                          "invoice_id" => new xmlrpcval($invoice_id, "int"),
                          "company_id" => new xmlrpcval($company_id, "int"),
                          "account_id" => new xmlrpcval($discount_id, "int"),
                          "product_id" => new xmlrpcval(0, "int"),
                          "name" => new xmlrpcval($discount_coupon_title, "string"),
                          "price_unit" => new xmlrpcval($discount_coupon_amount,"double"),
                          "quantity" => new xmlrpcval(1,'int'),
                          "invoice_line_tax_id" =>  new xmlrpcval(0, 'int'),
                        );

        $OSCOM_ODOO->createOdoo($values, "account.invoice.line");
      }

    $OSCOM_ODOO->buttonClickOdoo('account.invoice', 'button_reset_taxes', $invoice_id);


     if ($invoice_id != '') {
// Update ClicShopping
      $Qupdate = $OSCOM_PDO->prepare('update :table_orders
                                      set odoo_invoice = :odoo_invoice
                                      where orders_id = :orders_id
                                    ');
      $Qupdate->bindInt(':orders_id', $insert_id);
      $Qupdate->bindValue(':odoo_invoice', '2');
      $Qupdate->execute();
    }
  } // end $odoo_invoice
?>