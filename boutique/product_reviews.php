<?php
/*
 * product_reviews.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!isset($_GET['products_id'])) {
    osc_redirect(osc_href_link('reviews.php'));
  }

  $reviews_split = $OSCOM_ProductsReviews->getReviewsSplit($OSCOM_ProductsReviews->getReviews());

//  $reviews_split = new splitPageResults($reviews_query_raw, MAX_DISPLAY_NEW_REVIEWS);
  $reviews_query = osc_db_query($reviews_split->sql_query);

// Redirige le navigateur vers la page reviews.php si aucune informations est disponnible dans la base de donnees
      if (!osc_db_num_rows($products_query)) {
        osc_redirect(osc_href_link('reviews.php'));
      } else {
        $products = osc_db_fetch_array($products_query);
      }

// *************************
// display the differents prices before button
// **************************
      $product_price = $OSCOM_Products->getCustomersPrice();
// *************************
// See the button more wview details
// *************************
      $button_small_view_details = osc_draw_button(SMALL_IMAGE_BUTTON_DETAILS, '', osc_href_link('product_info.php', 'products_id='. $OSCOM_Products->getId() ), 'info', null,'small');
// *************************
// Gestion de l'affichage des images et des zooms
// *************************
       if (osc_not_null($products['products_image'])) {
          $review_image =  '<h1>' . osc_image(DIR_WS_IMAGES . $products['products_image'], $OSCOM_Products->getProductsName(), SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'hspace="5" vspace="5"').'</h1>';
       }

// *************************
// Doit etre a la fin du fichier
// archive management
// *************************

      if ( $OSCOM_Products->getProductsArchive() == '1' &&  $products['products_id'] == (int)$OSCOM_Products->getId()) {
        $product_price = '';
        $submit_button = '';
        $product_not_sell = PRODUCTS_NOT_SELL;
      }
  
  require($OSCOM_Template->GeTemplatetLanguageFiles('product_reviews'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE, osc_href_link('product_reviews.php', osc_get_all_get_params()));

  require($OSCOM_Template->getTemplateFiles('product_reviews'));

