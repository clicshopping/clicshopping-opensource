<?php
/*
 * checkout_shipping_address.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('checkout_address') ) {
    echo $OSCOM_MessageStack->get('checkout_address');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
  echo osc_draw_form('checkout_address', osc_href_link('checkout_shipping_address.php', '', 'SSL'), 'post', '', true);

// ------------------------------ 
// --- Shipping address   -----  
// ------------------------------- 
?>
<div class="contentContainer">
<?php
  if ( $OSCOM_Customer->hasDefaultAddress() ) {
    if ($process === false) {
?>
   <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
   <h3><?php echo TABLE_HEADING_SHIPPING_ADDRESS; ?></h3>

  <div class="contentText row">
    <div class="col-sm-8">
      <div><?php echo TEXT_SELECTED_SHIPPING_DESTINATION; ?></div>
    </div>
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading"><?php echo TITLE_SHIPPING_ADDRESS; ?></div>

        <div class="panel-body">
          <?php echo osc_address_label($OSCOM_Customer->getID(), $_SESSION['sendto'], true, ' ', '<br />'); ?>
        </div>
      </div>
    </div>

  </div>

  <div class="clearfix"></div>

<?php
      if ($addresses_count > 1) {
?>
  <h3><?php echo TABLE_HEADING_ADDRESS_BOOK_ENTRIES; ?></h3>
<?php
// ----------------------------------- 
// --- Select other destination   -----
// ------------------------------- ----
?>
  <div><?php echo TEXT_SELECT_OTHER_SHIPPING_DESTINATION; ?></div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  <div class="contentText row">
<?php
        $radio_buttons = 0;

        while ($addresses = $Qaddresses->fetch() ) {
          $format_id = osc_get_address_format_id($addresses['country_id']);
?>
      <div class="col-sm-4">
        <div class="panel panel-<?php echo ($addresses['address_book_id'] == $OSCOM_Customer->hasDefaultAddress()) ? 'primary' : 'default'; ?>">
          <div class="panel-heading"><strong><?php echo osc_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></strong></div>
          <div class="panel-body"><?php echo osc_address_format($format_id, $addresses, true, ' ', '<br />'); ?> </div>
          <div class="panel-footer text-center"><?php echo osc_draw_radio_field('address', $addresses['address_book_id'], ($addresses['address_book_id'] == $_SESSION['sendto'])); ?></div>
        </div>
      </div>
<?php
          $radio_buttons++;
        } // end while
?>

    </table>
  </div>
<?php
      } // $addresses_count
    } // $process
  } // has_default

  if ($addresses_count < MAX_ADDRESS_BOOK_ENTRIES) {
?>
  <h3><?php echo TABLE_HEADING_NEW_SHIPPING_ADDRESS; ?></h3>
<?php
// ------------------------------ 
// --- new address -------------  
// ------------------------------- 
?>
  <div><?php echo TEXT_CREATE_NEW_SHIPPING_ADDRESS; ?></div>
<?php
    if ($OSCOM_Customer->getCustomersGroupID() == '0' || ACCOUNT_ADRESS_BOOK_PRO == 'true') {
      require($OSCOM_Template->getTemplateModules('customers_address/checkout_new_address'));
    }
  }
?>
  <div class="row">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  </div>
    <div class="pull-right">
       <div class="buttonSet"><?php echo osc_draw_hidden_field('action', 'submit') . osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null); ?></div>
    </div>
<?php
  if ($process == true) {
?>
  <div class="pull-left">
    <div class="buttonSet"><?php echo osc_draw_button(IMAGE_BUTTON_BACK, '', osc_href_link('checkout_shipping_address.php', '', 'SSL'),'info', null, null); ?></div>
  </div>
<?php
  }
?>
  <div class="row">
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
  </div>
</div>
</form>
<?php   require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
