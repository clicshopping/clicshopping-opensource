<?php
/*
 * address_book.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:  
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('address_book'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('address_book.php', '', 'SSL'));

// select the address of customer

  $Qaddresses = $OSCOM_PDO->prepare('select address_book_id,
                                          entry_firstname as firstname,
                                          entry_lastname as lastname,
                                          entry_company as company,
                                          entry_street_address as street_address,
                                          entry_suburb as suburb,
                                          entry_city as city,
                                          entry_postcode as postcode,
                                          entry_state as state,
                                          entry_zone_id as zone_id,
                                          entry_country_id as country_id
                                   from :table_address_book
                                   where customers_id = :customers_id
                                   order by firstname, lastname
                                  ');
  $Qaddresses->bindInt(':customers_id', (int)$OSCOM_Customer->getID());

  $Qaddresses->execute();
  
  require($OSCOM_Template->getTemplateFiles('address_book'));
