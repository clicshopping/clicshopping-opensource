<?php
/*
 * account_history_info.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  if (!$OSCOM_Customer->isLoggedOn()) {
    $_SESSION['navigation']->set_snapshot();
    osc_redirect(osc_href_link('login.php', '', 'SSL'));
  }

  if (!isset($_GET['order_id']) || (isset($_GET['order_id']) && !is_numeric($_GET['order_id']))) {
    osc_redirect(osc_href_link('account_history.php', '', 'SSL'));
  }

  $QcustomerInfo = $OSCOM_PDO->prepare('select o.customers_id
                                       from :table_orders o,
                                            :table_orders_status s
                                       where o.orders_id = :orders_id
                                       and o.orders_status = s.orders_status_id 
                                       and s.language_id = :language_id
                                       and s.public_flag = :public_flag
                                      ');
  $QcustomerInfo->bindInt(':orders_id', (int)$_GET['order_id']);
  $QcustomerInfo->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $QcustomerInfo->bindValue(':public_flag', '1');

  $QcustomerInfo->execute();
  $customer_info = $QcustomerInfo->fetch();

  if ($customer_info['customers_id'] != $OSCOM_Customer->getID()) {
    osc_redirect(osc_href_link('account_history.php', '', 'SSL'));
  }

  require($OSCOM_Template->GeTemplatetLanguageFiles('account_history_info'));

  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_1, osc_href_link('account.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(NAVBAR_TITLE_2, osc_href_link('account_history.php', '', 'SSL'));
  $OSCOM_Breadcrumb->add(sprintf(NAVBAR_TITLE_3, $_GET['order_id']), osc_href_link('account_history_info.php', 'order_id=' . $_GET['order_id'], 'SSL'));

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order($_GET['order_id']);
 

// Count the number of the history order
  $Qordershistory = $OSCOM_PDO->prepare('select count(orders_status_id) as count
                                         from :table_orders_status_history
                                         where orders_id = :orders_id
                                        ');
  $Qordershistory->bindInt(':orders_id', (int)$_GET['order_id']);

  $Qordershistory->execute();
  $orders_history = $Qordershistory->fetch();
 
// display the pdf invoirce or not  
  $Qsupport = $OSCOM_PDO->prepare('select support_orders_flag
                                  from :table_orders_status
                                  where support_orders_flag = :support_orders_flag
                                  and orders_status_id = :orders_status_id
                                 ');
  $Qsupport->bindValue(':support_orders_flag', '0');
  $Qsupport->bindInt(':orders_status_id', '5');

  $Qsupport->execute();
  $support = $Qsupport->fetch();

// Display the pdf type in function the status
    if ($orders_history['count'] > '0') {
      if ($support['support_orders_flag'] == '0') {
        $print_invoice_pdf = '<a href="' . osc_href_link('orders_invoice.php', osc_get_all_get_params(array('order_id')) . 'order_id=' . (int)$_GET['order_id'], 'SSL') . '" target="_blank">' . osc_image_button('button_big_invoice_pdf.png', IMAGE_BUTTON_PRINT_ORDER_ORDER) . '<br />'.INVOICE_DOWNLOAD.'</a>';
      }
    } 

// display order history
  $Qstatuse = $OSCOM_PDO->prepare('select distinct os.orders_status_name,
                                                    osh.date_added,
                                                    osh.comments,
                                                    osh.orders_tracking_number
                                  from :table_orders_status os,
                                       :table_orders_status_history osh
                                  where osh.orders_id = :orders_id
                                  and osh.orders_status_id = os.orders_status_id
                                  and os.language_id = :language_id
                                  and os.public_flag = :public_flag
                                  order by osh.date_added
                                  ');
  $Qstatuse->bindInt(':orders_id', (int)$_GET['order_id']);
  $Qstatuse->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $Qstatuse->bindValue(':public_flag', '1');

  $Qstatuse->execute();

// select the tracking order
  $Qtracking = $OSCOM_PDO->prepare('select distinct  ost.orders_tracking_number,
                                                    osh.orders_status_tracking_id,
                                                    osh.orders_status_tracking_name,
                                                    osh.orders_status_tracking_link,
                                                    ost.orders_id
                                   from  :table_orders_status_history ost,
                                         :table_orders_status_tracking osh
                                   where ost.orders_id = :orders_id
                                   and ost.orders_tracking_number <> :orders_tracking_number
                                   and osh.orders_status_tracking_id = ost.orders_status_tracking_id
                                  ');
  $Qtracking->bindInt(':orders_id', (int)$_GET['order_id']);
  $Qtracking->bindValue(':orders_tracking_number', null);

  $Qtracking->execute();

// download product
   // Display the pdf type in function the status
  $QdonwloadProductsFiles = $OSCOM_PDO->prepare('select distinct p.products_id,
                                                                  p.products_download_filename,
                                                                  op.products_id,
                                                                  op.products_id,
                                                                  op.orders_id,
                                                                  o.orders_id
                                                 from :table_products p,
                                                      :table_orders_products  op,
                                                      :table_orders o,
                                                      :table_orders_status os
                                                 where p.products_id = op.products_id
                                                 and o.orders_id = op.orders_id
                                                 and op.orders_id = :orders_id
                                                 and o.orders_status = :orders_status
                                              ');
  $QdonwloadProductsFiles->bindInt(':orders_id', (int)$_GET['order_id']);
  $QdonwloadProductsFiles->bindInt(':orders_status', '3');

  $QdonwloadProductsFiles->execute();

  require($OSCOM_Template->getTemplateFiles('account_history_info'));
?>