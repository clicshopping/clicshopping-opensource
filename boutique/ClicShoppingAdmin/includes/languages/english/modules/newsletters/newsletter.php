<?php
/**
 * newsletters.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 

*/

define('TEXT_COUNT_CUSTOMERS', 'Customers receiving newsletter: %s');
define('TEXT_COUNT_CUSTOMERS_NO_ACCOUNT', 'Customers without account receiving newsletter : %s');

define('TEXT_UNSUBSCRIBE', 'Unsubscribe');

define('TEXT_SEND_NEWSLETTER_EMAIL','<p style="font-size: 10px; tex-align:center;">To make sure you receive all our emails, add <strong>'.STORE_OWNER_EMAIL_ADDRESS.'</strong> to your address book.</p><br/>');

define('TEXT_SEND_NEWSLETTER','<p style="font-size: 10px; tex-align:center;">If you don\'t see the '.STORE_NAME.' newsletter, you can copy this Link below in your browser.</p><br/>');
define('TEXT_FILE_NEWSLETTER','Newsletter file name created : ');
define('TEXT_FILE_DIRECTORIES','Directory : ');
define('TEXT_TWITTER','Find our lastest newsletter on '.STORE_NAME.' : ');
?>