<?php
/*
 * stats_products_validation.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
 */


define('HEADING_TITLE', 'Analyse the validity of email domain');

define('TABLE_HEADING_CUSTOMERS_ID', 'Id customers');
define('TABLE_HEADING_FIRST_NAME', 'First name');
define('TABLE_HEADING_LAST_NAME', 'Last name');
define('TABLE_HEADING_ADDRESS_EMAIL', 'Email');
define('TABLE_HEADING_VALIDATE_EMAIL', 'Analyse');
define('TABLE_HEADING_CLEAR', 'Delete');
define('TEXT_SUCCESS_DOMAIN','Valid domain');
define('TEXT_NO_SUCCESS_DOMAIN','<font color="#FF0000"><strong>Domain no valid<strong></font>');
define('IMAGE_RESET_EMAIL','Reset emails');
define('IMAGE_ANALYSE','Customers email analyse');
define('TITLE_AIDE_EMAIL','<strong>Note sur l\'analyse:</strong>');
define('TITLE_TEXT_AIDE_EMAIL','<li>The analysis focuses only on the domain of the email (for now).</li>
<li>More you have email, more the process will be long in the analysis.</li>
<li>If the domain of the email is not valid, it will automatically be removed from sending emails or newsletter.</li>
<li>If you have many contacts, please do not repeat the analysis of too many times (1 time per month).</li>
<li>Invalid emails will not be re-analyzed a second time unless you reset the analysis.</li>
<li>The analysis result is displayed in the listing of the form of customer.</li>
<li>you can cancelling the process at all moment.<li>
');
?>