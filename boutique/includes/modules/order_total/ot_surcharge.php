<?php
/**
 * ot_shurchage.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  class ot_surcharge {
    var $title, $output;

    function ot_surcharge() {
      $this->code = 'ot_surcharge';

      if (MODULE_PAYMENT_SURCHAGE_TEXT_TITLE != 'MODULE_PAYMENT_SURCHAGE_TEXT_TITLE') {
        $this->title = MODULE_PAYMENT_SURCHAGE_TEXT_TITLE;
      } else {
        $this->title = MODULE_PAYMENT_SURCHAGE_TITLE;
      }

      $this->description = MODULE_PAYMENT_SURCHAGE_DESCRIPTION;
      $this->enabled = MODULE_PAYMENT_SURCHAGE_STATUS;
      $this->sort_order = MODULE_PAYMENT_SURCHAGE_SORT_ORDER;
      $this->surchage = MODULE_PAYMENT_SURCHAGE_PERCENTAGE;
      $this->maximum = MODULE_PAYMENT_SURCHAGE_MINIMUM;
      $this->output = array();
    }

    function process() {
      global $order, $currencies;
      $order_total = $order->info['total'];
      $order_surchage_maximum = $this->maximum;

     if ($order_total > $order_surchage_maximum) {
        $od_amount = $this->surchage;
     }

      $this->output[] = array('title' => $this->title . ':',
                              'text' => $currencies->format($od_amount),
                              'value' => $od_amount);
      $order->info['total'] = $order->info['total'] + $od_amount;  
    }

    
    function check() {
      if (!isset($this->check)) {
        $check_query = osc_db_query("select configuration_value 
                                     from configuration
                                     where configuration_key = 'MODULE_PAYMENT_SURCHAGE_STATUS'
                                   ");
        $this->check = osc_db_num_rows($check_query);
      }

      return $this->check;
    }

    function keys() {
      return array('MODULE_PAYMENT_SURCHAGE_STATUS',
                   'MODULE_PAYMENT_SURCHAGE_TEXT_TITLE', 
                   'MODULE_PAYMENT_SURCHAGE_PERCENTAGE',
                   'MODULE_PAYMENT_SURCHAGE_MINIMUM',
                   'MODULE_PAYMENT_SURCHAGE_SORT_ORDER'
                  );
    }

    function install() {
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Voulez vous mettre en place le syst&egrave;me de surchage forfaitaire concernant la commande', 'MODULE_PAYMENT_SURCHAGE_STATUS', 'true', 'Souhaitez afficher ce syst&egrave;me de surcharge forfaitaire ? ', '6', '1','osc_cfg_select_option(array(\'true\', \'false\'), ', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Titre', 'MODULE_PAYMENT_SURCHAGE_TEXT_TITLE', 'Surchage forfaitaire', 'Vous pouvez changer le nom de ce module qui s\affichera pour le client.<br />', '6', '2', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Ordre de tri', 'MODULE_PAYMENT_SURCHAGE_SORT_ORDER', '888', 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr&eacute; en premier)<br /><br /><u><br />Note :</strong></u><br /><br />Veuillez mettre un num&eacute;ro d\'ordre de tri <strong>au dessus</strong> du total g&eacute;n&eacute;ral de la commande.<br />', '6', '3', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Veillez indiquer le montant de la surchage forfaitaire<br />', 'MODULE_PAYMENT_SURCHAGE_PERCENTAGE', '0', 'le montant de la surchage forfaitaire doit comprendre l\ensemble des taxes de la surchage .', '6', '4', now())");
      osc_db_query("insert into configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Veuillez indiquer le montant maximal de commande sur lequel s\'appliquera &agrave; ce forfait<br />', 'MODULE_PAYMENT_SURCHAGE_MINIMUM', '0', 'Si le montant total  de la commande d&acute;passe ce montant, la surchage forfaitaire ne sera pas appliqu&eacute;', '6', '5', now())");
    }

    function remove() {
      $keys = '';
      $keys_array = $this->keys();
      for ($i=0; $i<sizeof($keys_array); $i++) {
        $keys .= "'" . $keys_array[$i] . "',";
      }
      $keys = substr($keys, 0, -1);

      osc_db_query("delete from configuration where configuration_key in (" . $keys . ")");
    }
  }
?>
