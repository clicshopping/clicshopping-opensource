<?php
/**
 * discount_coupons_exclusions.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  /**********************************************/
  //toggle these true/false to determine which fields to display in product list in the same order as they are listed below

  $display_fields = array('categories' => array( 'c.categories_id' => false,
                                                 'cd.categories_name' => true ),
                                                 
                          'customers' => array( 'c.customers_id' => false,
                                                'CONCAT( c.customers_firstname, CONCAT( " ", c.customers_lastname ) ) AS name' => true,
                                                'c.customers_email_address' => false ),

                          'manufacturers' => array( 'm.manufacturers_id' => false,
                                                    'm.manufacturers_name' => true ),
                                                    
                          'zones' => array( 'gz.geo_zone_name' => true,
                                            'gz.geo_zone_description' => true ),  

                          'products' => array( 'p.products_id' => false,
                                               'p.products_model' => true,
                                               'pd.products_name' => true,
                                               'p.products_price' => false
                                              )
                          );

//separator for the fields in the listing
  $separator = ' :: ';

//category exclusions
//separator for the category names
  $category_separator = '->';
//toggle true/false to determine whether to display the full category path
  $category_path = true;
//end category exclusions

/**********************************************/

  require('includes/application_top.php' );
  require('includes/functions/discount_coupons.php' );
  require( 'includes/classes/discount_coupons_exclusions.php' );

  $action = ( isset( $_POST['action'] ) ? $_POST['action'] : '' );

  if( isset( $_GET['cID'] ) && $_GET['cID'] != '' ) {
    $coupons_id = osc_db_input($_GET['cID']);
  } else {
    osc_redirect(osc_href_link('discount_coupons.php', 'error=' . ERROR_DISCOUNT_COUPONS_NO_COUPON_CODE));
  }

  $type = ( isset( $_GET['type'] ) && $_GET['type'] != '' ? osc_db_input( $_GET['type'] ) : '' );

  if( !( $exclusion = new coupons_exclusions( $coupons_id, $type ) ) )
    osc_redirect( osc_href_link('discount_coupons.php', 'cID='.$coupons_id.'&error='.ERROR_DISCOUNT_COUPONS_INVALID_TYPE ) );

  if( osc_not_null( $action ) ) {

    switch( $action ) {
      case 'Sauvegarder':

/*debug*/
 //       print_r( '<pre>'.print_r( $exclusion, true ).'</pre>' );
//        print_r( '<pre>'.print_r( $_POST, true ).'</pre>' );

        $exclusion->save( $_POST['selected_'.$type] );

        osc_redirect( osc_href_link('discount_coupons.php', 'cID='.$coupons_id.'&message='.MESSAGE_DISCOUNT_COUPONS_EXCLUSIONS_SAVED ) );
      break;

      case 'Cancel':
      break;
    }

    osc_redirect( osc_href_link('discount_coupons.php', 'cID='.$coupons_id ) );
  } else {

    $display_fields = array_keys( $display_fields[$type], true );
    $display_fields = ( count( $display_fields ) > 0 ? ', '.implode( ', ', $display_fields ) : '' );

    switch( $type ) {

      case 'customers':
        $sql_selected = 'SELECT dc2c.customers_id AS id'.$display_fields.'
                         FROM discount_coupons_to_customers dc2c
                         LEFT JOIN customers c
                         ON c.customers_id=dc2c.customers_id
                         WHERE dc2c.coupons_id= "' . $coupons_id . '"';
        $sql_all = 'SELECT c.customers_id AS id'.$display_fields.'
                    FROM customers c
                    %s';
        $where = ' WHERE c.customers_id NOT IN( %s ) ';
      break;

        case 'categories':
        $sql_selected = 'SELECT dc2c.categories_id AS id'.$display_fields.'
                         FROM discount_coupons_to_categories dc2c
                         LEFT JOIN categories_description cd
                         ON cd.categories_id=dc2c.categories_id
                         LEFT JOIN categories c
                         ON c.categories_id=cd.categories_id
                         WHERE dc2c.coupons_id="'.$coupons_id.'"
                         AND cd.language_id='.(int)$_SESSION['languages_id'];
        $sql_all = 'SELECT c.categories_id AS id'.$display_fields.'
                        FROM categories_description cd
                        LEFT JOIN categories c
                        ON c.categories_id=cd.categories_id
                        WHERE cd.language_id='.(int)$_SESSION['languages_id'] .'
                        %s';
        $where = ' AND c.categories_id NOT IN( %s ) ';
  break;
  
  case 'manufacturers':
        $sql_selected = 'SELECT m.manufacturers_id AS id'. $display_fields . '
                         FROM discount_coupons_to_manufacturers dc2m
                         LEFT JOIN manufacturers m
                         ON m.manufacturers_id=dc2m.manufacturers_id
                         WHERE dc2m.coupons_id= "'.$coupons_id.'"';
        /*$sql_selected = 'SELECT m.manufacturers_id AS id'.$display_fields.'
                          FROM discount_coupons_TO_MANUFACTURERS.' dc2m
                          LEFT JOIN manufacturers m
                            ON m.manufacturers_id=dc2m.manufacturers_id
                          LEFT JOIN manufacturers_info mi
                            ON mi.manufacturers_id=m.manufacturers_id
                          WHERE dc2m.coupons_id="'.$coupons_id.'"
                          AND mi.languages_id='.(int)$_SESSION['languages_id'];*/
        /*$sql_all = 'SELECT m.manufacturers_id AS id'.$display_fields.'
              FROM manufacturers_info mi
                                      LEFT JOIN manufacturers m
                ON m.manufacturers_id=mi.manufacturers_id
              WHERE mi.languages_id='.(int)$_SESSION['languages_id'] .'
                    %s';*/
        $sql_all = 'SELECT m.manufacturers_id AS id'.$display_fields.'
                    FROM manufacturers m
                      %s';                      
        $where = ' WHERE m.manufacturers_id NOT IN( %s ) ';
      break;

      case 'products':
      $sql_selected = 'SELECT p.products_id AS id'.$display_fields.'
                      FROM discount_coupons_to_products dc2p
                      LEFT JOIN products_description pd ON pd.products_id=dc2p.products_id
                      LEFT JOIN products p ON p.products_id=pd.products_id
                      WHERE dc2p.coupons_id="'.$coupons_id.'"
                      AND pd.language_id=' . (int)$_SESSION['languages_id'];

      $sql_all = 'SELECT p.products_id AS id'.$display_fields.'
                  FROM products_description pd LEFT JOIN products p ON p.products_id=pd.products_id
                  WHERE pd.language_id=' . (int)$_SESSION['languages_id'] . '
                  %s';
      $where = ' AND p.products_id NOT IN( %s )  ';
      break;
        
      case 'zones' :
        $sql_selected = 'SELECT dc2z.geo_zone_id AS id'.$display_fields.'
                          FROM discount_coupons_to_zones dc2z
                          LEFT JOIN geo_zones gz
                            USING( geo_zone_id )
                          WHERE dc2z.coupons_id="'.$coupons_id.'"';
        $sql_all = 'SELECT gz.geo_zone_id AS id'.$display_fields.'
                    FROM geo_zones gz
                    %s';
        $where = 'WHERE gz.geo_zone_id NOT IN(%s) ';
      break;

    }

    if( ( $selected_ids = $exclusion->osc_get_selected_options( $sql_selected, $separator /*category exclusions*/, $category_separator, ( $type == 'categories' ? $category_path : false )/*end category exclusions*/ ) ) === false ) osc_redirect( osc_href_link('discount_coupons.php', 'cID='.$coupons_id.'&error='.ERROR_DISCOUNT_COUPONS_SELECTED_LIST ) );

    $where = ( count( $selected_ids ) > 0 ? sprintf( $where, implode( ', ', $selected_ids ) ) : '' );
    $sql_all = sprintf( $sql_all, $where );
//debug
//print_r( '<pre>'.print_r( $sql_all, true ).'</pre>' );

    if( ( $exclusion->osc_get_all_options( $sql_all, $separator/*category exclusions*/, $category_separator, ( $type == 'categories' ? $category_path : false )/*end category exclusions*/, $selected_ids ) ) === false ) osc_redirect( osc_href_link('discount_coupons.php', 'cID='.$coupons_id.'&error='.ERROR_DISCOUNT_COUPONS_ALL_LIST ) );

// debug
//print_r( '<pre>'.print_r( $exclusion, true ).'</pre>' );

    }

  require('includes/header.php');
?>
<!-- body //-->
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div class="adminTitle">
    <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/coupon.gif', HEADING_TITLE, '40', '40'); ?></span>
    <span class="col-md-10 pageHeading pull-left"><?php echo '&nbsp;' . sprintf( HEADING_TITLE, $coupons_id ); ?></span>
     <span class="col-md-1 pull-right">
       <?php echo  '<a href="' . osc_href_link('discount_coupons.php', 'page=' . $_GET['page'] . (isset($_GET['cID']) ? '&cID=' . $_GET['cID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>
     </span>
  </div>
  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
  <div><?php echo $exclusion->display(); ?></div>

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');

