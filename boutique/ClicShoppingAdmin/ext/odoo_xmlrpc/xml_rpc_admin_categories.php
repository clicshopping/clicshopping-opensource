<?php
/*
 * xml_rpc_admin_customers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  $Qcategories = $OSCOM_PDO->prepare('select cd.categories_name,
                                             c.categories_id,
                                             c.parent_id,
                                             c.categories_image,
                                             cd.categories_description,
                                             cd.categories_head_title_tag,
                                             cd.categories_head_desc_tag,
                                             cd.categories_head_keywords_tag
                                     from :table_categories c,
                                          :table_categories_description cd
                                     where c.categories_id = :categories_id
                                     and c.categories_id = cd.categories_id
                                     and cd.language_id = :language_id
                                    ');
  $Qcategories->bindInt(':categories_id', (int)$categories_id );
  $Qcategories->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
  $Qcategories->execute();

  $category = $Qcategories->fetch();

  if (file_exists(DIR_FS_CATALOG_IMAGES . $category['categories_image'])) {
    $categories_image = DIR_FS_CATALOG_IMAGES . $category['categories_image'];
    $categories_image = file_get_contents($categories_image);
    $categories_image = base64_encode($categories_image);
  }

// **********************************
// Search odoo Categories id
// **********************************

     $ids = $OSCOM_ODOO->odooSearchByTwoCriteria('clicshopping_categories_id', '=', $category['categories_id'], 'product.category', 'int',
                                                 'clicshopping_categories_parent_id', '=', $category['parent_id'], 'int');

// **********************************
// read id Categories odoo
// **********************************

    $field_list = array('id');

    $Qcategories_id = $OSCOM_ODOO->readOdoo($ids, $field_list, 'product.category');
    $categories_id = $Qcategories_id[0][id];

    if  (empty($categories_id)) {

// **********************************
// Create products if doesn't exist in odoo
// **********************************

      $values = array ("name" => new xmlrpcval($category['categories_name'], "string"),
                       "type"  => new xmlrpcval('normal', "string"),
                       "complete_name"  => new xmlrpcval($category['categories_name'], "string"),
                       "clicshopping_categories_id" => new xmlrpcval($category['categories_id'], "int"),
                       "clicshopping_categories_parent_id" => new xmlrpcval($category['parent_id'], "int"),
                       "clicshopping_categories_image" => new xmlrpcval($categories_image, "string"),
                       "clicshopping_categories_description" => new xmlrpcval($category['categories_description'], "string"),
                       "clicshopping_categories_head_title_tag" => new xmlrpcval($category['categories_head_title_tag'], "string"),
                       "clicshopping_categories_head_desc_tag" => new xmlrpcval($category['categories_head_desc_tag'], "string"),
                       "clicshopping_categories_head_keywords_tag" => new xmlrpcval($category['categories_head_keywords_tag'], "string"),
                      );

      $OSCOM_ODOO->createOdoo($values, "product.category");


    }  else {

// **********************************
// update products if exist
// **********************************

      $id_list = array();
      $id_list[]= new xmlrpcval($categories_id, 'int');

      $values = array ("name" => new xmlrpcval($category['categories_name'], "string"),
                       "complete_name"  => new xmlrpcval($category['categories_name'], "string"),
                       "clicshopping_categories_id" => new xmlrpcval($category['categories_id'], "int"),
                       "clicshopping_categories_parent_id" => new xmlrpcval($category['parent_id'], "int"),
                       "clicshopping_categories_description" => new xmlrpcval($category['categories_description'], "string"),
                       "clicshopping_categories_head_title_tag" => new xmlrpcval($category['categories_head_title_tag'], "string"),
                       "clicshopping_categories_head_desc_tag" => new xmlrpcval($category['categories_head_desc_tag'], "string"),
                       "clicshopping_categories_head_keywords_tag" => new xmlrpcval($category['categories_head_keywords_tag'], "string"),
                      "clicshopping_categories_image" => new xmlrpcval($categories_image, "string"),
                     );

      $OSCOM_ODOO->updateOdoo($categories_id, $values, 'product.category');
    }

?>