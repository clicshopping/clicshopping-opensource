<?php
/**
 * Product_archive.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/


define('HEADING_TITLE','Product archive management');
define('HEADING_TITLE_SEARCH','Search');

define('TABLE_HEADING_MODEL_ARCHIVES','Model');
define('TABLE_HEADING_DATE_ARCHIVES','Archive Date');
define('TABLE_HEADING_PRODUCTS_ARCHIVES','Products');
define('TABLE_HEADING_STATUS','Product available');
define('TABLE_HEADING_ACTION','Actions');
define('TEXT_HEADING_EDIT_PRODUCTS_ARCHIVE','Activate the product');
define('TEXT_EDIT_INTRO','Do you wish activate this product ?');
define('TEXT_ARCHIVE','<strong>Note : </strong>The reactivation article will display the product in the admin catalog at the same place where he was previously. <br /> Depending on the status, the product will be displayed or not in the catalog client side.');

?>