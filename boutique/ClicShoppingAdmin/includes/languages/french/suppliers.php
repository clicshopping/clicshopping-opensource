<?php
/**
 * suppliers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Gestion des fournisseurs');

define('TABLE_HEADING_SUPPLIERS', 'Nom du Fournisseur');

define('TABLE_HEADING_MANAGER','Nom responsable');
define('TABLE_HEADING_PHONE','Téléphone');
define('TABLE_HEADING_FAX','FAX');
define('TABLE_HEADING_EMAIL_ADDRESS','Email');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS','Status');

define('TITLE_SUPPLIERS_GENERAL','Informations généralres');
define('TITLE_SUPPLIERS_IMAGE','Image du fournisseur');
define('TAB_VISUEL','Image');
define('TAB_SUPPLIERS_NOTE','Notes');

define('TEXT_HEADING_NEW_SUPPLIERS', 'Nouveau fournisseur');
define('TEXT_HEADING_EDIT_SUPPLIERS', 'Editer fournisseur');
define('TEXT_HEADING_DELETE_SUPPLIERS', 'Supprimer fournisseur');

define('TEXT_SUPPLIERS', 'Fournisseurs :');
define('TEXT_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_PRODUCTS', 'Produits :');
define('TEXT_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');

define('TEXT_NEW_INTRO', 'Merci de compléter les informations sur le nouveau fournisseur');
define('TEXT_EDIT_INTRO', 'Merci de faire les changements nécessaires');

define('TEXT_SUPPLIERS_NAME', 'Nom du fournisseur :');
define('TEXT_SUPPLIERS_NEW_IMAGE', 'Nouvelle image du fournisseur :');
define('TEXT_SUPPLIERS_IMAGE', 'Image actuel du fournisseur :');
define('TEXT_SUPPLIERS_DELETE_IMAGE', 'Supprimer l\'image fournisseur');
define('HELP_IMAGE_SUPPLIERS', '* Veuillez à ne pas mettre plus d\'une image dans la section "Nouvelle image".<br /><br />* Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus).');

define('TEXT_SUPPLIERS_URL', 'URL du fournisseur :');

define('TEXT_SUPPLIERS_MANAGER', 'Nom responsable :');
define('TEXT_SUPPLIERS_PHONE', 'Télephone :');
define('TEXT_SUPPLIERS_FAX', 'Fax :');
define('TEXT_SUPPLIERS_EMAIL_ADDRESS','Adresse email :');
define('TEXT_SUPPLIERS_ADDRESS', 'Adresse :');
define('TEXT_SUPPLIERS_SUBURB', ' Addresse complémentaire :');
define('TEXT_SUPPLIERS_POSTCODE', 'Code Postale : ');
define('TEXT_SUPPLIERS_CITY', 'Ville :');
define('TEXT_SUPPLIERS_STATES', 'Etat / Département :');
define('TEXT_SUPPLIERS_COUNTRY', 'Pays :');
define('TEXT_SUPPLIERS_NOTES','Notes complémentaires');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE','Image');
define('TEXT_PRODUCTS_IMAGE_VISUEL','Prévisualition de l\'Image');
define('TEXT_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer ce fournisseur ?');
define('TEXT_DELETE_IMAGE', 'Suppprimer l\'image du fournisseur ?');
define('TEXT_DELETE_PRODUCTS', 'Supprimer tous les produits de ce fournisseur ? (en incluant les critiques, produits en promotion, produits à venir)');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>ATTENTION :</strong> Il reste %s produit(s) liées à ce fournisseur !');


define('TITLE_AIDE_IMAGE', 'Notes sur l\'utilisation des images');
define('HELP_IMAGE_SUPPLIERS', '<li>Veuillez à ne pas mettre plus d\'une image dans la sections "Image vignette de la catégorie".</li><li>Si vous avez inséré des plugins dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');

define('TITLE_HELP_DESCRIPTION', 'Notes sur l\'utilisation du wysiwyg');
define('HELP_DESCRIPTION', '<li>Si vous avez inséré des plugin dans Firefox, il peut y avoir des incompatibilités (ex : adblock plus)</li>');


define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Erreur : Impossible d\'écrire dans le répertoire. Merci de modifier les droits d\'accès sur : %s');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Erreur : Le répertoire n\'existe pas : %s');
?>