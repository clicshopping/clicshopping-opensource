<?php
/**
 * reviews.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Commentaires des clients');

define('TABLE_HEADING_PRODUCTS', 'Produits');
define('TABLE_HEADING_RATING', 'Evaluations');
define('TABLE_HEADING_DATE_ADDED', 'Date d\'ajout');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_APPROVED','Approuver ?');
define('TABLE_HEADING_REVIEW_AUTHOR','Auteur');						
define('TABLE_HEADING_PRODUCTS_AVERAGE_RATING','Moyenne');
define('TABLE_HEADING_REVIEW_READ','Lu');					
define('TABLE_HEADING_LAST_MODIFIED','Modifié');
define('ICON_PREVIEW_COMMENT','Prévisualiser le commentaire');
define('ENTRY_PRODUCT', 'Nom du produit :');
define('ENTRY_FROM', 'Nom du client :');
define('ENTRY_DATE', 'Date de l\'avis :');
define('ENTRY_REVIEW', 'Avis du client :');
define('ENTRY_REVIEW_TEXT', '<small><font color="#ff0000"><strong>REMARQUE :</strong></font></small>&nbsp;Le code HTML n\'est pas traduit !&nbsp;');
define('ENTRY_RATING', 'Evaluations :');
define('ENTRY_STATUS', 'Statut :');
define('ENTRY_STATUS_YES', 'Accepter le commentaire');
define('ENTRY_STATUS_NO', 'Refuser le commentaire');
define('ENTRY_ACTUAL_STATUS', '<br />Statut en cours : ');
define('ENTRY_STATUS_ACTUAL_YES', 'Commentaire accepté');
define('ENTRY_STATUS_ACTUAL_NO', 'Commentaire refusé');

define('TEXT_INFO_DELETE_REVIEW_INTRO', 'Voulez vous vraiment effacer ce commentaire ?');

define('TEXT_INFO_DATE_ADDED', 'Date d\'ajout :');
define('TEXT_INFO_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_INFO_IMAGE_NONEXISTENT', 'L\'IMAGE N\'EXISTE PAS');
define('TEXT_INFO_REVIEW_AUTHOR', 'Auteur :');
define('TEXT_INFO_REVIEW_RATING', 'Classement :');
define('TEXT_INFO_REVIEW_READ', 'Lu :');
define('TEXT_INFO_REVIEW_SIZE', 'Taille :');
define('TEXT_INFO_PRODUCTS_AVERAGE_RATING', 'Moyenne :');

define('TEXT_OF_5_STARS', '%s sur 5 étoiles !');
define('TEXT_GOOD', '<small><font color="#ff0000"><strong>BON</strong></font></small>');
define('TEXT_BAD', '<small><font color="#ff0000"><strong>MAUVAIS</strong></font></small>');
define('TEXT_INFO_HEADING_DELETE_REVIEW', 'Effacer commentaires');

define('TITLE_REVIEWS_GENERAL', 'Informations générales');
define('TITLE_REVIEWS_ENTRY', 'Avis du client');
define('TITLE_REVIEWS_RATING', 'Classement');
?>