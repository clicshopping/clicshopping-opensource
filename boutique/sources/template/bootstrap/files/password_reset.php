<?php
/*
 * password_reset.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('password_forgotten') ) {
    echo $OSCOM_MessageStack->get('password_forgotten');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '10', '30'); ?></div>
<?php
  if ( $OSCOM_MessageStack->exists('password_reset') ) {
    echo $OSCOM_MessageStack->get('password_reset');
  }

  echo osc_draw_form('password_reset', osc_href_link('password_reset.php', 'account=' . $email_address . '&key=' . $password_key . '&action=process', 'SSL'), 'post', '', true);
?>

<div class="contentContainer">
  <div class="contentText">
    <div><?php echo TEXT_MAIN; ?></div>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
    <div class="contentText">
      <div class="row">
        <div class="form-group has-feedback">
          <label class="control-label col-sm-4" for="password"><?php echo ENTRY_PASSWORD; ?></label>
          <div class="col-sm-6">
            <?php echo osc_draw_input_field('password', null, 'required aria-required="true"  autofocus="autofocus" id="password" placeholder="'.ENTRY_PASSWORD.'"', 'password'); ?>
          </div>
        </div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-4" for="confirmation"><?php echo ENTRY_PASSWORD_CONFIRMATION; ?></label>
          <div class="col-sm-6">
            <?php echo osc_draw_input_field('confirmation', null, 'required aria-required="true" id="confirmation" placeholder="'.ENTRY_PASSWORD_CONFIRMATION.'"', 'password'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="buttonSet">
      <div class="text-right"><?php echo  osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null); ?></div>
    </div>
</div>
</form>
<?php
  require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>
