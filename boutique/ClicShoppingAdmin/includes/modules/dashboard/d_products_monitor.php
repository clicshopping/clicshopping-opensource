<?php
/*
 * d_products_monitor.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/


  class d_products_monitor {
    var $code = 'd_products_monitor';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function d_products_monitor() {
      $this->title = MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_TITLE;
      $this->description = MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_DESCRIPTION;

      if ( defined('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_STATUS') ) {
        $this->sort_order = MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_SORT_ORDER;
        $this->enabled = (MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_STATUS == 'True');
      }
    }

    function getOutput() {
      global $OSCOM_PDO;

      $Qproducts = $OSCOM_PDO->prepare('select distinct pd.products_id,
                                                        p.products_last_modified,
                                                        p.products_model,
                                                        p.products_tax_class_id,
                                                        p.products_image,
                                                        p.products_price,
                                                        pd.products_name
                                        from :table_products_description pd left join :table_products p on (pd.products_id = p.products_id)
                                        where p.products_model = :products_model
                                         or p.products_model is null
                                         or p.products_image = :products_image
                                         or p.products_image is null
                                         or p.products_tax_class_id is null
                                         or p.products_tax_class_id = :products_tax_class_id
                                         or p.products_price is null
                                        and pd.language_id = :language_id
                                        and p.products_status = :products_status
                                        order by p.products_last_modified desc
                                        limit 6
                                      ');
      $Qproducts->bindValue(':products_model', '');
      $Qproducts->bindValue(':products_image', '');
      $Qproducts->bindInt(':products_tax_class_id', 0);
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
      $Qproducts->bindInt(':products_status',1 );

      $Qproducts->execute();


      if ($Qproducts->rowCount() == 0) {
        $output ='';
      } else {

        $output = '<div class="clearfix"></div>';

        $output .= '<table class="table table-hover table-responsive adminformTitle">';
        $output .= '<thead>';
        $output .= '<tr class="dataTableHeadingRow">';
        $output .= '<th class="dataTableHeadingContent" width="50">' . MODULE_ADMIN_DASHBOARD_PRODUCTS_INFO_PRODUCTS_ID . '</th>';
        $output .= '<th class="dataTableHeadingContent" width="250">' . MODULE_ADMIN_DASHBOARD_PRODUCTS_INFO_PRODUCTS_NAME . '</th>';
        $output .= '<th class="dataTableHeadingContent" width="300">' . MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_PRODUCTS_ERRORS . '</th>';
        $output .= '<th class="dataTableHeadingContent" width="230" align="center" colspan="2">' . MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_PRODUCTS_PRODUCTS_LAST_MODIFIED . '</th>';
        $output .= '</tr>';
        $output .= '</thead>';

        while ($products = $Qproducts->fetch() ) {

          $output .= '  <tr class="dataTableRow" onmouseover="rowOverEffect(this);" onmouseout="rowOutEffect(this);">' .
                     '    <td class="dataTableContent">' . (int)$products['products_id'] . ' </td> '.
                     '    <td class="dataTableContent"><a style="color:red;" href="' . osc_href_link('categories.php', 'pID=' . (int)$products['products_id'] . '&action=new_product') . '">' . osc_output_string_protected($products['products_name']) . '</td>';

          $err_list = '';
          $list_no = false;
          if (!osc_not_null($products['products_model'])) {
                $err_list .= MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_MODEL;
                $list_no = true;
          }
          if (!osc_not_null($products['products_image'])) {
                if ($list_no) {
                  $err_list .= ', ';
                }
                $err_list .= MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_PICTURE;
                $list_no = true;
          }
          if ($products['products_tax_class_id'] == 0) {
                if ($list_no) {
                  $err_list .= ', ';
                }
                $err_list .= MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_TAX;
                $list_no = true;
          }
          if ($products['products_price'] == 0) {
                if ($list_no) {
                  $err_list .= ', ';
                }
                $err_list .=  MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_NO_PRICE;
          }

          $output .= '<td class="dataTableContent">' . $err_list . '</td>';
          $output .= '<td class="dataTableContent" align="right">' . $products['products_last_modified'] . '</td>';
          $output .= '<td class="dataTableContent" align="right"><a href="' . osc_href_link('categories.php', 'pID=' . (int)$products['products_id'] . '&action=new_product') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a></td>' ;

          $output .=  '</tr>';               
        } // end while

        $output .= '</table>'; 

        $output .= '<div class="clearfix"></div>';
        $output .= osc_draw_separator('pixel_trans.gif', '1', '10');

      }
   

      return $output;
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_STATUS');
    }

    function install() {
     global $OSCOM_PDO;
     
      if ($_SESSION['languages_id'] =='1') {
        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Souhaitez vous activer ce module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Souhaitez vous activer ce module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Ordre de tri d\'affichage',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_SORT_ORDER',
            'configuration_value' => '500',
            'configuration_description' => 'Ordre de tri pour l\'affichage (Le plus petit nombre est montr� en premier)',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );

      } else {

        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Do you want enable this Module ?',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_STATUS',
            'configuration_value' => 'True',
            'configuration_description' => 'Do you want enable this Module ?',
            'configuration_group_id' => '6',
            'sort_order' => '1',
            'set_function' => 'osc_cfg_select_option(array(\'True\', \'False\'), ',
            'date_added' => 'now()'
          ]
        );


        $OSCOM_PDO->save('configuration', [
            'configuration_title' => 'Sort Order',
            'configuration_key' => 'MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_SORT_ORDER',
            'configuration_value' => '500',
            'configuration_description' => 'Sort order of display. Lowest is displayed first.',
            'configuration_group_id' => '6',
            'sort_order' => '2',
            'set_function' => '',
            'date_added' => 'now()'
          ]
        );
      }
    }

    function remove() {
      osc_db_query("delete from configuration where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_STATUS', 
                   'MODULE_ADMIN_DASHBOARD_PRODUCTS_MONITOR_SORT_ORDER');
    }
  }
