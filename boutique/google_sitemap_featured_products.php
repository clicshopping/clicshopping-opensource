<?php
/*
 * google_sitemap_featured_products.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

  include ('includes/application_top.php');

  if (MODE_VENTE_PRIVEE == 'false') {

    $xml = new SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n".'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

    $products_array = array();

    $QproductsFeatured = $OSCOM_PDO->prepare('select products_id,
                                                     products_featured_date_added as last_modified
                                                from products_featured
                                                where status = :status
                                                and customers_group_id = :customers_group_id
                                                order by last_modified DESC
                                               ');

    $QproductsFeatured->bindValue(':status', '1');
    $QproductsFeatured->bindValue(':customers_group_id', '0');
    $QproductsFeatured->execute();

    while ($products_featured = $QproductsFeatured->fetch() ) {
      $products_array[$products_featured['products_id']]['loc'] = osc_href_link('product_info.php', 'products_id=' . (int)$products_featured['products_id'], 'NONSSL', false);
      $products_array[$products_featured['products_id']]['lastmod'] = $products_featured['last_modified'];
      $products_array[$products_featured['products_id']]['changefreq'] = 'weekly';
      $products_array[$products_featured['products_id']]['priority'] = '0.5';
    }

    foreach ($special_array as $k => $v) {
      $url = $xml->addChild('url');
      $url->addChild('loc', $v['loc']);
      $url->addChild('lastmod', date("Y-m-d", strtotime($v['lastmod'])));
      $url->addChild('changefreq', 'weekly');
      $url->addChild('priority', '0.5');
    }

    header('Content-type: text/xml');
    echo $xml->asXML();
  }