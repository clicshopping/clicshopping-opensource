<?php
/**
 * french.php 
 * @copyright Copyright 2006 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

// look in your $PATH_LOCALE/locale directory for available locales..
// on RedHat6.0 I used 'en_US'
// on FreeBSD 4.0 I use 'en_US.ISO_8859-1'
// this may not work under win32 environments..
//setlocale(LC_TIME, 'en_US.ISO_8859-1');

setlocale(LC_ALL, array('en_US.UTF-8', 'en_US.UTF8', 'enu_usa'));
define('DATE_FORMAT_SHORT', '%m/%d/%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
define('DATE_FORMAT', 'm/d/Y'); // this is used for date()
define('PHP_DATE_TIME_FORMAT', 'm/d/Y H:i:s'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('JQUERY_DATEPICKER_I18N_CODE', 'en');
define('JQUERY_DATEPICKER_FORMAT', 'mm/dd/yy');

////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function osc_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 3, 2) . substr($date, 0, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 0, 2) . substr($date, 3, 2);
  }
}

// Global entries for the <html> tag
define('HTML_PARAMS','dir="ltr" lang="en"');

// charset for web pages and emails
define('CHARSET', 'UTF-8');

// page title
define('TITLE', ''.STORE_NAME.'');

// header text in includes/header.php
define('HEADER_TITLE_TOP', 'Administration');
define('HEADER_TITLE_SUPPORT_SITE', ' ');
define('HEADER_TITLE_ONLINE_CATALOG', 'Online Catalog');
define('HEADER_TITLE_ADMINISTRATION', 'Administration');

// text for gender
define('MALE', 'Male');
define('FEMALE', 'Female');

// text for date of birth example
define('DOB_FORMAT_STRING', 'mm/dd/yyyy');

// menu de la page d'accueil
define('BOX_HEADING_CONFIGURATION', 'Configuration');
define('BOX_CONFIGURATION_MYSTORE', 'My Store');
define('BOX_CONFIGURATION_LOGGING', 'Logging');
define('BOX_CONFIGURATION_CACHE', 'Cache');
define('BOX_CONFIGURATION_ADMINISTRATORS', 'Administrators');
define('BOX_MODULES_ADMIN_DASHBOARD', 'Dashboard Administration');
define('BOX_HEADING_MODULES', 'Modules');
define('BOX_MODULES_ACTION_RECORDER', 'Action Recorder');
define('BOX_MODULES_HEADER_TAGS', 'Header Tags');
define('BOX_MODULES_SOCIAL_BOOKMARKS', 'Social Networks');
define('BOX_MODULES_PAYMENT', 'Payment');
define('BOX_MODULES_SHIPPING', 'Shipping');
define('BOX_MODULES_ORDER_TOTAL', 'Order total');
define('BOX_CATALOG_MANUFACTURERS', 'Brands');
define('BOX_CUSTOMERS_CUSTOMERS', 'Customers');
define('BOX_CUSTOMERS_ORDERS', 'Orders');
define('BOX_TAXES_COUNTRIES', 'Countries');
define('BOX_TAXES_GEO_ZONES', 'Tax Zones');
define('BOX_LOCALIZATION_CURRENCIES', 'Currencies');
define('BOX_LOCALIZATION_LANGUAGES', 'Languages');
define('BOX_HEADING_TOOLS', 'Tools');

// menu accueil et support
define('MENU_HOME', 'Home');
define('MENU_HOME_STORE', 'Store');
define('MENU_HOME_ADMIN', 'Backoffice');
define('MENU_HELP', 'Support');
define('MENU_HELP_CREDIT','Credit');

// menu configuration
define('BOX_HEADING_CONFIGURATION', 'Configuration');
define('MENU_CONFIGURATION_GENERAL', 'General configuration');
define('MENU_CONFIGURATION_ADMINISTRATOR','Administrator members');
define('MENU_CONFIGURATION_LEGACY','Regulation Law');
define('MENU_CONFIGURATION_STORE', 'My store');
define('MENU_CONFIGURATION_BANK', 'Credit card configuration');
define('MENU_CONFIGURATION_MINIMUM', 'Minimum values management');
define('MENU_CONFIGURATION_MAXIMUM', 'Maximum - minimum values management');
define('MENU_CONFIGURATION_IMAGES', 'Images management');
define('MENU_CONFIGURATION_CUSTOMERS', 'Details customers');
define('MENU_CONFIGURATION_SHIPPING', 'Shipping/Handling');
define('MENU_CONFIGURATION_INVOICE', 'Invoice/Packaging');
define('MENU_CONFIGURATION_LISTING_PRODUCTS', 'Products listing');
define('MENU_CONFIGURATION_STOCK', 'Stock management');
define('MENU_CONFIGURATION_MAIL', 'Email management');
define('MENU_CONFIGURATION_DOWNLOAD', 'Download');
define('MENU_CONFIGURATION_REFERENCEMENT_META_DATA','Meta data');
define('MENU_CONFIGURATION_MODULES', 'Modules');
define('MENU_CONFIGURATION_MODULES_ACTION_RECORDER','Security');
define('MENU_CONFIGURATION_MODULES_HOOKS','Hooks');
define('MENU_CONFIGURATION_MODULES_SOCIAL_BOOKMARKS','Social networks activation');
define('MENU_CONFIGURATION_MODULES_HEADER_TAG','Meta Data Activation');
define('MENU_CONFIGURATION_MODULES_PAYMENT', 'Payment');
define('MENU_CONFIGURATION_MODULES_SHIPPING', 'Shipping');
define('MENU_CONFIGURATION_MODULES_ORDERTOTAL', 'Order total');
define('MENU_CONFIGURATION_B2C', 'Management customers B2C');
define('MENU_CONFIGURATION_B2C_GESTION', 'Configuration');
define('MENU_CONFIGURATION_B2C_MINIMUM', 'Values minimum');
define('MENU_CONFIGURATION_B2B', 'Management customers B2B');
define('MENU_CONFIGURATION_B2B_GESTION', 'Configuration');
define('MENU_CONFIGURATION_B2B_CUSTOMERS', 'Details customers');
define('MENU_CONFIGURATION_B2B_MINIMUM', 'Values minimum');
define('MENU_CONFIGURATION_B2B_MAXIMUM', 'Values maximum');
define('MENU_CONFIGURATION_QUICK_UPDATES','Quick Update');
define('MENU_CONFIGURATION_SESSION', 'Session & Cache');
define('MENU_CONFIGURATION_SESSION_CACHE', 'Cache');
define('MENU_CONFIGURATION_SESSION_CONTROLE_CACHE', 'Cache control');
define('MENU_CONFIGURATION_SESSION_GZIP', 'GZip compression');
define('MENU_CONFIGURATION_SESSION_LOGGING', 'Logging');
define('MENU_CONFIGURATION_SESSION_SESSION', 'Session');
define('MENU_CONFIGURATION_SEO','SEO URL site management');
define('MENU_CONFIGURATION_PRODUCTS_INFO','Producs display description');
define('MENU_CONFIGURATION_EXPORT','Datas import / export password');
define('MENU_CONFIGURATION_SOCIAL_WEB','Web social configuration');
define('MENU_CONFIGURATION_PRODUCTS_QUANTITY_UNIT','Quantity unit management');
define('MENU_CONFIGURATION_WEBSERVICE','Web Service');

// Menu Referencement et Statistiques
define('MENU_CONFIGURATION_REFERENCEMENT_STATS', 'Search engine / networking');
define('MENU_CONFIGURATION_REFERENCEMENT_META_DATA', 'Meta-data configuration');
define('MENU_CONFIGURATION_STATISTIC_REFERENCEMENT', 'Submitconfiguration');
define('MENU_CONFIGURATION_ORDERS_STATUS_TRACKING','Shipping tracking');

// menu Configuration BackOffice
define('MENU_CONFIGURATION_STORE_ADMINISTRATION','My backoffice');
define('MENU_PRINT_ORDERS_STATUS_INVOICE', 'Orders/Invoices PDF print status');
define('MENU_EDIT_ORDERS_STATUS_INVOICE', 'Orders/Invoices edition');

// menu catalogue
define('BOX_HEADING_CATALOG', 'Catalog');
define('MENU_CATALOG_CATEGORIES_PRODUCTS', 'Categories and Products');
define('MENU_CATALOG_PRICE_UPDATE', 'Quick update');
define('MENU_CATALOG_PRODUCTS_ATTRIBUTES', 'Products attributes management');
define('MENU_CATALOG_MANUFACTURERS', 'Brands management');
define('MENU_CATALOG_SUPPLIERS', 'Purchase suppliers management');
define('MENU_CATALOG_REVIEWS', 'Customers Comments');
define('MENU_CATALOG_PRODUCTS_EXTRA_FIELDS', 'Extra fields management');
define('MENU_CATALOG_PRODUCTS_ARCHIVES','Archives products management');

// menu clients
define('BOX_HEADING_CUSTOMERS', 'Customers');
define('MENU_CUSTOMERS_ORDERS', 'Orders');
define('MENU_CUSTOMERS', 'Customers');
define('MENU_CUSTOMERS_MEMBERS_B2B', 'Customers to be validated');
define('MENU_CUSTOMERS_GROUP_B2B', 'Customers group');
define('MENU_CUSTOMERS_MAIL', 'Send an email');
define('MENU_CONTACT_CUSTOMERS','Customers contact service');

// menu marketing
define('BOX_HEADING_MARKETING','Marketing');
define('MENU_CATALOG_SPECIALS', 'Specials products');
define('MENU_MARKETING_DISCOUNT_COUPONS','Discount Coupons');
define('MENU_MARKETING_PRODUCTS_HEART','Favorites Products');
define('MENU_MARKETING_PRODUCTS_FEATURED','Featured products');
define('MENU_MARKETING_PRODUCTS_RELATED','Cross sell & related products');
define('MENU_MARKETING_BANNER_MANAGER', 'Banner manager');
define('MENU_MARKETING_META_DATA','Meta data management');
define('MENU_REFERENCEMENT','Marketing management');

// menu Communication
define('BOX_HEADING_COMMUNICATION','Communication');
define('MENU_COMMUNICATION_NEWSLETTER', 'Newsletters');
define('MENU_COMMUNICATION_PAGE_INFORMATION','Main Page & Informations');
define('MENU_COMMUNICATION_BLOG','Blog');


// menu design
define('BOX_HEADING_DESIGN','Design');

define('MENU_DESIGN_ACCOUNT_CUSTOMERS','Customers information');
define('MENU_DESIGN_ADVANCED_SEARCH','Advanced search');
define('MENU_CONFIGURATION_DESIGN','Design configuration');
define('MENU_DESIGN_BOXES','Left and right boxes');
define('MENU_DESIGN_BLOG','Blog');
define('MENU_DESIGN_BLOG_CONTENT','Blog content');
define('MENU_DESIGN_CATEGORIES','Categories');
define('MENU_DESIGN_COMMENTS','Comments');
define('MENU_DESIGN_COMMUNICATION','Communication');
define('MENU_DESIGN_CONFIRMATION','Order confirmation');
define('MENU_DESIGN_CONTACT_US','Contact us');
define('MENU_DESIGN_CREATE_ACCOUNT','Customer create account (B2C)');
define('MENU_DESIGN_CREATE_ACCOUNT_PRO','Customer create account professionnal (B2B)');
define('MENU_DESIGN_CUSTOMERS','Customers account');
define('MENU_DESIGN_FOOTER','Catalog footer');
define('MENU_DESIGN_HEADER','Catalog header');
define('MENU_DESIGN_INDEX','Shop index / categories');
define('MENU_DESIGN_LAYOUT','Layout');
define('MENU_DESIGN_LOGIN_CUSTOMERS','Login customers');
define('MENU_DESIGN_LISTING','Listing');
define('MENU_DESIGN_PRODUCTS_LISTING_SORT_ORDER','Display the sort order');
define('MENU_DESIGN_MISCELLANEOUS','Miscellaneous');
define('MENU_DESIGN_NEW','Index');
define('MENU_DESIGN_NEW_PRODUCTS', 'Main pages & news');
define('MENU_DESIGN_PAYMENT','Payment choice');
define('MENU_DESIGN_PAYMENT_PROCESS','Order process');
define('MENU_DESIGN_PRODUCTS_FEATURED', 'Featured products');
define('MENU_DESIGN_PRODUCTS_HEART','Favorites products listing');
define('MENU_DESIGN_PRODUCTS_INFO', 'Products description');
define('MENU_DESIGN_PRODUCTS_INFO_PRODUCTS_RELATED','Products related');
define('MENU_DESIGN_PRODUCTS_INFO_CROSS_SELL','Cross sell products');
define('MENU_DESIGN_PRODUCTS_INFO_ALSO_PURCHASED','Also purchased');
define('MENU_DESIGN_PRODUCTS_LISTING', 'Products listing');
define('MENU_DESIGN_PRODUCTS_NEW', 'Products news Listing');
define('MENU_DESIGN_PRODUCTS_SPECIAL', 'Products special listing');
define('MENU_DESIGN_SPECIAL','Index specials');
define('MENU_DESIGN_SHOPPING_CART','Order shopping cart');
define('MENU_DESIGN_SHIPPING','Shipping choice');
define('MENU_DESIGN_SITEMAP','Sitemap');
define('MENU_DESIGN_SUCESS','Order success');

// menu lieu & taxes
define('BOX_HEADING_LOCATION_AND_TAXES', 'Location & Taxes');
define('MENU_TAXES_COUNTRIES', 'Countries');
define('MENU_TAXES_ZONES', 'Zones');
define('MENU_TAXES_GEO_ZONE', 'Tax zones');
define('MENU_TAXES_TAX_CLASSES', 'Tax classes');
define('MENU_TAXES_TAX_RATES', 'Tax rates');

// menu localisation
define('BOX_HEADING_LOCALIZATION', 'Others');
define('MENU_LOCALIZATION_CURRENCIES', 'Currencies');
define('MENU_LOCALIZATION_LANGUAGES', 'Languages');
define('MENU_LOCALIZATION_ORDERS_STATUS', 'Orders status');


// menu rapports
define('BOX_HEADING_REPORTS', 'Reports');
define('MENU_REPORTS_EMAIL_VALIDATION', 'Emails analyse');
define('MENU_REPORTS_FINANCIAL','Financial reports');
define('MENU_REPORTS_MARGIN_REPORT','Margin report');
define('MENU_REPORTS_PRODUCTS_NO_VIEWED', 'Products no viewed');
define('MENU_REPORTS_PRODUCTS_VIEWED', 'Products viewed');
define('MENU_REPORTS_PRODUCTS_NO_PURCHASED', 'Products no purchased');
define('MENU_REPORTS_PRODUCTS_PURCHASED', 'Products purchased');
define('MENU_REPORTS_ORDERS_TOTAL', 'Customer orders-total');
define('MENU_REPORTS_PRODUCTS_EXPECTED', 'Products expected');
define('MENU_REPORTS_PRODUCTS_LOW_STOCK', 'Low Stock');
define('MENU_REPORTS_PRODUCTS_NOTIFICATIONS','Products notifications');
define('MENU_REPORTS_SUPPLIERS','Orders to suppliers');
define('MENU_REPORTS_DISCOUNT','Coupons report');
define('MENU_REPORTS_NEWSLETTER_NO_ACCOUNT','Newsletter subscription with no account');
define('MENU_REPORTS_STATS','Products statitics report');
define('MENU_REPORTS_STATS_FINANCIAL_MANAGEMENT','Financial management report');
define('MENU_REPORTS_STATS_PRODUCTS_MANAGEMENT','Products management report');
define('MENU_REPORTS_PRODUCTS_WHAREHOUSE','Products wharehouse');

// Menu Market Place
define('MENU_MARKETPLACE','Market Place');

// menu outils
define('BOX_HEADING_TOOLS', 'Tools');
define('BOX_HEADING_EDITOR_HTML', 'HTML Editor');

define('MENU_TOOLS_BACKUP', 'Database backup');
define('MENU_TOOLS_DATABASE_TABLE','Database Analyse');
define('MENU_TOOLS_DEFINE_LANGUAGE', 'Define languages');
define('MENU_TOOLS_EDITOR_HTML','HTML Editor');
define('MENU_TOOLS_EDIT_CSS','CSS Editor');
define('MENU_TOOLS_EDIT_HTMl','Content HTML Editor');
define('MENU_TOOLS_EDIT_TEMPLATE_PRODUCTS','Template products Editor');
define('MENU_TOOLS_EXPORT','Datas exportation');
define('MENU_TOOLS_EXPORT_COMPARATEUR','price comparison');
define('MENU_TOOLS_EXPORT_DATAS','Datas Export');
define('MENU_TOOLS_FILE_MANAGER', 'File manager');
define('MENU_TOOLS_IMPORT_DATAS','Datas Import');
define('MENU_TOOLS_MODULE_INSTALLATION','Modules installation');
define('MENU_TOOLS_SEC_DIR_PERMISSIONS','Security Directory Permissions');
define('MENU_TOOLS_SERVER_INFO', 'Server info');
define('MENU_TOOLS_SECURITY', 'Security Informations');
define('MENU_TOOLS_SECURITY_SERVER_INFO','Server info');
define('MENU_TOOLS_SECURITY_CLICSHOPPING','ClicShopping Information');
define('MENU_TOOLS_SECURITY_RECORDER', 'Action recorder');
define('MENU_TOOLS_WHOS_ONLINE', 'Customers online');
define('MENU_TOOLS_TEMPLATE_EMAIL','Email template');

//menu Support
define('MENU_HELP_IMAGINIS','ClicShopping Help');
define('BOX_HEADING_HELP','Support');
define('MENU_EMAIL_ACCESS','My email');

// Menu groupe client
define('BOX_CATALOG_CATEGORIES_PRICEUPDATE', 'Quick update');
define('BOX_CUSTOMERS_APPROVAL', 'Waiting approval');
define('BOX_CUSTOMERS_GROUPS', 'Groups');


// javascript messages
define('JS_ERROR', 'Errors have occured during the process of your form!\nPlease make the following corrections:\n\n');

define('JS_OPTIONS_VALUE_PRICE', '* The new product atribute needs a price value\n');
define('JS_OPTIONS_VALUE_PRICE_PREFIX', '* The new product atribute needs a price prefix\n');

define('JS_PRODUCTS_NAME', '* The new product needs a name\n');
define('JS_PRODUCTS_DESCRIPTION', '* The new product needs a description\n');
define('JS_PRODUCTS_PRICE', '* The new product needs a price value\n');
define('JS_PRODUCTS_WEIGHT', '* The new product needs a weight value\n');
define('JS_PRODUCTS_QUANTITY', '* The new product needs a quantity value\n');
define('JS_PRODUCTS_MODEL', '* The new product needs a model value\n');
define('JS_PRODUCTS_IMAGE', '* The new product needs an image value\n');

define('JS_SPECIALS_PRODUCTS_PRICE', '* A new price for this product needs to be set\n');

define('JS_GENDER', '* The \'Gender\' value must be chosen.\n');
define('JS_FIRST_NAME', '* The \'First Name\' entry must have at least ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters.\n');
define('JS_LAST_NAME', '* The \'Last Name\' entry must have at least ' . ENTRY_LAST_NAME_MIN_LENGTH . ' characters.\n');
define('JS_DOB', '* The \'Date of Birth\' entry must be in the format: xx/xx/xxxx (month/date/year).\n');

define('JS_EMAIL_ADDRESS', '* The \'E-Mail Address\' is not correct.\n');
define('JS_ADDRESS', '* The \'Street Address\' entry must have at least ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' characters.\n');
define('JS_POST_CODE', '* The \'Post Code\' entry must have at least ' . ENTRY_POSTCODE_MIN_LENGTH . ' characters.\n');
define('JS_CITY', '* The \'City\' entry must have at least ' . ENTRY_CITY_MIN_LENGTH . ' characters.\n');
define('JS_STATE', '* The \'State\' entry is must be selected.\n');
define('JS_STATE_SELECT', '-- Select Above --');
define('JS_ZONE', '* The \'State\' entry must be selected from the list for this country.');
define('JS_COUNTRY', '* The \'Country\' value must be chosen.\n');
define('JS_TELEPHONE', '* The \'Telephone Number\' entry must have at least ' . ENTRY_TELEPHONE_MIN_LENGTH . ' characters.\n');
define('JS_CELLULAR_PHONE', '* The \'Cellular phone Number\' entry must have at least ' . ENTRY_CELLULAR_PHONE_MIN_LENGTH . ' characters.\n');

define('JS_PASSWORD', '* The \'Password\' amd \'Confirmation\' entries must match amd have at least ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.\n');

define('JS_ORDER_DOES_NOT_EXIST', 'Order Number %s does not exist!');

define('CATEGORY_PERSONAL', 'Personal');
define('CATEGORY_ADDRESS', 'Address');
define('CATEGORY_ADDRESS_DEFAULT', 'Adress principal');
define('CATEGORY_CONTACT', 'Contact');
define('CATEGORY_COMPANY', 'Detail on the company');
define('CATEGORY_OPTIONS', 'Options');
define('CATEGORY_NEWSLETTER', 'Inscription with the newsletters');
define('CATEGORY_GROUP_CUSTOMER', 'Type of customer');
define('CATEGORY_ORDER_TAXE', 'Mode of invoicing');
define('CATEGORY_ORDER_TAXE_GROUP', 'Mode of invoicing authorized on the group');
define('CATEGORY_SHIPPING_CUSTOMER', 'Mode of delivery authorized');
define('CATEGORY_SHIPPING_CUSTOMER_GROUP', 'Mode of delivery authorized on the group');
define('CATEGORY_ORDER_CUSTOMER', 'Mode of payment authorized');
define('CATEGORY_ORDER_CUSTOMER_GROUP', 'Mode of payment authorized on the group');

define('ENTRY_GENDER', 'Gender:');
define('ENTRY_GENDER_ERROR', '&nbsp;<span class="errorText">required</span>');
define('ENTRY_FIRST_NAME', 'First Name:');
define('ENTRY_FIRST_NAME_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' chars</span>');
define('ENTRY_LAST_NAME', 'Last Name:');
define('ENTRY_LAST_NAME_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_LAST_NAME_MIN_LENGTH . ' chars</span>');
define('ENTRY_DATE_OF_BIRTH', 'Date of Birth:');
define('ENTRY_DATE_OF_BIRTH_ERROR', '&nbsp;<span class="errorText">(eg. 05/21/1970)</span>');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail Address:');
define('ENTRY_EMAIL_ADDRESS_ERROR', '&nbsp;<span class="errorText">The email address is not correct</span>');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', '&nbsp;<span class="errorText">The email address doesn\'t appear to be valid!</span>');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', '&nbsp;<span class="errorText">This email address already exists!</span>');
define('ENTRY_COMPANY', 'Company name:');
define('ENTRY_COMPANY_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_COMPANY_MIN_LENGTH . ' chars</span>');
define('ENTRY_STREET_ADDRESS', 'Street Address:');
define('ENTRY_STREET_ADDRESS_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' chars</span>');
define('ENTRY_SUBURB', 'Suburb:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_POST_CODE', 'Post Code:');
define('ENTRY_POST_CODE_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_POSTCODE_MIN_LENGTH . ' chars</span>');
define('ENTRY_CITY', 'City:');
define('ENTRY_CITY_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_CITY_MIN_LENGTH . ' chars</span>');
define('ENTRY_STATE', 'State:');
define('ENTRY_STATE_ERROR', '&nbsp;<span class="errorText">required</span>');
define('ENTRY_COUNTRY', 'Country:');
define('ENTRY_COUNTRY_ERROR', '');
define('ENTRY_TELEPHONE_NUMBER', 'Telephone Number:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_TELEPHONE_MIN_LENGTH . ' chars</span>');

define('ENTRY_CELLULAR_PHONE_NUMBER', 'Cellular phone Number:');
define('ENTRY_CELLULAR_PHONE_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER', 'Fax Number:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_TELEPHONE','Phone Number');

define('ENTRY_NEWSLETTER', 'Newsletter:');
define('ENTRY_NEWSLETTER_YES', 'Subscribed');
define('ENTRY_NEWSLETTER_NO', 'Unsubscribed');
define('ENTRY_NEWSLETTER_ERROR', '');
define('ENTRY_NEWSLETTER_LANGUAGE', 'Language:');

// TVA, Siret et Code APE
define('ENTRY_ADDRESS_NUMBER','Address No');
define('ENTRY_ADRESS_DEFAULT','(Address principal)');
define('ENTRY_SIRET', 'Registration Code :');
define('ENTRY_SIRET_EXEMPLE', '(ex : RCS Code for the French company)');
define('ENTRY_APE', 'Nomenclatur Number :');
define('ENTRY_APE_EXEMPLE', '(ex : APE Code for the French company)');
define('ENTRY_TVA', 'No VAT Intracom :');
define('ENTRY_TVA_ISO_ERROR', '&nbsp;<span class="errorText">The code country of two letters does not seem to be correct.</span>');

// Groupe Client
define('ENTRY_CUSTOMERS_GROUP_NAME', 'Customer Group:');
define('ENTRY_GROUPS_NAME', 'Name:');

// images
define('IMAGE_ANI_SEND_EMAIL', 'Sending E-Mail');
define('IMAGE_ARCHIVE','Archive');
define('IMAGE_ARCHIVE_TO','Archive');
define('IMAGE_BACK', 'Back');
define('IMAGE_BACKUP', 'Backup');
define('IMAGE_BATCH_PRINT_ORDER','Batch Print');
define('IMAGE_CANCEL', 'Cancel');
define('IMAGE_CONFIRM', 'Confirm');
define('IMAGE_COPY', 'Copy');
define('IMAGE_COPY_TO', 'Copy To');
define('IMAGE_DETAILS', 'Details');
define('IMAGE_DELETE', 'Delete');
define('IMAGE_EDIT', 'Edit');
define('IMAGE_EMAIL', 'Email');
define('IMAGE_FILE_MANAGER', 'File Manager');
define('IMAGE_HELP','Help');
define('ICON_EDIT_CUSTOMER','Customer edit');
define('ICON_EDIT_ORDER','Editer la commande');
define('IMAGE_ICON_STATUS_GREEN', 'Active');
define('IMAGE_ICON_STATUS_GREEN_LIGHT', 'Set Active');
define('IMAGE_ICON_STATUS_RED', 'Inactive');
define('IMAGE_ICON_STATUS_RED_LIGHT', 'Set Inactive');
define('IMAGE_ICON_INFO', 'Activate');
define('IMAGE_INSERT', 'Insert');
define('IMAGE_LOCK', 'Lock');
define('IMAGE_MODULE_INSTALL', 'Install Module');
define('IMAGE_MODULE_REMOVE', 'Remove Module');
define('IMAGE_MOVE', 'Move');
define('IMAGE_NEW_BANNER', 'New Banner');
define('IMAGE_NEW_BLOG','New article');
define('IMAGE_NEW_CATEGORY', 'New Category');
define('IMAGE_NEW_COUNTRY', 'New Country');
define('IMAGE_NEW_CURRENCY', 'New Currency');
define('IMAGE_NEW_FILE', 'New File');
define('IMAGE_NEW_FOLDER', 'New Folder');
define('IMAGE_NEW_LANGUAGE', 'New Language');
define('IMAGE_NEW_NEWSLETTER', 'New Newsletter');
define('IMAGE_NEW_PRODUCT', 'New Product');
define('IMAGE_NEW_TAX_CLASS', 'New Tax Class');
define('IMAGE_NEW_TAX_RATE', 'New Tax Rate');
define('IMAGE_NEW_TAX_ZONE', 'New Tax Zone');
define('IMAGE_NEW_ZONE', 'New Zone');
define('IMAGE_ORDERS', 'Orders');
define('IMAGE_ORDERS_ODOO','Order registred in Odoo');
define('IMAGE_ORDERS_INVOICE_ODOO','Invoice Registred in Odoo');
define('IMAGE_ORDERS_INVOICE_MANUAL_ODOO', 'Invoice saved in Odoo');
define('IMAGE_ORDERS_INVOICE_CANCEL_ODOO', 'Invoice cancelled  in Odoo');
define('IMAGE_ORDERS_HISTORY','History');
define('IMAGE_ORDERS_INVOICE', 'Invoice');
define('IMAGE_ORDERS_PACKINGSLIP', 'Packing Slip');
define('IMAGE_CUSTOMERS', 'Customers');
define('IMAGE_NEWSLETTER', 'Newsletters');
define('IMAGE_REVIEWS', 'Opinion of the customers');
define('IMAGE_PREVIEW', 'Preview');
define('ICON_PREVIEW_CATALOG','Catalog preview');
define('IMAGE_RESTORE', 'Restore');
define('IMAGE_RESET', 'Reset');
define('IMAGE_SAVE', 'Save');
define('IMAGE_SEARCH', 'Search');
define('IMAGE_SELECT', 'Select');
define('IMAGE_SEND', 'Send');
define('IMAGE_SEND_EMAIL', 'Send Email');
define('IMAGE_UNLOCK', 'Unlock');
define('IMAGE_UPDATE', 'Update');
define('IMAGE_UPDATE_CURRENCIES', 'Update Exchange Rate');
define('IMAGE_UPLOAD', 'Upload');
define('IMAGE_CATEGORIES', 'Categories');
define('IMAGE_SPECIALS', 'Specials');
define('IMAGE_MANUFACTURERS', 'Brands');
define('IMAGE_PRODUCTS_EXPECTED', 'Management of the arrivals in stock');
define('IMAGE_UPDATE', 'Fast update of the prices ');
define('IMAGE_UNPACK','Unpack');
define('IMAGE_BACKUP_DB', 'Backup the data base');
define('IMAGE_BANNER', 'Banner Manager');
define('IMAGE_WHOS_ONLINE', 'Whos online');

// Bouton groupe client
define('IMAGE_NEW_GROUP', 'New Group');
define('IMAGE_UPDATEALLPRICE', 'Update all price of group');

define('ICON_EDIT','Edit');
define('ICON_CROSS', 'False');
define('ICON_CURRENT_FOLDER', 'Current Folder');
define('ICON_DELETE', 'Delete');
define('ICON_ERROR', 'Error');
define('ICON_FILE', 'File');
define('ICON_FILE_DOWNLOAD', 'Download');
define('ICON_FOLDER', 'Folder');
define('ICON_LOCKED', 'Locked');
define('ICON_PREVIOUS_LEVEL', 'Previous Level');
define('ICON_PREVIEW', 'Preview');
define('ICON_STATISTICS', 'Statistics');
define('ICON_SUCCESS', 'Success');
define('ICON_TICK', 'True');
define('ICON_UNLOCKED', 'Unlocked');
define('ICON_WARNING', 'Warning');
define('ICON_HELP', 'help');

// constants for use in osc_prev_next_display function
define('TEXT_RESULT_PAGE', 'Page %s of %d');
define('TEXT_DISPLAY_NUMBER_OF_BANNERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> banners)');
define('TEXT_DISPLAY_NUMBER_OF_COUNTRIES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> countries)');
define('TEXT_DISPLAY_NUMBER_OF_CUSTOMERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> customers)');
define('TEXT_DISPLAY_NUMBER_OF_CURRENCIES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> currencies)');
define('TEXT_DISPLAY_NUMBER_OF_LANGUAGES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> languages)');
define('TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> brands)');
define('TEXT_DISPLAY_NUMBER_OF_SUPPLIERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> suppliers)');
define('TEXT_DISPLAY_NUMBER_OF_NEWSLETTERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> newsletters)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> orders)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS_STATUS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> orders status)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> products)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_EXPECTED', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> products expected)');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> product reviews)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> products on special)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_HEART', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> products on favorites)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_CLASSES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> tax classes)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_ZONES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> tax zones)');
define('TEXT_DISPLAY_NUMBER_OF_TAX_RATES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> tax rates)');
define('TEXT_DISPLAY_NUMBER_OF_ZONES', 'Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> zones)');
define('TEXT_DISPLAY_NUMBER_OF_ENTRIES','Displaying <strong>%d</strong> to <strong>%d</strong> (of <strong>%d</strong> action recorder)'); 
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_QUANTITY_UNIT_STATUS','Displaying <strong>%d</strong> to <strong>%d</strong> (on <strong>%d</strong> order quantity types)');
define('PREVNEXT_BUTTON_PREV', '&lt;&lt;');
define('PREVNEXT_BUTTON_NEXT', '&gt;&gt;');

define('TEXT_DEFAULT', 'default');
define('TEXT_SET_DEFAULT', 'Set as default');
define('TEXT_FIELD_REQUIRED', '&nbsp;<span class="fieldRequired">* Required</span>');
define('TEXT_SORT_ALL', 'Sort ');
define('TEXT_DESCENDINGLY', 'descendingly');
define('TEXT_ASCENDINGLY', 'ascendingly');
define('TEXT_IMAGE_PREVIEW','Preview');
define('TEXT_OTHER', 'Others');
define('TEXT_LEGEND', 'Legend : ');
define('TEXT_ALL_GROUPS', 'All customers groups');

define('TEXT_CACHE_CATEGORIES', 'Categories Box');
define('TEXT_CACHE_MANUFACTURERS', 'Brands Box');
define('TEXT_CACHE_ALSO_PURCHASED', 'Also Purchased Module');
define('TEXT_CACHE_PRODUCTS_RELATED','related products module');
define('TEXT_CACHE_PRODUCTS_CROSS_SELL','Cross sell products module');
define('TEXT_CACHE_NEW', 'New Products');
define('TEXT_CACHE_UPCOMING', 'Upcoming Products');

define('TEXT_NONE', '--none--');
define('TEXT_TOP', 'Top');

define('TEXT_TWITTER_MESSAGE','Your message has been sended on Twitter!.<br />');
define('TEXT_TWITTER_PROFILE',' See your profil');
define('TEXT_ERROR_TWITTER_POST','Error, please insert a message and try again.<br />');	

define('TEXT_HEADER_USER_ADMINISTRATOR','Administrator');
define('TEXT_HEADER_LOGOFF_ADMINISTRATOR', 'Log off');
define('TEXT_HEADER_NUMBER_OF_CUSTOMERS', '%s Customer(s) online');
define('TEXT_HEADER_ICON_NUMBER_OF_CUSTOMERS', 'View the customers online');
define('TEXT_HELP_WYSIWYG', 'Information on supporting the use of WYSIWYG in pictures');
define('TEXT_CLOSE','Close');

define('ERROR_DESTINATION_DOES_NOT_EXIST', 'Error: Destination does not exist.');
define('ERROR_DESTINATION_NOT_WRITEABLE', 'Error: Destination not writeable.');
define('ERROR_FILE_NOT_SAVED', 'Error: File upload not saved.');
define('ERROR_FILETYPE_NOT_ALLOWED', 'Error: File upload type not allowed.');
define('SUCCESS_FILE_SAVED_SUCCESSFULLY', 'Success: File upload saved successfully.');
define('WARNING_NO_FILE_UPLOADED', 'Information: No image or file of transferred on the waiter.');
define('WARNING_EDIT_CUSTOMERS', 'Fields comprise errors to not more make it possible to record your modifications.');
define('ERROR_FILE_NOT_WRITEABLE','Impossible to write in this files');

// Mode de facturation des clients
define('OPTIONS_ORDER', 'Invoicing :');
define('OPTIONS_ORDER_NATIONAL', 'National');
define('OPTIONS_ORDER_CEE', 'European union');
define('OPTIONS_ORDER_INTERNATIONAL', 'International');
define('OPTIONS_ORDER_TAXE', 'Inc. to the tax');
define('OPTIONS_ORDER_NO_TAXE', 'Ex. to the tax');

// Nom affiche dans le menu deroulant pour client par defaut
define('VISITOR_NAME','New Client');

// Affichage sur different champ pour indiquer le montant en HT ou en TTC
define('TAX_INCLUDED', 'TAX inc.');
define('TAX_EXCLUDED', 'TAX exc.');

// Menu des onglets
define('TAB_GENERAL', 'General');
define('TAB_ORDERS', 'Invoicing');
define('TAB_SHIPPING', 'Delivery');
define('TAB_CATEGORIE', 'Categories');
define('TAB_PRICE', 'Price');
define('TAB_DESC', 'Description');
define('TAB_IMG', 'Images');
define('TAB_REF', 'Meta Data');
define('TAB_SOCIETE', 'Companies');
define('TAB_ADRESSE_BOOK', 'book');
define('TAB_CODE_HTML', 'Code HTML');
define('TAB_ORDERS_DETAILS', 'Order');
define('TAB_STATUT', 'Status');
define('TAB_OPTIONS','Options');

// Facture Y = Annee / m = mois / d = jours  (Voir aussi fonction osc_datereference_short() dans general.php)
define('DATE_FORMAT_REFERENCE', 'Ym');

// Discount coupon
define('FIXED_AMOUNT','Fixed Amount');
define('PERCENTAGE_DISCOUNT','Percentage discount');
define('PRICE_TOTAL','Price total');
define('PRODUCT_QUANTITY','Product quantity');

define('KEYWORDS_GOOGLE_TREND','Keywords Google trend');
define('ANALYSIS_GOOGLE_TOOL','Analysis Google Tool');


// Succes / error
define('SUCCESS_STATUS_UPDATED','Success update');
define('ERROR_UNKNOWN_STATUS_FLAG','Error unknown');


// Dynamic Template system
define('ALL_PAGES', 'All pages.');
define('ONE_BY_ONE', 'Or one by one :');
define('CHECK_ALL', 'Check All');
define('DESELECT_ALL', 'Uncheck All');

define('SELECT_DATAS','--- Select ---');

// No script
define('NO_SCRIPT_TEXT', "<p><strong>JavaScript seems to be disabled in your browser.</strong></p><p>You must have JavaScript enabled in your browser to utilize the functionality of this website. <a href='http://www.enable-javascript.com/' target='_blank' rel='nofollow'>Click here for instructions on enabling javascript in your browser</a>.</p>");

define('TITLE_HELP_GENERAL','Help');
?>
