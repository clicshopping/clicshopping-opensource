<?php
/**
 * session.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  if (STORE_SESSIONS == 'mysql') {

    function _sess_open($save_path, $session_name) {
      return true;
    }

    function _sess_close() {
      return true;
    }

    function _sess_read($key) {
      global $OSCOM_PDO;

      $Qsession = $OSCOM_PDO->prepare('select value
                                       from :table_sessions 
                                       where sesskey = :sesskey
                                      ');
      $Qsession->bindValue(':sesskey', $key);
      $Qsession->execute();

      if ($Qsession->fetch() !== false) {
        return $Qsession->value('value');

      }

      return '';
    }

    function _sess_write($key, $value) {
      global $OSCOM_PDO;

      $Qcheck = $OSCOM_PDO->prepare('select 1
                                    from :table_sessions
                                    where sesskey = :sesskey
                                   ');
      $Qcheck->bindValue(':sesskey', $key);
      $Qcheck->execute();      
      

      if (strlen(osc_db_input($value)) > 65535) {
// administrator email
        $email_subject = 'Error Mysql on ' . STORE_NAME;
        $message = 'Error Mysql, Data truncated for column value in last session and not saved for keeping stability. there are certainly too many items in your cart <br /><br />Please contact your administrator<br /><br />ClicShopping system has also send an email at your administrator';
        $name = STORE_NAME;
        $email_address = STORE_OWNER_EMAIL_ADDRESS;
        osc_mail(STORE_NAME, STORE_OWNER_EMAIL_ADDRESS, $email_subject, $message, $name, $email_address, '');

        osc_db_error("SQL SESSION ERROR", "<p>Warning: #1265, #1046 Data truncated for column 'value' in last session and not saved for keeping stability", strlen(osc_db_input($value)) . " characters would be truncated to 65535" . '</p><p><font color="#ff0000">Sorry for the inconvenience but ' . STORE_NAME . ' online shopping system has reached its limit of. Probably too many items in cart. We recommend that you remove some products from the basket, and instead do one more after this order.</font></p><br /><p>&nbsp;<a href="' . osc_href_link('shopping_cart.php') . '">' . IMAGE_BUTTON_CONTINUE . '</a><br /><br />&nbsp;<a href="' . osc_href_link('contact_us.php') . '">' . 'Contact' . '</a></p>' );
      }

      if ($Qcheck->fetch() !== false) {
        $result = $OSCOM_PDO->save('sessions', ['expiry' => time(),
                                     'value' => $value
                                     ],
                                     ['sesskey' => $key]
                                            
                                );
      } else {

        $result = $OSCOM_PDO->save('sessions', ['sesskey' => $key,
                                            'expiry' => time(),
                                            'value' => $value]
                                );
      }

      return $result !== false;
    }

    function _sess_destroy($key) {
      global $OSCOM_PDO;

      $result = $OSCOM_PDO->delete('sessions', ['sesskey' => $key]);

      return $result !== false;
    }

    function _sess_gc($maxlifetime) {
      global $OSCOM_PDO;

      $Qdel = $OSCOM_PDO->prepare('delete
                                   from :table_sessions 
                                   where expiry < :expiry
                                   ');
      $Qdel->bindValue(':expiry', time() - $maxlifetime);
      $Qdel->execute();
      
      return $Qdel->isError() === false;
    }

    session_set_save_handler('_sess_open', '_sess_close', '_sess_read', '_sess_write', '_sess_destroy', '_sess_gc');
  }

  function osc_session_start() {
    global $OSCOM_PDO;
     
    $sane_session_id = true;

    if ( isset($_GET[session_name()]) ) {
      if ( (SESSION_FORCE_COOKIE_USE == 'True') || (preg_match('/^[a-zA-Z0-9,-]+$/', $_GET[session_name()]) == false) ) {
        unset($_GET[session_name()]);

        $sane_session_id = false;
      }
    }

    if ( isset($_POST[session_name()]) ) {
      if ( (SESSION_FORCE_COOKIE_USE == 'True') || (preg_match('/^[a-zA-Z0-9,-]+$/', $_POST[session_name()]) == false) ) {
        unset($_POST[session_name()]);

        $sane_session_id = false;
      }
    }

    if ( isset($_COOKIE[session_name()]) ) {
      if ( preg_match('/^[a-zA-Z0-9,-]+$/', $_COOKIE[session_name()]) == false ) {
        $session_data = session_get_cookie_params();

        setcookie(session_name(), '', time()-42000, $session_data['path'], $session_data['domain']);
        unset($_COOKIE[session_name()]);

        $sane_session_id = false;
      }
    }

    if ($sane_session_id == false) {
      osc_redirect(osc_href_link('index.php', '', 'NONSSL', false));
    }

    register_shutdown_function('session_write_close');

    return session_start();
  }

  function osc_session_recreate() {
    global $SID;

    $old_id = session_id();

    session_regenerate_id(true);

    if (!empty($SID)) {
      $SID = session_name() . '=' . session_id();
      }

      osc_whos_online_update_session_id($old_id, session_id());
  }

