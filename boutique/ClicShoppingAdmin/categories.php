<?php
/**
 * categories.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

// Base sql de categorie description
  require('includes/functions/categories.php');

  require('includes/classes/currencies.php');
  $currencies = new currencies();
  
  require ('includes/classes/image_resample.php');
  $image = new SimpleImage();


  $action = (isset($_GET['action']) ? $_GET['action'] : '');

// ------------------------------------------------------------
// gallery and upload small, medium and big image
// ------------------------------------------------------------

  $exclude_folders = array(); // folders to exclude from adding new images
  $root_images_dir = DIR_FS_CATALOG_IMAGES .'products/' ;
  
/**
 * Return a remove file emphasis
 *
 * @param string $accent, original character
 * @param string  $new_accent,  the remplacement
 * @param  string $character, the new character converted
 * @param  return $character,, the new character converted
 * Doesn't work if inserted in general.php
*/

  function osc_remove_file_accents($character) {  
    $accent = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ',' ',':','.');
    $new_accent = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o','','','');
    return str_replace($accent, $new_accent, $character);
  }
// gallery
  $new_dir_without_accents = osc_remove_file_accents($_POST['new_directory']);
  $new_dir  = strtolower($new_dir_without_accents);
  $dir = 'products/' . (osc_not_null($new_dir) ? $new_dir : $_POST['directory']);

// image resample  
  $new_dir_products_image_without_accents = osc_remove_file_accents($_POST['new_directory_products_image']);
  $new_dir_products_image =strtolower($new_dir_products_image_without_accents);  
  $dir_products_image  = 'products/' . (osc_not_null($new_dir_products_image) ? $new_dir_products_image : $_POST['directory_products_image']);  


/**
 * Return a  the dir path
 *
 * @param string $path, the path
 * @param string  $result
 * Conflict with another file and doesn't work with general.php
*/   
  function osc_opendir($path) {

    $path = rtrim($path, '/') . '/';

    $exclude_array = array('.', '..', '.DS_Store', '.directory', '.htaccess', 'Thumbs.db','.php', '_note');
    $result = array();

    if ($handle = opendir($path)) {
      while (false !== ($filename = readdir($handle))) {
        if (!in_array($filename, $exclude_array)) {
          $file = array('name' => $path . $filename,
                        'is_dir' => is_dir($path . $filename),
                        'writable' => is_writable($path . $filename));
          $result[] = $file;
          if ($file['is_dir'] == true) {
            $result = array_merge($result, osc_opendir($path . $filename));
          }
        }
      }
      closedir($handle);
    }

    return $result;
  }

// ------------------------------------------------------------
// Action
// ------------------------------------------------------------

  if (osc_not_null($action)) {
    if ( $action == 'insert' || $action == 'update' || $action == 'setflag' || $action == 'archive' || $action == 'delete' ) { 
       if ( preg_match("/(insert|update|setflag|archive|delete)/i", $action) ){
// ULTIMATE Seo Urls 5
// If the action will affect the cache entries  
         osc_reset_cache_data_usu5( 'reset' );
       }
    }

    switch ($action) {
      case 'setflag':
        if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
          if (isset($_GET['pID'])) {
            osc_set_product_status($_GET['pID'], $_GET['flag']);
          }

          if (USE_CACHE == 'true') {
            $languages = osc_get_languages();
            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
              $language_id = $languages[$i]['id'];
              osc_cache_clear('category_tree-' . $language_id  .'.cache');
            }
            osc_reset_cache_block('also_purchased');
            osc_reset_cache_block('products_related');
            osc_reset_cache_block('products_cross_sell');
            osc_reset_cache_block('upcoming');
          }
        }

        osc_redirect(osc_href_link('categories.php', 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID']));
        break;
// ----------------------------------------
// Gestion de la categorie
// ----------------------------------------
      case 'insert_category':
      case 'update_category':
          if (isset($_POST['categories_id'])) $categories_id = osc_db_prepare_input($_POST['categories_id']);

          if ($categories_id == '') {
           $categories_id = osc_db_prepare_input($_GET['cID']);
          }

          $sort_order = osc_db_prepare_input($_POST['sort_order']);
          $sql_data_array = array('sort_order' => (int)$sort_order);

          if ($action == 'insert_category') {
            $insert_sql_data = array('parent_id' => $current_category_id,
                                     'date_added' => 'now()',
                                     'virtual_categories' => '0'
                                     );

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            $OSCOM_PDO->save('categories', $sql_data_array);

            $categories_id = $OSCOM_PDO->lastInsertId();

          } elseif ($action == 'update_category') {
            $update_sql_data = array('last_modified' => 'now()');

            $sql_data_array = array_merge($sql_data_array, $update_sql_data);

            $OSCOM_PDO->save('categories', $sql_data_array, ['categories_id' => (int)$categories_id] );
          }

          $languages = osc_get_languages();

          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {

            $categories_name_array = $_POST['categories_name'];

            $language_id = $languages[$i]['id'];

            $sql_data_array = array('categories_name' => osc_db_prepare_input($categories_name_array[$language_id]),
                                    'categories_description' => osc_db_prepare_input($_POST['categories_description'][$language_id]),
                                    'categories_head_title_tag' => osc_db_prepare_input($_POST['categories_head_title_tag'][$language_id]),
                                    'categories_head_desc_tag' => osc_db_prepare_input($_POST['categories_head_desc_tag'][$language_id]),
                                    'categories_head_keywords_tag' => osc_db_prepare_input($_POST['categories_head_keywords_tag'][$language_id])
                                   );

            if ($action == 'insert_category') {
              $insert_sql_data = array('categories_id' => $categories_id,
                                       'language_id' => $languages[$i]['id']);

              $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

              $OSCOM_PDO->save('categories_description', $sql_data_array );

            } elseif ($action == 'update_category') {

              $OSCOM_PDO->save('categories_description', $sql_data_array, ['categories_id' => (int)$categories_id,
                                                                            'language_id' => (int)$languages[$i]['id']
                                                                          ]
                              );
            }
          }


// Ajoute ou efface l'image dans la base de donees
          if ($_POST['delete_image'] == 'yes') {
            $categories_image = '';

              $Qupdate = $OSCOM_PDO->prepare('update :table_categories
                                              set categories_image = :categories_image 
                                              where categories_id = :categories_id
                                            ');
              $Qupdate->bindValue(':categories_image', $categories_image);
              $Qupdate->bindInt(':categories_id', (int)$categories_id);
              $Qupdate->execute();

          } elseif (isset($_POST['categories_image']) && osc_not_null($_POST['categories_image']) && ($_POST['categories_image'] != 'none')) {
            $categories_image = osc_db_prepare_input($_POST['categories_image']);


// Insertion images des produits via l'editeur FCKeditor (fonctionne sur les nouveaux produits et editions produits)
            if (isset($_POST['categories_image']) && osc_not_null($_POST['categories_image']) && ($_POST['categories_image'] != 'none')) {
              $categories_image_name = $_POST['categories_image'];
              $categories_image_name = htmlspecialchars($categories_image_name);
              $categories_image_name = strstr($categories_image_name, DIR_WS_CATALOG_IMAGES);
              $categories_image_name = str_replace(DIR_WS_CATALOG_IMAGES, '', $categories_image_name);
              $categories_image_name_end = strstr($categories_image_name, '&quot;');
              $categories_image_name = str_replace($categories_image_name_end, '', $categories_image_name);
              $categories_image_name = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $categories_image_name);
            } else {
              $categories_image_name = (isset($_POST['categories_previous_image']) ? $_POST['categories_previous_image'] : '');
            }

              $categories_image = $categories_image_name;


              $Qupdate = $OSCOM_PDO->prepare('update :table_categories
                                              set categories_image = :categories_image 
                                              where categories_id = :categories_id
                                            ');
              $Qupdate->bindValue(':categories_image', $categories_image);
              $Qupdate->bindInt(':categories_id', (int)$categories_id);
              $Qupdate->execute();
          }

          if (USE_CACHE == 'true') {
            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
              $language_id = $languages[$i]['id'];
              osc_cache_clear('category_tree-' . $language_id  .'.cache');
            }
            osc_reset_cache_block('also_purchased');
            osc_reset_cache_block('products_related');
            osc_reset_cache_block('products_cross_sell');
            osc_reset_cache_block('upcoming');
          }

//***************************************
// odoo web service
//***************************************
          if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
            require('ext/odoo_xmlrpc/xml_rpc_admin_categories.php');
          }
//***************************************
// End odoo web service
//***************************************

          osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories_id));
          
        break;

      case 'delete_category_confirm':
        if (isset($_POST['categories_id'])) {
          $categories_id = osc_db_prepare_input($_POST['categories_id']);

          $categories = osc_get_category_tree($categories_id, '', '0', '', true);
          $products = array();
          $products_delete = array();

          for ($i=0, $n=sizeof($categories); $i<$n; $i++) {

            $QproductIds = $OSCOM_PDO->prepare('select products_id
                                               from products_to_categories
                                               where categories_id = :categories_id
                                              ');

            $QproductIds->bindInt(':categories_id', (int)$categories[$i]['id'] );

            $QproductIds->execute();

            while ($product_ids = $QproductIds->fetch() ) {
              $products[$product_ids['products_id']]['categories'][] = $categories[$i]['id'];
            }
          }

          foreach ( $products as $key => $value ) {
            $category_ids = '';

            for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
              $category_ids .= "'" . (int)$value['categories'][$i] . "', ";
            }
            $category_ids = substr($category_ids, 0, -2);

            $Qcheck = $OSCOM_PDO->prepare('select count(*) as total
                                           from products_to_categories
                                           where products_id = :products_id
                                           and categories_id not in ( :categories_id )
                                          ');

            $Qcheck->bindInt(':products_id', (int)$key );
            $Qcheck->bindInt(':categories_id', $category_ids );
            $Qcheck->execute();

            $check= $Qcheck->fetch();

            if ($check['total'] < '1') {
              $products_delete[$key] = $key;
            }
          }

// removing categories can be a lengthy process
          osc_set_time_limit(0);
          for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
            osc_remove_category($categories[$i]['id']);
          }

          foreach  ( array_keys ($products_delete) as $key ) {
            osc_remove_product($key);
          }
        }

        if (USE_CACHE == 'true') {
          $languages = osc_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];
            osc_cache_clear('category_tree-' . $language_id  .'.cache');
          }
          osc_reset_cache_block('also_purchased');
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
          osc_reset_cache_block('upcoming');
        }

        osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath));
        break;

      case 'delete_product_confirm':

        if (isset($_POST['products_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories'])) {
          $product_id = osc_db_prepare_input($_POST['products_id']);
          $product_categories = $_POST['product_categories'];

          for ($i=0, $n=sizeof($product_categories); $i<$n; $i++) {

// delete product of categorie
            $Qdelete = $OSCOM_PDO->prepare('delete from products_to_categories
                                            where products_id = :products_id
                                           and categories_id = :categories_id
                                          ');
            $Qdelete->bindInt(':products_id', (int)$product_id );
            $Qdelete->bindValue(':categories_id', (int)$product_categories[$i]);
            $Qdelete->execute();

          } // end for


          $QproductCategories = $OSCOM_PDO->prepare('select count(*) as total 
                                                     from :table_products_to_categories
                                                     where products_id = :products_id
                                                    ');
          $QproductCategories->bindInt(':products_id', (int)$product_id);
          $QproductCategories->execute();

          $product_categories = $QproductCategories->fetch();

          if ($product_categories['total'] == '0') {
            osc_remove_product($product_id);
          }
        }

        if (USE_CACHE == 'true') {
          $languages = osc_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];
            osc_cache_clear('category_tree-' . $language_id  .'.cache');
          }
          osc_reset_cache_block('also_purchased');
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
          osc_reset_cache_block('upcoming');
        }

        osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath));
        break;

      case 'move_category_confirm':
        if (isset($_POST['categories_id']) && ($_POST['categories_id'] != $_POST['move_to_category_id'])) {
          $categories_id = osc_db_prepare_input($_POST['categories_id']);
          $new_parent_id = osc_db_prepare_input($_POST['move_to_category_id']);

          $path = explode('_', osc_get_generated_category_path_ids($new_parent_id));

          if (in_array($categories_id, $path)) {
            $OSCOM_MessageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');

            osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories_id));
          } else {

            $Qupdate = $OSCOM_PDO->prepare('update :table_categories
                                            set parent_id = :parent_id, 
                                            last_modified = now()
                                            where categories_id = :categories_id
                                          ');
            $Qupdate->bindInt(':parent_id', (int)$new_parent_id);
            $Qupdate->bindInt(':categories_id',(int)$categories_id);
            $Qupdate->execute();

            if (USE_CACHE == 'true') {
              $languages = osc_get_languages();
              for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                $language_id = $languages[$i]['id'];
                osc_cache_clear('category_tree-' . $language_id  .'.cache');
              }
              osc_reset_cache_block('also_purchased');
              osc_reset_cache_block('products_related');
              osc_reset_cache_block('products_cross_sell');
              osc_reset_cache_block('upcoming');
            }

            osc_redirect(osc_href_link('categories.php', 'cPath=' . $new_parent_id . '&cID=' . $categories_id));
          }
        }

        break;

      case 'move_product_confirm':
        $products_id = osc_db_prepare_input($_POST['products_id']);
        $new_parent_id = osc_db_prepare_input($_POST['move_to_category_id']);

        $QduplicateCheck = $OSCOM_PDO->prepare('select count(*) 
                                                from :table_products_to_categories
                                                where products_id = :products_id
                                                and categories_id not in ( :categories_id )
                                              ');
        $QduplicateCheck->bindInt(':products_id', (int)$products_id);
        $QduplicateCheck->bindInt(':categories_id',(int)$new_parent_id );
        $QduplicateCheck->execute();

        if ($QduplicateCheck->rowCount() < 1.01) {

          $Qupdate = $OSCOM_PDO->prepare('update :table_products_to_categories
                                          set categories_id = :categories_id
                                          where products_id = :products_id
                                          and categories_id = :categories_id1
                                        ');
          $Qupdate->bindInt(':categories_id',(int)$new_parent_id);
          $Qupdate->bindInt(':products_id', (int)$products_id);
          $Qupdate->bindInt(':categories_id1', (int)$current_category_id);

          $Qupdate->execute();
        }

        if (USE_CACHE == 'true') {
          $languages = osc_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];
            osc_cache_clear('category_tree-' . $language_id  .'.cache');
          }
          osc_reset_cache_block('also_purchased');
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
          osc_reset_cache_block('upcoming');
        }

        osc_redirect(osc_href_link('categories.php', 'cPath=' . $new_parent_id . '&pID=' . $products_id));
        break;
// -----------------------------------------
// Gestion des produits
// -----------------------------------------
      case 'insert_product':
      case 'update_product':

          if (isset($_GET['pID'])) $products_id = osc_db_prepare_input($_GET['pID']);
          $products_date_available = osc_db_prepare_input($_POST['products_date_available']);
          $products_date_available = (date('Y-m-d') < $products_date_available) ? $products_date_available : 'null';
          $twitter_status = osc_db_prepare_input($_POST['products_twitter']);

// Definir la position 0 ou 1 pour --> products_view : Affichage Produit Grand Public - orders_view : Autorisation Commande
          if (osc_db_prepare_input($_POST['products_view']) == '1') {
              $products_view = '1';
            } else {
              $products_view = '0';
            }

          if (osc_db_prepare_input($_POST['orders_view']) == '1') {
              $orders_view = '1';
            } else {
              $orders_view = '0';
            }
// Gestion de l'affichage concernant la le prix / kg
          if (osc_db_prepare_input($_POST['products_price_kilo']) == '1') {
              $products_price_kilo = '1';
           } else {
              $products_price_kilo = '0';
           }

// Gestion de l'affichage concernant les produits uniquement online ou en boutique (physique)
          if (osc_db_prepare_input($_POST['products_only_online']) == '1') {
              $products_only_online = '1';
          } else {
              $products_only_online = '0';
          }


// Gestion de l'affichage concernant les produits uniquement en boutique (physique)

          if (osc_db_prepare_input($_POST['products_only_shop']) == '1') {
              $products_only_shop = '1';
          } else {
              $products_only_shop = '0';
          }

// Gestion de l'affichage concernant le telechargementde fichier publix / privee

          if (osc_db_prepare_input($_POST['products_download_public']) == '1') {
            $products_download_public = '1';
          } else {
            $products_download_public = '0';
          }

// Affichage des produits, autorisation de commander et mode B2B en automatique mis par defaut en valeur 1 dans la cas de la B2B desactivee.
          if (MODE_B2B_B2C == 'false') {
//            $products_view = '1';
//            $orders_view = '1';
            $_POST['products_percentage'] = '1';
          }

          $sql_data_array = array('products_quantity' => (int)osc_db_prepare_input($_POST['products_quantity']),
                                  'products_ean' => osc_db_prepare_input($_POST['products_ean']),
                                  'products_price' => osc_db_prepare_input($_POST['products_price']),
                                  'products_date_available' => $products_date_available,
                                  'products_weight' => (float)osc_db_prepare_input($_POST['products_weight']),
                                  'products_price_kilo' => osc_db_prepare_input($_POST['products_price_kilo']),
                                  'products_status' => osc_db_prepare_input($_POST['products_status']),
                                  'products_percentage' => osc_db_prepare_input($_POST['products_percentage']),
                                  'products_view' => $products_view,
                                  'orders_view' => $orders_view,
                                  'products_tax_class_id' => osc_db_prepare_input($_POST['products_tax_class_id']),
                                  'manufacturers_id' => (int)osc_db_prepare_input($_POST['manufacturers_id']),
                                  'suppliers_id' => (int)osc_db_prepare_input($_POST['suppliers_id']),
                                  'products_min_qty_order' => (int)osc_db_prepare_input($_POST['products_min_qty_order']),
                                  'products_price_comparison' => osc_db_prepare_input($_POST['products_price_comparison']),
                                  'products_dimension_width'  => osc_db_prepare_input($_POST['products_dimension_width']),
                                  'products_dimension_height'  => osc_db_prepare_input($_POST['products_dimension_height']),
                                  'products_dimension_depth' => osc_db_prepare_input($_POST['products_dimension_depth']),
                                  'products_dimension_type'  => osc_db_prepare_input($_POST['products_dimension_type']),
                                  'admin_user_name'  => osc_user_admin($user_administrator),
                                  'products_volume' => osc_db_prepare_input($_POST['products_volume']),
                                  'products_quantity_unit_id'  => osc_db_prepare_input($_POST['products_quantity_unit_id']),
                                  'products_only_online'  => osc_db_prepare_input($_POST['products_only_online']),
                                  'products_weight_pounds' => (float)osc_db_prepare_input($_POST['products_weight_pounds']),
                                  'products_cost' => osc_db_prepare_input($_POST['products_cost']),
                                  'products_handling' => osc_db_prepare_input($_POST['products_handling']),
                                  'products_wharehouse_time_replenishment' => osc_db_prepare_input($_POST['products_wharehouse_time_replenishment']),
                                  'products_wharehouse' => osc_db_prepare_input($_POST['products_wharehouse']),
                                  'products_wharehouse_row' => osc_db_prepare_input($_POST['products_wharehouse_row']),
                                  'products_wharehouse_level_location' => osc_db_prepare_input($_POST['products_wharehouse_level_location']),
                                  'products_packaging' => osc_db_prepare_input($_POST['products_packaging']),
                                  'products_sort_order' => osc_db_prepare_input($_POST['products_sort_order']),
                                  'products_quantity_alert' => osc_db_prepare_input($_POST['products_quantity_alert']),
                                  'products_only_shop'  => osc_db_prepare_input($_POST['products_only_shop']),
                                  'products_download_public'  => osc_db_prepare_input($_POST['products_download_public']),
                                  'products_type' => osc_db_prepare_input($_POST['products_type'])
                                 );

// Ajoute une reference au produit et l'ean
// genere un nombre aleatoire si la reference est vide
           srand();
           $random_ref = rand();

           if (empty($_POST['products_model'])) {
             $sql_data_array['products_model'] = CONFIGURATION_PREFIX_MODEL.$random_ref;
             $products_model = CONFIGURATION_PREFIX_MODEL.$random_ref;
           } else {
             $sql_data_array['products_model'] = $_POST['products_model'];
             $products_model = $_POST['products_model'];            
           }

           if (empty($_POST['products_sku'])) {
             $sql_data_array['products_sku'] = CONFIGURATION_PREFIX_MODEL.$random_ref;
             $products_sku = CONFIGURATION_PREFIX_MODEL.$random_ref;             
           } else {
             $sql_data_array['products_sku'] = $_POST['products_sku'];
             $products_sku = $_POST['products_sku'];   
           }

//--------------------------------
// Bar code
// -------------------------------
          if (!empty($_POST['products_ean'])) {
            require_once('ext/barcode/pi_barcode.php');
            $barcode = $products_model.'_'.$_POST['products_ean'];
            $objCode = new pi_barcode() ;
            $objCode->setSize(BAR_CODE_SIZE);
            $objCode->hideCodeType();
            $objCode->setColors(BAR_CODE_COLOR);
            $objCode -> setType(BAR_CODE_TYPE) ;
            $objCode -> setCode($_POST['products_ean']) ;
            $objCode -> setFiletype(BAR_CODE_EXTENSION) ;
            $objCode -> writeBarcodeFile(DIR_FS_CATALOG_IMAGES . 'barcode/' . $barcode.'_barcode.' . BAR_CODE_EXTENSION);
          }

//--------------------------------
// Upload
// -------------------------------
          if ($products_download_module_file = new upload('products_download_filename', DIR_FS_DOWNLOAD_PUBLIC .'download')) {
            if (isset($products_download_module_file->filename)) {
              $products_download_module_file = $products_download_module_file->filename;        
            }

            if (osc_not_null($products_download_module_file)) {
              $sql_data_array['products_download_filename'] = $products_download_module_file;
            } 

          }

          if (!empty($_POST['products_download_filename'])) {
            $sql_data_array['products_download_filename'] = osc_db_prepare_input($_POST['products_download_filename']);
          }

//--------------------------------
// Small - medium - big image
// -------------------------------

// create directory for image resample
          if (osc_not_null($new_dir_products_image) && !is_dir($new_dir_products_image)) {
// depend server configuration
            mkdir($root_images_dir  . $new_dir_products_image, 0755, true);
            chmod($root_images_dir  . $new_dir_products_image, 0755);
            $separator = '/';
          }

          if (osc_not_null($_POST['directory_products_image'])) {
            $separator = '/';
          }

// load originale image
          $image_resample = new upload('products_image_resize', DIR_FS_CATALOG_IMAGES . $dir_products_image);

          if (isset($image_resample->filename)) {
            $image_resample = $image_resample->filename;
            $image->load(DIR_FS_CATALOG_IMAGES  . $dir_products_image . $separator . $image_resample);

// Zoom image
            $image->resizeToWidth(BIG_IMAGE_WIDTH);
            if (BIG_IMAGE_WIDTH  == '') {
              $big_image_width = 'big'; 
            } else {
              $big_image_width = BIG_IMAGE_WIDTH;
            }

            $big_image_width = str_replace(' ', '',  $big_image_width );
            $big_image_resized =  $dir_products_image . $separator . $big_image_width .'_' .  $image_resample;
            $image->save(DIR_FS_CATALOG_IMAGES  . $big_image_resized);

// medium image
            $image->resizeToWidth(MEDIUM_IMAGE_WIDTH);
            if (MEDIUM_IMAGE_WIDTH  == '') {
              $medium_image_width = 'medium'; 
            } else {
              $medium_image_width = MEDIUM_IMAGE_WIDTH;
            }

            $medium_image_width = str_replace(' ', '',  $medium_image_width );
            $medium_image_resized =  $dir_products_image . $separator . $medium_image_width .'_' . $image_resample;
            $image->save(DIR_FS_CATALOG_IMAGES  . $medium_image_resized);

// small image
            $image->resizeToWidth(SMALL_IMAGE_WIDTH);

            if (SMALL_IMAGE_WIDTH  == '') {
              $small_image_width = 'small'; 
            } else {
              $small_image_width = SMALL_IMAGE_WIDTH;
            }

            $small_image_width = SMALL_IMAGE_WIDTH;
            $small_image_width = str_replace(' ', '',  $small_image_width );
            $small_image_resized =  $dir_products_image . $separator . $small_image_width . '_' . $image_resample;

            $image->save(DIR_FS_CATALOG_IMAGES  . $small_image_resized);


// delete the orginal files
            if (file_exists(DIR_FS_CATALOG_IMAGES  . $dir_products_image . $separator . $image_resample)) {
              @unlink(DIR_FS_CATALOG_IMAGES  . $dir_products_image . $separator . $image_resample);
            }

          } // end isset $image_resample

// Ajoute ou efface l'image dans la base de donnees
          if ($_POST['delete_image'] == 'yes') {
            $sql_data_array['products_image'] = '';
            $sql_data_array['products_image_zoom'] = '';
            $sql_data_array['products_image_medium'] = '';
          } else {

           if ((isset($_POST['products_image']) && osc_not_null($_POST['products_image']) && ($_POST['products_image'] != 'none')) || $small_image_resized != '') {

// Insertion images des produits via l'editeur FCKeditor (fonctionne sur les nouveaux produits et editions produits)
              $products_image_name = osc_db_prepare_input($_POST['products_image']);
              $products_image_name = htmlspecialchars($products_image_name);
              $products_image_name = strstr($products_image_name, DIR_WS_CATALOG_IMAGES);
              $products_image_name = str_replace(DIR_WS_CATALOG_IMAGES, '', $products_image_name);
              $products_image_name_end = strstr($products_image_name, '&quot;');
              $products_image_name = str_replace($products_image_name_end, '', $products_image_name);
              $products_image_name = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $products_image_name);

// small image
              if (osc_not_null($small_image_resized)) {
                $sql_data_array['products_image'] = $small_image_resized;
              } else {
                $sql_data_array['products_image'] = $products_image_name;
              }
            
            } else {
              $products_image_name = (isset($_POST['products_previous_image']) ? $_POST['products_previous_image'] : '');
            }

// big image
            if ((isset($_POST['products_image_zoom']) && osc_not_null($_POST['products_image_zoom']) && ($_POST['products_image_zoom'] != 'none')) || $big_image_resized != '') {

              $products_image_zoom_name = osc_db_prepare_input($_POST['products_image_zoom']);
              $products_image_zoom_name = htmlspecialchars($products_image_zoom_name);
              $products_image_zoom_name = strstr($products_image_zoom_name, DIR_WS_CATALOG_IMAGES);
              $products_image_zoom_name = str_replace(DIR_WS_CATALOG_IMAGES, '', $products_image_zoom_name);
              $products_image_zoom_name_end = strstr($products_image_zoom_name, '&quot;');
              $products_image_zoom_name = str_replace($products_image_zoom_name_end, '', $products_image_zoom_name);
              $products_image_zoom_name = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $products_image_zoom_name);


              if (osc_not_null($big_image_resized)) {
                $sql_data_array['products_image_zoom'] = $big_image_resized;
              } else {
                $sql_data_array['products_image_zoom'] = $products_image_zoom_name;
              }
            } else {
              $products_image_zoom_name = (isset($_POST['products_previous_image_2']) ? $_POST['products_previous_image_2'] : '');
            }

// medium image
            if ((isset($_POST['products_image_medium']) && osc_not_null($_POST['products_image_medium']) && ($_POST['products_image_medium'] != 'none')) || $medium_image_resized != '') {

              $products_image_medium_name = osc_db_prepare_input($_POST['products_image_medium']);
              $products_image_medium_name = htmlspecialchars($products_image_medium_name);
              $products_image_medium_name = strstr($products_image_medium_name, DIR_WS_CATALOG_IMAGES);
              $products_image_medium_name = str_replace(DIR_WS_CATALOG_IMAGES, '', $products_image_medium_name);
              $products_image_medium_name_end = strstr($products_image_medium_name, '&quot;');
              $products_image_medium_name = str_replace($products_image_medium_name_end, '', $products_image_medium_name);
              $products_image_medium_name = str_replace(DIR_WS_CATALOG_PRODUCTS_IMAGES, '', $products_image_medium_name);

              if (osc_not_null($medium_image_resized)) {
                $sql_data_array['products_image_medium'] = $medium_image_resized;
              } else {
                $sql_data_array['products_image_medium'] = $products_image_medium_name;
              }
            } else {
              $products_image_medium_name = (isset($_POST['products_image_medium_name']) ? $_POST['products_image_medium_name'] : '');
            }
          }

// B2B 
          $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                                  customers_group_name,
                                                                  customers_group_discount
                                                  from :table_customers_groups
                                                  where customers_group_id != :customers_group_id
                                                  order by customers_group_id
                                                ');

          $QcustomersGroup->bindInt(':customers_group_id', 0 );
          $QcustomersGroup->execute();

          while ($customers_group = $QcustomersGroup->fetch() ) {

 //build the data for b2b
            if ($QcustomersGroup->rowCount()  > 0) {

              $Qattributes = $OSCOM_PDO->prepare('select customers_group_id,
                                                         customers_group_price,
                                                         products_price
                                                   from :table_products_groups
                                                   where products_id = :products_id
                                                   and customers_group_id = :customers_group_id
                                                   order by customers_group_id
                                                  ');
              $Qattributes->bindInt(':products_id',(int)$pInfo->products_id );
              $Qattributes->bindInt(':customers_group_id',(int)$customers_group['customers_group_id']);
              $Qattributes->execute();

              $attributes = $Qattributes->fetch();

              $Qdiscount = $OSCOM_PDO->prepare('select discount
                                                from :table_groups_to_categories
                                                where customers_group_id = :customers_group_id
                                                and categories_id = :categories_id
                                                ');
              $Qdiscount->bindInt(':categories_id',(int)$current_category_id );
              $Qdiscount->bindInt(':customers_group_id',(int)$customers_group['customers_group_id']);
              $Qdiscount->execute();

              $query_discount_result = $Qdiscount->fetch();

              if (is_null($query_discount_result['discount'])) {
                $ricarico = $customers_group['customers_group_discount'];
              } else {
                 $ricarico = $query_discount_result['discount'];
              }
            } // end osc_db_num_rows

 // if check is OFF the b2bsuite percentage is not apply
            if (($_POST['products_percentage']) || (MODE_B2B_B2C == 'false')) {
              $pricek = $_POST['products_price'];   
// apply b2b
              if ($pricek > 0){
                if (B2B == 'true') {
                  if ($ricarico > 0) $newprice = $pricek+($pricek/100)*$ricarico;
                   if ($ricarico == 0) $newprice = $pricek;
                }

                if (B2B == 'false') {
                  if ($ricarico > 0) $newprice = $pricek-($pricek/100)*$ricarico;
                  if ($ricarico == 0) $newprice = $pricek;
                }
// Prix TTC B2BSuite V0.97
                $_POST['price' . $customers_group['customers_group_id']] = $newprice;
              } else {
                $newprice;
              } // end $pricek

            } else if (osc_not_null($_POST)) {
// Prix TTC B2B
              $newprice  = $_POST['price' . $customers_group['customers_group_id']];
            } else {
            $newprice  = $attributes['customers_group_price'];

          } // end while
        }

//----------------------------------------------
// insert products
//----------------------------------------------

        if ($action == 'insert_product') {
          $insert_sql_data = array('products_date_added' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          $OSCOM_PDO->save('products', $sql_data_array);
          $products_id = $OSCOM_PDO->lastInsertId();

          $OSCOM_PDO->save('products_to_categories', [ 'products_id' => (int)$products_id ,
                                                       'categories_id' => (int)$current_category_id
                                                     ]
                          );

// ---------------------
// Others options
// ----------------------

// insert into favorites (products heart)
           if (!empty($_POST['products_heart'])) {

             $OSCOM_PDO->save('products_heart', [ 'products_id' => (int)$products_id,
                                                 'products_heart_date_added' => 'now()',
                                                 'status' => 1,
                                                 'customers_group_id' => 0
                                                ]
                             );
           }


// insert into featured (products featured)
          if (!empty($_POST['products_featured'])) {

            $OSCOM_PDO->save('products_featured', [
                                                    'products_id' => (int)$products_id,
                                                    'products_featured_date_added' => 'now()',
                                                    'status' => 1,
                                                    'customers_group_id' => 0
                                                  ]
                            );
          }

// insert into specials
          if (!empty($_POST['products_specials'])) { 
            if ($_POST['percentage_products_specials'] != '') {
              if (substr($_POST['percentage_products_specials'], -1) == '%') {            
                $specials_price = ($_POST['products_price'] - (($_POST['percentage_products_specials'] / 100) *  $_POST['products_price']));
              } else {
                $specials_price = $_POST['products_price'] - $_POST['percentage_products_specials'];
              }

              if (is_numeric($specials_price)) {

                $OSCOM_PDO->save('specials', [
                                              'products_id' => (int)$products_id,
                                              'specials_new_products_price' => $specials_price,
                                              'specials_date_added' => 'now()',
                                              'status' => 1,
                                              'customers_group_id' => 0
                                            ]
                                );
              } // end is_numeric
            } // $_POST['percentage_products_specials']
          } // $_POST['products_specials']           

//----------------------------------------------
// update products
//----------------------------------------------
        } elseif ($action == 'update_product') {

          $update_sql_data = array('products_last_modified' => 'now()');
          $sql_data_array = array_merge($sql_data_array, $update_sql_data);

          $OSCOM_PDO->save('products', $sql_data_array, ['products_id' => (int)$products_id ] );

        } // end else

          $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                                  customers_group_name,
                                                                  customers_group_discount
                                                  from :table_customers_groups
                                                  where customers_group_id != :customers_group_id
                                                  order by customers_group_id
                                                ');

          $QcustomersGroup->bindInt(':customers_group_id', 0 );
          $QcustomersGroup->execute();

 // Gets all of the customers groups
          while ($customers_group =  $QcustomersGroup->fetch() ) {

            $Qattributes = $OSCOM_PDO->prepare('select g.customers_group_id,
                                                       g.customers_group_price,
                                                       p.products_price
                                                from :table_products_groups g,
                                                     :table_products p
                                                where p.products_id = :products_id
                                                and p.products_id =g.products_id
                                                and g.customers_group_id = :customers_group_id
                                                order by g.customers_group_id
                                                ');
            $Qattributes->bindInt(':products_id',(int)$products_id );
            $Qattributes->bindInt(':customers_group_id',(int)$customers_group['customers_group_id']);
            $Qattributes->execute();

            if ($Qattributes->rowCount() > 0) {
// Definir la position 0 ou 1 pour --> Affichage Prix Public + Affichage Produit + Autorisation Commande 
// L'Affichage des produits, autorisation de commander et affichage des prix mis par defaut en valeur 1 dans la cas de la B2B desactive.
              if (MODE_B2B_B2C == 'true') {
                if (osc_db_prepare_input($_POST['price_group_view' . $customers_group['customers_group_id']]) == '1') {
                  $price_group_view = '1';
                } else {
                  $price_group_view = '0';
                }

                if (osc_db_prepare_input($_POST['products_group_view' . $customers_group['customers_group_id']]) == '1') {
                  $products_group_view = '1';
                } else {
                  $products_group_view = '0';
                }

                if (osc_db_prepare_input($_POST['orders_group_view' . $customers_group['customers_group_id']]) == '1') {
                  $orders_group_view = '1';
                } else {
                  $orders_group_view = '0';
                }

                $products_quantity_unit_id_group = $_POST['products_quantity_unit_id_group' . $customers_group['customers_group_id']];
                $products_model_group  = $_POST['products_model_group' . $customers_group['customers_group_id']];
                $products_quantity_fixed_group  = $_POST['products_quantity_fixed_group' . $customers_group['customers_group_id']];
         
              } else {
                $price_group_view = 1;
                $products_group_view = 1;
                $orders_group_view = 1;
                $products_quantity_unit_id_group = 0;
                $products_model_group = '';
                $products_quantity_fixed_group = 1;

              } //end MODE_B2B_B2C

              $Qupdate = $OSCOM_PDO->prepare('update products_groups
                                              set price_group_view = :price_group_view,
                                                  products_group_view = :products_group_view,
                                                  orders_group_view = :orders_group_view,
                                                  products_quantity_unit_id_group = :products_quantity_unit_id_group,
                                                  products_model_group= :products_model_group,
                                                  products_quantity_fixed_group= :products_quantity_fixed_group
                                              where customers_group_id = :customers_group_id
                                              and products_id = :products_id
                                            ');
              $Qupdate->bindInt(':price_group_view', $price_group_view);
              $Qupdate->bindInt(':products_group_view', $products_group_view);
              $Qupdate->bindInt(':orders_group_view', $orders_group_view);
              $Qupdate->bindInt(':products_quantity_unit_id_group', $products_quantity_unit_id_group);
              $Qupdate->bindValue(':products_model_group', $products_model_group);
              $Qupdate->bindValue(':products_quantity_fixed_group', $products_quantity_fixed_group);
              $Qupdate->bindInt(':customers_group_id', $Qattributes->valueInt('customers_group_id') );
              $Qupdate->bindInt(':products_id', (int)$products_id);
              $Qupdate->execute();

// Prix TTC B2B ----------
              if ( ($_POST['price' . $customers_group['customers_group_id']] != $Qattributes->value('customers_group_price')) && ($Qattributes->valueInt('customers_group_id')  == $customers_group['customers_group_id']) ) {

                $Qupdate = $OSCOM_PDO->prepare('update products_groups
                                                set customers_group_price = :customers_group_price,
                                                    products_price = :products_price
                                                where customers_group_id = :customers_group_id
                                                and products_id = :products_id
                                              ');
                $Qupdate->bindValue(':customers_group_price', $_POST['price' . $customers_group['customers_group_id']]);
                $Qupdate->bindValue(':products_price', $_POST['products_price']);
                $Qupdate->bindInt(':customers_group_id', $Qattributes->valueInt('customers_group_id') );
                $Qupdate->bindInt(':products_id', (int)$products_id);

                $Qupdate->execute();

              } elseif (($_POST['price' . $customers_group['customers_group_id']] == $Qattributes->valueInt('customers_group_price') )) {
                $attributes = $Qattributes->fetch();
              }

//***************************************
// odoo web service
//***************************************
              if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true' && ODOO_ACTIVATE_CUSTOMERS_GROUP == 'true') {
                require('ext/odoo_xmlrpc/xml_rpc_admin_categories_products_group.php');
              }
//***************************************
// End odoo web service
//***************************************


// Prix + Afficher Prix Public + Afficher Produit + Autoriser Commande
            } elseif ($_POST['price' . $customers_group['customers_group_id']] != '') {

              $OSCOM_PDO->save('products_groups', [
                                                 'products_id' => (int)$products_id,
                                                 'products_price' => $_POST['products_price'],
                                                 'customers_group_id' => (int)$customers_group['customers_group_id'],
                                                 'customers_group_price' => $_POST['price' . $customers_group['customers_group_id']],
                                                 'price_group_view' => $_POST['price_group_view' . $customers_group['customers_group_id']] ,
                                                 'products_group_view' => $_POST['products_group_view' . $customers_group['customers_group_id']],
                                                 'orders_group_view' => $_POST['orders_group_view' . $customers_group['customers_group_id']],
                                                 'products_quantity_unit_id_group' => $_POST['products_quantity_unit_id_group' . $customers_group['customers_group_id']],
                                                 'products_model_group' => $_POST['products_model_group' . $customers_group['customers_group_id']],
                                                 'products_quantity_fixed_group' => $_POST['products_quantity_fixed_group' . $customers_group['customers_group_id']]
                                                ]
              );

            } // edn osc_db_num_rows
          } // end while

          $languages = osc_get_languages();

          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];

// Referencement

            $sql_data_array = array('products_name' => osc_db_prepare_input($_POST['products_name'][$language_id]),
                                    'products_description' => osc_db_prepare_input($_POST['products_description'][$language_id]),
                                    'products_head_title_tag' => osc_db_prepare_input($_POST['products_head_title_tag'][$language_id]),
                                    'products_head_desc_tag' => osc_db_prepare_input($_POST['products_head_desc_tag'][$language_id]),
                                    'products_head_keywords_tag' => osc_db_prepare_input($_POST['products_head_keywords_tag'][$language_id]),
                                    'products_url' => osc_db_prepare_input($_POST['products_url'][$language_id]),
                                    'products_head_tag' => osc_db_prepare_input($_POST['products_head_tag'][$language_id]),
                                    'products_shipping_delay' => osc_db_prepare_input($_POST['products_shipping_delay'][$language_id]),
                                    'products_description_summary' => osc_db_prepare_input($_POST['products_description_summary'][$language_id])
                                  );
                                   

            if ($action == 'insert_product') {

              $insert_sql_data = array('products_id' => $products_id,
                                       'language_id' => $language_id);

              $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

              $OSCOM_PDO->save('products_description', $sql_data_array);

//update products
            } elseif ($action == 'update_product') {

              $OSCOM_PDO->save('products_description', $sql_data_array, ['products_id' => (int)$products_id,
                                                                        'language_id' => (int)$language_id
                                                                        ]
                              );
            } // end action
          } //end for

//--------------------------------
// Gallery  
// -------------------------------

          if (osc_not_null($new_dir) && !is_dir($new_dir)) {
// depend server configuration
            mkdir($root_images_dir  . $new_dir, 0755, true);
            chmod($root_images_dir  . $new_dir, 0755);
            $separator = '/';
          }

          if (osc_not_null($_POST['directory'])) {
            $separator = '/';
          }

          $pi_sort_order = 0;
          $piArray = array(0);

        foreach ($_FILES as $key => $value) {
// Update existing large product images

          if (preg_match('/^products_image_large_([0-9]+)$/', $key, $matches)) {
            $pi_sort_order++;

            $sql_data_array = array('htmlcontent' => osc_db_prepare_input($_POST['products_image_htmlcontent_' . $matches[1]]),
                                    'sort_order' => $pi_sort_order);

            $t = new upload($key);
            $t->set_destination(DIR_FS_CATALOG_IMAGES  . $dir);

            if ($t->parse() && $t->save()) {
              $sql_data_array['image'] = $dir . $separator . osc_db_prepare_input($t->filename);
            }


            $OSCOM_PDO->save('products_images', $sql_data_array, ['products_id' => (int)$products_id,
                                                                  'id' => (int)$matches[1]
                                                                 ]
                              );

            $piArray[] = (int)$matches[1];

          } elseif (preg_match('/^products_image_large_new_([0-9]+)$/', $key, $matches)) {
// Insert new large product images
            $sql_data_array = array('products_id' => (int)$products_id,
                                    'htmlcontent' => osc_db_prepare_input($_POST['products_image_htmlcontent_new_' . $matches[1]]));

            $t = new upload($key);
            $t->set_destination(DIR_FS_CATALOG_IMAGES . $dir);
            
            if ($t->parse() && $t->save()) {
              $pi_sort_order++;

              $sql_data_array['image'] = $dir . $separator . osc_db_prepare_input($t->filename);
              $sql_data_array['sort_order'] = $pi_sort_order;

              $OSCOM_PDO->save('products_images', $sql_data_array);

              $piArray[] = $OSCOM_PDO->lastInsertId();
            } // end $t->parse
          } // end preg_match
        } // end foreach
/*
        $QproductImages = $OSCOM_PDO->prepare('select image
                                              from :table_products_images
                                              where products_id = :products_id
                                              and id not in ( :id )
                                            ');
        $QproductImages->bindInt(':products_id', (int)$products_id);
        $QproductImages->bindInt(':id', implode(', ', $piArray));
        $QproductImages->execute();

//        if ($QproductImages->rowCount() > 0 ) {
*/

      $product_images_query = osc_db_query("select image from products_images
                                            where products_id = '" . (int)$products_id . "'
                                            and id not in (" . implode(',', $piArray) . ")
                                            ");

      if (osc_db_num_rows($product_images_query)) {

        while ($product_images = osc_db_fetch_array($product_images_query)) {

            $Qcheck = $OSCOM_PDO->prepare('select count(*)
                                           from :table_products_images
                                           where image = :image
                                          ');
            $Qcheck->bindValue(':image', osc_db_input($product_images['image']) );
            $Qcheck->execute();

            if ($Qcheck->rowCount() < 2) {
              if (file_exists(DIR_FS_CATALOG_IMAGES  . $dir . $product_images['image'])) {
                @unlink(DIR_FS_CATALOG_IMAGES .  $dir . $product_images['image']);
              }
            } // end while
          } //end osc_db_num_rows

          $Qdelete = $OSCOM_PDO->prepare('delete
                                          from :table_products_images
                                          where products_id = :products_id
                                          and id not in ( :id )
                                         ');
          $Qdelete->bindInt(':products_id', (int)$products_id);
          $Qdelete->bindValue(':id', implode(', ', $piArray));
          $Qdelete->execute();
        }

// ---------------------
// extra fields
// ----------------------
          $QextraFields= $OSCOM_PDO->prepare('select *
                                              from :table_products_to_products_extra_fields
                                              where products_id = :products_id
                                              ');
          $QextraFields->bindInt(':products_id', (int)$products_id );
          $QextraFields->execute();

          while ($products_extra_fields = $QextraFields->fetch() ) {
            $extra_product_entry[$products_extra_fields['products_extra_fields_id']] = $products_extra_fields['products_extra_fields_value'];
          }

          if ($_POST['extra_field']) { // Check to see if there are any need to update extra fields.
            foreach ($_POST['extra_field'] as $key=>$val) {

// concerne la gestion de la case & cocher et de sa suppression ou non de la table products_to_products_extra_fields (impact sur le catalogue)
              if ($val == 'NO_DISPLAY_CHECKBOX') {
                $val = '';
              }
 
              if (isset($extra_product_entry[$key])) { // an entry exists
                if ($val == '') {
                  $Qdelete = $OSCOM_PDO->prepare('delete 
                                                  from :table_products_to_products_extra_fields
                                                  where products_id = :products_id 
                                                  and products_extra_fields_id = :products_extra_fields_id
                                                ');
                  $Qdelete->bindInt(':products_id', (int)$products_id);
                  $Qdelete->bindInt(':products_extra_fields_id', (int)$key);
                  $Qdelete->execute();

                } else {

                  $Qupdate = $OSCOM_PDO->prepare('update :table_products_to_products_extra_fields
                                                  set products_extra_fields_value = :products_extra_fields_value 
                                                   where products_id = :products_id
                                                   and products_extra_fields_id = :products_extra_fields_id
                                                ');
                  $Qupdate->bindValue(':products_extra_fields_value', $val );
                  $Qupdate->bindInt(':products_id', (int)$products_id );
                  $Qupdate->bindInt(':products_extra_fields_id', (int)$key );
                  $Qupdate->execute();
                }

              } else { // an entry does not exist
                if ($val != '') {

                  $OSCOM_PDO->save('products_to_products_extra_fields', [
                                                                          'products_id' => (int)$products_id,
                                                                          'products_extra_fields_id' => (int)$key,
                                                                          'products_extra_fields_value' => $val
                                                                        ]
                                  );

                }
              }
            } // end foreach
          } // end $_POST['extra_field']

          if (USE_CACHE == 'true') {
            $languages = osc_get_languages();
            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
              $language_id = $languages[$i]['id'];
              osc_cache_clear('category_tree-' . $language_id  .'.cache');
            }
            osc_reset_cache_block('also_purchased');
            osc_reset_cache_block('products_related');
            osc_reset_cache_block('products_cross_sell');
            osc_reset_cache_block('upcoming');
          }

// ------------------------------------------------------
// Clone product in categorie follow and update
// New field change in the functions          
// ------------------------------------------------------

        if ($action == 'update_product') {
          $multi_clone_categories_id_to = $_POST['clone_categories_id_to'];
          if (!empty($multi_clone_categories_id_to)) {
            $clone_products_id = $products_id;
            echo osc_get_product_clone_to_category ();
          }// end if !empty
        } // end if update

// twitter
      if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
        if ($twitter_status == '1') {

          $Qproducts = $OSCOM_PDO->prepare("select distinct p.products_image,
                                                           pd.products_name,
                                                           p.products_status
                                           from :table_products p,
                                                :table_products_description pd
                                            where p.products_id = :products_id
                                            and p.products_id = pd.products_id
                                            and pd.language_id = :language_id
                                            and p.products_status = :products_status
                                          ");
          $Qproducts->bindInt(':products_id', (int)$products_id );
          $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
          $Qproducts->bindValue(':products_status', 1 );
          $Qproducts->execute();

          $products = $Qproducts->fetch();

          if ($products['products_status'] == 1) {

            $twitter_image =  DIR_FS_CATALOG_IMAGES . $products['products_image'];

            $twitter_text = TEXT_TWITTER_PRODUCTS;
            $text_twitter_products =  $twitter_text . $products['products_name'] .' : ' . HTTP_SERVER . DIR_WS_CATALOG . 'product_info.php?products_id='. $products_id;

            $_POST['twitter_msg'] =  $text_twitter_products;
            $_POST['twitter_media'] =  $twitter_image;

            echo osc_send_twitter($twitter_authentificate_administrator);
          }
        }
      }

//***************************************
// odoo web service
//***************************************
        if (ODOO_ACTIVATE_WEB_SERVICE == 'true' && ODOO_ACTIVATE_PRODUCTS_ADMIN == 'true') {
          require('ext/odoo_xmlrpc/xml_rpc_admin_products.php');
        }
//***************************************
// End odoo web service
//***************************************
        osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $products_id));
      break;

      case 'copy_to_confirm':
        if (isset($_POST['products_id']) && isset($_POST['categories_id'])) {
          $products_id = osc_db_prepare_input($_POST['products_id']);
          $categories_id = osc_db_prepare_input($_POST['categories_id']);

          if ($_POST['copy_as'] == 'link') {
            if ($categories_id != $current_category_id) {

              $Qcheck = $OSCOM_PDO->prepare("select count(*) as total 
                                             from :table_products_to_categories
                                             where products_id = :products_id
                                             and categories_id = :categories_id
                                          ");
              $Qcheck->bindInt(':products_id', (int)$products_id );
              $Qcheck->bindInt(':categories_id', (int)$categories_id );
              $Qcheck->execute();

              $check = $Qcheck->fetch();

              if ($check['total'] < '1') {

                $OSCOM_PDO->save('products_to_categories', [
                                                              'products_id' => (int)$products_id,
                                                              'categories_id' => (int)$categories_id
                                                            ]
                                );

              }
            } else {
              $OSCOM_MessageStack->add_session(ERROR_CANNOT_LINK_TO_SAME_CATEGORY, 'error');
            }
          } elseif ($_POST['copy_as'] == 'duplicate') {
//Duplication des champs ou copie de la table product

              $Qproduct = $OSCOM_PDO->prepare('select products_quantity, 
                                                      products_model,
                                                      products_ean,
                                                      products_sku,
                                                      products_image,  
                                                      products_image_zoom, 
                                                      products_price, 
                                                      products_date_available, 
                                                      products_weight, 
                                                      products_price_kilo,
                                                      products_tax_class_id, 
                                                      manufacturers_id,
                                                      suppliers_id,
                                                      products_min_qty_order,
                                                      products_view,
                                                      orders_view,
                                                      products_price_comparison,
                                                      products_dimension_width,
                                                      products_dimension_height,
                                                      products_dimension_depth,
                                                      products_dimension_type,
                                                      admin_user_name,
                                                      products_volume,
                                                      products_quantity_unit_id,
                                                      products_only_online,
                                                      products_image_medium,
                                                      products_weight_pounds,
                                                      products_cost,
                                                      products_handling,
                                                      products_wharehouse_time_replenishment,
                                                      products_wharehouse,
                                                      products_wharehouse_row,
                                                      products_wharehouse_level_location,
                                                      products_packaging,
                                                      products_sort_order,
                                                      products_quantity_alert,
                                                      products_only_shop,
                                                      products_download_filename,
                                                      products_download_public,
                                                      products_type
                                                 from :table_products
                                                 where products_id = :products_id
                                                ');
              $Qproduct->bindInt(':products_id', (int)$products_id );
              $Qproduct->execute();

              $product = $Qproduct->fetch();


             $OSCOM_PDO->save('products', [
                                             'products_quantity' => $product['products_quantity'],
                                             'products_model' => $product['products_model'],
                                             'products_ean' => $product['products_ean'],
                                             'products_sku' => $product['products_sku'],
                                             'products_image' => $product['products_image'],
                                             'products_image_zoom' => $product['products_image_zoom'],
                                             'products_price' => $product['products_price'],
                                             'products_date_added' => 'now()',
                                             'products_date_available' => (empty($product['products_date_available']) ? "null" : "'" . $product['products_date_available'] . "'"),
                                             'products_weight' => $product['products_weight'],
                                             'products_price_kilo' => $product['products_price_kilo'],
                                             'products_status' => 0,
                                             'products_tax_class_id' => (int)$product['products_tax_class_id'],
                                             'manufacturers_id' => (int)$product['manufacturers_id'],
                                             'products_view' => $product['products_view'],
                                             'orders_view' =>  $product['orders_view'],
                                             'suppliers_id' => $product['suppliers_id'],
                                             'products_min_qty_order' => $product['products_min_qty_order'],
                                             'products_price_comparison' => $product['products_price_comparison'],
                                             'products_dimension_width' => $product['products_dimension_width'],
                                             'products_dimension_height' => $product['products_dimension_height'],
                                             'products_dimension_depth' => $product['products_dimension_depth'],
                                             'products_dimension_type' => $product['products_dimension_type'],
                                             'admin_user_name' => osc_user_admin($user_administrator),
                                             'products_volume' => $product['products_volume'],
                                             'products_quantity_unit_id' => $product['products_quantity_unit_id'],
                                             'products_only_online' => $product['products_only_online'],
                                             'products_image_medium' => $product['products_image_medium'],
                                             'products_weight_pounds' => $product['products_weight_pounds'],
                                             'products_cost' => $product['products_cost'],
                                             'products_handling' => $product['products_handling'],
                                             'products_wharehouse_time_replenishment' => $product['products_wharehouse_time_replenishment'],
                                             'products_wharehouse' => $product['products_wharehouse'],
                                             'products_wharehouse_row' => $product['products_wharehouse_row'],
                                             'products_wharehouse_level_location' => $product['products_wharehouse_level_location'],
                                             'products_packaging' => $product['products_packaging'],
                                             'products_sort_order' => $product['products_sort_order'],
                                             'products_quantity_alert' => $product['products_quantity_alert'],
                                             'products_only_shop' => $product['products_only_shop'],
                                             'products_download_filename' => $product['products_download_filename'] ,
                                             'products_download_public' => $product['products_download_public'],
                                             'products_type' => $product['products_type']
                                           ]
                               );

              $dup_products_id = $OSCOM_PDO->lastInsertId();

// ---------------------
// gallery
// ----------------------
            $QproductImages = $OSCOM_PDO->prepare('select image,
                                                           htmlcontent,
                                                           sort_order
                                                    from :table_products_images
                                                    where products_id = :products_id
                                                  ');
            $QproductImages->bindInt(':products_id', (int)$products_id );
            $QproductImages->execute();

            while ($product_images = $QproductImages->fetch() ) {

              $OSCOM_PDO->save('products_images', [
                                                    'products_id' => (int)$dup_products_id,
                                                    'image' => $product_images['image'],
                                                    'htmlcontent' => $product_images['htmlcontent'],
                                                    'sort_order' => $product_images['sort_order']
                                                  ]
                              );

            }

// ---------------------
// referencement
// ----------------------
            $Qdescription = $OSCOM_PDO->prepare('select language_id,
                                                        products_name,
                                                        products_description,
                                                        products_head_title_tag,
                                                        products_head_desc_tag,
                                                        products_head_keywords_tag,
                                                        products_url,
                                                        products_head_tag,
                                                        products_shipping_delay,
                                                        products_description_summary
                                                 from :table_products_description
                                                 where products_id = :products_id
                                                 ');
            $Qdescription->bindInt(':products_id', (int)$products_id );
            $Qdescription->execute();

            while ($description = $Qdescription->fetch() ) {

              $OSCOM_PDO->save('products_description', [
                                                        'products_id' => (int)$dup_products_id,
                                                        'language_id' => (int)$description['language_id'],
                                                        'products_name' => $description['products_name'],
                                                        'products_description' => $description['products_description'],
                                                        'products_head_title_tag' => $description['products_head_title_tag'],
                                                        'products_head_desc_tag' => $description['products_head_desc_tag'],
                                                        'products_head_keywords_tag' => $description['products_head_keywords_tag'],
                                                        'products_url' => $description['products_url'],
                                                        'products_viewed' => 0,
                                                        'products_head_tag' => $description['products_head_tag'],
                                                        'products_shipping_delay' => $description['products_shipping_delay'],
                                                        'products_description_summary' => $description['products_description_summary']
                                                      ]
                              );

            }

            $OSCOM_PDO->save('products_to_categories', [
                                                        'products_id' => (int)$dup_products_id,
                                                        'categories_id' =>(int)$categories_id
                                                       ]
                            );

            $products_id = $dup_products_id;
          }

          if (USE_CACHE == 'true') {
            $languages = osc_get_languages();

            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
              $language_id = $languages[$i]['id'];
              osc_cache_clear('category_tree-' . $language_id  .'.cache');
            }
            osc_reset_cache_block('also_purchased');
            osc_reset_cache_block('products_related');
            osc_reset_cache_block('products_cross_sell');
            osc_reset_cache_block('upcoming');
          }
        }

        osc_redirect(osc_href_link('categories.php', 'cPath=' . $categories_id . '&pID=' . $products_id));

        break;

// archive a product
      case 'archive_to_confirm':
        $products_id = osc_db_prepare_input($_POST['products_id']);
        $categories_id = osc_db_prepare_input($_POST['categories_id']);

        $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                        set products_archive = :products_archive
                                        where products_id = :products_id
                                      ');
        $Qupdate->bindInt(':products_archive', '1');
        $Qupdate->bindInt(':products_id', (int)$products_id);
        $Qupdate->execute();

// Mise a zero des stats

        $Qupdate = $OSCOM_PDO->prepare('update :table_products_description
                                        set products_viewed = :products_viewed
                                        where products_id = :products_id
                                      ');
        $Qupdate->bindInt(':products_viewed', '0');
        $Qupdate->bindInt(':products_id', (int)$products_id);
        $Qupdate->execute();

// update products viewed
        $Qupdate = $OSCOM_PDO->prepare('update :table_products
                                        set products_ordered = :products_ordered
                                        where products_id = :products_id
                                      ');
        $Qupdate->bindInt(':products_ordered', '0');
        $Qupdate->bindInt(':products_id', (int)$products_id);
        $Qupdate->execute();

// update the products cross sell and related master

        $Qupdate = $OSCOM_PDO->prepare('update :table_products_related
                                        set products_cross_sell = :products_cross_sell,
                                        products_related = :products_related
                                        where products_related_id_master = :products_related_id_master
                                      ');
        $Qupdate->bindInt(':products_cross_sell', '0');
        $Qupdate->bindInt(':products_related', '0');
        $Qupdate->bindInt(':products_related_id_master', (int)$products_id);
        $Qupdate->execute();

// update the products cross sell and related salve

        $Qupdate = $OSCOM_PDO->prepare('update :table_products_related
                                        set products_cross_sell = :products_cross_sell,
                                        products_related = :products_related
                                        where products_related_id_slave = :products_related_id_slave
                                      ');
        $Qupdate->bindInt(':products_cross_sell', '0');
        $Qupdate->bindInt(':products_related', '0');
        $Qupdate->bindInt(':products_related_id_slave', (int)$products_id);
        $Qupdate->execute();

        if (USE_CACHE == 'true') {
          $languages = osc_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];
            osc_cache_clear('category_tree-' . $language_id  .'.cache');
          }
          osc_reset_cache_block('also_purchased');
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
          osc_reset_cache_block('upcoming');
        }

      osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $products_id));
     break;


// the images are not deleted in this case
// delete all product selected
      case 'delete_all':

       if ($_POST['selected'] != '') { 
         foreach ($_POST['selected'] as $products['products_id'] ) {
// Pour la suppression des produits voir la fonction osc_remove
//        if (isset($_POST['products_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories'])) {
           if (isset($products['products_id'])) {
             $products_id = $products['products_id'];

             $Qcheck = $OSCOM_PDO->prepare('select count(*)
                                           from :table_products_to_categories
                                           where products_id = :products_id
                                          ');
             $Qcheck->bindInt(':products_id', (int)$products_id);
             $Qcheck->execute();

             if ($Qcheck->rowCount() > 0 ) {
               osc_remove_product($products_id);
             }
           }
         }
       }

        if (USE_CACHE == 'true') {
          $languages = osc_get_languages();
          for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $language_id = $languages[$i]['id'];
            osc_cache_clear('category_tree-' . $language_id  .'.cache');
          }
          osc_reset_cache_block('also_purchased');
          osc_reset_cache_block('products_related');
          osc_reset_cache_block('products_cross_sell');
          osc_reset_cache_block('upcoming');
        }

      osc_redirect(osc_href_link('categories.php', 'cPath=' . $cPath));
      break;
    }
  }

// check if the catalog image directory exists
  if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!osc_is_writable(DIR_FS_CATALOG_IMAGES)) $OSCOM_MessageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
  } else {
    $OSCOM_MessageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
  }

  require('includes/header.php');
?>
<script type="text/javascript"><!--
function popupImageWindow(url) {
  window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<script type="text/javascript"><!--
function popupFilename(url) {
  window.open(url,'popupFilename','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=900,height=600,screenX=550,screenY=25,top=150,left=500')
}
//--></script>
<script type="text/javascript" src="ext/ckeditor/ckeditor.js"></script>

<?php
  //######################################################################################################## 
  //                                            EDITION CATEGORIE                                           
  //########################################################################################################
?>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top">
<!-- //######################################################################################################## //-->
<!-- //                                            EDITION CATEGORIE                                            //-->
<!-- //######################################################################################################## //-->
<?php 
  if ($_GET['action'] == 'new_category' || $_GET['action'] == 'edit_category') {
    if ( ($_GET['cID']) && (!$_POST) ) {


      $Qcategories = $OSCOM_PDO->prepare('select c.categories_id, 
                                                 cd.categories_name, 
                                                 cd.categories_description,
                                                 cd.categories_head_title_tag,
                                                 cd.categories_head_desc_tag,
                                                 cd.categories_head_keywords_tag,
                                                 c.categories_image, 
                                                 c.parent_id, 
                                                 c.sort_order, 
                                                 c.date_added, 
                                                 c.last_modified 
                                         from :table_categories c, 
                                              :table_categories_description cd
                                         where c.categories_id = :categories_id 
                                         and c.categories_id = cd.categories_id 
                                         and cd.language_id = :language_id
                                         order by c.sort_order, 
                                                  cd.categories_name
                                        ');
      $Qcategories->bindInt(':categories_id', (int)$_GET['cID'] );
      $Qcategories->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
      $Qcategories->execute();

      $category = $Qcategories->fetch();

      $cInfo = new objectInfo($category);
    } else {
      $cInfo = new objectInfo(array());
    } 

    $languages = osc_get_languages();
    $form_action = (isset($_GET['cID'])) ? 'update_category' : 'insert_category'; 
    echo osc_draw_form('new_category', 'categories.php', 'cPath=' . $cPath . '&cID=' . $_GET['cID'] . '&action='.$form_action, 'post', 'enctype="multipart/form-data"');
?>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <div>
          <div class="adminTitle">
            <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/categorie_produit.gif', HEADING_TITLE, '40', '40'); ?></span>
            <span class="col-md-4 pageHeading pull-left"><?php echo '&nbsp;' . TABLE_HEADING_CATEGORIES; ?></span>
            <span class="pull-right"><?php echo osc_draw_hidden_field('categories_date_added', (($cInfo->date_added) ? $cInfo->date_added : date('Y-m-d'))) . osc_draw_hidden_field('parent_id', $cInfo->parent_id) . osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
            <span class="pull-right" style="padding-left:5px;"><?php echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $_GET['cID']) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
          </div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        </div>
        <div class="clearfix"></div>
      </tr>
      <tr>
        <td>
          <?php echo osc_draw_separator('pixel_trans.gif', '2', '10'); ?>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' .  TAB_GENERAL . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' .  TAB_DESC . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' .  TAB_REF . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab4" role="tab" data-toggle="tab">' .  TAB_IMG . '</a>'; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
<?php
// -------------------------------------------------------------------
//          ONGLET General sur la description de la categorie
// -------------------------------------------------------------------
?>
                <div class="tab-pane active" id="tab1">

                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TEXT_PRODUCTS_NAME; ?></div>
                    <div class="pull-right"><?php echo TEXT_USER_NAME . osc_user_admin($user_administrator); ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div class="spaceRow"></div>
                    <div class="row">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                        <div class="col-md-12">
                          <div  class="col-md-9">
                            <span class="col-md-1 centerInputFields"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                            <span class="col-md-3 pull-left" style="height:50px;"><?php echo osc_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', (isset($categories_name[$languages[$i]['id']]) ? $categories_name[$languages[$i]['id']] : osc_get_category_name($cInfo->categories_id, $languages[$i]['id'])), 'class="form-control" required aria-required="true" required="" id="categories_name" placeholder="' . TEXT_EDIT_CATEGORIES_NAME . '"',  true) . '&nbsp;'; ?></span>
                          </div>
                        </div>

<?php
    }
?>
                    </div>
                  </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="col-md-12 mainTitle"><?php echo TEXT_DIVERS_TITLE; ?></div>
                  <div class="adminformTitle">
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-1 centerInputFields"><?php echo TEXT_EDIT_SORT_ORDER; ?></span>
                        <span class="col-md-1"><?php echo osc_draw_input_field('sort_order',$cInfo->sort_order, 'size="2"'); ?></span>
                      </div>
                    </div>
                  </div>
                </div>
<?php
// ----------------------------------------------------------- //-->
//          ONGLET sur la designation de la categorie          //-->
// ----------------------------------------------------------- //-->
?>
                <div class="tab-pane" id="tab2">
                  <div class="col-md-12 mainTitle">
                    <span><?php echo TEXT_DESCRIPTION_CATEGORIES; ?></span>
                  </div>
                  <div class="adminformTitle">
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div>
                      <script type='text/javascript' src='ext/ckeditor/ckeditor.js'></script>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                        <span class="col-md-2"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                        <span class="col-md-10">
                          <div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('categories_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300', (isset($categories_description[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($categories_description[$languages[$i]['id']])) : osc_get_category_description($cInfo->categories_id, $languages[$i]['id'])));?></div>
                        </span>
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
    }
?>
                    </div>
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="adminformAide">
                      <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_DESCRIPTION); ?>
                        <strong><?php echo '&nbsp;' . TITLE_HELP_DESCRIPTION; ?></strong>
                      </span>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                         <span class="col-md-12">
                           <?php echo HELP_OPTIONS; ?>
                           <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg2"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                           <div class="modal fade" id="myModalWysiwyg2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                   <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                                 </div>
                                 <div class="modal-body" style="text-align:center;">
                                   <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                                 </div>
                               </div>
                             </div>
                           </div>
                         </span>
                      </div>
                    </div>
                  </div>
                </div>
<?php
// -----------------------------------------------------//-->
//          ONGLET sur le référencement categories      //-->
// ---------------------------------------------------- //-->
?>
<!-- decompte caracteres -->
<script type="text/javascript">
  $(document).ready(function(){
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
    //default title
    $("#default_title_<?php echo $i?>").charCount({
      allowed: 70,
      warning: 20,
      counterText: ' Max : '
    });

    //default_description
    $("#default_description_<?php echo $i?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

    //default tag
    $("#default_tag_<?php echo $i?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '
    });
<?php
   }
?>
  });
</script>
                <div class="tab-pane" id="tab3">
                  <div class="col-md-12 mainTitle">
                    <span><?php echo TEXT_PRODUCTS_PAGE_REFEFRENCEMENT; ?></span>
                  </div>
                  <div class="adminformTitle">
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-3"></span>
                        <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                        <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                      </div>
                    </div>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <div class="row">
                        <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_PAGE_TITLE; ?></span>
                          <span  class="col-md-6"><?php echo  '&nbsp;' . osc_draw_input_field('categories_head_title_tag[' . $languages[$i]['id'] . ']', (($categories_head_title_tag[$languages[$i]['id']]) ? $categories_head_title_tag[$languages[$i]['id']] : osc_get_categories_head_title_tag($cInfo->categories_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?></span>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_HEADER_DESCRIPTION; ?></span>
                          <span  class="col-md-6"><?php echo osc_draw_textarea_field('categories_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '2', (isset($categories_head_desc_tag[$languages[$i]['id']]) ? $categories_head_desc_tag[$languages[$i]['id']] : osc_get_categories_head_desc_tag($cInfo->categories_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></span>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_KEYWORDS; ?></span>
                          <span  class="col-md-6"><?php echo osc_draw_textarea_field('categories_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '5', (isset($categories_head_keywords_tag[$languages[$i]['id']]) ? $categories_head_keywords_tag[$languages[$i]['id']] : osc_get_categories_head_keywords_tag($cInfo->categories_id, $languages[$i]['id']))); ?></span>
                        </div>
                      </div>

<?php
    }
?>
                    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                    <div class="adminformAide">
                      <div class="row">
                        <span class="col-md-12">
                          <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                          <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                        </span>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                      </div>
                    </div>
                  </div>
                </div>
<?php
// -----------------------------------------------------//-->
//          ONGLET sur l'image de la categorie          //-->
// ---------------------------------------------------- //-->
?>
                <div class="tab-pane" id="tab4">
                  <div class="mainTitle">
                    <span><?php echo TEXT_CATEGORIES_IMAGE_TITLE; ?></span>
                  </div>

                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminformTitle">
                    <tr>
                      <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
<!-- Editeur FCKeditor //-->
<!-- Le chiffre 700 (=width) et 150 (=height) permet de pouvoir choisir la taille de la fenetre (voir la fonction dans admin/includes/funtions/html_output.php)  //-->
                        <tr>
                          <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_CATEGORIES_IMAGE_VIGNETTE, '40', '40'); ?></td>
                              <td class="main"><?php echo TEXT_CATEGORIES_IMAGE_VIGNETTE; ?></td>
                            </tr>
                            <tr>
                              <td class="main" colspan="2"><?php echo osc_draw_file_field_image_ckeditor('categories_image', '300', '300', ''); ?></td>
                            </tr>
                          </table></td>
                          <td valign="top" width="80%"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td><table cellpadding="0" cellspacing="0" border="0">
                                 <tr>
                                  <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_categories.gif', TEXT_CATEGORIES_IMAGE_VISUEL, '40', '40'); ?></td>
                                  <td class="main" align="left"><?php echo TEXT_CATEGORIES_IMAGE_VISUEL; ?></td>
                                 </tr>
                              </table></td>
                            </tr>
                            <tr align="center">
                              <td width="100%" align="center" valign="top"><table width="100%" border="0" class="adminformAide">
                                 <tr>
                                   <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                 </tr>
                                 <tr>
                                   <td align="center"><?php echo osc_info_image($cInfo->categories_image, TEXT_CATEGORIES_IMAGE_VIGNETTE); ?></td>
                                     <tr>
                                       <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                     </tr>
                                     <tr>
                                       <td class="main" align="right" colspan="2"><?php echo TEXT_CATEGORIES_DELETE_IMAGE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                     </tr>
                                   </table></td>
                                 </tr>
                               </table></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <table width="100%" border="0" cellspacing="0" cellpadding="5">
                            <tr>
                              <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                            </tr>
                          </table>
                          <table width="100%" border="0" cellspacing="2" cellpadding="2" class="adminformAide">
                            <tr>
                              <td><table border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                  <td class="main"><?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_IMAGE); ?></td>
                                  <td class="main"><strong><?php echo '&nbsp;' . TITLE_AIDE_IMAGE; ?></strong></td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td><table border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                  <td><?php echo osc_draw_separator('pixel_trans.gif', '16', '1'); ?></td>
                                  <td class="main"><?php echo HELP_IMAGE_CATEGORIES; ?></td>
                                </tr>
                              </table></td>
                            </tr>
                          </table>
                        </tr>
                      </table></td>
                    </tr>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        </table></form>
<?php
     } elseif ($action == 'new_product') {
 // ################################################################################################################ -->
 //                                           EDITION OU NOUVEAU PRODUIT                                            -->
 //################################################################################################################ -->
  //if ($action == 'new_product') {

    $parameters = array('products_name' => '',
                       'products_description' => '',
                       'products_url' => '',
                       'products_id' => '',
                       'products_quantity' => '',
                       'products_model' => '',
                       'products_ean' => '',
                       'products_sku' => '',
                       'products_image' => '',
                       'products_image_zoom' => '',
                       'products_larger_images' => array(),
                       'products_price' => '',
                       'products_weight' => '',
                       'products_price_kilo' => '',
                       'products_date_added' => '',
                       'products_last_modified' => '',
                       'products_date_available' => '',
                       'products_status' => '',
// products_view : Affichage Produit Grand Public - orders_view : Autorisation Commande
                       'products_percentage' => '',
                       'products_view' => '',
                       'orders_view' => '',
                       'products_tax_class_id' => '',
                       'manufacturers_id' => '',
                       'suppliers_id' => '',
                       'products_min_qty_order' => '',
                       'products_price_comparison' => '',
                       'products_dimension_width' => '',
                       'products_dimension_height' => '',
                       'products_dimension_depth' => '',
                       'products_dimension_type' => '',
                       'products_volume' => '',
                       'products_quantity_unit_id' =>'',
                       'products_only_online' =>'',
                       'products_image_medium' => '',
                       'products_weight_pounds' => '',
                       'products_cost' => '',
                       'products_handling' => '',
                       'products_wharehouse_time_replenishment' => '',
                       'products_wharehouse' => '',
                       'products_wharehouse_row' => '',  
                       'products_wharehouse_level_location' => '',
                       'products_packaging' => '',
                       'products_sort_order' => '',
                       'products_shipping_delay' => '',
                       'products_quantity_alert' => '',
                       'products_only_shop' => '',
                       'products_download_filename' => '',
                       'products_download_public' => '',
                       'products_description_summary' => '',
                       'products_type' =>''
                      );

    $pInfo = new objectInfo($parameters);

    if (isset($_GET['pID']) && empty($_POST)) {
// products_view : Affichage Produit Grand Public - orders_view : Autorisation Commande - Referencement

      $Qproducts = $OSCOM_PDO->prepare("select pd.products_name, 
                                                pd.products_description, 
                                                pd.products_url, 
                                                pd.products_head_title_tag, 
                                                pd.products_head_desc_tag, 
                                                pd.products_head_keywords_tag,
                                                pd.products_head_tag,
                                                pd.products_shipping_delay,
                                                p.products_id, 
                                                p.products_quantity, 
                                                p.products_model, 
                                                p.products_ean,
                                                p.products_sku,
                                                p.products_image, 
                                                p.products_image_zoom, 
                                                p.products_price, 
                                                p.products_weight, 
                                                p.products_price_kilo, 
                                                p.products_date_added, 
                                                p.products_last_modified, 
                                                date_format(p.products_date_available, '%Y-%m-%d') as products_date_available,
                                                p.products_status, 
                                                p.products_percentage, 
                                                p.products_tax_class_id, 
                                                p.manufacturers_id, 
                                                p.products_view, 
                                                p.orders_view,
                                                p.suppliers_id,
                                                p.products_min_qty_order,
                                                p.products_price_comparison,
                                                p.products_dimension_width,
                                                p.products_dimension_height,
                                                p.products_dimension_depth,
                                                p.products_dimension_type,
                                                p.products_volume,
                                                p.products_quantity_unit_id,
                                                p.products_only_online,
                                                p.products_image_medium,
                                                p.products_weight_pounds,
                                                p.products_cost,
                                                p.products_handling,
                                                p.products_wharehouse_time_replenishment,
                                                p.products_wharehouse,
                                                p.products_wharehouse_row,  
                                                p.products_wharehouse_level_location,
                                                p.products_packaging,
                                                p.products_sort_order,
                                                p.products_quantity_alert,
                                                p.products_only_shop,
                                                p.products_download_filename,
                                                p.products_download_public,
                                                pd.products_description_summary,
                                                p.products_type
                                       from :table_products p, 
                                            :table_products_description pd 
                                       where p.products_id = :products_id 
                                       and p.products_id = pd.products_id 
                                       and pd.language_id = :language_id
                                     ");
      $Qproducts->bindInt(':products_id', (int)$_GET['pID'] );
      $Qproducts->bindInt(':language_id',  (int)$_SESSION['languages_id'] );
      $Qproducts->execute();

      $product = $Qproducts->fetch();

      $pInfo->objectInfo($product);

// ---------------------
// gallery
// ----------------------
      $QproductImage = $OSCOM_PDO->prepare('select id,
                                                   image,
                                                   htmlcontent,
                                                   sort_order
                                            from :table_products_images
                                            where products_id = :products_id
                                            order by sort_order
                                            ');
      $QproductImage->bindInt(':products_id', (int)$_GET['pID']);
      $QproductImage->execute();

      while ($product_images = $QproductImage->fetch() ) {

        $pInfo->products_larger_images[] = array('id' => $product_images['id'],
                                                 'image' => $product_images['image'],
                                                 'htmlcontent' => $product_images['htmlcontent'],
                                                 'sort_order' => $product_images['sort_order']
                                                );
      }

// ---------------------
// extra fields
// ----------------------
      $QproductsExtraFields = $OSCOM_PDO->prepare('select *
                                                   from :table_products_to_products_extra_fields
                                                   where products_id = :products_id
                                                  ');
      $QproductsExtraFields->bindInt(':products_id', (int)$_GET['pID']);
      $QproductsExtraFields->execute();

      while ($products_extra_fields = $QproductsExtraFields->fetch() ) {
        $extra_field[$products_extra_fields['products_extra_fields_id']] = $products_extra_fields['products_extra_fields_value'];
      }
      $extra_field_array=array('extra_field'=>$extra_field);
      $pInfo->objectInfo($extra_field_array);
    }

// dropdown for produt qty unit
    $products_quantity_unit_array = array(array('id' => '',
                                                'text' => TEXT_NONE));

    $QproductsQuantityUnit = $OSCOM_PDO->prepare('select products_quantity_unit_id,
                                                         products_quantity_unit_title
                                                 from :table_products_quantity_unit
                                                 where language_id = :language_id
                                                 order by products_quantity_unit_id
                                                ');
    $QproductsQuantityUnit->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $QproductsQuantityUnit->execute();

    while ($products_quantity_unit = $QproductsQuantityUnit->fetch() ) {
      $products_quantity_unit_array[] = array('id' => $products_quantity_unit['products_quantity_unit_id'],
                                               'text' => $products_quantity_unit['products_quantity_unit_title']);
    }

    $tax_class_array = array(array('id' => '0',
                                   'text' => TEXT_NONE));

    $QtaxClass = $OSCOM_PDO->prepare('select tax_class_id,
                                            tax_class_title
                                     from :table_tax_class
                                     order by tax_class_title
                                    ');
    $QtaxClass->execute();

    while ($tax_class = $QtaxClass->fetch() ) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

    $languages = osc_get_languages();

    if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
    switch ($pInfo->products_status) {
      case '0': $in_status = false; $out_status = true; break;
      case '1':
      default: $in_status = true; $out_status = false;
    }


    if (!isset($pInfo->products_price_comparison)) $pInfo->products_price_comparison = '1';
    switch ($pInfo->products_price_comparison) {
      case '0': $in_comparison = false; $out_comparison = true; break;
      case '1':
      default: $in_comparison = true; $out_comparison = false;
    }


//  B2B
    if (!isset($pInfo->products_percentage)) $pInfo->products_percentage = '1';
    switch ($pInfo->products_percentage) {
      case '0': $in_percent = false; $out_percent = true; break;
      case '1':
      default: $in_percent = true; $out_percent = false;
    }

    $form_action = (isset($_GET['pID'])) ? 'update_product' : 'insert_product';
?>


<script type="text/javascript"><!--
var tax_rates = new Array();
<?php
    for ($i=0, $n=sizeof($tax_class_array); $i<$n; $i++) {
      if ($tax_class_array[$i]['id'] > 0) {
        echo 'tax_rates["' . $tax_class_array[$i]['id'] . '"] = ' . osc_get_tax_rate_value($tax_class_array[$i]['id']) . ';' . "\n";
      }
    }
?>

function doRound(x, places) {
  return Math.round(x * Math.pow(10, places)) / Math.pow(10, places);
}

function getTaxRate() {
  var selected_value = document.forms["new_product"].products_tax_class_id.selectedIndex;
  var parameterVal = document.forms["new_product"].products_tax_class_id[selected_value].value;

  if ( (parameterVal > 0) && (tax_rates[parameterVal] > 0) ) {
    return tax_rates[parameterVal];
  } else {
    return 0;
  }
}

function updateGross() {
  var taxRate = getTaxRate();
  var grossValue = document.forms["new_product"].products_price.value;
  if (taxRate > 0) {
    grossValue = grossValue * ((taxRate / 100) + 1);
  }

<?php
// Desactivation du module B2B
    if (MODE_B2B_B2C == 'true') {
      $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                             customers_group_name,
                                                             customers_group_discount
                                             from :table_customers_groups
                                             where customers_group_id != :customers_group_id
                                             order by customers_group_id
                                            ');
       $QcustomersGroup->bindInt(':customers_group_id', 0);
       $QcustomersGroup->execute();

      while ($customers_group = $QcustomersGroup->fetch() ) {
?>
        var grossValue<?php echo $customers_group['customers_group_id'] ?> = document.forms["new_product"].price<?php echo $customers_group['customers_group_id'] ?>.value;

        if (taxRate > 0) {
          grossValue<?php echo $customers_group['customers_group_id'] ?> = grossValue<?php echo $customers_group['customers_group_id'] ?> * ((taxRate / 100) + 1);
        }

        document.forms["new_product"].price_gross<?php echo $customers_group['customers_group_id'] ?>.value = doRound(grossValue<?php echo $customers_group['customers_group_id'] ?>, 4);
<?php
      }
    }
?>
  document.forms["new_product"].products_price_gross.value = doRound(grossValue, 4);
}

/********************************/
/*        Margin report         */
/********************************/


function updateMargin() {

  var grossValue = document.forms["new_product"].products_price.value; // valeur net du prix
  var costValue = document.forms["new_product"].products_cost.value; // cout d'achat
  var handlingValue = document.forms["new_product"].products_handling.value; // manutention ou autres frais

 
  if (isNaN(costValue)) costValue=0;
  if (isNaN(handlingValue)) handlingValue=0;
 
  marginValue =  100 - ((( parseInt(costValue) + parseInt(handlingValue)) /  parseInt(grossValue)) * 100);
  marginValue = Math.round(marginValue,2);
  document.getElementById('products_price_margins').innerHTML = marginValue + "%";  
}

function updateNet() {
  var taxRate = getTaxRate();
  var netValue = document.forms["new_product"].products_price_gross.value;

  if (taxRate > 0) {
    netValue = netValue / ((taxRate / 100) + 1);
  }

<?php
// Desactivation du module B2B
  if (MODE_B2B_B2C == 'true') {
    $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                            customers_group_name,
                                                            customers_group_discount
                                             from :table_customers_groups
                                             where customers_group_id != :customers_group_id
                                             order by customers_group_id
                                            ');
    $QcustomersGroup->bindInt(':customers_group_id', 0);
    $QcustomersGroup->execute();

    while ($customers_group = $QcustomersGroup->fetch() ) {
?>
      var netValue<?php echo $customers_group['customers_group_id'] ?> = document.forms["new_product"].price_gross<?php echo $customers_group['customers_group_id'] ?>.value;

      if (taxRate > 0) {
        netValue<?php echo $customers_group['customers_group_id'] ?> = netValue<?php echo $customers_group['customers_group_id'] ?> / ((taxRate / 100) + 1);
      }

      document.forms["new_product"].price<?php echo $customers_group['customers_group_id'] ?>.value = doRound(netValue<?php echo $customers_group['customers_group_id'] ?>, 4);
<?php
    }
  }
?>

  document.forms["new_product"].products_price.value = doRound(netValue, 4);
}
//--></script>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <?php echo osc_draw_form('new_product', 'categories.php', 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <div>
          <div class="adminTitle">
            <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/produit_editer.gif', HEADING_TITLE, '40', '40'); ?></span>
            <span class="col-md-7 pageHeading"><?php echo '&nbsp;' . sprintf(TEXT_NEW_PRODUCT, osc_output_generated_category_path($current_category_id)); ?></span>
            <span><?php echo osc_draw_separator('pixel_trans.gif', '2', '1'); ?></span>
            <span  class="pull-right"><?php echo osc_draw_hidden_field('products_date_added', (osc_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d'))) . osc_image_submit('button_update.gif', IMAGE_UPDATE); ?>&nbsp;</span>
            <span class="pull-right" style="padding-left:5px;"><?php echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '')) . '">' . osc_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?>&nbsp;</span>
          </div>
          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
        </div>
        <div class="clearfix"></div>
      </tr>
      <tr>
        <td>
         <?php echo osc_draw_separator('pixel_trans.gif', '2', '10'); ?>
          <div>
            <ul class="nav nav-tabs" role="tablist"  id="myTab">
              <li class="active"><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab1" role="tab" data-toggle="tab">' . TAB_GENERAL . '</a>'; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab2" role="tab" data-toggle="tab">' . TAB_STOCK; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab3" role="tab" data-toggle="tab">' . TAB_PRICE; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab4" role="tab" data-toggle="tab">' . TAB_DESC; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab5" role="tab" data-toggle="tab">' . TAB_IMG; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab6" role="tab" data-toggle="tab">' . TAB_REF; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab7" role="tab" data-toggle="tab">' . TAB_OPTIONS_FIELDS; ?></a></li>
              <li><?php echo '<a href="' . substr(osc_href_link('categories.php', osc_get_all_get_params()), strlen($base_url)) . '#tab8" role="tab" data-toggle="tab">' . TAB_OTHER_OPTIONS; ?></a></li>
            </ul>
            <div class="tabsClicShopping">
              <div class="tab-content">
<?php
// ---------------------------------------------------------------//-->
//          ONGLET General sur les informations produits          //-->
// -------------------------------------------------------------- //-->
?>
                <div class="tab-pane active" id="tab1">
                  <div class="col-md-12 mainTitle">
                    <div class="pull-left"><?php echo TEXT_PRODUCTS_NAME; ?></div>
                    <div class="pull-right"><?php echo TEXT_USER_NAME . osc_user_admin($user_administrator); ?></div>
                  </div>
                  <div class="adminformTitle">
                    <div class="spaceRow"></div>
                    <div class="row">
  <?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
  ?>
                        <div class="col-md-12">
                          <div  class="col-md-9">
                            <span class="col-md-1 centerInputFields"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                            <span class="col-md-3 pull-left centerInputFields" style="height:50px;"><?php echo  osc_draw_input_field('products_name[' . $languages[$i]['id'] . ']', (isset($products_name[$languages[$i]['id']]) ? $products_name[$languages[$i]['id']] : osc_get_products_name($pInfo->products_id, $languages[$i]['id'])), 'class="form-control" required aria-required="true" required="" id="products_name" placeholder="' . TEXT_PRODUCTS_NAME . '"',  true) . '&nbsp;'; ?></span>
                          </div>
                        </div>

  <?php
    }
  ?>
                    </div>
                  </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="col-md-12 mainTitle"><?php echo TEXT_PRODUCTS_OTHER_INFORMATION; ?></div>
                  <div class="adminformTitle">
<?php
// ******************************************
// Manufacturers
//******************************************
    $Qmanufacturers = $OSCOM_PDO->prepare("select manufacturers_id,
                                                   manufacturers_name
                                             from :table_manufacturers
                                             where manufacturers_id = :manufacturers_id
                                          ");
    $Qmanufacturers->bindInt(':manufacturers_id', $pInfo->manufacturers_id);
    $Qmanufacturers->execute();

    $products_manufacturers = $Qmanufacturers->fetch();


// ******************************************
// Suppliers
//*******************************************
    $Qsuppliers = $OSCOM_PDO->prepare("select suppliers_id,
                                              suppliers_name
                                       from :table_suppliers
                                       where suppliers_id = :suppliers_id
                                     ");
    $Qsuppliers->bindInt(':suppliers_id', $pInfo->suppliers_id);
    $Qsuppliers->execute();

    $products_suppliers = $Qsuppliers->fetch();


?>
<!-- ******************************************
// Manufacturers
******************************************* -->
<script type="text/javascript">
$(document).ready(function() {
  $("#manufacturer").tokenInput("manufacturers_ajax.php" ,
    {
      tokenLimit: 1,
      resultsLimit: 5,
      onResult: function (results) {
        $.each(results, function (index, value) {
          value.name = value.id + " " + value.name;
        });
        return results;
      }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $("#manufacturer").tokenInput("manufacturers_ajax.php", {
    prePopulate: [
      {id: <?php echo $products_manufacturers['manufacturers_id']; ?>,
        name: "<?php echo $products_manufacturers['manufacturers_id'] . ' ' . $products_manufacturers['manufacturers_name']; ?> "
      }
    ],
    tokenLimit: 1
  });
});
</script>

<style>.modal-dialog {width: 900px!important;} </style>


                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_MANUFACTURER; ?></span>
                           <span class="col-md-1 centerInputFields" style="padding-bottom:50px;"><?php echo  osc_draw_input_field('manufacturers_id', $products_manufacturers['manufacturers_id'] . ' ' . $products_manufacturers['manufacturers_name'], ' id="manufacturer" class="token-input"');?>
                             <a data-toggle="modal"data-refresh="true"  href="<?php echo 'manufacturers_popup.php';?>" data-target="#myModal"><?php echo osc_image (DIR_WS_ICONS . 'create.gif', TEXT_CREATE) ?></a>
                             <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                 <div class="modal-content">
                                   <div class="modal-body"><div class="te"></div></div>
                                 </div> <!-- /.modal-content -->
                               </div><!-- /.modal-dialog -->
                             </div><!-- /.modal -->
                           </span>

 <!-- ******************************************
 // Suppliers
 ******************************************* -->
 <script type="text/javascript">
   $(document).ready(function() {
     $("#suppliers").tokenInput("suppliers_ajax.php" ,
       {
         tokenLimit: 1,
         resultsLimit: 5,
         onResult: function (results) {
           $.each(results, function (index, value) {
             value.name = value.id + " " + value.name;
           });
           return results;
         }

       });
   });
 </script>
 <script type="text/javascript">
   $(document).ready(function() {
     $("#suppliers").tokenInput("suppliers_ajax.php", {
       prePopulate: [
         {id: <?php echo $products_suppliers['suppliers_id']; ?>,
           name: "<?php echo $products_suppliers['suppliers_id'] . ' ' . $products_suppliers['suppliers_name']; ?> "
         }
       ],
       tokenLimit: 1
     });
   });
 </script>
                           <span class="col-md-2"></span>
                           <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_SUPPLIERS; ?></span>
                           <span class="col-md-2 centerInputFields"><?php  echo osc_draw_input_field('suppliers_id', $products_suppliers['suppliers_id'] . ' ' . $products_suppliers['suppliers_name'], 'id="suppliers" size="20" maxlength="40"'); ?>
                            <a data-toggle="modal" data-refresh="true"  href="<?php echo 'suppliers_popup.php';?>" data-target="#myModal1"><?php echo osc_image (DIR_WS_ICONS . 'create.gif', TEXT_CREATE) ?></a>
                            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-body"><div class="te"></div></div>
                                </div> <!-- /.modal-content -->
                              </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                           </span>
                         </div>
                       </div>
                       <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '15'); ?></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_MODEL; ?></span>
                           <span class="col-md-2 centerInputFields"><?php echo osc_draw_input_field('products_model', $pInfo->products_model); ?></span>
                           <span class="col-md-1 centerInputFields">
                             <a data-toggle="modal" data-refresh="true"  href="configuration_popup_fields.php?<?php echo 'cKey=CONFIGURATION_PREFIX_MODEL'; ?>" data-target="#myModal_configuration_prefix"><?php echo osc_image (DIR_WS_ICONS . 'edit.gif', TEXT_EDIT_DEFAULT_CONFIGURATION) ?></a>
                             <div class="modal fade" id="myModal_configuration_prefix" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                 <div class="modal-content">
                                   <div class="modal-body"><div class="te"></div></div>
                                 </div> <!-- /.modal-content -->
                               </div><!-- /.modal-dialog -->
                             </div><!-- /.modal -->
                           </span>
                           <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_EAN; ?></span>
                           <span class="col-md-2 centerInputFields"><?php echo osc_draw_input_field('products_ean', $pInfo->products_ean); ?></span>
                           <span class="col-md-2 centerInputFields">
                             <a data-toggle="modal" data-refresh="true"  href="configuration_popup_fields.php?<?php echo 'cKey=BAR_CODE_TYPE'; ?>" data-target="#myModal_configuration_ean"><?php echo osc_image (DIR_WS_ICONS . 'edit.gif', TEXT_EDIT_DEFAULT_CONFIGURATION) ?></a>
                             <div class="modal fade" id="myModal_configuration_ean" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                 <div class="modal-content">
                                   <div class="modal-body"><div class="te"></div></div>
                                 </div> <!-- /.modal-content -->
                               </div><!-- /.modal-dialog -->
                             </div><!-- /.modal -->
<br />
<?php
    if (!empty($pInfo->products_ean)) {
      echo osc_image(DIR_WS_CATALOG_IMAGES . 'barcode/' . $pInfo->products_model .'_'. $pInfo->products_ean .'_barcode.png');
    }
?>
                           </span>
                         </div>
                       </div>
 <!-- conversion poids -->
 <script type="text/javascript">
   <!--
   function FormatWeight(value,pow) {
     var fact = Math.pow(10,pow);
     var result = Math.round(value*fact)/fact;
     result += (result<0?-1:1)/Math.pow(10,pow+2);
     result = result.toString();
     var pos = result.indexOf('.');
     if (pos>=0) { result = result.substr(0,pos+pow+1).replace(/0+$/,'').replace(/\.$/,''); }
     return result;
   }

   function calc_poids(from,value) {
     var x = value.replace(/,/,'.');
     var ok = x.match(/^-?\d+(\.\d*)?$/);
     var g;
     if (ok) {
       switch (from) {
         case 'products_weight': g = 1000*x; break;
         case 'g': g = x; break;
         case 'products_weight_pounds': g = 453.59237*x; break;
         case 'oz': g =28.3495*x; break;
       }
     }

     if (from != 'products_weight') { document.new_product.products_weight.value = ok ? FormatWeight(g/1000,2) : ''; }
     //if (from != 'g') { document.new_product.g.value = ok ? FormatWeight(g,2) : ''; }
     if (from != 'products_weight_pounds') { document.new_product.products_weight_pounds.value = ok ? FormatWeight(g/453.59237,2) : ''; }
     //if (from != 'oz') { document.new_product.oz.value = ok ? FormatWeight(g/28.3495,2) : ''; }
     return true;
   }
   //-->
 </script>

                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_WEIGHT; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_weight', $pInfo->products_weight, 'onKeyUp="return calc_poids(\'products_weight\',value)" size="12"'); ?></span>
                           <span class="col-md-2"></span>
                           <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_SKU; ?></span>
                           <span class="col-md-2 centerInputFields"><?php echo osc_draw_input_field('products_sku', $pInfo->products_sku); ?></span>
                         </div>
                       </div>
                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_WEIGHT_POUND; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_weight_pounds', $pInfo->products_weight_pounds, 'onKeyUp="return calc_poids(\'products_weight_pounds\',value)" size="12"'); ?></span>
                         </div>
                       </div>
                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_VOLUME; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_volume', $pInfo->products_volume); ?></span>
                           <span class="col-md-2"></span>
                           <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_DIMENSION; ?></span>
                           <span class="col-md-1 pull-left"><?php echo osc_draw_input_field('products_dimension_width', $pInfo->products_dimension_width, 'class="input-small"') . ' ' . osc_draw_input_field('products_dimension_height', $pInfo->products_dimension_height, 'class="input-small"') . ' ' . osc_draw_input_field('products_dimension_depth', $pInfo->products_dimension_depth, 'class="input-small"'); ?><span>
                         </div>
                       </div>
                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_PRICE_COMPARISON; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_radio_field('products_price_comparison', '1', $in_comparison) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('products_price_comparison', '0', $out_comparison) . '&nbsp;' . TEXT_NO; ?></span>
                           <span class="col-md-2"></span>
                           <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_DIMENSION_TYPE; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_dimension_type', $pInfo->products_dimension_type, 'class="input-small"'); ?></span>
                         </div>
                       </div>
                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_ONLY_ONLINE; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_checkbox_field('products_only_online', '1', $pInfo->products_only_online); ?></span>
                           <span class="col-md-2"></span>
<?php
  $products_packaging_array = array(array('id' => '0', 'text' => TEXT_CHOOSE),
                                    array('id' => '1', 'text' => TEXT_PRODUCTS_PACKAGING_NEW),
                                    array('id' => '2', 'text' => TEXT_PRODUCTS_PACKAGING_REPACKAGED),
                                    array('id' => '3', 'text' => TEXT_PRODUCTS_PACKAGING_USED)
                                   );
?>
                           <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_WHAREHOUSE_PACKAGING; ?></span>
                           <span class="col-md-3 centerInputFields";"><?php echo  osc_draw_pull_down_menu('products_packaging', $products_packaging_array,  $pInfo->products_packaging); ?></span>
                         </div>
                       </div>
                       <div class="spaceRow"></div>
                       <div class="row">
                         <div class="col-md-12">
                           <span class="col-md-3 centerInputFields"><?php echo TEXT_PRODUCTS_ONLY_SHOP; ?></span>
                           <span class="col-md-1 centerInputFields"><?php echo osc_draw_checkbox_field('products_only_shop', '1', $pInfo->products_only_shop); ?></span>
                           <span class="col-md-2"></span>
                         </div>
                       </div>
                       <div class="spaceRow"></div>
                       <div class="row">
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                         <div class="col-md-12">
                           <div  class="col-md-3 centerInputFields"><?php if ($i == 0) echo TEXT_PRODUCTS_URL . '<br /><small>' . TEXT_PRODUCTS_URL_WITHOUT_HTTP . '</small>'; ?></div>
                           <div  class="col-md-9">
                             <span class="col-md-1 centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                             <span class="col-md-3 pull-left" style="height:50px"><?php echo osc_draw_input_field('products_url[' . $languages[$i]['id'] . ']', (isset($products_url[$languages[$i]['id']]) ? $products_url[$languages[$i]['id']] : osc_get_products_url($pInfo->products_id, $languages[$i]['id']))) . '&nbsp;'; ?></span>
                           </div>
                         </div>

<?php
    }
?>
                       </div>
                     </div>
                   <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                   <div class="col-md-12 mainTitle"><?php echo TEXT_PRODUCTS_OTHER_INFORMATION; ?></div>
                   <div class="adminformTitle">
                     <div class="col-md-12" style="padding-top:10px; padding-bottom:10px;">
                     <span class="col-md-4"><?php echo TEXT_PRODUCTS_SHIPPING_DELAY; ?></span>
                     <span>
                       <a data-toggle="modal" data-refresh="true"  href="configuration_popup_fields.php?<?php echo 'cKey=DISPLAY_SHIPPING_DELAY'; ?>" data-target="#myModal_configuration_shipping"><?php echo osc_image (DIR_WS_ICONS . 'edit.gif', TEXT_EDIT_DEFAULT_CONFIGURATION) ?></a>
                       <div class="modal fade" id="myModal_configuration_shipping" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                         <div class="modal-dialog">
                           <div class="modal-content">
                             <div class="modal-body"><div class="te"></div></div>
                           </div> <!-- /.modal-content -->
                         </div><!-- /.modal-dialog -->
                       </div><!-- /.modal -->
                     </span>
                   </div>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                      <div class="row">
                        <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                        <span  class="col-md-10 main"><?php echo osc_draw_input_field('products_shipping_delay[' . $languages[$i]['id'] . ']', (isset($products_shipping_delay[$languages[$i]['id']]) ? $products_shipping_delay[$languages[$i]['id']] : osc_get_products_shipping_delay($pInfo->products_id, $languages[$i]['id'])), 'size="90"') . '&nbsp;'; ?></span>
                       </div>
<?php
    }
?>
                     <div  class="row">
                       <span class="col-md-1 pull-left centerInputFields"><?php echo TEXT_PRODUCTS_SORT_ORDER; ?>&nbsp;&nbsp;</span>
                       <span class="col-md-2"><?php echo osc_draw_input_field('products_sort_order', $pInfo->products_sort_order, 'size="5"'); ?></span>
                     </div>
                   </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="adminformAide">
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_GENERAL); ?>
                        <strong><?php echo '&nbsp;' . TITLE_HELP_GENERAL; ?></strong>
                      </span>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <span class="col-md-12"><?php echo HELP_GENERAL; ?></span>
                    </div>
                  </div>



                </div>
<?php
  //  ----------------------------- //-->
  //          ONGLET Stock          //-->
  // ----------------------------- //-->
?>
                <div class="tab-pane" id="tab2">
                  <div class="col-md-12 mainTitle">
                    <span><?php echo TEXT_PRODUCTS_STOCK; ?></span>
                  </div>
                  <div class="adminformTitle">
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_STATUS; ?></span>
                        <span class="col-md-2 centerInputFields"><?php echo  osc_draw_radio_field('products_status', '1', $in_status) . '&nbsp;' . TEXT_PRODUCT_AVAILABLE . '&nbsp;' . osc_draw_radio_field('products_status', '0', $out_status) . '&nbsp;' . TEXT_PRODUCT_NOT_AVAILABLE;; ?></span>
                        <span class="col-md-1"></span>
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_QUANTITY_UNIT; ?></span>
                        <span class="col-md-1 centerInputFields"><?php echo osc_draw_pull_down_menu('products_quantity_unit_id', $products_quantity_unit_array, $pInfo->products_quantity_unit_id); ?><span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_STOCK; ?></span>
                        <span class="col-md-1"><?php echo osc_draw_input_field('products_quantity', $pInfo->products_quantity); ?></span>
                        <span class="col-md-2"></span>
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_MIN_ORDER_QUANTITY; ?></span>
                        <span class="col-md-1"><?php echo osc_draw_input_field('products_min_qty_order', $pInfo->products_min_qty_order); ?></span>
                         <span class="col-md-1">
                           <a data-toggle="modal" data-refresh="true"  href="configuration_popup_fields.php?<?php echo 'cKey=MAX_MIN_IN_CART'; ?>" data-target="#myModal_configuration_minqty"><?php echo osc_image (DIR_WS_ICONS . 'edit.gif', TEXT_EDIT_DEFAULT_CONFIGURATION) ?></a>
                           <div class="modal fade" id="myModal_configuration_minqty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-body"><div class="te"></div></div>
                               </div> <!-- /.modal-content -->
                             </div><!-- /.modal-dialog -->
                           </div><!-- /.modal -->
                         </span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_ALERT; ?></span>
                        <span class="col-md-1 centerInputFields"><?php echo  osc_draw_input_field('products_quantity_alert', $pInfo->products_quantity_alert); ?></span>
                         <span class="col-md-1 centerInputFields">
                           <a data-toggle="modal" data-refresh="true"  href="configuration_popup_fields.php?<?php echo 'cKey=STOCK_REORDER_LEVEL'; ?>" data-target="#myModal_configuration_alert"><?php echo osc_image (DIR_WS_ICONS . 'edit.gif', TEXT_EDIT_DEFAULT_CONFIGURATION) ?></a>
                           <div class="modal fade" id="myModal_configuration_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-body"><div class="te"></div></div>
                               </div> <!-- /.modal-content -->
                             </div><!-- /.modal-dialog -->
                           </div><!-- /.modal -->
                         </span>
<?php
    $stockable_array = array(array('id' =>'product', 'text' => TEXT_STOCKABLE_PRODUCT),
                            array('id' =>'consu', 'text' => TEXT_STOCKABLE_CONSUMABLE),
                            array('id' =>'service' , 'text' => TEXT_STOCKABLE_SERVICE),
                          );
?>
                        <span class="col-md-1"></span>
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCT_TYPE; ?></span>
                        <span class="col-md-1 centerInputFields"><?php echo osc_draw_pull_down_menu('products_type', $stockable_array, $pInfo->products_type); ?></span>
                      </div>
                    </div>
                    <div class="col-md-12"><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_DATE_AVAILABLE; ?><br /><small>(YYYY-MM-DD)</small></span>
                        <span class="col-md-1"><?php echo osc_draw_input_field('products_date_available', $pInfo->products_date_available, 'id="products_date_available"'); ?></span>

                        <span class="col-md-2"></span>
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_TIME_REPLENISHMENT; ?></span>
                        <span class="col-md-1 pull-left"><?php echo  osc_draw_input_field('products_wharehouse_time_replenishment', $pInfo->products_wharehouse_time_replenishment,'size="15"'); ?><span>
                      </div>
                    </div>
                  </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="col-md-12 mainTitle">
                    <span><?php echo TEXT_WHAREHOUSE; ?></span>
                  </div>
                  <div class="adminformTitle">
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_WHAREHOUSE; ?></span>
                        <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_wharehouse', $pInfo->products_wharehouse,'size="30"'); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_WHAREHOUSE_ROW; ?></span>
                        <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_wharehouse_row', $pInfo->products_wharehouse_row,'size="15"'); ?></span>
                      </div>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <div class="col-md-12">
                        <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_WHAREHOUSE_LEVEL_LOCATION; ?></span>
                        <span class="col-md-1 centerInputFields"><?php echo osc_draw_input_field('products_wharehouse_level_location', $pInfo->products_wharehouse_level_location,'size="15"'); ?></span>
                      </div>
                    </div>
                  </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="adminformAide">
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_GENERAL); ?>
                        <strong><?php echo '&nbsp;' . TITLE_HELP_GENERAL; ?></strong>
                      </span>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <span class="col-md-12"><?php echo HELP_STOCK; ?></span>
                    </div>
                  </div>
                </div>
<?php
  //  ----------------------------- //-->
  //          ONGLET Prix          //-->
  // ----------------------------- //-->
?>
                <div class="tab-pane" id="tab3">
                  <div class="col-md-12 mainTitle">
                    <span><?php echo TEXT_PRODUCTS_PRICE_PUBLIC; ?></span>
                  </div>
                  <div class="adminformTitle" style="height:100%; padding-left:0px; padding-right:0px; padding-bottom:0px; padding-top:0px;">
                    <div style="background-color:#ebebff; height:100%;">
                      <div class="spaceRow"></div>
                      <div class="col-md-12" style="padding-top:10px; padding-bottom:10px;">
                        <span class="col-md-2"><?php echo TEXT_PRODUCTS_TAX_CLASS; ?></span>
                        <span class="col-md-2"><?php echo osc_draw_pull_down_menu('products_tax_class_id', $tax_class_array, $pInfo->products_tax_class_id, 'onchange="updateGross()"'); ?></span>
                      </div>

                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2"><?php echo TEXT_PRODUCTS_PRICE; ?></span>
<?php
  if (DISPLAY_DOUBLE_TAXE == 'false') {
    echo '<span class="col-md-1 pull-left">'. osc_draw_input_field('products_price', $pInfo->products_price, 'onkeyup="updateGross()" size="12"') . '<strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong></span>';
    echo '<span class="col-md-1 pull-left">'.  osc_draw_input_field('products_price_gross', $pInfo->products_price, 'onkeyup="updateNet()" size="12"') . '<strong>' . TEXT_PRODUCTS_PRICE_GROSS . '</strong></span>';
  } else {
    echo '<span class="col-md-1 pull-left">' . osc_draw_input_field('products_price', $pInfo->products_price, 'onkeyup="updateGross()" size="12"') . '<strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong></span>';
  }
?>
                          </span>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_COST; ?></span>
                          <span class="col-md-2"><?php echo osc_draw_input_field('products_cost', $pInfo->products_cost, 'onkeyUp="updateMargin()" size="12"') . '<strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong>';  ?></span>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_HANDLING; ?></span>
                          <span class="col-md-2"><?php echo osc_draw_input_field('products_handling', $pInfo->products_handling, 'onkeyUp="updateMargin()" size="12"') . '<strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong>'; ?></span>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_PRICE_MARGINS; ?></span>
                          <div><span id='products_price_margins'></div>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo PRODUCTS_VIEW; ?></span>
<?php
  if (isset($_GET['pID'])) {
?>
                          <span class="col-md-2"><?php echo osc_draw_checkbox_field('products_view', '1', $pInfo->products_view) . osc_image (DIR_WS_IMAGES . 'icons/last.png', TAB_PRODUCTS_VIEW) . '&nbsp;&nbsp;' . osc_draw_checkbox_field('orders_view', '1', $pInfo->orders_view) . osc_image (DIR_WS_IMAGES . 'icons/orders-up.gif', TAB_ORDERS_VIEW); ?>&nbsp;</span>
<?php
  } else {
?>
                         <span class="col-md-2"><?php echo osc_draw_checkbox_field('products_view', '1', true) . osc_image (DIR_WS_IMAGES . 'icons/last.png', TAB_PRODUCTS_VIEW) . '&nbsp;&nbsp;' . osc_draw_checkbox_field('orders_view', '1', true) . osc_image (DIR_WS_IMAGES . 'icons/orders-up.gif', TAB_ORDERS_VIEW); ?>&nbsp;</span>
<?php
  }
?>
                        </div>
                      </div>
                      <div class="spaceRow"></div>
                      <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_PRICE_KILO; ?></span>
                          <span class="col-md-2"><?php echo osc_draw_checkbox_field('products_price_kilo', '1', $pInfo->products_price_kilo); ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
<?php
// Activation du module B2B
  if (MODE_B2B_B2C == 'true') {
    $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                            customers_group_name,
                                                            customers_group_discount
                                             from :table_customers_groups
                                             where customers_group_id != :customers_group_id
                                             order by customers_group_id
                                            ');
    $QcustomersGroup->bindInt(':customers_group_id', 0);
    $QcustomersGroup->execute();

    $header = false;

    while ($customers_group = $QcustomersGroup->fetch() ) {
      if (!$header) {
        $header = true;
?>

                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="col-md-12 mainTitle" style="height:30px;">
                    <?php echo TEXT_CUST_GROUPS . '&nbsp;&nbsp;&nbsp&nbsp;' . osc_draw_radio_field('products_percentage', '1', $in_percent) . '&nbsp;' . TEXT_OVERRIDE_ON . '&nbsp;&nbsp;&nbsp;' . osc_draw_radio_field('products_percentage', '0', $out_percent). '&nbsp;' . TEXT_OVERRIDE_OFF; ?>
                  </div>

                  <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                     <tr>
                       <td><table border="0" cellpadding="2" cellspacing="2">
                         <tr>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td>
<?php
       echo osc_draw_separator('pixel_trans.gif', '14', '15');
       echo osc_image (DIR_WS_IMAGES . 'icons/euro.png', TAB_PRICE_GROUP_VIEW);
       echo osc_draw_separator('pixel_trans.gif', '30', '15');
       echo osc_image (DIR_WS_IMAGES . 'icons/last.png', TAB_PRODUCTS_GROUP_VIEW);
       echo osc_draw_separator('pixel_trans.gif', '25', '15');
       echo osc_image (DIR_WS_IMAGES . 'icons/orders-up.gif', TAB_ORDERS_GROUP_VIEW);
       echo osc_draw_separator('pixel_trans.gif', '60', '15');
       echo TEXT_PRODUCTS_MODEL_GROUP;
       echo osc_draw_separator('pixel_trans.gif', '70', '15');
       echo TEXT_PRODUCTS_QUANTITY_FIXED_GROUP;
       echo osc_draw_separator('pixel_trans.gif', '70', '15');
       echo TEXT_PRODUCTS_MIN_ORDER_QUANTITY_GROUP;
?>
                           </td>
                         </tr>

<!-- Prix TTC + autorisation affichage prix public et produit + autorisation commande //-->
<?php
      }

      if ($QcustomersGroup->rowCount() > 0) {
        $Qattributes = $OSCOM_PDO->prepare('select g.customers_group_id,
                                                 g.customers_group_price,
                                                 g.price_group_view,
                                                 g.products_group_view,
                                                 g.orders_group_view,
                                                 p.products_price,
                                                 p.products_id,
                                                 g.products_quantity_unit_id_group,
                                                 g.products_model_group,
                                                 g.products_quantity_fixed_group
                                          from :table_products_groups g,
                                               :table_products p
                                          where p.products_id = :products_id
                                          and p.products_id = g.products_id
                                          and g.customers_group_id = :customers_group_id
                                          order by g.customers_group_id
                                            ');
        $Qattributes->bindInt(':products_id', $pInfo->products_id );
        $Qattributes->bindInt(':customers_group_id', (int)$customers_group['customers_group_id']);
        $Qattributes->execute();

      } else {
        $attributes = array('customers_group_id' => 'new');
      }
?>
                        <tr>
                          <td class="main"><?php echo $customers_group['customers_group_name']; ?>&nbsp;:&nbsp;</td>
                          <td class="dataTableContent">
                            <span class="col-md-2">
<?php
      if ($attributes = $Qattributes->fetch() ) {
      echo osc_draw_input_field('price' . $customers_group['customers_group_id'], $attributes['customers_group_price'], 'onchange="updateGross()" class="input-small"') .'<strong>' . TAX_EXCLUDED . '</strong>';
    } else {
      echo osc_draw_input_field('price' . $customers_group['customers_group_id'], '0', 'onchange="updateGross()" class="input-small"') .'<strong>' . TAX_EXCLUDED . '</strong>';
// Permet de cocher par defaut la case Afficher Prix Public, Afficher Produit et Autoriser commande
      $attributes['price_group_view'] = '1';
      $attributes['products_group_view'] = '1';
      $attributes['orders_group_view'] = '1';
      $attributes['products_quantity_unit_id_group'] = '0';
      $attributes['products_model_group'] = '';
      $attributes['products_quantity_fixed_group'] = '1';
    }
?>
                             </span>
                          </td>
                          <td class="dataTableContent">
                           <span class="col-md-2">
<?php
  if (DISPLAY_DOUBLE_TAXE == 'false') {
    echo  osc_draw_input_field('price_gross' . $customers_group['customers_group_id'], $attributes['customers_group_price'], 'onkeyUp="updateNet()" class="input-small"') .'<strong>' . TAX_INCLUDED . '</strong>';
  }
?>
                            </span>
                          </td>
<!-- Autorisation affichage prix public et produit + autorisation commande //-->
                          <td class="dataTableContent">

<?php
      echo osc_draw_separator('pixel_trans.gif', '15', '15') . osc_draw_checkbox_field('price_group_view' . $customers_group['customers_group_id'], '1', $attributes['price_group_view']) . osc_draw_separator('pixel_trans.gif', '30', '15') . osc_draw_checkbox_field('products_group_view' . $customers_group['customers_group_id'], '1', $attributes['products_group_view']) . osc_draw_separator('pixel_trans.gif', '30', '15') . osc_draw_checkbox_field('orders_group_view' . $customers_group['customers_group_id'], '1', $attributes['orders_group_view']);
      echo osc_draw_separator('pixel_trans.gif', '25', '15');
      echo osc_draw_input_field('products_model_group' . $customers_group['customers_group_id'], $attributes['products_model_group'], 'class="input-small"');

      echo osc_draw_separator('pixel_trans.gif', '25', '15');
      echo osc_draw_input_field('products_quantity_fixed_group' . $customers_group['customers_group_id'], $attributes['products_quantity_fixed_group'], 'class="input-small"');
      echo osc_draw_separator('pixel_trans.gif', '25', '15');
      echo osc_draw_pull_down_menu('products_quantity_unit_id_group' . $customers_group['customers_group_id'], $products_quantity_unit_array, $attributes['products_quantity_unit_id_group']);
?>
                          </td>
                        </tr>
<?php
    }
    if ($header) {
?>
                      </table></td>
<?php
    }

?>

                    </tr>
                  </table>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="adminformAide">
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_AIDE_PRICE); ?>
                        <strong><?php echo '&nbsp;' . TITLE_AIDE_PRICE; ?></strong>
                      </span>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/euro.png', TITLE_AIDE_PRICE); ?>
                        <?php echo '&nbsp;&nbsp;' . HELP_PRICE_GROUP_VIEW . '<strong>*</strong>'; ?>
                      </span>
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/last.png', TAB_PRODUCTS_GROUP_VIEW); ?>
                        <?php echo '&nbsp;&nbsp;' . HELP_PRODUCTS_VIEW; ?>
                      </span>
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/orders-up.gif', TAB_ORDERS_GROUP_VIEW); ?>
                        <?php echo '&nbsp;&nbsp;' . HELP_ORDERS_VIEW; ?>
                      </span>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_draw_separator('pixel_trans.gif', '16', '1');; ?>
                        <strong><?php echo '&nbsp;' . HELP_OTHERS_GROUP; ?></strong>
                      </span>
                    </div>
                  </div>

<?php
  }
?>
              </div>
<script type="text/javascript"><!--
updateGross();
//--></script>
<?php
// ------------------------------------ //-->
//          ONGLET Description          //-->
// ------------------------------------ //-->
?>
              <div class="tab-pane" id="tab4">
                <div class="col-md-12 mainTitle">
                  <span><?php echo TEXT_PRODUCTS_DESCRIPTION; ?></span>
                </div>
                <div class="adminformTitle">
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div>
<script type='text/javascript' src='ext/ckeditor/ckeditor.js'></script>
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                  <div class="row">
                    <span class="col-md-2"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                    <span class="col-md-10">
                      <div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor('products_description[' . $languages[$i]['id'] . ']', 'soft', '750', '300', (isset($products_description[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($products_description[$languages[$i]['id']])) : osc_get_products_description($pInfo->products_id, $languages[$i]['id']))); ?></div>
                    </span>
                  </div>
                  <div class="row"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div>
                    <div>
                      <span class="col-md-12"><?php echo TEXT_PRODUCTS_DESCRIPTION_SUMMARY; ?></span>
                    </div>
                    <div class="row">
                      <span class="col-md-2"><?php echo osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                      <span class="col-md-7">
                        <?php echo osc_draw_textarea_field('products_description_summary[' . $languages[$i]['id'] . ']', 'soft', '2', '3', (isset($products_description_summary[$languages[$i]['id']]) ? str_replace('& ', '&amp; ', trim($products_description_summary[$languages[$i]['id']])) : osc_get_products_description_summary($pInfo->products_id, $languages[$i]['id']))); ?>
                      </span>
                    </div>
                  </div>
                  <div class="row"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<?php
    }
?>
                  </div>
                  <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                  <div class="adminformAide">
                    <div class="row">
                      <span class="col-md-12">
                        <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', HELP_DESCRIPTION); ?>
                        <strong><?php echo '&nbsp;' . HELP_DESCRIPTION; ?></strong>
                      </span>
                    </div>
                    <div class="spaceRow"></div>
                    <div class="row">
                      <span class="col-md-12">
                        <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                        <div class="modal fade" id="myModalWysiwyg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                              </div>
                              <div class="modal-body" style="text-align:center;">
                                <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                              </div>
                            </div>
                          </div>
                        </div>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
<?php
// ------------------------------- //-->
//          ONGLET Images          //-->
// ------------------------------- //-->

// place allowed sub-dirs in array, non-recursive

    $dir_array = array();
    foreach (osc_opendir($root_images_dir) as $file) {
      if ($file['is_dir']) {
        $img_dir_products_image = substr($file['name'], strlen($root_images_dir));
        $drop_array[] = array('id' => $img_dir_products_image,
                              'text' => $img_dir_products_image
                             );
      }
    }
?>
              <div class="tab-pane" id="tab5">
                <div class="mainTitle">
                  <span><?php echo TEXT_PRODUCTS_IMAGE; ?></span>
                </div>
                <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                  <tr>
                    <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_PRODUCTS_IMAGE_VIGNETTE, '40', '40'); ?></td>
                            <td class="main" colspan="2"><?php echo TEXT_PRODUCTS_INSERT_BIG_IMAGE_VIGNETTE; ?></td>
                          </tr>
                          <tr>
                           <td colspan="5"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                          </tr>
                          <tr>
                            <td class="main" colspan="3"><?php echo TEXT_PRODUCTS_IMAGE_DIRECTORY; ?>
                            <td  colspan="2"><?php echo osc_draw_pull_down_menu('directory_products_image', $drop_array); ?><td>
                          </tr>
                          <tr>
                           <td colspan="5"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                          </tr>
                          <tr>
                            <td class="main" colspan="3"><?php echo TEXT_PRODUCTS_IMAGE_NEW_FOLDER; ?>
                            <td  colspan="2"><?php echo osc_draw_input_field('new_directory_products_image','','class="input-small"'); ?><td>
                          </tr>
                          <tr>
                           <td colspan="5"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                          </tr>
                          <tr>
                          <tr>
                            <td class="main" colspan="3"><?php echo TEXT_PRODUCTS_MAIN_IMAGE; ?>
                            <td colspan="2"><?php echo osc_draw_file_field('products_image_resize'); ?><td>
                          </tr>
                          <tr>
                           <td colspan="3"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                          </tr>
                        </table></td>
                        <td valign="top" width="50%"><table width="100%" border="0">
                          <tr>
                            <td><table>
                              <tr>
                                <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product_images.gif', TEXT_PRODUCTS_IMAGE_VISUEL, '40', '40'); ?></td>
                                <td class="main" align="left"><?php echo TEXT_PRODUCTS_IMAGE_VISUEL; ?></td>
                              </tr>
                            </table></td>
                          </tr>
                          <tr align="center">
                            <td width="100%" align="center" valign="top"><table width="100%" border="0" class="adminformAide">
                              <tr>
                                <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                              </tr>
                              <tr>
                                <td align="center"><?php echo osc_info_image($pInfo->products_image, TEXT_PRODUCTS_IMAGE_VIGNETTE); ?></td>
                              </tr>
<?php
  if ($pInfo->products_image_zoom != '') {
?>
                              <tr>
                                <td align="center"><table cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="20"><?php echo osc_image(DIR_WS_IMAGES . 'zoom.gif', HEADING_TITLE, '20', '20'); ?></td>
                                    <td class="main"><?php echo '<a href="javascript:popupImageWindow(\'' . 'popup_image.php' . '?products_image_zoom=' . (int)$_GET['pID'] . '\')">' . TEXT_PRODUCTS_IMAGE_VISUEL_ZOOM . '</a>'; ?></td>
                                 </table></td>
                               </tr>
<?php
  } else {
?>
                               <tr>
                                 <td align="center"><table>
                                   <tr>
                                     <td class="main"><?php echo TEXT_PRODUCTS_NO_IMAGE_VISUEL_ZOOM; ?></td>
                                   </tr>
                                 </table></td>
                               </tr>
<?php
  }
?>
                                <tr>
                                   <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                </tr>
                                <tr>
                                  <td class="main" align="right" colspan="2"><?php echo TEXT_PRODUCTS_DELETE_IMAGE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                </tr>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
<?php
// -----------------------------------
// Gallery
// -----------------------------------  
?>        
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo TEXT_PRODUCTS_GALLERY_IMAGE; ?></td>
                    </tr>
                  </table>
                  <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
<!-- eof image directory -->
                    <tr>

<script type="text/javascript" src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>

                      <td colspan="3">
                        <ul id="piList">
<?php 
// gallery


    echo TEXT_PRODUCTS_IMAGE_DIRECTORY. '&nbsp;' . osc_draw_pull_down_menu('directory', $drop_array) . '&nbsp;&nbsp;' . TEXT_PRODUCTS_IMAGE_NEW_FOLDER_GALLERY . osc_draw_input_field('new_directory','','class="input-small"') .'<br /><br />';

    $pi_counter = 0;

    foreach ($pInfo->products_larger_images as $pi) {
      $pi_counter++;

      echo '                <li id="piId' . $pi_counter . '" class="ui-state-default"><span style="float: right; padding-left:5px;"><i class="fa fa-arrow-circle-o-up fa-2x" style="float: right;"></i></span>
                             <a href="#" onclick="showPiDelConfirm(' . $pi_counter . ');return false;"><i class="fa fa-trash-o fa-2x" style="float: right;"></i></a>
                             <strong>' . TEXT_PRODUCTS_LARGE_IMAGE . '</strong><br />' . osc_draw_file_field('products_image_large_' . $pi['id']) . '<br />
                             <a href="' . DIR_WS_CATALOG_IMAGES . $pi['image'] . '" target="_blank">' . $pi['image'] . '</a><br /><br />' . TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT . '<br />' . osc_draw_textarea_field('products_image_htmlcontent_' . $pi['id'], 'soft', '70', '3', $pi['htmlcontent']) . '</li>';
    }
?>
                        </ul>
                          <a href="#" onclick="addNewPiForm();return false;"><span style="float: left; padding-right:5px;"><i class="fa fa-plus-square fa-2x"></i></span><?php echo TEXT_PRODUCTS_ADD_LARGE_IMAGE; ?></a>
                          <div id="piDelConfirm" title="<?php echo TEXT_DELETE_PRODUCTS_IMAGE; ?>">
                            <p style="padding-top:20px; padding-left:10px;"><span style="float:left; margin:0 7px 20px 0;"><i class="fa fa-exclamation-triangle fa-2x"></i> </span><?php echo TEXT_PRODUCTS_ADD_LARGE_IMAGE_DELETE; ?></p>
                          </div>

<style type="text/css">
#piList { list-style-type: none; margin: 0; padding: 0; }
#piList li { margin: 5px 0; padding: 2px; }
</style>

<script type="text/javascript">
$('#piList').sortable({
  containment: 'parent'
});

var piSize = <?php echo $pi_counter; ?>;

function addNewPiForm() {
  piSize++;
  $('#piList').append('<li id="piId' + piSize + '" class="ui-state-default"><i class="fa fa-arrow-circle-o-up fa-2x" style="float: right;  padding-left:5px;"></i><a href="#" onclick="showPiDelConfirm(' + piSize + ');return false;"  style="float: right;"><i class="fa fa-trash-o fa-2x"></i></a><strong><?php echo TEXT_PRODUCTS_LARGE_IMAGE; ?></strong><br /><input type="file" name="products_image_large_new_' + piSize + '" /><br /><br /><?php echo TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT; ?><br /><textarea name="products_image_htmlcontent_new_' + piSize + '" wrap="soft" cols="70" rows="3"></textarea></li>');
}

var piDelConfirmId = 0;

$('#piDelConfirm').dialog({
  autoOpen: false,
  resizable: false,
  draggable: false,
  modal: true,
  buttons: {
    'Delete': function() {
      $('#piId' + piDelConfirmId).effect('blind').remove();
      $(this).dialog('close');
    },
    Cancel: function() {
      $(this).dialog('close');
    }
  }
});

function showPiDelConfirm(piId) {
  piDelConfirmId = piId;

  $('#piDelConfirm').dialog('open');
}
</script>

                       </td>
                     </tr>
                     <tr>
                       <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                     </tr>
                   </table>
                   <table width="100%" border="0" cellspacing="0" cellpadding="5">
                     <tr>
                       <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                     </tr>
                   </table>
<?php
// -----------------------------------
// Manual image
// -----------------------------------  

  if (MANUAL_IMAGE_PRODUCTS_DESCRIPTION == 'true') {
?>        
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td class="mainTitle"><?php echo TEXT_PRODUCTS_IMAGE_CUSTOMIZE; ?></td>
                      </tr>
                    </table>
                    <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                      <tr>
                        <td><table width="100%" border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td valign="bottom"><table border="0" width="200" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product.gif', TEXT_PRODUCTS_IMAGE_VIGNETTE, '40', '40'); ?></td>
                                <td class="main"><?php echo TEXT_PRODUCTS_IMAGE_VIGNETTE; ?></td>
                              </tr>
                              <tr>
                                <td class="main" colspan="2"><?php echo osc_draw_file_field_image_ckeditor('products_image', '100', '100', ''); ?></td>
                              </tr>
                            </table></td>
                            <td valign="top"><table border="0" width="200" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product_zoom.gif', TEXT_PRODUCTS_IMAGE_MEDIUM, '40', '40'); ?></td>
                                <td class="main"><?php echo TEXT_PRODUCTS_IMAGE_MEDIUM; ?></td>
                              </tr>
                              <tr>
                                <td class="main" colspan="2"><?php echo osc_draw_file_field_image_ckeditor('products_image_medium', '100', '100', ''); ?></td>
                              </tr>
                            </table></td>
                            <td valign="top"><table border="0" width="200" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product_zoom.gif', TEXT_PRODUCTS_IMAGE_ZOOM, '40', '40'); ?></td>
                                <td class="main"><?php echo TEXT_PRODUCTS_IMAGE_ZOOM; ?></td>
                              </tr>
                              <tr>
                                <td class="main" colspan="2"><?php echo osc_draw_file_field_image_ckeditor('products_image_zoom', '100', '00', ''); ?></td>
                              </tr>
                            </table></td>
                            <td valign="top" width="50%"><table width="100%" border="0" cellpadding="0" cellspaing="0">
                              <tr>
                                <td><table>
                                  <tr>
                                    <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'images_product_images.gif', TEXT_PRODUCTS_IMAGE_VISUEL, '40', '40'); ?></td>
                                    <td class="main" align="left"><?php echo TEXT_PRODUCTS_IMAGE_VISUEL; ?></td>
                                  </tr>
                                </table></td>
                              </tr>
                              <tr align="center">
                                <td width="100%" align="center" valign="top"><table width="100%" border="0" class="adminformAide">
                                  <tr>
                                    <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                  </tr>
                                  <tr>
                                    <td align="center"><?php echo osc_info_image($pInfo->products_image, TEXT_PRODUCTS_IMAGE_VIGNETTE); ?></td>
                                  </tr>
<?php
  if ($pInfo->products_image_zoom != '') {
?>
                                  <tr>
                                    <td align="center"><table cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td width="20"><?php echo osc_image(DIR_WS_IMAGES . 'zoom.gif', HEADING_TITLE, '20', '20'); ?></td>
                                        <td class="main"><?php echo '<a href="javascript:popupImageWindow(\'' . 'popup_image.php' . '?products_image_zoom=' . (int)$_GET['pID'] . '\')">' . TEXT_PRODUCTS_IMAGE_VISUEL_ZOOM . '</a>'; ?></td>
                                     </table></td>
                                   </tr>
<?php
  } else {
?>
                                   <tr>
                                     <td align="center"><table>
                                       <tr>
                                         <td class="main"><?php echo TEXT_PRODUCTS_NO_IMAGE_VISUEL_ZOOM; ?></td>
                                       </tr>
                                     </table></td>
                                   </tr>
<?php
  }
?>
                                    <tr>
                                       <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                                    </tr>
                                    <tr>
                                      <td class="main" align="right" colspan="2"><?php echo TEXT_PRODUCTS_DELETE_IMAGE . osc_draw_checkbox_field('delete_image', 'yes', false); ?></td>
                                    </tr>
                                  </table></td>
                                </tr>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
<?php
  } // en manual_image_products_description
?>
                      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                      <div class="adminformAide">
                        <div class="row">
                          <span class="col-md-12">
                            <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_OPTIONS); ?>
                            <strong><?php echo '&nbsp;' . TITLE_HELP_OPTIONS; ?></strong>
                          </span>
                        </div>
                        <div class="spaceRow"></div>
                        <div class="row">
                          <span class="col-md-12">
                            <?php echo HELP_OPTIONS; ?>
                            <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg2"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                            <div class="modal fade" id="myModalWysiwyg2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                                  </div>
                                  <div class="modal-body" style="text-align:center;">
                                     <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                                  </div>
                               </div>
                              </div>
                            </div>
                          </span>
                        </div>
                      </div>
                    </div>
<?php
// ----------------------------- //-->
//          ONGLET Referencement //-->
// ----------------------------- //-->
?>
<!-- decompte caracteres -->
<script type="text/javascript">
  $(document).ready(function(){
<?php
    for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
    //default title
    $("#default_title_<?php echo $i?>").charCount({
      allowed: 70,
      warning: 20,
      counterText: ' Max : '
    });

    //default_description
    $("#default_description_<?php echo $i?>").charCount({
      allowed: 150,
      warning: 20,
      counterText: 'Max : '
    });

    //default tag
    $("#default_tag_<?php echo $i?>").charCount({
      allowed: 50,
      warning: 20,
      counterText: ' Max : '
    });
<?php
   }
?>
  });
</script>
                    <div class="tab-pane" id="tab6">
                      <div class="col-md-12 mainTitle">
                        <span><?php echo TEXT_PRODUCTS_PAGE_REFEFRENCEMENT; ?></span>
                      </div>
                      <div class="adminformTitle">
                        <div class="spaceRow"></div>
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-3"></span>
                            <span class="col-md-3"><a href="http://www.google.fr/trends" target="_blank"><?php echo KEYWORDS_GOOGLE_TREND; ?></a></span>
                            <span class="col-md-3"><a href="https://adwords.google.com/select/KeywordToolExternal" target="_blank"><?php echo ANALYSIS_GOOGLE_TOOL; ?></a></span>
                          </div>
                        </div>
<?php
  for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
?>
                          <div class="row">
                            <span  class="col-md-1 pull-left centerInputFields"><?php echo osc_image(  DIR_WS_CATALOG_IMAGES . 'icons/languages/' . $languages[$i]['image'], $languages[$i]['name']); ?>&nbsp;</span>
                          </div>


                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_PAGE_TITLE; ?></span>
                              <span  class="col-md-6"><?php echo  '&nbsp;' . osc_draw_input_field('products_head_title_tag[' . $languages[$i]['id'] . ']', (($products_head_title_tag[$languages[$i]['id']]) ? $products_head_title_tag[$languages[$i]['id']] : osc_get_products_head_title_tag($pInfo->products_id, $languages[$i]['id'])),'maxlength="70" size="77" id="default_title_'.$i.'"', false); ?></span>
                            </div>
                          </div>
                          <div class="spaceRow"></div>
                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_HEADER_DESCRIPTION; ?></span>
                              <span  class="col-md-6"><?php echo osc_draw_textarea_field('products_head_desc_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '2', (isset($products_head_desc_tag[$languages[$i]['id']]) ? $products_head_desc_tag[$languages[$i]['id']] : osc_get_products_head_desc_tag($pInfo->products_id, $languages[$i]['id'])),'id="default_description_'.$i.'"'); ?></span>
                            </div>
                          </div>
                           <div class="spaceRow"></div>
                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo TEXT_PRODUCTS_KEYWORDS; ?></span>
                              <span  class="col-md-6"><?php echo osc_draw_textarea_field('products_head_keywords_tag[' . $languages[$i]['id'] . ']', 'soft', '75', '5', (isset($products_head_keywords_tag[$languages[$i]['id']]) ? $products_head_keywords_tag[$languages[$i]['id']] : osc_get_products_head_keywords_tag($pInfo->products_id, $languages[$i]['id']))); ?></span>
                            </div>
                          </div>
                          <div class="spaceRow"></div>
                          <div class="row">
                            <div class="col-md-12">
                              <span class="col-md-2 centerInputFields"><?php echo  TEXT_PRODUCTS_TAG; ?></span>
                              <span  class="col-md-6"><?php echo osc_draw_input_field('products_head_tag[' . $languages[$i]['id'] . ']', (($products_head_tag[$languages[$i]['id']]) ? $products_head_tag[$languages[$i]['id']] : osc_get_products_tag($pInfo->products_id, $languages[$i]['id'])),'maxlength="50" size="77" id="default_tag_'.$i.'"', false); ?></span>
                            </div>
                          </div>
<?php
  }
?>
                          <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                          <div class="adminformAide">
                            <div class="row">
                              <span class="col-md-12">
                                <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_SUBMIT); ?>
                                <strong><?php echo '&nbsp;' . TITLE_HELP_SUBMIT; ?></strong>
                              </span>
                            </div>
                            <div class="spaceRow"></div>
                            <div class="row">
                              <span class="col-md-12"><?php echo '&nbsp;&nbsp;' . HELP_SUBMIT; ?></span>
                            </div>
                          </div>
                      </div>
                    </div>
<?php
// ---------------------------------------- //-->
//          ONGLET  fields Options          //-->
// -------------------------------------- //-->
?>
                    <div class="tab-pane" id="tab7">
                      <div class="col-md-12 mainTitle">
                        <span><?php echo TEXT_PRODUCTS_PAGE_OPTION; ?></span>
                      </div>
                      <table width="100%" cellpadding="5" cellspacing="0" border="0" class="adminformTitle">
                        <tr>
                          <td colspan="3"><?php echo osc_draw_separator('pixel_trans.gif', '1', '20'); ?></td>
                        </tr>
<?php
// ---------------------
// extra fields
// ----------------------
      for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
        $languages_array[$languages[$i]['id']]=$languages[$i];
      }

      $QextraFields = $OSCOM_PDO->prepare('select *
                                          from products_extra_fields
                                          order by products_extra_fields_order
                                        ');
      $QextraFields->execute();

      while ($extra_fields = $QextraFields->fetch() ) {
// Display language icon or blank space
        if ($extra_fields['languages_id'] == '0') {
          $fields_products = osc_draw_separator('pixel_trans.gif', '24', '15');
        } else {
          $fields_products= osc_image(DIR_WS_CATALOG_IMAGES . 'icons/languages/' .  $languages_array[$extra_fields['languages_id']]['image'], $languages_array[$extra_fields['languages_id']]['name']);
        }
// Display customers_group icon or blank space
       if ($extra_fields['customers_group_id'] != '0' ) {
         $fields_icon_customers_group = osc_image(DIR_WS_ICONS . 'group_client.gif', ICON_EDIT_CUSTOMERS_GROUP, 16,16);
       } else {
         $fields_icon_customers_group = osc_draw_separator('pixel_trans.gif', '16', '16');
       }

       if ($extra_fields['products_extra_fields_status'] == '1' ) {
         $fields_icon_status = osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', ICON_EDIT_STATUS_DISPLAY_CATALOG_ON, 16,16);
       } else {
         $fields_icon_status = osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', ICON_EDIT_STATUS_DISPLAY_CATALOG_OFF, 16,16);
       }
?>
                        <tr>
                          <td><table width="70%" cellpadding="O" cellspacing="O">
                            <tr align="center" valign="top">
                              <td width="20" align="left"><?php echo  $fields_products .'&nbsp;';?></td>
                              <td class="main" width="140" align="left"><?php echo  $extra_fields['products_extra_fields_name']; ?>:</td>
<?php
       if ($extra_fields['products_extra_fields_type'] == '1') {
?>
                              <td align="left" class="main"><div style="visibility:visible; display:block;"><?php echo osc_draw_textarea_ckeditor("extra_field[".$extra_fields['products_extra_fields_id']."]", 'soft','750','200', str_replace('& ', '&amp; ', trim($pInfo->extra_field[$extra_fields['products_extra_fields_id']])));?></div></td>
<?php
       } elseif ($extra_fields['products_extra_fields_type'] == '0') {
?>
                              <td class="main" align="left" width="85"><?php echo osc_draw_input_field("extra_field[".$extra_fields['products_extra_fields_id']."]", $pInfo->extra_field[$extra_fields['products_extra_fields_id']]); ?>
<?php
  } else {
      $checkbox_type_array = array(array('id' => 'NO_DISPLAY_CHECKBOX', 'text' => NO_DISPLAY_CHECKBOX),
                                   array('id' => 'DISPLAY_CHECKBOX', 'text' => DISPLAY_CHECKBOX)
                                   );
?>
                               <td class="main" align="left" width="85"><?php echo osc_draw_pull_down_menu("extra_field[".$extra_fields['products_extra_fields_id']."]", $checkbox_type_array, $pInfo->extra_field[$extra_fields['products_extra_fields_id']], ''); ?></td>
<?php
  }
?>
                               <td class="main" align="left"><?php echo  $fields_icon_status  .'&nbsp; ' . $fields_icon_customers_group; ?></td>
                             </tr>
                           </table></td>
                         </tr>
<?php
      }
// END: Extra Fields Contribution
?>
                      </table>

                      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                      <div class="adminformAide">
                        <div class="row">
                          <span class="col-md-12">
                            <?php echo osc_image (DIR_WS_IMAGES . 'icons/help.gif', TITLE_HELP_OPTIONS); ?>
                            <strong><?php echo '&nbsp;' . TITLE_HELP_OPTIONS; ?></strong>
                          </span>
                        </div>
                        <div class="spaceRow"></div>
                        <div class="row">
                          <span class="col-md-12">
                           <?php echo HELP_OPTIONS; ?>
                           <blockquote><i><a data-toggle="modal" data-target="#myModalWysiwyg2"><?php echo TEXT_HELP_WYSIWYG; ?></a></i></blockquote>
                           <div class="modal fade" id="myModalWysiwyg2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                   <h4 class="modal-title" id="myModalLabel"><?php echo TEXT_HELP_WYSIWYG; ?></h4>
                                 </div>
                                 <div class="modal-body" style="text-align:center;">
                                   <img src="<?php echo  DIR_WS_IMAGES . 'wysiwyg.png' ;?>">
                                 </div>
                               </div>
                             </div>
                           </div>
                          </span>
                        </div>
                      </div>
                    </div>
<?php
// ---------------------------------------- //-->
//          ONGLET Autres options            //-->
// -------------------------------------- //-->
?>
                    <div class="tab-pane" id="tab8">
                      <div class="col-md-12 mainTitle">
                        <span><?php echo TEXT_PRODUCTS_OTHERS_OPTIONS; ?></span>
                      </div>


                      <div class="adminformTitle">
                        <div class="spaceRow"></div>
<?php
  if(empty($_GET['pID'])) {
?>
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-3"><?php echo  TEXT_PRODUCTS_HEART; ?></span>
                            <span  class="col-md-1"><?php echo osc_draw_checkbox_field('products_heart', 'yes', false); ?></span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-3"><?php echo  TEXT_PRODUCTS_FEATURED; ?></span>
                            <span  class="col-md-1"><?php echo osc_draw_checkbox_field('products_featured', 'yes', false); ?></span>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-3"><?php echo  TEXT_PRODUCTS_SPECIALS; ?></span>
                            <span  class="col-md-2"><?php echo osc_draw_checkbox_field('products_specials', 'yes', false); ?></span>
                            <span class="col-md-3"><?php echo  TEXT_PRODUCTS_SPECIALS_PERCENTAGE; ?></span>
                            <span  class="col-md-1"><?php echo osc_draw_input_field('percentage_products_specials', '','class="input-small"'); ?></span>
                          </div>
                        </div>
<?php
  }
?>
                        <div class="row">
<?php
    if (MODULE_ADMIN_DASHBOARD_TWITTER_STATUS == 'True' ) {
?>
                          <div class="col-md-12">
                            <span class="col-md-3"><?php echo TEXT_PRODUCTS_TWITTER; ?></span>
                            <span  class="col-md-3"><?php echo  osc_draw_radio_field('products_twitter', '1', $pInfo->in_accept_twitter) . '&nbsp;' . TEXT_YES . '&nbsp;' . osc_draw_radio_field('products_twitter', '0', $pInfo->out_accept_twitter) . '&nbsp;' . TEXT_NO; ?></span>
                          </div>
<?php
    }
?>
                        </div>
                        <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
                        <div class="row">
                        <div class="col-md-12">
                          <span class="col-md-3"><?php echo  TEXT_PRODUCTS_FILE_DOWNLOAD; ?></span>
                          <span  class="col-md-2"><?php echo  osc_draw_file_field('products_download_filename'); ?><br />
<?php
  if (!empty($pInfo->products_download_filename)) {
?>
                                <?php echo osc_draw_input_field('products_download_filename', $pInfo->products_download_filename, 'disabled="disabled"'); ?>&nbsp;
<?php
  }
?>
                          </span>
                          <span class="col-md-3 centerInputFields"><?php echo  TEXT_PRODUCTS_FILE_DOWNLOAD_PUBLIC; ?></span>
                          <span  class="col-md-1 centerInputFields"><?php echo osc_draw_checkbox_field('products_download_public', '1', $pInfo->products_download_public); ?></span>
                        </div>
                      </div>
                    </div>
<?php
// ---------------------------------
// Product clone to categories
// ---------------------------------
  if (!empty($_GET['pID'])) {
    echo osc_draw_separator('pixel_trans.gif', '1', '10');
?>
                      <div class="col-md-12 mainTitle">
                        <span><?php echo TEXT_PRODUCTS_CATEGORIES_COPY; ?></span>
                      </div>
                      <div class="adminformTitle">
                        <div class="spaceRow"></div>
                        <div class="row">
                          <div class="col-md-12">
                            <span class="col-md-1"></span>
                            <span class="col-md-4 pull-left" style="padding-top:50px;">
                              <?php echo CLONE_PRODUCTS_FROM; ?>&nbsp;
                              <select name="clone_products_id_from">
<?php
    $Qproducts = $OSCOM_PDO->prepare("select p.products_id,
                                             pd.products_name,
                                             p.products_model
                                      from :table_products p,
                                           :table_products_description pd
                                      where pd.products_id = p.products_id
                                      and pd.language_id = :language_id
                                      and pd.products_id = :products_id
                                      order by pd.products_name
                                    ");
    $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id']);
    $Qproducts->bindInt(':products_id', (int)$_GET['pID']);
    $Qproducts->execute();

    $products_values = $Qproducts->fetch();

    echo '<option name="' . $products_values['products_name'] . '" value="' . $products_values['products_id'] . '">'. $products_values['products_model']  . ' - '  . $products_values['products_name'] . '</option>';
?>
                              </select>
                            </span>
                            <span  class="col-md-2 pull-left" style="padding-top:50px;"> <?php echo CLONE_PRODUCTS_TO; ?></span>
                            <span  class="col-md-4 pull-left">
                              <select name="clone_categories_id_to[]" multiple size="10">
<?php
  $Qcategories = $OSCOM_PDO->prepare('select c.categories_id,
                                             cd.categories_name
                                      from :table_categories c,
                                           :table_categories_description cd
                                      where cd.categories_id = c.categories_id
                                      and cd.language_id = :language_id
                                      and c.categories_id <> :categories_id
                                     ');
  $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id']);
  $Qcategories->bindInt(':categories_id', (int)$_GET['cPath']);
  $Qcategories->execute();

  while ($categories_copy = $Qcategories->fetch() ) {
    echo '<option name="'. $categories_copy['categories_name'].'" value="' . $categories_copy['categories_id'] . '">'. $categories_copy['categories_name'] . '</option>';
  }
?>
                              </select>
                            </span>
                          </div>
                        </div>
                      </div>
<?php
  } // end empty
?>
              </div>
            </div>
          </div>
        </div>
      </td>
    </tr>
  </table></form>
<!-- fin produit -->
<?php
    } else {
 // ################################################################################################################ -->
 //                                         LISTING PRODUITS ET CATEGORIES                                          -->
 // ################################################################################################################ -->
?>
    <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <div class="adminTitle" style="height:100px;">
          <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/categorie_produit.gif', HEADING_TITLE, '40', '40'); ?></span>
          <span class="col-md-3 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>

         <span class="col-md-3" style="text:align:center;">
           <div class="form-group">
             <div class="controls">
<?php
 echo osc_draw_form('search', 'categories.php', '', 'get');
 echo  osc_draw_input_field('search', '', 'id="inputKeywords" placeholder="'.HEADING_TITLE_SEARCH.'"');
 echo osc_hide_session_id();
?>
              </form>
             </div>
           </div>

<?php
  echo osc_draw_form('goto', 'categories.php', '', 'get') . HEADING_TITLE_GOTO . ' ' . osc_draw_pull_down_menu('cPath', osc_get_category_tree(), $current_category_id, 'onchange="this.form.submit();"');
  echo osc_hide_session_id() . '</form>';
?>
         </span>
         <span class="pull-right">
<?php
  echo '<a href="' . osc_href_link('categories.php', $cPath_back . 'cID=' . $current_category_id) . '">' . osc_image_button('button_back.gif', IMAGE_BACK) . '</a>&nbsp;';
  if (!isset($_GET['search'])) {
    echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&action=new_category') . '">' . osc_image_button('button_new_category.gif', IMAGE_NEW_CATEGORY) . '</a>&nbsp;';
    echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&action=new_product') . '">' . osc_image_button('button_new_product.gif', IMAGE_NEW_PRODUCT) . '</a>';
  }
// select all the product to delete
?>

          <form name="delete_all" <?php echo 'action="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&action=delete_all') . '"'; ?> method="post"><a onClick="$('delete').prop('action', ''); $('form').submit();" class="button"><?php echo osc_image_button('button_delete_big.gif', IMAGE_DELETE); ?></a>&nbsp;
        </span>

      </div>



      <div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
           <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
<!-- // select all the product to delete -->
                <td width="1" style="text-align: center;"><input type="checkbox" onClick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="dataTableHeadingContent" colspan="3">&nbsp;</td>
                <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_CATEGORIES_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRICE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_QTY; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAST_MODIFIED; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CREATED; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SORT_ORDER; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $categories_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $search = osc_db_prepare_input($_GET['search']);

// Recherche dans les categories
      $Qcategories= $OSCOM_PDO->prepare('select c.categories_id,
                                               cd.categories_name,
                                               c.categories_image,
                                               c.parent_id,
                                               c.sort_order,
                                               c.date_added,
                                               c.last_modified ,
                                               c.virtual_categories
                                        from :table_categories c,
                                             :table_categories_description cd
                                        where c.categories_id = cd.categories_id
                                        and cd.language_id = :language_id
                                        and cd.categories_name like :search
                                        order by c.sort_order,
                                                 cd.categories_name
                                        ');
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qcategories->bindValue(':search', '%'.$search.'%');
      $Qcategories->execute();

    } else {
      $Qcategories= $OSCOM_PDO->prepare('select c.categories_id,
                                               cd.categories_name,
                                               c.categories_image,
                                               c.parent_id,
                                               c.sort_order,
                                               c.date_added,
                                               c.last_modified,
                                               c.virtual_categories
                                        from :table_categories c,
                                             :table_categories_description cd
                                        where c.parent_id = :parent_id
                                        and c.categories_id = cd.categories_id
                                        and cd.language_id = :language_id
                                        order by c.sort_order,
                                        cd.categories_name
                                        ');

      $Qcategories->bindInt(':parent_id', (int)$current_category_id );
      $Qcategories->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qcategories->execute();
    }

    while ($categories =  $Qcategories->fetch() ) {
      $categories_count++;
      $rows++;

// Get parent_id for subcategories if search
      if (isset($_GET['search'])) $cPath= $categories['parent_id'];

      if ((!isset($_GET['cID']) && !isset($_GET['pID']) || (isset($_GET['cID']) && ($_GET['cID'] == $categories['categories_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
        $category_childs = array('childs_count' => osc_childs_in_category_count($categories['categories_id']));
        $category_products = array('products_count' => osc_products_in_category_count($categories['categories_id']));

        $cInfo_array = array_merge($categories, $category_childs, $category_products);
        $cInfo = new objectInfo($cInfo_array);
      }

      if (isset($cInfo) && is_object($cInfo) && ($categories['categories_id'] == $cInfo->categories_id) ) {
        echo '    <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('categories.php', osc_get_path($categories['categories_id'])) . '\'">' . "\n";
      } else {
        echo '    <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories['categories_id']) . '\'">' . "\n";
      }
?>
                <td class="dataTableContent" align="center">&nbsp;</td> 
                <td class="dataTableContent"><?php echo '<a href="' . osc_href_link('categories.php', osc_get_path($categories['categories_id'])) . '">' . osc_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER); ?></td>
                <td class="dataTableContent" colspan="2">&nbsp;</td>
                <td class="dataTableContent" align="left"><?php echo '<strong>' . $categories['categories_name'] . '</strong>'; ?></td>
                <td class="dataTableContent" align="center">&nbsp;</td>
                <td class="dataTableContent" align="center">&nbsp;</td>
                <td class="dataTableContent" align="center">&nbsp;</td>
                <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($categories['last_modified']); ?></td>
                <td class="dataTableContent" align="center">&nbsp;</td>          
                <td class="dataTableContent" align="center"><?php echo $categories['sort_order']; ?></td>                                            
                <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories['categories_id'] . '&action=edit_category') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories['categories_id'] . '&action=move_category') . '">' . osc_image(DIR_WS_ICONS . 'move.gif', IMAGE_MOVE) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories['categories_id'] . '&action=delete_category') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      if (isset($cInfo) && is_object($cInfo) && ($categories['categories_id'] == $cInfo->categories_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $categories['categories_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                </td>
              </tr>
<?php
  }
 // ################################################################################################################ -->
 //                                         LISTING PRODUITS                                          -->
 //################################################################################################################ -->
 
    $products_count = 0;
// Recherche des produits
    if (isset($_GET['search'])) {

      $Qproducts= $OSCOM_PDO->prepare('select p.products_id,
                                             pd.products_name,
                                             p.products_model,
                                             p.products_ean,
                                             p.products_sku,
                                             p.products_quantity,
                                             p.products_image,
                                             p.products_price,
                                             p.products_date_added,
                                             p.products_last_modified,
                                             p.products_date_available,
                                             p.products_status,
                                             p.admin_user_name,
                                             p.products_volume,
                                             p.products_quantity_unit_id,
                                             p2c.categories_id,
                                             p.products_sort_order,
                                             p.products_download_filename
                                       from :table_products p,
                                            :table_products_description pd,
                                            :table_products_to_categories p2c
                                       where p.products_id = pd.products_id
                                       and pd.language_id = :language_id
                                       and p.products_id = p2c.products_id
                                       and p.products_archive = 0
                                       and (pd.products_name like :search
                                            or  p.products_model like :search
                                            or p.products_ean like :search
                                           )
                                       order by pd.products_name
                                        ');
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->bindValue(':search', '%'.$_GET['search'].'%');
      $Qproducts->execute();

    } else {

      $Qproducts= $OSCOM_PDO->prepare('select p.products_id,
                                             pd.products_name,
                                             p.products_model,
                                             p.products_ean,
                                             p.products_sku,
                                             p.products_quantity,
                                             p.products_image,
                                             p.products_price,
                                             p.products_date_added,
                                             p.products_last_modified,
                                             p.products_date_available,
                                             p.products_status,
                                             p.admin_user_name,
                                             p.products_sort_order,
                                             p.products_download_filename
                                       from :table_products p,
                                            :table_products_description pd,
                                            :table_products_to_categories p2c
                                       where p.products_id = pd.products_id
                                       and pd.language_id = :language_id
                                       and p.products_id = p2c.products_id
                                       and p2c.categories_id = :categories_id
                                       and p.products_archive = 0
                                       order by pd.products_name
                                        ');

      $Qproducts->bindInt(':categories_id', (int)$current_category_id );
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->execute();

    }

    while ($products = $Qproducts->fetch() ) {
      $products_count++;
      $rows++;

// Get categories_id for product if search
      if (isset($_GET['search'])) $cPath = $products['categories_id'];

      if ( (!isset($_GET['pID']) && !isset($_GET['cID']) || (isset($_GET['pID']) && ($_GET['pID'] == $products['products_id']))) && !isset($pInfo) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {

// find out the rating average from customer reviews
/*
        $Qreviews = $OSCOM_PDO->prepare('select (avg(reviews_rating) / 5 * 100) as average_rating
                                         from reviews
                                         where products_id = :products_id
                                        ');
        $Qreviews->bindInt(':products_id',  (int)$products['products_id'] );

        $reviews = $Qreviews->fetch();
*/


        $reviews_query = osc_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating
                                        from reviews
                                        where products_id = '" . (int)$products['products_id'] . "'
                                        ");
        $reviews = osc_db_fetch_array($reviews_query);









        $pInfo_array = array_merge($products, $reviews);
        $pInfo = new objectInfo($pInfo_array);
      }

      if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id) ) {
        echo '      <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '      <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                      <td>
<?php // select all the product to delete 
        if ($products['selected']) { 
?>
                        <input type="checkbox" name="selected[]" value="<?php echo $products['products_id']; ?>" checked="checked" />
<?php 
        } else { 
?>
                        <input type="checkbox" name="selected[]" value="<?php echo $products['products_id']; ?>" />
<?php 
        }
?>
                      </td>

<?php
      if ((isset($products['products_id'])) && $products['products_status'] == '1') {
?>
                     <td class="dataTableContent" width="20px;"><?php echo '<a href="' . DIR_WS_CATALOG . 'product_info.php?products_id=' . $products['products_id'], '" target="_blank">' . osc_image(DIR_WS_ICONS . 'preview_catalog.png', ICON_PREVIEW_CATALOG) . '</a>'; ?></td>
<?php
        } else {
?>
                      <td></td>
<?php
        }
?>
                      <td class="dataTableContent"  width="20px;"><?php echo '<a href="' . osc_href_link('products_preview.php' . '?pID=' . $products['products_id']) . '">' . osc_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>'; ?></td>
                      <td class="dataTableContent"><?php echo  osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], SMALL_IMAGE_WIDTH_ADMIN, SMALL_IMAGE_HEIGHT_ADMIN); ?></td> 
                      <td class="dataTableContent"><?php echo $products['products_name'] .' ['. $products['products_model'] .']' ; ?></td>
                      <td class="dataTableContent" align="center">
<?php
      if ($products['products_status'] == '1') {
        echo '<a href="' . osc_href_link('categories.php', 'action=setflag&flag=0&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_RED_LIGHT, 16, 16) . '</a>';
      } else {
        echo '<a href="' . osc_href_link('categories.php', 'action=setflag&flag=1&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . osc_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 16, 16) . '</a>';
      }
?>
                      </td>
                      <td class="dataTableContent" align="center"><?php echo $products['products_price']; ?></td>
                      <td class="dataTableContent" align="center"><?php echo $products['products_quantity']; ?></td>
                      <td class="dataTableContent" align="center"><?php echo $OSCOM_Date->getShort($products['products_last_modified']); ?></td>
                      <td class="dataTableContent" align="center"><?php echo $products['admin_user_name']; ?></td>
                      <td class="dataTableContent" align="center"><?php echo $products['products_sort_order']; ?></td>
                      <td class="dataTableContent" align="right">
<?php
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' .  $products['products_id'] . '&action=new_product') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', IMAGE_EDIT) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' .  $products['products_id'] . '&action=move_product') . '">' . osc_image(DIR_WS_ICONS . 'move.gif', IMAGE_MOVE) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' .  $products['products_id'] . '&action=copy_to') . '">' . osc_image(DIR_WS_ICONS . 'copy.gif', IMAGE_COPY_TO) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' .  $products['products_id'] . '&action=archive') . '">' . osc_image(DIR_WS_ICONS . 'archive.gif', IMAGE_ARCHIVE_TO) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' .  $products['products_id'] . '&action=delete_product') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>' ;
      echo osc_draw_separator('pixel_trans.gif', '6', '16');
      if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id)) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $products['products_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; }
?>
                      </td>
                    </tr>
<?php
    }

    $cPath_back = '';
    if (isset($cPath_array) && sizeof($cPath_array) > 0) {
      for ($i=0, $n=sizeof($cPath_array)-1; $i<$n; $i++) {
        if (empty($cPath_back)) {
          $cPath_back .= $cPath_array[$i];
        } else {
          $cPath_back .= '_' . $cPath_array[$i];
        }
      }
    }

    $cPath_back = (osc_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
?>
                     <tr> 
                      <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                        <tr>
                          <td class="smallText"><?php echo TEXT_CATEGORIES . '&nbsp;' . $categories_count . '<br />' . TEXT_PRODUCTS . '&nbsp;' . $products_count; ?></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </form>
<?php
    $heading = array();
    $contents = array();
    switch ($action) {

      case 'delete_category':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CATEGORY . '</strong>');

        $contents = array('form' => osc_draw_form('categories', 'categories.php', 'action=delete_category_confirm&cPath=' . $cPath) . osc_draw_hidden_field('categories_id', $cInfo->categories_id));
        $contents[] = array('text' => TEXT_DELETE_CATEGORY_INTRO);
        $contents[] = array('text' => '<br /><strong>' . $cInfo->categories_name . '</strong>');
        if ($cInfo->childs_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_CHILDS, $cInfo->childs_count));
        if ($cInfo->products_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_PRODUCTS, $cInfo->products_count));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'move_category':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_CATEGORY . '</strong>');

        $contents = array('form' => osc_draw_form('categories', 'categories.php', 'action=move_category_confirm&cPath=' . $cPath) . osc_draw_hidden_field('categories_id', $cInfo->categories_id));
        $contents[] = array('text' => sprintf(TEXT_MOVE_CATEGORIES_INTRO, $cInfo->categories_name));
        $contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $cInfo->categories_name) . '<br />' . osc_draw_pull_down_menu('move_to_category_id', osc_get_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_move.gif', IMAGE_MOVE) . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'delete_product':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</strong>');

        $contents = array('form' => osc_draw_form('products', 'categories.php', 'action=delete_product_confirm&cPath=' . $cPath) . osc_draw_hidden_field('products_id', $pInfo->products_id));
        $contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);
        $contents[] = array('text' => '<br /><strong>' . $pInfo->products_name . '</strong>');

        $product_categories_string = '';
        $product_categories = osc_generate_category_path($pInfo->products_id, 'product');
        for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
          $category_path = '';
          for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
            $category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
          }
          $category_path = substr($category_path, 0, -16);
          $product_categories_string .= osc_draw_checkbox_field('product_categories[]', $product_categories[$i][sizeof($product_categories[$i])-1]['id'], true) . '&nbsp;' . $category_path . '<br />';
        }
        $product_categories_string = substr($product_categories_string, 0, -4);

        $contents[] = array('text' => '<br />' . $product_categories_string);
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
     break;

      case 'move_product':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_PRODUCT . '</strong>');

        $contents = array('form' => osc_draw_form('products', 'categories.php', 'action=move_product_confirm&cPath=' . $cPath) . osc_draw_hidden_field('products_id', $pInfo->products_id));
        $contents[] = array('text' => sprintf(TEXT_MOVE_PRODUCTS_INTRO, $pInfo->products_name));
        $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . osc_output_generated_category_path($pInfo->products_id, 'product') . '</strong>');
        $contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $pInfo->products_name) . '<br />' . osc_draw_pull_down_menu('move_to_category_id', osc_get_category_tree(), $current_category_id));
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_move.gif', IMAGE_MOVE) . '</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'copy_to':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');
        $contents = array('form' => osc_draw_form('copy_to', 'categories.php', 'action=copy_to_confirm&cPath=' . $cPath) . osc_draw_hidden_field('products_id', $pInfo->products_id));
        $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
        $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . osc_output_generated_category_path($pInfo->products_id, 'product') . '</strong>');
        $contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . osc_draw_pull_down_menu('categories_id', osc_get_category_tree(), $current_category_id));
        $contents[] = array('text' => '<br />' . TEXT_HOW_TO_COPY . '<br />' . osc_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_COPY_AS_LINK . '<br />' . osc_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE);
        $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_copy.gif', IMAGE_COPY) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
      break;

      case 'archive':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_ARCHIVE . '</strong>');
         $contents = array('form' => osc_draw_form('archive', 'categories.php', 'action=archive_to_confirm&cPath=' . $cPath) . osc_draw_hidden_field('products_id', $pInfo->products_id));
         $contents[] = array('text' => TEXT_INFO_ARCHIVE_INTRO);
         $contents[] = array('text' => '<br /><strong>' . $pInfo->products_name . '</strong><br />');
         $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . osc_output_generated_category_path($pInfo->products_id, 'product') . '</strong>');
         $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_archive.gif', IMAGE_ARCHIVE) . ' </div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('categories.php', 'cPath=' . $cPath . '&pID=' . $pInfo->products_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
        break;

        default:
        break;
    }

    if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
            </tr>
          </table></td>
        </tr>
      </table>
<?php
  }
?>
    </td>
  </tr>
</table>
<!-- body_eof //-->
<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
