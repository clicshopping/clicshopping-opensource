<?php
/**
 * categories.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Categories / Products');

define('HEADER_TAGS_CATEGORY_TITLE','Category Title');
define('HEADER_TAGS_CATEGORY_DESCRIPTION','Description');
define('HEADER_TAGS_CATEGORY_KEYWORDS','Keywords');
define('HEADER_TAGS_CATEGORY_REFEFRENCEMENT','Categories meta data');

define('HEADING_TITLE_SEARCH', 'Search');
define('HEADING_TITLE_GOTO', 'Go To:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_CATEGORIES_PRODUCTS', 'Categories / Products');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_CATEGORIES', 'Categories');
define('TABLE_HEADING_SORT_ORDER','Sort Order');

define('TABLE_HEADING_PRICE','Price');
define('TABLE_HEADING_QTY','Stock');
define('TABLE_HEADING_LAST_MODIFIED','last modification'); 
define('TABLE_HEADING_CREATED','Created / modified');

define('TEXT_PRODUCTS_SEO_URL', 'Products SEO URL:');
define('TEXT_EDIT_CATEGORIES_SEO_URL', 'Category SEO URL:');
define('TEXT_CATEGORIES_SEO_URL', 'Category SEO URL:');

define('TEXT_NEW_PRODUCT', 'New Product in &quot;%s&quot;');
define('TEXT_CATEGORIES', 'Categories:');
define('TEXT_SUBCATEGORIES', 'Subcategories:');
define('TEXT_DIVERS_TITLE', 'Others');
define('TEXT_DESCRIPTION_CATEGORIES','Categories Description');
define('TEXT_CATEGORIES_PREVIEW_TITLE','Categorie preview');

define('TEXT_PRODUCTS_PAGE_REFEFRENCEMENT','Product Meta Data');
define('TEXT_PRODUCTS_PAGE_TITLE', 'Title');
define('TEXT_PRODUCTS_HEADER_DESCRIPTION', 'Description');
define('TEXT_PRODUCTS_DESCRIPTION_SUMMARY', 'Short Description');
define('TEXT_PRODUCTS_KEYWORDS', 'Keywords');
define('TEXT_PRODUCTS_TAG','Product Tags');

define('TEXT_CREATE','create');

define('TEXT_PRODUCTS', 'Products:');
define('TEXT_PRODUCTS_PRICE_INFO', 'Sell price:');
define('TEXT_PRODUCTS_COST','Product supplier price:');
define('TEXT_PRODUCTS_HANDLING','Other costs associated:');
define('TEXT_PRODUCTS_PRICE_MARGINS','Profit Margin:');

define('TEXT_PRODUCTS_TAX_CLASS', 'Tax Class:');
define('TEXT_ECOTAX_RATES', 'Ecotaxe Rates :');
define('TEXT_EDIT_ECOTAX_CATEGORIES', 'Ecotaxe categories:');
define('TEXT_PRODUCTS_AVERAGE_RATING', 'Average Rating:');
define('TEXT_PRODUCTS_QUANTITY_INFO', 'Quantity:');

define('TEXT_DATE_ADDED','Date Added     :');
define('TEXT_DATE_AVAILABLE','Date Available :');
define('TEXT_LAST_MODIFIED',  'Last Modified  :');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');
define('TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS', 'Please insert a new category or product in this level.');
define('TEXT_PRODUCT_MORE_INFORMATION', '<a href="http://%s" target="blank"><span class="main"><font color="#0000FF"><strong><u>View URL</u></strong></font></span></a>');
define('TEXT_PRODUCT_DATE_ADDED', 'Product added our catalog on:');
define('TEXT_PRODUCT_DATE_AVAILABLE', 'Product in stock on:');
define('TEXT_PRODUCTS_SHIPPING_DELAY','Specific delivery (other than by default) : ');
define('TEXT_PRODUCT_OPTION','Optional fields');
define('TEXT_PRODUCTS_SORT_ORDER','Sort order');

define('TEXT_PRODUCTS_OTHER_INFORMATION','Others informations');
define('TEXT_PRODUCTS_TIME_REPLENISHMENT','Resupplying:');
define('TEXT_PRODUCTS_WHAREHOUSE','Wharehouse name / Case');
define('TEXT_PRODUCTS_WHAREHOUSE_ROW','Wharehouse row:');
define('TEXT_PRODUCTS_WHAREHOUSE_LEVEL_LOCATION','Location level / Rack');
define('TEXT_PRODUCTS_WHAREHOUSE_PACKAGING','Packaging type:');
define('TEXT_WHAREHOUSE','Storage');


define('TEXT_PRODUCTS_PACKAGING_NEW','New product');
define('TEXT_PRODUCTS_PACKAGING_REPACKAGED','Product repackaged');
define('TEXT_PRODUCTS_PACKAGING_USED','Product Used');


define('TEXT_PRODUCTS_ONLY_ONLINE','Web exclusivity (No selling in shop)');
define('TEXT_PRODUCTS_ONLY_SHOP','Shop exclusivity <br />(only available in your physical shop)');
define('TEXT_YES','yes');
define('TEXT_NO','no');
define('TEXT_CHOOSE','Select');

define('TEXT_PRODUCTS_GALLERY_IMAGE','Images gallery');
define('TEXT_PRODUCTS_IMAGE_DIRECTORY', 'Please, choose a directory in products : ');
define('TEXT_PRODUCTS_IMAGE_NEW_FOLDER', ' or create a directory in products : <br><font size="1"><i>(ex : my_directory_1/my_subdirectory_2)</i></font> ');
define('TEXT_PRODUCTS_IMAGE_NEW_FOLDER_GALLERY', ' or create a directory in products: <font size="1"><i>(ex : my_directory_1/my_subdirectory_2)</i></font> ');

define('TEXT_PRODUCTS_IMAGE_ROOT_DIRECTORY', '-- Sub Directories  available --');
define('TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT','Html Content (for pup up)');
define('TEXT_PRODUCTS_MAIN_IMAGE','Add a big image : ');
define('TEXT_PRODUCTS_IMAGE_MEDIUM','Medium Image(for the product description)');
define('TEXT_PRODUCTS_SMALL_IMAGE','Small image name : ');
define('TEXT_PRODUCTS_MEDIUM_IMAGE','Medium image name : ');
define('TEXT_PRODUCTS_BIG_IMAGE','Big image name : ');
define('TEXT_DELETE_PRODUCTS_IMAGE','Delete image');
define('TEXT_PRODUCTS_FILE_DOWNLOAD', 'upload Files');
define('TEXT_PRODUCTS_FILE_DOWNLOAD_PUBLIC','Is it a public download ?');
define('NO_DISPLAY_CHECKBOX','No display checkbox');
define('DISPLAY_CHECKBOX','Display checkbox');

define('TEXT_PRODUCTS_CATEGORIES_COPY', 'product to clone or copy');
define('CLONE_PRODUCTS_FROM','Product to clone');
define('CLONE_PRODUCTS_TO', 'Destination of the copy');

define('TEXT_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_EDIT_CATEGORIES_ID', 'Category ID:');
define('TEXT_EDIT_CATEGORIES_NAME', 'Category Name:');
define('TEXT_EDIT_CATEGORIES_IMAGE', 'Category Image:');
define('TEXT_EDIT_SORT_ORDER', 'Sort Order:');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new category you wish to copy this product to');
define('TEXT_INFO_CURRENT_CATEGORIES', 'Current Categories:');

define('TEXT_INFO_HEADING_NEW_CATEGORY', 'New Category');
define('TEXT_INFO_HEADING_EDIT_CATEGORY', 'Edit Category');
define('TEXT_INFO_HEADING_DELETE_CATEGORY', 'Delete Category');
define('TEXT_INFO_HEADING_MOVE_CATEGORY', 'Move Category');
define('TEXT_INFO_HEADING_DELETE_PRODUCT', 'Delete Product');
define('TEXT_INFO_HEADING_MOVE_PRODUCT', 'Move Product');
define('TEXT_INFO_HEADING_COPY_TO', 'Copy To');
define('TEXT_INFO_HEADING_ARCHIVE','Archiving Product');
define('TEXT_INFO_ARCHIVE_INTRO','Do you want to put this product in archive? <br /> <br /> This product can be reactivated in the archive section. <br /> <br /> <strong> Note: </strong> Archiving allows to remove your product catalog, but it does not deleted. Customers will have access to the product via search engines but they can not order it. This system is useful for referencing your site.');

define('TEXT_DELETE_CATEGORY_INTRO', 'Are you sure you want to delete this category?');
define('TEXT_DELETE_PRODUCT_INTRO', 'Are you sure you want to permanently delete this product?');

define('TEXT_DELETE_WARNING_CHILDS', '<strong>WARNING:</strong> There are %s (child-)categories still linked to this category!');
define('TEXT_DELETE_WARNING_PRODUCTS', '<strong>WARNING:</strong> There are %s products still linked to this category!');

define('TEXT_MOVE_PRODUCTS_INTRO', 'Please select which category you wish <strong>%s</strong> to reside in');
define('TEXT_MOVE_CATEGORIES_INTRO', 'Please select which category you wish <strong>%s</strong> to reside in');
define('TEXT_MOVE', 'Move <strong>%s</strong> to:');

define('TEXT_NEW_CATEGORY_INTRO', 'Please fill out the following information for the new category');
define('TEXT_CATEGORIES_NAME', 'Category Name:');
define('TEXT_CATEGORIES_NAME_TITLE', 'Category Name');
define('TEXT_CATEGORIES_IMAGE', 'Category Image:');
define('TEXT_SORT_ORDER', 'Sort Order:');

define('TEXT_PRODUCTS_STATUS', 'Status:');
define('TEXT_PRODUCTS_DATE_AVAILABLE', 'Date available:');
define('TEXT_PRODUCT_AVAILABLE', 'In Stock');
define('TEXT_PRODUCTS_DIMENSION','Dimensions (LxHxW)');
define('TEXT_PRODUCTS_DIMENSION_TYPE','Dimension type');
define('TEXT_PRODUCT_NOT_AVAILABLE', 'Out of stock');
define('TEXT_PRODUCTS_MANUFACTURER', 'Product brand: ');
define('TEXT_PRODUCTS_NAME', 'Products Name');
define('TEXT_PRODUCTS_DESCRIPTION', 'Products Description');
define('TEXT_PRODUCTS_QUANTITY', 'Stock quantity: ');

define('TEXT_PRODUCTS_MIN_ORDER_QUANTITY', 'Quantity minimal for a customer order :' );
define('TEXT_PRODUCTS_MIN_ORDER_QUANTITY_GROUP', 'Qty min type');
define('TEXT_PRODUCTS_VOLUME','Product volume : ');
define('TEXT_PRODUCTS_QUANTITY_UNIT','Quantity unit');

define('TEXT_PRODUCTS_QUANTITY_FIXED_GROUP','Qty min fixed /');
define('TEXT_PRODUCTS_MODEL', 'Model: ');
define('TEXT_PRODUCTS_MODEL_GROUP', 'Model ');

define('TEXT_PRODUCTS_EAN', 'Barcode : ');
define('TEXT_PRODUCTS_SKU','SKU:');
define('TEXT_PRODUCTS_PRICE_COMPARISON','Excluding price comparison ? ');
define('TEXT_PRODUCTS_SUPPLIERS', 'Product supplier: ');

define('ICON_EDIT_CUSTOMERS_GROUP','Is included in a customers group');
define('ICON_EDIT_STATUS_DISPLAY_CATALOG_ON','Field displayed in catalog');
define('ICON_EDIT_STATUS_DISPLAY_CATALOG_OFF','Field displayed in administration');

define('TEXT_PRODUCTS_IMAGE', 'Products Image (automatic resizing)');
define('TEXT_PRODUCTS_IMAGE_CUSTOMIZE','Products Image (manuel inserting)');
define('TEXT_PRODUCTS_IMAGE_VIGNETTE', 'Product Image label');
define('TEXT_PRODUCTS_IMAGE_ZOOM', 'Product Image zoom');
define('TEXT_PRODUCTS_IMAGE_VISUEL', 'Currently visible images on the product');
define('TEXT_PRODUCTS_IMAGE_VISUEL_ZOOM', 'To view the zoom');
define('TEXT_PRODUCTS_NO_IMAGE_VISUEL_ZOOM','There is no image Zoom inserted actually');
define('TEXT_PRODUCTS_DELETE_IMAGE', 'Not display this image on this product');
define('TEXT_PRODUCTS_LARGE_IMAGE','Large Image');
define('TEXT_PRODUCTS_ADD_LARGE_IMAGE','Add Large Image');
define('TEXT_PRODUCTS_ADD_LARGE_IMAGE_DELETE','Please confirm the removal of the large product image');
define('TEXT_PRODUCTS_INSERT_BIG_IMAGE_VIGNETTE','Please insert a big image');

define('TEXT_PRODUCTS_URL', 'Products URL:');
define('TEXT_PRODUCTS_URL_WITHOUT_HTTP', '<small>(without http://)</small>');
define('TEXT_PRODUCTS_PRESENTATION', 'Presentation');
define('TEXT_PRODUCTS_STOCK', 'Stock');
define('TEXT_PRODUCTS_ALERT','Warning Stock');
define('TEXT_PRODUCTS_PRICE_PUBLIC', 'Public price & TAXE');
define('TEXT_PRODUCTS_PRICE', 'Price:');
define('TEXT_PRODUCTS_PRICE_NET', 'TAX exc.');
define('TEXT_PRODUCTS_PRICE_GROSS', 'TAX inc.');
define('TEXT_PRODUCTS_WEIGHT', 'Product weight <i>(kg)</i> :');
define('TEXT_PRODUCTS_WEIGHT_POUND', 'Product weight <i>(pounds)</i> :');
define('TEXT_PRODUCTS_PRICE_KILO','Display the price per kilo:');
define('TEXT_PRODUCTS_DIRECTORY_DONT_EXIST','<strong>This directory don\'t exist. Please contact your administrator : </strong>');

define('TEXT_PRODUCTS_PREVIEW_TITLE', 'Product preview');
define('TEXT_PRODUCTS_PREVIEW_GENERAL', 'General');
define('TEXT_PRODUCTS_PREVIEW_NAME', 'Name of the product');
define('TEXT_PRODUCTS_PREVIEW_URL', 'External URL of the product:');
define('TEXT_PRODUCTS_PREVIEW_PRICE_PUBLIC', 'Public price Taxe exc.: ');
define('TEXT_PRODUCTS_PREVIEW_PRICE_GROUP', 'Price');
define('TEXT_PRODUCTS_PREVIEW_PRICE_GROUP_NO_TAXE', 'Taxe exc.:');
define('TEXT_PRODUCTS_PREVIEW_DESCRIPTION', 'Designation');
define('TEXT_PRODUCTS_PREVIEW_IMAGE', 'Visual');
define('TEXT_PRODUCTS_PREVIEW_PRICE_KILO','Px /Kg');
define('TEXT_PRODUCTS_PAGE_OPTION','Optional fields');
define('TEXT_PRODUCTS_OTHERS_OPTIONS','Others options');
define('TEXT_PRODUCTS_HEART','Do you want insert this product as favorites ?');
define('TEXT_PRODUCTS_FEATURED','Do you want insert this product as featured ?');
define('TEXT_PRODUCTS_SPECIALS','Do you want insert this product as specials ?');
define('TEXT_PRODUCTS_SPECIALS_PERCENTAGE','Discount Amount Percent (ex : 20%)');

define('TEXT_PRODUCT_TYPE', 'Product type');
define('TEXT_STOCKABLE_PRODUCT','Product');
define('TEXT_STOCKABLE_CONSUMABLE','Consumable');
define('TEXT_STOCKABLE_SERVICE','Service');

define('TEXT_PRODUCTS_TWITTER', 'Do you want publish on Twitter ?');
define('TEXT_TWITTER_PRODUCTS', 'Discover our new product ! ');

define('TEXT_USER_NAME','Created / Modified by : ');

define('TEXT_CATEGORIES_IMAGE_TITLE','Categorie main image');
define('TEXT_CATEGORIES_IMAGE_VIGNETTE', 'Categorie Image label');
define('TEXT_CATEGORIES_IMAGE_VISUEL', 'Currently visible image on the categorie');
define('TEXT_CATEGORIES_DELETE_IMAGE', 'Not display this image on this categorie');

define('EMPTY_CATEGORY', 'Empty Category');

define('TEXT_HOW_TO_COPY', 'Copy Method:');
define('TEXT_COPY_AS_LINK', 'Link product');
define('TEXT_COPY_AS_DUPLICATE', 'Duplicate product');

define('ERROR_CANNOT_LINK_TO_SAME_CATEGORY', 'Error: Can not link products in the same category.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: Catalog images directory is not writeable: ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Error: you can not move this category.');
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: Catalog images directory does not exist: ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT', 'Error : The category can not move in this under category.');

define('TEXT_CUST_GROUPS', 'Group Prices');

if (B2B == 'true'){
define('TEXT_OVERRIDE_ON', 'Price + %group');
} else {
define('TEXT_OVERRIDE_ON', 'Price - %group');
}

define('TEXT_OVERRIDE_OFF', 'Manual group price');

define('TAB_PRICE_GROUP_VIEW', 'CREDIT:  Post the price of the group - NOT CREDIT:  Post the public price');
define('TAB_PRODUCTS_GROUP_VIEW', 'To post the product for the customers of the group ');
define('TAB_PRODUCTS_VIEW', 'To post the product for the customers general public ');
define('PRODUCTS_VIEW', 'Options of posting:');
define('TAB_ORDERS_GROUP_VIEW', 'To authorize the ordering of the product for the customers of the group ');
define('TAB_ORDERS_VIEW', 'To authorize the ordering of the product for the customers General public');
define('TAB_OPTIONS_FIELDS','Fields options');
define('TAB_OTHER_OPTIONS','Others Options');
define('TAB_STOCK','Stock');
define('TITLE_AIDE_PRICE', 'Note on the use of the options of posting on the products');
define('HELP_PRICE_GROUP_VIEW', '<strong>Activated:</strong> Post the price of the group - <strong>Not Activated:</strong> Post the public price');
define('HELP_PRODUCTS_VIEW', '<strong>Activated:</strong> Authorize the posting of the product to the customers.');
define('HELP_ORDERS_VIEW', '<strong>Activated:</strong> Authorize with the customers to order the product.');
define('HELP_OTHERS_GROUP','Becarefull Qty min fixed / purchase type is not in relation with the order qty. ex : For 1 order qty we have 3 products by lot');
define('HELP_PRICE_GROUP_VIEW_NOTE', 'Include promotions.');

define('TEXT_EDIT_DEFAULT_CONFIGURATION','Edit the default configuration');

define('TITLE_AIDE_IMAGE', 'Note on the use of the images');

define('HELP_IMAGE_PRODUCTS', '
<li>Automatic resizing image : <br />
 <blockquote>
    - Small image for others site page : ' . SMALL_IMAGE_WIDTH . ' x ' . SMALL_IMAGE_HEIGHT .'<br />
    - Medium image for the products description page : ' . MEDIUM_IMAGE_WIDTH . ' x ' . MEDIUM_IMAGE_HEIGHT .'<br />
    - Image Zoom for the products description page : ' . BIG_IMAGE_WIDTH . ' x ' . BIG_IMAGE_HEIGHT .'<br />
    - Accepted formats : JPG, GIF, PNG. <br />
    - Max file size accepted : 2000 Ko max.<br />
    - You can change the automatic resizing (Design / Design Configuration/ Images).<br />
    - You can cactivate also the manual upload (Configuration / Mon administrations / Gestion des images).<br />
 </blockquote>
</li>
<li> The gallery requires the fancy Box plug in installed. If there is no image in the gallery, then this is the default image of the thumbnail that is displayed as a popup. </li>
');

define('HELP_IMAGE_CATEGORIES', '<li>Please not insert more than one image in the section Categorie image label.</li><li>If you use the Firefox navigator, you must click twice on the button source to use the wisiwyg correctly.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock +)</li>');

define('TITLE_HELP_DESCRIPTION', 'Note on the use of the wysiwyg');
define('HELP_DESCRIPTION', '<li>To activate wysiwyg, you must click twice on Source button for Firefox users.</li><li>If you inserted plugin in Firefox, there can be incompatibilities (ex: adblock more)</li>');

define('TITLE_HELP_SUBMIT', 'Note on the search engine optimization management');
define('HELP_SUBMIT', '<li>The system automatically manages the search engine optimization.<br />
<blockquote>It automatically included elements in the following order if the fields are not filled:<br />
Title : the name of your product (ex leather jacket)<br />	
Description: The product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Category: Category Product<br />
</blockquote></li>
<li>If you fill in the fields, the tags appear in the following way:<br />
<blockquote>
Title: The title of fields, the name of your product (ex leather jacket)<br />	
Description: The field description, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
Keywords: the fields keywords, the product name (leather jacket), the product name cut (jacket, leather), the category name<br />
</blockquote></li>
<li><strong>Please do not put too keywords (maximum 10)</strong> and establish consistency between each field to find the words the most important first</li>
');

define('TITLE_HELP_OPTIONS', 'Notes on the options');
define('HELP_OPTIONS', '<li>The options field on the products, there aren\'t included in the invoice.</li><li>If you wish create or delete a field, you must go in option field management in admin section.</li>
If the field is empty, it will not appear in the catalog.</li>');

define('TITLE_HELP_GENERAL', '	Briefing Notes');
define('HELP_GENERAL', '<li>A number of fields are configurable from the configuration menu.</li>');


define('HELP_STOCK', '<li>Regarding the management of orders through a <strong> minimum quantity Purchasing</strong>, please follow these instructions for optimal use. </Li><blockquote> - If the default settings (menu Configuration / My Store / minimum value - maximum) is = 1 and configuration of product = 0 then the minimum quantity purchasing a product will be 1 (default configuration). <br /> - If the default is > 30 (example) and configuring the product = 0 then the minimum quantity purchasing a product will be 30. <br /> - If the default settings  is > 30, and configuration of product = 1 then the minimum quantity purchasing a product will be 1. <br /> -  If the default configuration is = 0  and minimum quantity purchasing a Product = 0, the client can not pass an order for this product. </li></blockquote>.
<li><strong>Stock Alert product</strong>.-  On stock alerts, if stock warning products = 0 and stock alert warning general (see warning general configuration stock in configuration section), no mail will be sent.</li></blockquote>
');
?>
