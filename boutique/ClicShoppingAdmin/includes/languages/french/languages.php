<?php
/*
 * languages.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Langues');

define('TABLE_HEADING_LANGUAGE_NAME', 'Langue');
define('TABLE_HEADING_LANGUAGE_CODE', 'Code');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_LANGUAGE_STATUS','Actif / Non actif');
define('TEXT_INFO_EDIT_INTRO', 'Merci de faire les changements nécessaires');
define('TEXT_INFO_COMMON_CURRENCIES', '-- Monnaies communes --');
define('TEXT_INFO_LANGUAGE_NAME', 'Nom :');
define('TEXT_INFO_LANGUAGE_CODE', 'Code :');
define('TEXT_INFO_LANGUAGE_IMAGE', 'Image :');
define('TEXT_INFO_LANGUAGE_DIRECTORY', 'Répertoire :');
define('TEXT_INFO_LANGUAGE_SORT_ORDER', 'Ordre de tri :');
define('TEXT_INFO_INSERT_INTRO', 'Veuillez remplir les divers informations ci-dessous pour insérer une nouvelle langue.');
define('TEXT_INFO_DELETE_INTRO', 'Etes vous sur de vouloir supprimer cette langue ?<br /><br /><strong><font color="#FF0000">Note :</font></strong> Si vous supprimez cette langue, cela supprimera tous les enregistrements associés.');
define('TEXT_INFO_HEADING_NEW_LANGUAGE', 'Nouvelle langue');
define('TEXT_INFO_HEADING_EDIT_LANGUAGE', 'Editer langue');
define('TEXT_INFO_HEADING_DELETE_LANGUAGE', 'Supprimer langue');

define('TEXT_INFO_CREATE_LANGUAGE','Souhaitez-vous créer les répertoires ?');
define('TEXT_CREATE_LANGUAGE','Confirmation de la création des répertoires');
define('TEXT_NOTE_CREATE_LANGUAGE','<br /><strong>Notes :</strong><br /> La création des répertoires se fera à partir d\'une copie des fichiers du répertoire de langue par défaut. <br /><strong>En fonction des caractéristiques des serveurs, cette opération peut ne pas fonctionner.</strong> Veuillez alors faire cette opération manuellement.<br /><br />Les répertoires ou seront créés les fichiers sont : <br />- Dans l\'administration<br />- Dans le répertoire par défaut des langues du catalogue<br /><br />- Dans le répertoire langue du template du catalogue.<br />- Dans le répertoire graphisme du template par défaut.<br /><br />Veuillez vous connecter à la market place pour télécharger la langue et l\'installer à partir du module d\'installation de votre administration.');

define('ERROR_REMOVE_DEFAULT_LANGUAGE', 'Erreur : La langue par défaut ne peut être supprimée. Merci de choisir une nouvelle langue par défaut et réessayer.');
?>