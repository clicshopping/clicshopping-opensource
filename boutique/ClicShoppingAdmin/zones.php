<?php
/**
 * zones.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (osc_not_null($action)) {
    switch ($action) {
      case 'insert':
        $zone_country_id = osc_db_prepare_input($_POST['zone_country_id']);
        $zone_code = osc_db_prepare_input($_POST['zone_code']);
        $zone_name = osc_db_prepare_input($_POST['zone_name']);

        $OSCOM_PDO->save('zones', [
                                  'zone_country_id' =>  (int)$zone_country_id,
                                  'zone_code' => $zone_code,
                                  'zone_name' =>  $zone_name
                                  ]
                        );

        osc_redirect(osc_href_link('zones.php'));
        break;
      case 'save':
        $zone_id = osc_db_prepare_input($_GET['cID']);
        $zone_country_id = osc_db_prepare_input($_POST['zone_country_id']);
        $zone_code = osc_db_prepare_input($_POST['zone_code']);
        $zone_name = osc_db_prepare_input($_POST['zone_name']);


        $Qupdate = $OSCOM_PDO->prepare('update :table_zones
                                        set zone_country_id = :zone_country_id, 
                                            zone_code = :zone_code, 
                                            zone_name = :zone_name 
                                        where zone_id = :zone_id
                                      ');
        $Qupdate->bindInt(':zone_country_id', (int)$zone_country_id);
        $Qupdate->bindValue(':zone_code', osc_db_input($zone_code));
        $Qupdate->bindValue(':zone_name',osc_db_input($zone_name) );  
        $Qupdate->bindInt(':zone_id', (int)$zone_id );

        $Qupdate->execute();

        osc_redirect(osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $zone_id));
        break;
      case 'deleteconfirm':
        $zone_id = osc_db_prepare_input($_GET['cID']);


        $Qdelete = $OSCOM_PDO->prepare('delete 
                                        from :table_zones
                                        where zone_id = :zone_id 
                                      ');
        $Qdelete->bindValue(':zone_id',  (int)$zone_id );
        $Qdelete->execute();

        osc_redirect(osc_href_link('zones.php', 'page=' . $_GET['page']));
        break;
    }
  }
 require('includes/header.php');

?>
<!-- header_eof //-->
<!-- body //-->

<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>
<div class="adminTitle">
  <span class="col-md-1"><?php echo osc_image(DIR_WS_IMAGES . 'categories/zones.gif', HEADING_TITLE, '40', '40'); ?></span>
  <span class="col-md-8 pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></span>
<?php
  if (empty($action)) {
?>
    <span class="pull-right"><?php echo '<a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&action=new') . '">' . osc_image_button('button_new_zone.gif', IMAGE_NEW_ZONE) . '</a>'; ?></span>
<?php
  }
?>
</div>
<div><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></div>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="5">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ZONE_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ZONE_CODE; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $zones_query_raw = "select z.zone_id, 
                            c.countries_id, 
                            c.countries_name, 
                            z.zone_name, 
                            z.zone_code, 
                            z.zone_country_id 
                      from zones z,
                           countries c
                      where z.zone_country_id = c.countries_id 
                      order by c.countries_name, z.zone_name
                    ";
  $zones_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $zones_query_raw, $zones_query_numrows);
  $zones_query = osc_db_query($zones_query_raw);
  while ($zones = osc_db_fetch_array($zones_query)) {
    if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $zones['zone_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
      $cInfo = new objectInfo($zones);
    }

    if (isset($cInfo) && is_object($cInfo) && ($zones['zone_id'] == $cInfo->zone_id)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $zones['zone_id']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $zones['countries_name']; ?></td>
                <td class="dataTableContent"><?php echo $zones['zone_name']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $zones['zone_code']; ?></td>
                <td class="dataTableContent" align="right">
<?php
                  echo '<a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $zones['zone_id'] . '&action=edit') . '">' . osc_image(DIR_WS_ICONS . 'edit.gif', ICON_EDIT) . '</a>' ;
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  echo '<a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $zones['zone_id'] . '&action=delete') . '">' . osc_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE) . '</a>';
                  echo osc_draw_separator('pixel_trans.gif', '6', '16');
                  if (isset($cInfo) && is_object($cInfo) && ($zones['zone_id'] == $cInfo->zone_id) ) { echo osc_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $zones['zone_id']) . '">' . osc_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?></td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $zones_split->display_count($zones_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ZONES); ?></td>
                    <td class="smallText" align="right"><?php echo $zones_split->display_links($zones_query_numrows, MAX_DISPLAY_SEARCH_RESULTS_ADMIN, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_ZONE . '</strong>');

      $contents = array('form' => osc_draw_form('zones', 'zones.php', 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_ZONES_NAME . '<br />' . osc_draw_input_field('zone_name'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_ZONES_CODE . '<br />' . osc_draw_input_field('zone_code'));
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_NAME . '<br />' . osc_draw_pull_down_menu('zone_country_id', osc_get_countries()));
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_insert.gif', IMAGE_INSERT) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('zones.php', 'page=' . $_GET['page']) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'edit':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_ZONE . '</strong>');

      $contents = array('form' => osc_draw_form('zones', 'zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id . '&action=save'));
      $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
      $contents[] = array('text' => '<br />' . TEXT_INFO_ZONES_NAME . '<br />' . osc_draw_input_field('zone_name', $cInfo->zone_name));
      $contents[] = array('text' => '<br />' . TEXT_INFO_ZONES_CODE . '<br />' . osc_draw_input_field('zone_code', $cInfo->zone_code));
      $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_NAME . '<br />' . osc_draw_pull_down_menu('zone_country_id', osc_get_countries(), $cInfo->countries_id));
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_mini_update.gif', IMAGE_UPDATE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    case 'delete':
      $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_ZONE . '</strong>');

      $contents = array('form' => osc_draw_form('zones', 'zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
      $contents[] = array('text' => '<br /><strong>' . $cInfo->zone_name . '</strong>');
      $contents[] = array('align' => 'center', 'text' => '<div class="pull-left"  style="padding-left:50px; padding-top:10px;">' . osc_image_submit('button_delete.gif', IMAGE_DELETE) . '&nbsp;</div><div class="pull-right" style="padding-right:50px; padding-top:10px;"><a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id) . '">' . osc_image_button('button_mini_cancel.gif', IMAGE_CANCEL) . '</a></div>');
    break;
    default:
/*	
      if (isset($cInfo) && is_object($cInfo)) {
        $heading[] = array('text' => '<strong>' . $cInfo->zone_name . '</strong>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id . '&action=edit') . '">' . osc_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . osc_href_link('zones.php', 'page=' . $_GET['page'] . '&cID=' . $cInfo->zone_id . '&action=delete') . '">' . osc_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
        $contents[] = array('text' => '<br />' . TEXT_INFO_ZONES_NAME . '<br />' . $cInfo->zone_name . ' (' . $cInfo->zone_code . ')');
        $contents[] = array('text' => '<br />' . TEXT_INFO_COUNTRY_NAME . ' ' . $cInfo->countries_name);
      }
*/
      break;
  }

  if ( (osc_not_null($heading)) && (osc_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require('includes/footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php
  require('includes/application_bottom.php');
