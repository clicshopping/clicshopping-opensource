<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE_1', 'Mon compte');
define('NAVBAR_TITLE_2', 'Carnet d\'adresses');

define('HEADING_TITLE', 'Carnet d\'adresses personnel');

define('HEADING_DESCRIPTION', 'Créer les adresses que vous souhaitez : votre résidence principale, votre lieu de travail, etc... Elle vous servira à chaque commande et vous évitera de ressaisir l\'ensemble de vos coordonnées.');

define('PRIMARY_ADDRESS_TITLE', 'Adresse par défaut');
define('PRIMARY_ADDRESS_DESCRIPTION', 'Cette adresse est utilisée par défaut concernant l\'expédition ou les commandes passées sur '.STORE_NAME.'.<br /><br />Elle est également utilisée concernant le calcul sur les taxes et les frais d\'envois.');

define('ADDRESS_BOOK_TITLE', 'Ajouter ou modifier des adresses');

define('PRIMARY_ADDRESS', '(Adresse par défaut)');

define('TEXT_MAXIMUM_ENTRIES', '<p style="color:#ff0000;"><strong>NOTE:</strong></font> Un maximum de %s adresses différentes vous est permis.</p>');
?>
