<?php
  /*
   * @copyright Copyright 2008 - http://www.e-imaginis.com
   * @copyright Portions Copyright 2003 osCommerce
   * @license GNU Public License V2.0
   * @version $Id:
  */

  $directories = array('/ClicShoppingAdmin/images/graphs',
                       '/ClicShoppingAdmin/cache/twitter',
                       '/ClicShoppingAdmin/cache/session',
                       '/ClicShoppingAdmin/cache/temp',
                       '/ClicShoppingAdmin/backups',
                       '/cache/geolocalisation/',
                       '/cache/public/',
                       '/cache/session/',
                       '/cache/work/',
                       '/sources/public/download/',
                       '/sources/public/newsletter/',
                       '/sources/image/barcode',
                       '/sources/image/',
                       '/sources/image/template',
                       '/sources/templates'
                       );
  $compat_register_globals = true;

  $languages_array = array(array('id' => 'english', 'text' => 'English'),
                           array('id' => 'french', 'text' => 'French')
                           );

require ('includes/languages/' . $language . '/' . basename($_SERVER['PHP_SELF']));

?>

<div class="contentBlock">
  <h1><?php echo TEXT_TITLE_WELCOME; ?></h1>
  <?php echo TEXT_INTRO_WELCOME; ?>
</div>

  <div  class="contentBlock">
    <h1><?php echo TEXT_SERVER_CARACTERISTICS; ?></h1>
    <div class="infoPaneContents">
      <div class="col-sm-5 pull-right">
        <table border="0" width="300" cellspacing="0" cellpadding="2">
          <tr>
            <th><?php echo TEXT_DIRECTORIES; ?></th>
            <th align="right" width="25"></th>
          </tr>
<?php
  foreach ($directories as $directory) {
?>
          <tr>
            <th><?php echo $directory; ?></th>
          </tr>
<?php
  }
?>
        </table>
     </div>
     <div class="col-sm-7">
       <table border="0" width="300" cellspacing="0" cellpadding="2">
          <tr>
            <th><strong><?php echo TEXT_PHP_VERSION; ?></strong></th>
            <th style="text-align:right;"><?php echo phpversion(); ?></th>
            <th style="text-align:right;" width="25"><?php echo ((phpversion() >= '5.4.4') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
            <th style="text-align:right;" width="25"></th>
          </tr>
      <br />

        <tr>
          <th colspan="2"><strong><?php echo TEXT_PHP_SETTINGS; ?></strong></th>
          <th style="text-align:right;"></th>
        </tr>

<?php
  if (function_exists('ini_get')) {
?>

        <tr>
          <th><?php echo TEXT_R_G; ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('register_globals') === 0) ? 'Off' : 'On'); ?></th>
          <th style="text-align:right;"><?php echo (($compat_register_globals == true) ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_M_Q; ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('magic_quotes') === 0) ? 'Off' : 'On'); ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('magic_quotes') == 0) ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_F_U; ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('file_uploads') === 0) ? 'Off' : 'On'); ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('file_uploads') == 1) ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_SA_S; ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('session.auto_start') === 0) ? 'Off' : 'On'); ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('session.auto_start') == 0) ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_SU_T_S; ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('session.use_trans_sid') === 0) ? 'Off' : 'On'); ?></th>
          <th style="text-align:right;"><?php echo (((int)ini_get('session.use_trans_sid') == 0) ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
      </table>



      <br />
       <table border="0" width="400" cellspacing="0" cellpadding="2">
        <tr>
          <th width="200">MySQL > version 5 <style="color:#FF0000;">*</th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;"><?php echo (extension_loaded('mysqli') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th>GD <style="color:#FF0000;">*</th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;">
<?php


$gd_info_alternate = 'function gd_info() {
$array = Array("GD Version" => "",
               "FreeType Support" => 0,
               "FreeType Support" => 0,
               "FreeType Linkage" => "",
               "T1Lib Support" => 0,
               "GIF Read Support" => 0,
               "GIF Create Support" => 0,
               "JPG Support" => 0,
               "PNG Support" => 0,
               "WBMP Support" => 0,
               "XBM Support" => 0
              );
       $gif_support = 0;

       ob_start();
       eval("phpinfo();");
       $info = ob_get_contents();
       ob_end_clean();

       foreach(explode("\n", $info) as $line) {
           if(strpos($line, "GD Version")!==false)
               $array["GD Version"] = trim(str_replace("GD Version", "", strip_tags($line)));
           if(strpos($line, "FreeType Support")!==false)
               $array["FreeType Support"] = trim(str_replace("FreeType Support", "", strip_tags($line)));
           if(strpos($line, "FreeType Linkage")!==false)
               $array["FreeType Linkage"] = trim(str_replace("FreeType Linkage", "", strip_tags($line)));
           if(strpos($line, "T1Lib Support")!==false)
               $array["T1Lib Support"] = trim(str_replace("T1Lib Support", "", strip_tags($line)));
           if(strpos($line, "GIF Read Support")!==false)
               $array["GIF Read Support"] = trim(str_replace("GIF Read Support", "", strip_tags($line)));
           if(strpos($line, "GIF Create Support")!==false)
               $array["GIF Create Support"] = trim(str_replace("GIF Create Support", "", strip_tags($line)));
           if(strpos($line, "GIF Support")!==false)
               $gif_support = trim(str_replace("GIF Support", "", strip_tags($line)));
           if(strpos($line, "JPG Support")!==false)
               $array["JPG Support"] = trim(str_replace("JPG Support", "", strip_tags($line)));
           if(strpos($line, "PNG Support")!==false)
               $array["PNG Support"] = trim(str_replace("PNG Support", "", strip_tags($line)));
           if(strpos($line, "WBMP Support")!==false)
               $array["WBMP Support"] = trim(str_replace("WBMP Support", "", strip_tags($line)));
           if(strpos($line, "XBM Support")!==false)
               $array["XBM Support"] = trim(str_replace("XBM Support", "", strip_tags($line)));
       }

       if($gif_support==="enabled") {
           $array["GIF Read Support"]  = 1;
           $array["GIF Create Support"] = 1;
       }

       if($array["FreeType Support"]==="enabled"){
           $array["FreeType Support"] = 1;    }

       if($array["T1Lib Support"]==="enabled")
           $array["T1Lib Support"] = 1;

       if($array["GIF Read Support"]==="enabled"){
           $array["GIF Read Support"] = 1;    }

       if($array["GIF Create Support"]==="enabled")
           $array["GIF Create Support"] = 1;

       if($array["JPG Support"]==="enabled")
           $array["JPG Support"] = 1;

       if($array["PNG Support"]==="enabled")
           $array["PNG Support"] = 1;

       if($array["WBMP Support"]==="enabled")
           $array["WBMP Support"] = 1;

       if($array["XBM Support"]==="enabled")
           $array["XBM Support"] = 1;

       return $array;

}';
    if (!extension_loaded('gd')) {
     echo '<style="color:#00CC00;"><strong><?php echo TEXT_LIBRAIRIES_NOT_CONFIGURED; ?></strong>';
    } else {
      if (!function_exists('gd_info')) {
        eval($gd_info_alternate);
      }
      $gd_info = gd_info();

      if (isset($gd_info['GD Version'])) {
        $gd_version = $gd_info['GD Version'];
        $gd_version=preg_replace('%[^0-9.]%', '', $gd_version);
        if ($gd_version > "2.0") {
         echo '<p style="color:#00CC00;"><strong>'.TEXT_ACCEPTED.'</strong><br />Version installée </p>'.$gd_version;
        } else {
        echo '<p style="color:#00CC00;"><strong>'.TEXT_NOT_ACCEPTED.'</strong><br />Version installée </p>'.$gd_version;
        }
      } else {
        echo '<style="color:#00CC00;"><strong>'. TEXT_LIBRAIRIES_NOT_WELL_CONFIGURED .'</strong>.<br />'.TEXT_LIBRAIRIES_NOT_WELL_CONFIGURED_CONTINUE;
      }



      if (function_exists('imageantialias')) {
        echo '<style="color:#00CC00;"><strong>'.TEXT_ANTIALIASING.'</strong><';
      } else {
        echo '<style="color:#00CC00;"><strong>'.TEXT_NO_ANTIALIASING.'</strong>';
      }
    }
?>
          </th>
        </tr>
        <tr>
          <th><?php echo TEXT_SOAP; ?></th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;"><?php echo (extension_loaded('soap') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_XML; ?><p style="color:#FF0000;">*</p></th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;"><?php echo (extension_loaded('xml') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_XML_RPC; ?></th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;"><?php echo (extension_loaded('xmlrpc') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_CURL; ?><p style="color:#FF0000;">*</p></th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;""><?php echo (extension_loaded('curl') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th><?php echo TEXT_OPENSSL; ?><p style="color:#FF0000;">*</p></th>
          <th style="text-align:right;"></th>
          <th style="text-align:right;"><?php echo (extension_loaded('openssl') ? '<i class="fa fa-thumbs-up text-success"></i>' : '<i class="fa fa-thumbs-down text-danger"></i>'); ?></th>
        </tr>
        <tr>
          <th style="color:#FF0000;">*</td>*</th>
          <th style="text-align:right;"></th>
          <th style="text-align:right; color:#FF0000;"><strong><?php echo TEXT_MANDATORY; ?></strong></th>
        </tr>
<?php
  }
?>
        </table>
      </div>
      <div class="clearfix"></div>

    </div>
  </div>
  <div><p></p></div>
  <div class="contentPane">

    <h2>Notes :</h2>

    <div class="alert alert-info"><?php echo TEXT_NOTICE; ?></div>
    <div><p></p></div>

    <h2><?php echo TEXT_NEW_INSTALLATION; ?></h2>


   <div>
<?php

  $configfile_array = array();

  if (file_exists(osc_realpath(__DIR__ . '/../../../includes') . '/configure.php') && !osc_is_writable(osc_realpath(__DIR__ . '/../../../includes') . '/configure.php')) {
    @chmod(osc_realpath(__DIR__ . '/../../../includes') . '/configure.php', 0777);
  }

  if (file_exists(osc_realpath(__DIR__ . '/../../../ClicShoppingAdmin/includes') . '/configure.php') && !osc_is_writable(osc_realpath(__DIR__ . '/../../../includes/ClicShoppingAdmin') . '/configure.php')) {
    @chmod(osc_realpath(__DIR__ . '/../../../ClicShoppingAdmin/includes') . '/configure.php', 0777);
  }

  if (file_exists(osc_realpath(__DIR__ . '/../../../includes') . '/configure.php') && !osc_is_writable(osc_realpath(__DIR__ . '/../../../includes') . '/configure.php')) {
    $configfile_array[] = osc_realpath(__DIR__ . '/../../../includes') . '/configure.php';
  }

  if (file_exists(osc_realpath(__DIR__ . '/../../../ClicShoppingAdmin/includes') . '/configure.php') && !osc_is_writable(osc_realpath(__DIR__ . '/../../../ClicShoppingAdmin/includes') . '/configure.php')) {
    $configfile_array[] = osc_realpath(__DIR__ . '/../../../ClicShoppingAdmin/includes') . '/configure.php';
  }

    $warning_array = array();

    if (function_exists('ini_get')) {
      if ($compat_register_globals == false) {
        $warning_array['register_globals'] = TEXT_REGISTER_GLOBAL;
      }
    }

    if (!extension_loaded('mysqli')) {
      $warning_array['mysqli'] = TEXT_MYSQL_EXTANSION;
    }

    if ((sizeof($configfile_array) > 0) || (sizeof($warning_array) > 0)) {
?>
      <div class="alert alert-warning" style="padding: 10px 10px 10px 10px">

<?php
      if (sizeof($warning_array) > 0) {
?>

        <table class="table table-condensed table-striped">

<?php
        foreach ( $warning_array as $key => $value ) {
          echo '        <tr>' . "\n" .
               '          <th valign="top"><strong>' . $key . '</strong></th>' . "\n" .
               '          <th valign="top">' . $value . '</th>' . "\n" .
               '        </tr>' . "\n";
        }
?>
        </table>
<?php
      }
      if (sizeof($configfile_array) > 0) {
?>

      <?php echo TEXT_NOT_SAVE_PARAMETERS; ?>
      <blockquote>
<?php
          for ($i=0, $n=sizeof($configfile_array); $i<$n; $i++) {
            echo $configfile_array[$i];

            if (isset($configfile_array[$i+1])) {
              echo '<br />';
            }
          }
?>
      </blockquote>
<?php
      echo TEXT_INFO_CUSTOMER;
      }
?>
     </div>

<?php
    }

    if ((sizeof($configfile_array) > 0) || (sizeof($warning_array) > 0)) {
?>
      <div style="padding-top:10px;"></div>
      <div class="alert alert-danger">
        <blockquote>Veuillez corriger les erreurs suivantes avant de continuer la proc&eacute;dure d'installation.</blockquote>
      </div>
<?php
    if (sizeof($warning_array) > 0) {
      echo '    <div class="alert alert-info"><i>';
      echo '    <p><i>Si des changements doivent &ecirc;tre effectu&eacute;s sur le serveur, veuillez penser &agrave; le red&eacute;marrer et &agrave; relancer l\'installation de ClicShopping.</i></p>' . "\n";
      echo '</i></div>' . "\n";
   }
?>
      </div>
        <p><a href="index.php?step=2" class="btn btn-danger btn-block" role="button">Retry</a></p>

<?php
  } else {
    echo TEXT_INFO_SERVER_OK;
?>


      <div id="jsOn" style="display: none;">
        <p><a href="index.php?step=3" class="btn btn-success btn-block" role="button">Start the installation procedure</a></p>
      </div>

      <div id="jsOff">
        <p class="text-danger">Please enable Javascript in your browser to be able to start the installation procedure.</p>
        <p><a href="index.php" class="btn btn-danger btn-block" role="button">Retry</a></p>
      </div>

<script>
$(function() {
  $('#jsOff').hide();
  $('#jsOn').show();
});
</script>

<?php
  }
?>
  </div>
