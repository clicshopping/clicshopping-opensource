<?php
/*
 * account_edit.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
  require($OSCOM_Template->getTemplateHeaderFooter('header'));

  if ( $OSCOM_MessageStack->exists('account_edit') ) {
   echo $OSCOM_MessageStack->get('account_edit');
  }

  require($OSCOM_Template->getTemplateFiles('breadcrumb'));
  echo osc_draw_form('account_edit', osc_href_link('account_edit.php', '', 'SSL'), 'post', '', true) . osc_draw_hidden_field('action', 'process');
// ---------------------- 
// ------ Contact   -----  
// ---------------------- 
?>
<div class="contentContainer">
 <div class="contentText">
<?php
  if ($OSCOM_Customer->getCustomersGroupID() != 0) {
?>
    <div class="contentText">
      <legend>
        <p class="text-warning" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></p>
        <h3<?php echo CATEGORY_PERSONAL_PRO; ?></h3>
      </legend>
    </div>
<?php
  } else {
?>
    <div class="row">
      <legend>
        <p class="text-warning" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></p>
        <h3<?php echo MY_ACCOUNT_TITLE; ?></h3>
      </legend>
    </div>

<?php
  }
?>
  </div>
  <div class="contentText">
<?php
  if (((ACCOUNT_GENDER == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_GENDER_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
    if (isset($gender)) {
      $male = ($gender == 'm') ? true : false;
    } else {
      $male = ($account['customers_gender'] == 'm') ? true : false;
    }
    $female = !$male;
?>
    <div class="row">
      <div class="form-group has-feedback">
        <label class="control-label col-sm-5" for="gender"><?php echo ENTRY_GENDER; ?></label>
        <div class="col-sm-7">
          <label class="radio-inline">
            <?php echo osc_draw_radio_field('gender', 'm', $male, 'required aria-required="true"') . ' ' . MALE; ?>
          </label>
          <label class="radio-inline">
            <?php echo osc_draw_radio_field('gender', 'f', $female) . ' ' . FEMALE; ?>
          </label>
        </div>
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      </div>
    </div>

  <?php
  }
?>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-3" for="firstname"><?php echo ENTRY_FIRST_NAME; ?></label>
          <div class="col-sm-8">
<?php
  echo osc_draw_input_field('firstname', $account['customers_firstname'], 'id="firstname" required aria-required="true" placeholder="' . ENTRY_FIRST_NAME . '"');
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_FIRST_NAME_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_FIRST_NAME_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="text">' . ENTRY_FIRST_NAME_TEXT . '</span>': '');
  }
?>
          </div>
        </div>
      </div>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-3" for="lastname"><?php echo ENTRY_LAST_NAME; ?></label>
          <div class="col-sm-8">
<?php
  echo osc_draw_input_field('lastname', $account['customers_lastname'], 'id="lastname" "required aria-required="true" placeholder="' . ENTRY_LAST_NAME . '"');
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_LAST_NAME_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_LAST_NAME_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="text">' . ENTRY_LAST_NAME_TEXT . '</span>': '');
  }
?>
          </div>
        </div>
      </div>

<?php
  if (((ACCOUNT_DOB == 'true') && ($OSCOM_Customer->getCustomersGroupID() == 0)) || ((ACCOUNT_DOB_PRO == 'true') && ($OSCOM_Customer->getCustomersGroupID() != 0))) {
?>
     <link rel="stylesheet" type="text/css" href="<?php echo HTTP_SERVER .  DIR_WS_CATALOG . DIR_WS_SOURCES . 'javascript/bootstrap/less/datepicker.less';?>" />
     <script type="text/javascript" src="<?php echo HTTP_SERVER .  DIR_WS_CATALOG .  DIR_WS_SOURCES .'javascript/bootstrap/js/bootstrap-datepicker.js';?>" /></script>
     <script src="<?php echo HTTP_SERVER .  DIR_WS_CATALOG .  DIR_WS_SOURCES .'javascript/clicshopping/account_edit.js';?>" /></script>

    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group has-feedback">
        <label class="control-label col-sm-3" for="dob"><?php echo ENTRY_DATE_OF_BIRTH; ?></label>
        <div class="col-sm-8">
          <?php echo osc_draw_input_field('dob', $OSCOM_Date->getShort($account['customers_dob']), 'id="dob"') . '&nbsp;' . (osc_not_null(ENTRY_DATE_OF_BIRTH_TEXT) ? '<span class="text-warning">' . ENTRY_DATE_OF_BIRTH_TEXT . '</span>': ''); ?>
        </div>
      </div>
    </div>
<?php
  }
?>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-3" for="email_address"><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
          <div class="col-sm-8">
            <?php echo osc_draw_input_field('email_address', $account['customers_email_address'], 'id="email_address" required aria-required="true" placeholder="' . ENTRY_EMAIL_ADDRESS . '"', 'email') . (osc_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '&nbsp;<span class="text-warning">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group has-feedback">
          <label class="control-label col-sm-3" for="customers_telephone"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label>
          <div class="col-sm-8">
<?php
  echo osc_draw_input_field('telephone', $account['customers_telephone'], 'id="customers_telephone" required aria-required="true" id="inputTelephone" placeholder="' . ENTRY_TELEPHONE_NUMBER . '"', 'tel');
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ENTRY_TELEPHONE_MIN_LENGTH > 0)) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ENTRY_TELEPHONE_PRO_MIN_LENGTH > 0))) {
    echo '&nbsp;' . (osc_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="text-warning">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': '');
  }
?>
              </div>
        </div>
      </div>

<?php
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ACCOUNT_CELLULAR_PHONE =='true')) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ACCOUNT_CELLULAR_PHONE_PRO =='true'))) {
    ?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="cellular_phone"><?php echo ENTRY_CELLULAR_PHONE_NUMBER; ?></label>
        <div class="col-sm-8">
          <?php echo osc_draw_input_field('cellular_phone', $account['customers_cellular_phone'], 'id="customers_cellular_phone" placeholder="' . ENTRY_CELLULAR_PHONE_NUMBER . '"', 'tel') . '&nbsp;' . (osc_not_null(ENTRY_CELLULAR_PHONE_NUMBER_TEXT) ? '<span class="text-warning">' . ENTRY_CELLULAR_PHONE_NUMBER_TEXT . '</span>': ''); ?>
        </div>
      </div>
    </div>
  <?php
  }
  if ((($OSCOM_Customer->getCustomersGroupID() == 0) && (ACCOUNT_FAX =='true')) || (($OSCOM_Customer->getCustomersGroupID() != 0) && (ACCOUNT_FAX_PRO =='true'))) {
    ?>
    <div class="row">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="form-group">
        <label class="control-label col-sm-3" for="customers_fax"><?php echo ENTRY_FAX_NUMBER; ?></label>
        <div class="col-sm-8">
          <?php echo osc_draw_input_field('fax', $account['customers_fax'], 'id="customers_fax" placeholder="' . ENTRY_FAX_NUMBER . '"', 'tel') . '&nbsp;' . (osc_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="text-warning">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?>
        </div>
      </div>
    </div>
  <?php
  }
?>
  </div>
<?php
// ----------------------
// ----- Company   -----
// ----------------------
?>
  <div class="contentText">
    <table border="0" cellspacing="2" cellpadding="2" width="100%">
<?php
  if ( $OSCOM_MessageStack->exists('account_edit') ) {

?>
    <tr>
      <td><?php echo $OSCOM_MessageStack->get('account_edit');; ?></td>
    </tr>

<?php
  }
  if ($OSCOM_Customer->getCustomersGroupID() != 0) {
?>


    <h2><?php echo MY_ACCOUNT_TITLE; ?></h2>

<?php
    if (ACCOUNT_COMPANY_PRO == 'true') {
      if (osc_count_customers_modify_company() == 1) {
        $input_field_option = 'maxlength="' .  	ENTRY_COMPANY_PRO_MAX_LENGTH . '" placeholder="' . ENTRY_COMPANY . '" id="entry_company"';
      } else {
        $input_field_option = 'readonly="readonly"';
      }
?>

      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="entry_company"><?php echo ENTRY_COMPANY; ?></label>
          <div class="col-sm-8">
<?php
              echo osc_draw_input_field('company', $account['customers_company'], $input_field_option);
              if ((osc_count_customers_modify_company() == 1) && (ENTRY_COMPANY_PRO_MIN_LENGTH > 0)) {
                echo '&nbsp;' . (osc_not_null(ENTRY_COMPANY_TEXT_PRO) ? '<span class="text-warning">' . ENTRY_COMPANY_TEXT_PRO . '</span>': '');
              }
?>
          </div>
        </div>
      </div>
<?php
    }
    if (ACCOUNT_SIRET_PRO == 'true') {
      if (osc_count_customers_modify_company() == 1) {
        $input_field_option = 'maxlength="' . ENTRY_SIRET_MAX_LENGTH . '" placeholder="' . ENTRY_SIRET . '" id="entry_siret"';
      } else {
        $input_field_option = 'readonly="readonly"';
      }
?>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group">
          <label class="control-labelcol-sm-3" for="entry_siret"><?php echo ENTRY_SIRET; ?></label>
          <div class="col-sm-8">
            <?php
              echo osc_draw_input_field('siret', $account['customers_siret'], $input_field_option);
              if ((osc_count_customers_modify_company() == 1) && (ENTRY_SIRET_MIN_LENGTH > 0)) {
                echo '&nbsp;' . (osc_not_null(ENTRY_SIRET_TEXT) ? '<span class="text-warning">' . ENTRY_SIRET_TEXT . '</span>': '');
              }
              echo '&nbsp;' . (osc_not_null(ENTRY_SIRET_EXEMPLE) ? '<span class="text-warning">' . ENTRY_SIRET_EXEMPLE . '</span>': '');
            ?>
          </div>
        </div>
      </div>

<?php
    }
    if (ACCOUNT_APE_PRO == 'true') {
      if (osc_count_customers_modify_company() == 1) {
        $input_field_option = 'maxlength="' .  ENTRY_CODE_APE_MAX_LENGTH . '" placeholder="' . ENTRY_CODE_APE . '" id="entry_code_ape"';
      } else {
        $input_field_option = 'readonly="readonly"';
      }
?>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="entry_code_ape"><?php echo ENTRY_CODE_APE; ?></label>
          <div class="col-sm-8">
 <?php
  echo osc_draw_input_field('ape', $account['customers_ape'], $input_field_option);
  if ((osc_count_customers_modify_company() == 1) && (ENTRY_CODE_APE_MIN_LENGTH > 0)) {
    echo '&nbsp;' . (osc_not_null(ENTRY_CODE_APE_TEXT) ? '<span class="text-warning">' . ENTRY_CODE_APE_TEXT . '</span>': '');
  }
  echo '&nbsp;' . (osc_not_null(ENTRY_CODE_APE_EXEMPLE) ? '<span class="text-warning">' . ENTRY_CODE_APE_EXEMPLE . '</span>': '');
?>
          </div>
        </div>
      </div>

<?php
    }
    if (ACCOUNT_TVA_INTRACOM_PRO == 'true') {
      if (osc_count_customers_modify_company() == 1) {
        $input_field_option = 'maxlength="' . ENTRY_TVA_INTRACOM_MAX_LENGTH . '" placeholder="' . ACCOUNT_TVA_INTRACOM_PRO . '" id="entry_tva_intracom"';
      } else {
        $input_field_option = 'readonly="readonly"';
      }
      if (osc_count_customers_modify_company() == 1) {
?>
        <div class="row">
          <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
          <div class="form-group">
            <label class="control-label col-sm-3" for="entry_tva_intracom"><?php echo ENTRY_COUNTRY; ?></label>
            <div class="col-sm-8">
              <?php echo osc_get_iso_list('country', $account['customers_tva_intracom_code_iso'], 'onchange="ISO_account_edit();"') . '&nbsp;' . (osc_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="text-warning">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?>
            </div>
          </div>
        </div>

<?php
      }
?>
      <div class="row">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="entry_tva_intracom"><?php echo ENTRY_TVA_INTRACOM; ?></label>
          <div class="col-sm-8">
            <input type="text" size="2" maxlength="2" name="ISO" readonly="readonly" onFocus="setTimeout('document.country.ISO.blur()',1);" value="<?php echo $account['customers_tva_intracom_code_iso']; ?>" style ="bottom:auto; background-color:#fff; border: #fff;">&nbsp;
<?php
  echo osc_draw_input_field('tva_intracom', $account['customers_tva_intracom'], $input_field_option);
  if (osc_count_customers_modify_company() == 1) {
    echo '&nbsp;' . (osc_not_null(ENTRY_TVA_INTRACOM_TEXT) ? '<span class="text-warning">' . ENTRY_TVA_INTRACOM_TEXT . '</span>': '');
  }
?>
          </div>
        </div>
      </div>
<?php
    }
  }
?>
   </table>
  </div>
    <div class="buttonSet" align="right">
      <table border="0" width="100%" cellspacing="2" cellpadding="2">
        <tr>
         <td><?php echo osc_draw_button(IMAGE_BUTTON_BACK, null, osc_href_link('account.php', '', 'SSL'), 'primary');  ?></td>
         <td align="right"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null);  ?></td>
       </tr>
     </table>
  </div>
</div>
</form>
<?php  require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>