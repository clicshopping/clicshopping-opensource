<?php
/*
 * xml_rpc_admin_customers.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id:
*/

// update newsletter
  if ($customers_newsletter == 1) $customers_newsletter = 0;

// **********************************
// search iso code ClicShopping
// **********************************

    $QcountryIdCustomer = $OSCOM_PDO->prepare("select entry_country_id
                                             from :table_address_book
                                             where customers_id = :customers_id
                                            ");
    $QcountryIdCustomer->bindInt(':customers_id', (int)$customers_id);
    $QcountryIdCustomer->execute();

    $country_id_customer = $QcountryIdCustomer->fetch();

    $country_id_customer = $country_id_customer['entry_country_id'];


    $QcountryCode = $OSCOM_PDO->prepare("select countries_iso_code_2
                                         from :table_countries
                                         where countries_id = :countries_id
                                        ");
    $QcountryCode->bindInt(':countries_id',(int)$country_id_customer);
    $QcountryCode->execute();

    $Qcountry_code = $QcountryCode->fetch();
    $country_code = $Qcountry_code['countries_iso_code_2'];

// **********************************
// search id country odoo
// **********************************
    $ids = $OSCOM_ODOO->odooSearch('code', '=', $country_code, 'res.country');

// **********************************
// read id country odoo
// **********************************
      $field_list = array('country_id',
                          'name'
      );

    $country_id_odoo = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.country');
    $country_id_odoo = $country_id_odoo[0][id];


// **********************************
// Search odoo customer id
// **********************************
    $ids = $OSCOM_ODOO->odooSearch('ref', '=', 'WebStore - ' . $customers_id, 'res.partner');

// **********************************
// read id customer odoo
// **********************************
    $field_list = array('ref',
                        'id'
                        );

   $id_odoo_customer_array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'res.partner');
   $id_odoo_customer = $id_odoo_customer_array[0][id];

// **********************************
// Search odoo customer group id
// **********************************
  $ids = $OSCOM_ODOO->odooSearch('clicshopping_customers_group_id', '=', $customers_group_id, 'clicshopping.customers.group', 'int');

// **********************************
// read id customer odoo
// **********************************
  $field_list = array('id');

  $id_odoo_customer_group__array = $OSCOM_ODOO->readOdoo($ids, $field_list, 'clicshopping.customers.group');
  $id_odoo_customer_group = $id_odoo_customer_group__array[0][id];


  if  (empty($id_odoo_customer)) {

// **********************************
// Create Customer if doesn't exist in oddo
// **********************************
      $values = array(
                    "ref" => new xmlrpcval('WebStore - ' . $customers_id, "string"),
                    "phone" => new xmlrpcval($customers_telephone,"string"),
                    "mobile" => new xmlrpcval($customers_cellular_phone,"string"),
                    "fax" => new xmlrpcval($customers_fax,"string"),
                    "street" => new xmlrpcval($entry_street_address,"string"),
                    "street2"=> new xmlrpcval($entry_suburb,"string"),
                    "zip" => new xmlrpcval($entry_postcode,"string"),
                    "city" => new xmlrpcval($entry_city,"string"),
                    "comment" => new xmlrpcval('Website Registration - Admin Creation', "string"),
                    "name"    => new xmlrpcval( $customers_lastname . ' ' . $customers_firstname, "string"),
                    "email"  => new xmlrpcval($customers_email_address, "string"),
                    "country_id" => new xmlrpcval($country_id_odoo, "int"),
                    "tz"      => new xmlrpcval("Europe/Paris", "string"),
                    "clicshopping_customers_id" => new xmlrpcval($customers_id, "int"),
                    "clicshopping_partner_customer_group_id"  => new xmlrpcval($id_odoo_customer_group, "int"),
                   );

      $OSCOM_ODOO->createOdoo($values, "res.partner");

   } else {

// **********************************
// update Customer if exist
// **********************************

     $values = array(
                     "name"    => new xmlrpcval( $customers_lastname . ' ' . $customers_firstname, "string"),
                     "email"  => new xmlrpcval($customers_email_address, "string"),
                     'phone' => new xmlrpcval($customers_telephone,"string"),
                     'mobile' => new xmlrpcval($customers_cellular_phone,"string"),
                     'fax' => new xmlrpcval($customers_fax,"string"),
                     'street' => new xmlrpcval($entry_street_address,"string"),
                     'street2'=>new xmlrpcval($entry_suburb,"string"),
                     'fax' => new xmlrpcval($customers_fax,"string"),
                     'zip' => new xmlrpcval($entry_postcode,"string"),
                     'city' => new xmlrpcval($entry_city,"string"),
                     'country_id' => new xmlrpcval($country_id_odoo,"int"),
                     "ref" => new xmlrpcval('WebStore - ' . $customers_id, "string"),
                     "clicshopping_customers_id" => new xmlrpcval($customers_id, "int"),
                     "clicshopping_partner_customer_group_id"  => new xmlrpcval($id_odoo_customer_group, "int"),
                    );

     $OSCOM_ODOO->updateOdoo($id_odoo_customer, $values, 'res.partner');
   }
?>