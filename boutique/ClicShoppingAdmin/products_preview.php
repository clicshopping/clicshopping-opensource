<?php
/*
 * products_preview.php 
 * @copyright Copyright 2010 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  require('includes/application_top.php');

      $Qproducts = $OSCOM_PDO->prepare("select pd.*, 
                                              p.*
                                        from :table_products p, 
                                            :table_products_description pd 
                                         where p.products_id = :products_id
                                         and pd.language_id = :language_id
                                         and p.products_id = pd.products_id
                                       ");
      $Qproducts->bindInt(':products_id', (int)$_GET['pID'] );
      $Qproducts->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $Qproducts->execute();

      $products = $Qproducts->fetch();

      $QproductQtyUnit = $OSCOM_PDO->prepare("select p.products_quantity_unit_id, 
                                                    pqt.products_quantity_unit_title
                                             from :table_products p, 
                                                  :table_products_quantity_unit pqt 
                                             where p.products_id = :products_id
                                             and pqt.language_id = :language_id
                                             AND pqt.products_quantity_unit_id = p.products_quantity_unit_id
                                       ");
      $QproductQtyUnit->bindInt(':products_id', (int)$_GET['pID'] );
      $QproductQtyUnit->bindInt(':language_id', (int)$_SESSION['languages_id'] );
      $QproductQtyUnit->execute();

      $product_qty_unit = $QproductQtyUnit->fetch();

      $Qmanufacturer = $OSCOM_PDO->prepare("select m.manufacturers_id, 
                                                      m.manufacturers_name,
                                                      p.products_id
                                             from :table_products p, 
                                                  :table_manufacturers m
                                             where p.products_id = :products_id
                                             and m.manufacturers_id = p.manufacturers_id
                                       ");
      $Qmanufacturer->bindInt(':products_id', (int)$_GET['pID'] );
      $Qmanufacturer->execute();

      $manufacturer = $Qmanufacturer->fetch();

      $Qsupplier = $OSCOM_PDO->prepare("select s.suppliers_id, 
                                                s.suppliers_name,
                                                p.products_id
                                       from :table_products p, 
                                            :table_suppliers s
                                       where p.products_id = :products_id
                                       and p.suppliers_id = s.suppliers_id
                                       ");
      $Qsupplier->bindInt(':products_id', (int)$_GET['pID'] );
      $Qsupplier->execute();

      $supplier = $Qsupplier->fetch();

// Activation du module B2B
      if  (MODE_B2B_B2C == 'true') {
//inserer les informations concernant la B2B

        $QcustomersGroup = $OSCOM_PDO->prepare('select distinct customers_group_id,
                                                               customers_group_name,
                                                               customers_group_discount
                                               from :table_customers_groups
                                               where customers_group_id >  0
                                               order by customers_group_id
                                              ');

        $QcustomersGroup->execute();

      }        
 
  require('includes/header.php');

  if (!osc_not_null($_GET['pID'])) {
?>
<table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td class="pageHeading" align="center" valign="center" height="300"><?php echo TEXT_NO_PRODUCTS; ?></td>	
  </tr>
</table>
<?php
   exit;
  } 
?>

  <!-- body //-->
  <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
  <!-- body_text //-->
      <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
        <table border="0" width="100%" cellspacing="3" cellpadding="0" class="adminTitle">
          <tr>
            <td width="40"><?php echo osc_image(DIR_WS_IMAGES . 'categories/produit_preview.gif', HEADING_TITLE, '40', '40'); ?></td>
            <td class="pageHeading"><?php echo '&nbsp;' . HEADING_TITLE; ?></td>
            <td align="right"></td>
          </tr>
        </table>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><table border="0" width="90%" cellspacing="0" cellpadding="0" align="center">
              <tr>
                <td><?php echo osc_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <td><table border="0" width="70%" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td class="pageHeading"><?php echo TEXT_PRODUCTS_NAME  . $products['products_name']; ?></td>
                  <td align="right" class="Text" width="150"><strong><?php echo TEXT_PRODUCTS_MODEL  . $products['products_model']; ?></strong></td>
                </tr>
                <tr>
                  <td colspan="2"><?php echo osc_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
                </tr>
                <tr>
                 <td class="main" colspan="2"><table border="0" cellspacing="0" cellpadding="2" align="right">
                   <tr>
                     <td align="center" class="smallText">
<?php  
// ----------------------------------------------------------------//
//                      PopUp Image                                //
// ----------------------------------------------------------------//
  if (osc_not_null($products['products_image'])) { 	
    echo osc_image(DIR_WS_CATALOG_IMAGES . $products['products_image'], $products['products_name'], 'hspace="5" vspace="5"') . '<br />';
  }
?>
                     </td>
                   </tr>
                 </table>
                 <p><?php echo $products['products_description']; ?></p>
                 </td>
               </tr>
               <tr>
                 <td><?php echo osc_draw_separator('pixel_trans.gif', '100%', '30'); ?></td>
               </tr>
             </table></td>
            </tr>
<?ph
// ----------------------------------------------------------------//
// affichage presentation produit                               //
// ----------------------------------------------------------------//
?>
            <tr>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo TEXT_PRODUCTS_PRESENTATION; ?></td>
                    </tr>
                  </table>
            </tr>
                <tr>
                      <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2" class="adminformTitle">
                    <tr>
                      <td class="main" width="300"><?php echo TEXT_PRODUCTS_MODEL; ?></td>
                      <td class="main" width="20%" ><?php echo $products['products_model']; ?></td>
                      <td class="main" width="300"><?php echo TEXT_PRODUCTS_EAN; ?></td>
                      <td width="386" class="main"><?php echo $products['products_ean']; ?></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo TEXT_PRODUCTS_WEIGHT; ?></td>
                      <td class="main"><?php echo  $products['products_weight']; ?></td>
                      <td class="main"><?php echo TEXT_PRODUCTS_SKU; ?></td>
                      <td class="main"><?php echo $products['products_sku']; ?></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo TEXT_PRODUCTS_WEIGHT_POUNDS; ?></td>
                      <td class="main"><?php echo  $products['products_weight_pounds']; ?></td>
                      <td class="main"></td>
                      <td class="main"></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo TEXT_PRODUCTS_VOLUME ; ?></td>
                      <td class="main"><?php echo $products['products_volume']; ?></td>
                      <td class="main"><?php echo TEXT_PRODUCTS_DIMENSION; ?></td>
                      <td class="main"><?php echo $products['products_dimension_width'] .' x ' .$products['products_dimension_height'] . ' x ' . $products['products_dimension_depth'] . ' ' .  $products['products_dimension_type']; ?></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo TEXT_PRODUCTS_URL; ?></td>
                      <td class="main"><?php echo $products['products_url']; ?></td>
<?php
 if ($products['products_only_online'] == '1') $check_products_only_online = 'true';
?>
                      <td class="main"><?php echo TEXT_PRODUCTS_ONLY_ONLINE; ?></td>
                        <td class="main"><?php echo osc_draw_checkbox_field('products_only_online', '', $check_products_only_online); ?>&nbsp;</td>
                      <td class="main"></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo TEXT_PRODUCTS_MANUFACTURER ; ?></td>
                      <td class="main"><?php echo $manufacturer['manufacturers_name']; ?></td>
                      <td class="main"><?php echo TEXT_PRODUCTS_SUPPLIERS; ?></td>
                      <td class="main"><?php echo $supplier['suppliers_name'];?></td>
                    </tr>
                    <tr>
                      <td class="main"><?php echo TEXT_PRODUCTS_WHAREHOUSE_PACKAGING ; ?></td>
<?php
  if ($products['products_packaging'] == 0) $products_packaging = '';
  if ($products['products_packaging'] == 1) $products_packaging = TEXT_PRODUCTS_PACKAGING_NEW;
  if ($products['products_packaging'] == 2) $products_packaging = TEXT_PRODUCTS_PACKAGING_REPACKAGED;
  if ($products['products_packaging'] == 3) $products_packaging = TEXT_PRODUCTS_PACKAGING_USED;
?>
                      <td class="main"><?php echo $products_packaging; ?></td>
                      <td class="main"></td>
                      <td class="main"></td>
                    </tr>
                  </table></td>
              </tr>
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>

<?php
// ----------------------------------------------------------------//
// affichage STOCK produit                               //
// ----------------------------------------------------------------//
?>
            <tr>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo TEXT_PRODUCTS_STOCK; ?></td>
                    </tr>
                  </table>
            </tr>
                <tr>
                      <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2" class="adminformTitle">					
                    <tr>
                        <td class="main" width="300"><?php echo TEXT_PRODUCTS_STATUS; ?></td>
<?php
  if ($products['products_status'] == '1') {
    $products_status = TEXT_PRODUCT_AVAILABLE;
  }	else {
    $products_status =  TEXT_PRODUCT_NOT_AVAILABLE;
  } 
?>                     
                        <td class="main" width="20%" align="left"><?php echo $products_status; ?></td>
                        <td class="main" width="410"><?php echo TEXT_PRODUCTS_QUANTITY_UNIT; ?></td>
                        <td width="383" align="left" class="main"><?php echo  $product_qty_unit['products_quantity_unit_title']; ?></td>
                      </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_QUANTITY; ?></td>
                        <td class="main" align="left"  width="20%"><?php echo  $products['products_quantity']; ?></td>
                        <td class="main"><?php echo TEXT_PRODUCTS_MIN_ORDER_QUANTITY; ?></td>
                        <td class="main" align="left"><?php echo  $products['products_min_qty_order']; ?></td>
                      </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_DATE_AVAILABLE; ?> <small>(YYYY-MM-DD)</small></td>
                        <td class="main" align="left"><?php echo $products['products_date_available']; ?></td>
                        <td class="main"><?php echo TEXT_PRODUCTS_SHIPPING_DELAY; ?></td>
                        <td class="main" align="left"><?php echo $products['products_shipping_delay']; ?></td>
                    </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_WHAREHOUSE; ?></td>
                        <td class="main" align="left"><?php echo $products['products_wharehouse']; ?></td>
                        <td class="main"><?php echo TEXT_PRODUCTS_TIME_REPLENISHMENT; ?></td>
                        <td class="main" align="left"><?php echo $products['products_wharehouse_time_replenishment']; ?></td>
                    </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_WHAREHOUSE_ROW; ?></td>
                        <td class="main" align="left"><?php echo $products['products_wharehouse_row']; ?></td>
                        <td class="main"></td>
                        <td class="main"></td>
                    </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_WHAREHOUSE_LEVEL_LOCATION; ?></td>
                        <td class="main" align="left"><?php echo $products['products_wharehouse_level_location']; ?></td>
                        <td class="main"></td>
                        <td class="main"></td>
                    </tr>

                  </table></td>
          </tr>
<?php
// ----------------------------------------------------------------//
// affichage prix produit                               //
// ----------------------------------------------------------------//
?>
                  <tr>
                 <table width="100%" border="0" cellspacing="0" cellpadding="5">
                   <tr>
                     <td class="mainTitle"><?php echo TEXT_PRODUCTS_PRICE_PUBLIC; ?></td>
                   </tr>
                 </table>
                    </tr>
                    <tr>
                     <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="5" class="adminformTitle">                    
                    <tr bgcolor="#ebebff">
                     <td><table border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td class="main"><?php //echo TEXT_PRODUCTS_TAX_CLASS; ?></td>
                        <td class="main"></td>
                      </tr>
                      <tr>
                        <td class="main"><?php echo TEXT_PRODUCTS_PRICE; ?></td>
                        <td class="main"><?php echo $products['products_price'] . ' <strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong>'; ?></td>
                        <td width="50"></td>
                        <td class="main"><?php echo TEXT_PRODUCTS_COST; ?></td>
                        <td class="main"><?php echo $products['products_cost'] . ' <strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong>'; ?></td>
                        <td width="50"></td>
                        <td class="main"><?php echo TEXT_PRODUCTS_HANDLING; ?></td>
                        <td class="main"><?php echo $products['products_handling'] . ' <strong>' . TEXT_PRODUCTS_PRICE_NET . '</strong>'; ?></td>
                        </tr>
<?php
// Activation du module B2B
  if  (MODE_B2B_B2C == 'true') {
//inserer les informations concernant la B2B
    while ($customers_group = $QcustomersGroup->fetch()) {

      if ($customers_group->rowCount() > 0) {

        $Qattributes= $OSCOM_PDO->prepare('select g.customers_group_id,
                                                 g.customers_group_price,
                                                 g.price_group_view,
                                                 g.products_group_view,
                                                 g.orders_group_view,
                                                 p.products_price,
                                                 p.products_id
                                          from :table_products_groups g,
                                               :table_products p
                                          where p.products_id = :products_id
                                          and p.products_id = g.products_id
                                          and g.customers_group_id = :customers_group_id
                                          order by g.customers_group_id
                                          ');
        $Qattributes->bindInt(':products_id', (int)$_GET['pID'] );
        $Qattributes->bindInt(':customers_group_id', (int)$customers_group['customers_group_id'] );

        $Qattributes->execute();

      }

?>
                        <tr>
                          <td class="main"><?php echo $customers_group['customers_group_name']; ?>&nbsp;:&nbsp;</td>
                          <td class="main">
<?php
      if ($attributes = $Qattributes->fetch() ) {
      echo $attributes['customers_group_price'] .' <strong>' . TAX_EXCLUDED . '</strong>';
    } else {
      echo $attributes['customers_group_price'] . ' <strong>' . TAX_EXCLUDED . '</strong>';
    }
?>
                          </td>
                        </tr>
<?php
   } // end while	  
?>
  <!-- Afficher autoriser du produit + autorisation commande //-->
                        <tr>
                          <td class="main" valign="top"><?php echo TEXT_PRODUCTS_VIEW; ?></td>
<?php
    if (isset($_GET['pID'])) {
// Si c'est un nouveau produit case coche par defaut

 if ($products['products_view'] == '1') $check_product_view = 'true';
 if ($products['orders_view'] == '1') $check_product_order_view = 'true';
 if ($products['products_price_kilo'] == '1') $check_products_price_kilo = 'true';
?>
                        <td class="main"><?php echo osc_draw_checkbox_field('products_view', '', $check_product_view) . osc_image (DIR_WS_IMAGES . 'icons/last.png', TAB_PRODUCTS_VIEW) . '&nbsp;&nbsp;' . osc_draw_checkbox_field('$product_order_view', '', $check_product_order_view)  . osc_image (DIR_WS_IMAGES . 'icons/orders-up.gif', TAB_ORDERS_VIEW); ?>&nbsp;</td>
<?php
    } else {
?>
                        <td class="main"><?php echo osc_draw_checkbox_field('products_view', '', $check_product_view) . osc_image (DIR_WS_IMAGES . 'icons/last.png', TAB_PRODUCTS_VIEW) . '&nbsp;&nbsp;' . osc_draw_checkbox_field('$product_order_view', '', $check_product_order_view) . osc_image (DIR_WS_IMAGES . 'icons/orders-up.gif', TAB_ORDERS_VIEW); ?>&nbsp;</td>
<?php
    }
?>
                </tr>
<?php
  }
?>
                  <tr>
                    <td class="main"><?php echo TEXT_PRODUCTS_PRICE_KILO; ?></td>
                    <td class="main"><?php echo osc_draw_checkbox_field('products_view', '', $check_products_price_kilo); ?></td>
                  </tr>
                    </table></td>
                   </tr>
                </table></td>
<?php
// ----------------------------------------------------------------//
// affichage referencement                            //
// ----------------------------------------------------------------//
?>
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                  <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <td class="mainTitle"><?php echo TEXT_PRODUCTS_PAGE_REFEFRENCEMENT; ?></td>
                    </tr>
                  </table>
                </tr>
                <tr>
                    <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2" class="adminformTitle">
                  <tr>
                    <td class="main" width="350"><?php echo TEXT_PRODUCTS_PAGE_TITLE; ?></td>
                    <td class="main"><?php echo $products['products_head_title_tag']; ?></td>
                    <td class="main"></td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo TEXT_PRODUCTS_HEADER_DESCRIPTION; ?></td>
                    <td class="main"><?php echo $products['products_head_desc_tag']; ?></td>
                    <td class="main"></td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo TEXT_PRODUCTS_KEYWORDS ; ?></td>
                    <td class="main"><?php echo $products['products_head_keywords_tag']; ?></td>
                    <td class="main"></td>
                  </tr>
                  <tr>
                    <td class="main"><?php echo TEXT_PRODUCTS_TAG ; ?></td>
                    <td class="main"><?php echo $products['products_head_tag']; ?></td>
                    <td class="main"></td>
                  </tr>
                </table></td>
              </tr>
                <tr>
                  <td><?php echo osc_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>

           </table></td>
          </tr>
      </table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
 require('includes/footer.php');
 require('includes/application_bottom.php');
