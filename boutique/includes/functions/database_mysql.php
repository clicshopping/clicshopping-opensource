<?php
/**
 * database_mysql.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

  function osc_db_connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $link = 'db_link') {
    global $$link;


    if (USE_PCONNECT == 'true') {
      $server = 'p:' . $server;
      $$link = mysqli_connect($server, $username, $password, $database);
    } else {
      $$link = mysqli_connect($server, $username, $password, $database);      
    }

    if ($$link) { 

    } else {
// Alert by mail if there is a problem with the database
 
      require_once (DIR_WS_CLASSES . 'phpmailer/class.phpmailer.php');

      $mail = new PHPMailer(true);
      $mail->IsMAIL();
      $mail->From = 'support@e-imaginis.com';
      $mail->FromName = 'ClicShopping Administrator';
      $mail->AddAddress('support@e-imaginis.com');
      $subject = 'Erreur de connexion a la base de donnees concernant Clicshopping';
      $mail->Subject = $subject;
      $message = "Veuillez verifier les sites internet sur le serveur, un probleme de connexion a la base de donnees s\'est produit :". date ('G') . '/' . date('m').'/'.date('Y') . ' sur le domaine ' . HTTP_SERVER;
      $mail->Body = $message;

// verification
      $mail->IsHTML(true); // send as HTML

      if(!$mail->Send()){ //Teste le return code de la fonction
        echo $mail->ErrorInfo; //Affiche le message d'erreur (ATTENTION:voir section 7)
      } else {
        echo '<br /><br />An alert mail was sent to the administrator<br /><br />';
      }

      $mail->SmtpClose();
      unset($mail);

      exit;    
   }

    return $$link;
  }

  function osc_db_close($link = 'db_link') {
    global $$link;

    return mysqli_close($$link);
  }

  function osc_db_error($query, $errno, $error) { 
    die('<p style="color:#000000;"><strong>' . $errno . ' - ' . $error . '<br /><br />' . $query . '</strong><p><p style="color:#ff0000;">[TEP STOP]</p>');
  }

  function osc_db_query($query, $link = 'db_link') {
    global $$link;

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
      error_log('QUERY ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
    }

    $result = mysqli_query($$link, $query) or osc_db_error($query, mysqli_errno($$link), mysqli_error($$link));

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
       $result_error = mysqli_error($$link);
       error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
    }

    return $result;
  }

  function osc_db_fetch_array($db_query) {
    return mysqli_fetch_array($db_query, MYSQLI_ASSOC);
  }

  function osc_db_num_rows($db_query) {
    return mysqli_num_rows($db_query);
  }

  function osc_db_data_seek($db_query, $row_number) {
    return mysqli_data_seek($db_query, $row_number);
  }

  function osc_db_insert_id($link = 'db_link') {
    global $$link;

    return mysqli_insert_id($$link);
  }

  function osc_db_free_result($db_query) {
    return mysqli_free_result($db_query);
  }

  function osc_db_fetch_fields($db_query) {
    return mysqli_fetch_field($db_query);
  }

  function osc_db_output($string) {
    return htmlspecialchars($string);
  }

  function osc_db_input($string, $link = 'db_link') {
    global $$link;

    return mysqli_real_escape_string($$link, $string);
  }

  function osc_db_prepare_input($string) {
    if (is_string($string)) {
      return trim(osc_sanitize_string($string));
    } elseif (is_array($string)) {
      reset($string);
      while (list($key, $value) = each($string)) {
        $string[$key] = osc_db_prepare_input($value);
      }
      return $string;
    } else {
      return $string;
    }
  }

  function osc_db_affected_rows($link = 'db_link') {
    global $$link;

    return mysqli_affected_rows($$link);
  }

  function osc_db_get_server_info($link = 'db_link') {
    global $$link;

    return mysqli_get_server_info($$link);
  }

  if ( !function_exists('mysqli_connect') ) {
    define('MYSQLI_ASSOC', MYSQL_ASSOC);

    function mysqli_connect($server, $username, $password, $database) {
      if ( substr($server, 0, 2) == 'p:' ) {
        $link = mysql_pconnect(substr($server, 2), $username, $password);
      } else {
        $link = mysql_connect($server, $username, $password);
      }

      if ( $link ) {
        mysql_select_db($database, $link);
      }

      return $link;
    }

    function mysqli_close($link) {
      return mysql_close($link);
    }

    function mysqli_query($link, $query) {
      return mysql_query($query, $link);
    }

    function mysqli_errno($link = null) {
      return mysql_errno($link);
    }

    function mysqli_error($link = null) {
      return mysql_error($link);
    }

    function mysqli_fetch_array($query, $type) {
      return mysql_fetch_array($query, $type);
    }

    function mysqli_num_rows($query) {
      return mysql_num_rows($query);
    }

    function mysqli_data_seek($query, $offset) {
      return mysql_data_seek($query, $offset);
    }

    function mysqli_insert_id($link) {
      return mysql_insert_id($link);
    }

    function mysqli_free_result($query) {
      return mysql_free_result($query);
    }

    function mysqli_fetch_field($query) {
      return mysql_fetch_field($query);
    }

    function mysqli_real_escape_string($link, $string) {
      if ( function_exists('mysql_real_escape_string') ) {
        return mysql_real_escape_string($string, $link);
      }
      return addslashes($string);
    }

    function mysqli_affected_rows($link) {
      return mysql_affected_rows($link);
    }

    function mysqli_get_server_info($link) {
      return mysql_get_server_info($link);
    }
  }