<?php
/**
 * newlsetter_no_account.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/
 require($OSCOM_Template->getTemplateHeaderFooter('header'));

 require($OSCOM_Template->getTemplateFiles('breadcrumb'));

?>
<div class="clearfix"></div>

<?php
  if ( $OSCOM_MessageStack->exists('newsletter_no_account') ) {
    echo $OSCOM_MessageStack->get('newsletter_no_account');
  }

// ---------------------- 
// --- Success  -----  
// ---------------------- 

  if (isset($_GET['action']) && ($_GET['action'] == 'success')) {
?>
<div class="contentContainer">
  <div class="contentText">
    <?php echo TEXT_SUCCESS; ?>
  </div>

  <div style="float: right;"><?php echo osc_draw_button(IMAGE_BUTTON_CONTINUE, null, osc_href_link('index.php'),'success'); ?>
  </div>
</div>
<?php
  } else {
// ---------------------- 
// --- Newsletter   -----  
// ----------------------
   echo osc_draw_form('newsletter_no_account', osc_href_link('newsletter_no_account.php', 'action=send'), 'post', '', true);
?>
<div><?php echo osc_draw_separator('pixel_trans.gif', '10', '30'); ?></div>
<div class="contentContainer">
  <div class="contentText">
    <div class="page-header">
      <span class="text-warning" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></span>
      <h3><?php echo CATEGORY_PERSONAL; ?></legend></h3>
    </div>
  </div>
  <div class="contentText">
    <div class="form-group has-feedback">
      <label for="firstname" class="control-label col-sm-4"><?php echo ENTRY_LAST_NAME; ?></label>
      <div class="col-sm-6">
        <span class="text-warning pull-left"><?php echo osc_draw_input_field('firstname', NULL, 'required aria-required="true" id="firstname" placeholder="' . ENTRY_LAST_NAME . '" minlength="'. ENTRY_FIRST_NAME_MIN_LENGTH .'"'); ?></span>
        <span class="text-warning pull-left">&nbsp; <?php echo ENTRY_FIRST_NAME_TEXT; ?></span>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group has-feedback"  style="padding-top:10px;">
      <label for="lastname" class="control-label col-sm-4"><?php echo ENTRY_LAST_NAME; ?></label>
      <div class="col-sm-6">
        <span class="text-warning pull-left"><?php echo osc_draw_input_field('lastname', NULL, 'required aria-required="true" id="lastname" placeholder="' . ENTRY_LAST_NAME . '" minlength="'. ENTRY_LAST_NAME_MIN_LENGTH .'"'); ?></span>
        <span class="text-warning pull-left">&nbsp; <?php echo ENTRY_LAST_NAME_TEXT; ?></span>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group has-feedback" style="padding-top:10px;">
      <label for="email_address" class="control-label col-sm-4"><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
      <div class="col-sm-6">
        <span class="text-warning pull-left"><?php echo osc_draw_input_field('email_address', NULL, 'required aria-required="true" id="email_address" placeholder="' . ENTRY_EMAIL_ADDRESS . '"', 'email'); ?></span>
        <span class="text-warning pull-left">&nbsp;<?php echo ENTRY_EMAIL_ADDRESS_TEXT; ?></span>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="form-group has-feedback" style="padding-top:10px;">
      <label for="email_address_confirm" class="control-label col-sm-4"><?php echo ENTRY_EMAIL_ADDRESS_CONFIRMATION; ?></label>
      <div class="col-sm-6">
        <span class="text-warning pull-left"><?php echo osc_draw_input_field('email_address_confirm', NULL, 'required aria-required="true" id="email_address_confirm" placeholder="' . ENTRY_EMAIL_ADDRESS_CONFIRMATION . '"', 'email'); ?></span>
        <span class="text-warning pull-left">&nbsp;<?php echo ENTRY_EMAIL_ADDRESS_TEXT; ?></span>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  </div>
<?php 
// ---------------------- 
// --- Option   -----  
// ---------------------- 
?>

    <div class="contentText">
      <div class="page-header">
        <h3><?php echo CATEGORY_OPTIONS; ?></h3>
      </div>
    </div>
    <div class="contentText">
      <div class="form-group has-feedback">
        <label for="inputConfirmation" class="control-label col-xs-3"><?php echo ENTRY_NUMBER_EMAIL_CONFIRMATION; ?><span class="text-warning"><?php echo $number_confirmation; ?></span></label>
        <div class="col-sm-6">
          <span class="text-warning pull-left"><?php echo osc_draw_input_field('number_email_confirmation', NULL, 'required aria-required="true" id="inputFromEmail" placeholder="' . ENTRY_NUMBER_EMAIL_CONFIRMATION . '"', 'confirmation'); ?></span>
          <span class="text-warning pull-left">&nbsp; <?php echo ENTRY_SPAM_TEXT; ?></span>
        </div>
      </div>
    </div>
<?php
// ---------------------- 
// --- regulation   -----  
// ---------------------- 
  if (DISPLAY_PRIVACY_CONDITIONS == 'true') {
?>
    <div class="contentText">
      <div class="control-group col-sm-10">
        <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
        <div class="row">
          <?php echo TEXT_PRIVACY_CONDITIONS_DESCRIPTION; ?>
        </div>
      </div>
    </div>
<?php
  }
?>
    <div class="control-group">
      <div><?php echo osc_draw_separator('pixel_trans.gif', '10', '10'); ?></div>
      <div class="controls">
        <div class="buttonSet">
          <div class="pull-right"><?php echo  osc_draw_button(IMAGE_BUTTON_CONTINUE, null, null, 'success', null, null); ?></div>
        </div>
      </div>
    </div>

</form>
<?php
  }
 require($OSCOM_Template->getTemplateHeaderFooter('footer'));
?>