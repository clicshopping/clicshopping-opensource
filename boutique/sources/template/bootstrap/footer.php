<?php
/**
 * footer.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license Licensed under GPL and MIT
 * @version 
*/
?>
          </div><!-- end bodyContent -->

<?php
  if ($OSCOM_Template->hasBlocks('boxes_column_left')) {
?>

          <div id="columnLeft" class="col-md-<?php echo $OSCOM_Template->getGridColumnWidth(); ?> col-md-pull-<?php echo $OSCOM_Template->getGridContentWidth(); ?>">
            <?php echo $OSCOM_Template->getBlocks('boxes_column_left'); ?>
          </div>
<?php
  }

  if ($OSCOM_Template->hasBlocks('boxes_column_right')) {
?>

          <div id="columnRight" class="col-md-<?php echo $OSCOM_Template->getGridColumnWidth(); ?>">
            <?php echo $OSCOM_Template->getBlocks('boxes_column_right'); ?>
          </div>
<?php
}
?>
          <div class="clearfix"></div>

<?php //important before block ?>
          <footer>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
            <div class="footer"><?php echo $OSCOM_Template->getBlocks('modules_footer'); ?></div>
            <div class="footerSuffix"><?php echo $OSCOM_Template->getBlocks('modules_footer_suffix'); ?></div>
<?php echo $OSCOM_Template->getBlocks('footer_scripts'); ?>
            <script type="text/javascript">  $('.productListTable tr:nth-child(even)').addClass('alt');</script>
          </footer>
        </div> <!-- container -->
      </div> <!-- wrap //-->
    </div> <!-- bodyWrapper //-->
  </body>
</html>
<?php 
  require('includes/counter.php');
  require('includes/application_bottom.php');

