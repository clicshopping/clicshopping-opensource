<?php
/**
 * products_extra_fields.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Ajout de champs supplémentaires fiche produits');
define('SUBHEADING_TITLE', 'Ajouter un nouveau champs');

define('HEADING_TITLE_INSERT','Insertion du nom du champs supplémentaire');
define('HEADING_TITLE_MODIFIY','Modifier un champs');

define('TABLE_HEADING_ID_FIELDS', 'Id Champs');
define('TABLE_HEADING_DELETE_FIELDS', 'Suppression');
define('TABLE_HEADING_FIELDS', 'Nom du champs');
define('TABLE_HEADING_FIELDS_TYPE','Type de champs');
define('TABLE_HEADING_CUSTOMERS_GROUP','Groupe Client');

define('TABLE_HEADING_ORDER', 'Ordre de tri');
define('TABLE_HEADING_LANGUAGE', 'Language');
define('TABLE_HEADING_STATUS_DIPSLAY', 'Statut d\'affichage catalogue');
define ('TEXT_ALL_LANGUAGES', 'Tous');
define('ENTRY_TEXT','Champs texte simple (court)');
define('ENTRY_TEXT_AREA','Champs descriptif (long)');
define('ENTRY_TEXT_CHECKBOX','Case à cocher');
define('TITLE_AIDE_OPTIONS','Aide sur l\'ajout de champs supplémentaire');
define('TEXT_AIDE_OPTIONS','<li>L\'affichage en mode B2B (hors client normal) permet de voir tous les champs qui ont été créés  pour un client normal dans la fiche de produit');

?>