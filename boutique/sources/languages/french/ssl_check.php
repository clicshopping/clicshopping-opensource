<?php
/**
 * 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('NAVBAR_TITLE', 'Contr&ocirc;le de la sécurité');
define('HEADING_TITLE', 'Contr&ocirc;le de la sécurité');

define('TEXT_INFORMATION', 'Nous avons détecté que votre navigateur a produit une session SSL différente de l\'ID employé partout dans nos pages sécurisées. .<br /><br />Pour des mesures de sécurité vous devrez à l\'ouverture de session de votre compte de nouveau continuer à faire des achats en ligne.<br /><br />Quelques navigateurs comme Konqueror 3.1 n\'ont pas la capacité de produire une session SSL ID automatique sécurisé ce que nous exigeons. Si vous employez un tel navigateur, nous recommandons de passer é par un autre navigateur comme <a href="http://www.microsoft.com/ie/" target="_blank">Microsoft Internet Explorer</a>, <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Netscape</a>, ou <a href="http://www.mozilla.org/releases/" target="_blank">Mozilla</a>, pour continuer vos achats en ligne.<br /><br />Nous avons pris cette mesure de sécurité pour votre avantage, et nous nous excusons si n\'importe quels inconvénients sont causés .<br /><br />Entrez s\'il vous pla&icirc;t en contact avec le propriétaire du magasin si vous avez des questions touchant à cette exigence, ou continuer vos achats en ligne.');

define('BOX_INFORMATION_HEADING', 'Vie privée et sécurité ');
define('BOX_INFORMATION', 'Nous validons la session SSL ID automatiquement produit par votre navigateur à chaque demande de page sécurisé faite à ce serveur.<br /><br />Cette validation assure qu\'il n\'y est que vous qui naviguez sur ce site avec votre compte et pas quelqu\'un d\'autre.');
?>