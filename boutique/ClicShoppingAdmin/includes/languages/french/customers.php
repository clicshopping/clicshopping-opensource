<?php
/**
 * customers.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Clients');
define('HEADING_TITLE_EDIT', 'Code client No ');
define('HEADING_TITLE_SEARCH', 'Rechercher');

define('TABLE_HEADING_CUSTOMERS_ID', 'Référence');
define('TABLE_HEADING_FIRSTNAME', 'Prénom');
define('TABLE_HEADING_LASTNAME', 'Nom');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Compte créé');
define('TABLE_HEADING_ACTION', 'Actions');
define('TABLE_HEADING_COUNTRY','Provenance');
define('TABLE_HEADING_NUMBER_OF_REVIEWS','Nbr de commentaires');
define('TABLE_HEADING_ENTRY_EMAIL_VALIDATION','Email valide');

define('TABLE_ENTRY_GROUPS_NAME','Groupes Clients');

define('TABLE_HEADING_ENTRY_COMPANY_B2B','Société(B2B)');
define('TABLE_HEADING_ENTRY_COMPANY','Société');
define('TEXT_DATE_ACCOUNT_CREATED', 'Compte cré&eacute :');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Dernière modification :');
define('TEXT_INFO_DATE_LAST_LOGON', 'Dernière connexion :');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Nombre de connexions :');
define('TEXT_INFO_COUNTRY', 'Pays :');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Nombre d\'avis du client :');
define('TEXT_DELETE_INTRO', 'Etes vous s&ucirc;r de vouloir supprimer ce client ?');
define('TEXT_DELETE_REVIEWS', 'Supprimer %s le commentaire');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Supprimer le client');
define('TYPE_BELOW', 'Type ci-dessous');
define('PLEASE_SELECT', 'Choisissez en un');
define('TEXT_CUSTOMER_DISCOUNT', 'Taux de Réduction Client :');

define('TEXT_INFO_NUMBER_OF_ORDERS', 'Nombre de commandes réalisé :');

// Approbation ducompte client
define('TABLE_ENTRY_VALIDATE','Clients à valider');
define('APPROVED_CLIENT','Client B2B Non validé');

// Customer mail
define('EMAIL_SUBJECT', 'Ouverture de compte client sur ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_MS', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");
define('EMAIL_GREET_NONE', 'Bonjour ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ',' . "\n\n");

define('IMAGE_CUSTOMER','Nouveau client');
define('IMAGE_EMAIL_APPROVED','Email du client valide');
define('IMAGE_EMAIL_NOT_APPROVED',' Email du client non valide');


define('EMAIL_PASS','Votre mot de passe : ');
define('EMAIL_NETWORK','Suivez nous en temps réel sur :<br />');
define('EMAIL_TWITTER','http://www.twitter.com/');
define('EMAIL_FACEBOOK','http://www.facebook.com/');

// notes
define('CUSTOMERS_NOTE','Notes spécifiques concernant le client');
define('CUSTOMERS_NOTE_SUMMARY','Notes');
define('TAB_NOTES','Notes Client');

// envoi email
define('ENTRY_CONTACT_CUSTOMER','Souhaitez vous informer le client de la création du compte par email ?');
define('ENTRY_COUPON_CUSTOMER','Souhaitez vous offrir un coupon de réduction au client ?');
define('ENTRY_CUSTOMERS_YES','Oui');
define('ENTRY_CUSTOMERS_NO','Non');
define('TEXT_CONTACT_EMAIL','Informer le client');


// Autorisation de modification pour les clients
define('ENTRY_CUSTOMERS_MODIFY_COMPANY', 'Autoriser le client à modifier ses informations');
define('ENTRY_CUSTOMERS_MODIFY_ADDRESS_DEFAULT', 'Autoriser le client à modifier l\'adresse principale');
define('ENTRY_CUSTOMERS_ADD_ADDRESS', 'Autoriser le client à ajouter d\'autres adresses');
define('ENTRY_CUSTOMER_LOCATION','Localiser ');

// TVA Intracom
define('TVA_INTRACOM_VERIFY','Vérifier');
define('TITLE_AIDE_CUSTOMERS_TVA','Note sur la TVA intracommunautaire');
define('TITLE_AIDE_TVA_CUSTOMERS','Veuillez valider la fiche avant de vérifier le numéro de TVA Intracommunautaire de la société');

// Message pour les erreurs de saisie
define('ERROR_ENTRY_CUSTOMERS_MODIFY_YES', 'Autorisé');
define('ERROR_ENTRY_CUSTOMERS_MODIFY_NO', 'Refusé');

// Aide
define('TITLE_AIDE_CUSTOMERS_IMAGE', 'Aide');
define('TITLE_AIDE_CUSTOMERS_DEFAULT_ADRESSE', 'Note sur les autorisations de modifications des clients');
define('TEXT_AIDE_CUSTOMERS_DEFAULT_ADRESSE', '<u>Autoriser le client à ajouter d\'autres adresses</u> :<ul><li>Désactivé il interdit le client d\'ajouter de nouvelles adresses dans son carnet.</li><li>Option true dans la zone <i><strong>Mon Compte</strong></i> du client et lors des <i><strong>procédures de commandes</strong></i>.</li></ul><br /><u>Autoriser le client à modifier l\'adresse principale</u> :<ul><li>Désactivé il interdit le client de modifier ou supprimer l\'adresse par défaut dans son carnet.</li><li>Désactivé il interdit également <i><strong>la possibilité de choisir une autre adresse de facturation</i></strong> lors de la procèdure d\'une commande.</li></ul>');

// Raccourcis
define('TEXT_EDIT_GROUP_CUSTOMER', 'Modifier le groupe');
define('ICON_EDIT_CUSTOMER', 'Editer la fiche client');
define('ICON_EDIT_ORDERS', 'Rechercher les commandes du client');
define('ICON_EDIT_CUSTOMERS_GROUP','Editer le groupe client');
define('ICON_EDIT_NEW_PASSWORD','Envoyer un nouveau mot de passe');
?>