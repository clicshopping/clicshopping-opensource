<?php
/**
 * import.php 
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2006 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
*/

define('HEADING_TITLE', 'Import Universal Database');
define('HEADING_TITLE_STEP1', 'Mapping');
define('HEADING_TITLE_STEP2', 'Test or validation');
define('HEADING_TITLE_STEP3', 'Download the module');
define('HEADING_TITLE_STEP4', 'further instructions');
define('HEADING_TITLE', 'Informations');

define('DATABASE_COLUMNS','Fields of the database');
define('DATABASE_TYPE','Field Type');
define('DATABASE_DEFAULT', 'Defaults');
define('IMPORT_FIELD_FILES','Fields from the imported file');

define('TEXT_COLUMN_NUMBER','No. of columns that can be treated : ');
define('TEXT_LINE_IMPORT','<br>No. of lines in the file imported : ');

define('TEXT_SELECTED','-- Select --');
define('TEXT_CATEGORIES', 'Import categories');
define('TEXT_CATEGORIES_DESCRIPTION','Import the categories description');
define('TEXT_PRODUCTS','Import Products');
define('TEXT_PRODUCTS_DESCRIPTION','Import product description');
define('TEXT_PRODUCT_IN_CATEGORIES', 'Import products in a specific category');
define('TEXT_MANUFACTURERS','Import brands');
define('TEXT_MANUFACTURERS_INFO','Import information about my brands');
define('TEXT_SUPPLIERS','Import suppliers');
define('TEXT_SUPPLIERS_INFO','Import suppliers informations');

define('TEXT_CHOOSE_FILE', 'Choose CSV file to import');
define('TEXT_FIELDS_DELIMITED_BY','For the csv files, the field delemited by :');
define('TEXT_ENTER_DELIMITED_BY', 'For the csv files, the enter is delimited by :');
define('TEXT_TEST_IMPORT','Do you wish release a test before importation ? (strongly recommanded)');
define('TEXT_REFERENCE_ID','Indicate the fields reference_id ?');
define('TEXT_EXAMPLE','Data example will be inserted in the database');

define('TEXT_VIRTUAL_CATEGORIES_IMPORT','virtual Import (not seen in the catalog)');

define('TEXT_IMPORT_NO_FILE','No file');
define('TEXT_IMPORT_FILE_EMPTY','the file is empty. There is no data inside.');
define('TEXT_IMPORT_TEST_DATA','Import the first 6 line of the file');
define('TEXT_IMPORT_SUCCESS','The import is complete');
define('TEXT_IMPORT_SELECT_TYPE','Please select the type of import');
define('TEXT_IMPORT_OFF','Do you whish to put this importation off line ?');

define('TEXT_YES','Yes');
define('TEXT_NO','No');

define('TITLE_HELP_GENERAL','information');
define('HELP_GENERAL','<li>Please read this section before starting the import process.</li>
<blockquote>
<li><strong>An import procedure is risky and can seriously damage your database. </ Strong> You assume therefore the result of the import and impact in the database.</li>
<li>Tous les produits sont automatiquement sur le statut off, non visibles par les clients. C\'est à vous de les activer.</li>
<li>En fonction des options que vous allez choisir, les produits peuvent etre importés dans une catégorie virtuelle spécialement créée lors de l\'importation. Si vous procédez à une autre importation, une nouvelle catégorie virtuelle sera alors créée.<br>
la catégorie virtuelle n\'est pas visible dans le catalogue, vous devez retraiter vos produits et les déplacer dans les catégories adéquates</li>
</li>
<li>Veuillez faire attention a ce que votre fichier ne soit pas trop important, les d&eacutelais de traitement peuvent etre long et prendre beaucoup de ressources et pourraient causer un crash si le délai de traitement dépassent les 60 sec.</li>
<li>Your. Csv files must have a header and you must specify an id in the form of this header.</li>
<li>Le multilangue n\'est pas traité dans le cadre de cette import, le nom de votre produit sera alors enregistré par défaut dans toutes les langues de ClicShopping. C\'est à vous de refaire les traductions.</li>
<li>N\'oubliez pas d\'utiliser habillement la mise à jour rapide pour vous faire gagner encore du temps !</li>
');


// step1
define('WELCOME_HEADING_TITLE','<br /><br />Welcome to the import system Universal<br /><br />
        <ul>Installation</ul>
        <blockquote>
         <li>Step 1 - Select criteria and import the file to import</li>
         <li>Step 2 - Import and mapping data</li>
         <li>Step 3 - Import test or final validation</li>
        </blockquote>
           <br /><br />
');

// step1
define('WELCOME_MESSAGE_STEP1','
        <ul>Mapping Process</ul>
        <blockquote>
         <li>Step 2 - Import and mapping data</li>
         <li>Step 3 - Import test or final validation</li>
        </blockquote>
           <br /><br />
          <p align="center">Please select from the following options</p>
');

?>