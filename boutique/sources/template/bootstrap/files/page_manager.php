<?php
/*
 * page_manager.php
 * @copyright Copyright 2008 - http://www.e-imaginis.com
 * @copyright Portions Copyright 2003 osCommerce
 * @license GNU Public License V2.0
 * @version $Id: 
  
*/
 require($OSCOM_Template->getTemplateHeaderFooter('header'));
  define(HEADING_TITLE,'');
  require($OSCOM_Template->getTemplateFiles('breadcrumb'));

// ---------------------- 
// --- Title   -----  
// ---------------------- 
?>
<div class="contentContainer">
  <div class="pageHeading"><span class="pageHeading"><?php echo $page_manager->pages_informations['pages_title']; ?></span></div>
<?php
// ---------------------- 
// --- page html   -----  
// ---------------------- 
?>  
   <div class="contentText"> 
      <div class="pageManager"><?php echo $page_manager->pages_informations['pages_html_text']; ?></div>
   </div>
</div>
<?php require($OSCOM_Template->getTemplateHeaderFooter('footer')); ?>
